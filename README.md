# NA64 Data Processing

[![pipeline status](https://gitlab.cern.ch/P348/na64sw/badges/dev/pipeline.svg)](https://gitlab.cern.ch/P348/na64sw/-/commits/dev)
[![coverage](https://gitlab.cern.ch/P348/na64sw/badges/dev/coverage.svg?job=lcg105-gcc13-coverage)](https://na64sw.docs.cern.ch/na64sw-tests-coverage.dev/)

This project is devoted to the analysis and reconstruction of the HEP
experimental data.

Currently its main goal is to handle data produced by
the [NA64 experiment](http://na64.web.cern.ch/) (CERN, SPS).

## Repository Content and Structure

The project is written on C/C++. Mainly, project structure includes few shared
libraries that provide an API for users applications that implement a [pipeline
pattern](https://en.wikipedia.org/wiki/Pipeline_(software)) for data
processing. Aside from common libraries, a set of common data handling
executable procedures are provided within a dedicated libraries.

For details of the source code structure, components and API exposed consider
reading of the [corresponding documentation page](https://na64sw.docs.cern.ch/advanced/repo-structure.html).

## Public builds in CERN

A CVMFS is used to facilitate runnable package deployment for those who work
within CERN IT infrastructure. That has been considered as the most convenient
option for anyone involved in NA64 activity. Access can be checked with:

    $ source /cvmfs/na64.cern.ch/sft/LCG_105/x86_64-el9-gcc13-dbg/this-env.sh
    $ module load na64sw/0.4.dev
    $ na64sw-pipe --help

Refer to documentation pages for further reference.

## Documentation

A public online documentation page including basic tutorial and various
advanced topics is [available here](https://na64sw.docs.cern.ch/).
These pages get updated automatically and follows latest commits in `dev`
branch.

# Authors

On behalf of [NA64 collaboration](na64.web.cern.ch), chronologically:

* Renat R. Dusaev
* Ivan Kramoykin
* Ilya Voronchikhin
* Alexey Shevelev

# License

Source code is distributed under GPL license:

> Copyright 2018-2022 NA64 Collaboration, CERN
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.

