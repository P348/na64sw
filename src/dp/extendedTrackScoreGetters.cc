#include "na64dp/extendedTrackScoreGetters.hh"

namespace na64dp {

// trivial "getter" -- returns 1
static ExtendedTrackScoreGetters::EventGetterValue_t
    _identity(const event::ScoreFitInfo &) { return 1.0; }

// trivial "getter" -- returns 0
static ExtendedTrackScoreGetters::EventGetterValue_t
    _zero(const event::ScoreFitInfo &) { return 0.0; }

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_uncertainty_u(DetID did, const event::ScoreFitInfo & sfi) const {
    if(!sfi.score) return std::nan("0");
    const auto & item = get_placement_defined_item_for(did);
    std::cout << " xxx " << sfi.score->lRErr[0] << "*" << item.uNorm << std::endl;  // XXX
    return sfi.score->lRErr[0]*item.uNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_uncertainty_v(DetID did, const event::ScoreFitInfo & sfi) const {
    if(!sfi.score) return std::nan("0");
    const auto & item = get_placement_defined_item_for(did);
    return sfi.score->lRErr[1]*item.vNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_uncertainty_w(DetID did, const event::ScoreFitInfo & sfi) const {
    if(!sfi.score) return std::nan("0");
    const auto & item = get_placement_defined_item_for(did);
    return sfi.score->lRErr[2]*item.wNorm;
}


ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_biased_err_u(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRBErr[0]*item.uNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_biased_err_v(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRBErr[1]*item.vNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_biased_err_w(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRBErr[2]*item.wNorm;
}


ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_unbiased_err_u(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRUErr[0]*item.uNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_unbiased_err_v(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRUErr[1]*item.vNorm;
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::metric_unbiased_err_w(DetID did, const event::ScoreFitInfo & sfi) const {
    const auto & item = get_placement_defined_item_for(did);
    return sfi.lRUErr[2]*item.wNorm;
}


ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_cx( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.center[0];
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_cy( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.center[1];
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_cz( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.center[2];
}


ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_su( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.size[0];
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_sv( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.size[1];
}

ExtendedTrackScoreGetters::EventGetterValue_t
ExtendedTrackScoreGetters::detector_sw( DetID did, const event::ScoreFitInfo &) const {
    const auto & item = get_placement_defined_item_for(did);
    return item.placement.size[2];
}

static const struct {
    const char * name;
    ExtendedTrackScoreGetters::Getter getter;
} gGetters[] = {
    {"identity",    {true,  {.plainGetter = _identity}} },
    {"zero",        {true,  {.plainGetter = _zero}}     },
    //
    {"metricUncertaintyU",  {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_uncertainty_u}}},
    {"metricUncertaintyV",  {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_uncertainty_v}}},
    {"metricUncertaintyW",  {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_uncertainty_w}}},
    {"metricUResidualU",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_unbiased_err_u}}},
    {"metricUResidualV",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_unbiased_err_v}}},
    {"metricUResidualW",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_unbiased_err_w}}},
    {"metricBResidualU",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_biased_err_u}}},
    {"metricBResidualV",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_biased_err_v}}},
    {"metricBResidualW",    {false, {.methodGetter = &ExtendedTrackScoreGetters::metric_biased_err_w}}},
    {"det_cx",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_cx}}},
    {"det_cy",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_cy}}},
    {"det_cz",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_cz}}},
    {"det_su",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_su}}},
    {"det_sv",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_sv}}},
    {"det_sw",              {false, {.methodGetter = &ExtendedTrackScoreGetters::detector_sw}}},
    // Sentinel:
    {NULL, {false, {nullptr}}},
};

ExtendedTrackScoreGetters::Getter
ExtendedTrackScoreGetters::get_extended_getter(const std::string & nm) {
    for(const auto * c = gGetters; c->name; ++c ) {
        if(!strcmp(c->name, nm.c_str())) return c->getter;
    }
    ExtendedTrackScoreGetters::Getter emptyG = {false};
    bzero(&emptyG.cllb, sizeof(emptyG.cllb));
    return emptyG;
}

}  // namespace na64dp

