#include "na64dp/abstractHandler.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {

AbstractHandler::AbstractHandler()
            : _lmemPtr(nullptr)
            , _log(log4cpp::Category::getInstance("handlers")) {
    msg_debug( _log
             , "New handler instance created: %p"
             , this );
}

AbstractHandler::AbstractHandler(log4cpp::Category & logCat)
            : _lmemPtr(nullptr)
            , _log(logCat) {
    msg_debug( _log
             , "New handler instance created: %p"
             , this );
}

void
AbstractHandler::finalize() {
    msg_debug( _log
             , "Handler %p done."
             , this );
}

void
AbstractHandler::set_local_memory( LocalMemory & lmem_ ) {
    assert( ! _lmemPtr );
    _lmemPtr = &lmem_;
}

void
AbstractHandler::reset_local_memory() {
    assert( _lmemPtr );
    _lmemPtr = nullptr;
}

namespace aux {
log4cpp::Category &
get_logging_cat(const YAML::Node & cfg) {
    std::string catName;
    if(cfg["_log"]) {
        catName = cfg["_log"].as<std::string>();
    } else if(cfg["_label"]) {
        catName = "handlers." + cfg["_label"].as<std::string>();
    } else {
        catName = "handlers";
    }
    return log4cpp::Category::getInstance(catName);
}
}  // namespace ::na64dp::aux
}  // namespace na64dp

