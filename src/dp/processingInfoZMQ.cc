# include "na64dp/processingInfoZMQ.hh"

# include <sstream>
# include <iostream>  // xxx, for cerr

#if defined(ZMQ_FOUND) && ZMQ_FOUND

namespace na64dp {

static const char gEPITag[] = "epi:";

ZMQPubEventProcessingInfo::ZMQPubEventProcessingInfo( int portNo
                                                  , size_t nMaxEvents
                                                  , unsigned int refreshInterval)
                                : EvProcInfoDispatcher( nMaxEvents, refreshInterval )
                                , _zCtx(1)
                                , _zPubSock(_zCtx, ZMQ_PUB) {
    std::ostringstream oss;
    oss << "tcp://*:" << portNo;
    _zPubSock.bind( oss.str().c_str() );
}

// TODO: elaborate error reporting on 0MQ failures
void
ZMQPubEventProcessingInfo::_update_event_processing_info() {
    throw std::runtime_error("TODO: serialize and dispatch status");  // TODO
    // previously we had status serialized by means of capnp. Remnants of this
    // method are still saved in the `processingInfo.cc` (excerpt() method)
    #if 0
    // Get the short description of ongoing processing
    excerpt(_msgBuf);
    assert( _msgBuf.size() );

    // Prefix network message with 'EPS ' indicating that it is event
    // processing info data (to be filtered by subscribers)
    {
        if( ! _zPubSock.send( gEPITag, sizeof(gEPITag) - 1, ZMQ_SNDMORE ) ) {
            std::cerr << "Failed to send 0MQ msg prefix." << std::endl;
        }
        zmq::message_t message( _msgBuf.size() );
        memcpy( message.data()
              , _msgBuf.data()
              , _msgBuf.size() );
        if( ! _zPubSock.send( message, 0x0 ) ) {
            std::cerr << "Failed to send zmq msg" << std::endl;
        }
    }

    _msgBuf.clear();
    #endif
}

}  // namespace na64dp

# endif

