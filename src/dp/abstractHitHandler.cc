#include "na64dp/abstractHitHandler.hh"
#include <regex>

namespace na64dp {
namespace aux {

std::string
retrieve_det_selection( const YAML::Node &cfg ) {
    if( cfg["applyTo"] ) {
        //YAML_ASSERT_STR( cfg, "applyTo" );
        return cfg["applyTo"].as<std::string>();
    }
    return "";
}

std::string
get_naming_class(const YAML::Node & cfg) {
    return cfg["detectorNamingClass"]
        ? cfg["detectorNamingClass"].as<std::string>()
        : "default";
}

std::string
get_placements_class(const YAML::Node & cfg) {
    return cfg["detectorPlacementsClass"]
        ? cfg["detectorPlacementsClass"].as<std::string>()
        : "default";
}

std::string
get_val_entry_type(const std::string & str) {
    // TODO: when event x-macro will be implemented, extracted token shall
    // be verified against existing SADC hits set to provide getters of single
    // field within an event (like `mcTruth`).
    size_t n = str.find('/');
    if( std::string::npos == n ) return "event";
    return str.substr(0, n);
}

}  // namespace ::na64dp::aux

AbstractHitHandler<event::Track>::AbstractHitHandler( calib::Dispatcher & cdsp
                                                    , const std::string & selection_
                                                    , log4cpp::Category & logCat
                                                    , const std::string & namingCat
                      ) : HitHandlerStats(logCat)
                        , _permittedZones(0x0)
                        , _invertZonePatternMatching(false)
                        {
    std::string selection(selection_);
    if(selection.empty() || selection == "*") {
        _log.debug("Handler %p takes all tracks in an event.", this);
        return;  // match all tracks
    }
    //if((!namingCat.empty()) && namingCat != "default") {
    //    _log.warn("Naming class \"%s\" ignored by track handler.");
    //}

    const std::regex rxSel(R"rx(^(0x[0-9a-fA-F]+)(\s*\|\s*0x[0-9a-fA-F]+)*$)rx");
    // ^^^ examples: "0x1", "0x1|0x2", "0xDEAD | 0xBEEF", "0x0|0x0"
    if(!std::regex_match(selection, rxSel)) {
        NA64DP_RUNTIME_ERROR("Expression \"%s\" is not a tracking zone"
                " selection pattern.", selection.c_str());
    }

    std::unordered_set<int> parsed;
    const std::regex rxTok(R"rx(0x[0-9a-fA-F]+)rx");

    std::smatch sm;
    while(std::regex_search(selection, sm, rxTok)) {
        int code = std::stoi(sm.str(), nullptr, 16);
        parsed.insert(code);
        selection = sm.suffix();
    }
    assert(!parsed.empty());
    if(parsed.size() == 1) {
        _permittedZones = *parsed.begin();
        _log.debug("Handler %p takes tracks matching at least one zone in"
                " pattern %#x.", this, _permittedZones);
    } else {
        std::ostringstream oss;
        bool isFirst = true;
        for( auto code : parsed ) {
            if(!code) continue;
            _permittedZonePatterns.emplace(code);
            oss << (isFirst ? "" : ", ") << code;
        }
        _log.debug("Handler %p set up to handle tracks with zone patterns: %s."
                , this, oss.str().c_str() );
    }
    assert( ((bool)_permittedZones) == _permittedZonePatterns.empty() );
}

bool
AbstractHitHandler<event::Track>::zone_pattern_matches(int tp) const {
    bool matches = false;
    if( (!_permittedZones) && _permittedZonePatterns.empty() ) return true;
    assert( !(_permittedZones && (!_permittedZonePatterns.empty())) );
    if(_permittedZones) {
        matches = tp & _permittedZones;
    } else if( !_permittedZonePatterns.empty() ) {
        auto it = _permittedZonePatterns.find(tp);
        matches = (it != _permittedZonePatterns.end());
    }
    if( matches ) {
        return _invertZonePatternMatching ? false : true;
    } else {
        return _invertZonePatternMatching ? true : false;
    }
}

AbstractHandler::ProcRes
AbstractHitHandler<event::Track>::process_event(event::Event & e) {
    typedef event::Association<event::Event, event::Track> Association;
    _pr = kOk;
    bool keepGoing = false;
    std::vector<Association::Collection::iterator> tracksToRemove;
    _cEvPtr = &e;
    for( _cTrackIt = e.tracks.begin(); e.tracks.end() != _cTrackIt; ++_cTrackIt ) {
        if( ! zone_pattern_matches(_cTrackIt->first.zones()) ) {
            _log << log4cpp::Priority::DEBUG
                 << "Track with zone pattern " << (int) _cTrackIt->first.zones()
                 << " ignored (doesn't match).";
            continue;
        }
        ++_nHitsConsidered;
        _removeFieldEntry = false;
        keepGoing = process_hit( e.id, _cTrackIt->first, *(_cTrack = _cTrackIt->second) );
        if( _removeFieldEntry ) {
            tracksToRemove.push_back(_cTrackIt);
        }
        if(!keepGoing) break;
    }
    _cTrack.reset();
    for( auto it : tracksToRemove ) {
        _log << log4cpp::Priority::DEBUG
             << "Track " << it->first.code << " discriminated.";  // TODO: ostream op-r for TrackID
        Association::remove(e.tracks, it);
    }
    _nHitsDiscriminated += tracksToRemove.size();
    _cEvPtr = nullptr;
    return _pr;
}

}  // namespace na64dp

