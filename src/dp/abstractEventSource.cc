#include "na64dp/abstractEventSource.hh"
#include "na64util/str-fmt.hh"
#include "na64detID/TBName.hh"

#include <cstring>

namespace na64dp {

bool
AbstractEventSource::read_prev(event::Event &, event::LocalMemory &) {
    throw errors::UnsupportedDataSourceFeature("Access to previous event is"
            " not implemented by data source.");
}

bool
AbstractEventSource::read_by_id(EventID, event::Event &, event::LocalMemory &) {
    throw errors::UnsupportedDataSourceFeature("Random acces to events is"
            " not implemented by data source.");
}

size_t
AbstractEventSource::list_events(std::vector<EventID> &, size_t nPage, size_t nPerPage) {
    throw errors::UnsupportedDataSourceFeature("Events listing is"
            " not implemented by data source.");
}

AbstractEventSource::AbstractEventSource( iEvProcInfo * epi )
        : _log(log4cpp::Category::getInstance("sources"))
        , _epi(epi)
        {}

AbstractEventSource::AbstractEventSource( iEvProcInfo * epi
                                        , log4cpp::Category & logCat )
        : _log(logCat)
        , _epi(epi)
        {}

void
AbstractNameCachedEventSource::handle_update( const nameutils::DetectorNaming & names ) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(names);
    // Chips cache
    _detIDsCache.kSADC = names.chip_id( "SADC" );
    _detIDsCache.kAPV = names.chip_id( "APV" );
    _detIDsCache.kStwTDC = names.chip_id( "NA64TDC" );
    _detIDsCache.kF1 = names.chip_id( "F1" );
    // Kins caches
    // - SADC
    _detIDsCache.kECAL  = names.kin_id("ECAL").second;
    _detIDsCache.kHCAL  = names.kin_id("HCAL").second;
    _detIDsCache.kWCAL  = names.kin_id("WCAL").second;
    _detIDsCache.kVHCAL = names.kin_id("WCAL").second;
    _detIDsCache.kWCAT  = names.kin_id("WCAT").second;
    _detIDsCache.kZDCAL = names.kin_id("ZDCAL").second;
    _detIDsCache.kSRD   = names.kin_id("SRD").second;
    _detIDsCache.kVETO  = names.kin_id("VETO").second;
    _detIDsCache.kV     = names.kin_id("V").second;
    _detIDsCache.kS     = names.kin_id("S").second;
    _detIDsCache.kVTEC  = names.kin_id("VTEC").second;
    _detIDsCache.kVTWC  = names.kin_id("VTWC").second;
    _detIDsCache.kDM    = names.kin_id("DM").second;
    // - NA64TDC
    _detIDsCache.kSt    = names.kin_id("ST").second;
    _detIDsCache.kStt   = names.kin_id("STT").second;
    // - APV
    _detIDsCache.kMM    = names.kin_id("MM").second;
    _detIDsCache.kGEM   = names.kin_id("GM").second;
    // - F1
    _detIDsCache.kBMS   = names.kin_id("BM").second;
    _log.debug( "Source's names cache updated." );
}

const AbstractNameCachedEventSource::NameCache &
AbstractNameCachedEventSource::naming() const {
    if( _detIDsCache.kSADC == _detIDsCache.kAPV
     && _detIDsCache.kAPV == _detIDsCache.kStwTDC
     && 0x0 == _detIDsCache.kAPV ) {
        char errbf[256];
        snprintf( errbf, sizeof(errbf), "Chip IDs are not set for source %p;"
            " this probably indicates that none valid event number was given"
            " to calibration manager.", this );
        throw std::runtime_error( errbf );
    }
    return _detIDsCache;
}

AbstractNameCachedEventSource::AbstractNameCachedEventSource( calib::Manager & mgr
                                                            , log4cpp::Category & logCat
                                                            , const std::string & namingClass
                                                            , iEvProcInfo * epi
                                                            )
                    : AbstractEventSource(epi, logCat)
                    , calib::Handle<nameutils::DetectorNaming>(namingClass, mgr)
                    , _mgr(mgr)
                    { memset( &_detIDsCache, 0, sizeof(_detIDsCache) ); }

}  // namespace na64dp

