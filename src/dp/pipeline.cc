# include "na64dp/pipeline.hh"
# include "na64dp/processingInfo.hh"
# include "na64util/runtimeDirs.hh"
#include <exception>

namespace na64dp {

Pipeline::Pipeline(iEvProcInfo * pi) : _log(log4cpp::Category::getInstance("pipeline"))
                                     , _procInfoPtr(pi) {
    msg_debug( _log, "Empty pipeline object %p instantiated." );
}

Pipeline::Pipeline( YAML::Node cfg
                  , calib::Manager & dspPtr
                  , iEvProcInfo * pi ) : _log(log4cpp::Category::getInstance("pipeline"))
                                       , _procInfoPtr(pi) {
    if( ! cfg.IsSequence() ) {
        NA64DP_RUNTIME_ERROR( "Pipeline description is not a sequence." );
    }
    size_t nHandler = 0;
    for( auto cHandlerNode : cfg ) {
        AbstractHandler * hPtr;
        if( ! cHandlerNode.IsMap() ) {
            NA64DP_RUNTIME_ERROR( "Handler description #%zu is not"
                    " a map.", nHandler );
        }
        std::string handlerTypeStr;
        if( ! cHandlerNode["_type"] ) {
            NA64DP_RUNTIME_ERROR( "Handler description #%zu does not"
                    " provide \"_type\" property.", nHandler );
        }
        handlerTypeStr = cHandlerNode["_type"].as<std::string>();
        #if 0  // TODO: experimental feature -- conditional hanlers
        // check enableIf
        if( cHandlerNode["_enableIf"] ) {
            std::list<std::string> defs;
            if( cHandlerNode["_enableIf"].Type() == YAML::NodeType::Scalar ) {
                defs.push_back( cHandlerNode["_enableIf"].as<std::string>() );
            } else if( cHandlerNode["_enableIf"].Type() == YAML::NodeType::Sequence ) {
                defs = cHandlerNode["_enableIf"].as<std::list<std::string> >();
            } else {
                NA64DP_RUNTIME_ERROR( "\"_enableIf\" is expected to be a"
                        " string or list of strings." );
            }
            bool disable = false;
            for( const std::string & def : defs ) {
                if( (!cHandlerNode["__definitions"][def])
                 || (!cHandlerNode["__definitions"][def].as<bool>()) )
                    disable = true;
            }
            if(disable) continue;
        }
        #endif
        //try {
            hPtr = VCtr::self().make<AbstractHandler>( handlerTypeStr
                                                     , dspPtr
                                                     , cHandlerNode );
        //} catch(const std::exception & e) {
        //    _log.error( "An exception has been thrown during"
        //            " instantiation of handler %zu: %s", nHandler, e.what() );
        //    { // TODO: mark with padding or something...
        //        std::ostringstream yos;
        //        yos << cHandlerNode;
        //        _log.error( "Configuration of the problematic piece: \n%s", yos.str().c_str() );
        //    }
        //    throw;
        //}
        push_back( hPtr );
        if( _procInfoPtr ) {
            char epiLabelStr[256];
            if( cHandlerNode["_label"] && !cHandlerNode["_label"].as<std::string>().empty() ) {
                snprintf( epiLabelStr, sizeof(epiLabelStr)
                        , "%s : %s", cHandlerNode["_label"].as<std::string>().c_str(), handlerTypeStr.c_str() );
            } else {
                snprintf( epiLabelStr, sizeof(epiLabelStr)
                        , "%p : %s", hPtr, handlerTypeStr.c_str() );
            }
            _procInfoPtr->register_handler( hPtr, epiLabelStr );
        }
        ++nHandler;
    }
    msg_debug( _log
             , "Pipeline object %p with %zu handlers instantiated."
             , this, this->size() );
}

Pipeline::~Pipeline() {
    for( auto & handlerPtr : *this ) {
        handlerPtr->finalize();
    }
    for( auto & handlerPtr : *this ) {
        delete handlerPtr;
    }
}

std::pair<bool, bool>
Pipeline::process( event::Event & event, LocalMemory & lmem ) {
    bool processIngStoppedByHandler = false
       , eventDiscriminated = false;
    size_t nHandler = 0;
    if( _procInfoPtr ) {
        // Note: the notify_event_read() returns `false' when events counter
        // exceeds maximum number and probably may return it as feedback
        // mechanism for rudimentary remote control over running processing.
        // Nevertheless we do not use this result now.
        _procInfoPtr->notify_event_read();
        for( auto & handlerPtr : *this ) {
            ++nHandler;
            _procInfoPtr->notify_handler_starts( handlerPtr );

            AbstractHandler::ProcRes lpr;
            handlerPtr->set_local_memory(lmem);
            //try {
                lpr = handlerPtr->process_event( event );
            //} catch(std::exception & e) {
            //    _log << log4cpp::Priority::ERROR
            //         << "Handler #" << nHandler
            //         << " in pipeline " << ((void*) this)
            //         << " emitted error while processing event: " << e.what();
            //    throw;
            //}
            handlerPtr->reset_local_memory();  // TODO RAI

            processIngStoppedByHandler |= (lpr & AbstractHandler::kStopProcessing);
            if( lpr & AbstractHandler::kDiscriminateEvent ) {
                _procInfoPtr->notify_event_discriminated( handlerPtr );
                eventDiscriminated = true;
                break;
            } else {
                _procInfoPtr->notify_handler_done( handlerPtr );
            }
        }
    } else {
        for( auto & handlerPtr : *this ) {
            ++nHandler;
            AbstractHandler::ProcRes lpr;
            handlerPtr->set_local_memory(lmem);
            //try {
                lpr = handlerPtr->process_event( event );
            //} catch(std::exception & e) {
            //    _log << log4cpp::Priority::ERROR
            //         << "Handler #" << nHandler
            //         << " in pipeline " << ((void*) this)
            //         << " emitted error while processing event: " << e.what();
            //    throw;
            //}
            handlerPtr->reset_local_memory();  // TODO RAI

            processIngStoppedByHandler |= (lpr & AbstractHandler::kStopProcessing);
            if( lpr & AbstractHandler::kDiscriminateEvent ) {
                eventDiscriminated = true;
                break;
            }
        }
    }
    return {processIngStoppedByHandler, eventDiscriminated};
}


RunCfg::RunCfg( const std::string & name
              , const std::string & paths
              ) : _paths(paths)
                , _filename(_find_rcfg_file(name))
                {}

RunCfg::RunCfg( YAML::Node & root
              , const std::string & paths
              ) : _paths(paths)
                , _root(root)
                {}

std::string
RunCfg::_find_rcfg_file(const std::string & fname) {
    util::RuntimeDirs rd(_paths.c_str());
    auto files = rd.locate_files(fname);
    if( files.size() > 1 ) {
        std::ostringstream oss;
        bool isFirst = true;
        for( const auto & occ : files ) {
            if(isFirst) isFirst = false; else oss << ", ";
            oss << "\"" << occ << "\"";
        }
        log4cpp::Category::getInstance("pipeline.config")
            .warn( "Got %zu matches for runtime config matching pattern \"%s\": %s"
                   " Using first occurence (\"%s\")"
                   , files.size()
                   , fname.c_str()
                   , oss.str().c_str()
                   , files[0].c_str()
                   );
    } else if( files.empty() ) {
        log4cpp::Category::getInstance("pipeline.config")
            .debug(rd.dir_lookup_log().c_str());
        NA64DP_RUNTIME_ERROR( "No runtime config named \"%s\" found. Lookup"
                " locations: \"%s\".", fname.c_str(), _paths.c_str() );
    }
    return files[0];
}

YAML::Node
RunCfg::get() {
    if(_filename.empty()) {
        log4cpp::Category::getInstance("pipeline.config")
            << log4cpp::Priority::INFO
            << "Using cached pipeline config node.";
        return _root;
    }
    log4cpp::Category::getInstance("pipeline.config")
        << log4cpp::Priority::INFO
        << "Using pipeline configuration from \"" << _filename << "\".";

    // TODO: ... one can place some cfg-file preprocessing here

    try {
        _root = YAML::LoadFile(_filename);
    } catch( std::exception & e ) {
        log4cpp::Category::getInstance("pipeline.config")
            .error( "An error occured while accessing, reading, or parsing"
                    " file \"%s\": %s"
                   , _filename.c_str()
                   , e.what()
                   );
        throw e;
    }
    return _root;
}

}  // namespace na64dp

