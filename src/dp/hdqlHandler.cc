#include "na64dp/hdqlHandler.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64util/str-fmt.hh"

#include <hdql/query.h>
#include <hdql/types.h>
#include <hdql/attr-def.h>
#include <hdql/query-key.h>
#include <hdql/context.h>
#include <hdql/errors.h>

#include "na64dp/abstractHandler.hh"

namespace na64dp {

hdql_Compound * AbstractHDQLBasedHandler::eventCompound = nullptr;

AbstractHDQLBasedHandler::AbstractHDQLBasedHandler(
                      const std::string & expression
                    , hdql_Context_t rootContext
                    , bool keysNeeded
                    , log4cpp::Category & logCat )
        : AbstractHandler(logCat)
        , _ownContext(hdql_context_create_descendant(rootContext))
        , _query(nullptr)
        , _keys(nullptr)
        {
    if(!eventCompound) {
        NA64DP_RUNTIME_ERROR("AbstractHDQLBasedHandler::eventCompound pointer is not"
                " set. Please, init the root compound pointer in C/C++ code");
    }
    int rc;
    {  // compile the query
        char errBuf[256];
        int errDetails[5];
        _query = hdql_compile_query( expression.c_str()
                                   , eventCompound
                                   , _ownContext
                                   , errBuf, sizeof(errBuf)
                                   , errDetails
                                   );
        assert(NULL != _query);
        rc = errDetails[0];
        if(rc != HDQL_ERR_CODE_OK) {
            NA64DP_RUNTIME_ERROR( "HDQL expression error (%d,%d-%d,%d): %s\n"
                   , errDetails[1], errDetails[2], errDetails[3], errDetails[4]
                   , errBuf);
        }
    }
    // reserve keys and flat keys view, if need
    if(keysNeeded) {
        // reserve ordinary keys list
        rc = hdql_query_keys_reserve(_query, &_keys, _ownContext);
        // reserve flat keys view
        size_t flatKeyViewLength = hdql_keys_flat_view_size(_query, _keys, _ownContext);
        hdql_KeyView * kv = flatKeyViewLength
                      ? (hdql_KeyView *) malloc(sizeof(hdql_KeyView)*flatKeyViewLength)
                      : NULL;
        hdql_keys_flat_view_update(_query, _keys, kv, _ownContext);
    }
}

AbstractHandler::ProcRes
AbstractHDQLBasedHandler::process_event(event::Event & event) {
    int rc = hdql_query_reset( _query
                    , reinterpret_cast<hdql_Datum_t>(&event)
                    , _ownContext
                    );
    if(HDQL_ERR_CODE_OK != rc) {
        // TODO: get HDQL's error stack and add to a dedicated exception type?
        NA64DP_RUNTIME_ERROR("HDQL query reset error: %s", hdql_err_str(rc) );
    }
    hdql_Datum_t result;
    while(NULL != (result = hdql_query_get(_query, _keys, _ownContext))) {

    }
    // ...
}

AbstractHDQLBasedHandler::~AbstractHDQLBasedHandler() {
    if(_keys) {
        hdql_query_keys_destroy(_keys, _ownContext);
    }
    if(_query && _ownContext) {
        hdql_query_destroy(_query, _ownContext);
    }
    if(_ownContext) {
        hdql_context_destroy(_ownContext);
    }
}

}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
