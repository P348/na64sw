#include "na64dp/processingInfo.hh"
#include "na64util/str-fmt.hh"
#include "na64util/log4cpp-extras.hh"
#include "na64dp/abstractHitHandler.hh"

#include <cassert>
#include <ctime>
#include <iomanip>
#include <sstream>

namespace na64dp {

//
// Basic registry

void
iEvStatProcInfo::register_handler( const AbstractHandler * hPtr
                                 , const std::string & hName ) {
    auto ir = emplace( hPtr, aux::HandlerStatisticsEntry( hName ) );
    assert(ir.second);
    ir.first->second.handlerPtr = hPtr;
    ir.first->second.hitHandlerPtr = dynamic_cast<const HitHandlerStats *>(hPtr);
    _ordered.push_back( &stats_for(hPtr) );
}

aux::HandlerStatisticsEntry &
iEvStatProcInfo::stats_for( const AbstractHandler * hPtr ) {
    auto it = find(hPtr);
    if( end() == it ) {
        NA64DP_RUNTIME_ERROR( "Can not find registered handler by given pointer." );
    }
    return it->second;
}

void
iEvStatProcInfo::notify_event_read() {
    if(!_started) { _started = clock(); }
    ++_nEvsProcessed;
    //return _nEvsToProcess > 0 ? _nEvsProcessed < _nEvsToProcess : true;
}

void
iEvStatProcInfo::notify_event_discriminated( const AbstractHandler * hPtr ) {
    ++stats_for(hPtr).nDiscriminated;
}

void
iEvStatProcInfo::notify_handler_starts( const AbstractHandler * hPtr ) {
    stats_for(hPtr)._started = clock();
}

void
iEvStatProcInfo::notify_handler_done( const AbstractHandler * hPtr ) {
    auto & s = stats_for(hPtr);
    s.elapsed += clock() - s._started;
}

size_t
iEvStatProcInfo::n_events_discriminated() const {
    size_t n = 0;
    for(auto const hsp : _ordered) {
        n += hsp->nDiscriminated;
    }
    return n;
}

void
iEvStatProcInfo::finalize() {
    assert(_ended == 0);
    _ended = clock();
}

clock_t
iEvStatProcInfo::elapsed() const {
    if(0 == _started) return 0.;
    if(_ended) return _ended - _started;
    return clock() - _started;
}

// Helper function producing output:
//  - 604800 abs secs is "168h 0m 0.000s"
//                        123456789|1234
//  - 3 abs secs is      "3.000s"
//  - 75 abs secs is     "1m 15.000s"
size_t
abs_sec_to_hr(clock_t nSecAbs, char * strBuf, size_t bufLen) {
    unsigned int nHours, nMins;
    double nSec_ = static_cast<double>(nSecAbs)/CLOCKS_PER_SEC;
    nHours = static_cast<unsigned int>(nSec_/3600);
    nSec_ -= 3600*nHours;
    nMins = static_cast<unsigned int>(nSec_/60);
    nSec_ -= 60*nMins;
    if(0 != nHours) {
        return snprintf(strBuf, bufLen, "%uh %um %02.2fs", nHours, nMins, nSec_);
    } else if(0 != nMins) {
        return snprintf(strBuf, bufLen, "%um %02.2fs", nMins, nSec_);
    } else {
        if(nSec_ >= 5e-2) {
            return snprintf(strBuf, bufLen, "%02.2fs", nSec_);
        } else if(nSec_ > 5e-4) {
            return snprintf(strBuf, bufLen, "%.2fms", nSec_*1e3);
        } else {
            return snprintf(strBuf, bufLen, "%.2fus", nSec_*1e6);
        }
    }
}

#if 0
Overall summary:
    Time elapsed ........... 1h 3m 45.3s (1.23 ev/s)
    Events considered ...... 123478
    Events discriminated ... 345 (23%)
Handlers summary:
  #. LABEL                        ELAPSED     DISCRIMINATED
  1. Plot something ................ 0.3s ...... 0, 100.2%
  2. Discriminate RC ....... 12h 5m 23.1s ... 2345, 36.3%
  3. Subpipe: consider foo ......... 0.7s ..... 22, 34.5%
  3.1 Discriminate one ............. 0.4s ... [11], [16.25%]
  3.2 Discriminate two ............. 0.4s ... [11], [18.25%]
  4. Plot another ............... 2m 1.5s ..... 21, 0.2% ; hits: 456712/0, 34.2%
#endif

// aux struct collecting strings per handler
struct StrItem {
    std::string label, elapsed, nDiscriminated, percentage, hitsStats;
};

void
iEvStatProcInfo::print_summary(std::ostream & os) {
    // TODO: sub-pipe is not supported so far...
    const size_t nItems = _ordered.size();
    size_t fieldWidth[] = {0, 0, 0, 0, 0};  // label, elpased, disc, perc, hitsStats
    std::vector<StrItem> strItems;
    strItems.reserve(nItems);

    os << "Handlers summary:" << std::endl;
    char strBf[64];
    size_t nEventsSurvived = _nEvsProcessed
         , nDiscriminatedOverall = 0;
    size_t nItem = 0;
    for(auto handlerStatsEntry : _ordered) {
        ++nItem;
        StrItem strItem;

        // handler number + label
        size_t labelLength = snprintf(strBf, sizeof(strBf)
                , "%3lu. %s", nItem + 1, handlerStatsEntry->name.c_str() );
        if(fieldWidth[0] < labelLength) fieldWidth[0] = labelLength;
        strItem.label = strBf;
        // elapsed time
        size_t strTimeLength = abs_sec_to_hr(handlerStatsEntry->elapsed, strBf, sizeof(strBf));
        if(fieldWidth[1] < strTimeLength) fieldWidth[1] = strTimeLength;
        strItem.elapsed = strBf;
        // number of discriminated events
        size_t nDiscStrLength = snprintf(strBf, sizeof(strBf)
                , "%lu", handlerStatsEntry->nDiscriminated);
        if(fieldWidth[2] < nDiscStrLength) fieldWidth[2] = nDiscStrLength;
        strItem.nDiscriminated = strBf;
        // discriminated percentage
        float perc = nEventsSurvived
                   ? 100*(static_cast<float>(handlerStatsEntry->nDiscriminated) / nEventsSurvived)
                   : std::numeric_limits<float>::quiet_NaN()
                   ;
        // decrease number of survived events for further usage
        if(nEventsSurvived)
            nEventsSurvived -= handlerStatsEntry->nDiscriminated;
        nDiscriminatedOverall += handlerStatsEntry->nDiscriminated;
        size_t nPercStrLength = snprintf(strBf, sizeof(strBf), "%4.1f%%", perc);
        if(fieldWidth[3] < nPercStrLength) fieldWidth[3] = nPercStrLength;
        strItem.percentage = strBf;
        // hits stats
        if(!handlerStatsEntry->hitHandlerPtr) {
            strItems.push_back(strItem);
            continue;
        }
        // hits stats
        size_t hitsStatsLen = snprintf(strBf, sizeof(strBf), "%zu/%zu"
                , handlerStatsEntry->hitHandlerPtr->n_hits_discriminated()
                , handlerStatsEntry->hitHandlerPtr->n_hits_considered()
                );

        if(0 != handlerStatsEntry->hitHandlerPtr->n_hits_discriminated()) {
            float hitsPerc = 100
                           * static_cast<float>(handlerStatsEntry->hitHandlerPtr->n_hits_discriminated())
                           / handlerStatsEntry->hitHandlerPtr->n_hits_considered()
                           ;
            hitsStatsLen += snprintf(strBf + hitsStatsLen, sizeof(strBf) - hitsStatsLen, ", %.1f%%", hitsPerc);
        }
        strItem.hitsStats = strBf;
        if(fieldWidth[4] < hitsStatsLen) fieldWidth[4] = hitsStatsLen;

        strItems.push_back(strItem);
    }

    os  << std::setw(fieldWidth[0] + 4) << std::left << "  #. LABEL"
        << std::setw(fieldWidth[1] + 1) << std::right << "ELAPSED"
        << std::setw(17) << std::right << "DISCRIMINATED"
        << std::endl;
    for(const auto & strItem : strItems) {
        os  << std::setw(fieldWidth[0]+4) << std::setfill('.') << std::left  << strItem.label + " "
            << std::setw(fieldWidth[1]+2) << std::setfill('.') << std::right << " " + strItem.elapsed + " "
            << std::setw(fieldWidth[2]+4) << std::setfill('.') << std::right << " " + strItem.nDiscriminated
            << /*std::setw(fieldWidth[3])   <<*/ std::setfill(' ') << std::left  << ", " + strItem.percentage;
        if(!strItem.hitsStats.empty())
            os /* << std::setw(fieldWidth[4])*/ << " ; " + strItem.hitsStats;
        os << std::endl;
    }
    abs_sec_to_hr(elapsed(), strBf, sizeof(strBf));
    std::string elapsedTimeStr(strBf);
    float evsPerSec = 0.;
    if(_nEvsProcessed) {
        evsPerSec = static_cast<float>(_nEvsProcessed)*CLOCKS_PER_SEC / elapsed();
    }
    if(evsPerSec < 5e-2) {
        snprintf(strBf, sizeof(strBf), "%.2e", evsPerSec);
    } else {
        snprintf(strBf, sizeof(strBf), "%.2f", evsPerSec);
    }
    os << "Overall event processing summary:" << std::endl
       << "  Time elapsed overall ... " << elapsedTimeStr << " (" << strBf << " ev/s)" << std::endl
       << "  Events considered ...... " << _nEvsProcessed << std::endl
       << "  Events discriminated ... " << nDiscriminatedOverall << std::endl;
}

//
// Periodic dispatcher

void
EvProcInfoDispatcher::_dispatch_f() {
    while( _keep ) {
        std::this_thread::sleep_for(std::chrono::milliseconds(_rrMSec));
        _m.lock();
        _update_event_processing_info();
        _m.unlock();
    }
}

EvProcInfoDispatcher::EvProcInfoDispatcher( size_t nMaxEvs
                                          , unsigned int refreshTime 
                                          ) : iEvStatProcInfo( nMaxEvs )
                                            , _rrMSec(refreshTime)
                                            , _dispatcherThread(nullptr)
                                            , _keep(false)
                                            , _processingStartTime(clock()) {
}
EvProcInfoDispatcher::~EvProcInfoDispatcher() {
    if( _dispatcherThread ) {
        _m.lock();
        _keep = false;
        _m.unlock();
        _dispatcherThread->join();
        delete _dispatcherThread;
        _dispatcherThread = nullptr;
    }
}

void
EvProcInfoDispatcher::notify_event_read() {
    std::lock_guard<std::mutex> lock(_m);
    iEvStatProcInfo::notify_event_read();
}

void
EvProcInfoDispatcher::notify_event_discriminated( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvStatProcInfo::notify_event_discriminated(hPtr);
}

void
EvProcInfoDispatcher::notify_handler_starts( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvStatProcInfo::notify_handler_starts(hPtr);
}

void
EvProcInfoDispatcher::notify_handler_done( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvStatProcInfo::notify_handler_done(hPtr);
}

void
EvProcInfoDispatcher::start_event_processing() {
    assert(!_dispatcherThread);
    assert(!_keep);
    _keep = true;
    _dispatcherThread = new std::thread( &EvProcInfoDispatcher::_dispatch_f
                                       , this );
}

void
EvProcInfoDispatcher::finalize() {
    {
        _m.lock();
        _keep = false;
        _m.unlock();
    }
    if(_dispatcherThread) {
        _dispatcherThread->join();
        delete _dispatcherThread;
        _dispatcherThread = nullptr;
    }
}

//
// TTY print-out

void
TTYStatusProcessingInfo::_update_event_processing_info() {
    auto layoutPtr = na64dp::util::StdoutStreamAppender::self();
    if(layoutPtr) {
        float elapsed = ((float) clock() - started())/CLOCKS_PER_SEC
            , rate = ((float) n_events_processed())/(elapsed ? elapsed : 1)
            ;

        std::ostringstream oss;
        oss << "  ";
        if( n_events_to_process() ) {
            oss << n_events_processed() << " / "
                << n_events_to_process()
                ;
            if(rate && std::isfinite(rate)) {
                char buf[64];
                float reamainedSec
                    = (n_events_to_process() - n_events_processed())/rate;
                util::seconds_to_hmsm(reamainedSec, buf, sizeof(buf));
                oss << ", ~" << buf;
            }
        } else {
            oss << n_events_processed();
        }
        oss << ", ";

        layoutPtr->update_status_text(oss.str().c_str());
    }
}

#if 0
void
PrintEventProcessingInfo::_update_event_processing_info() {
    float elapsed = ((float) clock() - started())/CLOCKS_PER_SEC
        , rate = ((float) n_events_processed())/(elapsed ? elapsed : 1)
        ;

    static char bf[1024];
    // Note: following escape sequences are used (ANSI terminal):
    // \033[s -- save current cursor position
    // \033[u -- restore cursor position previously saved
    // \033[K -- clear line, from current position to end
    size_t nPrinted = snprintf( bf, sizeof(bf)
            , "\033[K\033[1m%zu\033[0m events processed in"
              " \033[1m%.2f\033[0m sec (\033[1m%.1f\033[0m event/sec)\n"
            , n_events_processed(), elapsed, rate );
    size_t nHandler = 1;
    for( auto handlerPair : *this ) {
        const AbstractHandler * handlerPtr = handlerPair.first;
        const aux::HandlerStatisticsEntry & hse = handlerPair.second;
        float perc = 100.*hse.nDiscriminated/((float) n_events_processed());
        nPrinted += snprintf( bf + nPrinted, sizeof(bf) - nPrinted
                            , "\033[K  %2zu : %p %5zu (%4.1f%%) : %s\n"
                            , nHandler, handlerPtr, hse.nDiscriminated
                            , perc, hse.name.c_str() );
        if( nPrinted >= sizeof(bf) ) {
            break;
        }
        ++nHandler;
    }
    fprintf(stdout, "\033[%zuA", nHandler);
    fputs(bf, stdout);
    fflush(stdout);
}
#endif

#if 0  // XXX, kept as a draft for future
void
EvProcInfoDispatcher::excerpt( std::vector<char> & buffer ) {
    //std::lock_guard<std::mutex> lock(_m);
    // TODO: this objects could be probably made reentrant
    ::capnp::MallocMessageBuilder msg;
    EventProcessingState::Builder state = msg.initRoot<EventProcessingState>();
    ::capnp::List<HandlerStats>::Builder hStates = state.initHandlers( size() );

    // Fill handlers infor
    int i = 0;
    std::map<unsigned short, const EventProcessingState *> sorted;
    for( const aux::HandlerStatisticsEntry * hse : ordered_stats() ) {
        HandlerStats::Builder hState = hStates[i];

        hState.setEventsDiscriminated( hse->nDiscriminated );
        hState.setElapsedTime( 1e3*float(hse->elapsed)/CLOCKS_PER_SEC );
        hState.setName( hse->name );
        ++i;
    }
    state.setElapsedTime( 1e3*float(clock() - started())/CLOCKS_PER_SEC );
    state.setEventsPassed( n_events_processed() );
    state.setNEventsExpected( n_events_to_process() );
    // Fill banks info
    auto banks = state.initBanks(); {
        banks.setNSADCHits( _banks.bankSADC.capacity() );
        banks.setNAPVHits( _banks.bankAPV.capacity() );
        banks.setNSADCFittingEntries( _banks.bankSADCFitting.capacity() );
        banks.setNAPVClusters( _banks.bankAPVClusters.capacity() );
        banks.setNTrackPoints( _banks.bankTrackPoints.capacity() );
        banks.setNTracks( _banks.bankTrackPoints.capacity() );
    }

    // serialize
    kj::Array<capnp::word> dataArr = capnp::messageToFlatArray(msg);
    kj::ArrayPtr<kj::byte> bytes = dataArr.asBytes();

    buffer.resize( bytes.size() );
    std::copy( bytes.begin(), bytes.end(), buffer.begin() );
}
#endif

}  // namespace na64

