#include "na64dp/pipeline.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64event/data/event.hh"
//#include "na64dp/abstractHitHandler.hh"
//#include "na64dp/abstractEventSource.hh"
//#include "na64detID/TBName.hh"
//#include "na64dp/pipeline.hh"

#include <gtest/gtest.h>

/**\file tests/PipelineBasic.cc
 *
 * Testing unit processing basic pipeline's discrimination logic as well as
 * construction of basic structures.
 * Creates two handlers within pipeline over a mocking source. The source
 * sets only the event identifier and yields fixed number of events. The
 * handlers discriminates each 2nd and 3rd event and records event ids passed
 * by.
 * Then the unit verifies that all the events supposed to be passed were passed
 * indeed.
 *
 * Other suite tests full stack of pipeline API with mock classes of event source, event
 * handler and calibration handle.
 * */

namespace na64dp {
namespace test {

// Mock handler used for testing purposes; discriminates events by their IDs
class MockHandler : public AbstractHandler {
public:
    std::vector<EventID> idsPassed;
private:
    int _evIDDiv;
public:
    MockHandler( int evIDDiv )
                : AbstractHandler(log4cpp::Category::getInstance("testing"))
                , _evIDDiv(evIDDiv) {}
    virtual ProcRes process_event(event::Event & event) override {
        if( event.id % _evIDDiv ) {
            idsPassed.push_back( event.id );
            return kOk;
        }
        return kDiscriminateEvent;
    }
};

// Mock events source
class MockSource : public AbstractEventSource {
private:
    size_t _evCounter
         , _evMaxCount;
public:
    MockSource( size_t max )
            : AbstractEventSource()
            , _evCounter(0)
            , _evMaxCount(max) {}
    virtual bool read(event::Event & e, event::LocalMemory &) override {
        ++_evCounter;
        e.id = EventID(_evCounter);
        return _evCounter < _evMaxCount;
    }
};

// Builds pipeline of two mock handlers over mock source, checks the basic
// discrimination logic
TEST(Pipeline, EventDiscriminationLogic) {
    MockHandler * h1 = new MockHandler(2)  // discriminate each 2nd event
              , * h2 = new MockHandler(3)  // discriminate each 3rd event
              ;
    Pipeline p;
    p.push_back(h1);
    p.push_back(h2);
    MockSource src(19);

    // TODO: since some important use cases of allocators lifecycle are not
    // clear yet, the block below will most probably change at some point.
    #if 0
    auto lmem = LocalMemory::instantiate();
    bool hasRead = false;
    do {
        auto evRef = lmem.new_event();
        if( hasRead = src.read(*evRef) ) {
            p.process(evRef);
        }
    } while( hasRead );
    #else
    {
        // Physical memory block holding the event data created
        char buffer[1024];
        // ^^^ TODO: this must be enlarged sometimes, when we are changing
        // event size... We have to foresee some adaptive mechanism here.

        // Memory management strategy
        // TODO: handle cases other than `PlainBlock`
        // (will vary depending on the NA64SW_EVMALLOC_STRATEGY)
        mem::PlainBlock block(buffer, sizeof(buffer));
        // Reentrant memory access API
        LocalMemory lmem(block);

        for(;;) {
            // Create event for current iteration
            auto evRef = lmem.create<event::Event>(lmem);

            util::reset(*evRef);
            if( src.read(*evRef, lmem) )
                p.process(*evRef, lmem);
            else
                break;
            lmem.reset();
            block.reset();
        }
    }
    #endif
    // h1 has contain increasing odd numbers: 1, 3, 5 ... 17
    int nC = -1;
    EXPECT_EQ( h1->idsPassed.size(), 9 );
    for( auto n : h1->idsPassed ) {
        EXPECT_TRUE( n%2 );
        EXPECT_EQ( nC += 2, n );
    }
    // h1 has contain 1, 5, 7, 11, 13, 17
    const int checkArr[] = { 1, 5, 7, 11, 13, 17 };
    const int * nCPtr = checkArr;
    EXPECT_EQ( h2->idsPassed.size(), 6 );
    for(auto n : h2->idsPassed) {
        EXPECT_TRUE( n%3 );
        EXPECT_EQ( *(nCPtr++), n );
    }
}

#if 0

/**\brief A mock detector naming
 * 
 * Represents a mock calibration data source: applies distinct naming schemata for
 * run #1 and run #2.
 **/
class MockCalibHandle : public iCalibHandle {
private:
    int _cRunNo;
    utils::TBNameMappings _nameMappings;
protected:
    // This mock implementation considers only run #1 and #2 and changes name
    // mappings between them.
    virtual bool _new_run_no( na64ee::runNo_t rNo ) override {
        bool doRecache = (rNo != _cRunNo);
        if(doRecache) {
            utils::TBNameMappings & m = _nameMappings;
            m.clear_mappings();
            if( 1 == rNo ) {
                m.add_chip( "SADCT", 0x1
                          , "A mock chip for SADC hits, v#1."
                          , "{kin}{statNum2}/{hist}" );
                m.add_chip( "APVT",  0x2
                          , "A mock chip for APV hits, v#1."
                          , "{kin}{statNum2}/{hist}" );

                m.define_kin( "NCAL", 0x1, "Non-existing SADC detector #1"
                            , "SADCT", "{kin}{statNum}" );
                m.define_kin( "NCBL", 0x2, "Non-existing SADC detector #2"
                            , "SADCT", "{kin}{statNum}" );
                m.define_kin( "TRCK", 0x1, "Non-existing APV detector #1"
                            ,  "APVT", "{kin}{statNum2}__" );
            } else if( 2 == rNo ) {
                m.add_chip( "SADCT", 0x3, "A mock chip for SADC hits, v#2.", "{kin}{statNum2}/{hist}" );
                m.add_chip( "APVT",  0x1, "A mock chip for APV hits, v#2.", "{kin}{statNum2}/{hist}" );

                m.define_kin( "NCAL", 0x1, "Non-existing SADC detector #1"
                            , "SADCT", "{kin}{statNum2}" );
                m.define_kin( "TRCK", 0x4, "Non-existing APV detector #1"
                            ,  "APVT", "{kin}{statNum2}__" );
                m.define_kin( "TRDK", 0x3, "Non-existing APV detector #2"
                            ,  "APVT", "{kin}{statNum2}__" );
            } else {
                throw std::runtime_error("Invalid run number provided.");
            }
        }
        _cRunNo = rNo;
        return doRecache;
    }
    virtual bool _has_calib_info( const std::string &, DetID_t ) const override { return false; }
    virtual const CalibBuffer & _get_calib_info( const std::string &, DetID_t ) override { throw std::runtime_error("forbidden call"); }
    virtual void _set_calib_info( const std::string &, DetID_t, const CalibBuffer & ) override {}
public:
    MockCalibHandle() {}
    virtual const utils::TBNameMappings & naming() override {
        return _nameMappings;
    }
};

class MockIndex : public iCalibRunIndex {
public:
    const int n;
    MockIndex(int n_) : n(n_) {}
    virtual void updates( EventID oldEventID
                        , EventID newEventID
                        , UpdatesList & ul ) {
        uint8_t ctrlOctet = newEventID.run_no();
        if( 1 == n ) {
            ctrlOctet &= 0xf;
        } else {
            ctrlOctet >>= 4;
        }
        if( ! (ctrlOctet & 0x1) ) return;
        oldEventID.event_no(n - 1);
        std::string loaderName = (0x2 & ctrlOctet) ? "loader1" : "loader2"
                  , calibTypeName = (0x4 & ctrlOctet) ? "foo" : "bar"
                  ;
        if( 0x8 & ctrlOctet ) {
            ul.enqueue_update_of_type<int>( calibTypeName
                                          , loaderName
                                          , oldEventID );
        } else {
            ul.enqueue_update_of_type<float>( calibTypeName
                                            , loaderName
                                            , oldEventID );
        }
    }
};

class MockLoader : public iCalibDataLoader {
public:
    const int n;
    uint8_t & ctrlByte;
    MockLoader( int n_, uint8_t & ctrlByte_ ) : n(n_), ctrlByte(ctrlByte_) {}
    /// Loads calibration data into given dispatcher instance
    virtual void load_data( EventID eventID
                          , Dispatcher::CIDataID cidID
                          , Dispatcher & ) override {
        std::string calibName = cidID.second;
        std::type_index calibTypeIndex = cidID.first;
        uint8_t ctrlSeq = 0x0;
        if( calibName == "foo" ) {
            ctrlSeq |= 0x4;
        } else {
            ASSERT_STREQ( calibName.c_str(), "bar" );
        }
        if( std::type_index(typeid(int)) == calibTypeIndex ) {
            ctrlSeq |= 0x8;
        } else {
            ASSERT_EQ(std::type_index(typeid(float)), calibTypeIndex);
        }
        if( 1 == n ) {
            ctrlSeq |= 0x2;
        }
        ctrlSeq |= 0x1;  // unconditionally set, indicating that index has emitted update
        ctrlByte |= (ctrlSeq << (eventID.event_no() ? 4 : 0) );
    }
    /// Shall return whether the specified calibration is available
    virtual bool has( EventID
                    , Dispatcher::CIDataID ) {
        return true;
    }
};

//template<typename HitT>
//struct MockSourceTraits;

/// Mock event source, producing distinct types of events dor two runs
class MockEventSource : public AbstractEventSource {
public:
    std::map<std::string, size_t> nHitsByName;
private:
    int _eventCounter;
    //MockCalibHandle & _ch;  //?

    template<typename HitT>
    void _fill_hits( event::Event & e, int count, const char * tbn ) {
        if( count < 1 ) return;  // no fill for false counts
        char * postfix = NULL;
        const utils::TBNameMappings & m = _ch.naming();
        DetID did = m.detector_id_by_ddd_name( tbn, postfix );
        _log.debug( "Name mappings for event %d yielded detector ID %#x for"
                " name \"%s\" (filling with %d hits).", e.id, did.id, tbn, count );
        //EXPECT_EQ('\0', *postfix);
        if( nHitsByName.end() != nHitsByName.find(tbn) ) {
            nHitsByName[tbn] += count;
        } else {
            nHitsByName[tbn]  = count;
        }
        for( int i = 0; i < count; ++i ) {
            DetID thisDid(did);     // we copy did (chip + kin + station number) to
            thisDid.payload(i+1);     // modify the payload
            try {
                HitT & newHit = *EvFieldTraits<HitT>::inserter(*this)( e, thisDid, _ch.naming() );
                // ...
            } catch( std::exception & e ) {
                _log.error( "Error occured while imposing new hit entry for"
                        " detector id %#x (chip = %#x \"%s\", kin = %#x,"
                        " stat.num.=%d, payload=%#x, name=\"%s\"/\"%s\")"
                        , thisDid.id
                        , thisDid.chip(), m.chip_name( thisDid.chip() )
                        , thisDid.kin()
                        , thisDid.number()
                        , thisDid.payload()
                        , tbn
                        , m.detector_ddd_name_by_id( thisDid ).c_str() );
                throw;
            }
            _log.debug( "Put hit in map; banks sizes: SADC - %zu, APV - %zu,"
                    " maps sizes: SADC - %zu, APV - %zu."
                    , _banks.bankSADC.size()
                    , _banks.bankAPV.size()
                    , e.sadcHits.size()
                    , e.apvHits.size() );
        }
    }
public:
    MockEventSource( calib::Manager & mgr ) : AbstractEventSource(banks)
                                            , _eventCounter(0)
                                            {}

    virtual bool read( event::Event & e ) override {
        na64ee::UEventID euID = { .numeric = na64ee::assemble_event_id( _eventCounter > 70 ? 1 : 2
                                , _eventCounter/10
                                , _eventCounter%10 ) };
        e.id = euID.chunklessLayout;
        // For series below the following summation formulae takes place:
        // * number of hits per event (negative must be zeroed):
        //      n := c % D - B
        // * number of events where hits of these type are present:
        //      E := floor(N/D)*(D - B - 1)
        //   May be understood as numer of passed events (D - B - 1) of on
        //   interval of length D multiplied by numer of intervals.
        // * overall number of hits "generated" for certain detector type:
        //      k = D - B - 1
        //      S := floor(N/D)*(n*(n-1)/2)
        // May be derived from previous takin into account that whithin each
        // interval, n-th triangular number of hits is generated -- a
        // binomial coefficient (n-2, 2) which is (n^2 - n)/2.
        if( _eventCounter < 50 ) {
            // run #1
            _ch.set_run_no(1);  // has to cause re-caching of all subscribers
            _fill_hits<event::SADCHit>( e, _eventCounter%3      , "NCAL1" );
            _fill_hits<event::SADCHit>( e, _eventCounter%5 - 2  , "NCAL2" );
            _fill_hits<event::SADCHit>( e, _eventCounter%3 - 1  , "NCBL1"  );
            _fill_hits<event::APVHit>(  e, _eventCounter%5 - 1  , "TRCK01" );
        } else {
            // run #2
            _ch.set_run_no(2);  // has to cause re-caching of all subscribers
            _fill_hits<event::SADCHit>( e, _eventCounter%5      , "NCAL01" );
            _fill_hits<event::APVHit>(  e, _eventCounter%3 - 1  , "TRCK01__" );
            _fill_hits<event::APVHit>(  e, _eventCounter%10     , "TRDK01__" );
            _fill_hits<event::APVHit>(  e, _eventCounter%10- 5  , "TRDK02__" );
        }
        // Expected counts:
        //  NCAL1 : 100 + 49 = 149
        //  NCAL2 : 30
        //  NCBL1 : 16
        //  TRCK01 : 60 + 17 = 77
        //  TRDK01 : 225
        //  TRDK02 : 50
        return (++_eventCounter) <= 100;
    }
};

struct MockHandlerStats {
    size_t nHits
         , nHitsInEvent;
    std::map<DetID_t, size_t> nHitsByDet;
    std::set<na64ee::EventNumericID> eventIDs;

    MockHandlerStats() : nHits(0) {}
};

// A mock hits processing handler template
// Gathers statistics of hits passed by.
template<typename HitT>
class MockHandler : public AbstractHitHandler<HitT>
                  , public MockHandlerStats {
protected:
    bool _removeOdd;
public:
    MockHandler( iCalibHandle * ch
               , const std::string & only=""
               , bool removeOdd=false )
                            : AbstractHitHandler<HitT>(ch, only)
                            , _removeOdd(removeOdd) {}

    virtual AbstractHandler::ProcRes process_event( Event * e ) override {
        nHitsInEvent = 0;
        na64ee::UEventID ueID = { .chunklessLayout = e->id };
        auto it = eventIDs.find(ueID.numeric);
        EXPECT_EQ( it, eventIDs.end() );  // assure events are unique
        eventIDs.insert(ueID.numeric);
        return AbstractHitHandler<HitT>::process_event(e);
    }

    virtual bool process_hit( na64ee::AbsEventID eventID
                            , DetID_t detID
                            , HitT & hit ) override {
        // increase general hits counters
        ++nHits;
        ++nHitsInEvent;
        // increase by-detector hit counter
        auto ir = nHitsByDet.emplace(detID, 1);
        if( ! ir.second ) {  // insertion failed because of existing element
            ++ (ir.first->second);
        }
        if( _removeOdd && nHits%2 ) {
            this->_set_hit_erase_flag();
        }
        return true;
    }
};

TEST(Pipeline, HitProcessorRecachingLogic) {
    // This string keys will be used to identify the mock SADC detectors
    const char sadcDetNames[][8] = { "NCAL1", "NCAL01"
                                   , "NCAL2"
                                   , "NCBL01"
                                   };
    // This string keys will be used to identify mock APV detectors
    const char apvDetNames[][8] = { "TRCK01"
                                  , "TRDK01"
                                  , "TRDK02"
                                  };
    // This array will be filled with ptrs to handlers for checks
    MockHandlerStats * stats[7], ** cStat = stats;
    // Hit banks is an important object for data source and handlers. It
    // provides pre-allocated memory for fast event modifications -- imposing
    // hits in event structure by source, and derived data by handlers.
    HitBanks banks;
    // Calibration handle is another important object providing calibration
    // data, detector name mappings and so on.
    MockCalibHandle ch;
    // Create the pipeline
    Pipeline p;

    MockHandler<SADCHit> * h1, * h3;
    MockHandler<APVHit> * h2, * h4;
    // Insert some handlers in pipeline
    {   // - This one will accept all the SADC hits
        auto hPtr = h1 = new MockHandler<SADCHit>( &ch );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    {   // - This one will accept all the APV hits
        auto hPtr = h2 = new MockHandler<APVHit>( &ch );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    {   // - This one shall trait only NCAL1 (NCAL01 for run #2) detector
        auto hPtr = h3 = new MockHandler<SADCHit>( &ch
                                                 , "NCAL == kin && 1 == number"
                                                 , true );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    {   // - This one shall trait only TRCK01 detector
        auto hPtr = h4 = new MockHandler<APVHit>( &ch
                                                , "TRCK == kin && 1 == number"
                                                , true );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    {   // - This one will accept all the SADC hits (after removal)
        auto hPtr = h1 = new MockHandler<SADCHit>( &ch );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    #if 0
    {   // - This one will accept all the APV hits (after removal)
        auto hPtr = h2 = new MockHandler<APVHit>( &ch );
        p.push_back( hPtr );
        *(cStat++) = hPtr;
    }
    #endif
    *(cStat++) = nullptr;
    // Create the source
    MockEventSource src( banks, ch );
    // Create reentrant event instance (owned by thread)
    Event e(banks);
    EXPECT_EQ( &banks.bankSADC, &e.sadcHits.pool() );
    EXPECT_EQ( &banks.bankAPV,  &e.apvHits.pool() );
    // Process events from source with pipeline
    while(src.read(e)) {
        EXPECT_EQ( &banks.bankSADC, &e.sadcHits.pool() );
        EXPECT_EQ( &banks.bankAPV,  &e.apvHits.pool() );
        p.process(e);
        e.clear();
        banks.clear();
    }
    // Check the results
    // - at least one event is present on handler #1
    ASSERT_FALSE( stats[0]->eventIDs.empty() );
    // - 100 events were passed through all the handlers, and check total
    // number of hits accepted by each of the handlers
    const size_t expectedHitCounts[] = {
            100 + 49 + 30 + 16,  /* SADC overall */
            60 + 17 + 225 + 50,  /* APV overall */
            100 + 49,            /* NCAL1/NCAL01 */
            60 + 17,             /* TRCK01 */
            (100+49)/2+30+16,    /* SADC overall, after discriminating odd NCAL1/NCAL01 */
            (60+17)/2+225+50,    /* APV overall, after discriminating odd TRCK01 */
        };
    const size_t * expectedCountPtr = expectedHitCounts;
    for( MockHandlerStats ** cStat = stats
       ; *cStat
       ; ++cStat, ++expectedCountPtr ) {
        EXPECT_EQ( 100, (*cStat)->eventIDs.size() );
        if( *expectedCountPtr ) {
            EXPECT_EQ( *expectedCountPtr, (*cStat)->nHits ) 
                << " at handler #"
                << cStat - stats;
        }
    }
}

#endif

}  // namespace na64dp::test
}  // namespace na64dp
