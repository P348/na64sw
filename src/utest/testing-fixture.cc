#include "na64utest/testing-fixture.hh"

namespace na64dp {
namespace test {

BasicEventDataOperations::BasicEventDataOperations() : _bufferSize(0x0)
                                                     , _bufferPtr(nullptr)
                                                     , _plainBlockPtr(nullptr)
                                                     , _lmemPtr(nullptr)
                                                     {}
void
BasicEventDataOperations::TearDown() {
    if(_lmemPtr) {
        delete _lmemPtr;
        _lmemPtr = nullptr;
    }
    if(_plainBlockPtr) {
        delete _plainBlockPtr;
        _lmemPtr = nullptr;
    }
    if(_bufferPtr) {
        free(_bufferPtr);
        _bufferPtr = nullptr;
    }
}

void
BasicEventDataOperations::_reallocate_event_buffer(size_t newSize) {
    if(_bufferPtr) {
        free(_bufferPtr);
        _bufferPtr = nullptr;
    }
    _bufferPtr = malloc(newSize);
    assert(_bufferPtr);
    _bufferSize = newSize;

    if(_plainBlockPtr) {
        _plainBlockPtr = nullptr;
        delete _plainBlockPtr;
    }
    _plainBlockPtr = new na64dp::mem::PlainBlock(_bufferPtr, _bufferSize);

    if(_lmemPtr) {
        _lmemPtr = nullptr;
        delete _lmemPtr;
    }
    _lmemPtr = new LocalMemory(*_plainBlockPtr);
}

LocalMemory &
BasicEventDataOperations::lmem() {
    assert(_lmemPtr);
    return *_lmemPtr;
}

}  // namespace ::na64dp::test
}  // namespace na64dp

