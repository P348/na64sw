#include <regex>
#include <iostream>

namespace na64dp {
namespace nameutils {

// This regex fits usual TBname + "extended" TBnames used by NA64sw. Usual ones
// are typical notions of detector stations, like MM03X or ECAL0. Extended
// has payloads after `:` or `-` sign. This regex, however, does not allow to
// mix this -- expression like MM03X:14 will not pass, but MM03:X14 or MM03X
// will do.
const std::regex gNameRx("^([A-Z]+)(\\d{1,2})?[:-]?([^\\d\\W]?[\\w-]*)?$");
const int gNameGroupNo = 1
        , gStatNumGroupNo = 2
        , gPayloadGroupNo = 3
        ;

bool
tokenize_name_str( const std::string & str
                 , std::string & detName
                 , std::string & number
                 , std::string & payload ) {
    std::smatch m;
    if( std::regex_match(str, m, gNameRx) ) {
        detName = m[gNameGroupNo].str();
        number = m[gStatNumGroupNo].str();
        payload = m[gPayloadGroupNo].str();
        return true;
    }
    return false;
}


}
}

