#include "na64detID/detectorID.hh"
#include "na64util/selector.hh"
#include "na64detID/wireID.hh"
#include "na64detID/cellID.hh"
#include "na64detID/TBName.hh"

namespace na64dp {

DetID::DetID(PlaneKey dpk) : DetID( dpk.id ) {}

bool
DetID::matches(const DetID & did) const {
    // TODO: re-implement following stuff on bitmasks for speed
    if( is_chip_set() && did.is_chip_set()
     && chip() != did.chip() ) {
        return false;
    }
    if( is_kin_set() && did.is_kin_set()
     && kin() != did.kin() ) {
        return false;
    }
    if( is_number_set() && did.is_number_set()
     && number() != did.number() ) {
        return false;
    }
    if( is_payload_set() && did.is_payload_set()
     && number() != did.number() ) {
        return false;
    }
    return true;
}


namespace util {
namespace detector_id_getters {

// Extern resolver, relying on dynamic DetectorNaming instance provided by
// void-ptr
static int
_dynamic_name_resolve( const char * name
                     , const void * nameMappingsObject
                     , dsul::CodeVal_t * value ) {
    assert(nameMappingsObject);
    assert(name);
    const nameutils::DetectorNaming & nm
        = *reinterpret_cast<const nameutils::DetectorNaming *>(nameMappingsObject);
    
    if( name[0] != '\0' && (name[1] == '\0' || name[2] == '\0') ) {  // projection for APV detectors
        WireID::Projection pj = WireID::proj_code( name[0] );
        if( name[1] != '\0' && pj != WireID::kUnknown ) {
            if( name[1] == '2' ) {
                *value = WireID::adjoint_proj_code(pj);
                return 1;
            } else if( name[1] != '1' ) {
                pj = WireID::kUnknown;
            }
        }
        if( WireID::kUnknown != pj ) {
            *value = pj;
            return 1;
        }
    }
    {   // chip
        if( nm.has_chip(name) ) {
            *value = nm.chip_id(name);
            return 1;
        }
    }
    {   // kin
        if( nm.has_kin( name ) ) {
            auto kinID = nm.kin_id(name);
            DetID did( kinID.first  // chip
                     , kinID.second  // kin
                     , 0x0  // station number
                     , 0x0  // payload
                     );
            *value = did;
            return 1;
        }
    }

    return 0;
}

// Static getter -- chip identifier
static dsul::CodeVal_t
_get_chip( dsul::Principal_t pv ) {
    return DetID(reinterpret_cast<uintptr_t>(pv)).chip();
}

// Static getter -- detector type (kin)
static dsul::CodeVal_t
_get_kin( dsul::Principal_t pv ) {
    // To make difference between detector kins with same numerical ID that
    // corresponds to different chips (i.e. to accomplish user's expectation
    // for case of expression "kin == MM" with, say, id(MM) = 1 and
    // id(ECAL) = 1) we have to compose the kin's value accounting chip id
    DetID did(reinterpret_cast<uintptr_t>(pv));
    return DetID(did.chip(), did.kin(), 0, 0);
}

// Static getter -- station number
static dsul::CodeVal_t
_get_station_number( dsul::Principal_t pv ) {
    DetID did(reinterpret_cast<uintptr_t>(pv));
    if( ! did.is_number_set() ) return 0;
    return did.number();
}

// Dynamic getter -- projection code for APV detectors
static dsul::CodeVal_t
_apv_get_projection( dsul::Principal_t pv ) {
    return WireID(DetID(reinterpret_cast<uintptr_t>(pv)).payload()).proj();
}

// Dynamic getter -- wire number for APV detectors
static dsul::CodeVal_t
_apv_get_wire_no( dsul::Principal_t pv ) {
    return WireID(DetID(reinterpret_cast<uintptr_t>(pv)).payload()).wire_no();
}

// Dynamic getter -- x-index for SADC segmentation detectors
static dsul::CodeVal_t
_sadc_get_x_idx( dsul::Principal_t pv ) {
    return CellID(DetID(reinterpret_cast<uintptr_t>(pv)).payload()).get_x();
}

// Dynamic getter -- x-index for SADC segmentation detectors
static dsul::CodeVal_t
_sadc_get_y_idx( dsul::Principal_t pv ) {
    return CellID(DetID(reinterpret_cast<uintptr_t>(pv)).payload()).get_y();
}

static dsul::CodeVal_t
_sadc_get_z_idx( dsul::Principal_t pv ) {
    return CellID(DetID(reinterpret_cast<uintptr_t>(pv)).payload()).get_z();
}

static const na64dpsu_SymDefinition _detIDGetters[] = {
    { "+", { .resolve_name = _dynamic_name_resolve }, "common external getter" },
    { "$chip", { _get_chip }, "detector chip identifier (static)" },
    { "$kin", { _get_kin }, "detector kin identifier (static)" },
    { "$number", { _get_station_number }, "number of detector station" },
    { "~projection", { _apv_get_projection }, "detector plane projection code (dynamic, for APV detectors only)" },
    { "~wireNo", { _apv_get_wire_no }, "wire number (dynamic, for APV detectors only)" },
    { "~xIdx", { _sadc_get_x_idx }, "X-index of cell (dynamic, for SADC detectors only)" },
    { "~yIdx", { _sadc_get_y_idx }, "Y-index of cell (dynamic, for SADC detectors only)" },
    { "~zIdx", { _sadc_get_z_idx }, "Z-index of cell (dynamic, for SADC detectors only)" },
    { NULL, { NULL }, NULL }
};

}  // namespace detector_id_getters

const na64dpsu_SymDefinition * gDetIDGetters = detector_id_getters::_detIDGetters;

}  // namespace util

}

