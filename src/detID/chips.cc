#include "na64detID/chips.hh"
#include "na64detID/TBNameErrors.hh"
#include "na64util/str-fmt.hh"

#include <log4cpp/Category.hh>

namespace na64dp {
namespace nameutils {

DetChip_t
ChipFeaturesTable::chip_id( const std::string & chipName ) const {
    assert(!_chipIDs.empty());  // LCOV_EXCL_LINE
    auto it = _chipIDs.find(chipName);
    if( _chipIDs.end() == it ) {
        throw errors::NoEntryForKey<std::string>(
                util::format( "Chip \"%s\" is not defined."
                            , chipName.c_str() ).c_str(), chipName);
    }
    return it->second;
}

const ChipFeaturesTable::Features &
ChipFeaturesTable::chip_features(DetChip_t chipID) const {
    auto it = _chipFeatures.find(chipID);
    if( _chipFeatures.end() == it ) {
        // TODO: exception UnknownChipID
        NA64DP_RUNTIME_ERROR( "Chip ID %#x is not defined.", chipID );
    }
    return it->second;
}

const ChipFeaturesTable::Features &
ChipFeaturesTable::chip_features(const std::string & chipName) const {
    DetChip_t chipID = chip_id(chipName);
    auto it = _chipFeatures.find(chipID);
    assert( _chipFeatures.end() != it ); // maps are not synchronized
    return it->second;
}

ChipFeaturesTable::Features &
ChipFeaturesTable::chip_add( const std::string & chipName
                           , DetChip_t chipID
                           , const std::string & chipDescription
                           , const std::string & histsTPath
                           ) {
    auto & L = log4cpp::Category::getInstance("detID");
    if( chipID > aux::gChipIDMax || 0 == chipID ) {
        L.error( "Provided detector ID %#x (> %#x) or =0 is out of range."
               , chipID
               , aux::gChipIDMax
               );
        throw errors::IDIsOutOfRange( "ChipID is out of range."
                                    , chipID, aux::gChipIDMax );
    }
    if( chipName.empty() ) {
        NA64DP_RUNTIME_ERROR( "Empty chip name provided for id %d."
                            , (int) chipID );
    }

    auto irName = _chipIDs.emplace(chipName, chipID);

    if( !irName.second ) {
        NA64DP_RUNTIME_ERROR( "Chip name \"%s\" is already defined"
                " (existing ID=%d, this ID=%d)."
                , chipName.c_str()
                , (int) irName.first->second
                , (int) chipID );
    }

    auto ir = _chipFeatures.emplace( chipID, Features(chipName) );

    if( ! ir.second ) {
        L.error( "Duplicating chip ID %#x.", chipID );
        _chipIDs.erase( irName.first );
        throw errors::IDIsNotUniq( "Non-unique chip ID.", chipID );
    }
    L.debug( "Registered new chip \"%s\" with ID %#x."
           , chipName.c_str(), chipID );
    Features & ft = ir.first->second;
    ft.description = chipDescription;
    ft.pathFormat = histsTPath;
    return ft;
}

void
ChipFeaturesTable::clear() {
    _chipFeatures.clear();
    _chipIDs.clear();
}

}
}

