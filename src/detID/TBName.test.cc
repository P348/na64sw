#include "na64detID/TBName.hh"
#include "na64detID/TBNameErrors.hh"

#include <gtest/gtest.h>

/**\file tests/TBName.cc
 * 
 * Tests methods of nameutils::DetectorNaming class providing mapping of NA58/NA64
 * so-called TBNames into numerical ID format of na64dp.
 * */

namespace na64dp {

TEST(NameMappings, NameRendering) {
    std::map<std::string, std::string> ctx;
    ctx["foo"] = "one";

    EXPECT_STREQ( "one", util::str_subst( "{foo}", ctx ).c_str() );
    EXPECT_STREQ( "one", util::str_subst( "{foo}", ctx ).c_str() );

    ctx["bar"] = "two";
    
    //ASSERT_THROW( util::str_subst( "{foo", ctx ), std::runtime_error );
    //ASSERT_THROW( util::str_subst( "fo}{o", ctx ), std::runtime_error );

    EXPECT_STREQ( "one", util::str_subst( "{foo}", ctx ).c_str() );
    EXPECT_STREQ( "one", util::str_subst( "{foo}", ctx ).c_str() );
    EXPECT_STREQ( "onetwo", util::str_subst( "{foo}two", ctx ).c_str() );
    EXPECT_STREQ( "twoone", util::str_subst( "two{foo}", ctx ).c_str() );
    EXPECT_STREQ( "onetwo", util::str_subst( "{foo}{bar}", ctx ).c_str() );
}

class NameMappingsTest : public ::testing::Test {
private:
    static void type_a_append_completion_context( DetID did
                                                , std::map<std::string, std::string> & m ) {
        if( ! did.is_payload_set() ) return;
        if( 1 == did.payload() ) {
            m["pl"] = "undefined";
            return;
        }
        if( 2 == did.payload() ) {
            m["pl"] = "foo";
            return;
        }
        if( 3 == did.payload() ) {
            m["pl"] = "bar";
            return;
        }
    }
    static void type_a_to_string( DetIDPayload_t pt
                                , char * s
                                , size_t available ) {
        if( pt == 0x1 ) {
            assert(available);
            *s = '\0';
            return;
        }
        if( pt == 0x2 ) {
            strncpy( s, "foo", available );
            return;
        }
        if( pt == 0x3 ) {
            strncpy( s, "bar", available );
            return;
        }
        assert(false);
    }
    static DetIDPayload_t type_a_from_string( const char * s ) {
        if(!s || '\0' == s[0]) return 1;
        std::string chck(s);
        if( chck == "foo" ) return 2;
        if( chck == "bar" ) return 3;
        return 0x0;
    }
public:
    nameutils::DetectorNaming m;
    const DetChip_t tstADID, tstBDID;
    const DetKin_t tstKinA1ID, tstKinA2ID, tstKinA3ID
                 , tstKinB1ID, tstKinB2ID;

    NameMappingsTest() : tstADID( aux::gChipIDMax-1 )
                       , tstBDID( aux::gChipIDMax )
                       , tstKinA1ID(0x1)
                       , tstKinA2ID(0x2)
                       , tstKinA3ID(0x3)
                       , tstKinB1ID(0x1)
                       , tstKinB2ID(0x12)
                       {
        // Add two chips
        {
            auto & ct
                    = m.chip_add( "TESTA", tstADID); 
            ct.description = "Testing chip #1";
            ct.pathFormat = "{kin}{statNum2}/{hist}";
            ct.append_completion_context = type_a_append_completion_context;
            ct.to_string = type_a_to_string;
            ct.from_string = type_a_from_string;
        }

        {
            auto & ct 
                    = m.chip_add( "TESTB", tstBDID );
            ct.description = "Testing chip #2";
            ct.pathFormat = "{kin}{statNum2}/{hist}";
            //ct.append_completion_context = t2_append_completion_context;
            //ct.to_string = t2_to_string;
            //ct.from_string = t2_from_string;
        }

        // Define kin #1 for first chip
        {
            auto & fts =
                m.define_kin( "TSTF", tstKinA1ID
                            , "TESTA"
                            , "{kin}{statNum2}{pl}"
                            , "Testing kin of detectors, chip#1"
                            );
            fts.dddNameFormat = "{kin}{statNum2}";
        }
        // Define kin #2 for first chip
        {
            auto & fts =
                m.define_kin( "TETF"
                            , tstKinA2ID
                            , "TESTA"
                            , "{kin}{statNum2}"
                            , "Testing kin of detectors, chip#1"
                            );
            fts.dddNameFormat = "{kin}{statNum2}__";
        }
        // Represents an important case of detector without station id
        {
            auto & fts =
                m.define_kin( "SNGL"
                            , tstKinA3ID
                            , "TESTA"
                            , "{kin}:{pl}"
                            , "Testing kin of detectors, chip#1"
                            );
            fts.dddNameFormat = "{kin}";
        }
        // NOTE: contrary to kin names, non-unique chip+kin pairs are allowed
        // for different kin names.
        // This allows us to unite some redundant TBNames classification.
        // For example, GM01 and GEM15 might be physically same detectors with
        // different naming scheme, but this mappings should still allow one
        // to trait them as if they are of the same kin.
        // Define single kin for second chip
        {
            auto & fts =
                m.define_kin( "TSTS"
                            , tstKinB1ID
                            , "TESTB"
                            , "{kin}{statNum}"
                            , "Testing kin of detectors, chip#2"
                            );
            fts.dddNameFormat = "{kin}{statNum}";
        }
        {
            auto & fts =
                m.define_kin( "SNGLB"
                            , tstKinB2ID
                            , "TESTB"
                            , "{kin}"
                            , "Testing kin of detectors, chip#2"
                            );
            fts.dddNameFormat = "{kin}";
        }
    }
};

TEST_F(NameMappingsTest, insertionFailures) {
    // This must not be added due to range overflow
    ASSERT_THROW( m.chip_add( "TESTC"
                            , aux::gChipIDMax+1
                            )
                , errors::IDIsOutOfRange );
    // This must fail due to non-unique kin name
    ASSERT_THROW( m.define_kin( "TETF"
                              , tstKinA1ID+1
                              , "TESTB"
                              , "{kin}{statNum2}{projection}__"
                              , "Testing kin of detectors, chip#?"
                              )
                , errors::NameIsNotUniq
                );
    // This must not be added due to range overflow
    ASSERT_THROW( m.define_kin( "TETE"
                              , 0
                              , "TESTB"
                              , ""
                              , "Testing kin of detectors, chip#?"
                              )
                , errors::IDIsOutOfRange
                );
    ASSERT_THROW( m.define_kin( "TETE"
                              , aux::gKinIDMax + 1
                              , "TESTB"
                              , ""
                              , "Testing kin of detectors, chip#?"
                              )
                , errors::IDIsOutOfRange
                );
    // This must not be added due to non-existing chip
    ASSERT_THROW( m.define_kin( "TETE"
                              , 0x1
                              , "TESTC"
                              , ""
                              , "Testing kin of detectors, chip#?"
                              )
                , errors::NoEntryForKey<std::string>
                );
}

TEST_F(NameMappingsTest, nameIndexInitializationValidity){
    // retrieve chip id by name
    EXPECT_EQ( tstADID, m.chip_id("TESTA"));
    EXPECT_EQ( tstBDID, m.chip_id("TESTB"));
    // retrieve chip name by ID
    EXPECT_STREQ( "TESTA", m.chip_features( tstADID ).name.c_str() );
    EXPECT_STREQ( "TESTB", m.chip_features( tstBDID ).name.c_str() );
}

TEST_F(NameMappingsTest, generalConversionsValid){
    // parse tbnames
    {
        DetID did = m.id( "TETF01" );

        EXPECT_EQ( did.chip(), tstADID );
        EXPECT_EQ( did.kin(),  tstKinA2ID );
        EXPECT_FALSE( did.is_payload_set() );
        EXPECT_STREQ( "TETF01__", m[did].c_str() );
        did.number(23);
        EXPECT_STREQ( "TETF23__", m[did].c_str() );
    }
    {
        DetID did = m["TSTS28"];

        EXPECT_EQ( did.chip(), tstBDID );
        EXPECT_EQ( did.kin(),  tstKinB1ID );
        EXPECT_FALSE( did.is_payload_set() );
        EXPECT_STREQ( "TSTS28", m[did].c_str() );
        // TODO: EXPECT_STREQ( "TSTS28Uxysome", m.full_detector_name(did).c_str() );
    }
}

TEST_F(NameMappingsTest, fullConversions) {
    {  // no payload stub
        const char * str = "TETF02__";
        DetID did1 = m[str];
        EXPECT_EQ( did1.chip(), tstADID );
        EXPECT_EQ( did1.kin(), tstKinA2ID );
        EXPECT_EQ( did1.number(), 2 );
        EXPECT_FALSE( did1.is_payload_set() );
        EXPECT_STREQ( m[did1].c_str(), str );
    }
    {  // no payload stub
        DetID did1 = m["TSTF02foo"];
        EXPECT_EQ( did1.chip(), tstADID );
        EXPECT_EQ( did1.kin(), tstKinA1ID );
        EXPECT_EQ( did1.number(), 2 );
        ASSERT_TRUE( did1.is_payload_set() );
        ASSERT_EQ( did1.payload(), 2 );
        ASSERT_EQ( m["TETF12bar"].payload(), 3 );
    }
}

TEST_F( NameMappingsTest, setUnsetDiffersForStation ) {
    DetID did;
    const char * nm = "TETF";
    did = m[nm];
    EXPECT_TRUE( did.is_chip_set() );
    EXPECT_TRUE( did.is_kin_set() );
    EXPECT_FALSE( did.is_number_set() );
    EXPECT_FALSE( did.is_payload_set() );
    #if 1
    EXPECT_THROW( m[did].c_str()
                , errors::IncompleteDetectorID );
    #else
    try {
        std::string nm = m[did];
        FAIL() << "Expected throw on incomplete det id to-str op.";
    } catch( errors::IncompleteDetectorID & e ) {
        // ...
    } catch( std::exception & e ) {
        FAIL() << "Expected throw specific type on incomplete det id"
                  " to-str op, got: " << e.what();
    }
    #endif
}

TEST_F(NameMappingsTest, setUnsetDiffersForPlane) {
    DetID did;
    const char * nm = "TETF22__";
    did = m[nm];
    EXPECT_TRUE( did.is_chip_set() );
    EXPECT_TRUE( did.is_kin_set() );
    EXPECT_TRUE( did.is_number_set() );
    EXPECT_FALSE( did.is_payload_set() );
    EXPECT_STREQ( nm, m[did].c_str() );
}

TEST_F(NameMappingsTest, setUnsetDiffersForDetEntity) {
    DetID did;
    const char * nm = "TSTF02bar";
    did = m[nm];
    EXPECT_TRUE( did.is_chip_set() );
    EXPECT_TRUE( did.is_kin_set() );
    EXPECT_TRUE( did.is_number_set() );
    EXPECT_TRUE( did.is_payload_set() );
    EXPECT_STREQ( nm, m[did].c_str() );
}

TEST_F(NameMappingsTest, exceptionThrownForNoPayloadTranslation) {
    DetID did;
    EXPECT_THROW( did = m["TSTS01:foo"]
                , std::exception );
    EXPECT_THROW( did = m["TSTS01:foo"]
                , std::exception );
}

TEST_F(NameMappingsTest, setUnsetDiffersForSingleDetEntity) {
    DetID did;
    const char * nm = "SNGL:foo";
    did = m[nm];
    EXPECT_TRUE( did.is_chip_set() );
    EXPECT_TRUE( did.is_kin_set() );
    EXPECT_FALSE( did.is_number_set() );
    EXPECT_TRUE( did.is_payload_set() );
    EXPECT_STREQ( nm, m[did].c_str() );
}

}  // namespace na64dp

