#include "na64detID/wireID.hh"

#include <cstdio>
#include <stdexcept>
#include <cstring>
#include <regex>

#include "na64util/str-fmt.hh"

namespace na64dp {

char
APVPlaneID::proj_label( Projection p ) {
    switch(p) {
        case kX:
        case kX2:
            return 'X';
        case kY:
        case kY2:
            return 'Y';
        case kU:
        case kU2:
            return 'U';
        case kV:
        case kV2:
            return 'V';
        case kP:
            return 'P';
        default:
            return p ? '?' : '*';
    }
}

APVPlaneID::Projection
APVPlaneID::proj_code( char p ) {
    switch(p) {
        case 'X':
            return kX;
        case 'Y':
            return kY;
        case 'U':
            return kU;
        case 'V':
            return kV;
        case 'P':
            return kP;
        default:
            return kUnknown;
    }
}

APVPlaneID::Projection
APVPlaneID::adjoint_proj_code( Projection p ) {
    if( !(0x8 & p) ) return kUnknown;
    // set 3rd bit
    return (Projection) (p | 0x4);
}

/**By "conjugated projection planes" we imply correspoance between two (usually
 * ortogonal) detector planes like X&Y and U&V.
 *
 * Currently, this function assumes that conjugated planes are pairwise, i.e.
 * there is no detector composed with, say, X, U and V planes triplet (NA58
 * actually uses it like so).
 *
 * @param pjCode projection code
 * @throws std::runtime_error if given numerical ID is not one of
 * the WireID::Projection values.
 * */
APVPlaneID::Projection
APVPlaneID::conjugated_projection_code( WireID::Projection pjCode ) {
    if( kX  == pjCode ) return kY;
    if( kY  == pjCode ) return kX;

    if( kU == pjCode ) return kV;
    if( kV == pjCode ) return kU;

    NA64DP_RUNTIME_ERROR(
            , "Not a projection code / failed to get conjugated for: %d"
            , pjCode );
}

void
WireID::append_completion_context( DetID did
                                 , std::map<std::string, std::string> & m ) {
    char bf[64];
    if( ! did.is_payload_set() ) {
        m["proj"] = "";
        return;
    }
    WireID wID(did.payload());

    if( wID.wire_no_is_set() ) {
        snprintf(bf, sizeof(bf), "%d", wID.wire_no() );
        m["wireNo"] = bf;
    }

    if( wID.proj_is_set() ) {
        if( !WireID::proj_is_adjoint(wID.proj()) ) {
            snprintf(bf, sizeof(bf), "%c", WireID::proj_label(wID.proj()) );
        } else {
            snprintf(bf, sizeof(bf), "%c2", WireID::proj_label(wID.proj()) );
        }
        m["proj"] = bf;
        snprintf( bf, sizeof(bf)
                , "%c%c"
                , WireID::proj_label(wID.proj())
                , WireID::proj_is_adjoint(wID.proj()) ? '2' : '1'
                );
        m["projN"] = bf;
    } else {
        m["proj"] = "";  // since projection is usually a part of station pattern
    }
    WireID::to_string( did.payload(), bf, sizeof(bf) );
    m["idPayload"] = bf;
}

void
WireID::to_string( DetIDPayload_t pl
                 , char * buf
                 , size_t available ) {
    WireID wid(pl);
    if( !pl ) return;
    size_t used = 0;
    if( wid.proj_is_set() ) {
        *buf = WireID::proj_label(wid.proj());
        if( !WireID::proj_is_adjoint(wid.proj()) ) {
            if( available < 2 ) {
                NA64DP_RUNTIME_ERROR("Buffer too short.");
            }
            *buf = WireID::proj_label(wid.proj());
            ++buf;
            --available;
            *buf = '\0';
        } else {
            if( available < 3 ) {
                NA64DP_RUNTIME_ERROR("Buffer too short.");
            }
            buf[1] = '2';
            buf[2] = '\0';
            buf += 2;
            available -= 2;
        }
    }
    if( wid.wire_no_is_set() ) {
        size_t n = snprintf(buf, available, "-%d", (int) wid.wire_no());
        if( n >= available ) {
            NA64DP_RUNTIME_ERROR("Buffer too short.");
        }
    }
    #if 0
    if( wid.proj_is_set() && wid.wire_no_is_set() ) {
        if( proj_is_adjoint(wid.proj()) ) {
            used = snprintf( buf, available
                           , "%c-%d"
                           , proj_label(wid.proj())
                           , wid.wire_no() );
        }
    } else if( wid.proj_is_set() && !wid.wire_no_is_set() ) {
        used = snprintf( buf, available
                       , "%c"
                       , proj_label(wid.proj())
                       );
    } else {
        used = snprintf( buf, available
                       , "-%d"
                       , wid.wire_no()
                       );
    }
    #endif
    #if 0
    if(!(wid.proj_is_set() || wid.wire_no_is_set())) { *buf = '\0'; }
    else {
        NA64DP_RUNTIME_ERROR( "Bad form of wire identifier for to-string"
                              " conversion: projection is %sset, wire number is %sset."
                            , wid.proj_is_set() ? "" : "not "
                            , wid.wire_no_is_set() ? "" : "not "
                            );
    }
    #endif
    if( (size_t) used >= available ) {
        NA64DP_RUNTIME_ERROR( "Buffer of length %zu is"
                              " insufficient for WireID string, result truncated."
                            , available );
    }
}

// Groups:
//  0 -- whole match
//  1 -- "XY" or "UV", special case for dedicated straw TDC
//  2 -- projection symbol, X,Y,U or V
//  3 -- plane sub-number (like for 2nd layer of straws), a single digit
//  4 -- wire number, if present
static std::regex gRxTail
    = std::regex(R"~(^(?:((?:XY)|(?:UV))|(?:([XYUVP])([12])?))(?:_+)?(?:-(\d+))?$)~");
    ;
static const int gSpecialBiprojKeyGNum = 1  // {XY|UV} group number
               , gProjKeyGNum = 2
               , gProjKeySubGNum = 3
               , gWireGNum = 4
               ;

DetIDPayload_t
WireID::from_string( const char * widStr ) {
    /* Expected format:
     *  "Y-123"  -- standard, projection char and wire no
     *  "Y1__"   -- kludgy, only first letter must be interpreted
     *  "Y1__-123" -- kludgy, with wire number
     */
    #if 1
    WireID wid;
    std::cmatch m;
    if( !std::regex_match(widStr, m, gRxTail) ) {
        NA64DP_RUNTIME_ERROR("Tail portion of detector ID \"%s\" does not"
                " match expected pattern for wired detectors."
                , widStr );
    }
    if( m[gSpecialBiprojKeyGNum].length() ) {
        assert(!m[gProjKeyGNum].length());  // {XY|UV} is mutually exclusive with indiv proj-s
        const std::string tok(m[gSpecialBiprojKeyGNum].str());
        if(      tok == "XY" ) wid.proj(kXY);
        else if( tok == "UV" ) wid.proj(kUV);
        wid.unset_wire_no();
    } else if( m[gProjKeyGNum].length() ) {
        assert(!m[gSpecialBiprojKeyGNum].length());  // {XY|UV} is mutually exclusive with indiv proj-s
        assert(m[gProjKeyGNum].str().length() == 1);
        WireID::Projection projCode = WireID::proj_code(m[gProjKeyGNum].str()[0]);
        if(m[gProjKeySubGNum].length()) {
            // plane sub-number is given
            char subNum = m[gProjKeySubGNum].str()[0];
            if('2' == subNum) {
                projCode = WireID::adjoint_proj_code(projCode);
            } else if('1' != subNum) {
                NA64DP_RUNTIME_ERROR( "Sub-plane number %c is not supported."
                                    , subNum );
            }
        }
        wid.proj(projCode);
    }
    if(m[gWireGNum].length()) {
        int wireNo = stoi(m[gWireGNum].str());
        if( 0 > wireNo || wireNo > WireID::wireMax ) {
            NA64DP_RUNTIME_ERROR( "Wire number %ld exceed definition limits [0:%d)."
                    , wireNo, wireMax );
        }
        wid.wire_no(wireNo);
    }
    #else  // this *may* be a bit more efficient, yet becoming really hard to maintain...
    assert(widStr);
    if('\0' == *widStr) return 0x0;
    WireID wid;
    if( std::isalpha(*widStr) ) {  // projection
        if( isalpha(widStr[1]) ) {  // XY, UV, UX, DX, etc
            Projection pc1 = proj_code( widStr[0] )
                     , pc2 = proj_code( widStr[1] )
                     ;
            if( pc1 != kUnknown && pc2 != kUnknown ) {
                // both letters correspond to projections
                if( pc1 == kX && pc2 == kY ) {
                    wid.proj(kXY);
                } else if( pc1 == kU && pc2 == kV ) {
                    wid.proj(kUV);
                } else {
                    NA64DP_RUNTIME_ERROR( "Unable to interpret projection"
                            " code(s): \"%s\"", widStr );
                }
                ++widStr;
            } else {  // both are chars but one or both are not projections
                // this meant to be up/down or left/right parts for multipart
                // construction
                NA64DP_RUNTIME_ERROR( "Multipart detID (%s) not implemented.", widStr );
            }
            // end of "two first chars are letters"-clause
        } else {  // second letter is not a projection letter
            wid.proj(proj_code( widStr[0] ));
        }
        if( (!wid.proj_is_set()) || kUnknown == wid.proj() ) {
            NA64DP_RUNTIME_ERROR( "Projection code label '%x' can not be"
                    " interpreted in string %s.", widStr[0], widStr );
        }
        // NOTE: for GEMs we have a special naming scheme in ~2017, when
        // TBNames had a suffix "{proj}1__". To circumvent this feature, whe
        // do here a direct string comparison. "__" have to be ignored as well.
        if( '-' != widStr[1] && '\0' != widStr[1] ) {
            // There is something at the tail
            if( '2' == widStr[1] ) {

            }
            if( strncmp( "1__", widStr+1, 3 ) || strncmp( "__", widStr+1, 2 ) ) {
                // comparison failure
                NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion of string"
                    " \"%s\" as wire number (GM-2017 scheme assumed).", widStr+1 );
            }
            widStr += isdigit(widStr[1]) ? 3 : 2;
        }
        if('\0' == widStr[1]) return wid.id;  // only projection char was given
        if('-' != widStr[1]) {
            NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion of string"
                    " \"%s\" as wire number. Expected \"-<wireno>\"", widStr );
        }
        widStr += 2;
    }  // first matches projection (X, Y, U or V)
    // wire no, if given
    char * tail;
    long int wireNo = strtol( widStr, &tail, 10 );
    if( '\0' != *tail || tail == widStr ) {
        NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion \"%s\" of string"
                " \"%s\" as wire number.", tail, widStr );
    }
    if( 0 > wireNo || wireNo > WireID::wireMax ) {
        NA64DP_RUNTIME_ERROR( "Wire number %ld exceed definition limits [0:%d)."
                , wireNo, wireMax );
    }
    wid.wire_no( (uint16_t) wireNo );
    #endif
    return wid.id;
}

}

