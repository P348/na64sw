#include "na64detID/detectorID.hh"
#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

#include <gtest/gtest.h>

#include <unordered_map>

namespace na64dp {

TEST(HitTypeKey, setUnsetAreCorrect) {
    HitTypeKey tpk;
    EXPECT_FALSE( tpk.is_chip_set() );

    tpk.chip(0x0);
    EXPECT_TRUE( tpk.is_chip_set() );
    tpk.unset_chip();
    EXPECT_FALSE( tpk.is_chip_set() );

    tpk.chip(aux::gChipIDMax);
    EXPECT_TRUE( tpk.is_chip_set() );
    tpk.unset_chip();
    EXPECT_FALSE( tpk.is_chip_set() );
}

TEST(HitTypeKey, dropsIrrelevant) {
    DetID did;

    did.chip(aux::gChipIDMax);
    did.kin(0x0);
    did.kin(aux::gKinIDMax);
    did.number(0x0);
    did.payload(aux::gPayloadMax);

    HitTypeKey tpkA(did);
    EXPECT_TRUE( tpkA.is_chip_set() );
    EXPECT_EQ( tpkA.chip(), did.chip() );

    {  // hit type key must drop kin, station, payload
        DetID nid(tpkA.id);
        EXPECT_TRUE( nid.is_chip_set() );
        EXPECT_EQ( nid.chip(), did.chip() );
        EXPECT_FALSE( nid.is_kin_set() );
        EXPECT_FALSE( nid.is_number_set() );
        EXPECT_FALSE( nid.is_payload_set() );
    }
}


TEST(DetKinKey, setUnsetAreCorrect) {
    DetKinKey id;
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );

    id.chip(0x0);
    EXPECT_TRUE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );

    id.kin(aux::gKinIDMax);
    EXPECT_TRUE( id.is_chip_set() );
    EXPECT_TRUE( id.is_kin_set() );

    id.unset_chip();
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_TRUE( id.is_kin_set() );

    id.unset_kin();
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );
}

TEST(DetKinKey, dropsIrrelevant) {
    DetID did;

    did.chip(aux::gChipIDMax);
    did.kin(0x0);
    did.kin(aux::gKinIDMax);
    did.number(0x0);
    did.payload(aux::gPayloadMax);

    DetKinKey kinKey(did);
    EXPECT_TRUE( kinKey.is_chip_set() );
    EXPECT_EQ( kinKey.chip(), aux::gChipIDMax );
    EXPECT_TRUE( kinKey.is_kin_set() );
    EXPECT_EQ( kinKey.kin(), aux::gKinIDMax );

    {  // hit type key must drop kin, station, payload
        DetID nid(kinKey.id);
        EXPECT_TRUE( nid.is_chip_set() );
        EXPECT_EQ( nid.chip(), did.chip() );
        EXPECT_TRUE( nid.is_kin_set() );
        EXPECT_EQ( nid.kin(), did.kin() );
        EXPECT_FALSE( nid.is_number_set() );
        EXPECT_FALSE( nid.is_payload_set() );
    }
}


TEST(StationKey, setUnsetAreCorrect) {
    StationKey id;
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );
    EXPECT_FALSE( id.is_number_set() );

    id.chip(0x0);
    EXPECT_TRUE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );
    EXPECT_FALSE( id.is_number_set() );

    id.kin(aux::gKinIDMax);
    EXPECT_TRUE( id.is_chip_set() );
    EXPECT_TRUE( id.is_kin_set() );
    EXPECT_FALSE( id.is_number_set() );

    id.number(aux::gKinIDMax);
    EXPECT_TRUE( id.is_chip_set() );
    EXPECT_TRUE( id.is_kin_set() );
    EXPECT_TRUE( id.is_number_set() );

    id.unset_chip();
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_TRUE( id.is_kin_set() );
    EXPECT_TRUE( id.is_number_set() );

    id.unset_kin();
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );
    EXPECT_TRUE( id.is_number_set() );

    id.unset_number();
    EXPECT_FALSE( id.is_chip_set() );
    EXPECT_FALSE( id.is_kin_set() );
    EXPECT_FALSE( id.is_number_set() );
}

TEST(StationKey, dropsIrrelevant) {
    DetID did;

    did.chip(aux::gChipIDMax);
    did.kin(0x0);
    did.kin(aux::gKinIDMax);
    did.number(0x0);
    did.payload(aux::gPayloadMax);

    StationKey stKey(did);
    EXPECT_TRUE( stKey.is_chip_set() );
    EXPECT_EQ( stKey.chip(), aux::gChipIDMax );
    EXPECT_TRUE( stKey.is_kin_set() );
    EXPECT_EQ( stKey.kin(), aux::gKinIDMax );
    EXPECT_TRUE( stKey.is_number_set() );
    EXPECT_EQ( stKey.number(), 0x0 );

    {  // hit type key must drop kin, station, payload
        DetID nid(stKey.id);
        EXPECT_TRUE( nid.is_chip_set() );
        EXPECT_EQ( nid.chip(), did.chip() );
        EXPECT_TRUE( nid.is_kin_set() );
        EXPECT_EQ( nid.kin(), did.kin() );
        EXPECT_TRUE( nid.is_number_set() );
        EXPECT_EQ( nid.number(), did.number() );
        EXPECT_FALSE( nid.is_payload_set() );
    }
}

// Checks that by default detector ID is initialized to zero.
TEST(DetID, defaultIsUninitialized) {
    DetID id;
    EXPECT_FALSE(id.is_chip_set());
    EXPECT_FALSE(id.is_kin_set());
    EXPECT_FALSE(id.is_number_set());
    EXPECT_FALSE(id.is_payload_set());
}

// Checks that there are no overlaps within detector ID bitfields
TEST(DetID, fieldsDoesNotOverlap) {
    DetID id;

    id.chip(0x0);
    id.kin(0x0);
    id.number(0x0);
    id.payload(0x0);

    EXPECT_EQ(0, id.chip());
    EXPECT_EQ(0, id.kin());
    EXPECT_EQ(0, id.number());
    EXPECT_EQ(0, id.payload());

    id.unset_chip();
    id.kin(aux::gKinIDMax);
    id.unset_number();
    id.payload(aux::gPayloadMax);

    EXPECT_FALSE(id.is_chip_set());
    EXPECT_EQ(aux::gKinIDMax, id.kin());
    EXPECT_FALSE(id.is_number_set());
    EXPECT_EQ(aux::gPayloadMax, id.payload());

    id.chip(aux::gChipIDMax);
    id.unset_kin();
    id.number(aux::gDetNumMax);
    id.unset_payload();

    EXPECT_EQ(aux::gChipIDMax, id.chip());
    EXPECT_FALSE(id.is_kin_set());
    EXPECT_EQ(aux::gDetNumMax, id.number());
    EXPECT_FALSE(id.is_payload_set());
}

TEST(DetID, asMapKey) {
    std::map<DetID, int> m;
    m[DetID(1, 2, 3, 4)] = 1234;
    m[DetID(4, 3, 2, 1)] = 4321;

    EXPECT_EQ( m.find(DetID(1, 1, 1, 1)), m.end() );
    EXPECT_NE( m.find(DetID(1, 2, 3, 4)), m.end() );
    EXPECT_EQ( m[DetID(4, 3, 2, 1)], 4321 );

    DetID did( 1, 1, 1, 1 );
    did.unset_chip();
    did.unset_payload();
    m[did] = 1111;
    EXPECT_EQ( m[did], 1111 );
}

TEST(DetID, asUnorderedMapKey) {
    std::unordered_map<DetID, int> m;
    m[DetID(1, 2, 3, 4)] = 1234;
    m[DetID(4, 3, 2, 1)] = 4321;

    EXPECT_EQ( m.find(DetID(1, 1, 1, 1)), m.end() );
    EXPECT_NE( m.find(DetID(1, 2, 3, 4)), m.end() );
    EXPECT_EQ( m[DetID(4, 3, 2, 1)], 4321 );

    DetID did( 1, 1, 1, 1 );
    did.unset_chip();
    did.unset_payload();
    m[did] = 1111;
    EXPECT_EQ( m[did], 1111 );
}

}  // namespace na64dp

