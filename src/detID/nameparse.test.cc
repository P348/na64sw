#include "na64detID/nameparse.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace nameutils {

TEST( detNameParsing, commonNameForm ) {
    std::string name,  number, payload;
    ASSERT_TRUE( tokenize_name_str( "SOMEDET01", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "SOMEDET" );
    ASSERT_STREQ( number.c_str(), "01" );
    ASSERT_TRUE( payload.empty() );

    // tokenization fails due to lowercase letters
    ASSERT_FALSE( tokenize_name_str( "somedet01", name, number, payload ) );
    // tokenization fail due to digits
    ASSERT_FALSE( tokenize_name_str( "1AB01", name, number, payload ) );
}

TEST( detNameParsing, numberlessNameForm ) {
    std::string name,  number, payload;
    ASSERT_TRUE( tokenize_name_str( "SOME", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "SOME" );
    ASSERT_TRUE( number.empty() );
    ASSERT_TRUE( payload.empty() );
}

TEST( detNameParsing, tbnamePayloadForm ) {
    std::string name,  number, payload;

    ASSERT_TRUE( tokenize_name_str( "HOD2X", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "HOD" );
    ASSERT_STREQ( number.c_str(), "2" );
    ASSERT_STREQ( payload.c_str(), "X" );

    ASSERT_TRUE( tokenize_name_str( "MM2X", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "MM" );
    ASSERT_STREQ( number.c_str(), "2" );
    ASSERT_STREQ( payload.c_str(), "X" );

    ASSERT_TRUE( tokenize_name_str( "GM0Y__", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "GM" );
    ASSERT_STREQ( number.c_str(), "0" );
    ASSERT_STREQ( payload.c_str(), "Y__" );
}

TEST( detNameParsing, extendedPayloadForm ) {
    std::string name,  number, payload;

    ASSERT_TRUE( tokenize_name_str( "ECAL0:1", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "ECAL" );
    ASSERT_STREQ( number.c_str(), "0" );
    ASSERT_STREQ( payload.c_str(), "1" );

    ASSERT_TRUE( tokenize_name_str( "ECAL:1-2-3", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "ECAL" );
    ASSERT_TRUE( number.empty() );
    ASSERT_STREQ( payload.c_str(), "1-2-3" );

    ASSERT_TRUE( tokenize_name_str( "SOME-0", name, number, payload ) );
    ASSERT_STREQ( name.c_str(), "SOME" );
    ASSERT_TRUE( number.empty() );
    ASSERT_STREQ( payload.c_str(), "0" );
}

}
}
