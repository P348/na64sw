#include "na64detID/trackID.hh"

#include "na64util/str-fmt.hh"

namespace na64dp {

void
TrackID::zones(ZoneID_t zoneID) {
    if(zoneID > zoneMax) {
        NA64DP_RUNTIME_ERROR("Number value for zone ID exceeds limits:"
                " requested %zu > max value is %zu"
                , (size_t) zoneID, (size_t) zoneMax );
    }
    code &= numberMax;
    code |= ((TrackID_t) zoneID) << numberBitlen;
}

void
TrackID::number(TrackID_t nm) {
    if(nm > numberMax) {
        NA64DP_RUNTIME_ERROR("Number value for track ID exceeds limits:"
                " requested %zu > max value is %zu"
                , (size_t) nm, (size_t) numberMax );
    }
    code &= ~numberMax;
    code |= nm;
}

TrackID_t
TrackID::number() const {
    return code & numberMax;
}

}  // namespace na64dp

