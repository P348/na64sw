#include "na64detID/TBName.hh"
#include "na64detID/TBNameErrors.hh"
#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"
#include "na64detID/nameparse.hh"

#include <limits>
#include <cstdio>
#include <cstring>

#include <yaml-cpp/yaml.h>
#include <log4cpp/Category.hh>

// NOTE: sections marked as "TODO: ChipTraits" has to become the subject of
// dedicated type system addressing the call to particular extension

namespace na64dp {
namespace errors {

IDIsOutOfRange::IDIsOutOfRange( const char * what_, int proposed, int maximum ) throw()
        : std::runtime_error(what_)
        , _proposedID(proposed)
        , _maximumValue(maximum) {}

TBNameParsingFailure::TBNameParsingFailure( const char * msg, const char * culprit_ ) throw() :
        std::runtime_error( msg ) {
    strncpy( _TBName, culprit_, sizeof(_TBName) );
}

NameIsNotUniq::NameIsNotUniq( const char * what_, const char * nm_ ) throw()
        : std::runtime_error(what_) {
    strncpy( _name, nm_, sizeof(_name) );
}

}

namespace nameutils {

void
DetectorNaming::clear() {
    ChipFeaturesTable::clear();
    KinFeaturesTable::clear();
}

DetectorNaming::KinFeatures &
DetectorNaming::define_kin( const std::string & kinName
                          , DetKin_t kinID
                          , const std::string & chipName
                          , const std::string & fmt
                          , const std::string & kinDescription
                          , const std::string & histsTPath ) {
    DetChip_t chipID = chip_id(chipName);
    return define_kin( kinName, kinID, chipID, fmt, kinDescription, histsTPath );
}

void
DetectorNaming::_append_subst_dict_for( DetID did
                                      , std::map<std::string, std::string> & m
                                      , const ChipFeatures * chipFtsPtr
                                      , const KinFeatures * kinFtsPtr ) {
    char bf[128];
    if( did.is_chip_set() ) {
        assert(chipFtsPtr);  // LCOV_EXCL_LINE
        m["chip"] = chipFtsPtr->name;
        m["chipDesc"] = chipFtsPtr->description;
        snprintf(bf, sizeof(bf), "%#x", did.chip());
        m["chipNo"] = bf;
    }

    if( did.is_kin_set() ) {
        snprintf(bf, sizeof(bf), "%#x", did.kin());
        m["kinNo"] = bf;

        if( did.is_chip_set() ) {
            assert( kinFtsPtr );  // LCOV_EXCL_LINE
            m["kin"] = kinFtsPtr->name;
            m["kinDesc"] = kinFtsPtr->description;
        }
    }

    if( did.is_number_set() ) {
        snprintf(bf, sizeof(bf), "%d", did.number() );
        m["statNum"] = bf;

        snprintf(bf, sizeof(bf), "%02d", did.number() );
        m["statNum2"] = bf;
    }

    if( did.is_chip_set() ) {
        assert(chipFtsPtr);
        if(chipFtsPtr->to_string && did.is_payload_set()) {
            char plBf[64];
            chipFtsPtr->to_string(did.payload(), plBf, sizeof(plBf));
            m["subDet"] = plBf;
        }
        if(chipFtsPtr->append_completion_context) {
            chipFtsPtr->append_completion_context( did, m );
        }
    }
}

/** Naming substitution is needed in some applications that requires
 * interpretation of numerical detector identifier. This method of naming,
 * together with `str_subst()` function provides a way to render
 * meaningful strings based on the detector identifier. For any valid
 * detector ID the following list of substitutions will be written in map
 * object provided as an argument in this method:
 *
 * If chip is set by the given detector ID:
 *  * chip -- chip name (SADC, APV, etc)
 *  * chipNo -- chip number in hex form (e.g. 0x1, 0xa, etc)
 *  * chipDesc -- a chip description
 *
 * If chip and kin are set by the given ID:
 *  * kin -- kin name (ECAL, HCAL, GEM, etc)
 *  * kinNo -- kin number in hex form (e.g. 0x1, 0xff, etc)
 *  * kinDesc -- a kin (detector type) textual description
 *  * TBName -- a name in format of DDD. Note that if DDD format string
 *              contains `statNum` or `statNum2` and station number is not set
 *              by ID it won't be resolved (so TBName is not guaranteed to be
 *              corrected if station number is not set)
 *
 * If station number is set by the given ID:
 *  * statNum -- station number (1, 2, 123, etc.)
 *  * statNum2 -- station number of two digits (01, 02, etc)
 *
 * If chip and payload are set, the dictionary will be appended with
 * appropriate payload information (see `ChipTraits`).
 * */
void
DetectorNaming::append_subst_dict_for( DetID_t did_
                                     , std::map<std::string, std::string> & m ) const {
    DetID did(did_);
    const ChipFeatures * chipFtsPtr = did.is_chip_set()
                                ? &chip_features(did.chip())
                                : nullptr;
    const KinFeatures * kinFtsPtr = did.is_chip_set() && did.is_kin_set()
                                ? &kin_features( std::make_pair(did.chip(), did.kin()) )
                                : nullptr;
    return _append_subst_dict_for( did, m, chipFtsPtr, kinFtsPtr );
}

void
DetectorNaming::append_subst_dict_for( TrackID trackID
                                     , std::map<std::string, std::string> & m ) const {
    char bf[256];
    {
        snprintf(bf, sizeof(bf), "%#x", trackID.code);
        m["trackID"] = bf;
    }
    {
        snprintf(bf, sizeof(bf), "%#x", trackID.zones());
        m["trackZonePattern"] = bf;
    }
    // ...
}

/** Uses string template provided in `fmt` argument to produce a string form
 * of detector ID by its numerical representation. Although this method is
 * convenient in various contexts, note the following features:
 *
 *  - if chip is not defined by num ID, an exception will be thrown.
 *  - if kin is not defined by num ID, returned string is the chip name.
 *  - if station number is not defined, but it appears at kin format string,
 *    an exception (stating that detector ID is not complete) will be thrown.
 *  - if payload is not set, the returned string is somewhat similar to
 *    orginal "TBName" from `DaqDataDecoding` library. Examples are:
 *    "MM01X__", "ECAL0", "HCAL3", etc. This specific form is quite important.
 *  - for payload being set, the "extension suffix" will appear in the output
 *    string.
 *
 * */
std::string
DetectorNaming::name( DetID did ) const {
    if( !did.is_chip_set() ) {
        NA64DP_RUNTIME_ERROR( "Dectector ID (=%#x) has no chip ID."
                            , (int) did );
    }
    if( !did.is_kin_set() ) {
        return chip_features(did.chip()).name;
    }
    auto kinID = std::make_pair( did.chip(), did.kin() );
    const ChipFeatures & chipFts = chip_features( kinID.first );
    const KinFeatures & kinFts = kin_features( kinID );

    const std::string * templatePtr;
    if( did.is_payload_set() ) {
        // payload is set -- use full name format
        templatePtr = &detector_string_patern<&KinFeatures::nameFormat>( chipFts, kinFts );
    } else {
        // payload is not set -- use TBName format
        templatePtr = &detector_string_patern<&KinFeatures::dddNameFormat>( chipFts, kinFts );
    }

    // if resulting string template is empty -- nor kin, nor chip template
    // were not defined that must be considered as error in runtime
    // configuration (config file)
    if( templatePtr->empty() ) {
        if( did.is_payload_set() ) {
            NA64DP_RUNTIME_ERROR( "Full name pattern is not defined for"
                    " detector entity of kin \"%s\" of chip \"%s\""
                    , kinFts.name.c_str()
                    , chipFts.name.c_str() );
        } else {
            NA64DP_RUNTIME_ERROR( "TBName pattern is not defined for"
                    " detector station of kin \"%s\" of chip \"%s\""
                    , kinFts.name.c_str()
                    , chipFts.name.c_str() );
        }
    }

    // acquire context and render string
    util::StrSubstDict ctx;
    append_subst_dict_for( did, ctx );
    try {
        return util::str_subst( *templatePtr, ctx );
    } catch( errors::StringIncomplete & e ) {
        // this can still be a valid conversion if kin template implies
        // station number
        if( e.token() == "{statNum}" || e.token() == "{statNum2}" ) {
            return util::str_subst( "{kin}", ctx );
        }
        throw errors::IncompleteDetectorID( templatePtr->c_str()
                                          , e.incomplete_string().c_str() );
    }
}

std::string
DetectorNaming::name( StationKey sk ) const {  // TODO: verify
    if( !sk.is_chip_set() ) {
        NA64DP_RUNTIME_ERROR( "Dectector ID (=%#x) has no chip ID."
                            , (int) sk.id );
    }
    if( !sk.is_kin_set() ) {
        return chip_features(sk.chip()).name;
    }
    //auto kinID = std::make_pair( sk.chip(), sk.kin() );
    //const ChipFeatures & chipFts = chip_features( kinID.first );
    //const KinFeatures & kinFts = kin_features( kinID );

    std::string strTemplate = "{kin}{statNum2}";

    // acquire context and render string
    util::StrSubstDict ctx;
    append_subst_dict_for( DetID(sk.id), ctx );
    return util::str_subst( strTemplate, ctx );
}

#if 0
std::string
DetectorNaming::smart_name(DetID id) const {
    uint8_t avail
        = (    id.is_chip_set() ? 0x1 : 0x0)
        | (     id.is_kin_set() ? 0x2 : 0x0)
        | (  id.is_number_set() ? 0x4 : 0x0)
        | ( id.is_payload_set() ? 0x8 : 0x0)
        ;
    util::StrSubstDict ctx;
    append_subst_dict_for(DetID(id), ctx);
    switch(avail) {
        case 0x7:
        case 0xf:
        { // all set or all set except for payload
            return name(id);
        } break;
        case 0x3:
        { // only chip+kin set
            std::string tmpl = "{kin}";
            return util::str_subst(tmpl, ctx);
        } break;
        case 0x1:
        { // only chip is set
            std::string tmpl = "{chip}";
            return util::str_subst(tmpl, ctx);
        }
    };
    // if we reach this line, detector ID does not bring some "standard"
    // identifiers. In this case we shall still provide user code with usable
    // information, but particular use cases are still unclear...
    NA64DP_RUNTIME_ERROR("TODO: convert partial detector ID to string."); // TODO
}
#endif

DetID
DetectorNaming::id(const std::string & nm, bool noThrow) const {
    std::string name
              , number
              , payload
              ;
    if( !tokenize_name_str( nm, name, number, payload ) ) {
        if( noThrow )
            return DetID();
        NA64DP_RUNTIME_ERROR( "Failed to convert string \"%s\" to detector"
                " name (lexical mismatch).", nm.c_str() );
    }
    assert( !name.empty() );
    bool nameRefersToKin = has_kin(name);
    if( number.empty() && payload.empty() ) {
        // assume it is kin or chip name
        if( nameRefersToKin ) {
            // interpret as kin name
            KinID kid = kin_id(name);
            return DetID( kid.first, kid.second );
        }
        // otherwise, interpret name as chip name
        DetID did = DetID( chip_id(name), 0x0 );
        did.unset_kin();
        return did;
    }
    if( !nameRefersToKin ) {
        //NA64DP_RUNTIME_ERROR( "Name \"%s\" does not refer to known kin."
        //                    , name.c_str() );
        throw errors::UnknownKin( name, nm.c_str() );
    }
    KinID kid = kin_id(name);
    DetID did( kid.first, kid.second );
    assert(!did.is_payload_set());
    if( !number.empty() ) {
        char * endptr = nullptr;
        long int statNum = strtol( number.data(), &endptr, 10 );
        assert( statNum > -1 );
        did.number( (DetNumber_t) statNum );
        assert( endptr == number.data() + number.size() );
    }
    if( !payload.empty() ) {
        const ChipFeatures & chipFts = chip_features( kid.first );
        if( !chipFts.from_string ) {
            if( noThrow )
                return did;
            NA64DP_RUNTIME_ERROR(
                    "No payload-from-str conversion defined"
                    " for chip \"%s\"."
                    , chipFts.name.c_str() );
        }
        auto pl = chipFts.from_string(payload.data());
        if( pl ) {
            did.payload( pl );
            assert( did.payload() == pl );
        }
    }
    return did;
}


const std::string &
DetectorNaming::name_template(DetID detID) const {
    if( detID.is_payload_set() ) {
        return detector_string_patern<&KinFeatures::nameFormat>(
                chip_features( detID.chip() ),
                kin_features( std::make_pair(detID.chip(), detID.kin()) )
                );
    } else {
        return detector_string_patern<&KinFeatures::dddNameFormat>(
                chip_features( detID.chip() ),
                kin_features( std::make_pair(detID.chip(), detID.kin()) )
                );
    }
}

const std::string &
DetectorNaming::path_template(StationKey sk) const {
    return detector_string_patern<&KinFeatures::pathFormat>(
            chip_features( sk.chip() ),
            kin_features( std::make_pair(sk.chip(), sk.kin()) )
        );
}


void
DetectorIDStrSubstTraits< std::pair<DetID, DetID> >::append_context(
                const nameutils::DetectorNaming & nm
              , std::pair<DetID, DetID> k
              , util::StrSubstDict & ctx
              ) {
    util::StrSubstDict one, two;
    DetectorIDStrSubstTraits<DetID>::append_context(nm, k.first, one);
    DetectorIDStrSubstTraits<DetID>::append_context(nm, k.second, two);
    for( const auto & p : one ) {
        ctx.emplace( "first." + p.first, p.second );
    }
    for( const auto & p : two ) {
        ctx.emplace( "second." + p.first, p.second );
    }
}

// Default path template for plots identified by detector ID pair
static const std::string _dftHstTemplate
        = "multityped/{hist}-{first.kin}-vs-{second.kin}";

const std::string *
DetectorIDStrSubstTraits< std::pair<DetID, DetID> >::path_template_for(
                const nameutils::DetectorNaming & nm
              , std::pair<DetID, DetID> k ) {
    return &_dftHstTemplate;
}

}  // namespace na64::nameutils
}  // namespace na64
