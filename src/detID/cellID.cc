#include "na64detID/cellID.hh"
#include "na64util/str-fmt.hh"

#include <iostream>  // XXX

namespace na64dp {

CellID::CellID( int x, int y, int z ) : cellID(0x0) {
    assert( x <= idxMax );
    assert( y <= idxMax );
    assert( z <= idxMax );
    cellID |= ((x+1) & 0x1f)
           | (((y+1) & 0x1f) << 5)
           | (((z+1) & 0x1f) << 10)
           ;
}

void
CellID::set_x(unsigned int x){
    assert( x <= idxMax );
    cellID &= ~0x1f;
    cellID |= (x+1) & 0x1f;
}

unsigned int
CellID::get_x() const {
    uint16_t code = 0x1f & (cellID);
    return code != 0 ? code - 1 : 0x0;
}

bool
CellID::is_x_set() const {
    uint16_t code = 0x1f & (cellID);
    return code;
}

void
CellID::set_y(unsigned int y) {
    assert( y <= idxMax );
    cellID &= ~(0x1f << 5);
    cellID |=  ((y+1) << 5);
}

unsigned int
CellID::get_y() const {
    uint16_t code = 0x1f & (cellID >> 5);
    return code != 0 ? code - 1 : 0x0;
}

bool
CellID::is_y_set() const {
    uint16_t code = 0x1f & (cellID >> 5);
    return code;
}

void
CellID::set_z(unsigned int z){
    assert(z <= idxMax);
    cellID &= ~(0x1f << 10);
    cellID |=  ((z+1) << 10);
}

unsigned int
CellID::get_z() const {
    uint16_t code = 0x1f & (cellID >> 10);
    return code != 0 ? code - 1 : 0x0;
}

bool
CellID::is_z_set() const {
    uint16_t code = 0x1f & (cellID >> 10);
    return code;
}

void
CellID::append_completion_context( DetID did
                                 , std::map<std::string, std::string> & m ) {
	if( !did.is_payload_set() ) return;
    char bf[64];
    CellID cid(did.payload());
    if( ! cid.is_set() ) return;
    if( cid.is_x_set() ) {
        snprintf(bf, sizeof(bf), "%d", cid.get_x());
        m["xIdx"] = bf;
    } else {
        m["xIdx"] = "-";
    }
    if( cid.is_y_set() ) {
        snprintf(bf, sizeof(bf), "%d", cid.get_y());
        m["yIdx"] = bf;
    } else {
        m["yIdx"] = "-";
    }
    if( cid.is_z_set() ) {
        snprintf(bf, sizeof(bf), "%d", cid.get_z());
        m["zIdx"] = bf;
    } else {
        m["zIdx"] = "-";
    }
    // Unique features for Hodoscopes
    if( cid.get_z() == CellID::idxMax ) {
        m["proj"] = "Y";
    } else if( cid.get_z() == CellID::idxMax-1 ) {
        m["proj"] = "X";
    }
}

void
CellID::to_string( DetIDPayload_t pl, char * bf, size_t available ) {
    assert( bf && available );
    CellID cid(pl);
    if( !cid.is_set() ) {
        bf[0] = '\0';
        return;
    }

    char xBf[3], yBf[3], zBf[3], noSetBf[2] = "-";
    char *xStr, *yStr, *zStr;
    if( cid.is_x_set() ) {
        snprintf(xBf, sizeof(xBf), "%u", cid.get_x());
        xStr = xBf;
    } else {
        xStr = noSetBf;
    }
    if( cid.is_y_set() ) {
        snprintf(yBf, sizeof(yBf), "%u", cid.get_y());
        yStr = yBf;
    } else {
        yStr = noSetBf;
    }
    if( cid.is_z_set() ) {
        snprintf(zBf, sizeof(zBf), "%u", cid.get_z());
        zStr = zBf;
    } else {
        zStr = noSetBf;
    }

    int used = snprintf( bf, available
                       , "%s-%s-%s"
                       , xStr
                       , yStr
                       , zStr );
    if( (size_t) used >= available ) {
        NA64DP_RUNTIME_ERROR( "Buffer of length %zu is"
                              " insufficient for CellID string, result truncated."
                            , available );
    }
}

DetIDPayload_t
CellID::from_string( const char * cidStr ) {
    assert(cidStr);
    if('\0' == *cidStr) return 0x0;  // no payload

    long idx;
    CellID cid;
    char * rest;
    #if 1  // Special case for hodoscopes (non-standard naming scheme) and TDC
    {
        bool isX = 'X' == *cidStr
           , isY = 'Y' == *cidStr
           ;
        if( isX || isY ) {
            if( '\0' == cidStr[1] ) {
                // This is a native TBName case -- use Z index within payload
                // to distinguish X/Y planes
                cid.set_z( isX ? CellID::idxMax-1 : CellID::idxMax );
                assert(cid.get_z());
                return cid.cellID;
            }
            #if 1
            ++cidStr;
            #else
            idx = strtol( cidStr+2, &rest, 10 );
            if( cidStr+2 == rest || '\0' != *rest ) {
                NA64DP_RUNTIME_ERROR( "Bad suffix (assumed hodoscope's): \"%s\"."
                                    , cidStr );
            }
            if( idx < 0 || idx > idxMax ) {
                NA64DP_RUNTIME_ERROR( "Hodoscope %c-index %ld is out of boundaries: [0,%d]."
                                    , *cidStr, idx, idxMax );
            }
            if( isX ) {
                cid.set_z(CellID::idxMax-1);
            } else {
                cid.set_z(CellID::idxMax  );
            }
            assert( cid.get_z() );
            return cid.cellID;
            #endif
        }
    }
    #endif
    // X
    if( '-' != *cidStr ) {
        idx = strtol(cidStr, &rest, 10);
        if( cidStr == rest ) {
            NA64DP_RUNTIME_ERROR( "Unable to interpret X-index in SADC name suffix \"%s\"."
                                , cidStr );
        }
        if( idx < 0 || idx > idxMax ) {
            NA64DP_RUNTIME_ERROR( "Cell index X %ld out of boundaries: [0,%d]."
                                , idx, idxMax );
        }
        cid.set_x(idx);
        if( *rest == '\0' ) return cid.cellID;
        if( *rest != '-' ) {
            NA64DP_RUNTIME_ERROR( "Unable to parse SADC cell/subdetector ID."
                    " Expected '-' after X-index, got '%c'.", *cidStr );
        }
    } else {
        rest = const_cast<char*>(cidStr) + 2;
    }
    // Y
    if( '-' != rest[1] ) {
        idx = strtol(rest + 1, &rest, 10);
        if( idx < 0 && idx > idxMax ) {
            NA64DP_RUNTIME_ERROR( "Cell index Y %d out of boundaries: [0,%d]."
                                , idx, idxMax );
        }
        cid.set_y(idx);
        if( *rest == '\0' ) return cid.cellID;
        if( *rest != '-' ) {
            NA64DP_RUNTIME_ERROR( "Unable to parse SADC cell/subdetector ID."
                    " Expected '-' after Y-index, got '%c'.", *cidStr );
        }
    } else {
        rest = rest + 2;
    }
    // Z
    if( '-' != rest[1] ) {
        idx = strtol(rest + 1, &rest, 10);
        if( idx < 0 && idx > idxMax ) {
            NA64DP_RUNTIME_ERROR( "Cell index Y %d out of boundaries: [0,%d]."
                                , idx, idxMax );
        }
        cid.set_z(idx);
    } else {
        rest = rest + 2;
    }
    if( *rest == '\0' ) return cid.cellID;
    NA64DP_RUNTIME_ERROR( "Cell index parsing error: remainder \"%s\"."
                        , rest );
}

}

