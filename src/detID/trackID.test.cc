#include "na64detID/trackID.hh"

#include <gtest/gtest.h>

namespace na64dp {

TEST(TrackID, getSet) {
    ZoneID_t zoneIDs[] = {0x0, 0x10, 129, TrackID::zoneMax};
    TrackID_t numbers[] = {0x0, 0x1010, 0x101010, TrackID::numberMax};
    for( size_t nZone = 0; nZone < sizeof(zoneIDs)/sizeof(zoneIDs[0]); ++nZone ) {
        for( size_t n = 0; n < sizeof(numbers)/sizeof(numbers[0]); ++n ) {
            TrackID tid(zoneIDs[nZone], n);
            EXPECT_EQ(tid.zones(), zoneIDs[nZone]);
            EXPECT_EQ(tid.number(), n);

            tid.zones(0x0);
            EXPECT_EQ(tid.zones(), 0x0);
            EXPECT_EQ(tid.number(), n);

            tid.zones(zoneIDs[nZone]);
            EXPECT_EQ(tid.zones(),zoneIDs[nZone]);
            EXPECT_EQ(tid.number(), n);

            tid.number(0x0);
            EXPECT_EQ(tid.zones(),zoneIDs[nZone]);
            EXPECT_EQ(tid.number(), 0);

            tid.number(n);
            EXPECT_EQ(tid.zones(),zoneIDs[nZone]);
            EXPECT_EQ(tid.number(), n);
        }
    }
}

}  // namespace na64dp
