#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for DriftDetScoreFeatures, MCTrueTrackScore, TrackScore, ScoreFitInfo, TrackFitInfo, Track.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1
 */

namespace na64dp {
namespace event {

void
define_track_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<DriftDetScoreFeatures>("DriftDetScoreFeatures")
        .attr<&DriftDetScoreFeatures::distance>("distance")
        .attr<&DriftDetScoreFeatures::distanceError>("distanceError")
        .attr<&DriftDetScoreFeatures::maxDistance>("maxDistance")
        .attr<&DriftDetScoreFeatures::wireCoordinates>("wireCoordinates")
        .attr<&DriftDetScoreFeatures::sideHitUError>("sideHitUError")
        .attr<&DriftDetScoreFeatures::sideHitBError>("sideHitBError")
        .attr<&DriftDetScoreFeatures::sideWeight>("sideWeight")
        .attr<&DriftDetScoreFeatures::uError>("uError")
    .end_compound();
    types.new_compound<MCTrueTrackScore>("MCTrueTrackScore")
        .attr<&MCTrueTrackScore::globalPosition>("globalPosition")
        .attr<&MCTrueTrackScore::geant4TrackID>("geant4TrackID")
    .end_compound();
    types.new_compound<TrackScore>("TrackScore")
        .attr<&TrackScore::lR>("lR")
        .attr<&TrackScore::gR>("gR")
        .attr<&TrackScore::lRErr>("lRErr")
        .attr<&TrackScore::driftFts>("driftFts")
        .attr<&TrackScore::time>("time")
        .attr<&TrackScore::mcTruth>("mcTruth")
    .end_compound();
    types.new_compound<ScoreFitInfo>("ScoreFitInfo")
        .attr<&ScoreFitInfo::score>("score")
        .attr<&ScoreFitInfo::lR>("lR")
        .attr<&ScoreFitInfo::lRUErr>("lRUErr")
        .attr<&ScoreFitInfo::lRBErr>("lRBErr")
        .attr<&ScoreFitInfo::lTan>("lTan")
        .attr<&ScoreFitInfo::qop>("qop")
        .attr<&ScoreFitInfo::time>("time")
        .attr<&ScoreFitInfo::weight>("weight")
    .end_compound();
    types.new_compound<TrackFitInfo>("TrackFitInfo")
        .attr<&TrackFitInfo::positionSeed>("positionSeed")
        .attr<&TrackFitInfo::momentumSeed>("momentumSeed")
        .attr<&TrackFitInfo::covarSeed>("covarSeed")
        .attr<&TrackFitInfo::chi2>("chi2")
        .attr<&TrackFitInfo::ndf>("ndf")
        .attr<&TrackFitInfo::pval>("pval")
    .end_compound();
    types.new_compound<Track>("Track")
        .attr<&Track::scores>("scores")
        .attr<&Track::momentum>("momentum")
        .attr<&Track::pdg>("pdg")
        .attr<&Track::fitInfo>("fitInfo")
        .attr<&Track::zonePattern>("zonePattern")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
