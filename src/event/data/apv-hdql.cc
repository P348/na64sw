#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for RawDataAPV, APVHit, APVCluster.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1
 */

namespace na64dp {
namespace event {

void
define_apv_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<RawDataAPV>("RawDataAPV")
        .attr<&RawDataAPV::wireNo>("wireNo")
        .attr<&RawDataAPV::chip>("chip")
        .attr<&RawDataAPV::srcID>("srcID")
        .attr<&RawDataAPV::adcID>("adcID")
        .attr<&RawDataAPV::chipChannel>("chipChannel")
        .attr<&RawDataAPV::timeTag>("timeTag")
        .attr<&RawDataAPV::samples>("samples")
    .end_compound();
    types.new_compound<APVHit>("APVHit")
        .attr<&APVHit::rawData>("rawData")
        .attr<&APVHit::maxCharge>("maxCharge")
        .attr<&APVHit::timeRise>("timeRise")
        .attr<&APVHit::a02>("a02")
        .attr<&APVHit::a12>("a12")
        .attr<&APVHit::t02>("t02")
        .attr<&APVHit::t12>("t12")
        .attr<&APVHit::t02sigma>("t02sigma")
        .attr<&APVHit::t12sigma>("t12sigma")
        .attr<&APVHit::time>("time")
        .attr<&APVHit::timeError>("timeError")
    .end_compound();
    types.new_compound<APVCluster>("APVCluster")
        .attr<&APVCluster::hits>("hits")
        .attr<&APVCluster::position>("position")
        .attr<&APVCluster::positionError>("positionError")
        .attr<&APVCluster::charge>("charge")
        .attr<&APVCluster::mcTruePosition>("mcTruePosition")
        .attr<&APVCluster::time>("time")
        .attr<&APVCluster::timeError>("timeError")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
