#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for RawDataF1, F1Hit.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1
 */

namespace na64dp {
namespace event {

void
define_f1_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<RawDataF1>("RawDataF1")
        .attr<&RawDataF1::sourceID>("sourceID")
        .attr<&RawDataF1::portID>("portID")
        .attr<&RawDataF1::channel>("channel")
        .attr<&RawDataF1::channelPosition>("channelPosition")
        .attr<&RawDataF1::time>("time")
        .attr<&RawDataF1::timeUnit>("timeUnit")
        .attr<&RawDataF1::timeDecoded>("timeDecoded")
        .attr<&RawDataF1::timeReference>("timeReference")
    .end_compound();
    types.new_compound<F1Hit>("F1Hit")
        .attr<&F1Hit::rawData>("rawData")
        .attr<&F1Hit::hitPosition>("hitPosition")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
