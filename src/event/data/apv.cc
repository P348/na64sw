/**\file
 * \brief Declarations for hits data on APV managed detectors.
 * 
 * APV chip is analogue demultiplexer generally used to communicate a vast
 * number of analog wires into sampling ADC input. Though the ADC is
 * generally a SADC chip we distinguish between SADC and APV managed detectors
 * following convention of original COMPASS/NA64 DAQ data decoding library.
 * 
 * For reconstruction, individual APV hits are usually joined together within
 * a cluster that then represents a fact of ionazing particle passing through
 * the detector's active medium.
 * 
 * These clusters then used in tracking algorithms to reconstruct particle
 * tracks.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/apv.hh"

#include "na64event/data/apv.hh"

#include "na64event/data/apv.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {





namespace event {
APVHit::APVHit( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp



namespace event {
APVCluster::APVCluster( LocalMemory & lmem )
     : hits(lmem)
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::RawDataAPV & obj ) {
    using namespace event;
    
    obj.wireNo = std::numeric_limits<uint16_t>::max();;
    ::na64dp::util::reset(obj.chip);
    
    ::na64dp::util::reset(obj.srcID);
    
    ::na64dp::util::reset(obj.adcID);
    
    ::na64dp::util::reset(obj.chipChannel);
    
    ::na64dp::util::reset(obj.timeTag);
    obj.samples[0]
    = obj.samples[1]
    = obj.samples[2]
    = std::numeric_limits<uint32_t>::max();

}
}  // namespace ::na64dp::util

namespace util {
void reset( event::APVHit & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.rawData);
    
    ::na64dp::util::reset(obj.maxCharge);
    
    ::na64dp::util::reset(obj.timeRise);
    
    ::na64dp::util::reset(obj.a02);
    
    ::na64dp::util::reset(obj.a12);
    
    ::na64dp::util::reset(obj.t02);
    
    ::na64dp::util::reset(obj.t12);
    
    ::na64dp::util::reset(obj.t02sigma);
    
    ::na64dp::util::reset(obj.t12sigma);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeError);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::APVCluster & obj ) {
    using namespace event;
    
    obj.hits.clear();
    ::na64dp::util::reset(obj.position);
    
    ::na64dp::util::reset(obj.positionError);
    
    ::na64dp::util::reset(obj.charge);
    
    ::na64dp::util::reset(obj.mcTruePosition);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeError);
    
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _RawDataAPV_get_wireNo( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.wireNo);  // simple value
}
static StdFloat_t _RawDataAPV_get_chip( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.chip);  // simple value
}
static StdFloat_t _RawDataAPV_get_srcID( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.srcID);  // simple value
}
static StdFloat_t _RawDataAPV_get_adcID( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.adcID);  // simple value
}
static StdFloat_t _RawDataAPV_get_chipChannel( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.chipChannel);  // simple value
}
static StdFloat_t _RawDataAPV_get_timeTag( const RawDataAPV & obj ) {
    return static_cast<StdFloat_t>(obj.timeTag);  // simple value
}
// getter for RawDataAPV::samples disabled



const Traits<RawDataAPV>::Getters
Traits<RawDataAPV>::getters = {
    
    { "wireNo", { "Triggered wire number (detector\u0027s channel)", _RawDataAPV_get_wireNo } },
    { "chip", { "chip ID wrt DDD DAQ", _RawDataAPV_get_chip } },
    { "srcID", { "source ID wrt DDD DAQ", _RawDataAPV_get_srcID } },
    { "adcID", { "adc ID wrt DDD DAQ", _RawDataAPV_get_adcID } },
    { "chipChannel", { "channel number inside chip (before remapping)", _RawDataAPV_get_chipChannel } },
    { "timeTag", { "Time with the respect to trigger time", _RawDataAPV_get_timeTag } },

    // getter for RawDataAPV::samples disabled


};






// propagated from  / rawData:mem::Ref<RawDataAPV> / wireNo:APVDAQWire_t
static StdFloat_t _APVHit_get_rawData_wireNo( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.wireNo);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataAPV> / chip:uint16_t
static StdFloat_t _APVHit_get_rawData_chip( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.chip);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataAPV> / srcID:uint16_t
static StdFloat_t _APVHit_get_rawData_srcID( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.srcID);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataAPV> / adcID:uint16_t
static StdFloat_t _APVHit_get_rawData_adcID( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.adcID);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataAPV> / chipChannel:uint16_t
static StdFloat_t _APVHit_get_rawData_chipChannel( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.chipChannel);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataAPV> / timeTag:uint32_t
static StdFloat_t _APVHit_get_rawData_timeTag( const APVHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.timeTag);  // simple value
    
}

// no std getter for APVHit/rawData/samples


static StdFloat_t _APVHit_get_maxCharge( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.maxCharge);  // simple value
}
static StdFloat_t _APVHit_get_timeRise( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.timeRise);  // simple value
}
static StdFloat_t _APVHit_get_a02( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.a02);  // simple value
}
static StdFloat_t _APVHit_get_a12( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.a12);  // simple value
}
static StdFloat_t _APVHit_get_t02( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.t02);  // simple value
}
static StdFloat_t _APVHit_get_t12( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.t12);  // simple value
}
static StdFloat_t _APVHit_get_t02sigma( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.t02sigma);  // simple value
}
static StdFloat_t _APVHit_get_t12sigma( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.t12sigma);  // simple value
}
static StdFloat_t _APVHit_get_time( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _APVHit_get_timeError( const APVHit & obj ) {
    return static_cast<StdFloat_t>(obj.timeError);  // simple value
}



const Traits<APVHit>::Getters
Traits<APVHit>::getters = {
    


    // propagated from  / rawData:mem::Ref<RawDataAPV> / wireNo:APVDAQWire_t
    { "rawData.wireNo", { "Triggered wire number (detector\u0027s channel)", _APVHit_get_rawData_wireNo } },
    // propagated from  / rawData:mem::Ref<RawDataAPV> / chip:uint16_t
    { "rawData.chip", { "chip ID wrt DDD DAQ", _APVHit_get_rawData_chip } },
    // propagated from  / rawData:mem::Ref<RawDataAPV> / srcID:uint16_t
    { "rawData.srcID", { "source ID wrt DDD DAQ", _APVHit_get_rawData_srcID } },
    // propagated from  / rawData:mem::Ref<RawDataAPV> / adcID:uint16_t
    { "rawData.adcID", { "adc ID wrt DDD DAQ", _APVHit_get_rawData_adcID } },
    // propagated from  / rawData:mem::Ref<RawDataAPV> / chipChannel:uint16_t
    { "rawData.chipChannel", { "channel number inside chip (before remapping)", _APVHit_get_rawData_chipChannel } },
    // propagated from  / rawData:mem::Ref<RawDataAPV> / timeTag:uint32_t
    { "rawData.timeTag", { "Time with the respect to trigger time", _APVHit_get_rawData_timeTag } },
    // no std getter for APVHit/rawData/samples


    { "maxCharge", { "Hit max charge wrt APV waveform", _APVHit_get_maxCharge } },
    { "timeRise", { "Time of rising front", _APVHit_get_timeRise } },
    { "a02", { "APV samples amplitude ratio a0/a2", _APVHit_get_a02 } },
    { "a12", { "APV samples amplitude ratio a1/a2", _APVHit_get_a12 } },
    { "t02", { "Tangent values of raising front, at a0 and a2", _APVHit_get_t02 } },
    { "t12", { "Tangent values of raising front, at a1 and a2", _APVHit_get_t12 } },
    { "t02sigma", { "Error estimation of tangent raising front", _APVHit_get_t02sigma } },
    { "t12sigma", { "Error estimation of tangent raising front", _APVHit_get_t12sigma } },
    { "time", { "Estimated hit time, [SADC sample #]", _APVHit_get_time } },
    { "timeError", { "Error of hit time estimation, [SADC sample #]", _APVHit_get_timeError } },


};




// getter for APVCluster::hits disabled
static StdFloat_t _APVCluster_get_position( const APVCluster & obj ) {
    return static_cast<StdFloat_t>(obj.position);  // simple value
}
static StdFloat_t _APVCluster_get_positionError( const APVCluster & obj ) {
    return static_cast<StdFloat_t>(obj.positionError);  // simple value
}
static StdFloat_t _APVCluster_get_charge( const APVCluster & obj ) {
    return static_cast<StdFloat_t>(obj.charge);  // simple value
}
// getter for APVCluster::mcTruePosition disabled
static StdFloat_t _APVCluster_get_time( const APVCluster & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _APVCluster_get_timeError( const APVCluster & obj ) {
    return static_cast<StdFloat_t>(obj.timeError);  // simple value
}

static StdFloat_t _APVCluster_get_size( const APVCluster & obj ) { return obj.hits.size(); }
static StdFloat_t _APVCluster_get_avrgCharge( const APVCluster & obj ) { return obj.charge / obj.hits.size(); }


const Traits<APVCluster>::Getters
Traits<APVCluster>::getters = {
    

    // getter for APVCluster::hits disabled
    { "position", { "Coordinate of cluster\u0027s center, wire number units", _APVCluster_get_position } },
    { "positionError", { "Coordinate uncertainty, wire units", _APVCluster_get_positionError } },
    { "charge", { "Cluster charge sum (unnormed), sum of all hit\u0027s amplitude measurements", _APVCluster_get_charge } },

    // getter for APVCluster::mcTruePosition disabled
    { "time", { "Cluster time", _APVCluster_get_time } },
    { "timeError", { "Cluster time estimated error", _APVCluster_get_timeError } },

    { "size", { "Returns number of hits included in cluster.", _APVCluster_get_size } },
    { "avrgCharge", { "Average charge value.", _APVCluster_get_avrgCharge } },

};



}  // namespace ::na64dp::event
}  // namespace na64dp
