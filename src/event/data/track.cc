/**\file
 * A (reconstructed) particle track data declaration.
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/track.hh"

#include "na64event/data/track.hh"

#include "na64event/data/track.hh"

#include "na64event/data/track.hh"

#include "na64event/data/track.hh"

#include "na64event/data/track.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {







namespace event {
TrackScore::TrackScore( LocalMemory & lmem )
     : hitRefs(lmem)
    
{}
}  // namespace ::event::na64dp



namespace event {
ScoreFitInfo::ScoreFitInfo( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp





namespace event {
Track::Track( LocalMemory & lmem )
     : scores(lmem)
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::DriftDetScoreFeatures & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.distance);
    
    ::na64dp::util::reset(obj.distanceError);
    
    ::na64dp::util::reset(obj.maxDistance);
    
    ::na64dp::util::reset(obj.wireCoordinates);
    
    ::na64dp::util::reset(obj.sideHitUError);
    
    ::na64dp::util::reset(obj.sideHitBError);
    
    ::na64dp::util::reset(obj.sideWeight);
    
    ::na64dp::util::reset(obj.uError);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::MCTrueTrackScore & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.globalPosition);
    
    ::na64dp::util::reset(obj.geant4TrackID);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::TrackScore & obj ) {
    using namespace event;
    
    obj.hitRefs.clear();
    ::na64dp::util::reset(obj.lR);
    
    ::na64dp::util::reset(obj.gR);
    
    ::na64dp::util::reset(obj.lRErr);
    
    ::na64dp::util::reset(obj.driftFts);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.mcTruth);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::ScoreFitInfo & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.score);
    
    ::na64dp::util::reset(obj.lR);
    
    ::na64dp::util::reset(obj.lRUErr);
    
    ::na64dp::util::reset(obj.lRBErr);
    
    ::na64dp::util::reset(obj.lTan);
    
    ::na64dp::util::reset(obj.qop);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.weight);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::TrackFitInfo & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.positionSeed);
    
    ::na64dp::util::reset(obj.momentumSeed);
    
    ::na64dp::util::reset(obj.covarSeed);
    
    ::na64dp::util::reset(obj.chi2);
    
    ::na64dp::util::reset(obj.ndf);
    
    ::na64dp::util::reset(obj.pval);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::Track & obj ) {
    using namespace event;
    
    obj.scores.clear();
    ::na64dp::util::reset(obj.momentum);
    
    ::na64dp::util::reset(obj.pdg);
    
    ::na64dp::util::reset(obj.fitInfo);
    
    ::na64dp::util::reset(obj.zonePattern);
    
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _DriftDetScoreFeatures_get_distance( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.distance);  // simple value
}
static StdFloat_t _DriftDetScoreFeatures_get_distanceError( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.distanceError);  // simple value
}
static StdFloat_t _DriftDetScoreFeatures_get_maxDistance( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.maxDistance);  // simple value
}
// getter for DriftDetScoreFeatures::wireCoordinates disabled
static StdFloat_t _DriftDetScoreFeatures_get_sideHitUError( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.sideHitUError);  // simple value
}
static StdFloat_t _DriftDetScoreFeatures_get_sideHitBError( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.sideHitBError);  // simple value
}
static StdFloat_t _DriftDetScoreFeatures_get_sideWeight( const DriftDetScoreFeatures & obj ) {
    return static_cast<StdFloat_t>(obj.sideWeight);  // simple value
}
// getter for DriftDetScoreFeatures::uError disabled



const Traits<DriftDetScoreFeatures>::Getters
Traits<DriftDetScoreFeatures>::getters = {
    
    { "distance", { "Estimated distance, cm", _DriftDetScoreFeatures_get_distance } },
    { "distanceError", { "Error of distance estimation, cm", _DriftDetScoreFeatures_get_distanceError } },
    { "maxDistance", { "max distance (cm) from the wire; typically a wire\u0027s stride", _DriftDetScoreFeatures_get_maxDistance } },

    // getter for DriftDetScoreFeatures::wireCoordinates disabled
    { "sideHitUError", { "unbiased error on other side of isochrone", _DriftDetScoreFeatures_get_sideHitUError } },
    { "sideHitBError", { "biased error on other side of isochrone", _DriftDetScoreFeatures_get_sideHitBError } },
    { "sideWeight", { "tracking weight of other side isochrone", _DriftDetScoreFeatures_get_sideWeight } },

    // getter for DriftDetScoreFeatures::uError disabled


};




// getter for MCTrueTrackScore::globalPosition disabled
static StdFloat_t _MCTrueTrackScore_get_geant4TrackID( const MCTrueTrackScore & obj ) {
    return static_cast<StdFloat_t>(obj.geant4TrackID);  // simple value
}



const Traits<MCTrueTrackScore>::Getters
Traits<MCTrueTrackScore>::getters = {
    

    // getter for MCTrueTrackScore::globalPosition disabled
    { "geant4TrackID", { "(only for MC) Geant4 track ID for this score", _MCTrueTrackScore_get_geant4TrackID } },


};




// getter for TrackScore::hitRefs disabled
// getter for TrackScore::lR disabled
// getter for TrackScore::gR disabled
// getter for TrackScore::lRErr disabled


// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / distance:float
static StdFloat_t _TrackScore_get_driftFts_distance( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.distance);  // simple value
    
}

// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / distanceError:float
static StdFloat_t _TrackScore_get_driftFts_distanceError( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.distanceError);  // simple value
    
}

// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / maxDistance:float
static StdFloat_t _TrackScore_get_driftFts_maxDistance( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.maxDistance);  // simple value
    
}

// no std getter for TrackScore/driftFts/wireCoordinates
// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitUError:float
static StdFloat_t _TrackScore_get_driftFts_sideHitUError( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideHitUError);  // simple value
    
}

// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitBError:float
static StdFloat_t _TrackScore_get_driftFts_sideHitBError( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideHitBError);  // simple value
    
}

// propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideWeight:float
static StdFloat_t _TrackScore_get_driftFts_sideWeight( const TrackScore & obj_ ) {
    if(!obj_.driftFts) return std::nan("0");
    const auto & driftFts = *obj_.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideWeight);  // simple value
    
}

// no std getter for TrackScore/driftFts/uError


static StdFloat_t _TrackScore_get_time( const TrackScore & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}


// no std getter for TrackScore/mcTruth/globalPosition
// propagated from  / mcTruth:mem::Ref<MCTrueTrackScore> / geant4TrackID:int
static StdFloat_t _TrackScore_get_mcTruth_geant4TrackID( const TrackScore & obj_ ) {
    if(!obj_.mcTruth) return std::nan("0");
    const auto & mcTruth = *obj_.mcTruth;
    const auto & obj = mcTruth;
    
    return static_cast<StdFloat_t>(obj.geant4TrackID);  // simple value
    
}




static StdFloat_t _TrackScore_get_u( const TrackScore & obj ) { return obj.lR[0]; }
static StdFloat_t _TrackScore_get_v( const TrackScore & obj ) { return obj.lR[1]; }
static StdFloat_t _TrackScore_get_w( const TrackScore & obj ) { return obj.lR[2]; }
static StdFloat_t _TrackScore_get_uE( const TrackScore & obj ) { return obj.lRErr[0]; }
static StdFloat_t _TrackScore_get_vE( const TrackScore & obj ) { return obj.lRErr[1]; }
static StdFloat_t _TrackScore_get_wE( const TrackScore & obj ) { return obj.lRErr[2]; }


const Traits<TrackScore>::Getters
Traits<TrackScore>::getters = {
    

    // getter for TrackScore::hitRefs disabled

    // getter for TrackScore::lR disabled

    // getter for TrackScore::gR disabled

    // getter for TrackScore::lRErr disabled


    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / distance:float
    { "driftFts.distance", { "Estimated distance, cm", _TrackScore_get_driftFts_distance } },
    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / distanceError:float
    { "driftFts.distanceError", { "Error of distance estimation, cm", _TrackScore_get_driftFts_distanceError } },
    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / maxDistance:float
    { "driftFts.maxDistance", { "max distance (cm) from the wire; typically a wire\u0027s stride", _TrackScore_get_driftFts_maxDistance } },
    // no std getter for TrackScore/driftFts/wireCoordinates
    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitUError:float
    { "driftFts.sideHitUError", { "unbiased error on other side of isochrone", _TrackScore_get_driftFts_sideHitUError } },
    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitBError:float
    { "driftFts.sideHitBError", { "biased error on other side of isochrone", _TrackScore_get_driftFts_sideHitBError } },
    // propagated from  / driftFts:mem::Ref<DriftDetScoreFeatures> / sideWeight:float
    { "driftFts.sideWeight", { "tracking weight of other side isochrone", _TrackScore_get_driftFts_sideWeight } },
    // no std getter for TrackScore/driftFts/uError


    { "time", { "Hit time, global to event (averaged on hits or provided by MC)", _TrackScore_get_time } },


    // no std getter for TrackScore/mcTruth/globalPosition
    // propagated from  / mcTruth:mem::Ref<MCTrueTrackScore> / geant4TrackID:int
    { "mcTruth.geant4TrackID", { "(only for MC) Geant4 track ID for this score", _TrackScore_get_mcTruth_geant4TrackID } },



    { "u", { "Local U of the measurement", _TrackScore_get_u } },
    { "v", { "Local V of the measurement", _TrackScore_get_v } },
    { "w", { "Local W of the measurement", _TrackScore_get_w } },
    { "uE", { "Local uncertainty by U of the measurement", _TrackScore_get_uE } },
    { "vE", { "Local uncertainty by V of the measurement", _TrackScore_get_vE } },
    { "wE", { "Local uncertainty by W of the measurement", _TrackScore_get_wE } },

};






// no std getter for ScoreFitInfo/score/hitRefs
// no std getter for ScoreFitInfo/score/lR
// no std getter for ScoreFitInfo/score/gR
// no std getter for ScoreFitInfo/score/lRErr


// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / distance:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_distance( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.distance);  // simple value
    
}

// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / distanceError:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_distanceError( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.distanceError);  // simple value
    
}

// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / maxDistance:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_maxDistance( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.maxDistance);  // simple value
    
}

// no std getter for ScoreFitInfo/score/driftFts/wireCoordinates
// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitUError:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_sideHitUError( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideHitUError);  // simple value
    
}

// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitBError:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_sideHitBError( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideHitBError);  // simple value
    
}

// propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideWeight:float
static StdFloat_t _ScoreFitInfo_get_score_driftFts_sideWeight( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.driftFts) return std::nan("0");
    const auto & driftFts = *score.driftFts;
    const auto & obj = driftFts;
    
    return static_cast<StdFloat_t>(obj.sideWeight);  // simple value
    
}

// no std getter for ScoreFitInfo/score/driftFts/uError


// propagated from  / score:mem::Ref<TrackScore> / time:float
static StdFloat_t _ScoreFitInfo_get_score_time( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    
    return static_cast<StdFloat_t>(obj.time);  // simple value
    
}



// no std getter for ScoreFitInfo/score/mcTruth/globalPosition
// propagated from  / score:mem::Ref<TrackScore> / mcTruth:mem::Ref<MCTrueTrackScore> / geant4TrackID:int
static StdFloat_t _ScoreFitInfo_get_score_mcTruth_geant4TrackID( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    if(!score.mcTruth) return std::nan("0");
    const auto & mcTruth = *score.mcTruth;
    const auto & obj = mcTruth;
    
    return static_cast<StdFloat_t>(obj.geant4TrackID);  // simple value
    
}




// propagated from  / score:mem::Ref<TrackScore> / u:
static StdFloat_t _ScoreFitInfo_get_score_u( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lR[0];  // custom getter
}
// propagated from  / score:mem::Ref<TrackScore> / v:
static StdFloat_t _ScoreFitInfo_get_score_v( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lR[1];  // custom getter
}
// propagated from  / score:mem::Ref<TrackScore> / w:
static StdFloat_t _ScoreFitInfo_get_score_w( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lR[2];  // custom getter
}
// propagated from  / score:mem::Ref<TrackScore> / uE:
static StdFloat_t _ScoreFitInfo_get_score_uE( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lRErr[0];  // custom getter
}
// propagated from  / score:mem::Ref<TrackScore> / vE:
static StdFloat_t _ScoreFitInfo_get_score_vE( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lRErr[1];  // custom getter
}
// propagated from  / score:mem::Ref<TrackScore> / wE:
static StdFloat_t _ScoreFitInfo_get_score_wE( const ScoreFitInfo & obj_ ) {
    if(!obj_.score) return std::nan("0");
    const auto & score = *obj_.score;
    const auto & obj = score;
    return obj.lRErr[2];  // custom getter
}

// getter for ScoreFitInfo::lR disabled
// getter for ScoreFitInfo::lRUErr disabled
// getter for ScoreFitInfo::lRBErr disabled
// getter for ScoreFitInfo::lTan disabled
static StdFloat_t _ScoreFitInfo_get_qop( const ScoreFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.qop);  // simple value
}
static StdFloat_t _ScoreFitInfo_get_time( const ScoreFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _ScoreFitInfo_get_weight( const ScoreFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.weight);  // simple value
}

static StdFloat_t _ScoreFitInfo_get_biasedResidualU( const ScoreFitInfo & obj ) { return obj.lRBErr[0]; }
static StdFloat_t _ScoreFitInfo_get_biasedResidualV( const ScoreFitInfo & obj ) { return obj.lRBErr[1]; }
static StdFloat_t _ScoreFitInfo_get_biasedResidualW( const ScoreFitInfo & obj ) { return obj.lRBErr[2]; }
static StdFloat_t _ScoreFitInfo_get_unbiasedResidualU( const ScoreFitInfo & obj ) { return obj.lRUErr[0]; }
static StdFloat_t _ScoreFitInfo_get_unbiasedResidualV( const ScoreFitInfo & obj ) { return obj.lRUErr[1]; }
static StdFloat_t _ScoreFitInfo_get_unbiasedResidualW( const ScoreFitInfo & obj ) { return obj.lRUErr[2]; }
static StdFloat_t _ScoreFitInfo_get_tanU( const ScoreFitInfo & obj ) { return obj.lTan[0]; }
static StdFloat_t _ScoreFitInfo_get_tanV( const ScoreFitInfo & obj ) { return obj.lTan[1]; }
static StdFloat_t _ScoreFitInfo_get_tanW( const ScoreFitInfo & obj ) { return obj.lTan[2]; }
static StdFloat_t _ScoreFitInfo_get_u( const ScoreFitInfo & obj ) { return obj.lR[0]; }
static StdFloat_t _ScoreFitInfo_get_v( const ScoreFitInfo & obj ) { return obj.lR[1]; }
static StdFloat_t _ScoreFitInfo_get_w( const ScoreFitInfo & obj ) { return obj.lR[2]; }


const Traits<ScoreFitInfo>::Getters
Traits<ScoreFitInfo>::getters = {
    


    // no std getter for ScoreFitInfo/score/hitRefs
    // no std getter for ScoreFitInfo/score/lR
    // no std getter for ScoreFitInfo/score/gR
    // no std getter for ScoreFitInfo/score/lRErr


    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / distance:float
    { "score.driftFts.distance", { "Estimated distance, cm", _ScoreFitInfo_get_score_driftFts_distance } },
    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / distanceError:float
    { "score.driftFts.distanceError", { "Error of distance estimation, cm", _ScoreFitInfo_get_score_driftFts_distanceError } },
    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / maxDistance:float
    { "score.driftFts.maxDistance", { "max distance (cm) from the wire; typically a wire\u0027s stride", _ScoreFitInfo_get_score_driftFts_maxDistance } },
    // no std getter for ScoreFitInfo/score/driftFts/wireCoordinates
    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitUError:float
    { "score.driftFts.sideHitUError", { "unbiased error on other side of isochrone", _ScoreFitInfo_get_score_driftFts_sideHitUError } },
    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideHitBError:float
    { "score.driftFts.sideHitBError", { "biased error on other side of isochrone", _ScoreFitInfo_get_score_driftFts_sideHitBError } },
    // propagated from  / score:mem::Ref<TrackScore> / driftFts:mem::Ref<DriftDetScoreFeatures> / sideWeight:float
    { "score.driftFts.sideWeight", { "tracking weight of other side isochrone", _ScoreFitInfo_get_score_driftFts_sideWeight } },
    // no std getter for ScoreFitInfo/score/driftFts/uError


    // propagated from  / score:mem::Ref<TrackScore> / time:float
    { "score.time", { "Hit time, global to event (averaged on hits or provided by MC)", _ScoreFitInfo_get_score_time } },


    // no std getter for ScoreFitInfo/score/mcTruth/globalPosition
    // propagated from  / score:mem::Ref<TrackScore> / mcTruth:mem::Ref<MCTrueTrackScore> / geant4TrackID:int
    { "score.mcTruth.geant4TrackID", { "(only for MC) Geant4 track ID for this score", _ScoreFitInfo_get_score_mcTruth_geant4TrackID } },



    // propagated from  / score:mem::Ref<TrackScore> / u:
    { "score.u", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / u:", _ScoreFitInfo_get_score_u } },
    // propagated from  / score:mem::Ref<TrackScore> / v:
    { "score.v", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / v:", _ScoreFitInfo_get_score_v } },
    // propagated from  / score:mem::Ref<TrackScore> / w:
    { "score.w", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / w:", _ScoreFitInfo_get_score_w } },
    // propagated from  / score:mem::Ref<TrackScore> / uE:
    { "score.uE", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / uE:", _ScoreFitInfo_get_score_uE } },
    // propagated from  / score:mem::Ref<TrackScore> / vE:
    { "score.vE", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / vE:", _ScoreFitInfo_get_score_vE } },
    // propagated from  / score:mem::Ref<TrackScore> / wE:
    { "score.wE", { "propagated from  / score:mem::Ref\u003cTrackScore\u003e / wE:", _ScoreFitInfo_get_score_wE } },


    // getter for ScoreFitInfo::lR disabled

    // getter for ScoreFitInfo::lRUErr disabled

    // getter for ScoreFitInfo::lRBErr disabled

    // getter for ScoreFitInfo::lTan disabled
    { "qop", { "charge over momentum, [GeV/c]", _ScoreFitInfo_get_qop } },
    { "time", { "estimated fit time, [TODO: GenFit2 RK units?]", _ScoreFitInfo_get_time } },
    { "weight", { "weight for track fit, if appliable", _ScoreFitInfo_get_weight } },

    { "biasedResidualU", { "Biased residual by U, cm", _ScoreFitInfo_get_biasedResidualU } },
    { "biasedResidualV", { "Biased residual by V, cm", _ScoreFitInfo_get_biasedResidualV } },
    { "biasedResidualW", { "Biased residual by W, cm", _ScoreFitInfo_get_biasedResidualW } },
    { "unbiasedResidualU", { "Unbiased residual by U, cm", _ScoreFitInfo_get_unbiasedResidualU } },
    { "unbiasedResidualV", { "Unbiased residual by V, cm", _ScoreFitInfo_get_unbiasedResidualV } },
    { "unbiasedResidualW", { "Unbiased residual by W, cm", _ScoreFitInfo_get_unbiasedResidualW } },
    { "tanU", { "First DRS tangent of track at intersection point", _ScoreFitInfo_get_tanU } },
    { "tanV", { "Second DRS tangent of track at intersection point", _ScoreFitInfo_get_tanV } },
    { "tanW", { "Third DRS tangent of track at intersection point", _ScoreFitInfo_get_tanW } },
    { "u", { "First local coordinate, cm", _ScoreFitInfo_get_u } },
    { "v", { "Second local coordinate, cm", _ScoreFitInfo_get_v } },
    { "w", { "Third local coordinate, cm", _ScoreFitInfo_get_w } },

};




// getter for TrackFitInfo::positionSeed disabled
// getter for TrackFitInfo::momentumSeed disabled
// getter for TrackFitInfo::covarSeed disabled
static StdFloat_t _TrackFitInfo_get_chi2( const TrackFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.chi2);  // simple value
}
static StdFloat_t _TrackFitInfo_get_ndf( const TrackFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.ndf);  // simple value
}
static StdFloat_t _TrackFitInfo_get_pval( const TrackFitInfo & obj ) {
    return static_cast<StdFloat_t>(obj.pval);  // simple value
}

static StdFloat_t _TrackFitInfo_get_chi2ndf( const TrackFitInfo & obj ) { return obj.chi2/obj.ndf; }


const Traits<TrackFitInfo>::Getters
Traits<TrackFitInfo>::getters = {
    

    // getter for TrackFitInfo::positionSeed disabled

    // getter for TrackFitInfo::momentumSeed disabled

    // getter for TrackFitInfo::covarSeed disabled
    { "chi2", { "chi-squared value of track fit", _TrackFitInfo_get_chi2 } },
    { "ndf", { "Number of degrees of freedom of track fit", _TrackFitInfo_get_ndf } },
    { "pval", { "p-value of track fitting", _TrackFitInfo_get_pval } },

    { "chi2ndf", { "chi^2/NDF value", _TrackFitInfo_get_chi2ndf } },

};




// getter for Track::scores disabled
static StdFloat_t _Track_get_momentum( const Track & obj ) {
    return static_cast<StdFloat_t>(obj.momentum);  // simple value
}
static StdFloat_t _Track_get_pdg( const Track & obj ) {
    return static_cast<StdFloat_t>(obj.pdg);  // simple value
}


// no std getter for Track/fitInfo/positionSeed
// no std getter for Track/fitInfo/momentumSeed
// no std getter for Track/fitInfo/covarSeed
// propagated from  / fitInfo:mem::Ref<TrackFitInfo> / chi2:float
static StdFloat_t _Track_get_fitInfo_chi2( const Track & obj_ ) {
    if(!obj_.fitInfo) return std::nan("0");
    const auto & fitInfo = *obj_.fitInfo;
    const auto & obj = fitInfo;
    
    return static_cast<StdFloat_t>(obj.chi2);  // simple value
    
}

// propagated from  / fitInfo:mem::Ref<TrackFitInfo> / ndf:int32_t
static StdFloat_t _Track_get_fitInfo_ndf( const Track & obj_ ) {
    if(!obj_.fitInfo) return std::nan("0");
    const auto & fitInfo = *obj_.fitInfo;
    const auto & obj = fitInfo;
    
    return static_cast<StdFloat_t>(obj.ndf);  // simple value
    
}

// propagated from  / fitInfo:mem::Ref<TrackFitInfo> / pval:float
static StdFloat_t _Track_get_fitInfo_pval( const Track & obj_ ) {
    if(!obj_.fitInfo) return std::nan("0");
    const auto & fitInfo = *obj_.fitInfo;
    const auto & obj = fitInfo;
    
    return static_cast<StdFloat_t>(obj.pval);  // simple value
    
}


// propagated from  / fitInfo:mem::Ref<TrackFitInfo> / chi2ndf:
static StdFloat_t _Track_get_fitInfo_chi2ndf( const Track & obj_ ) {
    if(!obj_.fitInfo) return std::nan("0");
    const auto & fitInfo = *obj_.fitInfo;
    const auto & obj = fitInfo;
    return obj.chi2/obj.ndf;  // custom getter
}

static StdFloat_t _Track_get_zonePattern( const Track & obj ) {
    return static_cast<StdFloat_t>(obj.zonePattern);  // simple value
}

static StdFloat_t _Track_get_nscores( const Track & obj ) { return obj.scores.size(); }


const Traits<Track>::Getters
Traits<Track>::getters = {
    

    // getter for Track::scores disabled
    { "momentum", { "Initial momentum of the track", _Track_get_momentum } },
    { "pdg", { "Particle type PDG code", _Track_get_pdg } },


    // no std getter for Track/fitInfo/positionSeed
    // no std getter for Track/fitInfo/momentumSeed
    // no std getter for Track/fitInfo/covarSeed
    // propagated from  / fitInfo:mem::Ref<TrackFitInfo> / chi2:float
    { "fitInfo.chi2", { "chi-squared value of track fit", _Track_get_fitInfo_chi2 } },
    // propagated from  / fitInfo:mem::Ref<TrackFitInfo> / ndf:int32_t
    { "fitInfo.ndf", { "Number of degrees of freedom of track fit", _Track_get_fitInfo_ndf } },
    // propagated from  / fitInfo:mem::Ref<TrackFitInfo> / pval:float
    { "fitInfo.pval", { "p-value of track fitting", _Track_get_fitInfo_pval } },

    // propagated from  / fitInfo:mem::Ref<TrackFitInfo> / chi2ndf:
    { "fitInfo.chi2ndf", { "propagated from  / fitInfo:mem::Ref\u003cTrackFitInfo\u003e / chi2ndf:", _Track_get_fitInfo_chi2ndf } },

    { "zonePattern", { "Bitmask of zones for the track", _Track_get_zonePattern } },

    { "nscores", { "Number of scores in a track.", _Track_get_nscores } },

};



}  // namespace ::na64dp::event
}  // namespace na64dp
