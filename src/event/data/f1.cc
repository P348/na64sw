/**\file
 * \brief F1 TDC hit
 * 
 * Representation of hit on the F1-chip based detector
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/f1.hh"

#include "na64event/data/f1.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {





namespace event {
F1Hit::F1Hit( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::RawDataF1 & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.sourceID);
    
    ::na64dp::util::reset(obj.portID);
    
    ::na64dp::util::reset(obj.channel);
    
    ::na64dp::util::reset(obj.channelPosition);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeUnit);
    
    ::na64dp::util::reset(obj.timeDecoded);
    
    ::na64dp::util::reset(obj.timeReference);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::F1Hit & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.rawData);
    
    ::na64dp::util::reset(obj.hitPosition);
    
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _RawDataF1_get_sourceID( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.sourceID);  // simple value
}
static StdFloat_t _RawDataF1_get_portID( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.portID);  // simple value
}
static StdFloat_t _RawDataF1_get_channel( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.channel);  // simple value
}
static StdFloat_t _RawDataF1_get_channelPosition( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.channelPosition);  // simple value
}
static StdFloat_t _RawDataF1_get_time( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _RawDataF1_get_timeUnit( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.timeUnit);  // simple value
}
static StdFloat_t _RawDataF1_get_timeDecoded( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.timeDecoded);  // simple value
}
static StdFloat_t _RawDataF1_get_timeReference( const RawDataF1 & obj ) {
    return static_cast<StdFloat_t>(obj.timeReference);  // simple value
}



const Traits<RawDataF1>::Getters
Traits<RawDataF1>::getters = {
    
    { "sourceID", { "DAQ channel source ID", _RawDataF1_get_sourceID } },
    { "portID", { "DAQ port ID", _RawDataF1_get_portID } },
    { "channel", { "DAQ detector channel number", _RawDataF1_get_channel } },
    { "channelPosition", { "channel position", _RawDataF1_get_channelPosition } },
    { "time", { "Time associated with the channel", _RawDataF1_get_time } },
    { "timeUnit", { "Time unit", _RawDataF1_get_timeUnit } },
    { "timeDecoded", { "time in ns wrt trigger time", _RawDataF1_get_timeDecoded } },
    { "timeReference", { "...", _RawDataF1_get_timeReference } },


};






// propagated from  / rawData:mem::Ref<RawDataF1> / sourceID:uint16_t
static StdFloat_t _F1Hit_get_rawData_sourceID( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.sourceID);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / portID:uint16_t
static StdFloat_t _F1Hit_get_rawData_portID( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.portID);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / channel:int32_t
static StdFloat_t _F1Hit_get_rawData_channel( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.channel);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / channelPosition:int32_t
static StdFloat_t _F1Hit_get_rawData_channelPosition( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.channelPosition);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / time:int32_t
static StdFloat_t _F1Hit_get_rawData_time( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.time);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / timeUnit:double
static StdFloat_t _F1Hit_get_rawData_timeUnit( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.timeUnit);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / timeDecoded:double
static StdFloat_t _F1Hit_get_rawData_timeDecoded( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.timeDecoded);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataF1> / timeReference:double
static StdFloat_t _F1Hit_get_rawData_timeReference( const F1Hit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.timeReference);  // simple value
    
}



static StdFloat_t _F1Hit_get_hitPosition( const F1Hit & obj ) {
    return static_cast<StdFloat_t>(obj.hitPosition);  // simple value
}



const Traits<F1Hit>::Getters
Traits<F1Hit>::getters = {
    


    // propagated from  / rawData:mem::Ref<RawDataF1> / sourceID:uint16_t
    { "rawData.sourceID", { "DAQ channel source ID", _F1Hit_get_rawData_sourceID } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / portID:uint16_t
    { "rawData.portID", { "DAQ port ID", _F1Hit_get_rawData_portID } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / channel:int32_t
    { "rawData.channel", { "DAQ detector channel number", _F1Hit_get_rawData_channel } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / channelPosition:int32_t
    { "rawData.channelPosition", { "channel position", _F1Hit_get_rawData_channelPosition } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / time:int32_t
    { "rawData.time", { "Time associated with the channel", _F1Hit_get_rawData_time } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / timeUnit:double
    { "rawData.timeUnit", { "Time unit", _F1Hit_get_rawData_timeUnit } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / timeDecoded:double
    { "rawData.timeDecoded", { "time in ns wrt trigger time", _F1Hit_get_rawData_timeDecoded } },
    // propagated from  / rawData:mem::Ref<RawDataF1> / timeReference:double
    { "rawData.timeReference", { "...", _F1Hit_get_rawData_timeReference } },


    { "hitPosition", { "Reconstructed position of hit", _F1Hit_get_hitPosition } },


};



}  // namespace ::na64dp::event
}  // namespace na64dp
