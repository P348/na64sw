/**\file
 * \brief Declarations for calorimetry data
 * 
 * The calorimeter assembly energy deposition sum. May be not unique on event
 * to facilitate pile-up separation.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/calo.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {



namespace event {
CaloHit::CaloHit( LocalMemory & lmem )
     : hits(lmem)
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::CaloHit & obj ) {
    using namespace event;
    
    obj.hits.clear();
    ::na64dp::util::reset(obj.timeClusterLabel);
    
    ::na64dp::util::reset(obj.eDep);
    
    ::na64dp::util::reset(obj.eDepError);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeStddev);
    
}
}  // namespace ::na64dp::util


namespace event {




// getter for CaloHit::hits disabled
static StdFloat_t _CaloHit_get_timeClusterLabel( const CaloHit & obj ) {
    return static_cast<StdFloat_t>(obj.timeClusterLabel);  // simple value
}
static StdFloat_t _CaloHit_get_eDep( const CaloHit & obj ) {
    return static_cast<StdFloat_t>(obj.eDep);  // simple value
}
static StdFloat_t _CaloHit_get_eDepError( const CaloHit & obj ) {
    return static_cast<StdFloat_t>(obj.eDepError);  // simple value
}
static StdFloat_t _CaloHit_get_time( const CaloHit & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _CaloHit_get_timeStddev( const CaloHit & obj ) {
    return static_cast<StdFloat_t>(obj.timeStddev);  // simple value
}

static StdFloat_t _CaloHit_get_nHits( const CaloHit & obj ) { return obj.hits.size(); }


const Traits<CaloHit>::Getters
Traits<CaloHit>::getters = {
    

    // getter for CaloHit::hits disabled
    { "timeClusterLabel", { "Time cluster label, may be not set", _CaloHit_get_timeClusterLabel } },
    { "eDep", { "Energy deposition sum, MeV", _CaloHit_get_eDep } },
    { "eDepError", { "Estimated energy deposition\u0027s error", _CaloHit_get_eDepError } },
    { "time", { "Mean time", _CaloHit_get_time } },
    { "timeStddev", { "Time standard deviation (of hits)", _CaloHit_get_timeStddev } },

    { "nHits", { "Returns number of SADC hits", _CaloHit_get_nHits } },

};



}  // namespace ::na64dp::event
}  // namespace na64dp
