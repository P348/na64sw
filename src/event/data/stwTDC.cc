/**\file
 * STRAW detectors are used for tracking in NA64. They are listened by a
 * dedicated NA64-specific DAQ entity (called "NA64TDC").
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1.dev
 */


#include "na64event/data/stwTDC.hh"

#include "na64event/data/stwTDC.hh"

#include "na64event/data/stwTDC.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {







namespace event {
StwTDCHit::StwTDCHit( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::StwTDCRawData & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.wireNo);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeDecoded);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::StwMCInfo & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.entryPoint);
    
    ::na64dp::util::reset(obj.exitPoint);
    
    ::na64dp::util::reset(obj.eDep);
    
    ::na64dp::util::reset(obj.trackE);
    
    ::na64dp::util::reset(obj.particlePDG);
    
    ::na64dp::util::reset(obj.geant4TrackID);
    
    ::na64dp::util::reset(obj.r);
    
    ::na64dp::util::reset(obj.u);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::StwTDCHit & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.rawData);
    
    ::na64dp::util::reset(obj.distance);
    
    ::na64dp::util::reset(obj.distanceError);
    
    ::na64dp::util::reset(obj.mcInfo);
    
    ::na64dp::util::reset(obj.correctedTime);
    
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _StwTDCRawData_get_wireNo( const StwTDCRawData & obj ) {
    return static_cast<StdFloat_t>(obj.wireNo);  // simple value
}
static StdFloat_t _StwTDCRawData_get_time( const StwTDCRawData & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _StwTDCRawData_get_timeDecoded( const StwTDCRawData & obj ) {
    return static_cast<StdFloat_t>(obj.timeDecoded);  // simple value
}



const Traits<StwTDCRawData>::Getters
Traits<StwTDCRawData>::getters = {
    
    { "wireNo", { "Triggered wire number (physical, decoded)", _StwTDCRawData_get_wireNo } },
    { "time", { "Time on a wire (internal units)", _StwTDCRawData_get_time } },
    { "timeDecoded", { "Time in ns with the respect to trigger time", _StwTDCRawData_get_timeDecoded } },


};




// getter for StwMCInfo::entryPoint disabled
// getter for StwMCInfo::exitPoint disabled
static StdFloat_t _StwMCInfo_get_eDep( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.eDep);  // simple value
}
static StdFloat_t _StwMCInfo_get_trackE( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.trackE);  // simple value
}
static StdFloat_t _StwMCInfo_get_particlePDG( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.particlePDG);  // simple value
}
static StdFloat_t _StwMCInfo_get_geant4TrackID( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.geant4TrackID);  // simple value
}
static StdFloat_t _StwMCInfo_get_r( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.r);  // simple value
}
static StdFloat_t _StwMCInfo_get_u( const StwMCInfo & obj ) {
    return static_cast<StdFloat_t>(obj.u);  // simple value
}



const Traits<StwMCInfo>::Getters
Traits<StwMCInfo>::getters = {
    

    // getter for StwMCInfo::entryPoint disabled

    // getter for StwMCInfo::exitPoint disabled
    { "eDep", { "energy deposition of incident particle", _StwMCInfo_get_eDep } },
    { "trackE", { "energy of initiating track", _StwMCInfo_get_trackE } },
    { "particlePDG", { "PDG code of initiating particle", _StwMCInfo_get_particlePDG } },
    { "geant4TrackID", { "Geant4 track ID", _StwMCInfo_get_geant4TrackID } },
    { "r", { "distance according to MC geometry", _StwMCInfo_get_r } },
    { "u", { "measured (local) coordinate according to MC geometry", _StwMCInfo_get_u } },


};






// propagated from  / rawData:mem::Ref<StwTDCRawData> / wireNo:uint16_t
static StdFloat_t _StwTDCHit_get_rawData_wireNo( const StwTDCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.wireNo);  // simple value
    
}

// propagated from  / rawData:mem::Ref<StwTDCRawData> / time:int16_t
static StdFloat_t _StwTDCHit_get_rawData_time( const StwTDCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.time);  // simple value
    
}

// propagated from  / rawData:mem::Ref<StwTDCRawData> / timeDecoded:double
static StdFloat_t _StwTDCHit_get_rawData_timeDecoded( const StwTDCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.timeDecoded);  // simple value
    
}



static StdFloat_t _StwTDCHit_get_distance( const StwTDCHit & obj ) {
    return static_cast<StdFloat_t>(obj.distance);  // simple value
}
static StdFloat_t _StwTDCHit_get_distanceError( const StwTDCHit & obj ) {
    return static_cast<StdFloat_t>(obj.distanceError);  // simple value
}


// no std getter for StwTDCHit/mcInfo/entryPoint
// no std getter for StwTDCHit/mcInfo/exitPoint
// propagated from  / mcInfo:mem::Ref<StwMCInfo> / eDep:double
static StdFloat_t _StwTDCHit_get_mcInfo_eDep( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.eDep);  // simple value
    
}

// propagated from  / mcInfo:mem::Ref<StwMCInfo> / trackE:float
static StdFloat_t _StwTDCHit_get_mcInfo_trackE( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.trackE);  // simple value
    
}

// propagated from  / mcInfo:mem::Ref<StwMCInfo> / particlePDG:int
static StdFloat_t _StwTDCHit_get_mcInfo_particlePDG( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.particlePDG);  // simple value
    
}

// propagated from  / mcInfo:mem::Ref<StwMCInfo> / geant4TrackID:int
static StdFloat_t _StwTDCHit_get_mcInfo_geant4TrackID( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.geant4TrackID);  // simple value
    
}

// propagated from  / mcInfo:mem::Ref<StwMCInfo> / r:float
static StdFloat_t _StwTDCHit_get_mcInfo_r( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.r);  // simple value
    
}

// propagated from  / mcInfo:mem::Ref<StwMCInfo> / u:float
static StdFloat_t _StwTDCHit_get_mcInfo_u( const StwTDCHit & obj_ ) {
    if(!obj_.mcInfo) return std::nan("0");
    const auto & mcInfo = *obj_.mcInfo;
    const auto & obj = mcInfo;
    
    return static_cast<StdFloat_t>(obj.u);  // simple value
    
}



static StdFloat_t _StwTDCHit_get_correctedTime( const StwTDCHit & obj ) {
    return static_cast<StdFloat_t>(obj.correctedTime);  // simple value
}



const Traits<StwTDCHit>::Getters
Traits<StwTDCHit>::getters = {
    


    // propagated from  / rawData:mem::Ref<StwTDCRawData> / wireNo:uint16_t
    { "rawData.wireNo", { "Triggered wire number (physical, decoded)", _StwTDCHit_get_rawData_wireNo } },
    // propagated from  / rawData:mem::Ref<StwTDCRawData> / time:int16_t
    { "rawData.time", { "Time on a wire (internal units)", _StwTDCHit_get_rawData_time } },
    // propagated from  / rawData:mem::Ref<StwTDCRawData> / timeDecoded:double
    { "rawData.timeDecoded", { "Time in ns with the respect to trigger time", _StwTDCHit_get_rawData_timeDecoded } },


    { "distance", { "estimated by time (ambiguous) distance from the wire (isochrone surface radius)", _StwTDCHit_get_distance } },
    { "distanceError", { "error of distance estimation", _StwTDCHit_get_distanceError } },


    // no std getter for StwTDCHit/mcInfo/entryPoint
    // no std getter for StwTDCHit/mcInfo/exitPoint
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / eDep:double
    { "mcInfo.eDep", { "energy deposition of incident particle", _StwTDCHit_get_mcInfo_eDep } },
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / trackE:float
    { "mcInfo.trackE", { "energy of initiating track", _StwTDCHit_get_mcInfo_trackE } },
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / particlePDG:int
    { "mcInfo.particlePDG", { "PDG code of initiating particle", _StwTDCHit_get_mcInfo_particlePDG } },
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / geant4TrackID:int
    { "mcInfo.geant4TrackID", { "Geant4 track ID", _StwTDCHit_get_mcInfo_geant4TrackID } },
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / r:float
    { "mcInfo.r", { "distance according to MC geometry", _StwTDCHit_get_mcInfo_r } },
    // propagated from  / mcInfo:mem::Ref<StwMCInfo> / u:float
    { "mcInfo.u", { "measured (local) coordinate according to MC geometry", _StwTDCHit_get_mcInfo_u } },


    { "correctedTime", { "Time corrected for T0", _StwTDCHit_get_correctedTime } },


};



}  // namespace ::na64dp::event
}  // namespace na64dp
