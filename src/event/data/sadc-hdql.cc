#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for MoyalSADCWfFitParameters, GaussianSADCFitParameters, MSADCDFFTCoefficients, MSADCPeak, RawDataSADC, SADCHit.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1
 */

namespace na64dp {
namespace event {

void
define_sadc_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<MoyalSADCWfFitParameters>("MoyalSADCWfFitParameters")
        .attr<&MoyalSADCWfFitParameters::mu>("mu")
        .attr<&MoyalSADCWfFitParameters::sigma>("sigma")
        .attr<&MoyalSADCWfFitParameters::S>("S")
        .attr<&MoyalSADCWfFitParameters::interval>("interval")
    .end_compound();
    types.new_compound<GaussianSADCFitParameters>("GaussianSADCFitParameters")
        .attr<&GaussianSADCFitParameters::mu>("mu")
        .attr<&GaussianSADCFitParameters::sigma>("sigma")
        .attr<&GaussianSADCFitParameters::S>("S")
        .attr<&GaussianSADCFitParameters::interval>("interval")
    .end_compound();
    types.new_compound<MSADCDFFTCoefficients>("MSADCDFFTCoefficients")
        .attr<&MSADCDFFTCoefficients::real>("real")
        .attr<&MSADCDFFTCoefficients::imag>("imag")
    .end_compound();
    types.new_compound<MSADCPeak>("MSADCPeak")
        .attr<&MSADCPeak::time>("time")
        .attr<&MSADCPeak::timeErr>("timeErr")
        .attr<&MSADCPeak::timeClusterLabel>("timeClusterLabel")
        .attr<&MSADCPeak::amp>("amp")
        .attr<&MSADCPeak::ampErr>("ampErr")
        .attr<&MSADCPeak::sumCoarse>("sumCoarse")
        .attr<&MSADCPeak::sumFine>("sumFine")
        .attr<&MSADCPeak::sumFineErr>("sumFineErr")
        .attr<&MSADCPeak::chi2>("chi2")
        .attr<&MSADCPeak::ndf>("ndf")
        .attr<&MSADCPeak::lastFitStatusCode>("lastFitStatusCode")
        .attr<&MSADCPeak::moyalFit>("moyalFit")
    .end_compound();
    types.new_compound<RawDataSADC>("RawDataSADC")
        .attr<&RawDataSADC::wave>("wave")
        .attr<&RawDataSADC::fft>("fft")
        .attr<&RawDataSADC::pedestals>("pedestals")
        .attr<&RawDataSADC::sum>("sum")
        .attr<&RawDataSADC::maxSample>("maxSample")
        .attr<&RawDataSADC::maxAmp>("maxAmp")
        .attr<&RawDataSADC::maxAmpError>("maxAmpError")
        .attr<&RawDataSADC::canBeZero>("canBeZero")
        .attr<&RawDataSADC::ledCorr>("ledCorr")
        .attr<&RawDataSADC::calibCoeff>("calibCoeff")
        .attr<&RawDataSADC::ampThreshold>("ampThreshold")
        .attr<&RawDataSADC::maxima>("maxima")
    .end_compound();
    types.new_compound<SADCHit>("SADCHit")
        .attr<&SADCHit::rawData>("rawData")
        .attr<&SADCHit::eDep>("eDep")
        .attr<&SADCHit::eDepError>("eDepError")
        .attr<&SADCHit::time>("time")
        .attr<&SADCHit::timeError>("timeError")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
