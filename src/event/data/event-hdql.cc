#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for MCTruthInfo, Vertex, Event.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1
 */

namespace na64dp {
namespace event {

void
define_event_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<MCTruthInfo>("MCTruthInfo")
        .attr<&MCTruthInfo::weight>("weight")
        .attr<&MCTruthInfo::beamPosition>("beamPosition")
        .attr<&MCTruthInfo::beamMomentum>("beamMomentum")
        .attr<&MCTruthInfo::beamE0>("beamE0")
        .attr<&MCTruthInfo::ecalEntryPoisition>("ecalEntryPoisition")
        .attr<&MCTruthInfo::beamPDG>("beamPDG")
    .end_compound();
    types.new_compound<Vertex>("Vertex")
        .attr<&Vertex::E0>("E0")
        .attr<&Vertex::E>("E")
        .attr<&Vertex::position>("position")
        .attr<&Vertex::decayPosition>("decayPosition")
        .attr<&Vertex::lvEm>("lvEm")
        .attr<&Vertex::lvEp>("lvEp")
        .attr<&Vertex::initPDG>("initPDG")
        .attr<&Vertex::DMTRID1>("DMTRID1")
        .attr<&Vertex::DMTRID2>("DMTRID2")
    .end_compound();
    types.new_compound<Event>("Event")
        .attr<&Event::id>("id")
        .attr<&Event::trigger>("trigger")
        .attr<&Event::evType>("evType")
        .attr<&Event::time>("time")
        .attr<&Event::masterTime>("masterTime")
        .attr<&Event::mcTruth>("mcTruth")
        .attr<&Event::sadcHits>("sadcHits")
        .attr<&Event::apvHits>("apvHits")
        .attr<&Event::stwtdcHits>("stwtdcHits")
        .attr<&Event::f1Hits>("f1Hits")
        .attr<&Event::caloHits>("caloHits")
        .attr<&Event::apvClusters>("apvClusters")
        .attr<&Event::trackScores>("trackScores")
        .attr<&Event::vertices>("vertices")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
