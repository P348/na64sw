/**\file
 * \brief Main event structure declaration
 * 
 * The event structure is the principal declaration affecting every handler
 * within the pipeline. Handlers take and modify the data from/within an event
 * instance, by accessing its members.
 * 
 * Refer to :ref:`event structure` for insights of
 * how to modify the events composition.
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/event.hh"

#include "na64event/data/event.hh"

#include "na64event/data/event.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {







namespace event {
Event::Event( LocalMemory & lmem )
     : sadcHits(lmem)
     , apvHits(lmem)
     , stwtdcHits(lmem)
     , f1Hits(lmem)
     , caloHits(lmem)
     , apvClusters(lmem)
     , trackScores(lmem)
     , tracks(lmem)
     , vertices(lmem)
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::MCTruthInfo & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.weight);
    
    ::na64dp::util::reset(obj.beamPosition);
    
    ::na64dp::util::reset(obj.beamMomentum);
    
    ::na64dp::util::reset(obj.beamE0);
    
    ::na64dp::util::reset(obj.ecalEntryPoisition);
    
    ::na64dp::util::reset(obj.beamPDG);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::Vertex & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.E0);
    
    ::na64dp::util::reset(obj.E);
    
    ::na64dp::util::reset(obj.position);
    
    ::na64dp::util::reset(obj.decayPosition);
    
    ::na64dp::util::reset(obj.lvEm);
    
    ::na64dp::util::reset(obj.lvEp);
    
    ::na64dp::util::reset(obj.initPDG);
    
    ::na64dp::util::reset(obj.DMTRID1);
    
    ::na64dp::util::reset(obj.DMTRID2);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::Event & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.id);
    
    ::na64dp::util::reset(obj.trigger);
    
    ::na64dp::util::reset(obj.evType);
    obj.time = std::make_pair<time_t, uint32_t>(0, 0);
    ::na64dp::util::reset(obj.masterTime);
    
    ::na64dp::util::reset(obj.mcTruth);
    
    obj.sadcHits.clear();
    obj.apvHits.clear();
    obj.stwtdcHits.clear();
    obj.f1Hits.clear();
    obj.caloHits.clear();
    obj.apvClusters.clear();
    obj.trackScores.clear();
    obj.tracks.clear();
    obj.vertices.clear();
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _MCTruthInfo_get_weight( const MCTruthInfo & obj ) {
    return static_cast<StdFloat_t>(obj.weight);  // simple value
}
// getter for MCTruthInfo::beamPosition disabled
// getter for MCTruthInfo::beamMomentum disabled
static StdFloat_t _MCTruthInfo_get_beamE0( const MCTruthInfo & obj ) {
    return static_cast<StdFloat_t>(obj.beamE0);  // simple value
}
// getter for MCTruthInfo::ecalEntryPoisition disabled
static StdFloat_t _MCTruthInfo_get_beamPDG( const MCTruthInfo & obj ) {
    return static_cast<StdFloat_t>(obj.beamPDG);  // simple value
}



const Traits<MCTruthInfo>::Getters
Traits<MCTruthInfo>::getters = {
    
    { "weight", { "A \"statistical weight\" of the event", _MCTruthInfo_get_weight } },

    // getter for MCTruthInfo::beamPosition disabled

    // getter for MCTruthInfo::beamMomentum disabled
    { "beamE0", { "initializing particle initial full E (TODO: redundant?)", _MCTruthInfo_get_beamE0 } },

    // getter for MCTruthInfo::ecalEntryPoisition disabled
    { "beamPDG", { "PDG code of initializing particle", _MCTruthInfo_get_beamPDG } },


};




static StdFloat_t _Vertex_get_E0( const Vertex & obj ) {
    return static_cast<StdFloat_t>(obj.E0);  // simple value
}
static StdFloat_t _Vertex_get_E( const Vertex & obj ) {
    return static_cast<StdFloat_t>(obj.E);  // simple value
}
// getter for Vertex::position disabled
// getter for Vertex::decayPosition disabled
// getter for Vertex::lvEm disabled
// getter for Vertex::lvEp disabled
static StdFloat_t _Vertex_get_initPDG( const Vertex & obj ) {
    return static_cast<StdFloat_t>(obj.initPDG);  // simple value
}
static StdFloat_t _Vertex_get_DMTRID1( const Vertex & obj ) {
    return static_cast<StdFloat_t>(obj.DMTRID1);  // simple value
}
static StdFloat_t _Vertex_get_DMTRID2( const Vertex & obj ) {
    return static_cast<StdFloat_t>(obj.DMTRID2);  // simple value
}



const Traits<Vertex>::Getters
Traits<Vertex>::getters = {
    
    { "E0", { "Initializing particle energy before DM producton [GeV]", _Vertex_get_E0 } },
    { "E", { "Initializing paritcle energy after DM production [GeV]", _Vertex_get_E } },

    // getter for Vertex::position disabled

    // getter for Vertex::decayPosition disabled

    // getter for Vertex::lvEm disabled

    // getter for Vertex::lvEp disabled
    { "initPDG", { "PDG code of the initializing particle (MC typically)", _Vertex_get_initPDG } },
    { "DMTRID1", { "(unknown MK\u0027s MC datum) \\todo", _Vertex_get_DMTRID1 } },
    { "DMTRID2", { "(unknown MK\u0027s MC datum) \\todo", _Vertex_get_DMTRID2 } },


};




static StdFloat_t _Event_get_id( const Event & obj ) {
    return static_cast<StdFloat_t>(obj.id);  // simple value
}
static StdFloat_t _Event_get_trigger( const Event & obj ) {
    return static_cast<StdFloat_t>(obj.trigger);  // simple value
}
static StdFloat_t _Event_get_evType( const Event & obj ) {
    return static_cast<StdFloat_t>(obj.evType);  // simple value
}
// getter for Event::time disabled
static StdFloat_t _Event_get_masterTime( const Event & obj ) {
    return static_cast<StdFloat_t>(obj.masterTime);  // simple value
}


// propagated from  / mcTruth:mem::Ref<MCTruthInfo> / weight:double
static StdFloat_t _Event_get_mcTruth_weight( const Event & obj_ ) {
    if(!obj_.mcTruth) return std::nan("0");
    const auto & mcTruth = *obj_.mcTruth;
    const auto & obj = mcTruth;
    
    return static_cast<StdFloat_t>(obj.weight);  // simple value
    
}

// no std getter for Event/mcTruth/beamPosition
// no std getter for Event/mcTruth/beamMomentum
// propagated from  / mcTruth:mem::Ref<MCTruthInfo> / beamE0:double
static StdFloat_t _Event_get_mcTruth_beamE0( const Event & obj_ ) {
    if(!obj_.mcTruth) return std::nan("0");
    const auto & mcTruth = *obj_.mcTruth;
    const auto & obj = mcTruth;
    
    return static_cast<StdFloat_t>(obj.beamE0);  // simple value
    
}

// no std getter for Event/mcTruth/ecalEntryPoisition
// propagated from  / mcTruth:mem::Ref<MCTruthInfo> / beamPDG:int
static StdFloat_t _Event_get_mcTruth_beamPDG( const Event & obj_ ) {
    if(!obj_.mcTruth) return std::nan("0");
    const auto & mcTruth = *obj_.mcTruth;
    const auto & obj = mcTruth;
    
    return static_cast<StdFloat_t>(obj.beamPDG);  // simple value
    
}



// getter for Event::sadcHits disabled
// getter for Event::apvHits disabled
// getter for Event::stwtdcHits disabled
// getter for Event::f1Hits disabled
// getter for Event::caloHits disabled
// getter for Event::apvClusters disabled
// getter for Event::trackScores disabled
// getter for Event::tracks disabled
// getter for Event::vertices disabled



const Traits<Event>::Getters
Traits<Event>::getters = {
    
    { "id", { "Event\u0027s unique identifier.", _Event_get_id } },
    { "trigger", { "Trigger code of this event", _Event_get_trigger } },
    { "evType", { "Event type code", _Event_get_evType } },

    // getter for Event::time disabled
    { "masterTime", { "Relative event master time, [ns]", _Event_get_masterTime } },


    // propagated from  / mcTruth:mem::Ref<MCTruthInfo> / weight:double
    { "mcTruth.weight", { "A \"statistical weight\" of the event", _Event_get_mcTruth_weight } },
    // no std getter for Event/mcTruth/beamPosition
    // no std getter for Event/mcTruth/beamMomentum
    // propagated from  / mcTruth:mem::Ref<MCTruthInfo> / beamE0:double
    { "mcTruth.beamE0", { "initializing particle initial full E (TODO: redundant?)", _Event_get_mcTruth_beamE0 } },
    // no std getter for Event/mcTruth/ecalEntryPoisition
    // propagated from  / mcTruth:mem::Ref<MCTruthInfo> / beamPDG:int
    { "mcTruth.beamPDG", { "PDG code of initializing particle", _Event_get_mcTruth_beamPDG } },



    // getter for Event::sadcHits disabled

    // getter for Event::apvHits disabled

    // getter for Event::stwtdcHits disabled

    // getter for Event::f1Hits disabled

    // getter for Event::caloHits disabled

    // getter for Event::apvClusters disabled

    // getter for Event::trackScores disabled

    // getter for Event::tracks disabled

    // getter for Event::vertices disabled


};



}  // namespace ::na64dp::event
}  // namespace na64dp
