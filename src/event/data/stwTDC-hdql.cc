#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for StwTDCRawData, StwMCInfo, StwTDCHit.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl-hdql.cc
 * \version 0.1.dev
 */

namespace na64dp {
namespace event {

void
define_stwTDC_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
    types.new_compound<StwTDCRawData>("StwTDCRawData")
        .attr<&StwTDCRawData::wireNo>("wireNo")
        .attr<&StwTDCRawData::time>("time")
        .attr<&StwTDCRawData::timeDecoded>("timeDecoded")
    .end_compound();
    types.new_compound<StwMCInfo>("StwMCInfo")
        .attr<&StwMCInfo::entryPoint>("entryPoint")
        .attr<&StwMCInfo::exitPoint>("exitPoint")
        .attr<&StwMCInfo::eDep>("eDep")
        .attr<&StwMCInfo::trackE>("trackE")
        .attr<&StwMCInfo::particlePDG>("particlePDG")
        .attr<&StwMCInfo::geant4TrackID>("geant4TrackID")
        .attr<&StwMCInfo::r>("r")
        .attr<&StwMCInfo::u>("u")
    .end_compound();
    types.new_compound<StwTDCHit>("StwTDCHit")
        .attr<&StwTDCHit::rawData>("rawData")
        .attr<&StwTDCHit::distance>("distance")
        .attr<&StwTDCHit::distanceError>("distanceError")
        .attr<&StwTDCHit::mcInfo>("mcInfo")
        .attr<&StwTDCHit::correctedTime>("correctedTime")
    .end_compound();
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND
