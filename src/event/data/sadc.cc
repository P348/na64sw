/**\file
 * \brief Declarations for hits data on SADC managed detectors.
 * 
 * The NA64/NA58 standard sampling ADC chip (SADC) performs sampling
 * analog-to-digital conversion of many detectors signals. Once triggered, it
 * measures the voltage on its input 16 times with time interval of 25ns.
 * In NA64 DAQ a pair of chips are always enabled in parallel at a single
 * channel with phase shift of ~12.5ns providing 32 samples with 12.5ns
 * interval. Each chip has its own zero, but measurement scale is expected
 * to be the same for both.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/impl.cc
 * \version 0.1
 */


#include "na64event/data/sadc.hh"

#include "na64event/data/sadc.hh"

#include "na64event/data/sadc.hh"

#include "na64event/data/sadc.hh"

#include "na64event/data/sadc.hh"

#include "na64event/data/sadc.hh"

#include <cmath>
#include "na64event/reset-values.hh"

namespace na64dp {









namespace event {
MSADCPeak::MSADCPeak( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp



namespace event {
RawDataSADC::RawDataSADC( LocalMemory & lmem )
     : maxima(lmem)
    
{}
}  // namespace ::event::na64dp



namespace event {
SADCHit::SADCHit( LocalMemory & lmem )
    
{}
}  // namespace ::event::na64dp




namespace util {
void reset( event::MoyalSADCWfFitParameters & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.mu);
    
    ::na64dp::util::reset(obj.sigma);
    
    ::na64dp::util::reset(obj.S);
    
    ::na64dp::util::reset(obj.interval);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::GaussianSADCFitParameters & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.mu);
    
    ::na64dp::util::reset(obj.sigma);
    
    ::na64dp::util::reset(obj.S);
    
    ::na64dp::util::reset(obj.interval);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::MSADCDFFTCoefficients & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.real);
    
    ::na64dp::util::reset(obj.imag);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::MSADCPeak & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeErr);
    
    ::na64dp::util::reset(obj.timeClusterLabel);
    
    ::na64dp::util::reset(obj.amp);
    
    ::na64dp::util::reset(obj.ampErr);
    
    ::na64dp::util::reset(obj.sumCoarse);
    
    ::na64dp::util::reset(obj.sumFine);
    
    ::na64dp::util::reset(obj.sumFineErr);
    
    ::na64dp::util::reset(obj.chi2);
    
    ::na64dp::util::reset(obj.ndf);
    
    ::na64dp::util::reset(obj.lastFitStatusCode);
    
    ::na64dp::util::reset(obj.moyalFit);
    
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::RawDataSADC & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.wave);
    
    ::na64dp::util::reset(obj.fft);
    
    ::na64dp::util::reset(obj.pedestals);
    
    ::na64dp::util::reset(obj.sum);
    
    ::na64dp::util::reset(obj.maxSample);
    
    ::na64dp::util::reset(obj.maxAmp);
    
    ::na64dp::util::reset(obj.maxAmpError);
    
    ::na64dp::util::reset(obj.canBeZero);
    
    ::na64dp::util::reset(obj.ledCorr);
    
    ::na64dp::util::reset(obj.calibCoeff);
    
    ::na64dp::util::reset(obj.ampThreshold);
    
    obj.maxima.clear();
}
}  // namespace ::na64dp::util

namespace util {
void reset( event::SADCHit & obj ) {
    using namespace event;
    
    ::na64dp::util::reset(obj.rawData);
    
    ::na64dp::util::reset(obj.eDep);
    
    ::na64dp::util::reset(obj.eDepError);
    
    ::na64dp::util::reset(obj.time);
    
    ::na64dp::util::reset(obj.timeError);
    
}
}  // namespace ::na64dp::util


namespace event {




static StdFloat_t _MoyalSADCWfFitParameters_get_mu( const MoyalSADCWfFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.mu);  // simple value
}
static StdFloat_t _MoyalSADCWfFitParameters_get_sigma( const MoyalSADCWfFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.sigma);  // simple value
}
static StdFloat_t _MoyalSADCWfFitParameters_get_S( const MoyalSADCWfFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.S);  // simple value
}
// getter for MoyalSADCWfFitParameters::interval disabled



const Traits<MoyalSADCWfFitParameters>::Getters
Traits<MoyalSADCWfFitParameters>::getters = {
    
    { "mu", { "Centers of peaks", _MoyalSADCWfFitParameters_get_mu } },
    { "sigma", { "Widths of peaks", _MoyalSADCWfFitParameters_get_sigma } },
    { "S", { "Scaling factor", _MoyalSADCWfFitParameters_get_S } },

    // getter for MoyalSADCWfFitParameters::interval disabled


};




static StdFloat_t _GaussianSADCFitParameters_get_mu( const GaussianSADCFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.mu);  // simple value
}
static StdFloat_t _GaussianSADCFitParameters_get_sigma( const GaussianSADCFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.sigma);  // simple value
}
static StdFloat_t _GaussianSADCFitParameters_get_S( const GaussianSADCFitParameters & obj ) {
    return static_cast<StdFloat_t>(obj.S);  // simple value
}
// getter for GaussianSADCFitParameters::interval disabled



const Traits<GaussianSADCFitParameters>::Getters
Traits<GaussianSADCFitParameters>::getters = {
    
    { "mu", { "Centers of peaks", _GaussianSADCFitParameters_get_mu } },
    { "sigma", { "Widths of peaks", _GaussianSADCFitParameters_get_sigma } },
    { "S", { "Scaling factor", _GaussianSADCFitParameters_get_S } },

    // getter for GaussianSADCFitParameters::interval disabled


};




// getter for MSADCDFFTCoefficients::real disabled
// getter for MSADCDFFTCoefficients::imag disabled



const Traits<MSADCDFFTCoefficients>::Getters
Traits<MSADCDFFTCoefficients>::getters = {
    

    // getter for MSADCDFFTCoefficients::real disabled

    // getter for MSADCDFFTCoefficients::imag disabled


};




static StdFloat_t _MSADCPeak_get_time( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _MSADCPeak_get_timeErr( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.timeErr);  // simple value
}
static StdFloat_t _MSADCPeak_get_timeClusterLabel( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.timeClusterLabel);  // simple value
}
static StdFloat_t _MSADCPeak_get_amp( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.amp);  // simple value
}
static StdFloat_t _MSADCPeak_get_ampErr( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.ampErr);  // simple value
}
static StdFloat_t _MSADCPeak_get_sumCoarse( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.sumCoarse);  // simple value
}
static StdFloat_t _MSADCPeak_get_sumFine( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.sumFine);  // simple value
}
static StdFloat_t _MSADCPeak_get_sumFineErr( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.sumFineErr);  // simple value
}
static StdFloat_t _MSADCPeak_get_chi2( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.chi2);  // simple value
}
static StdFloat_t _MSADCPeak_get_ndf( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.ndf);  // simple value
}
static StdFloat_t _MSADCPeak_get_lastFitStatusCode( const MSADCPeak & obj ) {
    return static_cast<StdFloat_t>(obj.lastFitStatusCode);  // simple value
}


// propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / mu:float
static StdFloat_t _MSADCPeak_get_moyalFit_mu( const MSADCPeak & obj_ ) {
    if(!obj_.moyalFit) return std::nan("0");
    const auto & moyalFit = *obj_.moyalFit;
    const auto & obj = moyalFit;
    
    return static_cast<StdFloat_t>(obj.mu);  // simple value
    
}

// propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / sigma:float
static StdFloat_t _MSADCPeak_get_moyalFit_sigma( const MSADCPeak & obj_ ) {
    if(!obj_.moyalFit) return std::nan("0");
    const auto & moyalFit = *obj_.moyalFit;
    const auto & obj = moyalFit;
    
    return static_cast<StdFloat_t>(obj.sigma);  // simple value
    
}

// propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / S:float
static StdFloat_t _MSADCPeak_get_moyalFit_S( const MSADCPeak & obj_ ) {
    if(!obj_.moyalFit) return std::nan("0");
    const auto & moyalFit = *obj_.moyalFit;
    const auto & obj = moyalFit;
    
    return static_cast<StdFloat_t>(obj.S);  // simple value
    
}

// no std getter for MSADCPeak/moyalFit/interval



static StdFloat_t _MSADCPeak_get_sqRatio( const MSADCPeak & obj ) { return obj.sumCoarse / obj.sumFine; }


const Traits<MSADCPeak>::Getters
Traits<MSADCPeak>::getters = {
    
    { "time", { "Waveform peak\u0027s time estimate", _MSADCPeak_get_time } },
    { "timeErr", { "Waveform peak\u0027s time estimate error", _MSADCPeak_get_timeErr } },
    { "timeClusterLabel", { "Waveform max time cluster label", _MSADCPeak_get_timeClusterLabel } },
    { "amp", { "Waveform peak\u0027s amp/square estimate", _MSADCPeak_get_amp } },
    { "ampErr", { "Waveform peak\u0027s amp/square estimate error", _MSADCPeak_get_ampErr } },
    { "sumCoarse", { "Waveform peak\u0027s coarse integral estimate (usually, by interpolation)", _MSADCPeak_get_sumCoarse } },
    { "sumFine", { "Waveform peak\u0027s fine integral estimate (usually, by fitting with model)", _MSADCPeak_get_sumFine } },
    { "sumFineErr", { "Waveform peak\u0027s fine integral estimate error", _MSADCPeak_get_sumFineErr } },
    { "chi2", { "Peak fitting chi2", _MSADCPeak_get_chi2 } },
    { "ndf", { "Peak fitting NDF", _MSADCPeak_get_ndf } },
    { "lastFitStatusCode", { "Encoded fit status (contextual).", _MSADCPeak_get_lastFitStatusCode } },


    // propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / mu:float
    { "moyalFit.mu", { "Centers of peaks", _MSADCPeak_get_moyalFit_mu } },
    // propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / sigma:float
    { "moyalFit.sigma", { "Widths of peaks", _MSADCPeak_get_moyalFit_sigma } },
    // propagated from  / moyalFit:mem::Ref<MoyalSADCWfFitParameters> / S:float
    { "moyalFit.S", { "Scaling factor", _MSADCPeak_get_moyalFit_S } },
    // no std getter for MSADCPeak/moyalFit/interval



    { "sqRatio", { "Ratio b/w coarse and fine integral estimates", _MSADCPeak_get_sqRatio } },

};




// getter for RawDataSADC::wave disabled


// no std getter for RawDataSADC/fft/real
// no std getter for RawDataSADC/fft/imag


// getter for RawDataSADC::pedestals disabled
static StdFloat_t _RawDataSADC_get_sum( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.sum);  // simple value
}
static StdFloat_t _RawDataSADC_get_maxSample( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.maxSample);  // simple value
}
static StdFloat_t _RawDataSADC_get_maxAmp( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.maxAmp);  // simple value
}
static StdFloat_t _RawDataSADC_get_maxAmpError( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.maxAmpError);  // simple value
}
static StdFloat_t _RawDataSADC_get_canBeZero( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.canBeZero);  // simple value
}
static StdFloat_t _RawDataSADC_get_ledCorr( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.ledCorr);  // simple value
}
static StdFloat_t _RawDataSADC_get_calibCoeff( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.calibCoeff);  // simple value
}
static StdFloat_t _RawDataSADC_get_ampThreshold( const RawDataSADC & obj ) {
    return static_cast<StdFloat_t>(obj.ampThreshold);  // simple value
}
// getter for RawDataSADC::maxima disabled

static StdFloat_t _RawDataSADC_get_pedestalEven( const RawDataSADC & obj ) { return obj.pedestals[0]; }
static StdFloat_t _RawDataSADC_get_pedestalOdd( const RawDataSADC & obj ) { return obj.pedestals[1]; }
static StdFloat_t _RawDataSADC_get_chargeVsMaxAmpRatio( const RawDataSADC & obj ) { return obj.sum / obj.maxAmp; }


const Traits<RawDataSADC>::Getters
Traits<RawDataSADC>::getters = {
    

    // getter for RawDataSADC::wave disabled


    // no std getter for RawDataSADC/fft/real
    // no std getter for RawDataSADC/fft/imag



    // getter for RawDataSADC::pedestals disabled
    { "sum", { "Computed sum of the waveform, number of channels", _RawDataSADC_get_sum } },
    { "maxSample", { "Number of waveform\u0027s sample considered as global maximum.", _RawDataSADC_get_maxSample } },
    { "maxAmp", { "Maximum of the waveform in raw SADC units.", _RawDataSADC_get_maxAmp } },
    { "maxAmpError", { "Error of the waveform amplitude max. in raw SADC units.", _RawDataSADC_get_maxAmpError } },
    { "canBeZero", { "Zero probability measure (0-1)", _RawDataSADC_get_canBeZero } },
    { "ledCorr", { "Correction by LED in use, ratio", _RawDataSADC_get_ledCorr } },
    { "calibCoeff", { "Calibration coefficient in use, chan/MeV", _RawDataSADC_get_calibCoeff } },
    { "ampThreshold", { "Peak-selection threshold, chan", _RawDataSADC_get_ampThreshold } },

    // getter for RawDataSADC::maxima disabled

    { "pedestalEven", { "Returns even pedestal value (for sample 0, 2, 4 ...)", _RawDataSADC_get_pedestalEven } },
    { "pedestalOdd", { "Returns odd pedestal value (for sample 1, 3, 5 ...)", _RawDataSADC_get_pedestalOdd } },
    { "chargeVsMaxAmpRatio", { "Calculates ratio between sum and max. amplitude", _RawDataSADC_get_chargeVsMaxAmpRatio } },

};






// no std getter for SADCHit/rawData/wave


// no std getter for SADCHit/rawData/fft/real
// no std getter for SADCHit/rawData/fft/imag


// no std getter for SADCHit/rawData/pedestals
// propagated from  / rawData:mem::Ref<RawDataSADC> / sum:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_sum( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.sum);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / maxSample:uint8_t
static StdFloat_t _SADCHit_get_rawData_maxSample( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.maxSample);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / maxAmp:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_maxAmp( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.maxAmp);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / maxAmpError:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_maxAmpError( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.maxAmpError);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / canBeZero:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_canBeZero( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.canBeZero);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / ledCorr:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_ledCorr( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.ledCorr);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / calibCoeff:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_calibCoeff( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.calibCoeff);  // simple value
    
}

// propagated from  / rawData:mem::Ref<RawDataSADC> / ampThreshold:StdFloat_t
static StdFloat_t _SADCHit_get_rawData_ampThreshold( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    
    return static_cast<StdFloat_t>(obj.ampThreshold);  // simple value
    
}

// no std getter for SADCHit/rawData/maxima

// propagated from  / rawData:mem::Ref<RawDataSADC> / pedestalEven:
static StdFloat_t _SADCHit_get_rawData_pedestalEven( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    return obj.pedestals[0];  // custom getter
}
// propagated from  / rawData:mem::Ref<RawDataSADC> / pedestalOdd:
static StdFloat_t _SADCHit_get_rawData_pedestalOdd( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    return obj.pedestals[1];  // custom getter
}
// propagated from  / rawData:mem::Ref<RawDataSADC> / chargeVsMaxAmpRatio:
static StdFloat_t _SADCHit_get_rawData_chargeVsMaxAmpRatio( const SADCHit & obj_ ) {
    if(!obj_.rawData) return std::nan("0");
    const auto & rawData = *obj_.rawData;
    const auto & obj = rawData;
    return obj.sum / obj.maxAmp;  // custom getter
}

static StdFloat_t _SADCHit_get_eDep( const SADCHit & obj ) {
    return static_cast<StdFloat_t>(obj.eDep);  // simple value
}
static StdFloat_t _SADCHit_get_eDepError( const SADCHit & obj ) {
    return static_cast<StdFloat_t>(obj.eDepError);  // simple value
}
static StdFloat_t _SADCHit_get_time( const SADCHit & obj ) {
    return static_cast<StdFloat_t>(obj.time);  // simple value
}
static StdFloat_t _SADCHit_get_timeError( const SADCHit & obj ) {
    return static_cast<StdFloat_t>(obj.timeError);  // simple value
}



const Traits<SADCHit>::Getters
Traits<SADCHit>::getters = {
    


    // no std getter for SADCHit/rawData/wave


    // no std getter for SADCHit/rawData/fft/real
    // no std getter for SADCHit/rawData/fft/imag


    // no std getter for SADCHit/rawData/pedestals
    // propagated from  / rawData:mem::Ref<RawDataSADC> / sum:StdFloat_t
    { "rawData.sum", { "Computed sum of the waveform, number of channels", _SADCHit_get_rawData_sum } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / maxSample:uint8_t
    { "rawData.maxSample", { "Number of waveform\u0027s sample considered as global maximum.", _SADCHit_get_rawData_maxSample } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / maxAmp:StdFloat_t
    { "rawData.maxAmp", { "Maximum of the waveform in raw SADC units.", _SADCHit_get_rawData_maxAmp } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / maxAmpError:StdFloat_t
    { "rawData.maxAmpError", { "Error of the waveform amplitude max. in raw SADC units.", _SADCHit_get_rawData_maxAmpError } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / canBeZero:StdFloat_t
    { "rawData.canBeZero", { "Zero probability measure (0-1)", _SADCHit_get_rawData_canBeZero } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / ledCorr:StdFloat_t
    { "rawData.ledCorr", { "Correction by LED in use, ratio", _SADCHit_get_rawData_ledCorr } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / calibCoeff:StdFloat_t
    { "rawData.calibCoeff", { "Calibration coefficient in use, chan/MeV", _SADCHit_get_rawData_calibCoeff } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / ampThreshold:StdFloat_t
    { "rawData.ampThreshold", { "Peak-selection threshold, chan", _SADCHit_get_rawData_ampThreshold } },
    // no std getter for SADCHit/rawData/maxima

    // propagated from  / rawData:mem::Ref<RawDataSADC> / pedestalEven:
    { "rawData.pedestalEven", { "propagated from  / rawData:mem::Ref\u003cRawDataSADC\u003e / pedestalEven:", _SADCHit_get_rawData_pedestalEven } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / pedestalOdd:
    { "rawData.pedestalOdd", { "propagated from  / rawData:mem::Ref\u003cRawDataSADC\u003e / pedestalOdd:", _SADCHit_get_rawData_pedestalOdd } },
    // propagated from  / rawData:mem::Ref<RawDataSADC> / chargeVsMaxAmpRatio:
    { "rawData.chargeVsMaxAmpRatio", { "propagated from  / rawData:mem::Ref\u003cRawDataSADC\u003e / chargeVsMaxAmpRatio:", _SADCHit_get_rawData_chargeVsMaxAmpRatio } },

    { "eDep", { "Reconstructed energy deposition, MeV.", _SADCHit_get_eDep } },
    { "eDepError", { "Error estimation of reconstructed energy deposition (sigma), MeV", _SADCHit_get_eDepError } },
    { "time", { "Reconstructed time of hit, nanoseconds", _SADCHit_get_time } },
    { "timeError", { "Estimateion of reconstructed time error (sigma), nanoseconds", _SADCHit_get_timeError } },


};



}  // namespace ::na64dp::event
}  // namespace na64dp
