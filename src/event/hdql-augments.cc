#include "na64event/hdql-augments.hh"
#include "na64calib/dispatcher.hh"
#include "na64detID/TBName.hh"
#include "na64detID/trackID.hh"
#include "na64detID/wireID.hh"
#include "na64event/data/calo.hh"
#include "na64event/data/f1.hh"
#include "na64event/data/sadc.hh"
#include "na64event/data/stwTDC.hh"
#include "na64event/data/track.hh"
#include "na64event/hdql-assets.hh"
#include <hdql/value.h>

#if defined(hdql_FOUND) && hdql_FOUND

#include <hdql/attr-def.h>
#include <hdql/compound.h>
#include <hdql/types.h>
#include <hdql/operations.h>
#include <hdql/context.h>
#include <hdql/function.h>

namespace na64dp {
namespace event {

void
create_event_id_compound( hdql::helpers::CompoundTypes & compoundTypes
                        , hdql_Context * context ) {
    // This customized compound interface provides breakdown of encoded event
    // ID into runNo/spillNo/eventInSpillNo properties. Additionally, a full
    // event ID is available as id
    // TODO: for now it is just an empty compound definition
    hdql_Compound * compound = hdql_compound_new("EventID", context);
    // runNo attribute
    // ...
    // spillNo attribute
    // ...
    // eventInSpillNo attribute
    // ...
    // id attribute
    // ...
    compoundTypes.emplace(typeid(EventID), compound);
}

void
create_timepair_compound( hdql::helpers::CompoundTypes & compoundTypes
                        , hdql_Context * context ) {
    // This customized compound interface provides breakdown of encoded event
    // ID into runNo/spillNo/eventInSpillNo properties. Additionally, a full
    // event ID is available as id
    // TODO: for now it is just an empty compound definition
    hdql_Compound * compound = hdql_compound_new("EventTime", context);
    // runNo attribute
    // ...
    // spillNo attribute
    // ...
    // eventInSpillNo attribute
    // ...
    // id attribute
    // ...
    compoundTypes.emplace(typeid(std::pair<time_t, uint32_t>), compound);
}

}  // namespace ::na64dp::event

namespace {

//
// Custom key types

//
// TrackID

int
copy_track_id( hdql_Datum_t dest
             , const hdql_Datum_t src
             , size_t byteLen
             , hdql_Context_t
             ) {
    assert(byteLen == sizeof(TrackID));
    memcpy(dest, src, byteLen);
    return 0;
}

int
get_track_id_as_string( const hdql_Datum_t d_, char * buf, size_t bufSize, hdql_Context_t) {
    throw std::runtime_error("TODO: TrackID to string");
}

int
set_track_id_from_string(hdql_Datum_t d_, const char * strexpr, hdql_Context_t) {
    throw std::runtime_error("TODO: TrackID from string");
}

int
add_track_id_type(hdql_ValueTypes * vt) {
    hdql_ValueInterface vti;
    vti.name = "TrackID";
    vti.init = NULL;
    vti.destroy = NULL;
    vti.size = sizeof(TrackID);
    vti.copy = &copy_track_id;
    vti.get_as_logic  = NULL;
    vti.set_as_logic  = NULL;
    vti.get_as_int    = NULL;
    vti.set_as_int    = NULL;
    vti.get_as_float  = NULL;
    vti.set_as_float  = NULL;
    vti.get_as_string = get_track_id_as_string;
    vti.set_from_string = set_track_id_from_string;
    return hdql_types_define(vt, &vti);
}

//
// DetID

int
copy_det_id( hdql_Datum_t dest
           , const hdql_Datum_t src
           , size_t byteLen
           , hdql_Context_t
           ) {
    assert(byteLen == sizeof(DetID));
    memcpy(dest, src, byteLen);
    return 0;
}

const nameutils::DetectorNaming &
get_naming_handle(hdql_Context_t context) {
    void * namingHandle_ = hdql_context_custom_data_get(context, "detNaming");
    if(NULL == namingHandle_) {
        throw std::runtime_error("No item \"detNaming\" bound to context");
    }
    return reinterpret_cast<calib::Handle<nameutils::DetectorNaming>*>(namingHandle_)->get();
}

int
get_det_id_as_string( const hdql_Datum_t d_
                    , char * buf
                    , size_t bufSize
                    , hdql_Context_t context
                    ) {
    std::string name = get_naming_handle(context)[*reinterpret_cast<DetID*>(d_)];
    strncpy(buf, name.c_str(), bufSize);
    return 0;
}

int
set_det_id_from_string( hdql_Datum_t d_
                      , const char * strexpr
                      , hdql_Context_t context
                      ) {
    *reinterpret_cast<DetID*>(d_) = get_naming_handle(context)[strexpr];
    return 0;
}

int
add_det_id_type(hdql_ValueTypes * vt) {
    hdql_ValueInterface vti;
    vti.name = "DetID";
    vti.init = NULL;
    vti.destroy = NULL;
    vti.size = sizeof(DetID);
    vti.copy = &copy_det_id;
    vti.get_as_logic  = NULL;
    vti.set_as_logic  = NULL;
    vti.get_as_int    = NULL;
    vti.set_as_int    = NULL;
    vti.get_as_float  = NULL;
    vti.set_as_float  = NULL;
    vti.get_as_string = get_det_id_as_string;
    vti.set_from_string = set_det_id_from_string;
    return hdql_types_define(vt, &vti);
}

//
// PlaneKey

int
copy_plane_key( hdql_Datum_t dest
              , const hdql_Datum_t src
              , size_t byteLen
              , hdql_Context_t
              ) {
    assert(byteLen == sizeof(PlaneKey));
    memcpy(dest, src, byteLen);
    return 0;
}

int
plane_key_as_string( const hdql_Datum_t d_, char * buf, size_t bufSize, hdql_Context_t context) {
    std::string name = get_naming_handle(context)[*reinterpret_cast<PlaneKey*>(d_)];
    strncpy(buf, name.c_str(), bufSize);
    return 0;
}

int
set_plane_key_from_string(hdql_Datum_t d_, const char * strexpr, hdql_Context_t context) {
    *reinterpret_cast<PlaneKey*>(d_) = PlaneKey(get_naming_handle(context)[strexpr]);
    return 0;
}

int
add_plane_key_type(hdql_ValueTypes * vt) {
    hdql_ValueInterface vti;
    vti.name = "PlaneKey";
    vti.init = NULL;
    vti.destroy = NULL;
    vti.size = sizeof(DetID);
    vti.copy = &copy_plane_key;
    vti.get_as_logic  = NULL;
    vti.set_as_logic  = NULL;
    vti.get_as_int    = NULL;
    vti.set_as_int    = NULL;
    vti.get_as_float  = NULL;
    vti.set_as_float  = NULL;
    vti.get_as_string = plane_key_as_string;
    vti.set_from_string = set_plane_key_from_string;
    return hdql_types_define(vt, &vti);
}

}  // empty ns

void
init_root_hdql_context( hdql_Context * context
                      , hdql::helpers::CompoundTypes *& compoundTypes
                      , calib::Dispatcher & cdsp
                      ) {
    // reentrant table with type interfaces
    hdql_ValueTypes * valTypes = hdql_context_get_types(context);
    // add standard (int, float, etc) types
    hdql_value_types_table_add_std_types(valTypes);
    // add na64sw-specific types
    add_det_id_type(valTypes);
    add_plane_key_type(valTypes);
    add_track_id_type(valTypes);

    // create and operations table
    hdql_Operations * operations = hdql_context_get_operations(context);
    // add std types arithmetics
    hdql_op_define_std_arith(operations, valTypes);

    // add track id as key-compound object with to-arithmetic interface


    // add event compounds, in order
    compoundTypes = new hdql::helpers::CompoundTypes(context);

    // add event compounds; these functions are generated automatically along
    // with changed event structure definitions. One function per single .yaml
    // definition tier
    event::create_event_id_compound(*compoundTypes, context);
    event::create_timepair_compound(*compoundTypes, context);

    event::define_sadc_hdql_compounds(*compoundTypes, context);
    event::define_apv_hdql_compounds(*compoundTypes, context);
    event::define_f1_hdql_compounds(*compoundTypes, context);
    event::define_stwTDC_hdql_compounds(*compoundTypes, context);
    event::define_track_hdql_compounds(*compoundTypes, context);
    event::define_calo_hdql_compounds(*compoundTypes, context);
    event::define_event_hdql_compounds(*compoundTypes, context);

    // add standard functions
    hdql_functions_add_standard_math(hdql_context_get_functions(context));
    // add standard type conversions
    hdql_converters_add_std(hdql_context_get_conversions(context), hdql_context_get_types(context));

    // create and associate detector naming handle to be used in
    // detID-filtering expressions
    auto namingHandle = new calib::Handle<nameutils::DetectorNaming>("default", cdsp);
    hdql_context_custom_data_add(context, "detNaming", namingHandle);
    // ...
}

void
destroy_root_hdql_context(hdql_Context * context, hdql::helpers::CompoundTypes * compoundTypes) {
    hdql_context_destroy(context);
    for(auto & ce : *compoundTypes) {
        hdql_compound_destroy(ce.second, context);
    }
    delete compoundTypes;
}

}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND

