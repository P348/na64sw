#include "na64util/uri.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace test {

TEST(URI, EncodeMathesDecodeSimple) {
    const std::string sample = " s`oom''\"e %123+17=150  #/#!$ h),),]"
                    , encoded = util::URI::encode(sample)
                    , decoded = util::URI::decode(encoded)
                    ;
    EXPECT_STREQ(sample.c_str(), decoded.c_str());
}

TEST(URI, CorrectlyParsesFullURI) {
    const std::string sample
        = "https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top";
    util::URI uri(sample);

    EXPECT_STREQ(uri.scheme().c_str(), "https");
    EXPECT_STREQ(uri.userinfo().c_str(), "john.doe");
    EXPECT_STREQ(uri.host().c_str(), "www.example.com");
    EXPECT_STREQ(uri.port().c_str(), "123");
    EXPECT_STREQ(uri.path().c_str(), "/forum/questions/");
    //EXPECT_STREQ(uri.query().c_str(), "tag=networking&order=newest");
    auto it1 = uri.query().find("tag");
    ASSERT_NE(it1, uri.query().end());
    EXPECT_STREQ(it1->second.c_str(), "networking");
    auto it2 = uri.query().find("order");
    ASSERT_NE(it2, uri.query().end());
    EXPECT_STREQ(it2->second.c_str(), "newest");
    EXPECT_STREQ(uri.fragment().c_str(), "top");

    EXPECT_STREQ(uri.authority().c_str(), "john.doe@www.example.com:123");

    //EXPECT_STREQ(sample.c_str(), uri.to_str().c_str());  // TODO: query string may differ (unordered map)
}

TEST(URI, CorrectlyHandlesSimplePath) {
    const std::string sample
        = "/foo/bar/../one\\ two/.././/";
    util::URI uri(sample);

    EXPECT_TRUE(uri.scheme().empty());
    EXPECT_TRUE(uri.userinfo().empty());
    EXPECT_TRUE(uri.host().empty());
    EXPECT_TRUE(uri.port().empty());
    EXPECT_STREQ(uri.path().c_str(), sample.c_str());
    EXPECT_TRUE(uri.query().empty());
    EXPECT_TRUE(uri.fragment().empty());

    EXPECT_TRUE(uri.authority().empty());

    EXPECT_STREQ(sample.c_str(), uri.to_str().c_str());
}

}
}

