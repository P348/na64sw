#include "na64util/config-helpers.hh"

#include <gtest/gtest.h>
#include <memory>

namespace na64dp {
namespace test {

//
// Path

const char gBadPaths[][64] = {
    "[]", ".", "[", "]", "..", ".[]", "foo.[12]", "foo[bar]",
    "foo.[112]", "foo.bar[12.34]", "one..two.three", ".",
    "2abc", "2.abc", "[12.abc]", "*[*]*.*", ".abc", "[12a]",
    "foo[**]", "bar-12]",
    ""
};

TEST(RConfigHelpers, PathHandlesErrors) {
    for(auto path = gBadPaths; '\0' != **path; ++path) {
        na64dp::util::pdef::Path * p;
        EXPECT_THROW( p = new na64dp::util::pdef::Path(*path)
                    , na64dp::errors::BadPath )
            << "Didn't detect error at \"" << *path << "\"";
        p = nullptr;
        EXPECT_FALSE(p);
    }
}

TEST(RConfigHelpers, PathParsesStringKeyOneLetter) {
    na64dp::util::pdef::Path p("a");
    ASSERT_EQ(1, p.size());
    auto & ptok = p[0];
    EXPECT_EQ(ptok.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok.pl.stringKey, "a");
}

TEST(RConfigHelpers, PathParsesSingleIndex) {
    na64dp::util::pdef::Path p("[-1]");
    ASSERT_EQ(1, p.size());
    auto & ptok = p[0];
    EXPECT_EQ(ptok.type, na64dp::util::pdef::PathToken::kIntKey);
    EXPECT_EQ(ptok.pl.index, -1);
}

TEST(RConfigHelpers, PathParsesStringKeyMultiLetter) {
    na64dp::util::pdef::Path p("abc");
    ASSERT_EQ(1, p.size());
    auto & ptok = p[0];
    EXPECT_EQ(ptok.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok.pl.stringKey, "abc");
}

TEST(RConfigHelpers, PathParsesStringKeysChain) {
    na64dp::util::pdef::Path p("abc.bca.z");
    ASSERT_EQ(3, p.size());

    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok1.pl.stringKey, "abc");

    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok2.pl.stringKey, "bca");

    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok3.pl.stringKey, "z");
}

TEST(RConfigHelpers, PathParsesIntsKeysChain) {
    na64dp::util::pdef::Path p("[20][-2][12]");
    ASSERT_EQ(3, p.size());

    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kIntKey);
    EXPECT_EQ(ptok1.pl.index, 20);

    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kIntKey);
    EXPECT_EQ(ptok2.pl.index, -2);

    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kIntKey);
    EXPECT_EQ(ptok3.pl.index, 12);
}

TEST(RConfigHelpers, PathParsesSingleAnyIndex) {
    na64dp::util::pdef::Path p("[*]");
    ASSERT_EQ(1, p.size());
    auto & ptok = p[0];
    EXPECT_EQ(ptok.type, na64dp::util::pdef::PathToken::kAnyIndex);
}

TEST(RConfigHelpers, PathParsesSingleAnyKey) {
    na64dp::util::pdef::Path p("*");
    ASSERT_EQ(1, p.size());
    auto & ptok = p[0];
    EXPECT_EQ(ptok.type, na64dp::util::pdef::PathToken::kAnyStrKey);
}

TEST(RConfigHelpers, PathParsesChainedAnyStrKeys) {
    na64dp::util::pdef::Path p("*.*.*");
    EXPECT_EQ(3, p.size());

    ASSERT_GE(p.size(), 1);
    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 2);
    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 3);
    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kAnyStrKey);
}

TEST(RConfigHelpers, PathParsesChainedAnyIndexKeys) {
    na64dp::util::pdef::Path p("[*][*][*]");
    EXPECT_EQ(3, p.size());

    ASSERT_GE(p.size(), 1);
    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 2);
    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 3);
    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kAnyIndex);
}

TEST(RConfigHelpers, PathParsesChainedAnyKeys) {
    na64dp::util::pdef::Path p("*[*].*.*");
    EXPECT_EQ(4, p.size());

    ASSERT_GE(p.size(), 1);
    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 2);
    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 3);
    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 4);
    auto & ptok4 = p[3];
    EXPECT_EQ(ptok4.type, na64dp::util::pdef::PathToken::kAnyStrKey);
}

TEST(RConfigHelpers, PathParsesChainedMixedKeys) {
    na64dp::util::pdef::Path p("one[*].foo[*][*].*.bar.*.zum[-12]");
    EXPECT_EQ(10, p.size());

    ASSERT_GE(p.size(), 1);  // one
    auto & ptok1 = p[0];
    EXPECT_EQ(ptok1.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok1.pl.stringKey, "one");

    ASSERT_GE(p.size(), 2);  // [*]
    auto & ptok2 = p[1];
    EXPECT_EQ(ptok2.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 3);  // foo
    auto & ptok3 = p[2];
    EXPECT_EQ(ptok3.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok3.pl.stringKey, "foo");

    ASSERT_GE(p.size(), 4);  // [*]
    auto & ptok4 = p[3];
    EXPECT_EQ(ptok4.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 5);  // [*]
    auto & ptok5 = p[4];
    EXPECT_EQ(ptok5.type, na64dp::util::pdef::PathToken::kAnyIndex);

    ASSERT_GE(p.size(), 6);  // *
    auto & ptok6 = p[5];
    EXPECT_EQ(ptok6.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 7);  // bar
    auto & ptok7 = p[6];
    EXPECT_EQ(ptok7.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok7.pl.stringKey, "bar");

    ASSERT_GE(p.size(), 8);  // *
    auto & ptok8 = p[7];
    EXPECT_EQ(ptok8.type, na64dp::util::pdef::PathToken::kAnyStrKey);

    ASSERT_GE(p.size(), 9);  // zum
    auto & ptok9 = p[8];
    EXPECT_EQ(ptok9.type, na64dp::util::pdef::PathToken::kStrKey);
    EXPECT_STREQ(ptok9.pl.stringKey, "zum");

    ASSERT_GE(p.size(), 10);  // -12
    auto & ptok10 = p[9];
    EXPECT_EQ(ptok10.type, na64dp::util::pdef::PathToken::kIntKey);
    EXPECT_EQ(ptok10.pl.index, -12);
}

TEST(RConfigHelpers, PathRestoresItselfFromString) {
    const std::string origPath = "one[*].foo[*][*].*.bar.*.zum[-12]";
    na64dp::util::pdef::Path p(origPath);
    EXPECT_EQ(10, p.size());
    std::string asString = p.to_string();
    EXPECT_EQ(origPath, asString);
}

TEST(RConfigHelpers, AssignmentOperatorWorks) {
    const std::string origPath = "one[*].foo[*][*].*.bar.*.zum[-12]";
    na64dp::util::pdef::Path copy;
    {
        na64dp::util::pdef::Path p(origPath);
        EXPECT_EQ(10, p.size());
        copy = p;
    }
    std::string asString = copy.to_string();
    EXPECT_EQ(origPath, asString);
}

TEST(RConfigHelpers, CopyCtrWorks) {
    const std::string origPath = "one[*].foo[*][*].*.bar.*.zum[-12]";
    na64dp::util::pdef::Path orig(origPath);
    na64dp::util::pdef::Path copy(orig);
    std::string asString = copy.to_string();
    EXPECT_EQ(origPath, asString);
}

//
// Scalars

TEST(RConfigHelpers, NumericInRangeScalarValidatorWorks) {
    util::pdef::Scalar<int> intPar;
    std::shared_ptr<util::pdef::RangeValidator<int>> vv
        = std::make_shared<util::pdef::RangeValidator<int>>();
    vv->left = -1;
    vv->right = 10;
    intPar.set_validator(vv);

    ASSERT_THROW(intPar.set(-2), errors::InvalidParameterValue );
    EXPECT_FALSE(intPar.is_set());

    ASSERT_THROW(intPar.set(11), errors::InvalidParameterValue );
    EXPECT_FALSE(intPar.is_set());

    intPar.set(0);
    EXPECT_TRUE(intPar.is_set());
    EXPECT_EQ(intPar.get(), 0);
}

TEST(RConfigHelpers, NumericLEScalarValidatorWorks) {
    util::pdef::Scalar<int> intPar;
    std::shared_ptr<util::pdef::RangeValidator<int>> vv
        = std::make_shared<util::pdef::RangeValidator<int>>();
    vv->right = 10;
    intPar.set_validator(vv);

    ASSERT_THROW(intPar.set(11), errors::InvalidParameterValue );
    EXPECT_FALSE(intPar.is_set());

    intPar.set(10);
    EXPECT_TRUE(intPar.is_set());
    EXPECT_EQ(intPar.get(), 10);
}

TEST(RConfigHelpers, NumericGEScalarValidatorWorks) {
    util::pdef::Scalar<int> intPar;
    std::shared_ptr<util::pdef::RangeValidator<int>> vv
        = std::make_shared<util::pdef::RangeValidator<int>>();
    vv->left = -10;
    intPar.set_validator(vv);

    ASSERT_THROW(intPar.set(-11), errors::InvalidParameterValue );
    EXPECT_FALSE(intPar.is_set());

    intPar.set(-10);
    EXPECT_TRUE(intPar.is_set());
    EXPECT_EQ(intPar.get(), -10);
}

//
// Nodes

TEST(RConfigHelpers, NodeDefaultStateUnset) {
    util::pdef::Node node;
    EXPECT_FALSE(node.is_array());
    EXPECT_FALSE(node.is_custom());
    EXPECT_FALSE(node.is_scalar());
    EXPECT_FALSE(node.is_section());
}

// - scalar

TEST(RConfigHelpers, CreatesScalarNode) {
    util::pdef::Node node;
    auto scalar = node.set_scalar<int>();
    EXPECT_FALSE(node.is_array());
    EXPECT_FALSE(node.is_custom());
    EXPECT_TRUE(node.is_scalar());
    EXPECT_FALSE(node.is_section());

    EXPECT_FALSE(scalar.is_set());

    EXPECT_THROW(scalar.get(), errors::ParameterValueIsNotSet);
}

TEST(RConfigHelpers, ScalarNodeDetectsBadType) {
    util::pdef::Node node;
    auto & scalar = node.set_scalar<int>();

    EXPECT_THROW(node.as_section(), errors::InvalidParameterType);
    EXPECT_THROW(node.as_array(),   errors::InvalidParameterType);
    auto & scalar2 = node.as_scalar_of_type<int>();
    EXPECT_EQ(&scalar, &scalar2);

    EXPECT_THROW(node.set_scalar<int>(), errors::NodeRedefinition);
    EXPECT_THROW(node.set_section(),    errors::NodeRedefinition);
    EXPECT_THROW(node.set_array(),      errors::NodeRedefinition);
}

// - section

TEST(RConfigHelpers, CreatesSectionNode) {
    util::pdef::Node node;
    auto & section = node.set_section();
    EXPECT_FALSE(node.is_array());
    EXPECT_FALSE(node.is_custom());
    EXPECT_FALSE(node.is_scalar());
    EXPECT_TRUE(node.is_section());

    EXPECT_TRUE(section.empty());
}

TEST(RConfigHelpers, GetSetOfSectionNodeWorks) {
    util::pdef::Node node;
    auto & sectionOrig = node.set_section();
    auto & section = node.as_section();
    EXPECT_EQ(&sectionOrig, &section);
    EXPECT_TRUE(section.empty());

    EXPECT_THROW(node.as_scalar(),  errors::InvalidParameterType);
    EXPECT_THROW(node.as_scalar_of_type<int>(), errors::InvalidParameterType);
    EXPECT_THROW(node.as_array(),   errors::InvalidParameterType);

    util::pdef::Node * leaf1 = new util::pdef::Node();
    leaf1->set_scalar<int>();
    EXPECT_THROW(leaf1->set_scalar<int>(), errors::NodeRedefinition);

    section.emplace("intNode", leaf1);

    util::pdef::Node * leaf2 = new util::pdef::Node();
    leaf2->set_scalar<float>();
    section.emplace("floatNode", leaf2);

    ASSERT_FALSE(section.empty());
    ASSERT_EQ(section.size(), 2);

    node["intNode"].as_scalar_of_type<int>().set(10);
    EXPECT_EQ(leaf1->as_scalar_of_type<int>().get(), 10);
}

// - array

TEST(RConfigHelpers, CreatesArrayNode) {
    util::pdef::Node node;
    auto array = node.set_array();
    EXPECT_TRUE(node.is_array());
    EXPECT_FALSE(node.is_custom());
    EXPECT_FALSE(node.is_scalar());
    EXPECT_FALSE(node.is_section());

    EXPECT_TRUE(array.empty());
    EXPECT_FALSE(array.topology) << "Topology pointer is set (it shouldn't)";
}

TEST(RConfigHelpers, GetSetOfArrayNodeWorks) {
    auto node = std::make_shared<util::pdef::Node>();
    auto & arrayOrig = node->set_array();
    auto & array = node->as_array();
    EXPECT_EQ(&arrayOrig, &array);
    EXPECT_TRUE(array.empty());

    EXPECT_THROW(node->as_scalar(),  errors::InvalidParameterType);
    EXPECT_THROW(node->as_scalar_of_type<int>(), errors::InvalidParameterType);
    EXPECT_THROW(node->as_section(), errors::InvalidParameterType);

    auto leaf1 = std::make_shared<util::pdef::Node>();
    leaf1->set_scalar<int>();
    EXPECT_THROW(leaf1->set_scalar<int>(), errors::NodeRedefinition);
    array.push_back(leaf1);

    auto leaf2 = std::make_shared<util::pdef::Node>();
    leaf2->set_scalar<float>();
    array.push_back(leaf2);

    ASSERT_FALSE(node->as_array().empty());
    ASSERT_EQ(node->as_array().size(), 2);

    EXPECT_THROW( (*node)["intNode"]
                , errors::InvalidParameterType );
    EXPECT_EQ( &((*node)[0])
             , leaf1.get()
             );
}

//
// Composer

TEST(RConfigHelpers, OneLevelSectionResolvesBasicNodes) {
    util::pdef::Node node;
    ASSERT_TRUE(node.is_unset());
    ASSERT_FALSE(node.is_section());

    util::pdef::Composer nc(node);
    nc.p<int>("one", "Some integer parameter")
      .p<float>("two", "Another parameter of type float")
      ;

    ASSERT_TRUE(node.is_section());

    auto & sec = node.as_section();

    ASSERT_EQ(sec.size(), 2);

    ASSERT_TRUE(node.has("one"));
    const util::pdef::Node & node1 = node["one"];
    ASSERT_TRUE(node1.is_scalar());
    ASSERT_FALSE(node1.as_scalar_of_type<int>().is_set());

    ASSERT_TRUE(node.has("two"));
    const util::pdef::Node & node2 = node["two"];
    ASSERT_TRUE(node2.is_scalar());
    ASSERT_FALSE(node2.as_scalar_of_type<float>().is_set());
}

TEST(RConfigHelpers, OneLevelArrayResolvesBasicNodes) {
    util::pdef::Node node;
    ASSERT_TRUE(node.is_unset());
    ASSERT_FALSE(node.is_array());

    util::pdef::Composer nc(node);
    nc.el<int>("Some integer parameter")
      .el<float>("Another parameter of type float")
      ;

    ASSERT_TRUE(node.is_array());

    auto & arr = node.as_array();

    ASSERT_EQ(arr.size(), 2);

    ASSERT_TRUE(node.has(0));
    const util::pdef::Node & node1 = node[0];
    ASSERT_TRUE(node1.is_scalar());
    ASSERT_FALSE(node1.as_scalar_of_type<int>().is_set());

    ASSERT_TRUE(node.has(1));
    const util::pdef::Node & node2 = node[1];
    ASSERT_TRUE(node2.is_scalar());
    ASSERT_FALSE(node2.as_scalar_of_type<float>().is_set());
}

TEST(RConfigHelpers, ConfigExample) {
    util::pdef::Node node;
    ASSERT_TRUE(node.is_unset());
    ASSERT_FALSE(node.is_section());
    ASSERT_FALSE(node.is_array());

    util::pdef::Composer nc(node);
    nc
        .p<std::string>("title", "Histogram name")
        .p<std::string>("description", "Description, with default value")
            //.value("{quantity} counts")
        .bgn_sub("axis", "Histogram bins and range, example")
            .p<int>("nBins", 10, "Number of bins")
            .p<float>("left", -1, "Axis lower limit")
            .p<float>("right", 1, "Axis upper limit")
        .end_sub("axis")
        ;
}

}
}


