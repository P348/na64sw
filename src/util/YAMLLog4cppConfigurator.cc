#include "na64util/YAMLLog4cppConfigurator.hh"
#include "YAMLLog4cppConfiguratorImpl.hh"

namespace na64dp {
namespace util {

void
YAMLLog4cppConfigurator::configure(const std::string & filename) {
    static YAMLLog4cppConfiguratorImpl configurator;
    configurator.doConfigure(filename);
}

}
}

