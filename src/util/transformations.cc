#include "na64util/transformations.hh"

#include <unordered_map>
#include <cassert>

namespace na64dp {
namespace util {

const na64sw_RotationOrder gStdRotationOrder
    = na64sw_Rotation_zyx;  // TODO: macro

template<> Matrix3T<float>
rotation_matrix<float>( na64sw_RotationOrder order, const float * angles ) {
    Matrix3T<float> m;
    int rc = na64sw_rotation_matrix_f(order, angles, m.m);
    if(rc) {
        NA64DP_RUNTIME_ERROR("Couldn't create (float) rotation matrix for"
                " given axes order.");
    }
    return m;
}

template<> Matrix3T<double>
rotation_matrix<double>( na64sw_RotationOrder order, const double * angles ) {
    Matrix3T<double> m;
    int rc = na64sw_rotation_matrix_d(order, angles, m.m);
    if(rc) {
        NA64DP_RUNTIME_ERROR("Couldn't create (double float) rotation matrix for"
                " given axes order.");
    }
    return m;
}

//
// Transformation object implementation

void
Transformation::_recache_affine_transforms() const {
    _rotation = util::rotation_matrix(_rotationOrder, _angles);
    for(int i = 0; i < 3; ++i) {
        _shearAndScale.cm[i][0] = _u[i];
        _shearAndScale.cm[i][1] = _v[i];
        _shearAndScale.cm[i][2] = _w[i];
    }
    _affineMatrix = _rotation*_shearAndScale;
    _cachesValid = true;
}

Transformation::Transformation()
    : _cachesValid(false)
    , _rotationOrder(gStdRotationOrder)
    , _invertOffset(false)
    , _offset{{0, 0, 0}}
    , _u{{1, 0, 0}}, _v{{0, 1, 0}}, _w{{0, 0, 1}}
    , _angles{0, 0, 0}
    {}

Transformation::Transformation(const Transformation & other)
    : _cachesValid(false)
    , _rotationOrder(other._rotationOrder)
    , _invertOffset(other._invertOffset)
    , _offset(other._offset)
    , _u(other._u), _v(other._v), _w(other._w)
    , _angles{other._angles[0], other._angles[1], other._angles[2]}
    {}

Transformation::Transformation( const Vec3 & offset_
                              , const Vec3 & u_, const Vec3 & v_, const Vec3 & w_
                              , Float_t alpha, Float_t beta, Float_t gamma
                              , na64sw_RotationOrder rotationOrder
                              , bool invertOffset
                              ) : _cachesValid(false)
                                , _rotationOrder(rotationOrder)
                                , _invertOffset(invertOffset)
                                , _offset(offset_)
                                , _u(u_), _v(v_), _w(w_)
                                , _angles{alpha, beta, gamma}
                                {}

Float_t
Transformation::angle( int i ) const {
    if( i < 0 || i > 2 ) {
        NA64DP_RUNTIME_ERROR("%d is out of range for transformation angle.", i);
    }
    return _angles[i];
}

void
Transformation::angle( int i, Float_t value ) {
    if( i < 0 || i > 2 ) {
        NA64DP_RUNTIME_ERROR("%d is out of range for transformation angle.", i);
    }
    _invalidate_caches();
    _angles[i] = value;
}


const Matrix3 &
Transformation::rotation_matrix() const {
    if(!_cachesValid) _recache_affine_transforms();
    return _rotation;
}

const Matrix3 &
Transformation::basis() const {
    if(!_cachesValid) _recache_affine_transforms();
    return _shearAndScale;
}

const Matrix3 &
Transformation::affine_matrix() const {
    if(!_cachesValid) _recache_affine_transforms();
    return _affineMatrix;
}


#if 0
void
apply_std_rotation( Vec3 & r
                  , const Float_t rot[3]
                  ) {
    #if 1
    const Float_t s1 = sin(rot[2]), c1 = cos(rot[2])
                , s2 = sin(rot[1]), c2 = cos(rot[1])
                , s3 = sin(rot[0]), c3 = cos(rot[0])
                ;
    Matrix3 m = {{ {c1*c2, c1*s2*s3 - c3*s1, s1*s3 + c1*c3*s2}
                 , {c2*s1, c1*c3 + s1*s2*s3, c3*s1*s2 - c1*s3}
                 , {  -s2,            c2*s3,            c2*c3}
                 }};
    r = m*r;
    #else
    //#if defined(ROOT_FOUND) && ROOT_FOUND
    //r.RotateZ( M_PI * rot[2] / 180 );
    //r.RotateY( M_PI * rot[1] / 180 );
    //r.RotateX( M_PI * rot[0] / 180 );
    //#endif
    #endif
}
#endif

#if 0
void
cardinal_vectors( const float center[3]
                , const float size[3]
                , const float rot[3]
                , Vec3 & o
                , Vec3 & u, Vec3 & v, Vec3 & w
                ) {
    o.r[0] = center[0]; o.r[1] = center[1]; o.r[2] = center[2];
    u.r[0] = size[0];   u.r[1] = 0.;        u.r[2] = 0.;
    v.r[0] = 0.;        v.r[1] = size[1];   v.r[2] = 0.;
    w.r[0] = 0.;        w.r[1] = 0.;        w.r[2] = size[2];

    apply_std_rotation(u, rot);
    apply_std_rotation(v, rot);
    apply_std_rotation(w, rot);
}
#endif

static std::unordered_map< std::string, std::unordered_map<std::string, float> >
    _gUnitsByQuantity;

static bool _init_units_conversion_table() {
    #define DEFINE_UNITS_ENTRY( name, value, quantity ) \
    { auto qIR = _gUnitsByQuantity.emplace(quantity, decltype(_gUnitsByQuantity)::mapped_type()); \
      qIR.first->second.emplace( # name, Units:: name ); }
    NA64SW_FOR_EVERY_UNITS(DEFINE_UNITS_ENTRY)
    #undef DEFINE_UNITS_ENTRY
    return true;
}

static bool _unitConverionTableInitialized
    = _init_units_conversion_table();

float
Units::get_units_conversion_factor( const std::string & strUnitsFrom
                                  , const std::string & strUnitsToOrQuantity
                                  ) {
    assert(_unitConverionTableInitialized);
    if( strUnitsToOrQuantity.empty() ) {
        // look over all the available quantities
        for( const auto & units : _gUnitsByQuantity ) {
            auto it = units.second.find(strUnitsFrom);
            if( units.second.end() != it ) return 1 / it->second;
        }
        throw errors::UnknownUnit(strUnitsFrom);
    }
    auto qIt = _gUnitsByQuantity.find(strUnitsToOrQuantity);
    if(_gUnitsByQuantity.end() != qIt) {
        // second argument is quantity => look for the units
        auto it = qIt->second.find(strUnitsFrom);
        if(qIt->second.end() != it) return it->second;
        throw errors::UnknownUnit(strUnitsFrom, strUnitsToOrQuantity);
    }
    // then both arguments are probably the units => find factors and return
    // ratio, assuring both are of the same quantity
    for( const auto & units : _gUnitsByQuantity ) {
        auto it1 = units.second.find(strUnitsFrom)
           , it2 = units.second.find(strUnitsToOrQuantity)
           ;
        if( it1 == it2 ) {
            if(it1 == units.second.end()) continue;
            return 1;
        }
        if( it1 == units.second.end() || it2 == units.second.end() )
            throw errors::QuantityMismatch(strUnitsFrom, strUnitsToOrQuantity);
        return it1->second / it2->second;
    }
    throw errors::UnknownUnit(strUnitsFrom, strUnitsToOrQuantity);
}


}  // namespace ::na64dp::util
}  // namespace na64dp

