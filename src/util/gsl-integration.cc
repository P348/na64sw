#include "na64util/gsl-integration.hh"

#include <gsl/gsl_errno.h>

namespace na64dp {
namespace errors {

GSLError::GSLError( int gslErrorCode_
                  , const char * msg )
    : GenericRuntimeError(msg)
    , gslErrorCode(gslErrorCode_)
    {}

GSLError::GSLError( int gslErrorCode_ )
    : GenericRuntimeError( util::format("GSL error: (%d) \"%s\""
                                       , gslErrorCode_
                                       , gsl_strerror(gslErrorCode_)
                                       ).c_str() )
    , gslErrorCode(gslErrorCode_)
    {}

void
GSLError::na64sw_gsl_error_handler( const char * reason
                                  , const char * file
                                  , int line
                                  , int gslErrNo ) {
    throw GSLError( gslErrNo
                  , util::format("GSL error at %s:%d: (%d) \"%s\","
                        " reason: \"%s\"", file, line, gslErrNo
                        , gsl_strerror(gslErrNo)
                        , reason ).c_str() );
}

}  // namespace ::na64dp::errors
}  // namespace na64dp

