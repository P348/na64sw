#include "na64util/str-fmt.hh"

#include <cstdarg>
#include <vector>
#include <map>
#include <unordered_set>
#include <cassert>
#include <cmath>
#include <cstring>
#include <cstdint>

#include <iostream>  // XXX

namespace na64dp {
namespace util {

std::string
str_replace( std::string src
           , const std::string & search
           , const std::string & replace
           ) {
    size_t pos = 0;
    while((pos = src.find(search, pos)) != std::string::npos) {
         src.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return src;
}


const StringSubstFormat gDefaultStrFormat = { "{", "}" };

/** This function reproduces pure C `printf()` behaviour, accepting a format
 * string and arbitrary number of arguments to produce output as an
 * `std::string` instance.
 *
 * Although the output string is allocated on heap, initial buffer is restrcted
 * by number defined by `NA64DP_STR_FMT_LENGTH` macro. It will be extended, but
 * this fact may have importance for performance concerns.
 * */
std::string
format(const char *fmt, ...) throw() {
    va_list args;
    va_start(args, fmt);
    std::vector<char> v(NA64DP_STR_FMT_LENGTH);
    while(true) {
        va_list args2;
        va_copy(args2, args);
        int res = vsnprintf(v.data(), v.size(), fmt, args2);
        if ((res >= 0) && (res < static_cast<int>(v.size()))) {
            va_end(args);
            va_end(args2);
            return std::string(v.data());
        }
        size_t size;
        if (res < 0)
            size = v.size() * 2;
        else
            size = static_cast<size_t>(res) + 1;
        v.clear();
        v.resize(size);
        va_end(args2);
    }
}

std::string
str_subst( const std::string & tmplt
         , const std::map<std::string, std::string> & context
         , bool requireCompleteness
         , const StringSubstFormat * fmt
         ) {
    std::string r(tmplt);
    // Iterate over all entries within a context (we assume context to be
    // short).
    for( auto entry : context ) {
        std::size_t pos;
        const std::string key = fmt->bgnMarker + entry.first + fmt->endMarker
                        , & val = entry.second;
        // Unil there is no more occurances of the string, perform search and
        // substitution
        while( std::string::npos != (pos = r.find(key)) ) {
            r = r.replace( pos, key.length(), val );
        }
    }
    if( requireCompleteness ) {
        assert_str_has_no_fillers(r, fmt);
    }
    return r;
}

static const std::unordered_set<std::string>
    _trueToks = { "true", "1", "yes", "enable", "on" },
    _falseToks = { "false", "0", "no", "disable", "off" };

bool
str_to_bool(std::string expr) {
    trim(expr);
    std::transform(expr.begin(), expr.end(), expr.begin(),
        [](unsigned char c){ return std::tolower(c); });
    if( _trueToks.find(expr)  !=  _trueToks.end() ) return true;
    if( _falseToks.find(expr) != _falseToks.end() ) return false;
    throw errors::InvalidOptionExpression(expr);
}

/** We consider any of curly brackets `{}` to be a remnant of template string
 * markup and return `true` if any of them is found in string
 *
 * \todo shall we imply some advanced logic here?
 * \todo Support `StringSubstFormat` (doesn't work for non-default)
 * */
bool
str_has_no_fillers( const std::string & s
                  , const StringSubstFormat * fmt ) {
    return std::string::npos == s.find_first_of("{}");
}

/** Uses `str_has_no_fillers()` to check if string is consistent and raises
 * `StringIncomplete` exception if expectation is not fullfilled.
 *
 * \todo Support `StringSubstFormat` (doesn't work for non-default)
 * */
void
assert_str_has_no_fillers( const std::string & s
                         , const StringSubstFormat * fmt ) {
    if( str_has_no_fillers(s) ) return;
    // find the first problematic token, if exists
    size_t sPos = 0;
    do {
        size_t tokBgn = s.find('{', sPos);
        if(tokBgn == std::string::npos) return;  // no open bracket
        if(tokBgn > 1 && s[tokBgn-1] == '\\' && tokBgn != s.size()) {
            sPos = tokBgn + 1;
            continue;  // escaped open bracket
        }
        size_t tokEnd = s.find('}', tokBgn);
        if(tokEnd > tokBgn)
            throw errors::StringIncomplete(s, s.substr(tokBgn, tokEnd));
    } while(false);
}

/** Examples:
 *      "one two" -> ("one", "two")
 *      ""some another" vlah" -> ("some another", "blah")
 * */
std::vector<std::string>
tokenize_quoted_expression(const std::string & strExpr) {
    std::vector<std::string> tokens;
    const char * tokBgn = nullptr;
    for( const char *c = strExpr.c_str(); '\0' != *c; ++c ) {
        if( ' ' == *c ) {
            if( !tokBgn ) continue;  // omit spaces between tokens
            // otherwise, push token
            tokens.push_back( std::string(tokBgn, c - tokBgn) );
            tokBgn = nullptr;
        } else if( '"' == *c && !tokBgn ) {
            tokBgn = ++c;
            // comma starts escaped sequence -- traverse for next comma
            while( *c != '"' ) {
                if( *c == '\0' )
                    throw std::runtime_error("Unbalanced quotes.");
                ++c;
            }
            // Push token
            tokens.push_back( std::string(tokBgn, c - tokBgn) );
            tokBgn = nullptr;
        } else {
            if( !tokBgn ) tokBgn = c;
        }
    }
    // push back last token in string
    if(tokBgn)
        tokens.push_back( tokBgn );
    return tokens;
}

size_t
seconds_to_hmsm( float seconds
               , char * dest , size_t destLen ) {
    size_t nWritten = 0;
    size_t intSecondsOverall = round(seconds)
         , hh = intSecondsOverall/3600;
    uint32_t mm = (intSecondsOverall%3600)/60
           , ss = intSecondsOverall%60
           , ms = size_t(seconds*1000)%1000
           ;
    char * c = dest;
    if(hh) {
        nWritten = snprintf(c, destLen, "%zuh", hh);
        if(nWritten >= destLen) return nWritten;
        c += nWritten;
    }
    if(mm) {
        nWritten = snprintf( c, destLen - (c - dest)
                           , "%02u:", mm);
        if(nWritten >= destLen) return nWritten;
        c += nWritten;
    }
    nWritten = snprintf( c, destLen - (c - dest)
                       , "%02u.%03u", ss, ms );
    if(nWritten >= destLen) return nWritten;
    c += nWritten;
    return c - dest;
}

time_t
parse_timedate_strexpr( const char * c ) {
    // Parse timedate string into GNU POSIX ::tm instance
    // Note, that these time strings are given in local CERN-Prevessin EHN1
    // location's timezone (Europe/Paris). To get the true timestamp (UTC)
    // one need to convert it using timezone info
    ::tm tm;
    memset(&tm, 0, sizeof(tm));
    strptime(c, "%Y-%m-%d-%H:%M:%S", &tm);  // TODO: configurable macro
    //printf( "xxx %s\n", tm.tm_zone );
    // Not set by strptime(); tells mktime() to determine whether daylight
    // saving time is in effect:
    tm.tm_isdst = -1;
    // Force the measurement timezone
    //tm.tm_zone = "Europe/Paris";  // does not work
    // Convert to time_t
    time_t t = mktime(&tm);
    if(-1 == t) {
        return 0;
    }
    //printf( "%s => year=%d, month=%d, date=%d, time=%d:%d:%d ; timestamp=%ld\n"
    //          , c
    //          , 1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday
    //          , tm.tm_hour, tm.tm_min, tm.tm_sec
    //          , t
    //          );  // xxx
    return t;
}

std::string
str_format_timedate(const time_t & t) {
    char bf[128];
    struct tm tm;
    gmtime_r(&t, &tm);
    strftime( bf, sizeof(bf)
            , "%Y-%m-%d-%H:%M:%S"  // TODO: configurable macro
            , &tm );
    return bf;
}

}  // namespace ::na64dp::util
}  // namespace na64dp

