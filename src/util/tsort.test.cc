#include "na64util/tsort.hh"

#include <gtest/gtest.h>

namespace na64dp {

TEST(TopologicalSort, resolvesTrivialCase) {
    util::DAG<char> g;
    g.add('A', 'B');
    auto r = g.sorted();
    ASSERT_EQ(r.size(), 2);
    EXPECT_EQ(r[0], 'A');
    EXPECT_EQ(r[1], 'B');
}

TEST(TopologicalSort, resolvesDiamondCase) {
    util::DAG<char> g;
    g.add('A', 'B');
    g.add('A', 'C');
    g.add('B', 'D');
    g.add('C', 'D');
    auto r = g.sorted();

    #if 0
    std::cout << "  result:";
    for( auto c : r ) {
        std::cout << " " << c;
    }
    std::cout << std::endl;
    #endif

    ASSERT_EQ(r.size(), 4);
    EXPECT_EQ(r[0], 'A');
    #if 0
    EXPECT_THAT((std::array{ 'B', 'C' }), Contains(r[1]));
    EXPECT_THAT((std::array{ 'B', 'C' }), Contains(r[2]));
    #else  // ^^^ TODO: for gtest >=1.10
    EXPECT_TRUE( (('B' == r[1] && 'C' == r[2]))
              || (('C' == r[1] && 'B' == r[2]))
               );
    #endif
    EXPECT_EQ(r[3], 'D');
}

TEST(TopologicalSort, resolvesComplexCase) {
    util::DAG<char> g;
    // TODO: messed with "product" and "dependency" -- must be vice-versa
    struct {
        char product;
        char deps[4];
    } deps[] = {
        {'Q', "I",},
        {'R', "I",},
        {'S', "IJK",},
        {'T', "L",},
        {'U', "TN",},
        {'V', "NG",},
        {'H', "D",},
        {'I', "D",},
        {'J', "DE",},
        {'K', "E",},
        {'L', "E",},
        {'M', "EF",},
        {'N', "F",},
        {'O', "FG",},
        {'P', "G",},
        {'D', "B",},
        {'E', "B",},
        {'F', "BC",},
        {'G', "C",},
        {'B', "A",},
        {'C', "A",},
        {'\0', ""},
    };

    std::map<char, std::string> chck;
    for(auto c = deps; '\0' != c->product; ++c) {
        for( auto cc = c->deps; '\0' != *cc; ++cc ) {
            g.add(*cc, c->product);
        }
        chck[c->product] = c->deps;
    }

    auto r = g.sorted();
    ASSERT_TRUE(g.empty());
    ASSERT_EQ(r.size(), 22);

    std::string produced;
    for( auto c : r ) {
        auto it = chck.find(c);
        if(it != chck.end()) {
            for( auto dep : it->second ) {
                bool wasNotProduced = produced.find(dep) == std::string::npos;
                EXPECT_FALSE( wasNotProduced ) << "product '" << c
                    << "' depends on '" << dep << "' but it was not produced";
            }
        }
        produced += c;
    }

    #if 0
    std::cout << "  result:";
    for( auto c : r ) {
        std::cout << " " << c;
    }
    std::cout << std::endl;
    #endif

    #if 0
    ASSERT_EQ(r.size(), 7);
    EXPECT_EQ(r[0], '0');
    // ...
    EXPECT_EQ(r[6], '6');
    #else  // ^^^ TODO: for gtest >=1.10
    // ...
    #endif
}

TEST(TopologicalSort, resolvesSpecialCase) {
    // somehow caused troubles
    util::DAG<std::string> g;

    g.add( "naming"      , "GEMMonitor/APVTiming"   );
    g.add( "MuMegaLayout", "GEMMonitor/APVTiming"   );
    g.add( "naming"      , "GEMMonitor/APVPedestals");
    g.add( "MuMegaLayout", "GEMMonitor/APVPedestals");
    g.add( "naming"      , "p348reco/CaloCellCalib" );
    g.add( "naming"      , "placements"             );
    g.add( "naming"      , "MuMegaLayout"           );
    g.add( "naming"      , "masterTimeSource"       );

    auto r = g.sorted();
    EXPECT_TRUE(g.empty());
}

}



