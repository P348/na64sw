#include "na64util/selector.hh"
#include "na64detID/TBName.hh"

#include <climits>
#include <gtest/gtest.h>

namespace na64dp {
namespace test {

typedef dsul::Selector<unsigned int, std::map<std::string, dsul::CodeVal_t>> TestingSelector;

static int
testing_extern_getter( const char * tok
                     , const void * resolverPtr
                     , dsul::CodeVal_t * cv ) {
    const std::map<std::string, dsul::CodeVal_t> * dct = 
        reinterpret_cast<const std::map<std::string, dsul::CodeVal_t> *>(resolverPtr);
    auto it = dct->find(tok);
    if( dct->end() == it ) {
        return 0;
    }
    *cv = it->second;
    return 1;
}

static dsul::CodeVal_t
first( dsul::Principal_t pv ) {
    return ((uintptr_t) pv) & UCHAR_MAX;
}

static dsul::CodeVal_t
second( dsul::Principal_t pv ) {
    return ((uintptr_t) pv) & (((uintptr_t) UCHAR_MAX) << 8);
}

static dsul::CodeVal_t
last( dsul::Principal_t ) {
    return 0;  // TODO
}

TestingSelector::SymDef gTestGetters[] = {
    { "+", { .resolve_name = testing_extern_getter }, "" },
    { "$first", { first }, "" },
    { "$second", { second }, "" },
    { "~last", { last }, "" },
    { NULL, {NULL}, NULL }
};

static bool
test_f1( unsigned int pv ) {
    unsigned int first_ = UCHAR_MAX & pv
           , second_ = ((uintptr_t) pv) & (((uintptr_t) UCHAR_MAX) << 8);
    return second_ == 85 && ((first_ >= 64 && first_ <= 128) || first_ == 255);
}

static struct expressionTest {
    const char * expression;
    bool (*c_func)(unsigned int);
    unsigned int pv[8];
} exprTests[] = {
    {
        "second == 85 && ((first >= 64 && first <= 128) || first == 255)",
        test_f1,
        {
            0x0,
            0x5500,
            0x5540,
            0x40,
            0x80,
            0x5580,
            0x55ff,
            0x55ff,
        }
    },
    #if 0  // well, whatever ....
    {
        "((first == 1 && second == 12) || (first == 12 && second == 1)) && last != 1",
        test_f1,
        {
            
        }
    },
    #endif
    { nullptr, nullptr, {0} }  // sentinel
};

/* This test operates with principal value of four independent bytes packed
 * into a single "unsigend int" value */
TEST(Selectors, Basic) {
    std::map<std::string, dsul::CodeVal_t> externVariables;

    for( auto et = exprTests; et->expression; ++et ) {
        TestingSelector * selPtr = nullptr;
        try {
             selPtr = new TestingSelector( et->expression
                                         , gTestGetters
                                         , externVariables );
        } catch( const errors::SelectorBuildupFailure & e ) {
            std::cerr << e.what();
            throw;
        }
        for( int i = 0; i < 8; ++i ) {
            ASSERT_EQ( selPtr->matches( et->pv[i] )
                     , et->c_func( et->pv[i] ) )
                << " on example #" << (exprTests - et) << ":" << i
                << ", ev. table dump:" << std::endl
                << *selPtr;
        }
    }
}

/* Tests detector ID type selectors with name caching (actually, is the main
 * purpose of DSuL). */

// Testing ficture
class MockNaming : public ::testing::Test {
protected:
    nameutils::DetectorNaming _naming;
public:
    virtual void SetUp() {
        // Define chips
        {
            auto & ct = 
                _naming.chip_add( "CHIP1", 0x1 );
            ct.description = "Testing chip #1";
            ct.pathFormat = "{kin}{statNum2}/{hist}";
        }
        {
            auto & ct =
                _naming.chip_add( "CHIP2", 0x2 );
            ct.description = "Testing chip #2";
            ct.pathFormat = "{kin}{statNum2}/{hist}";
        }
        // Define kins for chips
        _naming.define_kin( "ONE", 0x1, "CHIP1" );
        _naming.define_kin( "TWO", 0x2, "CHIP1" );
        _naming.define_kin( "THREE", 0x1, "CHIP2" );
        _naming.define_kin( "FOUR", 0x2, "CHIP2" );
    }
};

// Tests basic correspondance
TEST_F(MockNaming, BasicSelection) {
    DetSelect s1( "chip == CHIP1 && chip == 1 && kin == TWO"
                , util::gDetIDGetters, _naming );
    ASSERT_TRUE( s1( DetID(0x1, 0x2, 1, 0) ) )
        << "Dump of the evaluation workspace:" << std::endl
        << s1
        << "Dump end" << std::endl;
}

// Tests that DSuL is capable to differ kins with the same numerical ID but
// belonging to different chips
TEST_F(MockNaming, KinDiffers) {
    DetSelect s1( "(kin == ONE && kin != THREE) || (kin == TWO && kin != FOUR)"
              " || (kin == THREE && kin != ONE) || (kin == FOUR && kin != TWO)"
                , util::gDetIDGetters, _naming );
    for( int detChip = 1; detChip < 3; ++detChip ) {
        for( int detKin = 1; detKin < 3; ++detKin ) {
            EXPECT_TRUE( s1( DetID(detChip, detKin, 1, 0) ) )
                << "Dump of the evaluation workspace:" << std::endl
                << s1
                << "Dump end" << std::endl;
        }
    }
}

}
}

