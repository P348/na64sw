#include "na64util/uri.hh"
#include "na64util/str-fmt.hh"

#include <wordexp.h>
#include <regex>

namespace na64dp {
namespace errors {
InvalidURI::InvalidURI(const char * es) throw() : GenericRuntimeError(es) {}
}  // namespace ::na64dp::errors
namespace util {

std::vector<std::string>
expand_names( const std::string & expr ) {
    std::vector<std::string> r;
    wordexp_t p;
    char ** w;
    int wordexpResult = wordexp( expr.c_str(), &p, WRDE_UNDEF | WRDE_NOCMD );
    switch(wordexpResult) {
        case WRDE_BADCHAR: {
            NA64DP_RUNTIME_ERROR("wordexp(\"%s\") error: illegal character"
                    " occurence", expr.c_str() );
        } break;
        case WRDE_BADVAL: {
            NA64DP_RUNTIME_ERROR("wordexp(\"%s\") error: undefined shell"
                    " variable referenced", expr.c_str());
        } break;
        case WRDE_CMDSUB: {
            NA64DP_RUNTIME_ERROR("wordexp(\"%s\") error: command substitution"
                    " prohibited", expr.c_str());
        } break;
        case WRDE_NOSPACE: {
            NA64DP_RUNTIME_ERROR("wordexp(\"%s\") error: out of memory"
                    , expr.c_str());
        } break;
        case WRDE_SYNTAX: {
            NA64DP_RUNTIME_ERROR("wordexp(\"%s\") error: shell syntax error"
                    , expr.c_str());
        } break;
    };
    w = p.we_wordv;
    if( 0 == p.we_wordc ) {
        wordfree( &p );
        return r;
    }
    r.reserve(p.we_wordc);
    for( size_t i=0; i < p.we_wordc; i++ ) {
        r.push_back(w[i]);
    }
    wordfree( &p );
    return r;
}

std::string
expand_name(const std::string & expr) {
    auto l = expand_names(expr);
    if(l.empty())
        NA64DP_RUNTIME_ERROR( "Expression \"%s\" expanded as none strings, but"
                " single non-empty string expected."
                , expr.c_str() );
    if(l.size() > 1)
        NA64DP_RUNTIME_ERROR( "Expression \"%s\" expanded as %zu entries, but"
                " single non-empty string expected."
                , expr.c_str(), l.size() );
    if(l[0].empty())
        NA64DP_RUNTIME_ERROR( "Expression \"%s\" expanded as empty string, but"
                " non-empty string expected."
                , expr.c_str() );
    return l[0];
}


std::unordered_multimap<std::string, std::string>
URI::parse_query_string( const std::string & str
                       , const std::string & rxS
                       ) {
    std::regex rx(rxS);
    std::unordered_multimap<std::string, std::string> result;
    if(str.empty()) return result;
    auto const vec = std::vector<std::string>(
            std::sregex_token_iterator{begin(str), end(str), rx, -1},
            std::sregex_token_iterator{}
        );
    for(auto entry : vec) {
        size_t nEq = entry.find('=');
        if(std::string::npos != nEq)
            result.emplace( entry.substr(0, nEq), entry.substr(nEq+1) );
        else
            result.emplace( entry, "");
    }
    return result;
}

void
URI::scheme(const std::string & scheme_) {
    _scheme = scheme_;
}

void
URI::port(const std::string & strPortNo) {
    // TODO: check only int
    _port = strPortNo;
}

void
URI::port(uint16_t portNo) {
    _port = std::to_string(portNo);
}

void
URI::host(const std::string & host_) {
    _host = host_;
}

void
URI::path(const std::string & path_) {
    // TODO: check validity
    _path = path_;
}

//
// URI and related

//static const std::string _2encode = " !\"#$%&'()*+,/:;=?@[]";

std::string
URI::encode(const std::string & s) {
    std::string encoded;
    encoded.reserve(s.size());
    for(const char * c = s.c_str(); *c != '\0'; ++c) {
        if( isalnum(*c)
         || *c == '-'
         || *c == '_'
         || *c == '.'
         || *c == '~' ) {
            encoded += *c;
            continue;
        } else {
            char bf[8];
            snprintf(bf, sizeof(bf), "%02X", (int) *c);
            encoded += '%';
            encoded += bf;
        }
    }
    return encoded;
}

std::string
URI::decode(const std::string & s) {
    std::string decoded;
    decoded.reserve(s.size());
    for(const char * c = s.c_str(); '\0' != *c; ++c) {
        if(*c != '%') {
            if('+' != *c)
                decoded += *c;
            else
                decoded += ' ';
        } else {
            if(c[1] == '\0' || c[2] == '\0') {
                throw std::runtime_error("Bad string to decode (%%-encoding truncated)");
            }
            int code;
            char pbf[] = { c[1], c[2], '\0' };
            sscanf(pbf, "%02X", &code);
            if(code > 0xff) {
                throw std::runtime_error("Could not url-decode code large"
                        " number (extended set?)");
            }
            decoded += (char) code;
            c += 2;
        }
    }
    return decoded;
}

// See: https://www.rfc-editor.org/rfc/rfc3986#appendix-B
static const std::regex _rxURI(
        R"~(^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?)~");
//           12            3  4          5       6  7        8 9

URI::URI(const std::string & strUri) {
    std::smatch m;
    if(!std::regex_match(strUri, m, _rxURI)) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "String does not match URI format: \"%s\""
                , strUri.c_str() );
        throw errors::InvalidURI(errBf);
    }

    std::vector<std::string> groups(m.begin(), m.end());

    _scheme   = groups[2];
    //_host     = groups[4];
    _path     = groups[5];
    _qParams  = parse_query_string(groups[7]);
    _fragment = groups[9];

    std::string authority = groups[4];
    if(!authority.empty()) {
        size_t n = authority.find('@');
        if(n != std::string::npos) {
            _userinfo = authority.substr(0, n);
        }
        if(n == std::string::npos) n = 0; else ++n;
        size_t nn = authority.find(':', n);
        if(nn != std::string::npos) {
            _host = authority.substr(n, nn - n );
            _port = authority.substr(++nn);
        } else {
            _host = authority.substr(n);
        }
    }

    #if 0  // might be helpful in debug
    std::cout << "    scheme: \"" << _scheme << "\"" << std::endl
              << " authority: \"" << authority << "\"" << std::endl
              << "          userinfo: \"" << _userinfo << "\"" << std::endl
              << "              host: \"" << _host << "\"" << std::endl
              << "              port: \"" << _port << "\"" << std::endl
              << "      path: \"" << _path << "\"" << std::endl
              << "     query: \"" << _query << "\"" << std::endl
              << "  fragment: \"" << _fragment << "\"" << std::endl
              ;
    #endif
}

std::string
URI::authority() const {
    std::ostringstream oss;
    if( !_userinfo.empty() ) oss << _userinfo << '@';
    oss << ( _host.empty()
           ? ((_port.empty() && _userinfo.empty()) ? "" : "localhost")
           : _host
           );
    if( !_port.empty()) oss << ':' << _port;
    return oss.str();
}

std::string
URI::query_str() const {
    std::ostringstream oss;
    bool isFirst = true;
    for(auto entry : _qParams) {
        if(isFirst) isFirst = false; else oss << "&";
        oss << entry.first << "=" << entry.second;
    }
    return oss.str();
}

std::string
URI::to_str(bool noCheck) const {
    std::ostringstream oss;

    if(!_scheme.empty()) oss << _scheme << ':';
    std::string auth = authority();
    if(!auth.empty()) {
        oss << "//" << authority();
    }
    if(!_path.empty()) oss << _path;
    if(!_qParams.empty()) oss << '?' << query_str();
    if(!_fragment.empty()) oss << '#' << _fragment;

    std::string s = oss.str();
    if(!noCheck) {
        if(!std::regex_match(s, _rxURI)) {
            char errBf[128];
            snprintf( errBf, sizeof(errBf)
                    , "Incomplete URI: \"%s\""
                    , s.c_str() );
            throw errors::InvalidURI(errBf);
        }
    }
    return s;
}

}  // namespace ::na64dp::util
}  // namespace na64dp
