#include "na64util/log4cpp-extras.hh"

#include <log4cpp/Category.hh>
#include <log4cpp/AppendersFactory.hh>
#include <log4cpp/OstreamAppender.hh>

#include <regex>
#include <iomanip>
#include <cassert>

#include <memory>
#include <ctime>
#include <cstring>

#include <unistd.h>

#include "na64util/str-fmt.hh"

namespace na64dp {
namespace util {

StdoutStreamAppender * StdoutStreamAppender::_self = nullptr;

StdoutStreamAppender *
StdoutStreamAppender::self() {
    return _self;
}

StdoutStreamAppender::StdoutStreamAppender( const std::string & name
                                          , int fd )
        : log4cpp::LayoutAppender(name)
        , _fd(fd)
        {
    *_statusline = '\0';
    if(_self) {
        throw std::runtime_error("Duplicating isntantiation of layout appender.");
    }
    _self = this;
}

static const char gEraseLine[] = "\033[K"  // "erase in line", from cursor to the end of the line
                , gCurTo0[] = "\033[G"  // moves cursor to 1st column
                ;

void
StdoutStreamAppender::_append(const log4cpp::LoggingEvent & event) {
    std::string formatted = _getLayout().format(event);
    #if 0  // XXX
    assert(1 == _fd);
    write(_fd, formatted.c_str(), formatted.size());
    #else
    if(isatty(_fd) && '\0' != *_statusline) {
        formatted = gEraseLine + formatted;
        write(_fd, formatted.c_str(), formatted.size());
        if(formatted.empty()) return;
        std::lock_guard<std::mutex> guard(_lockStatusline);
        write(_fd, _statusline, strlen(_statusline));
        write(_fd, gCurTo0, sizeof(gCurTo0));
    } else {
        if(formatted.empty()) return;
        write(_fd, formatted.c_str(), formatted.size());
    }
    #endif
}

void
StdoutStreamAppender::update_status_text(const char * msg) {
    std::lock_guard<std::mutex> guard(_lockStatusline);
    if(isatty(_fd)) {
        write(_fd, gEraseLine, sizeof(gEraseLine));
        write(_fd, msg, strlen(msg));
        write(_fd, gCurTo0, sizeof(gCurTo0));
    }
    strncpy(_statusline, msg, sizeof(_statusline));
}

Log4cppAppendersFactoryResult_t
create_ostream_appender(const log4cpp::AppendersFactory::params_t & p) {
    std::string name; //, statuslineFormat;
    //bool enableStatusline = true;
    p.get_for("stdout appender")
        .required("name", name)
        //.optional("statusline", enableStatusline)
        //         ("statuslineFormat", statuslineFormat)
        ;
    //return Log4cppAppendersFactoryResult_t(new log4cpp::OstreamAppender(name, &std::cout));
    return Log4cppAppendersFactoryResult_t(
            new StdoutStreamAppender(name));
}

void
inject_extras_to_log4cpp() {
    log4cpp::AppendersFactory::getInstance().registerCreator( "stdout"
                , create_ostream_appender );
    log4cpp::LayoutsFactory::getInstance().registerCreator("extended-pattern"
            , PatternLayout::create );

    //#if defined(Geant4_FOUND) && Geant4_FOUND
    //log4cpp::AppendersFactory::getInstance().registerCreator( "Geant4 UI"
    //            , Geant4UISessionAppender::create );
    //assert(log4cpp::AppendersFactory::getInstance().registered("Geant4 UI"));
    //#endif
    // ^^^ TODO: move it to extensions/mc
}


//
// Custom pattern layout

struct StaticComponent : public PatternLayout::iComponent {
    const std::string str;
    StaticComponent(const std::string & s) : str(s) {}
    virtual void append_msg(const log4cpp::LoggingEvent &, std::ostringstream & oss) override
        { oss << str; }

    static iComponent * create_endl(const std::unordered_map<std::string, std::string> &) {
        std::ostringstream oss;
        oss << std::endl;
        return new StaticComponent(oss.str());
    }
};


struct CategoryComponent : public PatternLayout::iComponent {
    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream & oss) override
        { oss << lev.categoryName; }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        return new CategoryComponent();
    }
};


struct MessageComponent : public PatternLayout::iComponent {
    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream & oss) override
        { oss << lev.message; }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        return new MessageComponent();
    }
};


struct NDCComponent : public PatternLayout::iComponent {
    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream & oss) override
        { oss << lev.ndc; }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        return new NDCComponent();
    }
};


struct PriorityComponent : public PatternLayout::iComponent {
    /// Priority name will be trancated to a single letter
    bool isShort;

    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream &) override;

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        auto ptr = new PriorityComponent();
        auto it = p.find("short");
        if(it != p.end() && (it->second.empty() || it->second != "false")) {
            ptr->isShort = true;
        } else {
            ptr->isShort = false;
        }
        return ptr;
    }
};

void
PriorityComponent::append_msg( const log4cpp::LoggingEvent & lev
                                            , std::ostringstream & oss
                                            ) {
    std::string nm = log4cpp::Priority::getPriorityName(lev.priority);
    if(isShort) {
        nm = nm.substr(0, 1);
    }
    oss << nm;
}

struct ThreadNameComponent : public PatternLayout::iComponent {
    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream & oss) override
        { oss << lev.threadName; }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        return new ThreadNameComponent();
    }
};

struct TimeSinceStartComponent : public PatternLayout::iComponent {
    bool raw;
    TimeSinceStartComponent(bool raw_) : raw(raw_) {}
    virtual void append_msg(const log4cpp::LoggingEvent & event, std::ostringstream & out) override {
        double t = event.timeStamp.getSeconds() -
             log4cpp::TimeStamp::getStartTime().getSeconds();
        t += ( event.timeStamp.getMilliSeconds() -
             - log4cpp::TimeStamp::getStartTime().getMilliSeconds())*1e-3;
        if(raw) {
            out << std::setiosflags(std::ios::fixed)
                 << std::setprecision(0) << t;
            return;
        }
        char buf[64];
        util::seconds_to_hmsm(t, buf, sizeof(buf));
        out << buf;
    }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        auto it = p.find("raw");
        if(p.end() != it && it->second != "false") {
            return new TimeSinceStartComponent(true);
        } else {
            return new TimeSinceStartComponent(false);
        }
    }
};

struct CPUTimeComponent : public PatternLayout::iComponent {
    virtual void append_msg(const log4cpp::LoggingEvent & lev, std::ostringstream & oss) override
        { oss << std::clock(); }

    static iComponent * create(const std::unordered_map<std::string, std::string> & p) {
        return new CPUTimeComponent();
    }
};

static const struct {
    const std::string s[4];
} gColors[] = {
    // title-bgn, text-bgn, title-end, text-end
    { "\e[1;5;37;41m", "\e[0;1;31m"  "\e[0m",  "\e[0m"},  // FATAL
    { "\e[1;5;37;41m", "\e[1;31m",   "\e[0m",  "\e[0m"},  // ALERT
    { "\e[1;43;31m",   "\e[1;31m",   "\e[0m",  "\e[0m"},  // CRIT
    { "\e[1;31m",      "\e[31m",     "\e[0m",  "\e[0m"},  // ERROR
    { "\e[1;2;31;43m", "\e[1m",      "\e[0m",  "\e[0m"},  // WARN
    { "\e[1;30;46m",   "\e[1m",      "\e[0m",  "\e[0m"},  // NOTICE
    { "\e[0;32m",      "\e[0m",      "\e[0m",  "\e[0m"},  // INFO
    { "\e[2;36m",      "\e[2m",      "\e[0m",  "\e[0m"},  // DEBUG
    { "\e[1;35m",      "\e[0m",      "\e[0m",  "\e[0m"},  // NOTSET
};

struct SuffixComponent : public PatternLayout::iComponent {
    int n;
    SuffixComponent(int n_) : n(n_) {
        assert(n < 4);
    }

    virtual void append_msg( const log4cpp::LoggingEvent & lev
                           , std::ostringstream & oss
                           ) override {
        int numP = static_cast<int>(lev.priority);
        if(numP > static_cast<int>(log4cpp::Priority::NOTSET)) {
            numP = log4cpp::Priority::NOTSET;
        }
        numP /= 100;
        assert((size_t) numP < sizeof(gColors)/sizeof(*gColors));
        oss << gColors[numP].s[n];
    }

    static iComponent * create_title_begin(const std::unordered_map<std::string, std::string> &) {
        auto ptr = new SuffixComponent(0);
        return ptr;
    }
    static iComponent * create_text_begin(const std::unordered_map<std::string, std::string> &) {
        auto ptr = new SuffixComponent(1);
        return ptr;
    }
    static iComponent * create_title_end(const std::unordered_map<std::string, std::string> &) {
        auto ptr = new SuffixComponent(2);
        return ptr;
    }
    static iComponent * create_text_end(const std::unordered_map<std::string, std::string> &) {
        auto ptr = new SuffixComponent(3);
        return ptr;
    }
};


//
// Layout

Log4cppLayoutFactoryResult_t
PatternLayout::create(const log4cpp::LayoutsFactory::params_t & params) {
    PatternLayout * p = new PatternLayout();
    auto it = params.find("pattern");
    if(params.end() == it) {
        throw std::runtime_error("Parameter \"pattern\" is not given.");
    }
    p->set_pattern(it->second);

    Log4cppLayoutFactoryResult_t pp;
    pp.reset(p);
    return pp;
}

PatternLayout::PatternLayout() {
    _constructors.emplace("category",   CategoryComponent::create);
    _constructors.emplace("message",    MessageComponent::create);
    _constructors.emplace("ndc",        NDCComponent::create);
    _constructors.emplace("priority",   PriorityComponent::create);
    _constructors.emplace("thread-name",ThreadNameComponent::create);
    _constructors.emplace("cpu-time",   CPUTimeComponent::create);
    _constructors.emplace("run-time",   TimeSinceStartComponent::create);
    _constructors.emplace("endl",       StaticComponent::create_endl);

    _constructors.emplace("title-b",    SuffixComponent::create_title_begin);
    _constructors.emplace("title-e",    SuffixComponent::create_title_end);
    _constructors.emplace("txt-b",      SuffixComponent::create_text_begin);
    _constructors.emplace("txt-e",      SuffixComponent::create_text_end);
}

std::string
PatternLayout::format(const log4cpp::LoggingEvent & lev) {
    std::ostringstream oss;
    for(auto cPtr : _components) {
        cPtr->append_msg(lev, oss);
    }
    return oss.str();
}

static const std::regex gRxExprAsDelim(R"~(%\{[^}]+\}%)~")
    , gRxFullExpression(R"~(%\{\s*([^:\s]+)\s*(?:\:\s*([^}]+)\s*)?\}%)~")
    , gRxArgsExprDelim(R"~(\s*,\s*)~")
    , gRxValuedOption(R"~(^([^\s]+)\s*=\s*(.+)$)~")
    , gRxLogicOption(R"~([\-_a-zA-Z0-9]+)~")
    ;

void
PatternLayout::set_pattern(const std::string & strexpr) {
    // a "parameterized delimiter token" of the form %{...}%
	std::sregex_token_iterator str(strexpr.begin(), strexpr.end()
            , gRxExprAsDelim, {-1, 0});
    // fill string with tokens splitted by "parameterized delimiter"
	std::vector<std::string> tokens;
	std::remove_copy_if( str
                       , std::sregex_token_iterator()
                       , std::back_inserter(tokens)
                       , [](std::string const &s) { return s.empty(); }
                       );
    // get parameters from "p.delimiter"
    std::smatch sm;
    // every `p' here correspnds to a component
    for( const auto & p : tokens ) {
        if(!std::regex_match(p, sm, gRxFullExpression)) {
            // no "name", static component
            _components.push_back(new StaticComponent(p));
            continue;
        }
        // retrieve group match by number directly from matchobject
        const std::string componentName = sm[1]
                        , strArgsExpr = sm[2];
        std::unordered_map<std::string, std::string> arguments;
        if(!strArgsExpr.empty()) {
            // split ("arguments") string by comma delimiter, trimming spaces
            std::sregex_token_iterator firstArgIt{ strArgsExpr.begin()
                                                 , strArgsExpr.end()
                                                 , gRxArgsExprDelim
                                                 , -1 }
                                     , lastArgIt;
            for(auto argIt = firstArgIt; lastArgIt != argIt; ++argIt ) {
                std::string optStr(argIt->str());
                if(std::regex_match(optStr, gRxLogicOption)) {
                    // logic option, equivalent to <name>=true
                    auto ir = arguments.emplace(optStr, "true");
                    if(!ir.second) {
                        throw std::runtime_error("Option name \"" + optStr
                                + "\" given twice.");
                    }
                } else if(std::regex_match(optStr, sm, gRxValuedOption)) {
                    auto ir = arguments.emplace(sm[1].str(), sm[2].str());
                    if(!ir.second) {
                        throw std::runtime_error("Option name \"" + sm[1].str()
                                + "\" given twice.");
                    }
                } else {
                    throw std::runtime_error("Bad argument expression \"" + argIt->str() + "\"");
                }  // args loop
            }  // for every argument in list
        }  // if(!strArgsExpr.empty())
        // instantiate component
        auto ctrIt = _constructors.find(componentName);
        if(_constructors.end() == ctrIt) {
            throw std::runtime_error("No log layout component named \""
                    + componentName + "\".");
        }
        _components.push_back(ctrIt->second(arguments));
    }  // tokens
}

PatternLayout::~PatternLayout() {
    for(auto c : _components) {
        delete c;
    }
}

}  // namespace ::na64dp::util
}  // namespace na64dp

