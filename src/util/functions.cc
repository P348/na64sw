#include "na64util/functions.hh"
#include "na64util/str-fmt.hh"

#include <cstdio>
#include <cstring>
#include <stdexcept>

namespace na64dp {
namespace functions {

static bool _gt(double a, double b) { return a >  b; }
static bool _ge(double a, double b) { return a >= b; }
static bool _lt(double a, double b) { return a <  b; }
static bool _le(double a, double b) { return a <= b; }

Comparator get_comparator(const char * strExpr) {
    if( !strcmp(strExpr, ">") || !strcmp(strExpr, "gt") ) {
        return _gt;
    } else if( !strcmp(strExpr, ">=") || !strcmp(strExpr, "ge") ) {
        return _ge;
    } else if( !strcmp(strExpr, "<") || !strcmp(strExpr, "lt") ) {
        return _lt;
    } else if( !strcmp(strExpr, "<=") || !strcmp(strExpr, "le") ) {
        return _le;
    } else {
        NA64DP_RUNTIME_ERROR( "Can not interpret \"%s\" as comparison expression."
                            , strExpr );
    }
}

}
}
