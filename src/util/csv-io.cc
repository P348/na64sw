#include "na64util/csv-io.hh"

#include <cassert>

namespace na64dp {

namespace error {

CSVParsingError::CSVParsingError( const char * what_
                                , size_t nLine
                                , size_t nToken ) throw() : std::runtime_error( what_ )
                                                          , _nLine(nLine)
                                                          , _nToken(nToken) {}

}  // namespace ::na64dp::error

namespace csv {

Reader::Reader( std::istream & is
              , size_t lineCountStart
              , const std::string & tokRx ) : _lineCount(lineCountStart)
                                            , _is(is)
                                            , _wrx(tokRx)
                                            , _lastMatchPos(std::string::npos) {}

std::string
Reader::_get_next_line() {
    std::string line;
    while( std::getline(_is, line) ) {
        ++_lineCount;
        size_t startpos = line.find_first_not_of(" \t");
        // omit blank lines
        if( startpos == std::string::npos ) continue;
        // omit lines starting with `#`
        if( '#' == line[startpos] ) continue;
        return line;
    }
    assert(line.empty());
    return line;  // return empty line (stream depleted)
}

}  // namespace ::na64dp::csv
}  // namespace na64dp


