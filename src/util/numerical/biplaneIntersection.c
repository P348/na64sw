#include "na64util/numerical/biplaneIntersection.h"

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix_float.h>
#include <assert.h>

int
na64sw_projected_lines_intersection( const na64sw_Float_t * k1, const na64sw_Float_t * k2, const na64sw_Float_t * k3
                                   , const na64sw_Float_t * r01, const na64sw_Float_t * r02
                                   , na64sw_Float_t * x1, na64sw_Float_t * x2
                                   ) {
    assert(k1);
    assert(k2);
    double m_[9] = { k1[0], k2[0], 0
                   , k1[1], k2[1], 0
                   , k1[2], k2[2], 0 };
    if(!k3) {
        m_[2] = k1[1]*k2[2] - k1[2]*k2[1];  // a_y*b_z - a_z*b_y
        m_[5] = k1[2]*k2[0] - k1[0]*k2[2];  // a_z*b_x - a_x*b_z
        m_[8] = k1[0]*k2[1] - k1[1]*k2[0];  // a_x*b_y - a_y*b_x
    } else {
        m_[2] = k3[0];
        m_[5] = k3[1];
        m_[8] = k3[2];
    }
    gsl_matrix_view m = gsl_matrix_view_array(m_, 3, 3);
    #if 1  /* SVD-based solution; may be less efficient, but
              detects singular matrices */
    gsl_vector * w = gsl_vector_alloc(3);
    gsl_matrix * V = gsl_matrix_alloc(3, 3);
    gsl_vector * S = gsl_vector_alloc(3);
    gsl_linalg_SV_decomp( &m.matrix, V, S, w);

    /* test if matrix is singular */
    double sMax = gsl_vector_get(S, 0)
         , sMin = gsl_vector_get(S, 0);
    for(int i = 0; i < 2; ++i) {
        const double c = gsl_vector_get(S, i);
        if(sMax < c) sMax = c;
        else if(sMin > c) sMin = c;
    }
    if(sMin/sMax < 1e-12) return 1;

    gsl_vector * r = gsl_vector_alloc(3);
    double cv_[] = { r02[0] - r01[0], r02[1] - r01[1], r02[2] - r01[2] };
    gsl_vector_view cv = gsl_vector_view_array(cv_, 3);
    gsl_linalg_SV_solve( &m.matrix, V, S, &cv.vector, r);
    #else  /* LU decomp version */
    /* invert matrix */
    gsl_permutation * p = gsl_permutation_alloc(3);
    int s;
    gsl_linalg_LU_decomp(&m.matrix, p, &s);
    gsl_matrix * inv = gsl_matrix_alloc(3, 3);
    gsl_linalg_LU_invert(&m.matrix, p, inv);
    gsl_permutation_free(p);
    /* multiply matrix on diff vector to get {u1, -u2, d} */
    double cv_[] = { r02[0] - r01[0], r02[1] - r01[1], r02[2] - r01[2] };
    gsl_vector_view cv = gsl_vector_view_array(cv_, 3);

    gsl_vector * r = gsl_vector_alloc(3);
    gsl_vector_set_zero(r);
    gsl_blas_dgemv( CblasNoTrans, 1.
                  , inv, &cv.vector
                  , 1., r);
    #endif
    const double u1 =  gsl_vector_get(r, 0)
               , u2 = -gsl_vector_get(r, 1)
               //,  d =  gsl_vector_get(r, 2)  // distance is not needed
               ;
    if(x2) {
        for(int i = 0; i < 3; ++i) {
            x1[i] = r01[i] + u1*k1[i];
            x2[i] = r02[i] + u2*k2[i];
        }
    } else {
        for(int i = 0; i < 3; ++i) {
            x1[i] = (r02[i] + u2*k2[i] + r01[i] + u1*k1[i])/2;
        }
    }
    /* This produces gnuplot script that can be used to roughly visualize
     * results in form of parametric planes and arrows. I decided to keep it
     * in case we should make a pic for docs. */
    #if 0
    //printf( "# u1 = %e, u2 = %e, d=%e\n", u1, u2, d );
    printf( "set arrow 1 from %e,%e,%e to %e,%e,%e\n"
          , r01[0], r01[1], r01[2]
          , r01[0] + k1[0], r01[1] + k1[1], r01[2] + k1[2]
          );
    printf( "set arrow 2 from %e,%e,%e to %e,%e,%e\n"
          , r02[0], r02[1], r02[2]
          , r02[0] + k2[0], r02[1] + k2[1], r02[2] + k2[2]
          );
    printf( "set arrow 3 from %e,%e,%e to %e,%e,%e\n"
          , x1[0], x1[1], x1[2]
          , x2[0], x2[1], x2[2]
          );
    printf( "splot %.3e*u + %.3e*v + %.3e, %.3e*u + %.3e*v + %.3e, %.3e*u + %.3e*v + %.3e,"
            "  %.3e*u + %.3e*v + %.3e, %.3e*u + %.3e*v + %.3e, %.3e*u + %.3e*v + %.3e\n"
          , k1[0], k3[0], r01[0]
          , k1[1], k3[1], r01[1]
          , k1[2], k3[2], r01[2]
          //
          , k2[0], k3[0], r02[0]
          , k2[1], k3[1], r02[1]
          , k2[2], k3[2], r02[2]
          );
    #endif

    return 0;
}

#if 0
int
main(int argc, char * argv[]) {
    float k1[] = { 1,  0,  1}
        , k2[] = { 0,  1, -1}
        , k3[] = { 0,  0, -1}
        ;
    float r1[] = { .25,  0, -.25}
        , r2[] = { 0,  1,  1}
        ;
    float res[6];
    plane_intersection(k1, k2, k3, r1, r2, res, res + 3);

    return 0;
}
#endif

