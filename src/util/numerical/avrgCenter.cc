// a draft

#include <iostream>
#include <Eigen/Dense>

static const struct Line {
    float l[3];
    float r0[3];  // note not used in solution, just for plotting
} _lines[] = {
    { {0, 0, 0}, {1, 0, 0} },
    { {0, 0, 0}, {0, 1, 0} },
    { {0, 0, 0}, {0, 0, 1} },
};


int
main(int argc, char * argv[]) {
    const int nLines = sizeof(_lines)/sizeof(struct Line);
    Eigen::MatrixXf m(3*nLines, 2);
    for(int nLine = 0; nLine < nLines; ++nLine) {
        const struct Line & l = _lines[nLine];
        const int i = 3*nLine;
        m(i + 0, 0) = 0;        m(i + 0, 1) = +l.l[2];  m(i + 0, 2) = -l.l[1];
        m(i + 1, 0) = -l.l[2];  m(i + 1, 1) = 0;        m(i + 1, 2) = +l.l[0];
        m(i + 2, 0) = +l.l[1];  m(i + 2, 1) = -l.l[2];  m(i + 2, 2) = 0;
    }

    //Eigen::MatrixXf A = Eigen::MatrixXf::Random(3, 2);
    //std::cout << "Here is the matrix A:\n" << A << std::endl;
    //Eigen::VectorXf b = Eigen::VectorXf::Random(3);
    //std::cout << "Here is the right hand side b:\n" << b << std::endl;
    //std::cout << "The least-squares solution is:\n"
    //          << A.template bdcSvd<Eigen::ComputeThinU | Eigen::ComputeThinV>().solve(b) << std::endl;
}

