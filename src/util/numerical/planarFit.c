#include "na64util/numerical/planarFit.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

#include <stdint.h>
#include <assert.h>

#ifdef MAIN
#   include <string.h>
#   include <math.h>
#endif

static int
_mean_point( na64sw_Float_t * segments[]
           , size_t nSegments
           , size_t nDims
           , na64sw_Float_t * A ) {
    /* TODO: pairwise sum for large `nSegments'? */
    A[0] = A[1] = A[2] = 0;
    assert(3 == nDims);  // XXX
    for(size_t n = 0; n < nSegments; ++n) {
        for(size_t j = 0; j < nDims; ++j) {
            A[j] += .5*(segments[n][j] + segments[n][nDims + j]);
        }
    }
    for(size_t j = 0; j < nDims; ++j) {
        A[j] /= nSegments;
    }
    return 0;
}

int
na64sw_fit_line_segments_w_plane_unweighted(
          na64sw_Float_t * segments[]
        , size_t nSegments
        , size_t nDims
        , na64sw_Float_t * A
        , na64sw_Float_t * N
        ) {
    if(nSegments < 2) return 1;
    int rc = 0;
    /* Calc A as mean point of the line segments extremities (proven global
     * minimum of error function) */
    if(!!(rc = _mean_point(segments, nSegments, nDims, A))) return rc;
    /* Allocate U and Delta arrays to be used in SVD of M which computed as:
     *
     * M = \sum\limits^N \left(  1/3 U U^T + 1/2 (U \Delta^T + \Delta U^T) + \Delta \Delta^T \right)
     * */
    gsl_vector * U      = gsl_vector_alloc(nDims)
             , * Delta  = gsl_vector_alloc(nDims);
    gsl_matrix * M      = gsl_matrix_alloc(nDims, nDims);
    gsl_vector * eigVals = gsl_vector_calloc(nDims);
    gsl_matrix * eigVecs = gsl_matrix_calloc(nDims, nDims);
    gsl_eigen_symmv_workspace * eigws = gsl_eigen_symmv_alloc(nDims);
    size_t eigMinN = SIZE_MAX;
    double NNorm;
    gsl_vector_view leastEigVec;

    gsl_matrix_set_zero(M);
    for(size_t i = 0; i < nSegments; ++i) {
        /* Fill U, Delta */
        for(size_t j = 0; j < nDims; ++j) {
            gsl_vector_set(U,     j, segments[i][nDims + j] - segments[i][j] );
            gsl_vector_set(Delta, j, segments[i][j]         - A[j] );
        }
        gsl_blas_dsyr(  CblasUpper, 1./3, U,        M );  /* M += (1/3) U U^T */
        gsl_blas_dsyr2( CblasUpper,  .5,  U, Delta, M );  /* M += (1/2)  (U \Delta^T + \Delta U^T)*/
        gsl_blas_dsyr(  CblasUpper,   1., Delta,    M );  /* M += Delta Delta^T */
    }
    /* Copy other half of symmetric matrix as gsl_eigen_symmv() seem to use it
     * TODO: more efficient way? (doesn't really matter for small nDim) */
    for(int i=0; i < nDims; i++ ) {
        for(int j=i+1; j<nDims; j++ ){
            gsl_matrix_set(M, j, i, gsl_matrix_get(M, i, j));
        }
    }
    /* Now M has only upper half populated, apply sym SVD */
    gsl_eigen_symmv(M, eigVals, eigVecs, eigws);
    /* Get eigenvector corresponding to least eigenvalue */
    eigMinN = gsl_vector_min_index(eigVals);
    leastEigVec = gsl_matrix_column(eigVecs, eigMinN);
    NNorm = gsl_blas_dnrm2(&leastEigVec.vector);
    for(size_t i = 0; i < nDims; ++i) {
        N[i] = gsl_vector_get(&leastEigVec.vector, i) / NNorm;
    }

    gsl_eigen_symmv_free(eigws);
    gsl_vector_free(U);
    gsl_vector_free(Delta);
    gsl_matrix_free(M);
    return rc;
}

#ifdef MAIN
int
main(int argc, char * argv[]) {
    na64sw_Float_t thisLineSamples[6], ** samples = NULL;
    FILE * samplesFile = fopen(argv[1], "r");
    size_t nSamples = 0;
    while(fscanf( samplesFile, "%e %e %e %e %e %e"
                , thisLineSamples,     thisLineSamples + 1, thisLineSamples + 2
                , thisLineSamples + 3, thisLineSamples + 4, thisLineSamples + 5
                ) == 6) {
        samples = realloc(samples, (nSamples+1)*sizeof(na64sw_Float_t (*) [6]));
        samples[nSamples] = malloc(sizeof(na64sw_Float_t [6]));
        thisLineSamples[3] += thisLineSamples[0];
        thisLineSamples[4] += thisLineSamples[1];
        thisLineSamples[5] += thisLineSamples[2];
        memcpy(samples[nSamples], thisLineSamples, sizeof(na64sw_Float_t [6]));
        #if 0
        printf( "%e %e %e %e %e %e\n"
              , samples[nSamples][0], samples[nSamples][1], samples[nSamples][2]
              , samples[nSamples][3], samples[nSamples][4], samples[nSamples][5]
              );
        #endif
        ++nSamples;
    }
    fclose(samplesFile);

    na64sw_Float_t N[3] = {0, 0, 0}
                 , A[3] = {0, 0, 0};
    na64sw_fit_line_segments_w_plane_unweighted(samples, nSamples, 3, A, N);

    printf( "(%e - %e*x - %e*y)/%e title \"approx\"\n",
                N[0]*A[0] + N[1]*A[1] + N[2]*A[2], N[0], N[1], N[2] );

    return 0;
}
#endif

