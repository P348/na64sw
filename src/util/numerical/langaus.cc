#include "na64util/numerical/langaus.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include <TROOT.h>
#include <TFitResult.h>

namespace na64dp {
namespace util {

const Double_t gLandauFMax = -0.22278298;

Double_t
langaus( Double_t x, Double_t width
                   , Double_t mp
                   , Double_t area
                   , Double_t sigma
                   , Int_t np
                   , Double_t sc
                   ) {
      // Numeric constants
      const Double_t invsq2pi = 1/sqrt(2*M_PI);
      // Variables
      Double_t xx;
      Double_t mpc;
      Double_t fland;
      Double_t sum = 0.0;
      Double_t xlow,xupp;
      Double_t step;
      Double_t i;
      // MP shift correction
      mpc = mp - gLandauFMax * width;
      // Range of convolution integral
      xlow = x - sc * sigma;
      xupp = x + sc * sigma;
      step = (xupp-xlow) / np;
      // Convolution integral of Landau and Gaussian by sum
      for(i=1.0; i<=np/2.; i++) {
         xx = xlow + (i-.5) * step;
         fland = TMath::Landau(xx, mpc, width) / width;
         sum += fland * TMath::Gaus(x, xx, sigma);
         xx = xupp - (i-.5) * step;
         fland = TMath::Landau(xx, mpc, width) / width;
         sum += fland * TMath::Gaus(x, xx, sigma);
      }
      return (area * step * sum * invsq2pi / sigma);
}

Double_t
langausfun(Double_t *x, Double_t *par) {
    return langaus( *x, par[0], par[1], par[2], par[3] );
}

std::pair<TF1 *, bool>
langausfit( TH1F * his
          , Double_t * fitrange
          , Double_t * startvalues
          , Double_t * parlimitslo
          , Double_t * parlimitshi
          , const char * fitOpts
          , Double_t * fitparams
          , Double_t * fiterrors
          , Double_t & chiSqr
          , Int_t & ndf ) {
   Char_t FunName[128];
   snprintf( FunName, sizeof(FunName), "Fitfcn_%s", his->GetName() );

   TF1 *ffitold = (TF1*) ROOT::GetROOT()
                        ->GetListOfFunctions()
                        ->FindObject(FunName);
   if( ffitold )
       delete ffitold;
   TF1 *ffit = new TF1(FunName, langausfun, fitrange[0], fitrange[1], 4);

   ffit->SetParameters(startvalues);
   ffit->SetParNames( "Width", "MP", "Area", "GSigma" );
   for(Int_t i=0; i<4; i++) {
      ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
   }

   // fit within specified range, use ParLimits, do not plot
   auto result = his->Fit(FunName, fitOpts ? fitOpts : "SLRQ0");

   // Obtain output
   ffit->GetParameters(fitparams);
   for(Int_t i=0; i<4; i++)
      fiterrors[i] = ffit->GetParError(i);
   chiSqr = ffit->GetChisquare();
   ndf = ffit->GetNDF();
   return {ffit, result->IsValid()};
}

}
}

#endif

