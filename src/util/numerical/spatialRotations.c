#include "na64util/numerical/spatialRotations.h"

#include <math.h>
#include <string.h>
#include <assert.h>

#ifdef STANDALONE_BUILD
#   include <stdio.h>
#endif

/* Reference: https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix */

static const struct {
    char ext[3], intr[3];
} _gStrOrder[] = {
    { "XZX", "xzx" }, { "XYX", "xyx" }, { "YXY", "yxy" },
    { "YZY", "yzy" }, { "ZYZ", "zyz" }, { "ZXZ", "zxz" },
    { "XZY", "yzx" }, { "XYZ", "zyx" }, { "YXZ", "zxy" },
    { "YZX", "xzy" }, { "ZYX", "xyz" }, { "ZXY", "yxz" },
};

int
na64sw_rotation_order_from_str( const char * str
                              , enum na64sw_RotationOrder * dest ) {
    for(int i = 0; i < sizeof(_gStrOrder)/sizeof(*_gStrOrder); ++i) {
        if( strncmp(str, _gStrOrder[i].ext,  3)
         && strncmp(str, _gStrOrder[i].intr, 3) ) continue;
        *dest = (enum na64sw_RotationOrder) i;
        return 0;
    }
    return 1;
}

/*
 * Euler rotation matrices
 */
#define _M_Eul_X1_Z2_X3                                                 \
    c2,                     -c3*s2,                 s2*s3,              \
    c1*s2,                  c1*c2*c3 - s1*s3,       -c3*s1 - c1*c2*s3,  \
    s1*s2,                  c1*s3 + c2*c3*s1,       c1*c3 - c2*s1*s3
#define _M_Rev_X1_Z2_X3                                                 \
    a[0] = atan2(R[2*3+0], R[1*3+0]);                                   \
    a[1] = acos(R[0*3+0]);                                              \
    a[2] = atan2(R[0*3+2], -R[0*3+1]);

#define _M_Eul_X1_Y2_X3                                                 \
    c2,                     s2*s3,                  c3*s2,              \
    s1*s2,                  c1*c3-c2*s1*s3,         -c1*s3-c2*c3*s1,    \
    -c1*s2,                 c3*s1+c1*c2*s3,         c1*c2*c3-s1*s3
#define _M_Rev_X1_Y2_X3                                                 \
    a[0] = atan2(R[1*3+0], -R[2*3+0]);                                  \
    a[1] = acos(R[0*3+0]);                                              \
    a[2] = atan2(R[0*3+1], R[0*3+2]);

#define _M_Eul_Y1_X2_Y3                                                 \
    c1*c3 - c2*s1*s3,       s1*s2,                  c1*s3 + c2*c3*s1,   \
    s2*s3,                  c2,                     -c3*s2,             \
    -c3*s1 - c1*c2*s3,      c1*s2,                  c1*c2*c3 - s1*s3
#define _M_Rev_Y1_X2_Y3                                                 \
    a[0] = atan2(R[0*3+1], R[2*3+1]);                                   \
    a[1] = acos(R[1*3+1]);                                              \
    a[2] = atan2(R[1*3+0], -R[1*3+2]);

#define _M_Eul_Y1_Z2_Y3                                                 \
    c1*c2*c3 - s1*s3,       -c1*s2,                 c3*s1 + c1*c2*s3,   \
    c3*s2,                  c2,                     s2*s3,              \
    -c1*s3 - c2*c3*s1,      s1*s2,                  c1*c3 - c2*s1*s3
#define _M_Rev_Y1_Z2_Y3                                                 \
    a[0] = atan2(R[2*3+1], -R[0*3+1]);                                  \
    a[1] = acos(R[1*3+1]);                                              \
    a[2] = atan2(R[1*3+2], R[1*3+0]);

#define _M_Eul_Z1_Y2_Z3                                                 \
    c1*c2*c3 - s1*s3,      -c3*s1 - c1*c2*s3,       c1*s2,              \
    c1*s3 + c2*c3*s1,       c1*c3 - c2*s1*s3,       s1*s2,              \
    -c3*s2,                 s2*s3,                  c2
#define _M_Rev_Z1_Y2_Z3                                                 \
    a[0] = atan2(R[1*3+2], R[0*3+2]);                                   \
    a[1] = atan2(sqrt(1 - R[2*3+2]*R[2*3+2]), R[2*3+2]);                \
    a[2] = atan2(R[2*3+1], -R[2*3+0]);

#define _M_Eul_Z1_X2_Z3                                                 \
    c1*c3 - c2*s1*s3,       -c1*s3 - c2*c3*s1,      s1*s2,              \
    c3*s1 + c1*c2*s3,       c1*c2*c3 - s1*s3,       -c1*s2,             \
    s2*s3,                  c3*s2,                  c2
#define _M_Rev_Z1_X2_Z3                                                 \
    a[0] = atan2(R[0*3+2], -R[1*3+2]);                                  \
    a[1] = acos(R[2*3+2]);                                              \
    a[2] = atan2(R[2*3+0], R[2*3+1]);

/*
 * Tait-Bryan rotation matrices
 */
#define _M_TBr_X1_Z2_Y3                                                 \
    c2*c3,                  -s2,                    c2*s3,              \
    s1*s3 + c1*c3*s2,       c1*c2,                  c1*s2*s3-c3*s1,     \
    c3*s1*s2 - c1*s3,       c2*s1,                  c1*c3+s1*s2*s3
#define _M_Rev_X1_Z2_Y3                                                 \
    a[0] = atan2(R[2*3+1], R[1*3+1]);                                   \
    a[1] = asin(-R[0*3+1]);                                             \
    a[2] = atan2(R[0*3+2], R[0*3+0]);

#define _M_TBr_X1_Y2_Z3                                                 \
    c2*c3,                  -c2*s3,                 s2,                 \
    c1*s3 + c3*s1*s2,       c1*c3 - s1*s2*s3,       -c2*s1,             \
    s1*s3 - c1*c3*s2,       c3*s1 + c1*s2*s3,       c1*c2
#define _M_Rev_X1_Y2_Z3                                                 \
    a[0] = atan2(-R[1*3+2], R[2*3+2]);                                  \
    a[1] = asin(R[0*3+2]);                                              \
    a[2] = atan2(-R[0*3+1], R[0*3+0]);

#define _M_TBr_Y1_X2_Z3                                                 \
    c1*c3 + s1*s2*s3,       c3*s1*s2 - c1*s3,       c2*s1,              \
    c2*s3,                  c2*c3,                  -s2,                \
    c1*s2*s3 - c3*s1,       c1*c3*s2 + s1*s3,       c1*c2
#define _M_Rev_Y1_X2_Z3                                                 \
    a[0] = atan2(R[0*3+2], R[2*3+2]);                                   \
    a[1] = asin(-R[1*3+2]);                                             \
    a[2] = atan2(R[1*3+0], R[1*3+1]);

#define _M_TBr_Y1_Z2_X3                                                 \
    c1*c2,                  s1*s3 - c1*c3*s2,       c3*s1 + c1*s2*s3,   \
    s2,                     c2*c3,                  -c2*s3,             \
    -c2*s1,                 c1*s3 + c3*s1*s2,       c1*c3 - s1*s2*s3
#define _M_Rev_Y1_Z2_X3                                                 \
    a[0] = atan2(-R[2*3+0], R[0*3+0]);                                  \
    a[1] = asin(R[1*3+0]);                                              \
    a[2] = atan2(-R[1*3+2], R[1*3+1]);

#define _M_TBr_Z1_Y2_X3                                                 \
    c1*c2,                  c1*s2*s3 - c3*s1,       s1*s3 + c1*c3*s2,   \
    c2*s1,                  c1*c3 + s1*s2*s3,       c3*s1*s2 - c1*s3,   \
    -s2,                    c2*s3,                  c2*c3
#define _M_Rev_Z1_Y2_X3                                                 \
    a[0] = atan2(R[1*3+0], R[0*3+0]);                                   \
    a[1] = asin(-R[2*3+0]);                                             \
    a[2] = atan2(R[2*3+1], R[2*3+2]);

#define _M_TBr_Z1_X2_Y3                                                 \
    c1*c3 - s1*s2*s3,       -c2*s1,                 c1*s3 + c3*s1*s2,   \
    c3*s1 + c1*s2*s3,       c1*c2,                  s1*s3 - c1*c3*s2,   \
    -c2*s3,                 s2,                     c2*c3
#define _M_Rev_Z1_X2_Y3                                                 \
    a[0] = atan2(-R[0*3+1], R[1*3+1]);                                  \
    a[1] = asin(R[2*3+1]);                                              \
    a[2] = atan2(-R[2*3+0], R[2*3+2]);

/*
 * Implementation
 */

#define _M_implem( NAME_SUFFIX, FLOAT, BODY1, BODY2 )                         \
static void _mx_ ## NAME_SUFFIX (FLOAT * dest, const FLOAT * a) {             \
    const FLOAT s1 = sin(a[0]),     c1 = cos(a[0])                            \
              , s2 = sin(a[1]),     c2 = cos(a[1])                            \
              , s3 = sin(a[2]),     c3 = cos(a[2])                            \
              ;                                                               \
    const FLOAT m[] = { BODY1 };                                              \
    memcpy(dest, m, sizeof(m));                                               \
}                                                                             \
static void _angles_ ## NAME_SUFFIX (FLOAT * a, const FLOAT * R) {            \
    BODY2 ; \
}

/* Euler */
_M_implem(xzx_f, float,     _M_Eul_X1_Z2_X3, _M_Rev_X1_Z2_X3);
_M_implem(xyx_f, float,     _M_Eul_X1_Y2_X3, _M_Rev_X1_Y2_X3);
_M_implem(yxy_f, float,     _M_Eul_Y1_X2_Y3, _M_Rev_Y1_X2_Y3);
_M_implem(yzy_f, float,     _M_Eul_Y1_Z2_Y3, _M_Rev_Y1_Z2_Y3);
_M_implem(zyz_f, float,     _M_Eul_Z1_Y2_Z3, _M_Rev_Z1_Y2_Z3);
_M_implem(zxz_f, float,     _M_Eul_Z1_X2_Z3, _M_Rev_Z1_X2_Z3);
/* Tait-Bryan */
_M_implem(xzy_f, float,     _M_TBr_X1_Z2_Y3, _M_Rev_X1_Z2_Y3);
_M_implem(xyz_f, float,     _M_TBr_X1_Y2_Z3, _M_Rev_X1_Y2_Z3);
_M_implem(yxz_f, float,     _M_TBr_Y1_X2_Z3, _M_Rev_Y1_X2_Z3);
_M_implem(yzx_f, float,     _M_TBr_Y1_Z2_X3, _M_Rev_Y1_Z2_X3);
_M_implem(zyx_f, float,     _M_TBr_Z1_Y2_X3, _M_Rev_Z1_Y2_X3);
_M_implem(zxy_f, float,     _M_TBr_Z1_X2_Y3, _M_Rev_Z1_X2_Y3);

#define x (0x1)
#define y (0x2)
#define z (0x3)
#define _M_encode_seq( first, second, third ) \
    (first) | (second << 2) | (third << 4)

static struct {
    void (*implem)(float *, const float *);
    void (*rev)(float *, const float *);
} _float_matrices[] = {
#define _M_entry(first, second, third) \
    { _mx_ ## first ## second ## third ## _f \
    , _angles_ ## first ## second ## third ## _f },
    _M_entry( x, z, x )         _M_entry( x, y, x )
    _M_entry( y, x, y )         _M_entry( y, z, y )
    _M_entry( z, y, z )         _M_entry( z, x, z )

    _M_entry( x, z, y )         _M_entry( x, y, z )
    _M_entry( y, x, z )         _M_entry( y, z, x )
    _M_entry( z, y, x )         _M_entry( z, x, y )
#undef _M_entry
};

/* Euler */
_M_implem(xzx_d, double,    _M_Eul_X1_Z2_X3, _M_Rev_X1_Z2_X3);
_M_implem(xyx_d, double,    _M_Eul_X1_Y2_X3, _M_Rev_X1_Y2_X3);
_M_implem(yxy_d, double,    _M_Eul_Y1_X2_Y3, _M_Rev_Y1_X2_Y3);
_M_implem(yzy_d, double,    _M_Eul_Y1_Z2_Y3, _M_Rev_Y1_Z2_Y3);
_M_implem(zyz_d, double,    _M_Eul_Z1_Y2_Z3, _M_Rev_Z1_Y2_Z3);
_M_implem(zxz_d, double,    _M_Eul_Z1_X2_Z3, _M_Rev_Z1_X2_Z3);
/* Tait-Bryan */
_M_implem(xzy_d, double,    _M_TBr_X1_Z2_Y3, _M_Rev_X1_Z2_Y3);
_M_implem(xyz_d, double,    _M_TBr_X1_Y2_Z3, _M_Rev_X1_Y2_Z3);
_M_implem(yxz_d, double,    _M_TBr_Y1_X2_Z3, _M_Rev_Y1_X2_Z3);
_M_implem(yzx_d, double,    _M_TBr_Y1_Z2_X3, _M_Rev_Y1_Z2_X3);
_M_implem(zyx_d, double,    _M_TBr_Z1_Y2_X3, _M_Rev_Z1_Y2_X3);
_M_implem(zxy_d, double,    _M_TBr_Z1_X2_Y3, _M_Rev_Z1_X2_Y3);

static struct {
    void (*implem)(double *, const double *);
    void (*rev)(double *, const double *);
} _double_matrices[] = {
#define _M_entry(first, second, third) \
    { _mx_ ## first ## second ## third ## _d \
    , _angles_ ## first ## second ## third ## _d },
    _M_entry( x, z, x )         _M_entry( x, y, x )
    _M_entry( y, x, y )         _M_entry( y, z, y )
    _M_entry( z, y, z )         _M_entry( z, x, z )

    _M_entry( x, z, y )         _M_entry( x, y, z )
    _M_entry( y, x, z )         _M_entry( y, z, x )
    _M_entry( z, y, x )         _M_entry( z, x, y )
#undef _M_entry
};

#define _M_implement_unifunc(FLOAT, SUFFIX)                                   \
int                                                                           \
na64sw_rotation_matrix_ ## SUFFIX ( enum na64sw_RotationOrder n               \
                                  , const FLOAT * angles                      \
                                  , FLOAT * dest                              \
                                  ) {                                         \
    assert(n < sizeof(_ ## FLOAT ## _matrices) / sizeof(* _ ## FLOAT ## _matrices)); \
    _ ## FLOAT ## _matrices[n].implem(dest, angles);                          \
    return 0;                                                                 \
}                                                                             \
int                                                                           \
na64sw_find_rotation_for_ ## SUFFIX ( enum na64sw_RotationOrder n             \
                                    , const FLOAT * mx                        \
                                    , FLOAT * dest                            \
                                  ) {                                         \
    assert(n < sizeof(_ ## FLOAT ## _matrices) / sizeof(* _ ## FLOAT ## _matrices)); \
    _ ## FLOAT ## _matrices[n].rev(dest, mx);                                 \
    return 0;                                                                 \
}                                                                             \

_M_implement_unifunc( float,  f );
_M_implement_unifunc( double, d );

#ifdef STANDALONE_BUILD
int
main(int argc, char * argv[]) {
    float m[9]
        , angles[3] = {2*M_PI/6, 3*M_PI/7, 4*M_PI/9}
        , revAngles[3];

    for(int i = 0; i <= (int) na64sw_Rotation_zxy; ++i ) {
        const enum na64sw_RotationOrder order
            = (enum na64sw_RotationOrder) i;
        na64sw_rotation_matrix_f( order, angles, m );
        /*for(int p = 0; p < 3; ++p) {
            for(int k = 0; k < 3; ++k) {
                printf( "\t%e", m[p*3 + k] );
            }
            printf("\n");
        }*/
        na64sw_find_rotation_for_f( order, m, revAngles );
        printf( " #%i: {%e, %e, %e} - {%e %e %e}\n"
              , i
              , angles[0],      angles[1],      angles[2]
              , revAngles[0],   revAngles[1],   revAngles[2]
              );
    }

    return 0;
}
#endif  /*STANDALONE_BUILD*/

