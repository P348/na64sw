#include "na64util/numerical/linalg.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace test {

using namespace util;

TEST( ArtTrack, unitMatrixMult ) {
    // simple unit matrices
    Matrix3 unitMx = {{ {1, 0, 0}
                      , {0, 1, 0}
                      , {0, 0, 1}
                      }};
    Vec3 v = {{1, 2, 3}};
    //
    Vec3 vv = unitMx*v;
    EXPECT_EQ(vv.r[0], 1);
    EXPECT_EQ(vv.r[1], 2);
    EXPECT_EQ(vv.r[2], 3);
    //
    Matrix3 m = {{ {1, 2, 3}
                 , {4, 5, 6}
                 , {7, 8, 9}
                 }};
    Matrix3 rm = unitMx*m;
    EXPECT_EQ( rm.cm[0][0], 1 );
    EXPECT_EQ( rm.cm[1][1], 5 );
    EXPECT_EQ( rm.cm[2][2], 9 );
    EXPECT_EQ( rm.cm[1][2], 6 );
}

TEST( ArtTrack, rotationMatrix ) {
    // simple rotation matrices
    Matrix3 mOx = arbitrary_rotation_matrix<Float_t>({{1, 0, 0}}, M_PI/2)
          , mOy = arbitrary_rotation_matrix<Float_t>({{0, 1, 0}}, M_PI/2)
          , mOz = arbitrary_rotation_matrix<Float_t>({{0, 0, 1}}, M_PI/2)
          ;
    Vec3 v = {{1, 0, 0}};
    // We're testing very simple rotation here
    {
        Vec3 vv = mOx*v;
        EXPECT_NEAR( vv.r[0], 1, 1e-6 );
        EXPECT_NEAR( vv.r[1], 0, 1e-6 );
        EXPECT_NEAR( vv.r[2], 0, 1e-6 );
    }
    {
        Vec3 vv = mOz*v;
        EXPECT_NEAR( vv.r[0],  0, 1e-6 );
        EXPECT_NEAR( vv.r[1],  1, 1e-6 );
        EXPECT_NEAR( vv.r[2],  0, 1e-6 );
        vv = mOy*vv;
        EXPECT_NEAR( vv.r[0],  0, 1e-6 );
        EXPECT_NEAR( vv.r[1],  1, 1e-6 );
        EXPECT_NEAR( vv.r[2],  0, 1e-6 );
    }
}

TEST( ArtTrack, unitInvertedIsUnit ) {
    Matrix3 unit{{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}};
    Matrix3 unitInv = unit.inv();
    EXPECT_NEAR( unitInv.cm[0][0], 1, 1e-6 );
    EXPECT_NEAR( unitInv.cm[1][1], 1, 1e-6 );
    EXPECT_NEAR( unitInv.cm[2][2], 1, 1e-6 );
}

TEST( ArtTrack, testInversion ) {
    Matrix3 m{{ {1, 2, 3}
              , {0, 1, 4}
              , {5, 6, 0}
              }};

    Matrix3 mi = m.inv();
    Matrix3 unit = m*mi;

    EXPECT_NEAR( unit.cm[0][0], 1, 1e-6 );
    EXPECT_NEAR( unit.cm[1][1], 1, 1e-6 );
    EXPECT_NEAR( unit.cm[2][2], 1, 1e-6 );
}

}  // namespace ::arttrack::test
}  // namespace arttrack
