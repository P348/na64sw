#include "na64util/numerical/biplaneIntersection.h"
#include "na64util/numerical/linalg.hh"

#include <gtest/gtest.h>

TEST(biplaneIntersection, findsEquidistantPointOnSkewAxesCaseZero) {
    na64dp::util::Vec3 k1{{2, 0, 0}}, r01{{-1,  0, -1}}
                     , k2{{0, 2, 0}}, r02{{ 0, -1,  1}}
                     ;
    na64dp::util::Float_t eqp[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k2.r, NULL
                                                , r01.r, r02.r
                                                , eqp, NULL
                                                );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(eqp[0], 0, 1e-6);
    EXPECT_NEAR(eqp[1], 0, 1e-6);
    EXPECT_NEAR(eqp[2], 0, 1e-6);
}

TEST(biplaneIntersection, findsQuasiIntersectingPointsOnSkewAxesCaseZero) {
    na64dp::util::Vec3 k1{{1, 0, 0}}, r01{{-1,  0, -1}}
                     , k2{{0, 1, 0}}, r02{{ 0, -1,  1}}
                     , k3{{0, 0, 1}}
                     ;
    na64dp::util::Float_t x1[3], x2[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k2.r, k3.r
                                                , r01.r, r02.r
                                                , x1, x2
                                                );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(x1[0], 0, 1e-6);
    EXPECT_NEAR(x1[1], 0, 1e-6);
    EXPECT_NEAR(x1[2],-1, 1e-6);

    EXPECT_NEAR(x2[0], 0, 1e-6);
    EXPECT_NEAR(x2[1], 0, 1e-6);
    EXPECT_NEAR(x2[2], 1, 1e-6);
}

TEST(biplaneIntersection, findsRealIntersectionAtZero) {
    na64dp::util::Vec3 k1{{1, 0, 0}}, r01{{ 0,  0,  0}}
                     , k2{{0, 1, 0}}, r02{{ 0,  0,  0}}
                     , k3{{0, 0, 1}}
                     ;
    na64dp::util::Float_t x1[3], x2[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k2.r, k3.r
                                                , r01.r, r02.r
                                                , x1, x2
                                                );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(x1[0], 0, 1e-6);
    EXPECT_NEAR(x1[1], 0, 1e-6);
    EXPECT_NEAR(x1[2], 0, 1e-6);

    EXPECT_NEAR(x2[0], 0, 1e-6);
    EXPECT_NEAR(x2[1], 0, 1e-6);
    EXPECT_NEAR(x2[2], 0, 1e-6);
}

TEST(biplaneIntersection, findsRealIntersectionNonZeroAlmostParallelLines) {
    // this is really bad case as direction vectors differ slightly, so for
    // the control we increase tolerance up to 10um
    na64dp::util::Vec3 p0 = { 0.33,   -0.24, 17.23}
                     , k1 = { -0.01, 123.45, 10}
                     , k2 = { +0.01, 123.12, 9.87}
                     , r01 = p0 + k1*2
                     , r02 = p0 + k2*0.123
                     ;
    na64dp::util::Float_t eqp[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k2.r, NULL
                                                , r01.r, r02.r
                                                , eqp, NULL
                                                );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(eqp[0], p0.c.x, 1e-3);
    EXPECT_NEAR(eqp[1], p0.c.y, 1e-3);
    EXPECT_NEAR(eqp[2], p0.c.z, 1e-3);
}

TEST(biplaneIntersection, findsRealIntersectionNonZero) {
    na64dp::util::Vec3 p0 = { 0.33,   -0.24, 17.23}
                     , k1 = { -0.01, 123.45, 10}
                     , k2 = {  12, -2, 3.15}
                     , r01 = p0 + k1*2
                     , r02 = p0 + k2*0.123
                     ;
    na64dp::util::Float_t eqp[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k2.r, NULL
                                                , r01.r, r02.r
                                                , eqp, NULL
                                                );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(eqp[0], p0.c.x, 1e-3);
    EXPECT_NEAR(eqp[1], p0.c.y, 1e-3);
    EXPECT_NEAR(eqp[2], p0.c.z, 1e-3);
}

TEST(biplaneIntersection, detectsParallelLines) {
    na64dp::util::Vec3 p0 = { 0.33,   -0.24, 17.23}
                     , k1 = { -0.01, 123.45, 10}
                     , r01 = p0 + k1*2
                     , r02 = p0 + k1*0.123
                     ;
    na64dp::util::Float_t eqp[3];
    int rc = na64sw_projected_lines_intersection( k1.r, k1.r, NULL
                                                 , r01.r, r02.r
                                                 , eqp, NULL
                                                 );
    ASSERT_NE(rc, 0);
}

TEST(biplaneIntersection, genericCaseOne) {
    // ...
    na64dp::util::Vec3 r01{{-1.37063, -0.19879,  51.7479}}
                     , r02{{ 0.854195, 2.52535,  49.494 }}
                     ,  l1{{ 3.22572,  8.85461,   4.85289}}
                     ,  l2{{-10.5628, -0.699319, -0.54637}}
                     ;
    na64dp::util::Float_t eqp[3];
    int rc = na64sw_projected_lines_intersection(   l1.r,  l2.r, NULL
                                                 , r01.r, r02.r
                                                 , eqp, NULL
                                                 );
    ASSERT_EQ(rc, 0);
    EXPECT_NEAR(eqp[0], -0.973615, 1e-5);
    EXPECT_NEAR(eqp[1],  1.61684, 1e-5);
    EXPECT_NEAR(eqp[2], 50.8556, 1e-5);
}

