# include "na64util/na64/event-id.hh"
# include "na64util/str-fmt.hh"

namespace na64dp {

std::string
EventID::to_str() const {
    char bf[128];
    na64sw_eid2str( _eid, bf, sizeof(bf) );
    return bf;
}

std::ostream &
operator<<( std::ostream & stream, const EventID & eid ) {
    stream << eid.to_str();
    return stream;
}

EventID
EventID::from_str( const std::string & stlString ) {
    char * endPtr;
    na64sw_EventID_t eid = na64sw_str2eid( stlString.c_str(), &endPtr );
    if( na64sw_get_reservedBit(eid)
     && 0 == na64sw_get_runNo(eid)
     && 0 == na64sw_get_spillNo(eid)
     && 0 == na64sw_get_eventNo(eid)
     ) {
        std::string substring( stlString.begin()
                             , stlString.begin() + (endPtr - stlString.data()) );
        // TODO: dedicated exception class
        NA64DP_RUNTIME_ERROR( "Unable to interpret string \"%s\" as an event ID."
                            , substring.c_str() );
    }
    return EventID(eid);
}

namespace util {

void reset( EventID & eid ) {
    eid = EventID();
}

}

}  // namespace na64

