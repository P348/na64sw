# include "na64sw-config.h"
# include "na64util/na64/event-id.h"

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <printf.h>

#define define_event_field_limits( type, name, nBits, offset, bitmask, ... ) \
const na64sw_ ## name ## _t na64sw_g_ ## name ## _max = (1 << nBits) - 1;
NA64SW_FOR_ALL_EVENT_ID_FIELDS( define_event_field_limits )
#undef define_event_field_limits

#define impl_event_id_field_get_set( type, name, nBits, offset, bitmask, ... ) \
na64sw_ ## name ## _t na64sw_get_ ## name ( na64sw_EventID_t eid ) { \
    return (eid & bitmask) >> offset; \
} \
void na64sw_set_ ## name ( na64sw_EventID_t * eidPtr, na64sw_ ## name ## _t v ) { \
    *eidPtr &= ~bitmask; \
    v &= (bitmask >> offset); \
    *eidPtr |= (((na64sw_EventID_t) v) << offset); \
}
NA64SW_FOR_ALL_EVENT_ID_FIELDS( impl_event_id_field_get_set )
#undef impl_event_id_field_get_set

na64sw_EventID_t
na64sw_assemble_event_id( na64sw_runNo_t   runNo
                        , na64sw_spillNo_t spillNo
                        , na64sw_eventNo_t eventNo
                        ) {
    na64sw_EventID_t res = 0x0;
    na64sw_set_runNo( &res, runNo );
    na64sw_set_spillNo( &res, spillNo );
    na64sw_set_eventNo( &res, eventNo );
    return res;
}

size_t
na64sw_eid2str( na64sw_EventID_t eid
              , char * buf, size_t buflen) {
    return snprintf( buf, buflen
                   , "%hu/%hu.%u"
                   , na64sw_get_runNo( eid )
                   , na64sw_get_spillNo( eid )
                   , na64sw_get_eventNo( eid )
                   );
}

na64sw_EventID_t
na64sw_str2eid( const char * bf, char ** eventEnd ) {
    na64sw_EventID_t r = 0x0;
    char * runEnd
       , * spillEnd
       ;
    /* parse run number */
    long int parsed = strtol( bf, &runEnd, 10 );
    if( *runEnd != '/'
     || parsed > na64sw_g_runNo_max ) {
        goto parsingFailed;
    }
    na64sw_set_runNo( &r, parsed );

    /* parse spill number */
    parsed = strtol( runEnd + 1, &spillEnd, 10 );
    if( *spillEnd != '.'
     || parsed > na64sw_g_spillNo_max ) {
        goto parsingFailed;
    }
    na64sw_set_spillNo( &r, parsed );

    /* parse event number */
    parsed = strtol( spillEnd + 1, eventEnd, 10 );
    if( parsed > na64sw_g_eventNo_max ) {
        goto parsingFailed;
    }
    na64sw_set_eventNo( &r, parsed );

    return r;

parsingFailed:
    r = 0x0;
    na64sw_set_reservedBit( &r, 1 );
    return r;
}

