#include "na64util/mem/pools.hh"

namespace na64dp {
namespace mem {

// ...

}  // namespace ::na64dp::mem
}  // namespace na64dp

#if 0
#include <log4cpp/Category.hh>

namespace na64dp {

#if 1

namespace pools {
void
AbstractPool::_incref(size_t n) {
    assert( n < size() );
}

void
AbstractPool::_decref(size_t n) {
    assert( n < size() );
}

size_t
AbstractPool::_add_counter() {
    push_back(0);
    return size();
}
}

#else
HitBanks::HitBanks( size_t nSADCHits
                  , size_t nAPVHits
                  , size_t nStwTDCHits
                  , size_t nSADCFittingEntries
                  , size_t nAPVClusters
                  , size_t nCaloEDeps
                  , size_t nTrackPoints
                  , size_t nTracks
                  , size_t nMCTruth
                  , size_t nMCTrueTrackPoints
                  ) : bankSADC(nSADCHits)
                    , bankAPV(nAPVHits)
                    , bankStwTDC(nStwTDCHits)
                    , bankSADCFitting(nSADCFittingEntries)
                    , bankAPVClusters(nAPVClusters)
                    , bankCaloEDeps(nCaloEDeps)
                    , bankTrackPoints(nTrackPoints)
                    , bankTracks(nTracks)
                    , bankEventMCTrue(nMCTruth)
                    , bankMCTrueTrackPoint(nMCTrueTrackPoints)
                    {
    #ifndef NDEBUG
    auto & L = log4cpp::Category::getInstance("banks");
    L.info( "%zu SADC hits pre-allocated", bankSADC.capacity() );
    L.info( "%zu APV hits pre-allocated", bankAPV.capacity() );
    L.info( "%zu StwTDC hits pre-allocated", bankStwTDC.capacity() );
    L.info( "%zu SADC fitting entries pre-allocated", bankSADCFitting.capacity() );
    L.info( "%zu APV clusters pre-allocated", bankAPVClusters.capacity() );
    L.info( "%zu Calo energy depositions pre-allocated", bankCaloEDeps.capacity() );
    L.info( "%zu track points pre-allocated", bankTrackPoints.capacity() );
    L.info( "%zu tracks pre-allocated", bankTracks.capacity() );
    L.info( "%zu entries for per-event MC truth pre-allocated", bankEventMCTrue.capacity() );
    L.info( "%zu entries for MC-true track points pre-allocated", bankMCTrueTrackPoint.capacity() );
    // ...
    #endif
}

HitBanks::HitBanks() : HitBanks(10, 10, 10, 10, 10, 10, 10, 10, 1, 10) {}

void
HitBanks::clear() {
    bankSADC.clear();
    bankAPV.clear();
    bankStwTDC.clear();
    bankSADCFitting.clear();
    bankAPVClusters.clear();
    bankCaloEDeps.clear();
    bankTrackPoints.clear();
    bankTracks.clear();
    // ...
    #ifndef NDEBUG
    auto & L = log4cpp::Category::getInstance("banks");
    // XXX, to frequent, creates flood
    #if 1
    msg_debug(L, "Memory banks cleared.");
    msg_debug(L, "SADC bank capacity after clearing: %zu", bankSADC.capacity());
    msg_debug(L, "APV bank capacity after clearing: %zu", bankAPV.capacity());
    msg_debug(L, "StwTDC bank capacity after clearing: %zu", bankStwTDC.capacity());
    msg_debug(L, "SADC fitting bank capacity after clearing: %zu", bankSADCFitting.capacity());
    msg_debug(L, "APV clusters bank capacity after clearing: %zu", bankAPVClusters.capacity());
    msg_debug(L, "CaloEDep bank capacity after cleaning: %zu", bankCaloEDeps.capacity() );
    msg_debug(L, "track points bank capacity after clearing: %zu", bankTrackPoints.capacity());
    // ...
    #endif
    #endif
}
#endif
}
#endif
