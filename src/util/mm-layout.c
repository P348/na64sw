#include "na64util/mm-layout.h"

#include <stdlib.h>

/**\file
 * \brief File containing static data related to APV-managed detectors.
 *
 * Strip mapping for GEMs and Micromegas is described here.
 * */

// In DDD remapping is already done, so we should return identity.
// However, it may be useful to store remapping formula for
// future usage and for possible reference.
# define GEM_APV_IDENTITY

const NA64DP_APVStripNo_t na64dp_MM_JointWires[64][5] = {
    {  0,  64, 128, 192, 256 },
    {  1, 119, 133, 219, 297 },
    {  2, 110, 138, 246, 274 },
    {  3, 101, 143, 209, 315 },
    {  4,  92, 148, 236, 292 },
    {  5,  83, 153, 199, 269 },
    {  6,  74, 158, 226, 310 },
    {  7,  65, 163, 253, 287 },
    {  8, 120, 168, 216, 264 },
    {  9, 111, 173, 243, 305 },
    { 10, 102, 178, 206, 282 },
    { 11,  93, 183, 233, 259 },
    { 12,  84, 188, 196, 300 },
    { 13,  75, 129, 223, 277 },
    { 14,  66, 134, 250, 318 },
    { 15, 121, 139, 213, 295 },
    { 16, 112, 144, 240, 272 },
    { 17, 103, 149, 203, 313 },
    { 18,  94, 154, 230, 290 },
    { 19,  85, 159, 193, 267 },
    { 20,  76, 164, 220, 308 },
    { 21,  67, 169, 247, 285 },
    { 22, 122, 174, 210, 262 },
    { 23, 113, 179, 237, 303 },
    { 24, 104, 184, 200, 280 },
    { 25,  95, 189, 227, 257 },
    { 26,  86, 130, 254, 298 },
    { 27,  77, 135, 217, 275 },
    { 28,  68, 140, 244, 316 },
    { 29, 123, 145, 207, 293 },
    { 30, 114, 150, 234, 270 },
    { 31, 105, 155, 197, 311 },
    { 32,  96, 160, 224, 288 },
    { 33,  87, 165, 251, 265 },
    { 34,  78, 170, 214, 306 },
    { 35,  69, 175, 241, 283 },
    { 36, 124, 180, 204, 260 },
    { 37, 115, 185, 231, 301 },
    { 38, 106, 190, 194, 278 },
    { 39,  97, 131, 221, 319 },
    { 40,  88, 136, 248, 296 },
    { 41,  79, 141, 211, 273 },
    { 42,  70, 146, 238, 314 },
    { 43, 125, 151, 201, 291 },
    { 44, 116, 156, 228, 268 },
    { 45, 107, 161, 255, 309 },
    { 46,  98, 166, 218, 286 },
    { 47,  89, 171, 245, 263 },
    { 48,  80, 176, 208, 304 },
    { 49,  71, 181, 235, 281 },
    { 50, 126, 186, 198, 258 },
    { 51, 117, 191, 225, 299 },
    { 52, 108, 132, 252, 276 },
    { 53,  99, 137, 215, 317 },
    { 54,  90, 142, 242, 294 },
    { 55,  81, 147, 205, 271 },
    { 56,  72, 152, 232, 312 },
    { 57, 127, 157, 195, 289 },
    { 58, 118, 162, 222, 266 },
    { 59, 109, 167, 249, 307 },
    { 60, 100, 172, 212, 284 },
    { 61,  91, 177, 239, 261 },
    { 62,  82, 182, 202, 302 },
    { 63,  73, 187, 229, 279 },
};


const NA64DP_APVStripNo_t *
na64_APV_strip_mapper__micromegas_joints( NA64DP_APVWireNo_t wN, NA64DP_APVNWires_t * lPtr ) {
    if( wN >= sizeof(na64dp_MM_JointWires) / (sizeof(*na64dp_MM_JointWires)) ) {
        return NULL;
    }
    if( lPtr ) {
        *lPtr = 5;
    }
    return na64dp_MM_JointWires[wN];
}

# ifndef GEM_APV_IDENTITY
static NA64DP_APVStripNo_t _static_na64_tripleGEMMappingDummy; 
# endif

/**The particular relation may be seen in Sebastian Uhl diploma thesis, Nov. 2008,
 * (4.7), p.30, or in gemMonitor/src/gemDecode.cpp:556 :
 *      strip nr. = 32*(n mod 4) + 8*int(n/4) - 31*int(n/16)
 */
const NA64DP_APVStripNo_t *
na64_APV_strip_mapper__GEM_demultiplexing_scheme( NA64DP_APVWireNo_t n, NA64DP_APVNWires_t * lPtr ) {
    if( lPtr ) {
        *lPtr = 1;
    }
    if( n > 256 ) {  // TODO: check this value!
        return NULL;
    }
    # ifdef GEM_APV_IDENTITY
    return na64_APV_strip_mapper__identity( n, lPtr );
    # else
    _static_na64_tripleGEMMappingDummy
        = 32*(n%4) + 8*(n/4) - 31*(n/16);
    return &_static_na64_tripleGEMMappingDummy;
    # endif
}

static NA64DP_APVStripNo_t _static_na64_identityStripMappingDummy; 
const NA64DP_APVStripNo_t *
na64_APV_strip_mapper__identity( NA64DP_APVWireNo_t wn, NA64DP_APVNWires_t * lPtr ) {
    if( lPtr ) {
        *lPtr = 1;
    }
    _static_na64_identityStripMappingDummy = wn;
    return &_static_na64_identityStripMappingDummy;
}



