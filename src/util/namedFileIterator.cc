#include "na64util/namedFileIterator.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include <TROOT.h>
#include <TKey.h>

#include <iostream>

namespace na64dp {
namespace util {

NamedFileIterator::NamedFileIterator( TFile * file
                 , const char * clName
                 , const char * regex
                 , std::ostream * debugLogPtr
                 )
        : _file(file), _className(clName), _rx(regex), _debugLogPtr(debugLogPtr) {
    //assert( _rx.IsValid() ); // always false; what this method is intended for?!
    _dirStack.push( StackEntry(TIter(file->GetListOfKeys()), _file) );
}

TObject *
NamedFileIterator::get_match(TObjArray ** subs) {
    if( _dirStack.empty() ) return nullptr;
    TKey * key;
    while((key = (TKey*) _dirStack.top().first.Next())) {
        TClass * cl = ROOT::GetROOT()->GetClass(key->GetClassName());
        // if current is dir
        if( cl->InheritsFrom("TDirectory") ) {
            TDirectory * dir = (TDirectory*) key->ReadObj();
            assert(dir);
            _dirStack.push(StackEntry(TIter(dir->GetListOfKeys()), dir));
            return get_match(subs);  // recursive forwarding here
        }
        // if current is look-up target
        bool hasMatch;
        if(subs) {
            *subs = _rx.MatchS(key->GetName());
            hasMatch = (*subs)->At(0);
        } else {
            hasMatch = _rx.Match(key->GetName());
        }
        if(_debugLogPtr) {
            *_debugLogPtr << "Name \"" << key->GetName() <<
                (hasMatch ? "\"" : "\" does not")
                << " match name-lookup regexp \"" << _rx.GetPattern() << "\"" << std::endl;
        }
        if( cl->InheritsFrom(_className) && hasMatch ) {
            // TODO: check here name vs regex match
            TObject * res = key->ReadObj();
            if(_debugLogPtr) *_debugLogPtr << "...class matches." << std::endl;
            return res;
        } else if(subs) {
            if(_debugLogPtr) *_debugLogPtr << "...class does not match." << std::endl;
            delete *subs;
            *subs = nullptr;
        }
    }
    _dirStack.pop();
    return get_match(subs);  // recursive forwarding here
}

}
}

#endif  // defined(ROOT_FOUND) && ROOT_FOUND
