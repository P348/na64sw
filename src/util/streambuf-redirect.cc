#include "na64util/streambuf-redirect.hh"
#include <sstream>

namespace na64dp {
namespace util {

Log4Cpp_StreambufRedirect::Log4Cpp_StreambufRedirect( log4cpp::Category & c
                             , log4cpp::Priority::PriorityLevel p
                             ) : _category(c)
                               , _priority(p) {
    std::ostringstream oss;
    oss << std::endl;
    _pdNl = oss.str();
}

void
Log4Cpp_StreambufRedirect::_print_message_buffer() {
    for( size_t n = _buf.find(_pdNl)
       ; n != std::string::npos && !_buf.empty()
       ; n = _buf.find(_pdNl)
       ) {
        if(_buf == _pdNl) {
            _buf.clear();
            break;
        }
        _category << _priority << _buf.substr(0, n);
        _buf = _buf.substr(n+_pdNl.size());
    }
}

std::streamsize
Log4Cpp_StreambufRedirect::xsputn(const char_type * c, std::streamsize n) {
    if(0 == n) return 0;
    std::string part(c, c+n);
    _buf += part;
    _print_message_buffer();
    return n;
}

std::streambuf::int_type
Log4Cpp_StreambufRedirect::overflow(int_type c) {
    if(!traits_type::eq_int_type(c, traits_type::eof())){
        char_type const t = traits_type::to_char_type(c);
        this->xsputn(&t, 1);
    }
    return !traits_type::eof();
}

int
Log4Cpp_StreambufRedirect::sync() {
    if(_buf.empty()) return 0;
    _print_message_buffer();
    return 0;
}

}  // namespace ::na64dp::util
}  // namespace na64dp
