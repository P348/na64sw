#include "na64util/ROOT-sighandlers.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND
#include <TSystem.h>

void
disable_ROOT_sighandlers() {
    gSystem->ResetSignals();
}
#endif
