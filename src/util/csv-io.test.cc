#if 0
#include "na64util/csv-io.hh"

#include <gtest/gtest.h>

/**\file csv.cc
 *
 * Testing unit for the templated CSV reader.
 * */

namespace na64dp {
namespace test {

// Simple test, checking very basic features of CSV reader with static
// typization
TEST(CSVRead, simple) {
    using namespace std;
    // instantiate mock input stream from 4 lines, mixed with "comments"
    istringstream iss("# Look at my horse\n"
                      "one 1.23 11 \n"
                      "  # my horse is amazing\n"
                      "5.46 something\n\n" );
    // construct a reader over the stream
    na64dp::csv::Reader vr(iss);

    // declare reading destination tuples
    tuple<string, double, int> t1;
    tuple<double, string> t2;
    
    // read the data
    vr >> t1 >> t2;

    // check we've read what is expected
    EXPECT_STREQ( get<0>(t1).c_str(), "one" );
    EXPECT_NEAR( get<1>(t1), 1.23, 1e-3 );
    EXPECT_EQ( get<2>(t1), 11 );

    EXPECT_NEAR( get<0>(t2), 5.46, 1e-3 );
    EXPECT_STREQ( get<1>(t2).c_str(), "something" );
}

// Advanced test, checks iteration over stream and tail detaching functions
TEST(CSVRead, iteration) {
    using namespace std;
    // instantiate mock input stream to test iteration capabilities
    istringstream iss(
            /* 0 */ "Look at my horse\n"
            /* 1 */ "\tmy  horse is amazing  \n"  // spaces on the tail shall be ignored
            /* 2 */ "  give it  a lick\t\n"
            /* - */ "#Hmm, It tastes just? like raisins\n\n"
            /* 3 */ "I have a stroke of it's mane\n"  // "of it's mane" shall be tail here
            /* 4 */ " It turnes  into a plane\n"  // "plane" shall be the tail here
            /* 5 */ "And then it turns"  // no tail, for sure
            );
    // construct a reader over the stream
    na64dp::csv::Reader vr(iss);

    // declare reading destination tuples
    tuple<string, string, string, string> t;
    
    int lineCount = 0;
    while( vr.read(t) ) {
        if( !vr.tail().empty() ) {
            EXPECT_TRUE( lineCount == 3 || lineCount == 4 );
            if( 3 == lineCount ) {
                EXPECT_STREQ( "of it's mane", vr.tail().c_str() );
            } else if( 4 == lineCount ) {
                EXPECT_STREQ( "plane", vr.tail().c_str() );
            }
        } else {
            EXPECT_FALSE(lineCount == 3 || lineCount == 4);
        }
        ++lineCount;
    }
}

}
}
#endif
