#include "na64util/exception.hh"

#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
#   include <cxxabi.h>
#   include <execinfo.h>
#   include <unistd.h>
#endif

#include <iostream>
#include <iomanip>

namespace na64dp {
namespace errors {

#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE

// shall be a platform-dependant value? anyway, stack trace is usually
// pointless at the level of recursion...
#define MAX_STACK_DEPTH_N 255

#if defined(BFD_FOUND) && BFD_FOUND
// RD: Binutils version 2.33 -> 2.34 made `*_get_*()` functions deprecated.
// However I failed to find out proper version macro to make a conditional
// block, so if your glibc is of version <2.34, you have to uncomment this line
// manually :(.
//#define OLD_BFD_API
static void
_seek_entry_in_section( bfd * abfd
                      , asection * section
                      , void * entry_) {
    auto & entry = *reinterpret_cast<StackRecord*>(entry_);
    if( entry.lineno ) { /* line found */ return; }

    if(!(
        #ifdef OLD_BFD_API
                bfd_get_section_flags(abfd, section) & SEC_ALLOC
        #else
                bfd_section_flags(section) & SEC_ALLOC
        #endif
      )) {
                         /*no debug info here*/ return; }
    bfd_vma section_vma =
        #ifdef OLD_BFD_API
        bfd_get_section_vma(abfd, section)
        #else
        bfd_section_vma(section)
        #endif
        ;
    if(entry.soLibAddr < section_vma) {
                        /* the addr lies above the section */ return;}
    bfd_size_type section_size =
        #ifdef OLD_BFD_API
        bfd_section_size(abfd, section)
        #else
        bfd_section_size(section)
        #endif
        ;
    if(entry.soLibAddr >= section_vma + section_size) {
                        /* the addr lies below the section */ return; }
    // Calculate the correct offset of our line in the section
    bfd_vma offset = entry.soLibAddr - section_vma - 1;
    // Locate the line by offset
    const char * filename=NULL,
               * functionName=NULL;
    entry.lFound = bfd_find_nearest_line(
                    abfd,   section,    entry.sTable,
                    offset, &filename,  &functionName,
                    &entry.lineno );

    if( !entry.lFound ) {
        entry.failure = "Source lookup failed.";
    }

    entry.srcFilename = filename        ? filename : "";
    entry.function    = functionName    ? functionName : "";

}

static int
_sup_positional_info( const SafeString & path
                    , bfd_vma addr
                    , StackRecord & entry ) {
    // see: std::string addr2str(std::string file_name, bfd_vma addr)
    #if defined(BFD_FOUND) && BFD_FOUND
    bfd * abfd;
    abfd = bfd_openr(path.c_str(), NULL); {
        char ** matching; // TODO: clear?
        if( !abfd ) {
            entry.failure = "Couln't open " + path + "."; return -1;
        } if( bfd_check_format(abfd, bfd_archive) ) {
            entry.failure = "File " + path + " is not a BFD-compliant archive."; return -1;
        } if(!bfd_check_format_matches(abfd, bfd_object, &matching)) {
            entry.failure = "File " + path + " has not a BFD-compliant archive format."; return -1;
        } if((bfd_get_file_flags(abfd) & HAS_SYMS) != 0) {
            unsigned int symbolSize;
            void ** sTablePtr = (void**) &(entry.sTable);
            long nSymbols = bfd_read_minisymbols( abfd,         false,
                                                  sTablePtr,    &symbolSize );

            if( !nSymbols ) {
                // If the bfd_read_minisymbols() already allocated the table, we need
                // to free it first:
                if( entry.sTable != NULL )
                    free( entry.sTable );
                // dynamic
                nSymbols = bfd_read_minisymbols( abfd,          true,
                                                 sTablePtr,     &symbolSize );
            } else if( nSymbols < 0 ) {
                entry.failure = "bfd_read_minisymbols() failed.";
                return -1;
            }
        } else {
            entry.failure = "BFD archive " + path + " has no symbols.";
        }
        bfd_map_over_sections(abfd, _seek_entry_in_section, &entry);
    } bfd_close(abfd);
    if( entry.sTable ) { free(entry.sTable); }
    # endif

    return 0;
}
#endif  // defined(BFD_FOUND) && BFD_FOUND

static int
_so_lib_callback( struct dl_phdr_info *info
                , size_t size
                , void * _data
                ) {
    struct StackRecord * data = (struct StackRecord *) _data;
    for( int i=0; i < info->dlpi_phnum; i++ ) {
        if( info->dlpi_phdr[i].p_type == PT_LOAD ) {
            ElfW(Addr) min_addr = info->dlpi_addr + info->dlpi_phdr[i].p_vaddr;
            ElfW(Addr) max_addr = min_addr + info->dlpi_phdr[i].p_memsz;
            if ((data->addr >= min_addr) && (data->addr < max_addr)) {
                data->elfFilename = info->dlpi_name;
                data->soLibAddr = data->addr - info->dlpi_addr;
                // We found a match, return a non-zero value
                return 1;
            }
        }
    }
    // We didn't find a match, return a zero value
    return 0;
}

static int
_supplement_stacktrace( StackRecords & target, size_t n ) {
    #if defined(BFD_FOUND) && BFD_FOUND
    bfd_init();
    #endif
    for( auto it = target.begin(); it != target.end(); ++it ) {
        if( dl_iterate_phdr( _so_lib_callback, &(*it) ) == 0) {
            it->failure = "dl_iterate_phdr() failed.";
            return -1;
        }
        if( it->elfFilename.length() > 0 ) {
            // This happens for shared libraries.
            // 'it->elfFilename' then contains the full path to the .so library.
            _sup_positional_info(it->elfFilename, it->soLibAddr, *it);
        } else {
            // The 'addr_in_file' is from the current executable binary.
            // It can be debugger (gdb, valgrind, etc.), so:
            //pid_t pid = getpid();
            //char bf[32];
            //snprintf( bf, 32, "/proc/%d/exe", pid ); (TODO: not work)
            _sup_positional_info( "/proc/self/exe", it->soLibAddr, *it);
        }
    }
    return 0;
}

/**@brief Obtains stacktrace info using binary file descriptor library library.
 * 0 -- ok
 */
int obtain_stacktrace( StackRecords & target ) {
    target.clear();
    void * stackPointers[MAX_STACK_DEPTH_N];
    const size_t size = backtrace(
            stackPointers,
            MAX_STACK_DEPTH_N );
    for( size_t i = 1; i < size; ++i ) {  // Note: hides own entries
        StackRecord entry {
                # if 0 /* unsupported by GCC */
                .addr       = (bfd_vma) stackPointers[i],
                .soLibAddr  = 0, .lFound = 0, .lineno     = 0,
                "",         "",         "",         "",
                # else
                0, 0,
                "",         "",         "",         "",
                # endif
                #if defined(BFD_FOUND) && BFD_FOUND
                (bfd_vma) stackPointers[i],
                0,
                0,
                # endif
            };
        target.push_front( entry );
    }
    return _supplement_stacktrace( target, size );
}

SafeString demangle_class( const char * classname ) {
    if( (!classname) || (*classname == '\0') ) {
        return "<null>";
    } else {
        int status = -4;
        char *d = 0;
        d = abi::__cxa_demangle(classname, 0, 0, &status);
        if( d ) {
            SafeString s(d);
            free(d);
            if( s.size() > 0 ) {
                return s;
            } else {
                return classname;
            }
        } else {
            return classname;
        }
    }
}

SafeString demangle_function( const SafeString & name ) {
    SafeString s;
    if( name.length() == 0 ) {
        s = "??";
    } else {
        int status = 0;
        char *d = 0;
        d = abi::__cxa_demangle(name.c_str(), 0, 0, &status);
        if (d) {
            s = d;
            free(d);
            if (s.size() > 0) {
                if (s[s.size()-1] != ')') {
                    // This means that the C++ demangler failed for some
                    // reason, as each symbol needs to end with ")". We
                    // just return the original version:
                    s = name + "()";
                }
            }
        } else {
            s = name + "()";
        }
    }

    return s;
}

static void
_fancy_print_single(std::ostream& os, const StackRecord & t) {
    const static char * ESC_BLDRED = "\033[1;31m"
                    , * ESC_CLRCLEAR = "\033[0m"
                    , * ESC_CLRGREEN = "\033[0;33m"
                    , * ESC_BLDGREEN = "\033[1;33m"
                    , * ESC_BLDWHITE = "\033[1m"
                    ;
    if( !t.failure.empty() ) os << " / "
                                << ESC_BLDRED << t.failure
                                << ESC_CLRCLEAR << " / ";
    if( !t.elfFilename.empty() ) os << t.elfFilename;
    else os << "??";
    os << " / ";
    os << "0x" << std::hex << std::setw(8) << std::setfill('0') << t.addr << " "
       << "0x" << std::hex << std::setw(8) << std::setfill('0') << t.soLibAddr << " "
       << std::dec;
    if( !t.srcFilename.empty() ) {
        os << "\n\t\tat "
           << ESC_CLRGREEN << t.srcFilename << ESC_CLRCLEAR << ":"
           << ESC_BLDGREEN << t.lineno << ESC_CLRCLEAR << ",";
    }
    os << "\n\t\tfunction " << ESC_BLDWHITE << demangle_function( t.function ) << ESC_CLRCLEAR
       ;
}

void
fancy_print( std::ostream & os, const StackRecords & recs) {
    for( const auto & rec : recs ) {
        _fancy_print_single(os, rec);
    }
}

#endif  // defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE

GenericRuntimeError::GenericRuntimeError( const char * s ) throw()
        : std::runtime_error( s ) {
    #if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
    obtain_stacktrace(_stackRecords);
    #endif
}

}  // namespace ::na64dp::errors
}  // namespace na64dp

