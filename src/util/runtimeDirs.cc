#include "na64util/runtimeDirs.hh"

#include <wordexp.h>
#include <cassert>
#include <cstring>

#include <sys/stat.h>
#include <glob.h>
#include <unistd.h>

namespace na64dp {
namespace util {

RuntimeDirs::RuntimeDirs( const char * path_ ) {
    assert( path_ && path_[0] != '\0' );  // non-empty lookup path
    char * searchPath = strdup(path_);
    char * strtokSavePtr;
    for( char * pathEntry = strtok_r(searchPath, ":", &strtokSavePtr)
       ; NULL != pathEntry
       ; pathEntry = strtok_r(NULL, ":", &strtokSavePtr) ) {
        wordexp_t p;
        char ** w;

        assert( pathEntry[0] != '\0' );
        _mlLog << "Expanding \"" << pathEntry << "\":" << std::endl;
        wordexp( pathEntry, &p, 0 );
        w = p.we_wordv;
        for( size_t i = 0; i < p.we_wordc; ++i ) {
            struct stat dirPathStat;
            int dirStatRC = stat( w[i], &dirPathStat );
            if( -1 == dirStatRC ) {
                int dirStatErrcode = errno;
                _mlLog << "  Error accessing \""
                       << w[i] << "\": "
                       << strerror(dirStatErrcode)
                       << std::endl;
                continue;
            }
            if( ! S_ISDIR( dirPathStat.st_mode ) ) {
                _mlLog << "  Omit \""
                       << w[i] << "\": "
                       << " not a directory, skip"
                       << std::endl;
                continue;
            }
            _mlLog << "  -> " << w[i] << std::endl;
            push_back( w[i] );
        }
        wordfree(&p);
    }
}

// static int _glob_errfunc()

std::vector<std::string>
RuntimeDirs::locate_files( const std::string & filePattern ) const {
    std::vector<std::string> results;
    // first, consider a trivial case when provided "pattern" is just a file
    // name or path
    if( access(filePattern.c_str(), F_OK) == 0 ) {
        results.push_back(filePattern);
        return results;
    }
    // do globbing lookup
    glob_t globbuf;
    globbuf.gl_offs = 0;
    bool ranOnce = false;
    for( auto dir : *this ) {
        /*int globRC =*/ glob( (dir + "/" + filePattern).c_str()
                         , GLOB_NOSORT | GLOB_MARK | ( ranOnce ? GLOB_APPEND | GLOB_DOOFFS : 0x0 ) | GLOB_TILDE | GLOB_BRACE
                         , NULL
                         , &globbuf );
        // ...
        //globbuf.gl_offs = globbuf.gl_pathc;
        ranOnce = true;
    }
    for( size_t i = 0; i < globbuf.gl_pathc; ++i ) {
        results.push_back( globbuf.gl_pathv[i] );
    }
    globfree( &globbuf );
    return results;
}

#if 0
std::string
find_files_by_name_in( const std::string & paths_
                     , const std::string & patterns_
                     , bool multiple
                     ) {
    RuntimeDirs rd(paths_);
    const auto rdPaths = rd.locate_files(patterns_);
    if( rdPaths.empty() ) return "";
    if( 1 == rdPaths.size() ) return rdPaths[0];
    if( !multiple ) {

    }
}
#endif

}
}

