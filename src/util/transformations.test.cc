#include "na64util/transformations.hh"

#include <gtest/gtest.h>

namespace na64dp {

using util::Vec3;
using util::Matrix3;
using util::Float_t;

TEST(Transformation, simpleUzRotationIsValid) {
    Vec3 u{{1, 0, 0}};
    
    util::Transformation t;
    t.angle(0, M_PI/2);
    t.apply(u);

    EXPECT_NEAR(u.c.x, 0, 1e-6);
    EXPECT_NEAR(u.c.y, 1, 1e-6);
    EXPECT_NEAR(u.c.z, 0, 1e-6);
}

TEST(Transformation, sizedUzRotationIsValid) {
    Vec3 u{{10, 0, 0}};

    util::Transformation t;
    t.angle(0, M_PI/2);
    t.apply(u);

    EXPECT_NEAR(u.c.x,  0, 1e-6);
    EXPECT_NEAR(u.c.y, 10, 1e-6);
    EXPECT_NEAR(u.c.z,  0, 1e-6);
}

TEST(Transformation, simpleUvRotationIsValid) {
    Vec3 v{{0, 1, 0}};
    
    util::Transformation t;
    t.angle(0, M_PI/2);
    t.apply(v);

    EXPECT_NEAR(v.c.x, -1, 1e-6);
    EXPECT_NEAR(v.c.y,  0, 1e-6);
    EXPECT_NEAR(v.c.z,  0, 1e-6);
}

TEST(Transformation, sizedUvRotationIsValid) {
    Vec3 v{{0, 10, 0}};
    
    util::Transformation t;
    t.angle(0, M_PI/2);
    t.apply(v);

    EXPECT_NEAR(v.c.x,-10, 1e-6);
    EXPECT_NEAR(v.c.y,  0, 1e-6);
    EXPECT_NEAR(v.c.z,  0, 1e-6);
}

TEST(Transformation, arbitraryVectorRotation) {
    Vec3 v{{-2.12, 12.2, -3.87}};
    // NOTE: x, y, z angles here, but internal rotation is applied in order z,y,x
    //const Float_t rot[3] = {M_PI/3, -M_PI/16, 3*M_PI/4};
    //util::apply_std_rotation(v, rot);

    util::Transformation t;
    t.angle(0,  3*M_PI/4);
    t.angle(1, -M_PI/16);
    t.angle(2,  M_PI/3);
    t.apply(v);

    // -4.02239429, -9.34407109,  8.05108562
    EXPECT_NEAR(v.c.x, -4.02239429, 1e-6);
    EXPECT_NEAR(v.c.y, -9.34407109, 1e-6);
    EXPECT_NEAR(v.c.z,  8.05108562, 1e-6);
}


TEST(Transformation, initialOffsetIsCorrect) {
    util::Transformation T;

    EXPECT_NEAR( (Vec3{{0, 0, 0}} - T.o()).norm(), 0, 1e-6 );
}

TEST(Transformation, offsetGetSetWorks) {
    util::Transformation T;
    T.o(Vec3{{1, 2, 3}});
    EXPECT_NEAR( (Vec3{{1, 2, 3}} - T.o()).norm(), 0, 1e-6 );
}

TEST(Transformation, initialBasisIsCorrect) {
    util::Transformation T;

    EXPECT_NEAR( (Vec3{{1, 0, 0}} - T.u()).norm(), 0, 1e-6 );
    EXPECT_NEAR( (Vec3{{0, 1, 0}} - T.v()).norm(), 0, 1e-6 );
    EXPECT_NEAR( (Vec3{{0, 0, 1}} - T.w()).norm(), 0, 1e-6 );
}

TEST(Transformation, basisGetSetWorks) {
    util::Transformation T;

    T.u(Vec3{{0, 0, 1}});
    T.v(Vec3{{1, 0, 1}});
    T.w(Vec3{{1, 1, 1}});

    EXPECT_NEAR( (Vec3{{0, 0, 1}} - T.u()).norm(), 0, 1e-6 );
    EXPECT_NEAR( (Vec3{{1, 0, 1}} - T.v()).norm(), 0, 1e-6 );
    EXPECT_NEAR( (Vec3{{1, 1, 1}} - T.w()).norm(), 0, 1e-6 );
}

TEST(Transformation, initialAnglesAreCorrect) {
    util::Transformation T;

    EXPECT_NEAR( T.angle(0), 0, 1e-6 );
    EXPECT_NEAR( T.angle(1), 0, 1e-6 );
    EXPECT_NEAR( T.angle(2), 0, 1e-6 );
}

TEST(Transformation, anglesGetSetWorks) {
    util::Transformation T;

    T.angle(2, 2.34);
    T.angle(0, 0.12);
    T.angle(1, 1.23);

    EXPECT_NEAR( T.angle(0), 0.12, 1e-6 );
    EXPECT_NEAR( T.angle(1), 1.23, 1e-6 );
    EXPECT_NEAR( T.angle(2), 2.34, 1e-6 );
}

// these values were pre-calculated for sample given below
static Float_t _gTransformedVectorSamples[][2][3] = {
    {{7.734556e+00, 1.062882e+01, 1.984666e+00}, {-4.492330e+01, 2.586341e+02, 3.246454e+01}},
    {{4.677710e+00, 8.355586e+00, 1.950938e+00}, {-2.818364e+01, 1.985020e+02, 3.786228e+01}},
    {{9.483337e+00, 9.465663e+00, 6.241140e+00}, {-8.540511e+01, 2.388219e+02, 2.319455e+01}},
    {{2.157431e+00, 6.181000e+00, 9.541237e+00}, {-5.366580e+01, 1.417398e+02, 6.257688e+01}},
    {{3.762719e+00, 9.179159e+00, 5.416733e+00}, {-3.462582e+01, 2.131077e+02, 6.119522e+01}},
    {{9.065874e+00, 2.088812e+00, 4.908827e+00}, {-1.043431e+02, 7.620484e+01, -3.369938e+01}},
    {{8.711467e+00, 8.200485e+00, 8.451773e+00}, {-9.500193e+01, 2.083453e+02, 2.587281e+01}},
    {{6.832368e-01, 7.340059e+00, 1.080566e+01}, {-4.310456e+01, 1.619517e+02, 8.611005e+01}},
    {{2.134734e+00, 1.008227e+01, 1.028308e+01}, {-4.180284e+01, 2.269067e+02, 9.458950e+01}},
    {{5.658832e-02, 2.580660e+00, 7.324582e+00}, {-3.922784e+01, 5.602782e+01, 4.430639e+01}}
};

TEST(Transformation, arbitraryTransformWorks) {
    const Vec3 u = {{ 10.23,    0.34,  5.67}}
             , v = {{ -0.001, -23.45, -1.23}}
             , w = {{ -1.23,   -0.34,  5.67}}
             , o = {{-12.3, 5e-7, 3.43783}}
             ;
    const Float_t angles[] = {-M_PI/123, -2*M_PI/5, 7*M_PI/8};
    util::Transformation T( o, u, v, w
                          , angles[0], angles[1], angles[2]
                          , na64sw_Rotation_xyz
                          );
    // check general validity with getters
    EXPECT_NEAR((T.u() - u).norm()/u.norm(), 0, 1e-6);
    EXPECT_NEAR((T.v() - v).norm()/v.norm(), 0, 1e-6);
    EXPECT_NEAR((T.w() - w).norm()/w.norm(), 0, 1e-6);
    EXPECT_NEAR((T.o() - o).norm()/o.norm(), 0, 1e-6);
    for(int i = 0; i < 3; ++i) {
        EXPECT_NEAR(T.angle(i), angles[i], 1e-6);
    }
    // check vs samples
    for( size_t n = 0
       ; n < sizeof(_gTransformedVectorSamples)/sizeof(*_gTransformedVectorSamples)
       ; ++n ) {
        Vec3 v{{ _gTransformedVectorSamples[n][0][0]
               , _gTransformedVectorSamples[n][0][1]
               , _gTransformedVectorSamples[n][0][2]
               }};
        Vec3 ctrl {{ _gTransformedVectorSamples[n][1][0]
                   , _gTransformedVectorSamples[n][1][1]
                   , _gTransformedVectorSamples[n][1][2]
                   }};
        Vec3 t = T(v);
        Vec3 r = t - ctrl;
        EXPECT_NEAR(r.norm()/ctrl.norm(), 0, 1e-6);
    }
}

// units

TEST(UnitsTransformation, simpleDegToRadUnitsConversionWorks) {
    float ctrl = 60.;  // degrees
    ctrl *= util::Units::get_units_conversion_factor( "deg", "angle" );
    // ^^^ ctrl should now be in radians
    EXPECT_NEAR( ctrl
               , M_PI/3
               , 1e-6 );
}

TEST(UnitsTransformation, lengthUnitsConversionWorks) {
    float ctrl = 126;  // 126 mm
    ctrl *= util::Units::get_units_conversion_factor( "mm", "m" );
    EXPECT_NEAR( ctrl
               , 0.126  // m
               , 1e-6
               );
}

}
