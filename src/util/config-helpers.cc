#include "na64util/config-helpers.hh"
#include "na64util/exception.hh"
#include "na64util/pair-hash.hh"
#include "na64util/str-fmt.hh"
#include <cctype>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <typeindex>
#include <typeinfo>
#include <yaml-cpp/exceptions.h>
#include <iostream>

#include <cstring>
#include <regex>

namespace na64dp {
namespace util {
namespace pdef {

//
// Path object

//
// TODO: perhaps, it might be better in general to substitute this hand-written
//       parser by some generated one? I foresee difficulties with future
//       maintenance of this thing...

static const std::regex gRxStrPathTok(R"~([A-Za-z_][A-Za-z_0-9\-]*)~");

bool validate_parameter_name(const char * nm) {
    if((!nm) || '\0' == nm[0]) return false;
    return std::regex_match(nm, gRxStrPathTok);
}

struct PathParserState {
    size_t nChar;
    char * c, * token;
    bool isIndex, isStr, done;

    std::vector<PathToken> & dest;

    void _reset_state() {
        token = NULL;
        isIndex = false;
        isStr = false;
    }

    void finalize_index_token() {
        assert(']' == *c);
        if((!isIndex) || NULL == token)
            throw errors::BadPath("Bad integer index notion (near closing `]')", nChar);
        *c = '\0';
        if(!strcmp(token, "*")) {
            PathToken tok;
            tok.type = PathToken::kAnyIndex;
            dest.push_back(tok);
            _reset_state();
            return;
        }
        char * strEnd = NULL;
        assert(token);
        ssize_t i = strtol(token, &strEnd, 10);
        if(strEnd != c) {
            throw errors::BadPath(util::format("Bad integer literal \"%s\" near closing `]'",
                    token).c_str(), nChar);
        }
        PathToken tok(i);
        dest.push_back(tok);
        _reset_state();
    }

    void finalize_string_token() {
        done = ('\0' == *c);
        assert(done || '.' == *c || '[' == *c);
        if(isIndex)
            throw errors::BadPath("Bad integer key"
                    " near end of expression or prior to delimiter `.' or `['"
                    " (integer index expected)", nChar);
        if(NULL == token) {
            if(done) return;  // ok for string terminator
            throw errors::BadPath("Bad string key"
                    " near end of expression or prior to delimiter `.' or `['"
                    " -- nothing to terminate", nChar);
        }
        *c = '\0';
        if(!isStr)
            throw errors::BadPath(util::format("Bad string key \"%s\""
                    " near end of expression or prior to delimiter `.' or `['"
                    , token).c_str() , nChar);
        if( 1 > (c - token)) {
            throw errors::BadPath("Empty string key, near end of expression or delimiter `.' or `['", nChar);
        }
        if(!strcmp(token, "*")) {
            if(!isStr)
                throw errors::BadPath("Unexpected `*' selector", nChar);
            PathToken tok;
            tok.type = PathToken::kAnyStrKey;
            dest.push_back(tok);
            _reset_state();
            return;
        }
        if(!std::regex_match(token, gRxStrPathTok)) {
            throw errors::BadPath(format("Bad string key (\"%s\" doesn't match"
                    " permitted pattern), near end of expression"
                    " or delimiter `.'", token).c_str(), nChar);
        }
        //std::cout << "token \"" << token << "\"" << std::endl;  // XXX
        PathToken tok(token);
        dest.push_back(tok);
        _reset_state();
    }
};

static void
tokenize_path(char * path, std::vector<PathToken> & p) {
    PathParserState state = {0, path, NULL, false, true, false, p};
    const size_t strLen = strlen(path);
    for(state.c = path; !state.done; ++state.c, ++state.nChar) {
        if(state.nChar > strLen) {
            throw errors::BadPath("Bad path expression", state.nChar - 1);
        }
        if('[' == *state.c) {
            if(state.isIndex)
                throw errors::BadPath("Bad integer index notion near opening `['", state.nChar);
            if(state.isStr) {
                if(state.c == path) {
                    // first char in expression, switch to numeric index
                    state.isIndex = true;
                    state.isStr = false;
                    *state.c = '\0';
                    assert(state.token == NULL);
                    continue;
                }
                if(NULL == state.token) {
                    throw errors::BadPath("No item to index near opening `['", state.nChar);
                }
                //std::cout << " xxx str:\"" << state.token << "\""  << std::endl;  // XXX
                state.finalize_string_token();
            } else {
                *state.c = '\0';
            }
            assert(!state.isStr);
            state.isIndex = true;
            continue;
        }
        if(']' == *state.c) {
            state.finalize_index_token();
            assert(!state.isIndex);
            assert(!state.isStr);
            assert(NULL == state.token);
            continue;
        }
        if('.' == *state.c || '\0' == *state.c) {
            if(state.isIndex && '.' == *state.c) {
                throw errors::BadPath("Expected integer index before `.' or end of line", state.nChar);
            }
            if(state.token) {
                state.finalize_string_token();
            } else {
                if('\0' != *state.c) {
                    if( (p.empty() || !(p.back().type & PathToken::kAnyIndex) ) ) {
                        throw errors::BadPath("Expected item before `.'", state.nChar);
                    }
                    // NOTE: do nothing otherwise and just expect string
                } else {
                    break;
                }
            }
            state.isStr = true;
            continue;
        }
        if(*state.c != '\0' && NULL == state.token) {
            state.token = state.c;
            if(!(state.isIndex || state.isStr)) {
                throw errors::BadPath("Unable to assume token type", state.nChar);
            }
        }
    }
    if(p.empty() && strLen) {
        throw errors::BadPath("Can not interpret path expression", strLen - 1);
    }
}

Path::Path(const char * strexpr) {
    if(!strexpr) {
        _strBuf = nullptr;
        _strBufLen = 0;
        return;
    }
    assert('\0' != *strexpr);
    _strBufLen = strlen(strexpr);
    if(_strBufLen) {
        _strBuf = static_cast<char*>(malloc(_strBufLen+1));
        memcpy(_strBuf, strexpr, _strBufLen+1);
        // tokensize expression
        try {
            tokenize_path(_strBuf, *this);
        } catch(errors::BadPath & e) {
            e.path = strexpr;
            throw e;
        }
    } else {
        _strBuf = nullptr;
    }
}

Path &
Path::operator=(const Path & orig) {
    clear();
    if(orig.empty()) return *this;
    std::string strexpr = orig.to_string();
    _strBufLen = strexpr.size();
    _strBuf = static_cast<char*>(realloc(_strBuf, _strBufLen + 1));
    memcpy(_strBuf, strexpr.c_str(), _strBufLen + 1);
    // tokensize expression
    try {
        tokenize_path(_strBuf, *this);
    } catch(errors::BadPath & e) {
        e.path = strexpr;
        throw e;
    }
    return *this;
}

Path::~Path() {
    if(_strBufLen) free(_strBuf);
    _strBufLen = 0;
}

std::string
Path::to_string() const {
    std::ostringstream oss;
    bool isFirst = true;
    for(const PathToken & tok: *this) {
        if(tok.type & PathToken::kAnyStrKey) {
            if(!isFirst) { oss << "."; }

            if(tok.type == PathToken::kStrKey) {
                oss << tok.pl.stringKey;
            } else {
                oss << "*";
            }
        } else if(tok.type & PathToken::kAnyIndex) {
            oss << "[";
            if(tok.type == PathToken::kIntKey) {
                oss << tok.pl.index;
            } else {
                oss << "*";
            }
            oss << "]";
        } else {
            NA64DP_RUNTIME_ERROR("Undefined node type in path object.");
        }
        isFirst = false;
    }
    return oss.str();
}

//
// Scalars

#if 0
template< typename SourceT
        , typename DestT> void
assign_numeric_value(DestT & dest, const SourceT & src) {
    if(src > std::numeric_limits<DestT>::max()) {
        std::stringstream oss;
        oss << "Value " << src << " is greater than destination"
            " type's upper limit (" << std::numeric_limits<DestT>::max() << ")";
        throw errors::ParameterTypeOverflow(oss.str().c_str());
    }
    if(src < std::numeric_limits<DestT>::min()) {
        std::stringstream oss;
        oss << "Value " << src << " is lesser than destination"
            " type's lower limit (" << std::numeric_limits<DestT>::min() << ")";
        throw errors::ParameterTypeOverflow(oss.str().c_str());
    }
    dest = static_cast<DestT>(src);
}

std::unordered_map< std::pair<std::type_index, std::type_index>
                  , void(*)(void *, void *)
                  , util::PairHash
                  > gConversionTable;

void
assign( void * destPtr, const std::type_info & destTI
      , void * srcPtr,  const std::type_info & srcTI
      ) {
    // ...
}
#endif

//
// Config node object

const char *
Node::node_type_name() const {
    switch(_type) {
        case kUnset:
            return "UNSET";
        case kScalar:
            return "SCALAR";
        case kArray:
            return "ARRAY";
        case kSection:
            return "SECTION";
        case kUnion:
            return "UNION";
        case kCustom:
            return "CUSTOM";
        default:
            return "?";
    };
}

Node::Section &
Node::set_section() {
    if(_type != kUnset)
        throw errors::NodeRedefinition(
                format("Node type has been set as %s already, can't reset as section"
                    , node_type_name()).c_str());
    _type = kSection;
    _pl.section = new Section;
    return *_pl.section;
}

Node::Array &
Node::set_array() {
    if(_type != kUnset)
        throw errors::NodeRedefinition(
                format("Node type has been set as %s already, can't reset as array"
                    , node_type_name()).c_str());
    _type = kArray;
    _pl.array = new Array;
    return *_pl.array;
}


BaseScalar &
Node::as_scalar() {
    if(kScalar != _type) throw errors::InvalidParameterType(
            format("Node is of type %s while scalar requested."
                , node_type_name() ).c_str());
    assert(_pl.parameterPtr);
    return static_cast<BaseScalar&>(*_pl.parameterPtr);
}

const BaseScalar &
Node::as_scalar() const {
    if(kScalar != _type) throw errors::InvalidParameterType(
            format("Node is of type %s while (const) scalar requested."
                , node_type_name() ).c_str());
    assert(_pl.parameterPtr);
    return static_cast<BaseScalar&>(*_pl.parameterPtr);
}


Node::Section &
Node::as_section() {
    if(kSection != _type)
        throw errors::InvalidParameterType(
            format("Node is of type %s while section requested."
                , node_type_name() ).c_str());
    assert(_pl.section);
    return static_cast<Section&>(*_pl.section);
}

const Node::Section &
Node::as_section() const {
    if(kSection != _type)
        throw errors::InvalidParameterType(
            format("Node is of type %s while (const) section requested."
                , node_type_name() ).c_str());
    assert(_pl.section);
    return static_cast<const Section&>(*_pl.section);
}


Node::Array &
Node::as_array() {
    if(kArray != _type)
        throw errors::InvalidParameterType(
            format("Node is of type %s while array requested."
                , node_type_name() ).c_str());
    assert(_pl.array);
    return static_cast<Array&>(*_pl.array);
}

const Node::Array &
Node::as_array() const {
    if(kArray != _type)
        throw errors::InvalidParameterType(
            format("Node is of type %s while (const) array requested."
                , node_type_name() ).c_str());
    assert(_pl.array);
    return static_cast<const Array&>(*_pl.array);
}


Node &
Node::operator[](const std::string & nm) {
    auto & sec = as_section();
    auto it = sec.find(nm);
    if(sec.end() == it) {
        throw errors::NoKeyInSection(nm.c_str());
    }
    assert(it->second);
    return *it->second;
}

const Node &
Node::operator[](const std::string & nm) const {
    auto & sec = as_section();
    auto it = sec.find(nm);
    if(sec.end() == it) {
        throw errors::NoKeyInSection(nm.c_str());
    }
    assert(it->second);
    return *it->second;
}

bool
Node::has(const std::string & nm) const {
    auto & sec = as_section();
    auto it = sec.find(nm);
    return sec.end() != it;
}


Node &
Node::operator[](ssize_t n) {
    return const_cast<Node&>(static_cast<const Node *>(this)->operator[](n));
}

const Node &
Node::operator[](ssize_t n) const {
    const ssize_t origN = n;
    auto & arr = as_array();
    if(n < 0) {
        n = arr.size() + n;
        if(n < 0)
            throw errors::NoItemInArray(origN);
    }
    assert(n >= 0);
    assert(static_cast<size_t>(n) <= std::numeric_limits<size_t>::max());
    size_t n_ = n;
    if(n_ >= arr.size())
        throw errors::NoItemInArray(origN);
    return *arr[n_];
}

bool
Node::has(ssize_t n) const {
    auto & arr = as_array();
    if(n < 0) {
        n = arr.size() + n;
        if(n < 0)
            return false;
    }
    assert(n >= 0);
    assert(static_cast<size_t>(n) <= std::numeric_limits<size_t>::max());
    size_t n_ = n;
    if(n_ >= arr.size())
        return false;
    return true;
}

//
// Composer

Composer::Composer(Node & n) {
    _stack.push_back({n, "", 0});
}

void
Composer::_assure_section() {
    if(_stack.back().node.is_section()) return;
    if(!_stack.back().node.is_unset()) {
        throw errors::InvalidParameterType(format("Parent node is"
                    " of %s type while section implied"
                    , _stack.back().node.node_type_name()).c_str());
    }
    _stack.back().node.set_section();
}

void
Composer::_assure_array() {
    if(_stack.back().node.is_array()) return;
    if(!_stack.back().node.is_unset()) {
        throw errors::InvalidParameterType(format("Parent node is of %s type"
                    " while array implied"
                    , _stack.back().node.node_type_name()).c_str());
    }
    _stack.back().node.set_array();
}

}  // namespace ::na64dp::util::pdef

#if 0
void
Node::apply(const YAML::Node & yamlValueNode) {
    switch( _type ) {
        case kScalar:
            _pl.parameterPtr->set(yamlValueNode);
        break;
        case kSection:
            for(auto & p : *_pl.section) {
                YAML::Node v;
                try {
                    v = yamlValueNode[p.first];
                } catch(YAML::Exception & e) {
                    auto ee = errors::GenericYAMLError(e
                            , "YAML emitted error"
                            );
                    ee.path.push_front(p.first);
                    throw ee;
                }
                try {
                    p.second->apply(v);
                } catch( na64dp::errors::PDefError & e ) {
                    e.path.push_back(p.first);
                    throw e;
                }
            }
        break;
        // ...
        default:
            throw std::runtime_error("Inconsistency in pdef: bad node type.");  // TODO
    };
}

}  // namespace ::na64dp::util::pdef

void
RuntimeCfg::_finalize_last_node() {
    assert(_lastNode);
    assert(_lastNodeName);
    try {
        _lastNode->_check_consistency();
    } catch( errors::PDefError & pdErr ) {
        pdErr.path.push_front(_lastNodeName);
        throw pdErr;
    }
    _nodes.emplace(_lastNodeName, _lastNode);
    _lastNode = nullptr;
}
#endif

}  // namespace ::na64dp::util
}  // namespace na64dp

