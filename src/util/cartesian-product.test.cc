#include "na64util/cartesian-product.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace test {

TEST(CartesianProduct, Basic) {
    static const int data[][5] = {
        {1},
        {3,4,5},
        {6,7},
    };

    static const int tstSeq[][3] = {
        {1, 3, 6}, {1, 3, 7},
        {1, 4, 6}, {1, 4, 7},
        {1, 5, 6}, {1, 5, 7},
    };

    std::vector<int>   one(data[0], data[0] + 1)
                   ,   two(data[1], data[1] + 3)
                   , three(data[2], data[2]+2 );
    util::CartesianProduct< 3, std::vector<int> > g(one, two, three);
    std::array<int, 3> c;
    size_t n = 0;
    while(g >> c) {
        EXPECT_EQ(tstSeq[n][0], c[0]);
        EXPECT_EQ(tstSeq[n][1], c[1]);
        EXPECT_EQ(tstSeq[n][2], c[2]);
        ++n;
    }
}

TEST(CartesianProduct, Collections) {
    static const int tstSeq[][3] = {
        {4, 3, 1}, {4, 3, 2},
        {5, 3, 1}, {5, 3, 2},
    };

    util::CartesianProductCollections<int, 3> col;
    col.add(0, 4); col.add(0, 5);
    col.add(1, 3);
    col.add(2, 1); col.add(2, 2);

    util::CartesianProduct< 3, std::vector<int> > g = col.get();
    std::array<int, 3> c;
    size_t n = 0;
    while(g >> c) {
        EXPECT_EQ(tstSeq[n][0], c[0]);
        EXPECT_EQ(tstSeq[n][1], c[1]);
        EXPECT_EQ(tstSeq[n][2], c[2]);
        ++n;
    }
}

}
}

