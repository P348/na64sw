#include "na64util/observer.hh"

#include "na64util/observer.hh"

#include <gtest/gtest.h>

/**\file pubsub.cc
 *
 * Testing unit for the templated pubusb implementation
 * */

namespace na64dp {
namespace test {

class MockIntObserver : public util::Observable<int>::iObserver {
public:
    bool intUpdated;
    MockIntObserver() : intUpdated(false) {}
protected:
    virtual void handle_update( const int   & ) override { intUpdated   = true; }
};

class MockFloatObserver : public util::Observable<float>::iObserver {
public:
    bool floatUpdated;
    MockFloatObserver() : floatUpdated(false) {}
protected:
    virtual void handle_update( const float & ) override { floatUpdated   = true; }
};

class MockObserver : public util::Observable<int>::iObserver
                   , public util::Observable<float>::iObserver {
public:
    bool intUpdated, floatUpdated;
    MockObserver() : intUpdated(false), floatUpdated(false) {}
protected:
    virtual void handle_update( const int   & ) override { intUpdated   = true; }
    virtual void handle_update( const float & ) override { floatUpdated = true; }
};

TEST(Observer, basics) {
    util::Observable<int> intObservable;
    util::Observable<float> floatObservable;

    MockObserver observer;

    intObservable.notify_observers(0);
    floatObservable.notify_observers(0);

    // assure mock initialized and no notification had changes yet
    ASSERT_FALSE( observer.intUpdated );
    ASSERT_FALSE( observer.floatUpdated );

    // bind observer and observables
    intObservable.bind_observer(observer);
    floatObservable.bind_observer(observer);

    intObservable.notify_observers(0);

    ASSERT_TRUE(  observer.intUpdated );
    ASSERT_FALSE( observer.floatUpdated );

    floatObservable.notify_observers(0);

    ASSERT_TRUE( observer.intUpdated );
    ASSERT_TRUE( observer.floatUpdated );

    observer.intUpdated = observer.floatUpdated = false;

    intObservable.unbind_observer(observer);

    intObservable.notify_observers(0);
    floatObservable.notify_observers(0);

    ASSERT_FALSE( observer.intUpdated );
    ASSERT_TRUE( observer.floatUpdated );
}

TEST(Observer, observablesSet) {
    util::Observables observables;

    util::Observables::ConcreteObservable<int> intValue;
    util::Observables::ConcreteObservable<float> floatValue;

    MockFloatObserver floatObserver;
    MockIntObserver intObserver;
    MockObserver observer;

    // Check that we can not update non-existing observables
    EXPECT_THROW( observables.get_observable<int>().notify_observers(0)
                , error::UnknownObservableType );
    EXPECT_THROW( observables.get_observable<float>().notify_observers(0)
                , error::UnknownObservableType );
    // Check that nothing was really updated yet
    EXPECT_FALSE(intObserver.intUpdated);
    EXPECT_FALSE(floatObserver.floatUpdated);
    EXPECT_FALSE(observer.intUpdated || observer.floatUpdated);

    // Add observables and emit event
    observables.add_observable( intValue );
    observables.get_observable<int>().notify_observers(0);
    EXPECT_THROW( observables.get_observable<float>().notify_observers(0)
                , error::UnknownObservableType );
    // Check that nothing was really updated yet
    EXPECT_FALSE(intObserver.intUpdated);
    EXPECT_FALSE(floatObserver.floatUpdated);
    EXPECT_FALSE(observer.intUpdated || observer.floatUpdated);

    observables.add_observable( floatValue );

    observables.get_observable<int>().bind_observer( intObserver );
    observables.get_observable<int>().bind_observer( observer );
    // Check that nothing was really updated yet
    EXPECT_FALSE(intObserver.intUpdated);
    EXPECT_FALSE(floatObserver.floatUpdated);
    EXPECT_FALSE(observer.intUpdated || observer.floatUpdated);

    observables.get_observable<int>().notify_observers(0);

    // Check that int observables got updating signal and float is kept intact
    EXPECT_TRUE(intObserver.intUpdated);
    EXPECT_FALSE(floatObserver.floatUpdated);
    EXPECT_TRUE(observer.intUpdated);
    EXPECT_FALSE(observer.floatUpdated);
}

}
}

