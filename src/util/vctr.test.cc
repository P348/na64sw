#include "na64util/vctr.hh"

#include <gtest/gtest.h>

namespace na64dp {

namespace test {
class iMockEntity { public: virtual ~iMockEntity(){} };
}

template<>
struct CtrTraits<test::iMockEntity> {
    typedef test::iMockEntity * (*Constructor)(int);
};

namespace test {

class MockEntity_A : public iMockEntity { };
class MockEntity_B : public iMockEntity { };

static iMockEntity * construct_a( int ) { return new MockEntity_A(); }
static iMockEntity * construct_b( int ) { return new MockEntity_B(); }

TEST(VCtr, CommonFunctions) {
    VCtr::self().register_class<iMockEntity>( "A", construct_a, "mock ctr A" );
    VCtr::self().register_class<iMockEntity>( "B", construct_b, "mock ctr B" );

    int x;
    iMockEntity * aPtr = VCtr::self().make<iMockEntity>("A", x = 0)
              , * bPtr = VCtr::self().make<iMockEntity>("B", x = 1)
              ;
    ASSERT_TRUE(aPtr);
    ASSERT_TRUE(bPtr);
    MockEntity_A * aPtrGenuine = dynamic_cast<MockEntity_A *>(aPtr);
    MockEntity_B * bPtrGenuine = dynamic_cast<MockEntity_B *>(bPtr);
    ASSERT_TRUE(aPtrGenuine);
    ASSERT_TRUE(bPtrGenuine);
    delete aPtrGenuine;
    delete bPtrGenuine;

    EXPECT_THROW( VCtr::self().make<iMockEntity>("C", x = 3)
                , errors::CtrNotFound );
}

}
}
