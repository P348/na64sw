#include "na64app/extension.hh"

#include <log4cpp/Category.hh>

namespace na64dp {

Extensions * Extensions::_self = nullptr;

Extensions::Extensions() {}

Extensions &
Extensions::self() {
    if(!_self) _self = new Extensions();
    return *_self;
}

bool
Extensions::add( const std::string & name
               , iRuntimeExtension * extPtr
               ) {
    auto ir = emplace(name, extPtr);
    if(!ir.second) {
        throw errors::DuplicateExtensionName(name);
    }
    return true;
}

iRuntimeExtension &
Extensions::operator[](const std::string & nm) {
    auto extIt = find(nm);
    if(extIt == end()) {
        throw errors::NoExtension(nm);
    }
    return *(extIt->second);
}

void
Extensions::set_parameter( const std::string & parPath
                         , const std::string & parVal
                         ) {
    auto c = parPath.find('.');
    if(std::string::npos == c) {
        NA64DP_RUNTIME_ERROR( "Parameter path string \"%s\" have no extension"
                " name delimiter (.)", parPath.c_str() );
    }
    std::string extName = parPath.substr(0, c)
              , parName = parPath.substr(c+1)
              ;
    // If parameter has to be set BEFORE logging is initialized, consider
    // to delete line below:
    log4cpp::Category::getInstance("extensions") << log4cpp::Priority::DEBUG
        << " setting parameter \"" << parName << "\" of extension \""
        << extName << "\" to \"" << parVal << "\"";
    this->operator[](extName).set_parameter(parName, parVal);
}

void
Extensions::init( calib::Manager & cmgr, iEvProcInfo * epiPtr ) {
    for( auto p : *this ) {
        p.second->init(cmgr, epiPtr);
    }
}

void
Extensions::finalize() {
    for( auto p : *this ) {
        p.second->finalize();
    }
}

void
Extensions::shutdown() {
    for( auto p : *this ) {
        delete p.second;
    }
    clear();
}

}  // namespace na64dp

