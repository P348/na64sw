#include "na64calib/placements.hh"

namespace na64dp {
namespace util {

Transformation
transformation_by(const calib::Placement & pl) {
    // TODO: check validity of the placement (everything set)
    // TODO: support for non-rectangular frames (affects basis vectors)
    return Transformation( Vec3{{pl.center[0], pl.center[1], pl.center[2]}}
                         , Vec3{{pl.size[0], 0, 0}}
                         , Vec3{{0, pl.size[1], 0}}
                         , Vec3{{0, 0, pl.size[2]}}
                         , pl.rot[0], pl.rot[1], pl.rot[2]
                         , pl.rotationOrder
                         );
}

}  // namespace ::na64dp::util
}  // namespace na64dp

