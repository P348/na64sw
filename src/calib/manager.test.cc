#include "na64calib/manager.hh"
#include <gtest/gtest.h>

namespace na64dp {
namespace calib {

// TODO
#if 0

/* Controlling byte consists of two octets steering, correspondingly, two
 * loaders. Bits in octets:
 *  1 (0x1) - index has to emit update
 *  2 (0x2) - loader1 / loader2
 *  3 (0x4) - "foo" / "bar"
 *  4 (0x8) - int / float
 *  
 * Note, that upon update at least one bit in octet (first, 0x1 or 0x10) is set
 * unconditionally, indicating that update has been performed.
 *
 * First inserted index (index1) takes precedence, so if first bit (0x1) is set
 * AND the 3rd and 4th bits are identical in both octets, the second octet
 * shall stay empty. This way we check calibration loaders order precedence
 * while combinatorially providing coverage over all the possible cases of:
 *  - two distinct indexes
 *  - two distinct loaders
 *  - over four distinct calibration data types
 */

class MockIndex : public iIndex {
public:
    const int n;
    MockIndex(int n_) : n(n_) {}
    void updates( EventID oldEventID, EventID newEventID
                , const std::pair<time_t, uint32_t> & oldDatetime
                , const std::pair<time_t, uint32_t> & newDateTime
                , Updates & ul ) override {
        uint8_t ctrlOctet = newEventID.run_no();
        if( 1 == n ) {
            ctrlOctet &= 0xf;
        } else {
            ctrlOctet >>= 4;
        }
        if( ! (ctrlOctet & 0x1) ) return;
        oldEventID.event_no(n - 1);
        std::string loaderName = (0x2 & ctrlOctet) ? "loader1" : "loader2"
                  , calibTypeName = (0x4 & ctrlOctet) ? "foo" : "bar"
                  ;
        YAML::Node pl;
        pl["eventID"] = oldEventID.event_no();
        if( 0x8 & ctrlOctet ) {
            ul.enqueue_update( "int-" + calibTypeName, iIndex::UpdateRecipe{loaderName, pl} );
        } else {
            ul.enqueue_update( "float-" + calibTypeName, iIndex::UpdateRecipe{loaderName, pl} );
        }
    }
};

class MockLoader : public iLoader {
public:
    const int n;
    uint8_t & ctrlByte;
    MockLoader( int n_, uint8_t & ctrlByte_ )
            : n(n_)
            , ctrlByte(ctrlByte_) {}
    /// Loads calibration data into given dispatcher instance
    virtual void load_data( const iIndex::UpdateRecipe & updRecipe
                          , Dispatcher::CIDataID cidID
                          , Dispatcher & ) override {
        std::string calibName = cidID.second;
        std::type_index calibTypeIndex = cidID.first;
        uint8_t ctrlSeq = 0x0;
        if( calibName == "foo" ) {
            ctrlSeq |= 0x4;
        } else {
            ASSERT_STREQ( calibName.c_str(), "bar" );
        }
        if( std::type_index(typeid(int)) == calibTypeIndex ) {
            ctrlSeq |= 0x8;
        } else {
            ASSERT_EQ(std::type_index(typeid(float)), calibTypeIndex);
        }
        if( 1 == n ) {
            ctrlSeq |= 0x2;
        }
        ctrlSeq |= 0x1;  // unconditionally set, indicating that index has emitted update
        EventID eventID( updRecipe.payload["eventID"].as<na64sw_EventID_t>() );
        ctrlByte |= (ctrlSeq << (eventID.event_no() ? 4 : 0) );
    }
    /// Shall return whether the specified calibration is available
    virtual bool provides( const iIndex::UpdateRecipe &
                         , Dispatcher::CIDataID ) override {
        return true;
    }
protected:
    virtual void put_provided_types( iLoader::CITypeAliases & pt ) override {
        pt.emplace("int-foo", Dispatcher::info_id<int>("foo"));
        pt.emplace("int-bar", Dispatcher::info_id<int>("bar"));
        pt.emplace("float-foo", Dispatcher::info_id<float>("foo"));
        pt.emplace("float-bar", Dispatcher::info_id<float>("bar"));
    }
};

class CalibManagerTest : public ::testing::Test {
public:
    MockIndex index1
            , index2
            ;
    MockLoader loader1
             , loader2
             ;
    uint8_t ctrlByte;
    calib::Manager mgr;
    calib::Handle<int> intFoo
                     , intBar
                     ;
    calib::Handle<float> floatFoo
                       , floatBar
                       ;
    CalibManagerTest() : index1(1),  index2(2)
                       , loader1(1, ctrlByte), loader2(2, ctrlByte)
                       , intFoo("foo", mgr), intBar("bar", mgr)
                       , floatFoo("foo", mgr), floatBar("bar", mgr) {
        mgr.add_run_index( &index1 );
        mgr.add_run_index( &index2 );
        mgr.add_loader( "loader1", &loader1 );
        mgr.add_loader( "loader2", &loader2 );
    }
};

TEST_F( CalibManagerTest, resolveUpdates ) {
    mgr.event_id( EventID{0, 1, 1}, {0, 0} );
    for( uint8_t i = 0; i < 0xff; ++i ) {
        ctrlByte = 0x0;
        mgr.event_id( EventID{i, 1, 1}, {0, 0} );
        if( i & 0x11 ) {  // manager has to emit updates once per 0x11 = 17 events
            bool useIndex1 = 0x1 & i
               , useIndex2 = 0x10 & i
               , typeMatches = (((0xf & i) & 0xc) == ((i >> 4) & 0xc))
               // ^^^ means that both octets describes updating of the same
               // type
               ;
            uint8_t idxOctet1 = 0xf & i
                  , idxOctet2 = i >> 4
                  , ctrlOctet1 = 0xf & ctrlByte
                  , ctrlOctet2 = ctrlByte >> 4
                  ;
            if( useIndex1 && useIndex2 && typeMatches ) {
                // A special case -- collision; both indexes enqueued update of
                // the same calibration information type. Manager has to treat
                // this case in a favor of loader being inserted first into
                // updates list, so octet2 is only important here
                // - Assure info was loaded by first loader
                EXPECT_EQ( ctrlOctet1, idxOctet1 ) << "at case#" << (int) i
                                                   << ", ctrl=" << (int) ctrlByte;
                // - Assure the second loader was not invoked
                EXPECT_FALSE( ctrlOctet2 ) << "at case#" << (int) i
                                                    << ", ctrl=" << (int) ctrlByte;
            } else {
                if( useIndex1 && useIndex2 ) {
                    EXPECT_EQ( i, ctrlByte ) << "at case#" << (int) i;
                } else if( useIndex1 ) {
                    EXPECT_EQ( idxOctet1, ctrlOctet1 ) << "at case#" << (int) i;
                } else if( useIndex2 ) {
                    EXPECT_EQ( idxOctet2, ctrlOctet2 ) << "at case#" << (int) i;
                }
            }
        } else {
            // prohibit updates
            EXPECT_FALSE( ctrlByte );
        }
    }
}

#endif
}
}
