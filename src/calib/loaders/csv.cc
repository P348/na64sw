#include "na64calib/loaders/csv.hh"

#if 0
#include "na64util/uri.hh"

namespace na64dp {
namespace calib {

void
CSVLoader::load_data( const iIndex::UpdateRecipe & recipe
                    , Dispatcher::CIDataID dataID
                    , Dispatcher & dsp
                    ) {
    // Find the conversion callback
    auto it = _converters.find(dataID);
    if( _converters.end() == it ) {
        NA64DP_RUNTIME_ERROR( "No data conversion from CSV to type %s defined"
                " for generic CSV loader.", util::calib_id_to_str(dataID).c_str() );
    }
    // Reach the file and construct the reader around it

    std::string path = recipe.payload["source"].as<std::string>();
    auto names = util::expand_names( path );
    if( 1 != names.size() ) {
        NA64DP_RUNTIME_ERROR( "Failed to expand string expression to the"
                " file name: \"%s\".", path.c_str() );
    }

    _log.debug( "Loading CSV file \"%s\"...", names[0].c_str() );
    std::ifstream ifs(names[0]);
    if( ifs.is_open() ) {
        csv::Reader reader(ifs);
        // Perform the conversion and dispatch the data
        it->second->convert_and_dispatch(reader, dsp);
    } else {
        NA64DP_RUNTIME_ERROR( "Can't open file \"%s\".", names[0].c_str() );
    }
    _log.info( "Calibration info from CSV source \"%s\" has been loaded into %s."
              , names[0].c_str()
              , util::calib_id_to_str(dataID).c_str() );
}

void
CSVLoader::put_provided_types( CITypeAliases & aliases ) {
    assert(!_aliases);  // same loader instance is added to another manager?
    _aliases = &aliases;
}

bool
CSVLoader::provides( const iIndex::UpdateRecipe & updRecipe
                   , Dispatcher::CIDataID dataID ) {
    // TODO: check if the file is reachable using updRecipe.payload["source"]
    return _converters.end() != _converters.find(dataID);
}

}
}
#endif

