#include "na64calib/loaders/yaml-document.hh"
#include "na64util/uri.hh"
#include "na64calib/manager.hh"

namespace na64dp {
namespace calib {

void
YAMLDocumentLoader::load( const iUpdate & recipe
                        , Dispatcher & dsp
                        ) {
    auto rttiIt = CIDataAliases::self()
                    .name_by_type_id()
                    .find(recipe.subject_data_type());
    if( CIDataAliases::self().name_by_type_id().end() == rttiIt ) {
        NA64DP_RUNTIME_ERROR( "Can't lookup for conversion of unaliased type"
                " %s."
                , util::calib_id_to_str(recipe.subject_data_type()).c_str() );
    }
    const std::string & typeAlias = rttiIt->second;
    // Find the conversion callback
    auto it = _conversions.find(typeAlias);
    if( _conversions.end() == it ) {
        NA64DP_RUNTIME_ERROR( "No data conversion from YAML node to type \"%s\""
                " (alias to %s) defined for generic YAML document loader."
                , typeAlias.c_str()
                , util::calib_id_to_str(recipe.subject_data_type()).c_str() );
    }

    // Reach the file and parse it to the YAML node
    YAML::Node n;
    // Downcast to yaml update
    const iSpecificUpdate<YAML::Node> & upd
        = static_cast<const iSpecificUpdate<YAML::Node>&>(recipe);
    if( upd.payload["source"] ) {
        // load source
        std::string path = upd.payload["source"].as<std::string>();
        auto names = util::expand_names( path );
        if( 1 != names.size() ) {
            NA64DP_RUNTIME_ERROR( "Failed to expand string expression to the"
                    " file name: \"%s\".", path.c_str() );
        }
        _log << log4cpp::Priority::DEBUG
             << "\"source\" field provided for update -- reading file \""
             << path << "\" expanded as \"" << names[0].c_str() << "\"..."
             ;
        try {
            n = YAML::LoadFile( names[0] );
        } catch( std::exception & e ) {
            _log.error( "Error occured while accessing, reading or parsing"
                    " YAML calibration data \"%s\": %s"
                    , names[0].c_str()
                    , e.what() 
                    );
            throw;
        }
        _log.debug( "Converting data of YAML document \"%s\" into %s..."
                  , names[0].c_str()
                  , util::calib_id_to_str(recipe.subject_data_type()).c_str() );
        // Perform the conversion and dispatch the data
        try {
            it->second(n, dsp);
        } catch( std::exception & e ) {
            _log.error( "During conversion of YAML document data \"%s\" into"
                    " %s occured an error: %s."
                    , names[0].c_str()
                    , util::calib_id_to_str(recipe.subject_data_type()).c_str()
                    , e.what()
                    );
            throw;
        }
        _log.debug( "Calibration info from YAML document \"%s\" has been loaded into %s."
                  , names[0].c_str()
                  , util::calib_id_to_str(recipe.subject_data_type()).c_str() );
    } else if( upd.payload["payload"] ) {
        // directly use information from payload
        _log << log4cpp::Priority::DEBUG
             << "\"payload\" field provided for update -- converting it to "
             << util::calib_id_to_str(recipe.subject_data_type()) << "..."
             ;
        // Perform the conversion and dispatch the data
        it->second(upd.payload["payload"], dsp);
        _log.debug( "Calibration data from YAML node has been loaded into %s."
                  , util::calib_id_to_str(recipe.subject_data_type()).c_str() );
    } else {
        // This must be a result of a wrong specification
        NA64DP_RUNTIME_ERROR( "Update did not bring payload info meaningful"
                " for this loader (perhaps, wrong specification of"
                " recommended loader in the update)." );
    }
}

bool
YAMLDocumentLoader::can_handle( const iUpdate & recipe ) {
    // This handler is capable to handle update if:
    //  - type is aliased
    //  - the subject type conversion is defined
    //  - update type can be downcasted to the iSpecificUpdate<YAML::Node>
    //  - YAML node has "payload" or "source" fields
    auto rttiIt = CIDataAliases::self()
                    .name_by_type_id()
                    .find(recipe.subject_data_type());
    if( CIDataAliases::self().name_by_type_id().end() == rttiIt ) {
        return false;  // unaliased type
    }
    const std::string & typeAlias = rttiIt->second;

    auto it = _conversions.find(typeAlias);
    if( _conversions.end() == it ) return false;  // no conversion defined

    auto ptr = dynamic_cast<const iSpecificUpdate<YAML::Node>*>(&recipe);
    if(!ptr) return false;  // downcast failed

    return ((bool) ptr->payload["payload"])
        || ((bool) ptr->payload["source"]);
}

void
YAMLDocumentLoader::define_conversion(
                  const std::string & aliasName
                , void (*callback)(const YAML::Node &, Dispatcher &)
                ) {
    auto ir = _conversions.emplace(aliasName, callback);
    if( ! ir.second ) {
        if(ir.first->second != ir.first->second)
            NA64DP_RUNTIME_ERROR( "Ambiguious conversion for type alias"
                    " \"%s\" requested (different conversion"
                    " callback for this type is provided to YAML document"
                    " loader)."
                    , aliasName.c_str() );
        _log.debug( "Repeated registration of the conversion callback for alias"
                " \"%s\" is omitted.", aliasName.c_str() );
    } else {
        _log.debug( "Added YAML document conversion for type with alias \"%s\"."
                  , aliasName.c_str() );
    }
}

void
YAMLDocumentLoader::to_yaml_as_loader( YAML::Node & node ) const {
    char bf[128];
    node["_type"] = "YAMLDocumentLoader";
    snprintf(bf, sizeof(bf), "%p", this);
    node["_ptr"] = bf;
    YAML::Node conversions;
    for( auto cnvPair : _conversions ) {
        snprintf(bf, sizeof(bf), "%p", cnvPair.second);
        conversions[cnvPair.first] = bf;
    }
    node["conversions"] = conversions;
}


#if 0
void
YAMLDocumentLoader::put_provided_types( CITypeAliases & aliases ) {
    assert(!_aliases);  // same loader instance is added to another manager?
    _aliases = &aliases;
}

bool
YAMLDocumentLoader::can_handle( const iIndex::UpdateRecipe & updRecipe
                              , Dispatcher::CIDataID dataID ) {
    // TODO: check if the file is reachable using updRecipe.payload["source"]
    return _callbacks.end() != _callbacks.find(dataID);
}
#endif

}
}

