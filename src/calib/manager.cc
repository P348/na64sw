#include "na64calib/manager.hh"
#include "na64util/str-fmt.hh"
#include "na64util/tsort.hh"

#include "yaml-cpp/yaml.h"

#if defined(jsoncpp_FOUND) && jsoncpp_FOUND
#   include <json/writer.h>
#endif

// create_calibration_handle() shall know about all the existing handle
// subclasses. TODO: refactor it for VCtr
//#include "na64calib/YAMLCalibHandle.hh"

#include <fstream>
#include <unordered_set>

namespace na64dp {
namespace calib {

CIDataAliases * CIDataAliases::_self = nullptr;

CIDataAliases::CIDataAliases() : _logPtr(nullptr), _depsCacheValid(false) {}

CIDataAliases &
CIDataAliases::self() {
    if(!_self) _self = new CIDataAliases();
    return *_self;
}

void
CIDataAliases::add_alias( const std::string & name
                        , Dispatcher::CIDataID dataID ) {
    _depsCacheValid = false;
    {
        auto ir = _nameToTypeID.emplace(name, dataID);
        auto it = ir.first;
        if( (!ir.second) && dataID != it->second ) {
            NA64DP_RUNTIME_ERROR( "Calibration data type alias ambiguity:"
                    " name \"%s\" is already reserved for %s, while"
                    " another type (%s) claims same name now."
                    , name.c_str()
                    , util::calib_id_to_str(it->second, true).c_str()
                    , util::calib_id_to_str(dataID, true).c_str()
                    );
        }
    }
    {
        auto ir = _typeIDToName.emplace(dataID, name);
        auto it = ir.first;
        if( _typeIDToName.end() == it && name != it->second ) {
            NA64DP_RUNTIME_ERROR( "Calibration data type alias ambiguity:"
                    " type %s is already aliased by name \"%s\", while"
                    " it is referenced again by name alias \"%s\"."
                    , util::calib_id_to_str(dataID, true).c_str()
                    , it->second.c_str()
                    , name.c_str()
                    );
        }
    }
    if( _logPtr )
        _logPtr->debug(
                "Alias \"%s\" from now refers to data type %s."
                , name.c_str()
                , util::calib_id_to_str(dataID, true).c_str()
            );
}

void
CIDataAliases::init_logging() {
    if( _logPtr ) {
        _logPtr->debug( "Skipping repeated CIDataAliases logging initialization." );
        return;
    }
    _logPtr = &(log4cpp::Category::getInstance( "calib.aliases" ));
    std::ostringstream oss;
    oss << "Existing type aliases: ";
    bool isFirst = true;
    for( const auto & p : _nameToTypeID ) {
        if(!isFirst) oss << ", "; else isFirst = false;
        oss << "\"" << p.first << "\":" << util::calib_id_to_str(p.second, true);
    }
    _logPtr->debug( oss.str() );
    #if 0
    oss.clear();
    if( !_depends.empty() ) {
        oss << "Type dependencies: ";
        for()  // ... TODO
    }
    #endif
}

void
CIDataAliases::add_dependency( const std::string & product
                             , const std::string & requirement
                             ) {
    _depsCacheValid = false;
    auto itProd = _nameToTypeID.find(product);
    if( _nameToTypeID.end() == itProd ) {
        NA64DP_RUNTIME_ERROR( "Unknown dependee type alias \"%s\""
                            , product.c_str() );
    }
    auto er = _depends.equal_range(itProd->second);
    // if we already know this dependency, just skip it
    for( auto it = er.first; it != er.second; ++it ) {
        if(it->second != requirement) continue;
        if(_logPtr)
            _logPtr->debug(
                    "Calib data type dependency already known: \"%s\" depends"
                    " on \"%s\"."
                    , product.c_str()
                    , requirement.c_str()
                );
        return;  // known
    }

    _depends.emplace(itProd->second, requirement);
    if( _logPtr )
        _logPtr->debug(
                "New calib data type dependency registered: \"%s\""
                " depends on \"%s\"."
                , product.c_str()
                , requirement.c_str()
            );
}

const std::unordered_multimap< Dispatcher::CIDataID
                             , Dispatcher::CIDataID
                             , util::PairHash > &
CIDataAliases::dependency_map() const {
    if( _depsCacheValid ) return _depsCache;
    _depsCache.clear();

    for( const auto & p : _depends ) {
        auto it = type_id_by_name().find(p.second);
        if( type_id_by_name().end() == it ) {
            NA64DP_RUNTIME_ERROR("Unknown type alias \"%s\" provided as a"
                    " requirement for %s."
                    , p.second.c_str()
                    , util::calib_id_to_str(p.first).c_str()
                    );
        }
        _depsCache.emplace(p.first, it->second);
    }

    _depsCacheValid = true;
    return _depsCache;
}

//
// Manager
/////////

Manager::Manager(log4cpp::Category & L) : Dispatcher(L) {}
Manager::~Manager() {}

void Manager::_collect_updates( Updates & upds
                     , EventID eventID
                     , const std::pair<time_t, uint32_t> & eventTime
                     ) const {
    for(auto iPtr : _indeces) {
        iPtr->append_updates( _cEventID, eventID
                            , _cDatetime, eventTime
                            , upds );
    }
}

void
Manager::_load_updates( const Updates & updates ) {
    size_t nLoaded = 0;
    // load updates if they are relevant (at least one observable exists)
    for( const iUpdate * updateEntry : updates ) {
        if(!observable_exists(updateEntry->subject_data_type())) {
            _log.debug( "Update of type %s is not in demand."
                      , util::calib_id_to_str(updateEntry->subject_data_type(), true).c_str()
                      );
            continue;
        }
        if(updateEntry->recommended_loader()) {
            #ifndef NDEBUG
            // returned is const ptr, we shall find non-const ptr within
            // a mgr to assure it is owned by mgr. TODO: is it needed?
            // For instance, registering a loader in the mgr will guarantee
            // correct accounting of the deps prior to _sort_updates()
            auto it = std::find_if( _loaders.begin(), _loaders.end()
                    , [&]( const std::pair<std::string, std::shared_ptr<iLoader>> & p ){
                          return updateEntry->recommended_loader() == p.second.get();
                    });
            if( _loaders.end() == it ) {
                // update provided a recommended loader, but it is not
                // known to the mgr. Currently unclear that it is really
                // necessary...
                NA64DP_RUNTIME_ERROR("recommended loader is not owned by mgr");  // TODO
            }
            #endif
            updateEntry->recommended_loader()->load(*updateEntry, *this);
            ++nLoaded;
            continue;
        }
        // no recommended loader -- try to handle with any existing
        bool loaded = false;
        for( auto lp : _loaders ) {
            if( ! lp.second->can_handle(*updateEntry) ) continue;
            lp.second->load(*updateEntry, *this);
            loaded = true;
            ++nLoaded;
            break;
        }
        if( ! loaded ) {
            // means that this update was enqueued with no recommended
            // loader and none of the enabled loaders were capable to
            // load this update
            NA64DP_RUNTIME_ERROR("can't load an update");  // TODO details
        }
    }
    _log.debug("%zu calibration updates have been applied.", nLoaded );
}

void
Manager::event_id( EventID newEventID
                 , const std::pair<time_t, uint32_t> & newDateTime
                 ) {
    Updates upds;
    // collect updates for the event
    for( auto p : _indeces ) {
        // msg_debug( "Loading updates of type ...", p.first.name() )
        p->append_updates( _cEventID, newEventID
                         , _cDatetime, newDateTime
                         , upds
                         );
    }
    if( upds.empty() ) {
        _log.debug( "No calibration data updates for event %s -> %s"
                  , _cEventID.to_str().c_str()  // datetime?
                  , newEventID.to_str().c_str()
                  );
    } else {
        std::string msg;
        #if 0  // TODO; temporary disabled as periods aliasing moved to extension lib
        {  // for better verbosity, attempt to get description of runs
           // transition using aliases. Get aliases by time and run number and:
           //   - on failure, just print warning on failure to notify user on
           //     possible problems (yet, that's normal for runs outside
           //     official data taking).
           //   - if succeed, check that alias is the same (otherwise run and
           //     time identification diverged in calibrations) and add
           //     period description to message
            try {
                // get aliases independently for run number and run time, if
                // possible.
                auto aTo1 = Periods::self()[p348reco::RunNo_t{newEventID.run_no()}]
                   , aTo2 = Periods::self()[p348reco::Datetime_t{newDateTime.first}]
                   ;
                if( aTo1.alias != aTo2.alias ) {
                    _log.warn( "Erroneous period aliasing for (current) run"
                               " #%d; by time -- \"%s\", by run number -- \"%s\"."
                             , (int) newEventID.run_no()
                             , aTo2.name.c_str()
                             , aTo1.name.c_str()
                             );
                } else {
                    msg = util::format( "(\"%s\" -- %s)"
                                      , aTo1.name.c_str()
                                      , aTo2.comment.c_str() );
                }
            } catch( na64dp::errors::NoAliasDefined & e ) {
                _log.debug( "Couldn't find alias for run causing calibration"
                            " update: %s (should be ok for non-data taking"
                            " runs).", e.what() );
                msg.clear();
            }
        }
        #endif
        _sort_updates(upds);
        _log.debug( "Loading %zu calibration data updates for event %s -> %s %s."
                  , upds.size()
                  , _cEventID.to_str().c_str()  // datetime?
                  , newEventID.to_str().c_str()
                  , msg.empty() ? "" : msg.c_str()
                  );
        _load_updates(upds);
        _log.info( "%zu calibration data updates have been loaded for event %s -> %s %s."
                  , upds.size()
                  , _cEventID.to_str().c_str()  // datetime?
                  , newEventID.to_str().c_str()
                  , msg.empty() ? "" : msg.c_str()
                  );
    }
    _cEventID = newEventID;
    _cDatetime = newDateTime;

    // let indeces to cleanup temporary objects
    for( auto p : _indeces ) {
        p->clear_tmp_update_objects();
    }
}

void
Manager::_sort_updates( Updates & upds ) {
    if( upds.size() < 2 ) return;  // nothing to sort
    // retrieve types
    //std::unordered_multimap<Dispatcher::CIDataID, iUpdate>
    std::unordered_map< Dispatcher::CIDataID
                      , std::vector<typename Updates::value_type>
                      , util::PairHash> byTypes;
    for(auto upd : upds) {
        auto ir = byTypes.emplace( upd->subject_data_type()
                                 , decltype(byTypes)::mapped_type()
                                 );
        ir.first->second.push_back(upd);
    }

    const auto & depMap = CIDataAliases::self().dependency_map();
    // Updates list not necessarily contains all the dependencies of depndee.
    // They must be loaded beforehead (in previous invokations).
    std::vector<Dispatcher::CIDataID> order;
    {
        std::unordered_set<Dispatcher::CIDataID, util::PairHash> unaccountedTypes;
        std::transform( byTypes.begin(), byTypes.end()
                      , std::inserter(unaccountedTypes, unaccountedTypes.end())
                      , [](const decltype(byTypes)::value_type & e){
                            return e.first;
                        }
                      );
        util::DAG< Dispatcher::CIDataID
                 , util::PairHash
                 , std::equal_to<Dispatcher::CIDataID>
                 , CIDataIDCompare
                 > g;
        for( const auto & tp : byTypes ) {
            const auto & requestedType = tp.first;
            // get all deps for this type
            auto allRequirements = depMap.equal_range(requestedType);
            // add any deps with type existing in current updates list to graph
            for( auto it = allRequirements.first
               ; it != allRequirements.second
               ; ++it ) {
                // required type that may or may not be present in updates list
                const auto & typeRequiredByRequested = it->second;
                if( byTypes.end() != byTypes.find(typeRequiredByRequested) ) {
                    // push node in graph
                    g.add( it->second, requestedType );
                    unaccountedTypes.erase(it->second);
                    unaccountedTypes.erase(requestedType);
                    _log << log4cpp::Priority::DEBUG
                         << "Calibration data type "
                         << util::calib_id_to_str(requestedType)
                         << " depends on "
                         << util::calib_id_to_str(typeRequiredByRequested)
                         << " (added to DAG for tsort)"
                         ;
                }
            }
        }
        #if 0
        for(const auto & tp1 : byTypes) {
            for(const auto & tp2 : byTypes) {
                if(std::equal_to<Dispatcher::CIDataID>()(tp1.first, tp2.first)) {
                    std::cout << util::calib_id_to_str(tp1.first)
                        << " == "
                        << util::calib_id_to_str(tp2.first)
                        << std::endl;
                        ;
                } else {
                    std::cout << util::calib_id_to_str(tp1.first)
                        << " != "
                        << util::calib_id_to_str(tp2.first)
                        << std::endl;
                        ;
                }
            }
        }
        #endif
        if(!unaccountedTypes.empty()) {
            order.insert( order.end()
                        , unaccountedTypes.begin()
                        , unaccountedTypes.end()
                        );
        }
        const auto sorted = g.sorted();
        if(!g.empty()) {
            const auto edges = g.edges();
            _log << log4cpp::Priority::ERROR
                 << "Edges remained after Kahn's algorithm for topological"
                    " sort (DFS): "
                 << util::str_join<decltype(edges)::const_iterator>(edges.begin(), edges.end(), ", "
                         , [](decltype(edges)::const_iterator it) {
                            return "(" + util::calib_id_to_str(it->first)
                                 + "<-"
                                 + util::calib_id_to_str(it->second) + ")";
                                 } )
                 << "; retrieved: "
                 << util::str_join<decltype(order)::const_iterator>(
                         sorted.begin(), sorted.end(), ", "
                         , [](decltype(order)::const_iterator it){
                                return util::calib_id_to_str(*it);
                            })
                 ;
            NA64DP_RUNTIME_ERROR("Cyclic dependency revealed in the calibration"
                    " data.");  // TODO: how to get info on loops in Kahns' algo?
        }
        // concat free ones with sorted
        order.insert(order.end(), sorted.begin(), sorted.end() );
    }
    // Iterate over the sorted types vector to put the updates in a proper
    // order
    Updates r;
    std::ostringstream oss;
    oss << "Calibration data types enqueued for update: ";
    bool isFirst = true;
    for( auto tpe : order ) {
        const auto & upds = byTypes[tpe];
        r.insert(r.end(), upds.begin(), upds.end());
        if(isFirst) isFirst = false; else oss << ", ";
        oss << util::calib_id_to_str(tpe) << "(" << upds.size() << ")";
    }
    _log.info(oss.str());
    assert(r.size() == upds.size());
    upds = r;
}

void
Manager::to_yaml(YAML::Node & rootNode) const {
    {  // state
        YAML::Node cStateNode;
        cStateNode["eventID"] = _cEventID.to_str();
        YAML::Node timeNode;
        {
            std::ostringstream oss;
            oss << _cDatetime.first << "." << _cDatetime.second;
            cStateNode["time"] = oss.str();
        }
        rootNode["currentState"] = cStateNode;
    }
    {  // loaders
        YAML::Node loadersNode;
        size_t nLoader = 0;
        for( auto loaderNamedPair : _loaders ) {
            YAML::Node loaderNode;
            loaderNamedPair.second->to_yaml_as_loader(loaderNode);
            loaderNode["_name"] = loaderNamedPair.first;
            loadersNode[nLoader++] = loaderNode;
        }
        rootNode["loaders"] = loadersNode;
    }
    {  // indices
        YAML::Node idxsNode;
        size_t nIdx = 0;
        for( auto idxPtr : _indeces ) {
            YAML::Node idxNode;
            idxPtr->to_yaml(idxNode);
            idxsNode[nIdx++] = idxNode;
        }
        rootNode["indeces"] = idxsNode;
    }
}

}  // namespace ::na64dp::calib
}  // namespace na64dp

