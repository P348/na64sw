#include "na64calib/manager.hh"

namespace na64dp {

namespace util {
std::string
calib_id_to_str( const std::pair<std::type_index, std::string> & kp
               , bool forceNative
               ) {
    if(!forceNative){
        const auto & m = calib::CIDataAliases::self().name_by_type_id();
        auto it = m.find(kp);
        if( it != m.end() ) {
            return format( "`%s'", it->second.c_str() );
        }
    }
    return format( "{{%s;%s}}", demangle_cpp(kp.first.name()).get()
                            , kp.second.c_str() );
}
}  // namespace ::na64dp::util

namespace errors {

NoCalibrationInfoEntry::NoCalibrationInfoEntry( const std::string & nm
                                              , const std::type_info & ti ) throw() 
    : std::runtime_error(
            util::format( "Calibration info of type %s is not known to"
                " dispatcher (no subscribers?)."
                        , util::calib_id_to_str(calib::Dispatcher::CIDataID(ti, nm)).c_str()
                        ).c_str() )
    , _ti(ti)
    , _name(nm) {}

PrematureDereferencing::PrematureDereferencing( const std::string & nm
                                              , const std::type_info & ti ) throw() 
    : std::runtime_error(
            util::format( "Calibration info %s entry was not loaded."
                         , util::calib_id_to_str(calib::Dispatcher::CIDataID(ti, nm)).c_str()
                         ).c_str() )
        , _ti(ti)
        , _name(nm) {}

}
}

