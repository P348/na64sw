#include "na64calib/dispatcher.hh"
#include <gtest/gtest.h>

namespace na64dp {
namespace calib {

class ClientInt : public na64dp::util::Observable<int>::iObserver {
public:
    int value;
    ClientInt() : value(0) {}
    virtual void handle_update(const int & v) override {
        value = v;
    }
};

class ClientFloat : public na64dp::util::Observable<float>::iObserver {
public:
    float value;
    ClientFloat() : value(0.) {}
    virtual void handle_update(const float & v) override {
        value = v;
    }
};

class CalibDispatcher : public ::testing::Test {
public:
    Dispatcher dispatcher;
    CalibDispatcher() : dispatcher(log4cpp::Category::getInstance("calib.manager")) {}
};

TEST_F( CalibDispatcher, ObservableCreatedOnSubscription ) {
    auto key = Dispatcher::info_id<int>("subscription-test");
    ASSERT_FALSE( dispatcher.observable_exists( key ) );

    ClientInt ci1, ci2;
    dispatcher.subscribe<int>(ci1, "subscription-test");
    ASSERT_TRUE( dispatcher.observable_exists( key ) );
    dispatcher.subscribe<int>(ci2, "subscription-test");
    ASSERT_TRUE( dispatcher.observable_exists( key ) );

    dispatcher.unsubscribe<int>(ci2, "subscription-test");
    ASSERT_TRUE( dispatcher.observable_exists( key ) );
    dispatcher.unsubscribe<int>(ci1, "subscription-test");
    ASSERT_FALSE( dispatcher.observable_exists( key ) );
}

TEST_F( CalibDispatcher, ObservablesNotified ) {
    ClientInt ci1, ci2;
    ClientFloat cf1, cf2;

    EXPECT_THROW( dispatcher.set<int>("notification-test-one", 42)
                , na64dp::errors::NoCalibrationInfoEntry );

    EXPECT_TRUE(  dispatcher.subscribe<int>(ci1, "notification-test-one") );
    EXPECT_FALSE( dispatcher.subscribe<int>(ci1, "notification-test-one") );
    EXPECT_TRUE(  dispatcher.subscribe<int>(ci2, "notification-test-one") );
    EXPECT_TRUE(  dispatcher.subscribe<float>(cf1, "notification-test-two") );
    EXPECT_TRUE(  dispatcher.subscribe<float>(cf2, "notification-test-three") );

    dispatcher.set<int>("notification-test-one", 42);
    EXPECT_EQ( ci1.value, 42 );
    EXPECT_EQ( ci2.value, 42 );
    EXPECT_EQ( cf1.value, 0. );
    EXPECT_EQ( cf2.value, 0. );

    dispatcher.set<float>("notification-test-three", 12.34);
    EXPECT_EQ( ci1.value, 42 );
    EXPECT_EQ( ci2.value, 42 );
    EXPECT_EQ( cf1.value, 0. );
    EXPECT_NEAR( cf2.value, 12.34, 1e-2 );

    dispatcher.unsubscribe<int>(  ci1, "notification-test-one" );
    dispatcher.unsubscribe<int>(  ci2, "notification-test-one" );
    dispatcher.unsubscribe<float>( cf1, "notification-test-two" );
    dispatcher.unsubscribe<float>( cf2, "notification-test-three" );
}

TEST_F( CalibDispatcher, Handle ) {
    struct MockData { int a; float b; };
    {
        Handle<MockData> varHandle( "handle-test", dispatcher );
        EXPECT_TRUE( dispatcher.observable_exists(Dispatcher::info_id<MockData>("handle-test")) );
        EXPECT_THROW( varHandle->a
                    , errors::PrematureDereferencing );
        dispatcher.set<MockData>( "handle-test", MockData{43, 34.12} );

        EXPECT_EQ( varHandle->a, 43  );
        EXPECT_NEAR( varHandle->b, 34.12, 1e-2 );
    }
    EXPECT_FALSE( dispatcher.observable_exists(Dispatcher::info_id<MockData>("handle-test")) );
}

}
}

