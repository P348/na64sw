#include "na64calib/setupGeoCache.hh"

namespace na64dp {
namespace calib {

SetupGeometryCache::SetupGeometryCache( calib::Dispatcher & cdsp
                                      , log4cpp::Category & logCat
                                      , const std::string & namingCalibClass
                                      , const std::string & placementsCalibClass
                                      )
            : calib::Handle<nameutils::DetectorNaming>(namingCalibClass, cdsp)
            , _log(logCat)
            {
    cdsp.subscribe<calib::Placements>(*this, placementsCalibClass);
}

void
SetupGeometryCache::handle_update( const calib::Placements & placements ) {
    for( const auto & pl : placements ) {
        handle_single_placement_update(pl);
    }
}

}  // namespace ::na64dp::calib
}  // namespace na64dp
