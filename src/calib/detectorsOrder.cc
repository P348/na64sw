#include "na64calib/detectorsOrder.hh"

namespace na64dp {
namespace calib {

struct StationEntry {
    std::unordered_set<DetID> layerIDs;
    float avrgVal;
    StationEntry() : avrgVal(0.) {}
};

std::map<DetID, unsigned int>
OrderedStations::_sort_detectors( const std::unordered_map<DetID, Placement> & byIDs
                                , size_t & nSortedGroups
                                ) const {
    // collect station keys
    std::unordered_map<StationKey, StationEntry > detIDsByStationKeys;
    for(auto & entry : byIDs) {
        // check if matches selection/zones criteria
        bool matches = true;
        for(auto selPair : _selectorPtrs) {
            if(!selPair->second) {
                NA64DP_RUNTIME_ERROR("Unresolved selector \"%s\"", selPair->first.c_str());
            }
            if(selPair->second->matches(entry.first)) continue;
            _log << log4cpp::Priority::DEBUG
                 << "Placement item \"" << naming()[entry.first] << "\" declined from"
                " detectors order " << (void*) this << " by selector \"" << selPair->first.c_str()
                << "\"";
            matches = false;
            break;
        }
        if(!matches) continue;
        if((!_permittedZones.empty()) && _permittedZones.find(entry.second.zone) == _permittedZones.end()) {
            _log << log4cpp::Priority::DEBUG
                 << "Placement item \"" << naming()[entry.first] << "\" declined from"
                " detectors order " << (void*) this << " by zone mismatch";
            continue;
        }
        // Add station key
        auto ir = detIDsByStationKeys.emplace( StationKey(entry.first), StationEntry() );
        if(!ir.second) {
            ir.first->second.avrgVal += _callback(entry.first, entry.second);
        } else {
            ir.first->second.avrgVal = _callback(entry.first, entry.second);
        }
        ir.first->second.layerIDs.insert(entry.first);
    }
    // find averaged value per station
    for(auto & p : detIDsByStationKeys) {
        assert(!p.second.layerIDs.empty());
        p.second.avrgVal /= p.second.layerIDs.size();
    }
    // build reverse map for stations only
    std::map<float, StationKey> skByVal;
    std::transform( detIDsByStationKeys.begin(), detIDsByStationKeys.end()
                  , std::inserter(skByVal, skByVal.end())
                  , [](const auto & p){
                        return std::pair<float, StationKey>(p.second.avrgVal, p.first);}
                  );
    // build enumerated list
    unsigned int n = 0;
    std::ostringstream oss;
    std::map<DetID, unsigned int> r;
    for( const auto & stationEntry : skByVal ) {
        oss << "#" << n << ", station \"" << naming()[stationEntry.second] << "\" (avrg="
            << stationEntry.first << "):";
        for( const auto & layerID : detIDsByStationKeys.find(stationEntry.second)->second.layerIDs ) {
            r.emplace(layerID, n);
            oss << "\"" << naming()[layerID] << "\" ";
        }
        r.emplace(stationEntry.second, n);  // add station id itself too
        ++n;
    }
    _log << log4cpp::Priority::DEBUG << "Station order recached: " << oss.str();
    nSortedGroups = skByVal.size();
    return r;
}

float OrderedStations::z_center(DetID, const Placement & pl) {
    return pl.center[2];
}

}  // namespace ::na64dp::calib
}  // namespace na64dp

