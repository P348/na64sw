#include "na64calib/evType.hh"
#include "na64event/data/event.hh"
#include "na64util/str-fmt.hh"
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <sstream>

namespace na64dp {

std::string
EventBitTags::EventSemanticBits::bitflags_to_str(uint16_t flags) const {
    if(!flags) return "0x0";
    // create sorted dict
    std::map<std::string, uint16_t> sortedDict(begin(), end());
    std::ostringstream oss;
    bool isFirst = true;
    for(const auto & e : sortedDict) {
        if(!(e.second & flags)) continue;
        if(isFirst) {
            isFirst = false;
        } else {
            oss << ", ";
        }
        oss << e.first;
        flags &= ~e.second;
    }
    if(!flags) return oss.str();
    // otherwise, some unknown numeric codes remained; append it in hex form:
    char strbf[32];
    snprintf(strbf, sizeof(strbf), "%#x", flags);
    if(!isFirst) oss << ", ";
    oss << strbf;
    return oss.str();
}

EventBitTags::EventBitTags(const YAML::Node & cfg) {
    if(cfg.IsNull())
        return;

    if(cfg["triggerBits"]) {
        const YAML::Node & ttNode = cfg["triggerBits"];
        for(auto nodeIt = ttNode.begin(); nodeIt != ttNode.end(); ++nodeIt) {
            std::string bitMeaning = nodeIt->first.as<std::string>();
            unsigned int bitPosition = nodeIt->second.as<unsigned int>();
            if(bitPosition > 8*sizeof(na64dp::event::Event::trigger)) {
                NA64DP_RUNTIME_ERROR("Can not set %u-th bit of Event::trigger"
                        " field (too large value).", bitPosition);
            }
            triggers.emplace(bitMeaning, 0x1 << bitPosition);
        }
    } else {
        log4cpp::Category::getInstance("calib") << log4cpp::Priority::WARN
            << "No \"triggerBits\" in event semantical bits object.";
    }

    if(cfg["eventTypes"]) {
        const YAML::Node & etNode = cfg["eventTypes"];
        for(auto nodeIt = etNode.begin(); nodeIt != etNode.end(); ++nodeIt) {
            std::string bitMeaning = nodeIt->first.as<std::string>();
            unsigned int bitPosition = nodeIt->second.as<unsigned int>();
            if(bitPosition > 8*sizeof(na64dp::event::Event::evType)) {
                NA64DP_RUNTIME_ERROR("Can not set %u-th bit of Event::evType"
                        " field (too large value).", bitPosition);
            }
            types.emplace(bitMeaning, 0x1 << bitPosition);
        }
    }  else {
        log4cpp::Category::getInstance("calib") << log4cpp::Priority::WARN
            << "No \"eventTypes\" in event semantical bits object.";
    }
}

}

