#include "na64calib/index.hh"

namespace na64dp {
namespace calib {
namespace aux {

template<> void update_payload_to_yaml(const YAML::Node & pl, YAML::Node & node) {
    // copy
    node["payload"] = YAML::Node(pl);
}
}  // namespace ::na64dp::calib::aux
}  // namespace ::na64dp::calib
}  // namespace na64dp

