#include "na64calib/mkROOTGeo.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include "na64util/str-fmt.hh"

#include <TGeoVolume.h>
#include <TGeoMatrix.h>
#include <TGeoManager.h>
#include <TGeoMaterial.h>

namespace na64dp {

PlacementsBasedGeometry::PlacementsBasedGeometry(
          TGeoManager * gmgr
        , log4cpp::Category & L_
        ) : _geoManager(gmgr)
          , _L(L_)
          {
    _instantiate_materials();  // TODO: other source
}

PlacementsBasedGeometry::~PlacementsBasedGeometry() {
}

void
PlacementsBasedGeometry::_instantiate_materials() {
    // TODO: this hardcoded definitions will be most likely deleted in favour
    //       of more elaborated materials management API. Copied from p348reco
    //_matMixtures
    unsigned int mediumIndex = 0;
    TGeoMaterial *_siliconMat = new TGeoMaterial( "siliconMat"
                                                , 28.0855
                                                , 14.
                                                , 2.329 );
    _siliconMat->SetRadLen(1.);//calc automatically, need this for elemental mats.
    _matMixtures.emplace( "silicon"
            , new TGeoMedium( "silicon", mediumIndex++, _siliconMat, 0) );

    TGeoMixture *_airMat = new TGeoMixture("airMat",3);
    _airMat->AddElement(14.01,7.,.78);
    _airMat->AddElement(16.00,8.,.21);
    _airMat->AddElement(39.95,18.,.01);
    _airMat->SetDensity(1.2e-3);
    _matMixtures.emplace( "air"
            , new TGeoMedium("air", mediumIndex++, _airMat, 0) );

    TGeoMixture *_vacuumMat = new TGeoMixture("vacuumMat",3);
    _vacuumMat->AddElement(14.01,7.,.78);
    _vacuumMat->AddElement(16.00,8.,.21);
    _vacuumMat->AddElement(39.95,18.,.01);
    _vacuumMat->SetDensity(1.2e-15);
    _matMixtures.emplace( "vacuum"
            , new TGeoMedium("vacuum", mediumIndex++, _vacuumMat, 0) );
}

TGeoMatrix *
PlacementsBasedGeometry::_instantiate_ROOT_placement(const calib::Placement & pl) {
    TGeoTranslation tr( pl.center[0], pl.center[1], pl.center[2] );
    TGeoRotation rot( util::format("%s-rotation", pl.name.c_str()).c_str()
                    , pl.rot[0], pl.rot[1], pl.rot[2] );
    // TODO: ROOT does not copy this object, but it is unclear whether it
    // deletes it (memleak)
    return new TGeoCombiTrans(tr, rot);
}

TGeoVolume *
PlacementsBasedGeometry::_instantiate_entity( const calib::Placement & pl
                                            , TGeoVolume * parent ) {
    TGeoVolume * volPtr = nullptr;
    TGeoMedium * mediumPtr;
    switch(pl.suppInfoType) {
        case calib::Placement::kRegularWiredPlane :
        case calib::Placement::kIrregularWiredPlane :
        case calib::Placement::kVolumetricDetector :
        {
            volPtr = _geoManager->MakeBox( pl.name.c_str()
                                         , mediumPtr = get_medium(pl.material)
                                         , pl.size[0]/2
                                         , pl.size[1]/2
                                         , pl.size[2]/2
                                         );
            // ^^^ TODO: rotation?
            _L << log4cpp::Priority::DEBUG
               << "Created ROOT geometry box item \"" << pl.name << "\" of sizes "
               << pl.size[0]
               << "x" << pl.size[1]
               << "x" << pl.size[2]
               << " at " << pl.center[0]
               << "x" << pl.center[1]
               << "x" << pl.center[2]
               << " filled with material \"" << mediumPtr->GetMaterial()->GetName()
               << "\""
               ;
        } break;
        // ...
        default:
            _L << log4cpp::Priority::WARN
               << "No ROOT geometry set for placement item \""
               << pl.name.c_str() << "\".";
            return nullptr;
    };
    if( (!mediumPtr) && !pl.material.empty() ) {
        NA64DP_RUNTIME_ERROR("Unknown material or medium \"%s\" (for entity \"%s\")."
                , pl.material.c_str(), pl.name.c_str() );
    }

    //volPtr->SetTransparency(15);
    //volPtr->SetLineColor(kBlue);  
    //^^^ TODO: parameterise?

    parent->AddNode(volPtr, 1, _instantiate_ROOT_placement(pl));
    assert(volPtr);
    return volPtr;
}

void
PlacementsBasedGeometry::add_placement(const calib::Placement & pl ) {
    _nativePlacements.push_back(pl);
}

std::pair<TVector3, TVector3>
PlacementsBasedGeometry::world_boundaries() const {
    // init dft values to nan
    float vs[2][3];
    for( int i = 0; i < 2; ++i ) {
        for( int j = 0; j < 3; ++j ) {
            vs[i][j] = std::nan("0");
        }
    }
    // iterate over all placements available
    for(const calib::Placement & pl : _nativePlacements) {
        // calculate radius of the sphere encompassing object (distance to
        // most distant possible point, ignoring actual rotation)
        float sz = sqrt( (std::isnan(pl.size[0]) ? 0. : pl.size[0]*pl.size[0])
                       + (std::isnan(pl.size[1]) ? 0. : pl.size[1]*pl.size[1])
                       + (std::isnan(pl.size[2]) ? 0. : pl.size[2]*pl.size[2])
                       );
        // compare min/max estimation for each dimension
        for( int j = 0; j < 3; ++j ) {
            const float mn = pl.center[j] - sz
                      , mx = pl.center[j] + sz
                      ;
            if( std::isnan(vs[0][j]) || vs[0][j] > mn ) vs[0][j] = mn;
            if( std::isnan(vs[1][j]) || vs[1][j] < mx ) vs[1][j] = mx;
        }
    }
    return std::pair<TVector3, TVector3>(
            TVector3( vs[0][0], vs[0][1], vs[0][2] ),
            TVector3( vs[1][0], vs[1][1], vs[1][2] ));
}

void
PlacementsBasedGeometry::instantiate_geometry( TGeoVolume * topVol ) {
    for (const calib::Placement & pl : _nativePlacements) {
        /*TGeoVolume * newVol = */ _instantiate_entity(pl, topVol);
    }
    _nativePlacements.clear();
}

TGeoMedium *
PlacementsBasedGeometry::get_medium(const std::string & nm) {
    auto it = _matMixtures.find(nm);
    if( _matMixtures.end() == it ) return nullptr;
    return it->second;
}

}
#endif  // defined(ROOT_FOUND) && ROOT_FOUND
