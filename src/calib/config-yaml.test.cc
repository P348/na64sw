#if 0

#include "na64calib/config-yaml.hh"
#include "na64calib/range-override-run-index.hh"
#include <gtest/gtest.h>

namespace na64dp {
namespace calib {

template<typename T>
class MockDataIndex : public iYAMLIndex
                    , public std::map<na64sw_runNo_t, T> {
public:
    virtual void append( EventID eid
                       , const YAML::Node & node ) {
        auto ir = this->emplace( eid.run_no(), node["value"].as<T>() );
        assert(ir.second);
    }

    virtual void put( EventID eid
                    , GenericLoader::DispatcherProxy & proxy ) {
        auto it = this->find( eid.run_no() );
        assert( this->end() != it );
        proxy = it->second;
    }

    virtual bool has( EventID eid ) {
        return this->find(eid.run_no()) != this->end();
    }
};

iYAMLIndex *
_construct_type1() {
    return new MockDataIndex<int>();
}


TEST( YAMLCalibConfig, MissedTypeIgnored ) {
    YAML::Node root(YAML::Load(R"exYAML(
type1:
    - validFromRun: 1
      value: 20
    - validFromRun: 2
      value: 30
type2:
    - validFromRun: 2
      value: some
    - validFromRun: 3
      value: another
)exYAML"));
    RangeOverrideRunIndex fsi;
    GenericLoader loader;
    std::unordered_map<std::string, YAMLCalibInfoCtr> ctrs;
    MockDataIndex<std::string> indexStr;
    ctrs.emplace( "type1", YAMLCalibInfoCtr{ Dispatcher::info_id<int>("intOne")
                                           , (iYAMLIndex *) nullptr
                                           , _construct_type1
                                           } );
    configure_from_YAML( root, &fsi
                       , &loader, "defaultLoader"
                       , ctrs );
    // Check configuration is correct
    // - type #1
    // -- has no "int:intOne" for run #0
    ASSERT_FALSE( loader.has( EventID(0, 0, 0)
                            , Dispatcher::info_id<int>("intOne") ) );
    // -- has "int:intOne" for run #1
    ASSERT_TRUE(  loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // -- has "int:intOne" for run #2
    ASSERT_TRUE(  loader.has( EventID(2, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // -- has no "int:intOne" for run #2
    ASSERT_FALSE( loader.has( EventID(3, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // - type #2
    // -- has no "string:stringOne" for run #1
    ASSERT_FALSE( loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<std::string>("stringOne") ) );
    // --has no "string:stringOne" for run #2
    ASSERT_FALSE( loader.has( EventID(2, 0, 0)
                           , Dispatcher::info_id<std::string>("stringOne") ) );
    // --has no "string:stringOne" for run #3
    ASSERT_FALSE( loader.has( EventID(3, 0, 0)
                           , Dispatcher::info_id<std::string>("stringOne") ) );
}


class YAMLCalibConfigTest : public ::testing::Test {
public:
    YAML::Node root;
    // Run index
    RangeOverrideRunIndex fsi;
    // Loader
    GenericLoader loader;
    // Calib info index maps
    std::unordered_map<std::string, YAMLCalibInfoCtr> ctrs;
    // Externally constructed data index instance
    MockDataIndex<std::string> indexStr;

    YAMLCalibConfigTest() : root(YAML::Load(R"exYAML(
type1:
    - validFromRun: 1
      value: 20
    - validFromRun: 2
      value: 30
type2:
    - validFromRun: 2
      value: some
    - validFromRun: 3
      value: another
)exYAML")) {
        // Fill the info index construction map
        ctrs.emplace( "type1", YAMLCalibInfoCtr{ Dispatcher::info_id<int>("intOne")
                                               , (iYAMLIndex *) nullptr
                                               , _construct_type1
                                               } );
        ctrs.emplace("type2", YAMLCalibInfoCtr{ Dispatcher::info_id<std::string>("stringOne")
                                              , &indexStr
                                              , (iYAMLIndex * (*)()) nullptr
                                              } );
        // ^^^ important note: since we have provided data index instance
        // instead of its constructor, the `configure_from_YAML()` function
        // will not automatically add this run index into `FilesLoader`. This
        // is expected behaviour and one shall add separately.
        loader.add_data_index( Dispatcher::info_id<std::string>("stringOne")
                             , &indexStr );

        // Configure from YAML
        configure_from_YAML( root, &fsi
                           , &loader, "defaultLoader"
                           , ctrs );
    }

    ~YAMLCalibConfigTest() {
        // delete dynamically-allocated data index for "type1"
        auto it = ctrs.find("type1");
        if( ctrs.end() != it && it->second.dataIndexPtr ) {
            delete it->second.dataIndexPtr;
        }
    }
};

TEST_F( YAMLCalibConfigTest, CorrectTypesBinding ) {
    // Check configuration is correct
    // - type #1
    // -- has no "int:intOne" for run #0
    ASSERT_FALSE( loader.has( EventID(0, 0, 0)
                            , Dispatcher::info_id<int>("intOne") ) );
    // -- has "int:intOne" for run #1
    ASSERT_TRUE(  loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // -- has "int:intOne" for run #2
    ASSERT_TRUE(  loader.has( EventID(2, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // -- has no "int:intOne" for run #2
    ASSERT_FALSE( loader.has( EventID(3, 0, 0)
                           , Dispatcher::info_id<int>("intOne") ) );
    // -- has no "float:intOne" for run #1
    ASSERT_FALSE( loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<float>("intOne") ) );
    // -- has no "float:intOne" for run #1
    ASSERT_FALSE( loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<int>("intTwo") ) );
    // - type #2
    // -- has no "string:stringOne" for run #1
    ASSERT_FALSE( loader.has( EventID(1, 0, 0)
                           , Dispatcher::info_id<std::string>("stringOne") ) );
    // -- has "string:stringOne" for run #4
    ASSERT_FALSE( loader.has( EventID(4, 0, 0)
                           , Dispatcher::info_id<std::string>("stringOne") ) );
}

TEST_F( YAMLCalibConfigTest, UpdatePropagated ) {
    Manager mgr;

    Handle<int> intHandle("intOne", mgr);
    Handle<std::string> stringHandle("stringOne", mgr);

    mgr.add_run_index( &fsi );
    mgr.add_loader( "defaultLoader", &loader );

    ASSERT_THROW( *intHandle
                , errors::PrematureDereferencing );
    ASSERT_THROW( stringHandle->empty()
                , errors::PrematureDereferencing );

    mgr.event_id( EventID(0, 100, 200) );
    // no updates delivered so far
    EXPECT_THROW( *intHandle
                , errors::PrematureDereferencing );
    EXPECT_THROW( stringHandle->empty()
                , errors::PrematureDereferencing );

    mgr.event_id( EventID(1, 0, 0) );
    // int value has been updated
    EXPECT_EQ( *intHandle, 20 );
    EXPECT_THROW( stringHandle->empty()
                , errors::PrematureDereferencing );

    mgr.event_id( EventID(2, 0, 0) );
    // int value has been updated to second value, string value set to first
    EXPECT_EQ( *intHandle, 30 );
    EXPECT_STREQ( stringHandle->c_str()
                , "some" );

    mgr.event_id( EventID(3, 0, 0) );
    // int value remains, string value set to second
    EXPECT_EQ( *intHandle, 30 );
    EXPECT_STREQ( stringHandle->c_str()
                , "another" );
}

}
}

#endif
