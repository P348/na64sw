#include <gtest/gtest.h>
#include "na64calib/indices/range-override.hh"

namespace na64dp {
namespace calib {

// Tests validity ranges lookup
TEST(FileIndex, RunsValidityRanges) {
    //  |---a---|---b--|-----c-----|----d-----|--e--...
    //  ^1      ^10    ^33         ^78        ^99
    na64dp::calib::RangeIndex<int, char> i;
    i.insert({ 1, 'a' });
    i.insert({10, 'b' });
    i.insert({33, 'c' });
    i.insert({78, 'd' });
    i.insert({99, 'e' });
    // test middle cases
    EXPECT_EQ( i.get_valid_entry_for(  5)->second, 'a' );
    EXPECT_EQ( i.get_valid_entry_for( 20)->second, 'b' );
    EXPECT_EQ( i.get_valid_entry_for( 40)->second, 'c' );
    EXPECT_EQ( i.get_valid_entry_for( 80)->second, 'd' );
    EXPECT_EQ( i.get_valid_entry_for(110)->second, 'e' );
    // test boundary cases
    EXPECT_EQ( i.get_valid_entry_for(  1)->second, 'a' );
    EXPECT_EQ( i.get_valid_entry_for( 10)->second, 'b' );
    EXPECT_EQ( i.get_valid_entry_for( 33)->second, 'c' );
    EXPECT_EQ( i.get_valid_entry_for( 78)->second, 'd' );
    EXPECT_EQ( i.get_valid_entry_for( 99)->second, 'e' );
    // test error (no `valid from' entry)
    ASSERT_EQ( i.get_valid_entry_for(0), i.end() );
}

// TODO: rewrite
#if 0
class TestRangeOverrideRunIndex : public ::testing::Test {
public:
    RangeOverrideRunIndex fi;
    TestRangeOverrideRunIndex() {
        fi.add_entry( EventID(5, 0, 12)
                    , "foo-int"
                    , RangeOverrideRunIndex::UpdateRecipe{ "loader1" } );
        fi.add_entry( EventID(10, 0, 0)
                    , "foo-int"
                    , iIndex::UpdateRecipe{ "loader2" } );
    }
};

TEST_F(TestRangeOverrideRunIndex, indexReturnsCalibOnCalib) {
    iIndex::Updates ul;
    fi.updates( EventID( 5, 0, 11), EventID( 5, 0, 12)
              , {0,0}, {0,0}
              , ul );
    ASSERT_EQ( 1, ul.size() );
    auto update = *ul.begin();
    EXPECT_EQ( update.first, "foo-int" );
    ASSERT_EQ( 1, update.second.size() );
    EXPECT_STREQ( update.second.begin()->loaderName.c_str(), "loader1" );
    //const EventID calibEID(5, 0, 12);
    //EXPECT_EQ( update.second.begin()->eventID, calibEID );
}
#endif

}
}

