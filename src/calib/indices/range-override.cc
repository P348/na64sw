#include "na64calib/indices/range-override.hh"

#include <ctime>

namespace na64dp {
namespace calib {
namespace aux {

template<> std::string
key_to_str_for_yaml(std::pair<time_t, uint32_t> timePair) {
    //std::ostringstream oss;
    //oss << timePair.first << "." << timePair.second;
    //return oss.str();
    char bf[128];
    struct tm tmVal;
    gmtime_r( &timePair.first, &tmVal );
    //char timeBf[26];  // len defined by asctime_r() spec
    //std::string strTime(asctime_r(&tm_val, timeBf));
    //strTime.pop_back();
    char tsBuf[64];
    strftime(tsBuf, sizeof(tsBuf), "%F-%T", &tmVal);
    snprintf(bf, sizeof(bf), "%s.%u", tsBuf, timePair.second);
    return bf;
}

template<> std::string
key_to_str_for_yaml(EventID eid) {
    return eid.to_str();
}

template<> std::pair<EventID, EventID>
deduce_key( EventID oldEventID, EventID newEventID
          , const std::pair<time_t, uint32_t> & oldDatetime
          , const std::pair<time_t, uint32_t> & newDateTime
          ) { return {oldEventID, newEventID}; }

template<> std::pair<std::pair<time_t, uint32_t>, std::pair<time_t, uint32_t>>
deduce_key( EventID oldEventID, EventID newEventID
          , const std::pair<time_t, uint32_t> & oldDatetime
          , const std::pair<time_t, uint32_t> & newDateTime
          ) { return {oldDatetime, newDateTime}; }

}  // namespace ::na64dp::calib::aux
}  // namespace ::na64dp::calib
}  // namespace na64dp

