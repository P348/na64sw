#include "na64calib/indices/yaml-index.hh"
#include "na64util/str-fmt.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace calib {

void
YAMLIndex::add_records( const YAML::Node & entries
                      , const std::unordered_map<std::string, Dispatcher::CIDataID> & aliases
                      , const std::unordered_map<std::string, std::shared_ptr<iLoader>> & loadersByName
                      ) {
    auto & L = log4cpp::Category::getInstance("calibrations.yamlDoc");
    if( ! entries.IsSequence() ) {
        NA64DP_RUNTIME_ERROR( "YAML entity provided to YAMLIndex ctr"
                " is not a sequence of objects." );
    }
    size_t nRecord = 0;
    for( YAML::const_iterator it = entries.begin()
       ; entries.end() != it
       ; ++it, ++nRecord ) {
        if( ! it->IsMap() ) {
            NA64DP_RUNTIME_ERROR( "YAML node provided as data record #%zu"
                " is not a map (object).", nRecord );
        }
        if( ! (*it)["type"] ) {
            NA64DP_RUNTIME_ERROR( "YAML node provided as data record #%zu"
                " has no \"type\" key.", nRecord );
        }
        // Get type
        const std::string aliasTypeName = (*it)["type"].as<std::string>();
        auto aliasIt = aliases.find(aliasTypeName);
        if( aliases.end() == aliasIt ) {
            L.warn("YAML calibration index has record #%zu defined for"
                   " an unknown aliased type \"%s\". Record omitted."
                   , nRecord, aliasTypeName.c_str() );
            continue;
        }
        const Dispatcher::CIDataID typeID(aliasIt->second);
        // Get loader
        std::shared_ptr<iLoader> loaderPtr;
        if( (*it)["loader"] ) {
            auto loaderIt = loadersByName.find((*it)["loader"].as<std::string>());
            if(loadersByName.end() == loaderIt) {
                // Not a warning -- dynamic loader lookup will be involved,
                // probably resulting insignificant performance drop.
                L.debug("YAML calibration index has record #%zu defined for"
                   " an aliased type \"%s\" refers to unknown loader \"%s\"."
                   , nRecord
                   , aliasTypeName.c_str()
                   , (*it)["loader"].as<std::string>().c_str()
                   );
            } else {
                loaderPtr = loaderIt->second;
            }
        }
        // Require that validity is set either by runs or by date and time
        if(((bool) (*it)["validForRuns"]) == ((bool) (*it)["validForDatetime"])) {
            NA64DP_RUNTIME_ERROR( "Validity range"
                    " for the record #%zu (type \"%s\") is not properly set."
                    " Record have one of \"validForRuns\" / \"validForDatetime\""
                    " attributes.", nRecord, aliasTypeName.c_str() );
        }
        // Add to the validity map
        if( (*it)["validForRuns"] ) {
            const YAML::Node & runsValidity = (*it)["validForRuns"];

            if(!(runsValidity.IsSequence() && 2 == runsValidity.size()) ) {
                NA64DP_RUNTIME_ERROR( "Validity range (\"validForRuns\")"
                    " for the record #%zu is not a list of length 2."
                    , nRecord );
            }

            // copy YAML, remove the `validFromRun` and `loader` fields
            YAML::Node nodeCpy(*it);
            nodeCpy.remove("loader");
            nodeCpy.remove("validForRuns");

            EventID from( (!runsValidity[0].IsNull())
                        ? runsValidity[0].as<na64sw_runNo_t>()
                        : 1
                        , 0, 0 )
                  , to((runsValidity[1] && !runsValidity[1].IsNull())
                      ? runsValidity[1].as<na64sw_runNo_t>()
                      : 0  // (unset)
                      )
                  ;

            L << log4cpp::Priority::DEBUG
              << "Adding entry of " << aliasTypeName
              << " with run number validity "
              << from.to_str()
              << " - "
              << to.to_str()
              << ") from YAML index.";

            RangeOverrideRunIndex<EventID, YAML::Node>
                ::add_entry( from
                     , aliasTypeName
                     , RangeOverrideRunIndex<EventID, YAML::Node>::UpdateRecipe(
                         typeID
                       , to
                       , loaderPtr
                       , nodeCpy
                       )
                     );
        } else if( (*it)["validForDatetime"] ) {
            const YAML::Node & runsValidity = (*it)["validForDatetime"];
            if( !(runsValidity.IsSequence() && 2 == runsValidity.size()) ) {
                NA64DP_RUNTIME_ERROR( "Validity range (\"validForDatetime\")"
                    " for the record #%zu is not a list of length 2."
                    , nRecord );
            }

            std::pair<time_t, uint32_t> from{ util::parse_timedate_strexpr(
                    (*it)["validForDatetime"][0].as<std::string>().c_str()), 0 };
            if( !from.first ) {
                NA64DP_RUNTIME_ERROR( "Failed to parse validity range"
                    " `from'-token (first element of \"validForDatetime\""
                    " attribute) \"%s\" for the record #%zu."
                    , (*it)["validForDatetime"][0].as<std::string>().c_str()
                    , nRecord
                    );
            }
            std::pair<time_t, uint32_t> to{0, 0};
            if((*it)["validForDatetime"][1]) {
                to.first = util::parse_timedate_strexpr(
                    (*it)["validForDatetime"][1].as<std::string>().c_str());
                if( !from.first ) {
                    NA64DP_RUNTIME_ERROR( "Failed to parse validity range"
                        " to-token (first element of \"validForDatetime\""
                        " attribute) \"%s\" for the record #%zu."
                        , (*it)["validForDatetime"][1].as<std::string>().c_str()
                        , nRecord );
                }
            }

            // copy YAML, remove the `validFromRun` and `loader` fields
            YAML::Node nodeCpy(*it);
            nodeCpy.remove("loader");
            nodeCpy.remove("validForDatetime");

            L << log4cpp::Priority::DEBUG
              << "Adding entry of " << aliasTypeName
              << " with datetime validity "
              << util::str_format_timedate(from.first)
              << " ("
              << aux::key_to_str_for_yaml( from )
              << ") - "
              << util::str_format_timedate(to.first)
              << " ("
              << aux::key_to_str_for_yaml( to )
              << ") from YAML index.";
            RangeOverrideRunIndex<std::pair<time_t, uint32_t>, YAML::Node>
                ::add_entry( from
                     , aliasTypeName
                     , RangeOverrideRunIndex<std::pair<time_t, uint32_t>, YAML::Node>::UpdateRecipe(
                            typeID
                          , to, loaderPtr, nodeCpy )
                     );
        }
    }
}

void
YAMLIndex::append_updates( EventID oldEventID, EventID newEventID
                         , const std::pair<time_t, uint32_t> & oldDatetime
                         , const std::pair<time_t, uint32_t> & newDateTime
                         , Updates & upds ) {
    RangeOverrideRunIndex<EventID, YAML::Node>::append_updates(
            oldEventID, newEventID, oldDatetime, newDateTime, upds );
    RangeOverrideRunIndex<std::pair<time_t, uint32_t>, YAML::Node>::append_updates(
            oldEventID, newEventID, oldDatetime, newDateTime, upds );
}

void
YAMLIndex::to_yaml(YAML::Node & outNode) const {
    char bf[64];
    YAML::Node byEvent, byTime;
    RangeOverrideRunIndex<EventID, YAML::Node>::to_yaml(byEvent);
    RangeOverrideRunIndex<std::pair<time_t, uint32_t>,  YAML::Node>::to_yaml(byTime);
    outNode["byEvent"] = byEvent;
    outNode["byTime"] = byTime;
    outNode["_type"] = "YAMLIndex";
    snprintf(bf, sizeof(bf), "%p", this);
    outNode["_ptr"] = bf;
}

}
}

