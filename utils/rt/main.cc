#include "reconstruction/stwtdc/rt.hh"

#include <TFile.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TGraph.h>

static void
_print_usage_info(std::ostream & os, const std::string & appName) {
    os << "Usage" << std::endl
       << "    $ " << appName << " <processed.root> <calib-ASCII-file>" << std::endl;
}

int
main(int argc, char * argv[]) {
    if( argc != 3 ) {
        _print_usage_info(std::cerr, argv[0]);
        return 1;
    }
    TApplication * rootApp= new TApplication("app",0,0);
    // disable silly ROOT sighandlers
    for( int sig = 0; sig < kMAXSIGNALS; sig++) {
        gSystem->ResetSignal((ESignals)sig);
    }

    auto runID = P348_RECO_NAMESPACE::RunNo_t{ 6030 };

    sdc::CalibDataTraits<na64dp::calib::RTCalibEntry>::Collection<na64dp::calib::RTCalibEntry> rts;
    {  // load data using SDC
        std::shared_ptr<sdc::Documents<P348_RECO_NAMESPACE::RunNo_t>::iLoader>
            extCSVLoader = std::make_shared<sdc::ExtCSVLoader<P348_RECO_NAMESPACE::RunNo_t>>();
        sdc::Documents<P348_RECO_NAMESPACE::RunNo_t> docs;
        docs.loaders.push_back(extCSVLoader);
        // push calibs file to the index
        if( ! docs.add( argv[2] ) ) {
            std::cerr << "Failed to add SDC document \""
                      << argv[2] << "\"." << std::endl;
            return 1;
        }
        //docs.dump_to_json(std::cout);  // this is to inspect loader state
        // load data for run:
        rts = docs.load<na64dp::calib::RTCalibEntry>(runID);
    }
    
    std::cout << "Loaded " << rts.size() << " entries for run "
              << runID.v << ":" << std::endl;
    for( const auto rte : rts ) {
        std::cout << "  "
                  << rte.first << " : a="
                  << rte.second.a << " b="
                  << rte.second.b << " t0="
                  << rte.second.t0
                  << (rte.second.invertTime ? ", inverted time axis" : "")
                  << std::endl;
    }

    // Parse calibrations file:
    //sdc::Documents<P348_RECO_NAMESPACE::RunNo_t>::iLoader * loader;
    //loader->read_data( argv[1], 123, "na64sw/rt" );

    TFile * f = TFile::Open(argv[1], "READ");
    if( !f ) {
        std::cerr << "Failed to open file \"" << argv[1] << "\"." << std::endl;
        return 1;
    }
    std::cout << "Looking for ST R(T) histograms in \"" << argv[1] << "\":"
        << std::endl;
    std::unordered_map<std::string, TH2F *> hsts;
    for( int nStation = 0; nStation < 15; ++nStation ) {
        const char projsStr[] = "XYUV";
        for( const char * proj = projsStr; '\0' != *proj; ++proj ) {
            for( int nPlane = 0; nPlane < 2; ++nPlane ) {
                char pathBf[128];
                snprintf( pathBf, sizeof(pathBf)
                        , "ST/%02d/RT-%02d-ST-%c%c"
                        , nStation, nStation, *proj
                        , nPlane ? '2' : '\0'
                        );
                TObject * objPtr = f->Get(pathBf);
                if(!objPtr) continue;
                char planeNameBf[128];
                snprintf( planeNameBf, sizeof(planeNameBf)
                        , "ST%02d%c%c"
                        , nStation
                        , *proj
                        , 0 == nPlane ? '1' : '2'
                        );
                TH2F * hstPtr = dynamic_cast<TH2F*>(objPtr);
                if( !hstPtr ) {
                    std::cerr << "\"" << planeNameBf << "\" is not of"
                        " type TH2F, skipping..." << std::endl;
                    continue;
                }
                std::cout << "  found histogram " << pathBf << ", assuming it"
                    " to be of " << planeNameBf << " plane." << std::endl;
                hsts[planeNameBf] = hstPtr;
            }
        }
    }
    if( hsts.empty() ) {
        std::cerr << "Found no R(T) histograms in file \"" << argv[1]
            << "\", giving up." << std::endl;
        return 1;
    }

    unsigned short nCols = ceil(sqrt(hsts.size()))  // std::round
                 , nRows = ceil( float(hsts.size())/nCols )
                 ;

    TCanvas * cnv = new TCanvas( "cnv-RT", "R(T) distributions" );
    cnv->Divide(nCols, nRows);
    int nHst = 0;
    for( auto p : hsts ) {
        cnv->cd(++nHst);
        // Draw the R(T) histogram plot
        p.second->Draw("COLZ");
        // If R(T) calibration entry exists, create a TGraph based on it and
        // overimpose a curve
        auto cIt = rts.find(p.first);
        if( rts.end() == cIt ) {
            std::cerr << "No R(T) calibration entry for \""
                      << p.first << "\"..." << std::endl;
            continue;
        }
        na64dp::calib::RT & rt = cIt->second;
        double rv[40], tv[40];
        for( size_t i = 0; i < 20; ++i ) {
            tv[20 + i] = rt.t0 + i*2.5;
            rv[20 + i] = rt(tv[20 + i]);
            rv[19 - i] = - rv[20 + i];
            if(rt.invertTime) {  // negate time
                tv[20 + i] = rt.t0 - i*2.5;
            }
            tv[19 - i] = tv[20+i];
        }
        TGraph * rtGraph = new TGraph(sizeof(rv)/sizeof(*rv), rv, tv);
        rtGraph->SetLineColor(2);
        rtGraph->SetLineWidth(2);
        rtGraph->Draw("SAME");
    }

    #if 0
    TH2F * hstPtr; {
        TObject * hstObj = f->Get(histPath.c_str());
        if(!hstObj) {
            std::cerr << "File \"" << filePath
                      << "\": failed to retrieve histogram \""
                      << histPath
                      << "\"." << std::endl;
            return 1;
        }
        hstPtr = dynamic_cast<TH2F*>(hstObj);
        assert(hstPtr);  // is not of TH2F
    }
    hstPtr->Draw("COLZ");
    #endif
    //f->Close();  // in script mode: do not close the file
    rootApp->Run();
    f->Close();
    return EXIT_SUCCESS;
}
