/* This file is a part of NA64SW software
 *
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include "na64common/app.hh"
#include "na64app/extension.hh"
#include "na64util/gsl-integration.hh"
#include "na64event/data/event.hh"

#include "na64dp/abstractEventSource.hh"
#include "na64dp/pipeline.hh"
#include "na64dp/processingInfo.hh"
#include "na64util/runtimeDirs.hh"

#include "na64calib/manager.hh"
#include <hdql/context.h>

#if defined(ZMQ_FOUND) && ZMQ_FOUND
#   include "na64dp/processingInfoZMQ.hh"
#endif

#include "na64util/log4cpp-extras.hh"
#include "na64util/YAMLLog4cppConfigurator.hh"

#include <log4cpp/Priority.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PropertyConfigurator.hh>

#include "na64event/hdql-assets.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND
#include "na64util/ROOT-sighandlers.hh"
#include <TFile.h>
#include <TInterpreter.h>  // xxx
#endif

#if defined(NCRM_FOUND) && NCRM_FOUND
#   include "ncrm_fork.h"
#endif

#include <yaml-cpp/yaml.h>
#include <gsl/gsl_errno.h>

#include <unistd.h>
#include <poll.h>
#include <getopt.h>

#include <iostream>

// Size of buffer to intercept unhandled stdin/stdout output
#define STD_STREAMS_BUFFER_MAX_SIZE (5*1024*1024)

// General application-relevant settings and options set from command line
struct ApplicationConfig {
    // .yaml file used to configure the pipeline
    std::string runConfigFile;
    // Input configuration file name
    std::string inputConfigFile;
    // List of input sources
    std::vector<std::string> inputs;
    #if defined(ROOT_FOUND) && ROOT_FOUND
    // Name of the ROOT output file
    std::string ROOTOutputFile;
    # endif
    // Max number of events
    size_t nMaxEvs;
    // length of event buffer
    size_t evBufSizeKb;
    // Calibration config file
    std::string calibCfgURI;
    // Logger configuration file
    std::string logCfg;
    // Loadable modules list
    std::list<std::string> modules;
    // Parameters for plug-ins
    std::unordered_map<std::string, std::string> extPars;
    // When non-empty makes application to dump calibrations as YAML document
    // to this file, after all events processed
    std::string makeCalibsDumpTo;
    #if defined(NCRM_FOUND) && NCRM_FOUND
    ncrm_Fork * ncrmFork;
    #endif
    // ... more runtime options may be added here
};

// Sets some default values for the app
static void
initialize_defaults( ApplicationConfig & appCfg ) {
    // Note: after this call NA64SW_MODULES_PATH_ENVVAR and
    // NA64SW_RUN_CFG_PATH_ENVVAR must be set to their defaulr
    // values. This function can raise an error if NA64SW_PREFIX is not set
    // (std::runtime_error)
    na64dp::util::set_std_environment_variables();
    auto cfgRootDirs = na64dp::util::expand_names("${" NA64SW_RUN_CFG_PATH_ENVVAR "}");

    appCfg.evBufSizeKb = 1024;
    appCfg.runConfigFile = "default.yaml"; //cfgRootDir + "/presets/pipe-configs/default.yaml";
    appCfg.inputConfigFile = "${" NA64SW_PREFIX_ENVVAR "}/share/na64sw/source-ddd.yaml";
    appCfg.calibCfgURI     = "${" NA64SW_PREFIX_ENVVAR "}/share/na64sw/calibrations.yaml";
    appCfg.logCfg          = "${" NA64SW_PREFIX_ENVVAR "}/share/na64sw/logging.yaml";
    // ^^^ TODO: default input has to become the stdin here
    #if defined(ROOT_FOUND) && ROOT_FOUND
    appCfg.ROOTOutputFile = "processed.root";
    # endif
    appCfg.nMaxEvs = 0;
    appCfg.modules.push_back( "std-handlers" ); // extensions/handlers  // standard handlers
    //appCfg.modules.insert( "exp-handlers" );  // experimental handlers
    appCfg.modules.push_back( "ddd" );  // DDD data source
    // ... more default values for runtime options may be added here
}

void
usage_info( const char * appName
          , std::ostream & os
          , const ApplicationConfig & appCfg ) {
    std::string dftModules;
    {
        std::ostringstream oss;
        for( auto mf : appCfg.modules ) {
            oss << mf << ",";
        }
        oss << "\b";
        dftModules = oss.str();
    }
    os << "\033[1mNA64sw pipeline command-line launcher\033[0m" << std::endl
       << "Usage:" << std::endl
       << "  $ \033[1m" << appName
       << "\033[0m [-h,--help] [-l,--list [<type>]]"
          #if defined(ROOT_FOUND) && ROOT_FOUND
          " [-o,--root-file outputROOTFile]"
          #endif
          " [-i,--input-cfg <file>]"
          " [-r,--run <file>]"
          " [-N,--max-events maxEventsNo <number>]"
          " [-c,--calibrations <calibCfgURI>]"
          " [-m,--load-module <module>]"
          " [-J,--log-cfg <file>]"
          " [--dump-calibs <file>]"
          " [--event-buffer-size-kb <number>]"
          " [--ext-par <parameterName>=<parameterValue>]"
          " [--no-banner]"
          " [file1] [file2 ...]"
       << std::endl;
    os << "where:" << std::endl
       << "  \033[0;32m-h,--help\033[0m causes app to print out this message and exit." << std::endl
       << "  \033[0;32m-l,--list\033[0m [=handlers|sources|getters-SADC|getters-APV|detector-id-definitions] causes"
          " app to print the lists of known handlers, sources and various event"
          " getters available to use within the run-config. If given in a long"
          " form with =<arg> part, only the corresponding sort of entities"
          " will be printed (e.g. only handlers if provided as"
          " \"--list=handlers\"."
          " Important note: due to C library limitations (getopt_long), the"
          " argument for this"
          " option (e.g. \"hanlers\", \"sources\", etc.) will be recognized"
          " only if given with equal sign (=); space separation won't work."
       << std::endl
       << "  \033[0;32m-r,--run\033[0m <runConfigFile=\033[2m" << appCfg.runConfigFile
       << "\033[0m> shall refer to the YAML file configuring the data treatment"
          " pipeline. File may be referenced by its path or name one file in"
          " the directories listed in \033[0;32mNA64SW_RUN_CFG_PATH\033[0m"
          " environment variable."
       << std::endl
       << "  \033[0;32m-i, --input-cfg\033[0m";
    if(!appCfg.inputConfigFile.empty()) {
       os << " <input-config-file=\033[2m" << appCfg.inputConfigFile << "\033[0m>";
    }
    os << " is the input configuration .YMAL description." << std::endl
       #if defined(ROOT_FOUND) && ROOT_FOUND
       << "  \033[0;32m-o,--root-file\033[0m <outputROOTFile=\033[2m" << appCfg.ROOTOutputFile
       << "\033[0m> refers the ROOT output file where processed data has to be written"
       << std::endl
       #endif
       << "  \033[0;32m-c,--calibrations\033[0m <calibCfgURI=\033[2m" << appCfg.calibCfgURI << "\033[0m> sets the YAML file"
          " configuring the calibration system." << std::endl
       << "  \033[0;32m--dump-calibs\033[0m <file> sets the output YAML file"
          " for full calibrations state dump, after all the event processed."
          " Used for debugging purposes." << std::endl
       << "  \033[0;32m-N,--max-events\033[0m <maxEventsNo=\033[2m" << appCfg.nMaxEvs << "\033[0m> sets the number of events"
          " to be read. Set to 0 to read till the end of input source(s)." << std::endl
       << "  \033[0;32m-J,--log-cfg\033[0m <log4cppConfig=\033[2m" << appCfg.logCfg << "\033[0m> is log4cpp configuration"
          " \"properties\" file that sets up logging configuration (sinks,"
          " layouts, etc)." << std::endl
       << "  \033[0;32m--event-buffer-size-kb\033[0m <evBufferSize=\033[2m" << appCfg.evBufSizeKb << "\033[0m> is"
          " the reentrant event buffer size in kilobytes used to keep all the"
          " single event information. Better to keep this buffer to be of some"
          " reasonably small capacity to benefit performance from on-CPU cache"
          " (up to one Mb or two), but it for some data it may be impossible"
          " due to pile-up high hits multiplicity, etc." << std::endl
       << "  \033[0;32m-m,--load-module\033[0m <moduleFile=\033[2m" << dftModules
       << "\033[0m> dynamically loads an extending"
          " module file (typically, a shared object file). The module path may"
          " be given as follows: a path to the ELF file, the name of the"
          " loadable without leading `lib` and"
          " trailing .so to be found in one of the directories given by"
          " \033[0;32mNA64SW_MODULES_PATH\033[0m environment variable."
       << std::endl
       << "  \033[0;32m-E,--ext-par\033[0m <parName>=<parValue>"
          " Sets the supplementary extension parameter to certain value (one"
          " may query the list of available parameters with --list=ext-pars)."
          " Note that extension parameters becoming available once"
          " corresponding module is loaded."
       << std::endl
       << "  \033[0;32m--no-banner\033[0m"
          " Disables banner and copyright information printed by default."
       << std::endl
       << std::endl
       #if defined(NCRM_FOUND) && NCRM_FOUND
       << "  \033[0;32m-p,--processes\033[0m <number>"
          " Makes application to run in multiprocesses mode (via ncrm)."
          " Specified number of processes must be >1 and less than maximum"
          " number processes supported by ncrm."
       #endif
       << std::endl
       << "If no input file(s) given, the application expects binary input"
          " from stdin to be a result of `ECALEventWrite' handler."
       << std::endl
       << "  NOTE: The loadable module look-up paths may be provided by"
          " \033[0;32m" NA64SW_MODULES_PATH_ENVVAR "\033[0m environment variable,"
          " a colon-separated list of paths): \""
       << (getenv(NA64SW_MODULES_PATH_ENVVAR) ? getenv(NA64SW_MODULES_PATH_ENVVAR) : "(not set)") << "\""
       << std::endl
       << " One may override it with list of directories"
          " where modules look-up shall be performed."
       << std::endl
       << "  NOTE: Runtime config lookup is done in the directories listed in "
          "\033[0;32m" NA64SW_RUN_CFG_PATH_ENVVAR "\033[0m environment variable,"
          " a colon-delimited list of directories paths: \""
       << (getenv(NA64SW_RUN_CFG_PATH_ENVVAR) ? getenv(NA64SW_RUN_CFG_PATH_ENVVAR) : "(not set)") << "\""
       << std::endl
       ;
}

// Sets the application config based on command line arguments. Returns
// non-zero in case of problems.
// NOTE: may fork process internally
int set_app_config( int argc, char * argv[]
                  , ApplicationConfig & appCfg ) {
    static struct option longOpts[] = {
        { "help",           no_argument,       NULL, 'h' },
        { "no-banner",      no_argument,       NULL,  0  },
        { "list",           optional_argument, NULL, 'l' },
        { "root-file",      required_argument, NULL, 'o' },
        { "input-cfg",      required_argument, NULL, 'i' },
        { "run",            required_argument, NULL, 'r' },
        { "max-events",     required_argument, NULL, 'N' },
        { "calibrations",   required_argument, NULL, 'c' },
        { "load-module",    required_argument, NULL, 'm' },
        { "log-cfg",        required_argument, NULL, 'J' },
        { "ext-parameter",  required_argument, NULL, 'E' },
        { "dump-calibs",    required_argument, NULL,  0  },
        { "event-buffer-size-kb", required_argument, NULL, 0 },
        #if defined(NCRM_FOUND) && NCRM_FOUND
        { "processes",      no_argument,       NULL, 'p' },
        #endif
        { NULL, 0x0, NULL, 0x0 },
    };
    std::string listToks;
    int c, optIdx;
    size_t n;
    std::string pstr;
    bool hadError = false
       , listRegistered = false
       , noBanner = false
       ;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt_long( argc, argv, "c:"
                            #if defined(ROOT_FOUND) && ROOT_FOUND
                            "o:"
                            #endif
                            #if defined(NCRM_FOUND) && NCRM_FOUND
                            "p:"
                            #endif
                            "r:D:C:N:m:n:i:J:E:hl::"
                          , longOpts, &optIdx )) != -1 ) {
        switch(c) {
            case 0 :
                if(!strcmp( longOpts[optIdx].name
                          , "event-buffer-size-kb")) {
                    appCfg.evBufSizeKb = atol(optarg);
                } else if(!strcmp( longOpts[optIdx].name
                                 , "no-banner"
                                 )) {
                    noBanner = true;
                } else if(!strcmp( longOpts[optIdx].name
                                 , "dump-calibs"
                                 )) {
                    appCfg.makeCalibsDumpTo = optarg;
                } else {
                    std::cerr << "Unable to parse option \""
                              << longOpts[optIdx].name << "\""  << std::endl;
                    hadError = true;
                }
                break;
            case 'r' : appCfg.runConfigFile = optarg;
                break;
            #if defined(ROOT_FOUND) && ROOT_FOUND
            case 'o' :
                appCfg.ROOTOutputFile = optarg;
                break;
            #endif
            case 'N' :
                appCfg.nMaxEvs = atol(optarg);
                break;
            case 'c' :
                appCfg.calibCfgURI = optarg;
                break;
            case 'i' :
                appCfg.inputConfigFile = optarg;
                break;
            case 'm' :
                appCfg.modules.push_back(optarg);
                break;
            case 'J' :
                appCfg.logCfg = optarg;
                break;
            case 'l' :
                listRegistered = true;
                //if(optarg) listToks = optarg;
                if( optarg == NULL  // if have next argument 
                 && optind < argc  // ..(or separated w/ whitespace)
                 && '-' != *argv[optind]  // and it does not look like an option
                 && 0 != access( argv[optind], F_OK )  // and it is not a file
                 ) {
                    optarg = argv[optind++];  // consider as an option's argument
                }
                if( optarg ) listToks = optarg;
                break;
            case 'E' :
                pstr = optarg;
                for( n = pstr.find('='); n != std::string::npos && n > 0 && pstr[n-1] != '\\';
                     n = pstr.find('=', n+1) ) { if('=' == pstr[n]) break; }
                if(n == std::string::npos || pstr[n] != '=') {
                    std::cerr << "Extension parameter \"" << optarg << "\": format error."
                          << std::endl;
                    hadError = true;
                } else {
                    appCfg.extPars[pstr.substr(0, n)] = pstr.substr(n+1);
                }
                break;
            #if defined(NCRM_FOUND) && NCRM_FOUND
            case 'p' :
                // ... TODO
                break;
            #endif
            // ... more runtime options may be added here
            case 'h' :
                usage_info( argv[0], std::cout, appCfg );
                return -1;
            case ':' :
                std::cerr << "Option -" << optopt << " requires an argument."
                          << std::endl;
                hadError = true;
                break;
            case '?' :
                std::cerr << "Unrecognized option '-" << optopt << "'."
                          << std::endl;
                hadError = true;
        }
    }
    if( hadError )
        return -2;
    if( !noBanner ) {
        na64dp::util::print_banner( std::cout
                , "NA64 data processing software (pipelining tool)");
    }
    #if defined(NCRM_FOUND) && NCRM_FOUND
    if( !nProcesses ) {
        appCfg.ncrmFork = ncrm_fork();
        if( -1 == childPid ) {
            /* Something went wrong with forking... Continue in
             * single-process mode */
            fputs( "Failed to fork; continuing in single-process mode due"
                   " to an error.\n"
                 , stderr );
        } else if( 0 != childPid ) {
            /* Denotes exit of parent process after successful fork() and
             * execution. No further actions should be taken. */
            return 1;
        }
    }
    #endif
    for ( ; optind < argc; ++optind ) {
        appCfg.inputs.push_back(argv[optind]);
    }
    // Before any sybsystems initialized, load subsidiary modules
    int modulesLoadRC = na64dp::util::load_modules( appCfg.modules
                                                  , getenv(NA64SW_MODULES_PATH_ENVVAR) );
    if( listRegistered ) {
        na64dp::util::list_registered( std::cout,
                listToks.empty() ? nullptr : listToks.c_str() );
        return -1;
    }
    return modulesLoadRC ? modulesLoadRC : 0;
}

int
main(int argc, char *argv[]) {
    //                                                               __________
    // ____________________________________________________________/ App infra
    #if defined(ROOT_FOUND) && ROOT_FOUND
    // disable ROOT signal handlers
    {
        const char * v = getenv("KEEP_ROOT_SIGHANDLERS");
        if( !v || !(v && ('1' == v[0] && '\0' == v[1] )) ) {
            disable_ROOT_sighandlers();
        } else {
            std::cerr << "ROOT signal handlers are kept." << std::endl;
        }
    }
    #endif
    // Set custom exception-throwing error handler for GSL
    /*old_handler = */ gsl_set_error_handler( na64dp::errors::GSLError::na64sw_gsl_error_handler );

    ApplicationConfig appCfg;
    initialize_defaults( appCfg );
    int rc = set_app_config( argc, argv, appCfg );  // loads modules, may fork()
    if(  1 == rc ) {  // indicates parent process' exit (when forking)
        return EXIT_SUCCESS;  // TODO: use saved child's return code!
    }
    if( -2 == rc ) {  // error occured during initial application configuration
        usage_info( argv[0], std::cerr, appCfg );
        return EXIT_FAILURE;
    }
    if( -1 == rc ) {  // one of the "single-action" options is given (-l, -h)
        return EXIT_SUCCESS;
    }
    //                                                                    _____
    // _________________________________________________________________/ Logs
    // Initialize logging subsystem
    na64dp::util::inject_extras_to_log4cpp();
    log4cpp::Appender * consoleAppender = nullptr;
    if( ! appCfg.logCfg.empty() ) {
        try {
            na64dp::util::YAMLLog4cppConfigurator::configure(appCfg.logCfg); //< expansion not needed here, done inside
        } catch(std::exception & e) {
            std::cerr << "\033[1;31mError\033[0m: while configuring logging system on file \""
                      << appCfg.logCfg << "\": " << e.what() << std::endl;
            throw;
        }
    } else {
        consoleAppender
                = new log4cpp::OstreamAppender("console", &std::cout);
        consoleAppender->setLayout(new log4cpp::BasicLayout());
        log4cpp::Category & L = log4cpp::Category::getRoot();
        L.addAppender( consoleAppender );
        L.info( "Using zero-conf logging." );
    }
    log4cpp::Category & L = log4cpp::Category::getRoot();
    L.info("Logging initialized.");
    if(rc > 0) {
        L.error( "(There were errors during shared object loading before"
                " logging was initialized)" );
    }

    //                                                            _____________
    // _________________________________________________________/ Calibrations
    // Calib mgr keeps type aliases that maps human-readable type name (C-string)
    // to pair of (RTTI type hash + arbitrary string). Aliases needed before
    // the calibration config file being loaded and usually brought up by
    // extension modules.
    na64dp::calib::Manager calibMgr(log4cpp::Category::getInstance("calib.manager"));
    na64dp::util::setup_default_calibration(calibMgr,
                appCfg.calibCfgURI  //< expansion not needed here, done inside
            );

    // Configure extensions
    for(const auto & p : appCfg.extPars) {
        na64dp::Extensions::self().set_parameter(p.first, p.second);
    }
    hdql_Context * rootHDQLContext = nullptr;
    hdql::helpers::CompoundTypes * compoundTypes = nullptr;
    #if defined(hdql_FOUND) && hdql_FOUND
    // HDQL is enabled -- emit the event data structure definitions and set the
    // root context pointer
    rootHDQLContext = hdql_context_create(HDQL_CTX_PRINT_PUSH_ERROR);  // TODO: configurable
    na64dp::init_root_hdql_context(rootHDQLContext, compoundTypes, calibMgr);
    #endif
    //                                                     ____________________
    // __________________________________________________/ Monitoring & output
    #if defined(ROOT_FOUND) && ROOT_FOUND
    // Open file for root histograms; must be called before handlers creation
    // since handler constructors rely on file already being opened
    TFile * f = appCfg.ROOTOutputFile.empty()
              ? nullptr
              : new TFile(appCfg.ROOTOutputFile.c_str(), "RECREATE")
              ;
    #endif
    // Initialize event processing observer instance, if need
    na64dp::iEvProcInfo * epi
            = new na64dp::TTYStatusProcessingInfo( "", appCfg.nMaxEvs );
    //                                              ___________________________
    // ___________________________________________/ Extension's global objects
    na64dp::Extensions::self().init(calibMgr, epi);
    //                                         ________________________________
    // ______________________________________/ Fork for process-on-demand mode
    // Everything is set up for data processing (initialization done). At this
    // point, pipe process may be put in the waiting state, waiting for
    // incoming connection that will request processing of particular input.
    YAML::Node inputCfgNode, runCfgNode;
    L << log4cpp::Priority::INFO
      << "Using run config from command line: " << appCfg.runConfigFile;
    // construct run config from input parameter
    na64dp::RunCfg rcfg( appCfg.runConfigFile
                       , getenv(NA64SW_RUN_CFG_PATH_ENVVAR)
                       );
    runCfgNode = rcfg.get();
    // TODO: rcfg.override_parameter(...)
    //                                                         ________________
    // ______________________________________________________/ Data processing
    // Pre-allocate local memory storage for hits
    size_t evBufSize = appCfg.evBufSizeKb*1024;
    void * evBuffer = malloc( evBufSize );
    if(!evBuffer) {
        L.fatal( "Failed to allocate reentrant event buffer of %zuKb size."
               , appCfg.evBufSizeKb );
        exit(EXIT_FAILURE);
    }
    // top-level counters, independent on EPI
    size_t evsCount = 0, evsDiscriminated = 0;
    {
        na64dp::AbstractEventSource * evSrc = nullptr;
        // instantiate source
        if( inputCfgNode.IsNull() && !appCfg.inputConfigFile.empty() ) {
            L << log4cpp::Priority::INFO
              << "Using source config from command line: " << appCfg.inputConfigFile;
            try {
                inputCfgNode = YAML::LoadFile( na64dp::util::expand_name(appCfg.inputConfigFile) );
            } catch( std::exception & e ) {
                L.fatal( "Error occurred while accessing, reading or parsing"
                         " file \"%s\": %s", na64dp::util::expand_name(appCfg.inputConfigFile).c_str()
                         , e.what() );
                throw;
            }
        }
        // Create the pipeline object
        if(!runCfgNode["pipeline"]) {
            NA64DP_RUNTIME_ERROR("No \"pipeline\" field in run config object.");
        }
        if(!runCfgNode["pipeline"].IsSequence()) {
            NA64DP_RUNTIME_ERROR("\"pipeline\" field of config object is not a list.");
        }
        na64dp::Pipeline handlers = na64dp::Pipeline(runCfgNode["pipeline"], calibMgr, epi);
        if( handlers.empty() ) {
            L.warn( "Empty pipeline has been built." );
        }

        if(!(inputCfgNode["_type"] && inputCfgNode["_type"].IsScalar())) {
            L.fatal( "Input config has no \"_type\" parameter / parameter is"
                    " not a scalar value." );
            NA64DP_RUNTIME_ERROR("Input config \"_type\" is absent or not a"
                    " scalar value");
        }
        std::string sourceType;
        try {
            sourceType = inputCfgNode["_type"].as<std::string>();
        } catch(std::exception & e) {
            L.fatal( "Input config, \"_type\" parameter error: \"%s\".", e.what() );
            throw;
        }
        try {
            evSrc = na64dp::VCtr::self().make<na64dp::AbstractEventSource>(
                        sourceType,
                        calibMgr, inputCfgNode, appCfg.inputs );
        } catch(std::exception & e) {
            L.fatal( "During instantiation of the data source object an"
                    " exception has occurred: \"%s\".", e.what() );
            throw;
        }

        // Reentrant memory access handle, configurable via macros
        // TODO: handle cases other than `PlainBlock`
        na64dp::mem::PlainBlock evMemBlock(evBuffer, evBufSize);
        // Reentrant memory access API ("pools")
        na64dp::LocalMemory lmem(evMemBlock);
      
        if(epi) {
            epi->start_event_processing();
        }

        bool reRunPipeline;
        do { // this loop is iterated only when external manager is in use
            reRunPipeline = false;
            std::pair<bool, size_t> pplRes;
            // Process the data
            assert(evSrc);
            for(;;) {
                // Create event for current iteration
                auto evRef = lmem.create<na64dp::event::Event>(lmem);
                na64dp::util::reset(*evRef);
                if( evSrc->read(*evRef, lmem) ) {
                    #if defined(ROOT_FOUND) && ROOT_FOUND
                    if(f) f->cd();
                    #endif  // defined(ROOT_FOUND) && ROOT_FOUND
                    pplRes = handlers.process(*evRef, lmem);
                    ++evsCount;
                    if( appCfg.nMaxEvs && evsCount >= appCfg.nMaxEvs ) break;  // max counter exceeded
                    if( pplRes.second ) ++evsDiscriminated;
                    if( pplRes.first ) {
                        L.info( "Top-level pipe abrupt processing." );
                    }
                } else {
                    break;
                }
                lmem.reset();
                evMemBlock.reset();
            }
        } while(reRunPipeline);

        if( !appCfg.makeCalibsDumpTo.empty() ) {  // XXX?
            std::ofstream ofs(appCfg.makeCalibsDumpTo);
            YAML::Node calibsDump;
            calibMgr.to_yaml(calibsDump);
            ofs << calibsDump;
        }
        #if defined(ROOT_FOUND) && ROOT_FOUND
        if(f) f->cd();
        #endif  // defined(ROOT_FOUND) && ROOT_FOUND

        if( evSrc ) {
            evSrc->finalize();
            delete evSrc;
        }

        na64dp::Extensions::self().finalize();
    }
    na64dp::Extensions::self().shutdown();
    free(evBuffer);
    //                                                                _________
    // _____________________________________________________________/ Clean up
    if(epi) {
        // epi->n_events_processed()
        // epi->n_events_discriminated()
        // TODO: print report
        dynamic_cast<na64dp::iEvStatProcInfo*>(epi)->print_summary(std::cout);
        epi->finalize();
        delete epi;
        epi = nullptr;
    }
    L.info( "At top-level pipe: %zu events considered, %zu discriminated."
          , evsCount, evsDiscriminated );

    #if defined(ROOT_FOUND) && ROOT_FOUND
    // Close file for root histograms
    if(f) {
        f->cd();
        f->Write();
        delete f;
        f = nullptr;
    }
    //gApplication->Terminate(0);
    gCling->UnloadAllSharedLibraryMaps();
    #endif
    #if defined(hdql_FOUND) && hdql_FOUND
    if(rootHDQLContext) {
        na64dp::destroy_root_hdql_context(rootHDQLContext, compoundTypes);
    }
    #endif
    // That's all, folks
    L.info("All done -- exiting normally." );
    log4cpp::Category::shutdown();

    return EXIT_SUCCESS;
}





