/*
 * par_gen is the root script providing 
 * moyal parameters triplets generation.
 * The first arguement of par_gen is the 
 * absolute path to root file, containing
 * 3D hists of parameters distribution.
 * The second arguement is the path to 
 * concrete histogram in root file.
 * The third arguement is the name of
 * 3-D histigram. It is easy to redirect
 * triplets output in txt file:
 * root '(par_gen("...")' > ../data/parameters.txt 
 */

#include <iostream>

void par_gen(std::string fileName, std::string dirName, std::string histName) {
    const char * tFileName = fileName.c_str();
    const char * tDirName = dirName.c_str();
    const char * tHistName = histName.c_str();
    
    // Moyal function parameters
    Double_t x = 0;  // Area
    Double_t y = 0;  // Width 
    Double_t z = 0;  // MaxPosition
    
    // Open the root file
    TFile * file = TFile::Open(tFileName);
    if (!file) {
        std::cerr << "Error: unable to open file \""
                  << tFileName << "\"." << std::endl;
    } 
    
    // Open the directory
    TDirectory::Cd(tDirName);
    if (!gDirectory) {
      std::cerr << "Error2";
    }
    
    // Create an instance of TH3F
    TH3F * hist = (TH3F*)gDirectory->Get(tHistName);
    if (!hist) {
    std::cerr << "Cannot create an instance of TH3F";
    } 

    std::cout << "Area        " << "Width      "     
              << "MaxPosition" << std::endl;
    std::cout << std::endl;
              
    for (int i = 0; i < 10000000; i ++) {
    // Get parameters triplet
    hist->GetRandom3( x, y, z);
    
    std::cout << x << "    "
              << y << "    "
              << z << std::endl;
    std::cout << std::endl;
    }
}
    
        
    
