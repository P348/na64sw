#include "ApplicationConfig.h"

void usage_info() {
    printf ( " %s\n %s\n %s\n %s\n %s\n %s\n %s\n %s\n %s\n" 
           , "-f name of the input file."
           , "-o name of the output file."
           , "-x sets x-axis coordinate (for ECAL from 0 to 5)."
           , "-y sets y-axis coordinate (for ECAL from 0 to 5)."
           , "-z sets z-axis coordinate (for ECAL 0 - preshower, 1 -main)."
           , "-I print info on the program work process (default - false)."
           , "-E print info energy (default - false)."
           , "-T printing coefficient in table 72 by 6 (default - false)."
           , "-C printing information on a specific cell (x_y_z) (default - false), need enter x, y, z."  
           );
}

// command line interface
int set_app_config(ApplicationConfig* AppConf, int argc, char * argv[]) {

    int c;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt(argc, argv, "f:x:y:z:o:hIETC")) != -1) {
        switch(c) {
            case 'f' :
                (*AppConf).DirParam.InFileName = optarg;
                break;
            case 'x' :
                (*AppConf).position.x = atoi(optarg);
                break;
            case 'y' :
                (*AppConf).position.y = atoi(optarg);
                break;
            case 'z' :
                (*AppConf).position.z = atoi(optarg);
                break;
            case 'o' :
                (*AppConf).DirParam.OutFileName = optarg;
                break;
            case 'I' :
                (*AppConf).FlagProc.PrintInfoProcess = true;
                break;
            case 'T' :
                (*AppConf).FlagProc.PrintTableCoeff = true;
                break;
            case 'E' :
                (*AppConf).FlagProc.PrintInfoEnerge = true;
                break;
            case 'C' :
                (*AppConf).FlagProc.PrintCell = true;
                break;
            case 'h' :
                usage_info();
                return 1;
                break;
        }
    }
    return 0;
}
