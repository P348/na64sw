#pragma once
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_linalg.h>

// Get inverse matrix from output matrix 
// and recoding inverse matrix in output matrix
float invert_matrix( gsl_matrix* InMatrix
                   , gsl_matrix* OutMatrix);

// Fill this matrix with random from 0 to RandMax
void fill_random_matrix( gsl_matrix* InMatrix
                       , int RandMax);

// Get pseudoinverse matrix
float get_pseudoinv_matrix ( gsl_matrix* OriginalMatrix
                           , gsl_matrix* PseudoInverseMatrix);

// sum all element matrix
float sum_gsl_matrix_element (gsl_matrix* CheckMatrix);

// solving the matrix equation for square matrices
void gsl_solover_matrix_eq ( gsl_matrix* OriginalMatrix
                           , gsl_vector* EnergTotl 
                           , gsl_vector* CoeffS);
