#pragma once
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_linalg.h>

// Print matrix in terminal
void print_matrix(gsl_matrix* InMatrix);

// Print runtime
float print_time(unsigned int start_time);

//
void fill_vector_energ (gsl_vector * EnergTotl);

//
void print_gsl_vector(gsl_vector* Coeff);

//
float mean_gsl_vector(gsl_vector* Coeff);

//
float chi_sq_gsl_matrix_vector(gsl_vector* EnergTotl, gsl_vector* Coeff, gsl_matrix* OriginalMatrix);
