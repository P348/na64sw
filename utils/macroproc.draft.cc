
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <regex>
#include <list>
#include <unordered_map>
#include <cassert>
#include <unordered_set>
#include <cstring>

namespace na64dp {
namespace errors {
class MacroSubstitutionError : public std::runtime_error {
public:
    std::string srcID;
    size_t nLine, nColumn;
    std::string lineStr;  // can be partially substituted already (nColumn differs)
    MacroSubstitutionError(const char * reason) : std::runtime_error(reason) {}
};
}

namespace util {

/**\brief Macro-processed file interface */
class iMacroProcessedFile {
public:
    typedef std::unordered_map<std::string, std::string> Dict;

    static std::shared_ptr<iMacroProcessedFile> read(std::istream & is
            , const Dict & dict
            , const std::string & sourceID);

    virtual bool is_good() const = 0;
    virtual std::string get_line() = 0;
};

/**\brief Simple macro-processed file interface */
class SimpleMacroFile : public iMacroProcessedFile {
private:
    std::istream & _is;
    struct State {
        const std::string _srcID;
        size_t nLine;
        const Dict & _dict;
        std::string line;

        struct ConditionStackItem {
            size_t originLine;
            bool alterated;  // set after `else` block
            bool isTrue;     // controls output
            bool accounted;  // set after `elif` block
        };
        std::list<ConditionStackItem> conditionsStack;

        bool do_fwd_output() const;

        // dereferences identifier
        std::string dereference(const std::string & id, bool permitEmpty=true) const;
        std::string dereference(const std::string & id, const std::string &, bool permitEmpty=true) const;
        // returns true if line should be forwarded
        bool handle_line();
        // handles markup content, returns substitution string
        std::string handle_markup(const std::string & expression);
        // handles condition string given within [el]if-clause
        bool handle_if_expression(const std::string & expression);
    } _state;
public:
    SimpleMacroFile( std::istream & is
                   , const Dict & dict
                   , const std::string & srcID
                   ) : _is(is)
                     , _state{srcID, 0, dict}
                     {}

    bool is_good() const override { return _is.good(); }

    std::string get_line() override {
        if(!_is.good()) return "";
        do {
            std::getline(_is, _state.line);
            ++_state.nLine;
            if(_state.handle_line()) return _state.line;
        } while(_is.good());
        return "";
    }

    static bool lexical_cast(const std::string & expr);
};

//
// Value to boolean
static const char 
      * gTrueValues[] =  {"true",  "yes", "on", "enable",  "1", ""}
    , * gFalseValues[] = {"false", "no", "off", "disable", "0", ""}
    ;

bool
SimpleMacroFile::lexical_cast(const std::string & expr_) {
    std::string expr(expr_);
    std::transform(expr.begin(), expr.end(), expr.begin(),
        [](unsigned char c){ return std::tolower(c); });
    for(const char ** trueTok = gTrueValues; '\0' != **trueTok; ++trueTok) {
        if(0 == strcmp(expr.c_str(), *trueTok)) return true;
    }
    for(const char ** falseTok = gFalseValues; '\0' != **falseTok; ++falseTok) {
        if(0 == strcmp(expr.c_str(), *falseTok)) return false;
    }
    assert(false);  // TODO
}

bool
SimpleMacroFile::State::do_fwd_output() const {
    if(conditionsStack.empty()) return true;
    for(const auto & p : conditionsStack) {
        if(!p.isTrue) return false;
    }
    return true;
}

static const std::regex
      gMarkupRx = std::regex(R"~(\{%(-)?\s*(.+?[^\\])\s*(-)?%\})~")  // markup expression regex
    , gSubstExprRx = std::regex(R"~(^\s*(\$?\w+)(?:\s*\?\s*(.+?))?\s*$)~")  // identifier (possibly with default value)
    ;

bool
SimpleMacroFile::State::handle_line() {
    std::string::const_iterator start = line.begin()
                              , end = line.end()
                              ;
    std::string substToken;
    std::smatch m;
    while(std::regex_search(start, end, m, gMarkupRx)) {
        bool stripSpacesLeft  = !m.str(1).empty()
           , stripSpacesRight = !m.str(3).empty()
           ;
        std::cout << "[debug] \""
            << m.str(2) << "\" at position " << m.position() << " of "
            << _srcID << ":" << nLine << '\n';
        try {
            substToken = handle_markup(m.str(2));
        } catch(errors::MacroSubstitutionError & e) {
            e.srcID = _srcID;
            e.nLine = nLine;
            e.nColumn = m.position();
            e.lineStr = line;
            throw e;
        }
        line.replace(m[0].first, m[0].second, substToken);
        start = line.cbegin();
        end = line.cend();
    }

    return do_fwd_output();
}

std::string
SimpleMacroFile::State::dereference(const std::string & id
        , bool permitEmpty) const {
    assert(!id.empty());
    if(id[0] == '$') {
        assert(id.size() > 1);
        const char * substValue = getenv(id.c_str() + 1);
        if((!substValue) || (substValue[0] == '\0' && !permitEmpty)) {
            char errbf[128];
            snprintf( errbf, sizeof(errbf)
                    , "Environment variable \"%s\" is %s"
                    , id.c_str(), substValue ? "empty" : "not defined" );
            throw errors::MacroSubstitutionError(errbf);
        }
        return substValue;
    }
    auto it = _dict.find(id);
    if(_dict.end() == it) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "No macro substitution for \"%s\""
                , id.c_str() );
        throw errors::MacroSubstitutionError(errbf);
    }
    return it->second;
}

std::string
SimpleMacroFile::State::dereference(const std::string & id
        , const std::string & dft
        , bool permitEmpty) const {
    assert(!id.empty());
    if(id[0] == '$') {
        assert(id.size() > 1);
        const char * substValue = getenv(id.c_str() + 1);
        if((!substValue) || (substValue[0] == '\0' && !permitEmpty)) {
            return dft;
        }
        return substValue;
    }
    auto it = _dict.find(id);
    if(_dict.end() == it) {
        return dft;
    }
    return it->second;
}

static const std::regex
      gRxIfClauseBgn   = std::regex(R"~(^\s*if\s+(.+?)\s*)~")
    , gRxElIfClauseBgn = std::regex(R"~(^\s*elif\s+(.+?)\s*)~")
    ;

std::string
SimpleMacroFile::State::handle_markup(const std::string & expr) {
    // supported expressions:
    // (implemented)
    //  "foo" -- look for eponymous item in substitution dict,
    //      substitute. Error if not defined
    //  "foo?some thing" -- look for eponymous item in substitution dict,
    //      substitute. Use default value ("bar") if not defined
    // TODO:
    //  "if <expr>" -- push enable output flag, look for "foo" and allow
    //      subsequent output if found, otherwise disable output till next
    //      else/endif, empty subst
    //  "elif <expr>" -- check condition and alterate output flag
    //  "else" -- alterate current output flag, empty subst
    //  "endif" -- pop current output flag
    
    std::smatch substExpr;
    // NOTE: check for `else`, `endif` before the identifiers to prevent
    // interpretation of these keywords as id.
    if( expr == "else" ) {
        if(conditionsStack.empty()) throw errors::MacroSubstitutionError("Stray \"else\" keyword.");
        if(conditionsStack.back().alterated) throw errors::MacroSubstitutionError("Misplaced or repeatative \"else\"");
        
        conditionsStack.back().isTrue =     !conditionsStack.back().isTrue;
        conditionsStack.back().accounted |=  conditionsStack.back().isTrue;
        return "";
    } else if( expr == "endif" ) {
        if(conditionsStack.empty()) {
            throw errors::MacroSubstitutionError("Stray \"endif\" keyword.");
        }
        conditionsStack.pop_back();
        return "";
    } else if(std::regex_match(expr, substExpr, gSubstExprRx)) {  // substitution
        if(!do_fwd_output()) return substExpr.str(1);  // keep intact if output disabled
        std::string id  = substExpr[1]
                  , dft = substExpr[2]
                  ;
        return dereference(id, dft.c_str());
    } else if( std::regex_match(expr, substExpr, gRxIfClauseBgn) ) {  // IF
        bool r = handle_if_expression(substExpr.str(1));
        conditionsStack.push_back({nLine, false, r, r});
        return "";
    } else if( std::regex_match(expr, substExpr, gRxElIfClauseBgn) ) {  // ELIF
        if(conditionsStack.empty()) throw errors::MacroSubstitutionError("Stray \"elif\" keyword.");
        if(conditionsStack.back().alterated) throw errors::MacroSubstitutionError("Misplaced \"elif\" (\"else\""
                " already given to this tier)");
        if(!conditionsStack.back().accounted) {
            // consider this elif only if if/elif was not true before
            conditionsStack.back().originLine = nLine;
            conditionsStack.back().isTrue = handle_if_expression(substExpr.str(1));
            conditionsStack.back().accounted |= conditionsStack.back().isTrue;
        } else {
            std::cout << "[debug] omitting elif-clause as previous if eval-d to true" << std::endl;
        }
        return "";
    }
    else {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Can't interpret macro expression \"%s\""
                , expr.c_str() );
        throw errors::MacroSubstitutionError(errbf);
    }
    assert(false);
    return "???";
}

const std::regex
          gRxCheckVar  = std::regex(R"~(^\s*(not\s+)?(\$?\w+)(\s*is\s+(not)?\s*set)?\s*$)~")
        // 1 - invert #1
        // 2 - id to check
        // 3 - non-empty, if check is for is-set rather than for value
        // 4 - invert #2
        // NOTE: both 1 and 4 can not be non-empty at the same time
        , gRxCheckPath = std::regex(R"~(^\s*can\s+(not\s+)?(read|write)\s+(dir|file)\s+(.+?)\s*$)~")
        ;

bool
SimpleMacroFile::State::handle_if_expression(const std::string & expr) {
    //  expr (1): <id> [is [not] set]
    std::smatch match;
    bool result = true;
    if(std::regex_match(expr, match, gRxCheckVar)) {  // simple substitution
        // check for double-not:
        if((!match.str(1).empty()) && (!match.str(4).empty())) {
            throw errors::MacroSubstitutionError("Two \"not\" keywords given in"
                    " same logic expression.");
        }
        bool invert = !(match.str(4).empty() && match.str(4).empty());
        // group #2 can be given; if it is not empty, we check that variable
        // is set (defined and not empty) rather, than dereferencing it
        bool checkIfSet = !match.str(3).empty();
        std::string id = match[2];
        assert(!id.empty());
        if(!checkIfSet) {  // check by value
            // group #1 should be always given, it provides id to chec
            std::string expressionToCheck = dereference(id, "");  // TODO: permitEmpty?
            result = SimpleMacroFile::lexical_cast(expressionToCheck);
            std::cout << "[debug] line " << nLine << " condition check (if/elseif clause \"" << expr
                << "\"), condition: \""
                << match[2].str() << "\" -> \"" << expressionToCheck << "\", invert="
                << (invert ? "true" : "false") << ", eval-d to: "
                << std::endl;
        } else {  // check if set
            if('$' == id[0]) {
                assert(id.size() > 1);
                const char * v = getenv(id.c_str() + 1);
                result = (v && *v != '\0');  // env.var is defined and not empty
            } else {
                auto it = _dict.find(id);
                result = (it != _dict.end() && !it->second.empty());  // var defined and not empty
            }
            std::cout << "[debug] line " << nLine << " condition check (if/elseif clause \"" << expr
                << "\"), condition: whether \""
                << match[2].str() << "\" defined -> \"" << (result ? "true" : "false") << "\", invert="
                << (invert ? "true" : "false") << std::endl;
        }
        if(invert) result = !result;
    }
    //  expr (2): can [not] (read|write) [file|dir] <path>
    //  expr (3): <path> exists
    //  expr (4): [not] <id|literal> <('<'|'>'[=])|'&'> <id|literal>
    //  expr (5): <id> is [not] finite
    else {
        throw errors::MacroSubstitutionError("Unknown/unsupported if-clause syntax.");
    }
    return result;
}

std::shared_ptr<iMacroProcessedFile>
iMacroProcessedFile::read(std::istream &is, const Dict &dict, const std::string &sourceID) {
    return std::make_shared<SimpleMacroFile>(is, dict, sourceID);
}

}  // namespace ::na64dp::util
}  // namespace na64dp

int
main(int argc, char * argv[]) {
    // definitions dictionary
    na64dp::util::iMacroProcessedFile::Dict dict;

    //dict["one"] = "";
    dict["two"] = "2";
    dict["uno"] = "";
    dict["dos"] = "%{two}%";

    // open file to read
    std::ifstream ifs(argv[1]);

    std::shared_ptr<na64dp::util::iMacroProcessedFile> fp
        = na64dp::util::iMacroProcessedFile::read(ifs, dict, argv[1]);

    try {
        while(fp->is_good()) {
            std::string l = fp->get_line();
            std::cout << "OUT> \"" << l << "\"" << std::endl;
        }
    } catch(na64dp::errors::MacroSubstitutionError & e) {
        std::cerr << "Error at " << e.srcID << ":" << e.nLine << ":" << std::endl
            << " > " << e.lineStr << std::endl
            << e.what() << std::endl;
    }
    //std::cout << fp.get_output();

    return 0;
}


