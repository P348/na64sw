#include "EcalCalibViewer.h"
#include "Event.h"

void EcalCalibViewer::visual_jenks ( TVirtualPad* padPtr
                                   , std::map< unsigned char, std::vector<Float_t> > jBreaks
                                   , double yMin
                                   , double yMax)
{
    for( auto it : jBreaks ) {
        CellID id;
        id.cellID = it.first;
        if (  !(id.is_preshower() == 
              (appCfg.RootParam.TSumWFDirPartECAL == "preshower" ? 1 : 0)) ) {
            continue;    
        }
        std::cout << !id.is_preshower() << "  " << ( appCfg.RootParam.TSumWFDirPartECAL 
                                                     == "main" ? 1 : 0) << std::endl;
        for( auto val : it.second ) {
            TLine *l = new TLine( val, yMin, val, yMax);
            std::cout << val << std::endl;
            l->Draw();
            l->SetLineWidth(1);
            std::cout << " draw line in hist\n";
        }
    }
}

void EcalCalibViewer::fit_gaus_jbreak ( TVirtualPad* padPtr
                                      , std::map< unsigned char, std::vector<Float_t> > jBreaks){
    double xMin, xMax;
    for( auto it : jBreaks ) {
        CellID id;
        id.cellID = it.first;
        if ( (id.is_preshower() == ( appCfg.RootParam.TSumWFDirPartECAL 
                                      == "preshower" ? 1 : 0)) ) {
            xMin = it.second[1];
            xMax = it.second[2];
            std::cout << "fiting on  " << xMin <<"  "
                      << "to  " << xMax << std::endl;    
            continue;    
        }
    }    
    std::stringstream ss;
    ss << appCfg.RootParam.TNameHist << appCfg.x << "_" 
                           << appCfg.y << "_" 
                           << ( appCfg.RootParam.TSumWFDirPartECAL 
                                == "main" ? 1 : 0 );

    TH1F* hist =(TH1F*)gDirectory->Get(ss.str().c_str());
    if(!hist) { std::cerr << "Error5 no histogram in fit\n";}
    TF1 *gfit = (TF1 *) hist->GetFunction("gaus");
    hist->Fit("gaus","Q", "E", xMin, xMax);
}

// division of the convoy into 9 zones 
// relative to the central cell, which is set.
void EcalCalibViewer::zones(TCanvas* c1) 
{ 
    // default value
    int IminX = 0, ImaxX = 3;
    int IminY = 0, ImaxY = 3;
    int _ampRange[2] = {0, 1000};
    int _ampNBins = 300;
    
    // selection of cases at the edge
    //if( y == 5 && x > 0 && x < 5 ){ 
    //IminX = 0; ImaxX = 3; IminY = 1; ImaxY = 3;
    //}
    //if (y == 0 && x > 0 && x < 5 ){ 
    //IminX = 0; ImaxX = 3; IminY = 0; ImaxY = 2;
    //}
    //if (x == 5 && y > 0 && y < 5 ){ Imin = 0; Imax = 6;}
    //if (x == 0 && y > 0 && y < 5 ){ Imin = 0; Imax = 6;}  
    
    // status plotting histogram
    std::cout << "ploted histogram in TCanvas" << std::endl;

    // filling histograms in a TCanvas 3 by 3. 
    for(int i = IminX; i < ImaxX; i++){
        for(int j = IminY; j < ImaxY; j++){
            // transition to the next TCanvas
            std::cout << i*ImaxY + j + 1 << std::endl;
            TVirtualPad* padPtr = c1->cd(i*ImaxY + j + 1);
            // set log scale if appCfg.flaglog true
            if(appCfg.FlagProc.flaglog){padPtr->SetLogy();}
            // selection of 8 adjacent histograms, 
            // one at each iteration.
            // name generation based on cell position.
            std::stringstream ss;
            ss << appCfg.RootParam.TNameHist << appCfg.x - 1 + j << "_" 
                                   << appCfg.y + 1 - i << "_" 
                                   << ( appCfg.RootParam.TSumWFDirPartECAL 
                                        == "main" ? 1 : 0 );
            // output to the console the names of the printed histograms
            std::cout << ss.str().c_str() 
                      << " - TCanvas " 
                      << i*ImaxY + j + 1 
                      << std::endl;
            // getting histograms from root file
            TH1F* hist =(TH1F*)gDirectory->Get(ss.str().c_str());
            //
            if(!hist) { std::cerr << "Error4 no histogram\n"; break;}
            hist->DrawCopy();
        }
    }
}

void EcalCalibViewer::read_breaks( TDirectory * tDir
                            , std::map< unsigned char, std::vector<Float_t> > & breaks ) 
{
    //TIter next(GetListOfPrimitives());
    //while( (TObject * obj = next()) ) {
    //    std::cout << "xxx " << obj->GetName() << std::endl;
    //}
    assert(tDir);
    int xIdx = -1
      , yIdx = -1
      , isPreshower = -1;
    char valueName[32], nameBuffer[64];
    for( auto it : *(tDir->GetListOfKeys())  ) {
        // NOTE: arrays names are given in form:
        // "breaks_%s_%d_%d_%d" (value name, x index, y index, is preshower?)
        char * savePtr, * tok;
        strncpy( nameBuffer, it->GetName(), sizeof(nameBuffer) );
        //std::cout << " - " << nameBuffer << "::" << it->ClassName() << " : ";
        //std::cout.flush();  // XXX
        int n = 0;
        for( tok = strtok_r( nameBuffer, "_", &savePtr )
           ; tok ; tok = strtok_r( NULL, "_", &savePtr ), ++n ) {
            //std::cout << tok << ", ";
            switch( n ) {
                case 1: {
                    strncpy( valueName, tok, sizeof(valueName) );
                } break;
                case 2: {
                    xIdx = atoi(tok);
                } break;
                case 3: {
                    yIdx = atoi(tok);
                } break;
                case 4: {
                    isPreshower = atoi(tok);
                } break;
                default:
                    if( n > 4 ) {
                        std::cerr << "Unrecognized Jenks breaks array name \""
                            << it->GetName() << "\". Omitting entry." << std::endl;
                    }
                    break;
            };
        }
        if( n > 5 ) continue;
        TArrayF * arr = (TArrayF *) tDir->Get(it->GetName());
        assert(arr);
        assert(xIdx > -1 && xIdx < 6);
        assert(yIdx > -1 && yIdx < 6);
        assert(isPreshower == 0 || isPreshower == 1);
        breaks[CellID( xIdx, yIdx, isPreshower ).cellID] =
                       std::vector<Float_t>( arr->GetArray()
                                            , arr->GetArray() + arr->GetSize()) ;
    }
}

// open root file with costant part to file.
// checks if the file is opened correctly.
// print histogram in pdf use function 'zones'. 
void EcalCalibViewer::process_view_calib ()
{
        
    // open TFile (root file)
    TFile* f = TFile::Open(appCfg.RootParam.TFileName.c_str(), "read");
    if (!f){std::cerr << "Error1, no root file\n";}

    // go to proper directory (name handler)
    TDirectory::Cd(appCfg.RootParam.TDirName.c_str());
    if (!gDirectory){std::cerr << "Error2, no TDirName\n";}
    // go to proper directory (preshower or main part)
    TDirectory::Cd(appCfg.RootParam.TSumWFDirPartECAL.c_str());
    if (!gDirectory){std::cerr << "Error2, no TSumWFDirPartECAL\n";}

    std::map< unsigned char, std::vector<Float_t> > jBreaks;
    if( appCfg.FlagProc.doCalib ) {
        TDirectory * jenksDirs[3] = {NULL, NULL, NULL};
        if( (jenksDirs[0] = (TDirectory *) gFile->Get("jenksBreaks") )) {
            jenksDirs[1] = (TDirectory *) jenksDirs[0]->Get("preshower");
            jenksDirs[2] = (TDirectory *) jenksDirs[0]->Get("main");
            read_breaks( jenksDirs[1], jBreaks );
            read_breaks( jenksDirs[2], jBreaks );
            std::cout << "Found " << jBreaks.size() << " breaks entries:" << std::endl;
            for( auto p : jBreaks ) {
                CellID id;
                id.cellID = p.first;
                std::cout << "  * " << id.get_x() << "-" << id.get_y() << "-"
                          << (id.is_preshower() ? "p" : "m") << " : ";
                for( auto cb : p.second ) {
                    std::cout << cb << ", ";
                }
                std::cout << std::endl;
            }
        } else {
            std::cout << "No directory \"jenksBreaks\" in TFile." << std::endl;
        }
    }

    // creating a 3 by 3 multipads TCanvas
    TCanvas* c1 = new TCanvas("c1","multipads",800, 600);
    c1->Divide(3,3,0,0);
    
    //
    if(appCfg.FlagProc.doFitGaus){
    fit_gaus_jbreak(c1->cd(5), jBreaks);
    }

    // print histogram in pdf 
    zones(c1);
    
    // visualization of fisher's natural breaks
    visual_jenks(c1->cd(5), jBreaks, 0, 100000 );

    
    //print TCanvas to file .PDF
    c1->Print(appCfg.NameFileOut.c_str());

    // close root file    
    f->Close();   
}

