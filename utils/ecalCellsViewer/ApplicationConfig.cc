#include "ApplicationConfig.h"

ApplicationConfig::ApplicationConfig(){
            x = 3; 
            y = 3; 
            NameFileOut = "plots.pdf";   
            RootParam.TFileName = "ecalOnly.root";
            RootParam.TDirName = "PmtWfAreaHist_AreaWf";
            RootParam.TNameHist = "PmtWfAreaHist_AreaWf_";
            RootParam.TSumWFDirPartECAL = "main";
            FlagProc.flaglog = false;
            FlagProc.doCalib = false;
            FlagProc.doFitGaus = false;
        }

// displays information on the command line interface
void ApplicationConfig::usage_info( const char * appName
                                  , std::ostream & os) 
{
    os << "Usage:" << std::endl
       << "  $ " << appName
       << " [-f <rootFile>] [-d <dirName>] [-n <histName>] [-p]"
       << " [-x <xCellIdx>] [-y <yCellIdx>] [-l] [-h] [-o <outFile>] [-C] [-F]"
       << std::endl;
    os << "where:" << std::endl
       << "  -f <rootFile=" << RootParam.TFileName << "> name of the root file." << std::endl
       << "  -d <dirName=" << RootParam.TDirName << "> name of the directory with histograms." << std::endl
       << "  -n <histName=" << RootParam.TNameHist << "> name of the histogram without position cell (1_1_1)." 
       << std::endl
       << "  -p sets a logical flag that corresponds to preshower." << std::endl
       << "  -x <xCellIdx=" << x << "> sets x-axis coordinate (for ECAL from 0 to 5)." << std::endl
       << "  -y <xCellIdx=" << y << "> sets y-axis coordinate (for ECAL from 0 to 5)." << std::endl
       << "  -o <outFile=" << NameFileOut << "> name of the output file." << std::endl
       << "  -l sets log scale." << std::endl
       << "  -h causes app to print out this message and exit." << std::endl
       << "  -C enables calibration routine (relies on presense of Jenks"
       << " breaks values at the input file)" << std::endl
       << "  -F fit gaus (data input jenks break) cell in (x,y)" << std::endl;
}

// command line interface
int ApplicationConfig::set_app_config( int argc, char * argv[]) {

    int c;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt(argc, argv, "f:d:n:px:y:lho:CF")) != -1) {
        switch(c) {
            case 'f' :
                RootParam.TFileName = optarg;
                break;
            case 'd' :
              RootParam.TDirName = optarg;
                break;
            case 'n' :
                RootParam.TNameHist = optarg;
                break;
            case 'p' :
                RootParam.TSumWFDirPartECAL = "preshower";
                break;
            case 'x' :
                x = atoi(optarg);
                break;
            case 'y' :
                y = atoi(optarg);
                break;
            case 'o' :
                NameFileOut = optarg;
                break;
            case 'l' :
                FlagProc.flaglog = true;
                break;
            case 'C' :
                FlagProc.doCalib = true;
                break;
            case 'F' :
                FlagProc.doFitGaus = true;
                break;
            case 'h' :
                usage_info( argv[0], std::cout);
                return 1;
                break;
        }
    }
    return 0;
}
