#pragma once

#include <TCanvas.h>
#include <TH1.h>
#include <TStyle.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TLine.h>

//#include <TMath.h>
//#include "Fit/Fitter.h"
//#include "Fit/BinData.h"
//#include "Fit/Chi2FCN.h"
//#include "TList.h"
//#include "Math/WrappedMultiTF1.h"
//#include "HFitInterface.h"
//#include "TCanvas.h"
//#include "TStyle.h"

#include "ApplicationConfig.h"

#include <map>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>

class EcalCalibViewer {

private:
    public:

    ApplicationConfig appCfg;
    TFile* f;
    TCanvas* c1;  
        
    

    EcalCalibViewer(){};

    void visual_jenks( TVirtualPad* padPtr
                     , std::map< unsigned char, std::vector<Float_t> > jBreaks
                     , double yMin
                     , double yMax);

    void fit_gaus_jbreak( TVirtualPad* padPtr
                        , std::map< unsigned char, std::vector<Float_t> > jBreaks);

    void zones(TCanvas* c1);

    void read_breaks( TDirectory * tDir
                    , std::map< unsigned char, std::vector<Float_t> > & breaks );

    void process_view_calib ();    
};
