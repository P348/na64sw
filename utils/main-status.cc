#include <string.h>
#include <stdlib.h>
#include <panel.h>

/** Enumeration for view feature flags
 *
 * Views define a content block within a window. Content block shows a plain
 * text content that can be scrolled using scroll bars. Views can also have
 * a progress bar associated and a buttons row.
 * */
enum na64stw_ViewFeatures {
    /** View can have progress bar */
    na64stw_kViewHasProgressBar = 0x1,
    /** View can have vertical scroll bar */
    na64stw_kViewHasVertScroll = 0x1 << 1,
    /** View can have horizontal scroll bar */
    na64stw_kViewHasHorScroll = 0x1 << 2,
    /** View has buttons */
    na64stw_kViewHasButtons = 0x1 << 3,
    /** (?) If enabled, view has dynamic content */
    na64stw_kDynamicContent = 0x1 << 4,
    /* ... */
};

/** Represents a state of viewport: progress- and scroll- bar shown. */
struct na64stw_View {
    /** Name of the viewport. When placed in multi-tab window, this label
     * is used to identify a tab */
    char * title;
    /** Features available/enabled for this view */
    uint32_t features;
    /** Progress, 0..1 */
    float progress;
    /** Scrollbars, 0..1 */
    float scrollBar[2];
};

struct na64stw_View *
na64stw_new_view( const char * title
                , enum na64stw_ViewFeatures features ) {
    struct na64stw_View * v = malloc(sizeof(struct na64stw_View *));
    v->title = strdup(title);
    v->features = (uint32_t) features;
    v->progress = 0;
    v->scrollBar[0] = 0;
    v->scrollBar[1] = 0;
    return v;
}

/** Represents a window, posibly multi-tabbed */
struct na64stw_Window {
    /** Title of the window. */
    char * title;
    /** Array of views, guarded by NULLptr */
    struct na64stw_View * views[8];
    /** Number of currently active view */
    int activeNView;

    int x,y,w,h;
    /** Pointer to ncurses' window */
    WINDOW * ncWin;
    /** Pointer to ncurses' panel */
    PANEL * ncPanel;
};

void
na64stw_win_update( struct na64stw_Window * w ) {
    int startx, starty, height, width;

    getbegyx(w->ncWin, starty, startx);
    getmaxyx(w->ncWin, height, width);

    box(w->ncWin, 0, 0);
    mvwaddch(w->ncWin, 2, 0, ACS_LTEE); 
    mvwhline(w->ncWin, 2, 1, ACS_HLINE, width - 2); 
    mvwaddch(w->ncWin, 2, width - 1, ACS_RTEE); 
}

/** Current applications's state */
struct na64stw_App {
    /** Array of associated windows shown */
    struct na64stw_Window * windows[8];
};

/** Draws window */
int
na64stw_init( struct na64stw_App * app ) {
    for( struct na64stw_Window ** cw_ = app->windows; *cw_ ; ++cw_ ) {
        struct na64stw_Window * cw = *cw_;
        cw->ncWin = newwin(cw->h, cw->w, cw->y, cw->x);
        cw->ncPanel = new_panel(cw->ncWin);
        set_panel_userptr(cw->ncPanel, cw);
        na64stw_win_update(cw);
    }
    return 0;
}

/* Entry point */
int
main(int argc, char * argv[]) {
    /*                                          _______________________________
     * _______________________________________/ Initialize layout and states */
    const int dims[3][4] = {  /* x, y, w, h */
        /* handlers (left panel) */
        { 0, 0, 30, 50 },
        /* attributes (right panel) */
        { 0, 0, 50, 50 },
        /* messages (bottom panel) */
        { 0, 50, 80, 50 },
    };

    struct na64stw_App app;
    {   /* Create "handlers" window */
        app.windows[0] = malloc(sizeof(struct na64stw_Window));
        app.windows[0]->title = strdup("Handlers");
        {   /* Create "handlers" view */
            app.windows[0]->views[0] = na64stw_new_view("handlers"
                    , ((int) na64stw_kViewHasVertScroll)
                    | ((int) na64stw_kViewHasHorScroll)
                    );
        }
        app.windows[0]->views[1] = NULL;

        app.windows[0]->activeNView = 0;
        app.windows[0]->ncWin = NULL;
        /* dims */
        app.windows[0]->x = dims[0][0];
        app.windows[0]->y = dims[0][1];
        app.windows[0]->w = dims[0][2];
        app.windows[0]->h = dims[0][3];
    }
    {   /* Create "browser" window */
        app.windows[1] = malloc(sizeof(struct na64stw_Window));
        app.windows[1]->title = strdup("Browser");
        {   /* Create "handler attributes" view */
            app.windows[1]->views[0] = na64stw_new_view("Attributes"
                    , ((int) na64stw_kViewHasVertScroll)
                    | ((int) na64stw_kViewHasHorScroll)
                    );
        }
        {   /* Create "handler statistics" view */
            app.windows[1]->views[1] = na64stw_new_view("Statistics"
                    , ((int) na64stw_kViewHasVertScroll)
                    | ((int) na64stw_kViewHasHorScroll)
                    );
        }
        app.windows[1]->views[2] = NULL;

        app.windows[1]->activeNView = 0;
        app.windows[1]->ncWin = NULL;
        /* dims */
        app.windows[1]->x = dims[1][0];
        app.windows[1]->y = dims[1][1];
        app.windows[1]->w = dims[1][2];
        app.windows[1]->h = dims[1][3];
    }
    {   /* Create "Messages" window */
        app.windows[2] = malloc(sizeof(struct na64stw_Window));
        app.windows[2]->title = strdup("Messages");
        {   /* Create "log" view */
            app.windows[1]->views[0] = na64stw_new_view("Log"
                    , ((int) na64stw_kViewHasVertScroll)
                    | ((int) na64stw_kViewHasHorScroll)
                    );
        }
        app.windows[2]->views[1] = NULL;

        app.windows[2]->activeNView = 0;
        app.windows[2]->ncWin = NULL;
        /* dims */
        app.windows[2]->x = dims[2][0];
        app.windows[2]->y = dims[2][1];
        app.windows[2]->w = dims[2][2];
        app.windows[2]->h = dims[2][3];
    }
    /* Put a sentinel */
    app.windows[3] = NULL;
    /*                                                     ____________________
     * __________________________________________________/ Initialize curses */
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    /* Initialize all the colors */
    //init_pair(1, COLOR_RED, COLOR_BLACK);
    //init_pair(2, COLOR_GREEN, COLOR_BLACK);
    //init_pair(3, COLOR_BLUE, COLOR_BLACK);
    //init_pair(4, COLOR_CYAN, COLOR_BLACK);

    na64stw_init(&app);
    update_panels();
    refresh();

    doupdate();
    char ch;
    while((ch = getch()) != KEY_F(1)) {
        switch(ch) {
            case 9:
                //top = (PANEL *)panel_userptr(top);
                //top_panel(top);
                break;
        }
        update_panels();
        doupdate();
    }
    endwin();
    return 0;
}

