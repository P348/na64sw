// This program fills map with Mean And Standart Deviation values 
// of pedestals distribution from file Peds.txt. 
// There are two maps for Odd and Even pedestal

#include "DaqEventsManager.h"
#include "Event.h"
#include <map>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>

// The temporary structure for filling maps
struct PedestalInfo {
    float MeanV, StdDevV;
};

std::map <unsigned char, PedestalInfo> PedestalEven;
std::map <unsigned char, PedestalInfo> PedestalOdd;

void read_file (std::ifstream &ifs)
{   
    // instantiation of CellID
    CellID currentID;
    
    // instantiation of PedestalInfo
    PedestalInfo currentPedestalInfo;
    
    std::string s;
    int x, y, flag, isEven;
    float Mean, StdDev;
    
    do
    {
        // Assuming, that data parted by backspaes, read it into variables
        ifs >> isEven >> x >> y >> flag >> Mean >> StdDev;
    
        // Forming cellID from file's information
        currentID.set_x(x);
        currentID.set_y(y);
        currentID.set_is_preshower((bool)flag);
    
        // filling the temporary structure
        currentPedestalInfo.MeanV = Mean;
        currentPedestalInfo.StdDevV = StdDev;
    
        // filling Even or Odd maps
        if (isEven == 1)
        {
            PedestalEven[currentID.cellID] = currentPedestalInfo;
        }
        else
        {
            PedestalOdd[currentID.cellID] = currentPedestalInfo;  
        }
    }
    while (getline(ifs, s));

    // Output information from map
    for (auto it = PedestalEven.begin(); it != PedestalEven.end(); ++it ) 
    {
        std::cout << (*it).second.MeanV << "   " << (*it).second.StdDevV << std::endl;
    } 
    
    std::cout << std::endl;
    
    for (auto it = PedestalOdd.begin(); it != PedestalOdd.end(); ++it ) 
    {
        std::cout << (*it).second.MeanV << "   " << (*it).second.StdDevV << std::endl;
    }
}

int main () 
{
    std::ifstream ifs;
    ifs.open("Peds.txt");
    
    read_file(ifs);
    
    ifs.close();
    
 return 0;   
}

