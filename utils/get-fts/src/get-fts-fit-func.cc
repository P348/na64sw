#include "get-fts-fit-func.hh"

#include <TF1.h>

namespace na64dp {
namespace getFts {

// class GenericHstParameters
// 1D histogram query interface
//

const HstGetterDefinition gGenericHstParametersGetters[] = {
    #define M_MTD(nm) { #nm,       &GenericHstParameters:: nm }
    M_MTD(mean),
    M_MTD(stddev),
    M_MTD(S),
    M_MTD(x_low),
    M_MTD(x_up),
    M_MTD(max_value),
    M_MTD(n_entries),
    M_MTD(mu_minus_sigma_or_x_low), M_MTD(mu_minus_15sigma_or_x_low),   M_MTD(mu_minus_3sigma_or_x_low),
    M_MTD(mu_plus_sigma_or_x_up),   M_MTD(mu_plus_15sigma_or_x_up),     M_MTD(mu_plus_3sigma_or_x_up),
    M_MTD(stddev01), M_MTD(stddev033), M_MTD(stddev10), M_MTD(stddev3),
    M_MTD(S01), M_MTD(S05), M_MTD(S10), M_MTD(S15), M_MTD(S3),
    // ...
    { "", NULL }
};

/// Returns 1D histogram getter func ptr by name
generic_hst_p_getter
generic_getter_by_name(const char * name) {
    for(auto * p = gGenericHstParametersGetters; '\0' != *p->name; ++p) {
        if(!strcmp(name, p->name)) return p->getter;
    }
    return NULL;
}


// class FitFunc
//

// (aux) CrystallBall function
template<int nT>
Double_t
cbf( double x
   , double mu, double sigma
   , double alpha
   , double S
   , int sign  // set to 1 for left, -1 to right
   ) {
    const Double_t alpha2 = alpha*alpha
                 , absAlpha = fabs(alpha)
                 , sq2 = sqrt(2.)
                 , u = sign*(x - mu)/sigma
                 , C = nT*exp(-alpha2/2)/((absAlpha)*(nT-1))
                 , D = sqrt(M_PI/2)*(1 + TMath::Erf(absAlpha/sq2))
                 , N = 1/(sigma*(C+D))
                 ;
    Double_t r;
    if( u <= -alpha ) {
        // sq tail
        const Double_t A = pow((nT/absAlpha), nT)*exp(-alpha2/2)
                     , B = nT/absAlpha - absAlpha
                     ;
        r = A*TMath::Power((B - u), - nT);
    } else {
        // Gaussian
        r = TMath::Exp( -u*u/2 );
    }
    return S*r*N;
}

Double_t
FitFunc::fitf_cb_g(Double_t * x, Double_t *par) {
    return cbf<2>(x[0], par[0], par[1], par[2], par[3], 1)
      + TMath::Exp( -pow(par[4] - x[0], 2)/(2*(par[5]*par[5])) )*(par[6]/(par[5]*sqrt(2*M_PI)));
}

double
FitFunc::fitf_cb(Double_t * x, Double_t *par) {
    return cbf<2>(x[0], par[0], par[1], par[2], par[3], 1);
}

double
FitFunc::fitf_rcb(Double_t * x, Double_t *par) {
    return cbf<2>(x[0], par[0], par[1], par[2], par[3], -1);
}

double
FitFunc::fitf_g(Double_t * x, Double_t *par) {
    return TMath::Exp( -pow(par[0] - x[0], 2)/(2*(par[1]*par[1])) )*(par[2]/(par[1]*sqrt(2*M_PI)));
}

#if 0
double
FitFunc::fitf_tukey_window(Double_t * x, Double_t *par) {
    // par[0] -- center
    // par[2] -- \alpha 
    return TMath::Exp( -pow(par[0] - x[0], 2)/(2*(par[1]*par[1])) )*(par[2]/(par[1]*sqrt(2*M_PI)));
}
#endif

static void
_static_value_or_getter( const YAML::Node & cfg
                       , Double_t & staticValueRef
                       , generic_hst_p_getter & getterRef
                       ) {
    if(!cfg.IsScalar()) throw("Scalar expected");
    const std::string asString = cfg.as<std::string>();
    if(std::isdigit(asString[0]) || '.' == asString[0]) {
        staticValueRef = cfg.as<Double_t>();
        getterRef = nullptr;
    } else {
        staticValueRef = std::nan("0");
        getterRef = generic_getter_by_name(cfg.as<std::string>().c_str());
    }
}

FitFunc::FitFunc( const std::string & tag
                , const YAML::Node & cfg)
        : _nPars(0)
        , _tag(tag)
        , _fitFunc(nullptr)
        {
    const std::string strType = cfg["type"].as<std::string>();  // required
    _xlim[0].initValueGetter = &GenericHstParameters::x_low;
    _xlim[1].initValueGetter = &GenericHstParameters::x_up;
    if(cfg["range"]) {
        _static_value_or_getter(cfg["range"][0], _xlim[0].staticValue, _xlim[0].initValueGetter);
        _static_value_or_getter(cfg["range"][1], _xlim[1].staticValue, _xlim[1].initValueGetter);
    }
    #define _M_set_if_given(nm, dftGetter, dftLow, dftUp)                   \
        strcpy(_pars[_nPars].name, #nm );                                   \
        _pars[_nPars].initValueGetter    = &GenericHstParameters:: dftGetter;  \
        _pars[_nPars].valueBoundaries[0] = &GenericHstParameters:: dftLow;  \
        _pars[_nPars].valueBoundaries[1] = &GenericHstParameters:: dftUp;   \
        if(cfg[ #nm ]) {                                                    \
            if(cfg[ #nm ]["value"])                                         \
                _static_value_or_getter(cfg[ #nm ]["value"], _pars[_nPars].staticValue, _pars[_nPars].initValueGetter);  \
            if(cfg[ #nm ]["range"]) {                                       \
                _static_value_or_getter(cfg[ #nm ]["range"][0], _pars[_nPars].limits[0], _pars[_nPars].valueBoundaries[0]);  \
                _static_value_or_getter(cfg[ #nm ]["range"][1], _pars[_nPars].limits[1], _pars[_nPars].valueBoundaries[1]);  \
            }                                                               \
        }                                                                   \
        ++_nPars;
    if(strType == "Gaussian") {
        // set function type
        _f = FitFunc::fitf_g;
        _M_set_if_given(mean, mean, x_low, x_up);
        _M_set_if_given(stddev, stddev, stddev01, stddev10);
        _M_set_if_given(S, S, S05, S15);
    } /*else if(strType == "CrystalBall") {
        _f = fitf_cb;
        // ...
    } else if(strType == "right-crystalball") {
        _f = fitf_rcb;
        // ...
    }*/
    #undef _M_set_if_given
    else {
        throw std::runtime_error(std::string("Unknown fit func. type"));
    }
}

TF1 *
FitFunc::instantiate( const GenericHstParameters & ghp) {
    if(!_fitFunc) {
        char namebf[128];
        snprintf(namebf, sizeof(namebf), "fit_f_%s", _tag.c_str());
        _fitFunc = new TF1( namebf, _f
               , std::isnan(_xlim[0].staticValue)
                    ? (ghp.*_xlim[0].initValueGetter)()
                    : std::isnan(_xlim[0].staticValue)
               , std::isnan(_xlim[1].staticValue)
                    ? (ghp.*_xlim[1].initValueGetter)()
                    : std::isnan(_xlim[1].staticValue)
               , _nPars
            );
        for(Int_t i = 0; i < _nPars; ++i) {
            _fitFunc->SetParName(i, _pars[i].name);
        }
    }
    for(Int_t i = 0; i < _nPars; ++i) {
        _fitFunc->SetParName(i, _pars[i].name);
        _fitFunc->SetParLimits( i
                       , std::isnan(_pars[i].limits[0]) ? (ghp.*_pars[i].valueBoundaries[0])() : _pars[i].limits[0]
                       , std::isnan(_pars[i].limits[1]) ? (ghp.*_pars[i].valueBoundaries[1])() : _pars[i].limits[1]
                       );
        _fitFunc->SetParameter(i, std::isnan(_pars[i].staticValue)
                ? (ghp.*_pars[i].initValueGetter)()
                : _pars[i].staticValue
                );
    }
    return _fitFunc;
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp

