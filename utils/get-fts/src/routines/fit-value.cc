#include "routines/fit-value.hh"

#include "na64util/namedFileIterator.hh"
#include "get-fts-fit-func.hh"

#include <yaml-cpp/yaml.h>

#include <TF1.h>
#include <TProfile.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TString.h>
#include <TPRegexp.h>

namespace na64dp {
namespace getFts {

//
// Procedure class implementation

ValueFittingProcedure::ValueFittingProcedure( const YAML::Node & cfg )
        : FormattedOutputProcedure(cfg, "")
        {
    // Configure fitting with available parameters
    const YAML::Node & fitFuncsNode = cfg["fitFuncs"];
    // iterate over fitting functions defined and fill vector with 
    // configured instances for further fitting
    for(auto it = fitFuncsNode.begin(); it != fitFuncsNode.end(); ++it) {
        const std::string fitFuncName = it->first.as<std::string>();
        fitFIdxs.emplace(fitFuncName, std::make_shared<FitFunc>(fitFuncName, it->second));
    }
}
bool
ValueFittingProcedure::eval_on_object(TObject * obj_, util::StrSubstDict & semantics) {
    // do the fitting
    std::unordered_map<std::string, TFitResultPtr> fitResults;
    TH1 * asTH1Ptr;
    if(!!(asTH1Ptr = dynamic_cast<TH1*>(obj_))) {
        fitResults = fit_histogram<TH1>(asTH1Ptr, fitFIdxs, semantics);
    } /*else if(ROOTClassName == "TProfile") {
        fitResults = _do_fit<TProfile>(objPtr, fitFIdxs, *descRx
                , (cfg["description"] && cfg["description"]["groups"])
                  ? cfg["description"]["groups"]
                  : YAML::Node(YAML::NodeType::Null)
                , semantics);
    }*/
    else {
        char errbf[128];
        snprintf(errbf, sizeof(errbf), "Don't know how to fit values within ROOT"
                " class \"%s\".", obj_->IsA()->GetName());
        throw std::runtime_error(errbf);
    }

    return FormattedOutputProcedure::eval_on_object(obj_, semantics);
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp

