#include <THnBase.h>
#include <THn.h>

#include "routines/nary-ecdf.hh"
#include "na64util/str-fmt.hh"

#include <TSpline.h>
#include "Math/RootFinder.h"

#include <iostream>
#include <exception>
#include <stdexcept>
#include <cassert>

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace getFts {

NAryECDF::NAryECDF(const YAML::Node & cfg)
    : AbstractProcedure(cfg) {
    // ...
}

bool
NAryECDF::eval_on_object(
          TObject * hst_
        , util::StrSubstDict & semantics
        ) {
    THnBase * hst = dynamic_cast<THnBase*>(hst_);
    if(!hst) {
        throw std::runtime_error(util::format("Object \"%s\" is not a"
                    " subclass of THnBase", hst_->GetName()));
    }

    char namebf[128];
    snprintf( namebf, sizeof(namebf), "%s-ecdf", hst->GetName());
    const Int_t nDims = hst->GetNdimensions();
    Int_t nBins[64];  // terminated with "0"
    Double_t ranges[2][64];
    size_t nEls = 1;
    for(Int_t nDim = 0; nDim < nDims; ++nDim) {
        // todo: mem usage might be significantly decreased here, by redicing
        //       number of bins in use to only non-zero ones
        TAxis & ax = *hst->GetAxis(nDim);
        nBins[nDim] = ax.GetNbins();
        ranges[0][nDim] = ax.GetXmin();
        ranges[1][nDim] = ax.GetXmax();
    }
    std::cout << "Info: allocating THnF with dimensions: ";
    for(Int_t nDim = 0; nDim < nDims; ++nDim) {
        if(nDim) std::cout << ",";
        std::cout << nBins[nDim] << " (" << ranges[0][nDim] << ", "
            << ranges[1][nDim] << ")";
    }
    std::cout << ", " << nEls << " elements overall, expected memory usage is "
        << nEls*sizeof(float)/(1024*1024*1024) << " GB." << std::endl;
    THnF * ecdfHst = new THnF(namebf, hst->GetTitle(), nDims, nBins, ranges[0], ranges[1]);
    
    // ...
    //std::vector<float> ecdf(hst->GetNbins()+1);
    Int_t * coord = new Int_t[nDims];
    Long64_t i = 0;
    THnIter iter(hst);
    while ((i = iter.Next(coord)) >= 0) {
        Double_t v = hst->GetBinContent(i);
        // check whether the bin is regular
        bool regularBin = true;
        for (Int_t dim = 0; dim < nDims; dim++) {
            if (coord[dim] < 1 || coord[dim] > hst->GetAxis(dim)->GetNbins()) {
                regularBin = false;
                break;
            }
        }
        // if outlayer, count it with zero weight
        if (!regularBin) v = 0.;
        //ecdf[i + 1] = ecdf[i] + v;
        ecdfHst->SetBinContent(
                  i + 1
                , ecdfHst->GetBinContent(i) + v
                );
    }
    delete [] coord;

    
    return true;
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp



