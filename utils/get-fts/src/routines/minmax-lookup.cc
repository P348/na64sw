#include "routines/minmax-lookup.hh"
#include "TCanvas.h"
#include "get-fts-fit-func.hh"
#include "get-fts-routine.hh"

#include "umff/numeric/gaussian-convolution.hh"
#include "umff/numeric/fdiff.h"
#include "na64util/str-fmt.hh"

#include <TH2.h>
#include <TObject.h>
#include <TH1.h>
#include <TF1.h>
#include <TFitResult.h>

#include <string>
#include <yaml-cpp/yaml.h>

#include <limits>
#include <stdexcept>
#include <cassert>

#include <iostream>  // XXX

namespace na64dp {
namespace getFts {


MinMaxLookup::MinMaxLookup(const YAML::Node & cfg)
        : FormattedOutputProcedure(cfg, ""){
    _variance = cfg["variance"].as<double>();
    _projectionParameters.axis = '\0';
    _projectionParameters.range[0]
        = _projectionParameters.range[1]
        = std::nan("0");
    if(!strncmp( "TH2", cfg["ROOTClassName"].as<std::string>().c_str(), 3 )) {
        if(!cfg["projection"]) throw std::runtime_error("featurePoints1D on TH2 needs"
                    " \"projection\" section");
        std::string projStr = cfg["projection"]["axis"].as<std::string>();
        // TODO make sure it is either x or y
        _projectionParameters.axis = projStr[0];
        if(cfg["projection"]["range"]) {
            _projectionParameters.range[0] = cfg["projection"]["range"][0].as<double>();
            _projectionParameters.range[1] = cfg["projection"]["range"][1].as<double>();
        }
    }
}

MinMaxLookup::~MinMaxLookup() {
    for(auto & p : _rws) {
        delete p.second;
    }
}

MinMaxLookup::ReentrantWs::ReentrantWs(size_t nBins, double variance) {
    dgcWs = na64sw_DiscreteGaussianCnv_init(variance);
    assert(dgcWs);
    srcSamples  = new double [nBins];
    samples     = new double [nBins];
    dSamples1   = new double [nBins];
    dSamples2   = new double [nBins];
}

MinMaxLookup::ReentrantWs::~ReentrantWs() {
    na64sw_DiscreteGaussianCnv_free(dgcWs);
    delete [] srcSamples;
    delete [] samples;
    delete [] dSamples1;
    delete [] dSamples2;
}

// Describes characteristic point on spectrum -- min, max, deflection point
struct FeaturePoint {
    enum {
        kNone = 0, kMin, kMax, kDeflectionRaising, kDeflectionFalling
    } type;
    double x, y;
};

static void 
_print_item_line( std::ostream & os, const std::string & entryFmt
        , unsigned int nSample
        , const FeaturePoint & fpt
        , util::StrSubstDict & semantics
        ) {
    try {
        std::string fptType;
        assert(fpt.type != FeaturePoint::kNone);
        /*if(p.second.type == FeaturePoint::kNone) {
            fptType = "none";
        } else */if(fpt.type == FeaturePoint::kMin) {
            fptType = "min";
        } else if(fpt.type == FeaturePoint::kMax) {
            fptType = "max";
        } else if(fpt.type == FeaturePoint::kDeflectionFalling) {
            fptType = "inflFall";
        } else if(fpt.type == FeaturePoint::kDeflectionRaising) {
            fptType = "inflRaise";
        }
        semantics["pointType"] = fptType;

        char bf[64];

        snprintf(bf, sizeof(bf), "%e", fpt.x);
        semantics["x"] = bf;

        snprintf(bf, sizeof(bf), "%e", fpt.x);
        semantics["y"] = bf;

        snprintf(bf, sizeof(bf), "%u", nSample);
        semantics["nSample"] = bf;
        
        os << na64dp::util::str_subst(entryFmt, semantics);
    } catch(na64dp::errors::StringIncomplete & e) {
        std::cerr << "Available string subst items:" << std::endl;
        for(const auto & p : semantics) {
            std::cerr << " - \"" << p.first << "\" -> \"" << p.second << "\""
                      << std::endl;
        }
        throw;
    }
}

bool
MinMaxLookup::eval_on_object(TObject * obj_, util::StrSubstDict & semantics) {
    int rc;
    TH1 * hst = nullptr;
    // obtain pointer to TH1 instance for the fitting. Few cases are possible:
    // - TObject can be downcasted to TH1 ptr and fitted directly
    // - in case of TH2 we might be interested in x/y projection -- within a
    //   sub-range or all
    if('\0' == _projectionParameters.axis) {
        // downcast TObject to histogram
        hst = dynamic_cast<TH1*>(obj_);
        if(!hst) {
            char errmsg[256];
            snprintf( errmsg, sizeof(errmsg)
                    , "Retireved is of class %s, can't downcast it to TH1"
                      " to perform Gaussian smoothing."
                    , obj_->ClassName() );
            throw std::runtime_error(errmsg);
        }
    } else if('x' == _projectionParameters.axis || 'y' == _projectionParameters.axis) {
        auto hst2D = dynamic_cast<TH2*>(obj_);
        if(!hst2D) {
            char errmsg[256];
            snprintf( errmsg, sizeof(errmsg)
                    , "Retireved is of class %s, can't downcast it to TH2"
                      " to obtain 1D projection and perform Gaussian smoothing."
                    , obj_->ClassName() );
            throw std::runtime_error(errmsg);
        }
        Int_t firstBinN = 0
            , lastBinN = -1;
        if('x' == _projectionParameters.axis) {
            hst2D->GetXaxis()->FindBin(_projectionParameters.range[0]);
            hst2D->GetXaxis()->FindBin(_projectionParameters.range[1]);
            hst = hst2D->ProjectionX("prj-x", firstBinN, lastBinN);
        } else {
            assert('y' == _projectionParameters.axis);
            hst2D->GetYaxis()->FindBin(_projectionParameters.range[0]);
            hst2D->GetYaxis()->FindBin(_projectionParameters.range[1]);
            hst = hst2D->ProjectionY("prj-y", firstBinN, lastBinN);
        }
        hst->SetDirectory(nullptr);
    }
    assert(hst);

    // get or create smoothing workspace
    ReentrantWs * ws = nullptr;
    unsigned int nBins;
    {
        Int_t nBins_ = hst->GetNbinsX();
        if(nBins_ <= 0 ) {
            throw std::runtime_error("Bad number of bins in retrieved TH1 subclass.");
        }
        nBins = static_cast<unsigned int>(nBins_);
        auto it = _rws.find(nBins);
        if(_rws.end() == it) {
            auto ir = _rws.emplace(nBins
                        , new ReentrantWs(nBins, _variance));
            assert(ir.second);
            it = ir.first;
        }
        ws = it->second;
    }
    assert(ws);

    // copy bin content to "source samples" of workspace
    for(unsigned int nBin = 0; nBin < nBins; ++nBin) {
        ws->srcSamples[nBin] = hst->GetBinContent(nBin+1);
    }
    // calc step as diff of first two bin centers (todo: generally, not true for
    // unevenly spaced histograms, but this is the natural restriction for the
    // entire procedure)
    assert(nBins > 2);
    const double step = hst->GetBinCenter(2) - hst->GetBinCenter(1);
    // do smoothing (eval the convolution); last two args defines fadeout near
    // the extremities which we do not apply here.
    na64sw_DiscreteGaussianCnv_eval(ws->dgcWs, ws->srcSamples, ws->samples, nBins
            , NULL, NULL);
    // Run the FDF and get 1st derivative approximation
    rc = na64sw_finite_diff_approx_on_unigrid(1, 2, ws->samples, ws->dSamples1, nBins, step);
    assert(0 == rc);
    rc = na64sw_finite_diff_approx_on_unigrid(2, 2, ws->samples, ws->dSamples2, nBins, step);
    assert(0 == rc);
    // Do lookup
    std::map<unsigned int, FeaturePoint> fPts;
    for(unsigned int nBin = 1; nBin < nBins; ++nBin) {
        const bool deriv1Alterates = !((ws->dSamples1[nBin-1] >= 0) == (ws->dSamples1[nBin] >= 0))
                 , deriv2Alterates = !((ws->dSamples2[nBin-1] >= 0) == (ws->dSamples2[nBin] >= 0))
                 ;
        if(!(deriv1Alterates || deriv2Alterates)) continue;  // not interested
        const double cx = (hst->GetBinCenter(nBin) + hst->GetBinCenter(nBin+1))/2
                   , cy = (hst->GetBinContent(nBin) + hst->GetBinContent(nBin+1))/2
                   ;
        FeaturePoint fPt{.type = FeaturePoint::kNone, .x=cx, .y=cy};
        // check if 1st derivative alterates sign
        if(deriv1Alterates) {
            if(ws->dSamples1[nBin-1] < ws->dSamples1[nBin]) {
                // prev sample negative, this sample positive => this is min.
                fPt.type = FeaturePoint::kMin;
            } else {
                // prev is positive, this is negative => this is max.
                fPt.type = FeaturePoint::kMax;
            }
        }
        if(deriv2Alterates) {
            if(ws->dSamples1[nBin-1] < ws->dSamples1[nBin]) {
                // prev sample negative, this sample positive => raising.
                fPt.type = FeaturePoint::kDeflectionRaising;
            } else {
                // prev is positive, this is negative => falling.
                fPt.type = FeaturePoint::kDeflectionFalling;
            }
        }

        fPts.emplace(nBin, fPt);
    }

    if(!output_enabled()) return true;  // disable further output

    if(config()["maxGaussianFit"] && config()["maxGaussianFit"].IsMap()) {  // "old style"
        std::map<double, std::pair<unsigned int, const FeaturePoint *>> maxByAmp;
        for(const auto & p : fPts) {
            maxByAmp.emplace(p.second.y
                    , std::pair<unsigned int, const FeaturePoint *>(p.first, &p.second));
        }
        // get number of maxs to fit. interpret 0 as "all"
        int nMax2Fit
            = config()["maxGaussianFit"]["applyTo"]
            ? config()["maxGaussianFit"]["applyTo"].as<int>()
            : 1;
        if(0 == nMax2Fit) {
            nMax2Fit = std::numeric_limits<int>::max();
        }
        if(nMax2Fit < 0) {
            throw std::runtime_error("Error: negative number of peaks to fit"
                    " specified.");  // TODO: support "least" max?
        }
        int nMaxFitted = 0;
        for(auto it = maxByAmp.rbegin(); it != maxByAmp.rend(); ++it) {
            const auto & p = *it;
            if(p.second.second->type != FeaturePoint::kMax) continue;
            ++nMaxFitted;
            char nmBf[128];

            // TODO: re-use fit-value routine?
            snprintf(nmBf, sizeof(nmBf), "gaus-fit-max-%d", nMaxFitted);
            TF1 * gausFit = new TF1( nmBf, "gaus"
                    , p.second.second->x + config()["maxGaussianFit"]["window"][0].as<float>() 
                    , p.second.second->x + config()["maxGaussianFit"]["window"][1].as<float>()
                    );
            hst->Fit(nmBf, "SQLRM");
            {
                //auto fitResult = fitResultPtr.Get();
                if(config()["maxGaussianFit"]["report"]) {
                    TCanvas * cnv = new TCanvas("cnv", "Value fitting results");
                    cnv->cd();
                    hst->Draw();
                    gausFit->Draw("SAME");
                    std::string fName
                        = util::str_subst( config()["maxGaussianFit"]["report"].as<std::string>().c_str()
                            , semantics
                            );

                    cnv->Print(fName.c_str());

                    delete cnv;
                }
            }

            char strbf[64];
            for(Int_t nPar = 0; nPar < gausFit->GetNpar(); ++nPar) {
                std::string parName = "gaus";
                parName += gausFit->GetParName(nPar);
                snprintf(strbf, sizeof(strbf), "%e", gausFit->GetParameter(nPar));
                semantics[parName] = strbf;

                parName = "gaus";
                parName += gausFit->GetParName(nPar);
                parName += "Err";
                snprintf(strbf, sizeof(strbf), "%e", gausFit->GetParError(nPar));
                semantics[parName] = strbf;
                //std::cout << "  " << gausFit->GetParName(nPar) << ": "
                //    << gausFit->GetParameter(nPar) << " +/- "
                //    << gausFit->GetParError(nPar) << std::endl;
            }

            _print_item_line(_os(), get_entry_format()
                    , p.first, *p.second.second, semantics);

            delete gausFit;
            // check exit condition
            if(nMaxFitted >= nMax2Fit) break;
        }
        if(maxByAmp.empty()) {
            std::cerr << "Warning: no max found in hist!" << std::endl;
        }
    } else if(config()["fitFunctions"]) {  // "new style"
        throw std::runtime_error("TODO: generic features fitting procedure");
        //fit_histogram( hst
        //        , std::map<std::string, std::shared_ptr<FitFunc>> &fitFuncs
        //        , na64dp::util::StrSubstDict &semantics
        //        );
    } else {  // no fitting
        for(const auto & p : fPts) {
            _print_item_line(_os(), get_entry_format()
                    , p.first, p.second, semantics);
        }
    }
    return true;
    // forward to parent to print the formatted output and return the reslt
    // code
    //return FormattedOutputProcedure::eval_on_object(obj_, semantics);
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp


