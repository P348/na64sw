#include "routines/quantile-1d.hh"
#include "na64util/str-fmt.hh"

#include <TSpline.h>
#include <TH1.h>
#include <Math/RootFinder.h>

#include <exception>
#include <stdexcept>
#include <cassert>

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace getFts {

QuantileFunction::QuantileFunction(const YAML::Node & cfg)
    : FormattedOutputProcedure(cfg
            , ""  // no default formatting
            ) {
    if(cfg["quantiles"]) {
        if(!cfg["quantiles"].IsSequence()) {
            throw std::runtime_error("\"quantiles\" must be a list.");
        }
        const YAML::Node & n = cfg["quantiles"];
        for(auto it = n.begin(); it != n.end(); ++it) {
            float q = it->as<float>();
            if(q <= 0. || q >= 1) {
                throw std::runtime_error(util::format("Bad element #%zu in"
                            " \"quantiles\" list (should be 0 < q < 1)"
                            , _quantiles.size() ));
            }
            _quantiles.push_back(q);
        }
    }
}

// helper struct, a wrapper over TSpline to provide call operator
struct SplineWrapper {
    TSpline * s;
    Double_t q;
    Double_t operator()(Double_t x) {
        assert(s);
        return s->Eval(x) - q;
    }
};

float
QuantileFunction::_locate_quantile(TH1 * hst, float q) {
    Double_t * integral = hst->GetIntegral();
    Double_t * lowerBound = TMath::BinarySearch( integral
                       , integral + hst->GetNbinsX()
                       , q
                       );
    assert(lowerBound);
    size_t nBin = lowerBound - integral;
    assert(nBin > 2);
    assert(nBin < (size_t) hst->GetNbinsX());
    Double_t interpX[5], interpY[5];
    for(size_t i = nBin - 2; i < nBin + 3; ++i) {
        size_t idx = i + 2 - nBin;
        interpX[idx] = hst->GetBinCenter(i+1);
        interpY[idx] = integral[i];
    }
    TSpline3 * spl = new TSpline3("spl", interpX, interpY, 5);
    ROOT::Math::RootFinder rf;
    SplineWrapper sw{spl, q};
    if(!rf.Solve(sw, interpX[0], interpX[4])) {
        delete spl;
        return std::nan("0");
    }
    delete spl;
    Double_t r = rf.Root();
    return r;
}

bool
QuantileFunction::eval_on_object(
          TObject * hst_
        , util::StrSubstDict & semantics
        ) {
    TH1 * hst = dynamic_cast<TH1*>(hst_);
    if(!hst) {
        throw std::runtime_error(util::format("Object \"%s\" is not a"
                    " subclass of TH1", hst_->GetName()));
    }
    #if 0  // not needed for quantiles lookup
    TH1 * cdf = hst->GetCumulative();
    #ifndef NDEBUG  // assure I understood ROOT functions right...
    Double_t * integral = hst->GetIntegral();
    for (Int_t i = 1; i <= cdf->GetNbinsX(); ++i) {
       assert(std::abs(integral[i] * hst->GetEntries() - cdf->GetBinContent(i)) < 1e-7);
    }
    #endif
    cdf->Scale(1./hst->GetEntries());
    #endif
    int nQ = 0;
    char nm[32], val[32];
    for(float q : _quantiles) {
        ++nQ;
        float r = _locate_quantile(hst, q);
        snprintf(nm,  sizeof(nm),  "q%d", nQ);
        snprintf(val, sizeof(val), "%e",  r );
        semantics.emplace(nm, val);
    }
    // Generate formatted output
    FormattedOutputProcedure::eval_on_object(hst_, semantics);
    return true;
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp


