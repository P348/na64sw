#include <TPRegexp.h>
#include <TObjString.h>

#include "get-fts-TObject-traits.hh"
#include "get-fts-routine.hh"
#include "na64util/namedFileIterator.hh"

#include <stdexcept>
#include <string>
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>

namespace na64dp {
namespace getFts {

//
// Abstract procedure

TFile &
AbstractProcedure::_current_TFile() {
    if(!_cFilePtr) throw std::runtime_error("No TFile associated.");
    return *_cFilePtr;
}

size_t
AbstractProcedure::eval_on_file(const std::string & filePath, TFile * rootFile) {
    _cFilePtr = rootFile;
    size_t objCount = 0;
    // Strings containing semantical regexps
    std::string nameRxs = config()["nameLookup"].IsMap()
                        ? config()["nameLookup"]["expression"].as<std::string>()
                        : config()["nameLookup"].as<std::string>()
              , descRxs
              ;
    // Init description regex, if provided by config (might not be used for
    // some TObject descendants, though)
    if(config()["description"]) {
        if(config()["description"].IsMap()) {
            descRxs = config()["description"]["expression"].as<std::string>();
        } else {
            descRxs = config()["description"].as<std::string>();
        }
    }
    // Ptr to regular expression to match object description string. Note,
    // that not every TObject descendant class has description, though (so
    // this ptr can be null)
    TPRegexp * descRx = nullptr;
    if(!descRxs.empty()) descRx = new TPRegexp(descRxs);
    const std::string ROOTClassName = config()["ROOTClassName"].as<std::string>();
    TObjArray * nmSubs;
    // Output format string
    //const std::string outFmtStr = cfg["output"]["format"].as<std::string>();
    // Create TFile iterator to obtain items
    na64dp::util::NamedFileIterator it(rootFile
            , ROOTClassName.c_str()
            , nameRxs.c_str()
            , nullptr  //&std::cout
            );

    TObject * objPtr;
    while((objPtr = it.get_match(&nmSubs))) {
        // Instantiate string substitution dictionary to fill with name-matching
        // groups
        na64dp::util::StrSubstDict semantics;
        semantics["filePath"] = filePath;
        // append subst dict with items obtained from name matching regex
        if(config()["nameLookup"].IsMap() && config()["nameLookup"]["groups"]) {
            const YAML::Node & groups = config()["nameLookup"]["groups"];
            for(auto it = groups.begin(); it != groups.end(); ++it) {
                std::string groupName(it->first.as<std::string>());
                int nGroup = it->second.as<int>();
                if(nGroup >= nmSubs->GetSize() || nGroup < 0) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression results"
                            " in only %d groups while #%d requested for"
                            " \"%s\"", nmSubs->GetSize(), nGroup
                            , groupName.c_str()));
                }
                TObjString * tStr = nullptr;
                if(nmSubs->At(nGroup)) {
                    tStr = dynamic_cast<TObjString *>(nmSubs->At(nGroup));
                    assert(tStr);
                    assert(tStr->GetString().Data());
                }
                auto ir = semantics.emplace(groupName
                            , tStr ? tStr->GetString().Data() : ""
                            );
                if(!ir.second) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression"
                            " group \"%s\" has non-unique number", groupName.c_str()));
                }
            }
        }
        // delete name matching results as TPRegexp::MatchS() results are owned
        // by user according to docs
        delete nmSubs;  // owned by user
        // extract semantics from object description, if available
        if(descRx) {
            const char * descStr = get_description( ROOTClassName, objPtr );
            if(!descStr) {
                throw std::runtime_error(na64dp::util::format("Objects of ROOT class"
                            " %s do not provide description string"
                            , ROOTClassName.c_str() ));
            }
            TObjArray * subs = descRx->MatchS(descStr);
            if( (!subs) || !subs->At(1) ) {
                throw std::runtime_error(na64dp::util::format("Description string \"%s\" of"
                            " object \"%s\" does not match description lookup"
                            " expression (\"%s\").", descStr, objPtr->GetName()
                            , descRxs.c_str() ));
            }
            const YAML::Node & descGroups = (config()["description"]
                                    && config()["description"]["groups"])
                      ? config()["description"]["groups"]
                      : YAML::Node(YAML::NodeType::Null);
            for(auto it = descGroups.begin(); it != descGroups.end(); ++it) {
                std::string groupName(it->first.as<std::string>());
                int nGroup = it->second.as<int>();
                if(nGroup >= subs->GetSize() || nGroup < 0) {
                    throw std::runtime_error(na64dp::util::format("Description matching"
                            " expression results in only %d groups while #%d requested"
                            " for \"%s\"", subs->GetSize(), nGroup
                            , groupName.c_str()));
                }
                TObjString * tStr = nullptr;
                if(subs->At(nGroup)) {
                    tStr = dynamic_cast<TObjString *>(subs->At(nGroup));
                    assert(tStr);
                }
                auto ir = semantics.emplace(groupName
                            , tStr ? tStr->GetString().Data() : ""
                            );
                if(!ir.second) {
                    throw std::runtime_error(na64dp::util::format("Description matching"
                                " expression group \"%s\" has non-unique number"
                                , groupName.c_str()));
                }
            }
            // delete descr matching results as TPRegexp::MatchS() results are owned
            // by user according to docs
            if(subs) delete subs;
        }
        if(eval_on_object(objPtr, semantics)) ++objCount;
    }  // TObject retrieval loop
    _cFilePtr = nullptr;
    return objCount;
}


//
// (Single) file output procedure

FileOutputProcedure::FileOutputProcedure(const YAML::Node & cfg)
    : AbstractProcedure(cfg)
    , _osPtr(nullptr)
    {
}

size_t
FileOutputProcedure::eval_on_file(const std::string & filePath, TFile * rootFile) {
    std::string outFileName;
    if(config()["output"]) {
        if(config()["output"].IsMap()) {
            outFileName = config()["output"]["path"].as<std::string>();
            if(config()["output"]["format"])
                _outputFormat = config()["output"]["format"].as<std::string>();
        } else if(config()["output"].IsNull()) {
            // do nothing, keep outFileName empty
        } else {
            outFileName = config()["output"].as<std::string>();
        }
    }
    if("-" == outFileName) {
        _osPtr = &std::cout;
    } else {
        auto oflags = std::ios::out;
        if( config()["output"]
         && config()["output"].IsMap()
         && config()["output"]["append"]
         && config()["output"]["append"].as<bool>() ) oflags |= std::ios::app;
        _osPtr = new std::ofstream(outFileName.c_str(), oflags);
        if(_osPtr->fail()) {
            throw std::ios_base::failure(std::strerror(errno));
        }
        //make sure write fails with exception if something is wrong
        _osPtr->exceptions( _osPtr->exceptions()
                          | std::ios::failbit
                          | std::ifstream::badbit
                          );
    }
    size_t itemsCount = AbstractProcedure::eval_on_file(filePath, rootFile);
    if(_osPtr && _osPtr != &std::cout) {
        delete _osPtr;
    }
    return itemsCount;
}

std::ostream &
FileOutputProcedure::_os() {
    if(!_osPtr) {
        throw std::runtime_error("FileOutputProcedure() subclasses must"
                " check `output_enabled() prior of using output stream.");
    }
    return *_osPtr;
}

//
// Formatted output procedure

FormattedOutputProcedure::FormattedOutputProcedure(
                  const YAML::Node & cfg
                , const std::string & defaultFormat
                )
        : FileOutputProcedure(cfg)
        , _defaultFormat(defaultFormat)
        {
    _cFmt = ( config()["output"] && config()["output"].IsMap() && config()["output"]["format"] )
          ? config()["output"]["format"].as<std::string>()
          : _defaultFormat;
    if(_cFmt.empty()) {
        throw std::runtime_error("Empty format string for formatted file"
                " output procedure.");
    }
}

bool
FormattedOutputProcedure::eval_on_object( TObject *
                                        , util::StrSubstDict & semantics
                                        ) {
    if(!output_enabled()) return true;
    try {
        _os() << na64dp::util::str_subst(_cFmt, semantics);
    } catch(na64dp::errors::StringIncomplete & e) {
        std::cerr << "Available string subst items:" << std::endl;
        for(const auto & p : semantics) {
            std::cerr << " - \"" << p.first << "\" -> \"" << p.second << "\""
                      << std::endl;
        }
        throw;
    }
    return true;
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp

