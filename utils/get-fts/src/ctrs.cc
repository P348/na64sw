#include "routines/quantile-1d.hh"
#include "routines/nary-ecdf.hh"
#include "routines/fit-value.hh"
#include "routines/minmax-lookup.hh"

#include <memory>
#include <stdexcept>

namespace na64dp {
namespace getFts {

std::shared_ptr<na64dp::getFts::AbstractProcedure>
AbstractProcedure::instantiate(const YAML::Node & cfg) {
    std::string routineName = cfg["routine"].as<std::string>();
    if(routineName == "quantile-1d") {
        return std::make_shared<QuantileFunction>(cfg);
    } else if( routineName == "NAryECDF" ) {
        return std::make_shared<NAryECDF>(cfg);
    } else if( routineName == "fitDistribution" ) {
        return std::make_shared<ValueFittingProcedure>(cfg);
    } else if( routineName == "featurePoints1D" ) {
        return std::make_shared<MinMaxLookup>(cfg);
    }
    // ...
    else {
        throw std::runtime_error(util::format("No routine named \"%s\""
                    , routineName.c_str() ));
    }
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp

