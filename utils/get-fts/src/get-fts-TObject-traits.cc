#include "get-fts-TObject-traits.hh"
#include "TNamed.h"

#include <cassert>

namespace na64dp {
namespace getFts {

const char *
get_description( const std::string & rootClassName
               , TObject * obj_
               ) {
    assert(obj_);
    assert(!rootClassName.empty());
    TNamed * obj = dynamic_cast<TNamed*>(obj_);
    if(!obj) return nullptr;
    return obj->GetTitle();
}

}  // namespace ::na64dp::util
}  // namespace na64dp

