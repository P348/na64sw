#include "na64util/namedFileIterator.hh"

#include "get-fts-routine.hh"

#include <RtypesCore.h>
#include <THnSparse.h>
#include <TObjArray.h>
#include <TProfile.h>
#include <TString.h>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <TH2F.h>
#include <TF1.h>
#include <TObjString.h>
#include <TFitResult.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TMath.h>

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/type.h>
#include <yaml-cpp/yaml.h>

#include <iostream>

/* Util 
 */

static void
print_usage( const std::string & appName
           , std::ostream & os ) {
    os << "Usage:" << std::endl
       << "    $ " << appName << " <procedure> <config.yaml> <processed-file.root>" << std::endl
       << "Applications reads histograms data from <processed-file.root>"
          " and performs data extraction according to <procedure> using configuration"
          "  provided in <config.yaml>."
       << std::endl;
}

int 
main(int argc, char * argv[]) {
    if( 4 != argc ) {
        std::cerr << "Bad number of arguments (expected 3 or 4)." << std::endl;
        print_usage( argv[0], std::cerr );
        exit(EXIT_FAILURE);
    }

    // Load to the YAML::Node object describing pipeline
    YAML::Node cfg;
    try {
        cfg = YAML::LoadFile( argv[2] );
    } catch( std::exception & e ) {
        std::cerr << "Error occured while accessing, reading or parsing file \""
                  << argv[2]
                  << "\": "
                  << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    TFile * f = TFile::Open( argv[3], "READ" );
    if( !f ) {
        std::cerr << "Failed to instantiate ROOT file \"" << argv[3]
                  << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string procedure = argv[1];
    #if 0
    if(!strcmp(argv[1], "pedestals-depr")) {
        return _get_pedestals(cfg["pedestals"], f);
    } else if(!strcmp(argv[1], "dft-zeros")) {
        return _get_fft_zeros(cfg["dft-zeros"], f);
    } else if(!strcmp(argv[1], "dft-ecdf")) {
        return _get_dft_ecdf(cfg["dft-ecdf"], f);
    } else if(cfg[routine]) {
        return _single_value_fit(cfg[routine], f);
    } else
    #else
    if(!cfg[procedure])
    #endif
    // ...
    {
        std::cerr << "Error: no config section named \"" << procedure << "\""
            << std::endl;
        return EXIT_FAILURE;
    }

    auto proc = na64dp::getFts::AbstractProcedure::instantiate(cfg[procedure]);
    assert(proc);
    size_t nEntriesProcessed = proc->eval_on_file(argv[3], f);
    if(!nEntriesProcessed) {
        std::cerr << "No entries has been processed." << std::endl;
    }

    return EXIT_SUCCESS;
}

