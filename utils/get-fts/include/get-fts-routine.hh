#pragma once

#include "na64util/str-fmt.hh"

#include <yaml-cpp/node/node.h>

class TObject;  // fwd
class TFile;  // fwd
class TPRegexp;  // fwd

namespace na64dp {
namespace util {
class NamedFileIterator;  // fwd
}  // namespace ::na64dp::util
namespace getFts {

/**\brief Abstract base for feature acquizition routine 
 *
 * This class typically gets instantiated once per application run, working on
 * a certain ROOT TObject subclass. Its purpose is to implement basic item
 * retrieval logic for descendants.
 *
 * It provides basic handling of semantics object's name and (optionally)
 * description via common regular expressions from the config file.
 *
 * \note Class instance owns the copy to YMAL node provided in ctr.
 * */
class AbstractProcedure {
private:
    /// Gets set only while `eval_on_object()` is running, managed by this
    /// instance.
    TFile * _cFilePtr;
protected:
    /// Meant to be used by descendant classes to access current file object
    TFile & _current_TFile();
    /// Meant to be used by descendant classes to access current config object
    const YAML::Node _cfg;
public:
    /// Ctr receiving ref to routine's config (copied)
    AbstractProcedure(const YAML::Node & cfg)
        : _cfg(cfg)
        {}
    /// Returns ref to routine's config
    ///
    /// Expects the config to provide
    /// following parameters:
    ///
    /// * `ROOTClassName` should be one of the TObject descendants to work
    ///   with
    /// * `nameLookup` object shall consist of `expression` regexp string and
    ///   optionally `groups` dictionary of named group numbers
    /// * `description` object is optional (can be utilized by only the classes
    ///   that do provide `GetTitle()` or similar method) with `expression` and
    ///   `groups`
    const YAML::Node & config() const { return _cfg; }
    ///\brief Procedure entry point
    ///
    /// Iterates over objects found in file.
    ///
    /// Re-creates and re-fills semantics object each time a new instance is
    /// found in a file, respecting matching groups. After that a single object
    /// treatment is delegated to `eval_on_object()` method.
    ///
    /// Semantics will be appended with `"filePath"` entry referring to a path of
    /// file given as 1st argument.
    ///
    /// \returns Number of entries for which `eval_on_object()` returned `true`
    virtual size_t eval_on_file(const std::string & filePath, TFile *);

    /// Extension working with individual object must implement this.
    virtual bool eval_on_object(TObject *, util::StrSubstDict &) = 0;

    static std::shared_ptr<na64dp::getFts::AbstractProcedure> instantiate(const YAML::Node &);
};  // class AbstractProcedure


/**\brief Procedure generating native single file output
 *
 * Procedure may use this file to generate ASCII files (in CSV or CSV-like
 * form, etc). This class expects procedure config object to provide `"output"`
 * section (see ctr doc).
 * */
class FileOutputProcedure : public AbstractProcedure {
private:
    /// Used stream ptr (can be null)
    std::ostream * _osPtr;
    /// Output format; can be empty if subclass has its own output format
    std::string _outputFormat;
protected:
    bool output_enabled() const { return _osPtr != nullptr; }
    std::ostream & _os();
public:
    ///\breif Ctr, expecting `"output"` section in the config
    ///
    /// The `"output"` can be a string and then it gets intepreted as output
    /// file path. In case file exists it will be truncated.
    /// 
    /// If `"output"` is given as YAML `null`, then output is suppressed.
    ///
    /// If `"output"` is given as YAML map it should provide `"path"` entry
    /// which can be `null` (output is suppressed, used for quick edits), or
    /// string denoting the output file desgination path. Section also can
    /// contain `"append": true` option causing the routine to not truncate
    /// the file. Certain routines can rely on the 
    ///
    /// One can put `"-"` string as a path to make the app print to `stdout`
    /// instead of a file.
    ///
    /// \todo Support numerical fds?
    FileOutputProcedure(const YAML::Node &);
    ///\brief Appends default implementation with file management.
    ///
    /// Opens file, forwards to `AbstractProcedure::eval_on_file()`, closes the
    /// file if need.
    size_t eval_on_file(const std::string & filePath, TFile *) override;
};  // class FileOutputProcedure


/**\brief Procedure generating output of the format configured using std
 *        semantics
 *
 * Appends `FileOutputProcedure` requiring `"output"` to provide also `"format"`
 * string that must rely on the string-substitution of standard
 * `na64dp::format()` function.
 * */
class FormattedOutputProcedure : public FileOutputProcedure {
private:
    /// Default format to be used, if not given in config.
    const std::string _defaultFormat;
    /// Format template string currently in use
    std::string _cFmt;
public:
    ///\breif Ctr, expecting `"format"` string in `"output"` section in the config
    FormattedOutputProcedure( const YAML::Node &
                            , const std::string & defaultFormat
                            );
    ///\brief Prints new formatted line in the output file
    ///
    /// `semantics` assumed to be filled with results of object processing.
    ///
    /// \note Meant to be overriden by descendant and parent-called at the end
    ///       of subclasses' implementation
    bool eval_on_object(TObject *, util::StrSubstDict &) override;

    const std::string get_entry_format() const { return _cFmt; }
};  // class FormattedOutputProcedure

}  // namespace ::na64dp::getFts
}  // namespace na64dp

