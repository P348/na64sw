#pragma once

#include "get-fts-routine.hh"

namespace na64dp {
namespace getFts {

class FitFunc;

/**\brief Fits 1D distribution or profile for series of histograms
 *
 * Provides versataile TH1/TProfile fitting routine, capable to fit data
 * within TFile based on type matching and regular expression. Use cases:
 *  - per-detector fitting of pedestals (even, odd)
 *  - fitting of linear sums for zero suppression
 *  - various othe cases of per-detector binned data of normal, lognormal,
 *    Landau-like etc distributions
 *  - automated fitted of energy deposition most prob. peaks for calibs
 * Resulting CSV output can be used in many applications.
 * */
class ValueFittingProcedure : public FormattedOutputProcedure {
protected:
    std::map< std::string
            , std::shared_ptr<FitFunc>
            > fitFIdxs;
public:
    ValueFittingProcedure(const YAML::Node &);
    bool eval_on_object(TObject * obj_, util::StrSubstDict & semantics) override;
};  // class ValueFittingProcedure

}  // namespace ::na64dp::getFts
}  // namespace na64dp

