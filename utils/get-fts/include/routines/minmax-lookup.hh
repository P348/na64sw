#pragma once

#include "get-fts-routine.hh"

#include <na64/libna64utils-config.h>
#include <unordered_map>

struct na64sw_DiscreteGaussianCnvWorkspace;

namespace na64dp {
namespace getFts {

/**\brief Finds peaks on TH1 instances
 *
 * Applies Gaussian smoothing to spectrum of certain quantity, provided as
 * an instance of TH1 subclass, searches the min, max, deflection points using
 * finite difference derivative approximation.
 *
 * Optionally, can fit leading maximum with Gaussian model within specified
 * range (as it is very frequent need).
 *
 * \note This procedure prints the output for every feature point found: max,
 *       min, inflection points, not once per item.
 *
 * Injected substitution variables:
 *  - `pointType` is a string, one of: "min", "max", "inflFall", "inflRaise".
 *    Appearing of "none" string indicates error.
 *  - `x` argument value of the point (bin edge)
 *  - `y` histogram value of the point (bin width 1/2)
 *  - `nSample` number of bin of the characteristic point
 *
 * If `maxGaussianFit` is enabled and fit converged, `x`, `y` become the
 * estimates of the fitted function and additional substitution variables
 * appear:
 *  - `gausWidth` estimate of Gaussian width (\f$\sigma\f$)
 *  - `gausS` estimate of renormalization value for Gaussian PDF
 *
 * \todo `x`/`y` are not precise; apply b-spline interpolation here or something...
 * */
class MinMaxLookup : public FormattedOutputProcedure {
private:
    /// Type of reentrant workspace instance, parameterised with number of bins
    struct ReentrantWs {
        ReentrantWs(size_t nBins, double variance);
        ~ReentrantWs();
        na64sw_DiscreteGaussianCnvWorkspace * dgcWs;
        double * srcSamples
             , * samples
             , * dSamples1
             , * dSamples2
             ;
    };
protected:
    /// Gaussian variance factor used for smoothing (in histogram's units)
    double _variance;
    /// Dict of reentrant smoothing workspace instances
    std::unordered_map<unsigned int, ReentrantWs *> _rws;
    struct {
        unsigned char axis;  ///< 'x' for hor axis, 'y' for vertical
        float range[2];  ///< set to non-finite numbers means in a whole region
    } _projectionParameters;  ///< makes sense only for TH2
public:
    MinMaxLookup(const YAML::Node &);
    ~MinMaxLookup();
    bool eval_on_object(TObject * obj_, util::StrSubstDict & semantics) override;
};  // class ValueFittingProcedure

}  // namespace ::na64dp::getFts
}  // namespace na64dp


