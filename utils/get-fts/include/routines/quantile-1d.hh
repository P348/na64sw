#pragma once

#include "get-fts-routine.hh"

class TH1;  // fwd

namespace na64dp {
namespace getFts {

// ...?

/**\brief Considers given histogram object and returns quantile thresholds
 *
 * Assumes given object is of `TH1*` or `TProfile` subclasses. Computes
 * ECDF for it and derives thresholds for quantiles.
 *
 * Quantiles should be provided as floating values within a list
 * named `"quantiles"` in the config file.
 * */
class QuantileFunction : public FormattedOutputProcedure {
private:
    std::vector<float> _quantiles;
    //TFile * _outFile;

    float _locate_quantile(TH1 * hst, float q);
public:
    QuantileFunction(const YAML::Node & cfg);
    bool eval_on_object(TObject * obj_, util::StrSubstDict & semantics) override;
};  // class QuantileFunction

}  // namespace ::na64dp::getFts
}  // namespace na64dp

