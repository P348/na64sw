#pragma once

#include "get-fts-routine.hh"

namespace na64dp {
namespace getFts {

/**\brief Uses THnSparse to produce ECDF
 *
 * Assumes given object is of `THnSparse*` subclasses. Computes n-ary ECDF for
 * it.
 * */
class NAryECDF : public AbstractProcedure {
private:
public:
    NAryECDF(const YAML::Node & cfg);
    bool eval_on_object(TObject * obj_, util::StrSubstDict & semantics) override;
};  // class QuantileFunction

}  // namespace ::na64dp::getFts
}  // namespace na64dp


