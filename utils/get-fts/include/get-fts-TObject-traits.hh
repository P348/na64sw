#pragma once

#include <string>

class TObject;  // fwd

namespace na64dp {
namespace getFts {

#if 0
template<typename T>
struct TObjectTraits;

template<>
struct TObjectTraits<TH1> {
    static const char * description_string( TH1 * ) 
};
#endif

const char * get_description(const std::string & rootClassName, TObject *);

}  // namespace ::na64dp::util
}  // namespace na64dp

