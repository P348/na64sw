#pragma once

#include "na64util/str-fmt.hh"

#include <TH1.h>
#include <TF1.h>
#include <TProfile.h>
#include <TFitResult.h>

#include <yaml-cpp/yaml.h>  // TODO: fwd?

namespace na64dp {
namespace getFts {

///\brief Programmatic interface to query general 1D histogram features
///
/// Used to address key features from 1D histogram: mean, limits, etc.
class GenericHstParameters {
private:
    Double_t _mean, _stddev, _S, _xlim[2], _maxValue, _nEntries;
public:
    GenericHstParameters( TH1* hst) {
        _mean = hst->GetMean();
        _stddev = hst->GetStdDev();
        _S = hst->GetSumOfWeights()*hst->GetXaxis()->GetBinWidth(1);
        _xlim[0] = hst->GetXaxis()->GetXmin();
        _xlim[1] = hst->GetXaxis()->GetXmax();
        _maxValue = hst->GetMaximum();
        _nEntries = hst->GetEntries();
    }
    GenericHstParameters(TProfile*);  // TODO

    Double_t mean() const       { return _mean; }
    Double_t stddev() const     { return _stddev; }
    Double_t S() const          { return _S; }
    Double_t x_low() const      { return _xlim[0]; }
    Double_t x_up() const       { return _xlim[1]; }
    Double_t max_value() const  { return _maxValue; }
    Double_t n_entries() const    { return _nEntries; }

    Double_t mu_minus_sigma_or_x_low() const
        { return _mean - _stddev > _xlim[0] ? _mean - _stddev : _xlim[0]; }
    Double_t mu_minus_15sigma_or_x_low() const
        { return _mean - 1.5*_stddev > _xlim[0] ? _mean - 1.5*_stddev : _xlim[0]; }
    Double_t mu_minus_3sigma_or_x_low() const
        { return _mean - 3*_stddev > _xlim[0] ? _mean - 3*_stddev : _xlim[0]; }

    Double_t mu_plus_sigma_or_x_up() const
        { return _mean + _stddev > _xlim[1] ? _mean + _stddev : _xlim[1]; }
    Double_t mu_plus_15sigma_or_x_up() const
        { return _mean + 1.5*_stddev > _xlim[1] ? _mean + 1.5*_stddev : _xlim[1]; }
    Double_t mu_plus_3sigma_or_x_up() const
        { return _mean + 3*_stddev > _xlim[1] ? _mean + 3*_stddev : _xlim[1]; }

    Double_t stddev01() const  {  return  0.1*_stddev; }
    Double_t stddev033() const {  return 0.33*_stddev; }
    Double_t stddev10() const  {  return   10*_stddev; }
    Double_t stddev3() const   {  return    3*_stddev; }

    Double_t S01() const { return 0.1*_S; }
    Double_t S05() const { return 0.5*_S; }
    Double_t S10() const { return 10*_S; }
    Double_t S15() const { return 1.5*_S; }
    Double_t S3() const  { return 3*_S; }
};

typedef Double_t (GenericHstParameters::*generic_hst_p_getter)() const;

/// Getters indexed by name, for runtime query
extern const struct HstGetterDefinition {
    char name[64];
    generic_hst_p_getter getter;
} gGenericHstParametersGetters[];

///\brief Fit function wrapper relying on distribution features
class FitFunc {
public:
    // set of pretty common models
    /// Sum of CB and Gaussian
    /// \todo some sort of CrystallBall function
    static Double_t fitf_cb_g(Double_t * x, Double_t *par);
    /// Classic CrystallBall function
    static double fitf_cb(Double_t * x, Double_t *par);
    /// Reverse CrystallBall function
    static double fitf_rcb(Double_t * x, Double_t *par);
    /// Gaussian function
    static double fitf_g(Double_t * x, Double_t *par);
    /// Tukey window fit function (cosine-tapered window function)
    ///
    /// Commonly used in FFT as widnow function. Practicaly useful
    /// for fitting meander-like functions (close to square pulse) with diffuse
    /// edges.
    //static double fitf_tukey_window(Double_t * x, Double_t *par);
private:
    /// Fit function callback
    Double_t (*_f)(Double_t *, Double_t *);
    /// Parameters list
    struct Par {
        char name[16];
        Double_t staticValue, limits[2];
        generic_hst_p_getter initValueGetter, valueBoundaries[2];
        Par() : name("")
              , staticValue(std::nan("0"))
              , limits{std::nan("0"), std::nan("0")}
              , initValueGetter(nullptr)
              , valueBoundaries{nullptr, nullptr}
              {}
    } _pars[16], _xlim[2];
    int _nPars;
    const std::string _tag;
    TF1 * _fitFunc;
public:
    FitFunc( const std::string & tag
           , const YAML::Node & cfg);

    const std::string tag() const { return _tag; }

    TF1 * instantiate( const GenericHstParameters & ghp);
};  // class FitFunc

///\brief Generic fitting routine performing series of fit with different
///       models
///
/// For given histogram `hst` runs a sequence of named fitting attempts as
/// defined in `fitFuncs` (models). Prior to the fit the `semantics` dictionary
/// will be populated with `hst` features as defined in `GenericHstParameters`:
///     - `mean`
///     - `stddev`
///     - `S`
///     - `x_low`
///     - `x_up`
///     - `n_entries`
/// In case these definitions were defined in `semantics` they will be
/// re-written.
///
/// Given `semantics` map can contain extra definitions prior the fit -- they
/// can be used and will be re-written in case of collisions. This map also
/// can be empty. It is possible to re-use results of previous fit attempts
/// with next ones -- one can thus construct elaborated scenarios. Caveat:
/// anticipate that "previous" fit may fail (its parameters will be NaN).
template<typename T> std::unordered_map<std::string, TFitResultPtr>
fit_histogram( T * hst
       , std::map< std::string
                      , std::shared_ptr<FitFunc>
                      > & fitFuncs
       //, TPRegexp & descRx, const YAML::Node & descGroups
       , na64dp::util::StrSubstDict & semantics
       , const char * fitOpts="SQLN"
       ) {

    // Get generic parameters from histogram -- populate semantics dictionary
    // based on histogram parameters
    GenericHstParameters genPars(hst);
    char valuebf[64];
    #define _M_set_hist_p(nm)                                       \
    snprintf(valuebf, sizeof(valuebf), "%e", genPars. nm ());    \
    semantics.emplace("hist." #nm, valuebf)
    _M_set_hist_p(mean);
    _M_set_hist_p(stddev);
    _M_set_hist_p(S);
    _M_set_hist_p(x_low);
    _M_set_hist_p(x_up);
    _M_set_hist_p(n_entries);
    #undef _M_set_hist_p

    // try to perform the fit with all proposed functions
    std::unordered_map<std::string, TFitResultPtr> r;
    for(auto & fitFuncPair: fitFuncs) {
        TFitResultPtr frPtr;
        TF1 * rootTF1 = fitFuncPair.second->instantiate(genPars);
        frPtr = hst->Fit( rootTF1
                , fitOpts
                , ""
                );
        r.emplace(fitFuncPair.first, frPtr);
        char parNameBf[128], valueBf[64];
        const bool isGood = frPtr->IsValid();
        // populate parameters into output map
        for(Int_t nPar = 0; nPar < rootTF1->GetNpar(); ++nPar) {
            snprintf(parNameBf, sizeof(parNameBf), "%s.%s"
                    , fitFuncPair.second->tag().c_str()
                    , rootTF1->GetParName(nPar)
                    );
            snprintf( valueBf, sizeof(valueBf), "%e"
                    , isGood ? rootTF1->GetParameter(nPar) : std::nan("0")
                    );
            semantics[parNameBf] = valueBf;
        }
    }
    return r;
}

}  // namespace ::na64dp::getFts
}  // namespace na64dp

