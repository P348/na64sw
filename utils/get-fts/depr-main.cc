
//
// Pedestals extraction routine {{{
static int
_get_pedestals(const YAML::Node & cfg, TFile * f) {
    const std::string nameRxs = cfg["lookupRegex"].as<std::string>()
                    , descRxs = cfg["descriptionRegex"].as<std::string>()
                    ;

    std::cout << "Name lookup regex in use: " << nameRxs << std::endl
              << "Description regex in use: " << descRxs << std::endl
              ;

    na64dp::util::NamedFileIterator it( f, "TH1F",
            nameRxs.c_str() );

    TPRegexp descRx( descRxs );

    TObject * objPtr;
    size_t objCount = 0;

    // set (open) output stream
    std::ostream * osPtr;
    {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    }
    std::ostream & os = *osPtr;
    bool useExtFmt = "extended" == cfg["output"]["format"].as<std::string>();

    const std::string goodPathPat = cfg["saveHists"]["good"].as<std::string>()
                    , badPathPat = cfg["saveHists"]["bad"].as<std::string>()
                    ;

    os << "#TBName,pedType,";
    if( useExtFmt ) {
        // use extended format for studies
        os << "mean,rms,skewness,kurtosis,"
           << "width,widthErr,"
           << "mpv,mpvE,"
           << "area,areaErr,"
           << "sigma,sigmaErr,"
           << "chi2,ndf"
           ;
    } else {
        // use more laconic form
        os << "pedVal,pedErr,nEntries";
    }
    os << std::endl;
    while((objPtr = it.get_match())) {
        TH1F * hstPtr = dynamic_cast<TH1F*>(objPtr);
        assert( hstPtr );
        
        TObjArray * subs = descRx.MatchS( hstPtr->GetTitle() );
        TString pedType, tbname;
        if (subs->GetLast()+1 > 2) {
            pedType = dynamic_cast<TObjString *>(subs->At(1))->GetString();
            tbname  = dynamic_cast<TObjString *>(subs->At(2))->GetString();
        } else {
            std::cerr << "Description string \"" << hstPtr->GetTitle()
                      << "\" of histogram \"" << hstPtr->GetName()
                      << "\" does not match regex. Ignored." << std::endl;
            continue;
        }

        if( hstPtr->GetSumOfWeights() == 0 ) {
            std::cerr << "Histogram \"" << hstPtr->GetName()
                      << "\" has zero sum of weights, ignored." << std::endl;
            continue;
        }
        ++objCount;

        std::ios_base::fmtflags commonFlags( os.flags() );

        os << tbname << "," << pedType << ","
           << std::scientific;  // <== NOTE this!

        if( useExtFmt ) {
           os << hstPtr->GetMean() << ","
              << hstPtr->GetRMS() << ","
              << hstPtr->GetSkewness() << ","
              << hstPtr->GetKurtosis() << ","
              ;
        }
    
        {
            TCanvas *c = new TCanvas();
            c->SetLogy();
            gStyle->SetOptStat(111111111);  // Set meaningful stats TPaveStat
            gStyle->SetOptFit(1111);

            Double_t 
                     #if 0
                     fitRange[2] = { hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ?   hstPtr->GetRMS()/2 : 30 )
                                   , hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ? 3*hstPtr->GetRMS()   : 100) }
                     #else
                     fitRange[2] = { 10
                                   , 800 }
                     #endif
                   , startValues[4] = { .8  // scaled width (Landau distribution)
                                      , hstPtr->GetMean()  // most prob value
                                      , hstPtr->GetSum()*hstPtr->GetXaxis()->GetBinWidth(1)  // area
                                      , hstPtr->GetStdDev()  // sigma
                                      }
                   , lowParLims[4] = { 1e-6
                                     , hstPtr->GetXaxis()->GetXmin()
                                     , .01*hstPtr->GetSum()*hstPtr->GetXaxis()->GetBinWidth(1)  // area
                                     , .5*hstPtr->GetStdDev()
                                     }
                   , upParLims[4] = { 1.
                                    , hstPtr->GetXaxis()->GetXmax()
                                    , 10*hstPtr->GetSum()*hstPtr->GetXaxis()->GetBinWidth(1)  // area
                                    , 1.5*hstPtr->GetStdDev()
                                    }
                   ;

            //std::cout << "Scaled width: " << lowParLims[0] << ", " << startValues[0] << ", " << upParLims[0] << std::endl
            //          << "         MPV: " << lowParLims[1] << ", " << startValues[1] << ", " << upParLims[1] << std::endl
            //          << "        area: " << lowParLims[2] << ", " << startValues[2] << ", " << upParLims[2] << std::endl
            //          << "       sigma: " << lowParLims[3] << ", " << startValues[3] << ", " << upParLims[3] << std::endl
            //          ;

            bool isGood = true;
            Double_t pars[4], errors[4], chiSq;
            Int_t ndf;
            auto fitResults = na64dp::util::langausfit( hstPtr
                                               , fitRange
                                               , startValues
                                               , lowParLims, upParLims
                                               , NULL
                                               , pars
                                               , errors
                                               , chiSq, ndf
                                               );
            isGood = fitResults.second;
            isGood = false;  // XXX
            //if( chiSq/ndf > 1e2
            // || fabs(hstPtr->GetMean() - pars[1]) > pars[3]*3 ) {
            //    isGood = false;
            //}

            if( useExtFmt ) {
                if( isGood ) {
                    for(int i = 0; i < 4; ++i) {
                        os << pars[i] << "," << errors[i] << ",";
                    }
                } else {
                    for(int i = 0; i < 4; ++i) {
                        os << "nan,nan,";
                    }
                }
                os << chiSq << ","
                   << fitResults.first->GetNDF() << ","
                   << hstPtr->GetEntries();
            } else {
                if( isGood ) {
                    // use MVP of langaus fit as the pedestal value and
                    // sigma + Error_MVP as validity interval
                    os << std::scientific << pars[1] << ","
                       << pars[0] << ",";
                    os.flags( commonFlags );
                    os << hstPtr->GetEntries()
                       ;
                } else if(cfg["fallbackValues"] && cfg["fallbackValues"].as<bool>()) {
                    // use MVP of langaus fit as the pedestal value and
                    // sigma + Error_MVP as validity interval
                    os << std::scientific << hstPtr->GetMean() << ","
                       << hstPtr->GetStdDev() << ",";
                    os.flags( commonFlags );
                    os << hstPtr->GetEntries();
                } else {
                    for(int i = 0; i < 4; ++i) {
                        os << "nan,nan,0,";
                    }
                }
            }

            hstPtr->Draw();
            fitResults.first->Draw("SAME");
            char bf[256];
            const char * pat = goodPathPat.c_str();
            if( !isGood ) {
                pat = badPathPat.c_str();
            }
            snprintf( bf, sizeof(bf), pat
                    , hstPtr->GetName() );
            c->Print( bf );
            delete c;
        }
        os << std::endl;
    }
    if( osPtr != &std::cout ) {
        static_cast<std::ofstream&>(os).close();
        delete osPtr;
    }
    std::cout << objCount << " entries have been processed."
              << std::endl;
    return 0;
}  // }}}

//
// (D)FFT (M)SADC Zero identification {{{

template<int nT>
Double_t
cbf( double x
   , double mu, double sigma
   , double alpha
   , double S
   , int sign  // set to 1 for left, -1 to right
   ) {
    # if 0
    double u   = (x-mu)/width;
    double A2  = TMath::Power(p/TMath::Abs(a),p)*TMath::Exp(-a*a/2);
    double B2  = p/TMath::Abs(a) - TMath::Abs(a);

    double result(1);
    //if      (u<-a1) result *= A1*TMath::Power(B1-u,-p1);
    if (u<a)  result *= s*TMath::Exp(-u*u/2);
    else      result *= A2*TMath::Power(B2+u,-p);
    return result;
    #else
    const Double_t alpha2 = alpha*alpha
                 , absAlpha = fabs(alpha)
                 , sq2 = sqrt(2.)
                 , u = sign*(x - mu)/sigma
                 , C = nT*exp(-alpha2/2)/((absAlpha)*(nT-1))
                 , D = sqrt(M_PI/2)*(1 + TMath::Erf(absAlpha/sq2))
                 , N = 1/(sigma*(C+D))
                 ;
    Double_t r;
    if( u <= -alpha ) {
        // sq tail
        const Double_t A = pow((nT/absAlpha), nT)*exp(-alpha2/2)
                     , B = nT/absAlpha - absAlpha
                     ;
        r = A*TMath::Power((B - u), - nT);
    } else {
        // Gaussian
        r = TMath::Exp( -u*u/2 );
    }
    #endif
    return S*r*N;
}


Double_t
fitf_cb_g(Double_t * x, Double_t *par) {
  return cbf<2>(x[0], par[0], par[1], par[2], par[3], 1)
      + TMath::Exp( -pow(par[4] - x[0], 2)/(2*(par[5]*par[5])) )*(par[6]/(par[5]*sqrt(2*M_PI)));
}

double
fitf_cb(Double_t * x, Double_t *par) {
  return cbf<2>(x[0], par[0], par[1], par[2], par[3], 1);
}

double
fitf_rcb(Double_t * x, Double_t *par) {
  return cbf<2>(x[0], par[0], par[1], par[2], par[3], -1);
}

double
fitf_g(Double_t * x, Double_t *par) {
  return TMath::Exp( -pow(par[0] - x[0], 2)/(2*(par[1]*par[1])) )*(par[2]/(par[1]*sqrt(2*M_PI)));
}

/**\brief Fits DFT amplitude distributions with few functions
 *
 * Designed to extract per-amplitude PDF for (M)SADC detectors for subsequent
 * zero-denial procedures.
 *
 * Output format is JSON.
 */
static int
_get_fft_zeros(const YAML::Node & cfg, TFile * f) {
    const std::string nameRxs = cfg["lookupRegex"].as<std::string>()
                    , descRxs = cfg["descriptionRegex"].as<std::string>()
                    ;

    const std::string className = cfg["className"]
            ? cfg["className"].as<std::string>()
            : "TProfile"
            ;

    std::cout << "Name lookup regex in use ... : " << nameRxs << std::endl
              << "Description regex in use ... : " << descRxs << std::endl
              << "Looking for objects ........ : " << className << std::endl
              ;

    na64dp::util::NamedFileIterator it( f, className.c_str(),
            nameRxs.c_str() );

    TPRegexp descRx( descRxs );

    TObject * objPtr;
    size_t objCount = 0;

    // set (open) output stream
    std::ostream * osPtr;
    {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    }
    std::ostream & os = *osPtr;
    std::ios_base::fmtflags commonFlags( os.flags() );

    const std::string outFormat = "json";   // TODO: configurable

    if(outFormat == "json") os << "{\n  \"detectors\":[\n";
    if(className == "TH2") {
        while((objPtr = it.get_match())) {
            TH2 * hstPtr = dynamic_cast<TH2*>(objPtr);
            assert( hstPtr );
            
            TObjArray * subs = descRx.MatchS( hstPtr->GetTitle() );
            TString tbname, baseFreqLow, baseFreqUp;
            if (subs->GetLast()+1 == 4) {
                tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
                baseFreqLow = dynamic_cast<TObjString *>(subs->At(2))->GetString();
                baseFreqUp  = dynamic_cast<TObjString *>(subs->At(3))->GetString();
            } else if (subs->GetLast()+1 == 2) {
                tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
                baseFreqLow = "0";
                baseFreqUp  = "0";
            } else {
                std::cerr << "Description string \"" << hstPtr->GetTitle()
                          << "\" of TH2 \"" << hstPtr->GetName()
                          << "\" does not match regex. Ignored." << std::endl;
                continue;
            }

            if( hstPtr->GetSumOfWeights() == 0 ) {
                std::cerr << "TH2 \"" << hstPtr->GetName()
                          << "\" has zero sum of weights, ignored." << std::endl;
                continue;
            }

            if(outFormat == "json") {
                if(objCount) os << ",";
                os << "    {\n      \"tbname\":\"" << tbname << "\",\n      \"freqs\":["
                    << baseFreqLow << "," << baseFreqUp << "],\n      \"type\":\""
                    << cfg["type"].as<std::string>() << "\",\n      \"freqFits\":[\n        ";
            } else {
                os << tbname << "," << baseFreqLow << "," << baseFreqUp
                   << "," << cfg["type"].as<std::string>();
            }
            ++objCount;
            // Get profiles from n-th bin by x-axis (freqs) and fit each
            // frequence profile with lognormal-like function. If drawing
            // requested, create a canvas subdivided per channel
            TCanvas * cnv = nullptr;
            if(cfg["reports"]["enable"] && cfg["reports"]["enable"].as<bool>()) {
                size_t nPlots = hstPtr->GetXaxis()->GetNbins()
                     , nRows = floor(sqrt((float) nPlots))
                     , nCols = ceil(((float) nPlots)/nRows);
                assert(nRows*nCols >= nPlots);
                cnv = new TCanvas();
                cnv->Divide(nRows, nCols);
            }
            for(Int_t nBin = 1; nBin < hstPtr->GetXaxis()->GetNbins()+1; ++nBin) {
                if(cnv) cnv->cd(nBin);
                // Get amplitude profile for certain frequency
                char bf[64];
                snprintf(bf, sizeof(bf), "%s-freq%d", tbname.Data(), nBin);
                TH1D * nAmpProfile = hstPtr->ProjectionY(
                        bf, nBin, nBin+1, "");
                Double_t meanAmp = nAmpProfile->GetMean(1)
                       , stdDev = nAmpProfile->GetRMS(1)
                       , S = nAmpProfile->GetSum()*nAmpProfile->GetXaxis()->GetBinWidth(1) //nAmpProfile->Integral()
                       ;
                if(outFormat == "json") {
                    if(nBin != 1) os << ", {\n";
                    else os << "{";
                    os << std::scientific
                       << "           \"meanAmp\":" << meanAmp
                       << ",\n          \"stdDev\":" << stdDev
                       << ",\n          \"sum\":" << S
                       << ",\n          \"fitResults\":{\n            ";
                }
                // Try to fit with crystalball+gauss, crystalball only and
                // gauss only
                struct {
                    char nm[32];
                    Double_t (*ff)(Double_t *, Double_t *);
                    Double_t pValue;
                    Double_t pars[16];
                    char parNames[16][32];
                    Double_t range[2];
                    TF1 * tf1;
                } fitResults[] = {
                      {"gauss",  fitf_g,    0., { meanAmp-stdDev/2, stdDev/1.5, S, std::nan("0") }
                            , { "mean", "sigma", "sum" }
                            , {meanAmp - 1.5*stdDev, meanAmp + 1.5*stdDev}}
                    , {"cb_g",   fitf_cb_g, 0., {/* CB parameters */ meanAmp - stdDev/4, stdDev/2, .99, .85*S,
                                                 /* Additive gaussian parameters */ meanAmp + 1.5*stdDev, 0.4*stdDev, 0.15*S
                                                 , std::nan("0") }
                            , { "cbMean", "cbSigma", "cbAlpha", "cbSum", "mean", "sigma", "sum" }
                            , {meanAmp - 6*stdDev, meanAmp + 6*stdDev}}
                    , {"cb",     fitf_cb,   0., { meanAmp, stdDev/2, .9, S, std::nan("0") }
                            , { "cbMean", "cbSigma", "alpha", "cbSum" }
                            , {meanAmp - 3*stdDev, meanAmp + 3*stdDev}}
                    , {"rcb",    fitf_rcb,  0., { meanAmp-stdDev/2, 2*stdDev/3, .9, .9*S, std::nan("0") }
                            , { "cbMean", "cbSigma", "alpha", "cbSum" }
                            , {meanAmp - stdDev, meanAmp + 3*stdDev}}
                    , {"", NULL}
                };
                int nBestFit = -1, nFit = 0;
                Double_t bestPvalue = 0;
                for(auto fr = fitResults; '\0' != *fr->nm; ++fr, ++nFit ) {
                    int nPar = 0;
                    for(Double_t * p = fr->pars; !std::isnan(*p); ++p, ++nPar) {}
                    fr->tf1 = new TF1(fr->nm, fr->ff
                            , fr->range[0] > nAmpProfile->GetXaxis()->GetXmin() ? fr->range[0] : nAmpProfile->GetXaxis()->GetXmin()
                            , fr->range[1] < nAmpProfile->GetXaxis()->GetXmax() ? fr->range[1] : nAmpProfile->GetXaxis()->GetXmax()
                            , nPar );
                    fr->tf1->SetLineWidth(1);
                    fr->tf1->SetLineColorAlpha(kGreen, .9);
                    nPar = 0;
                    for(Double_t * p = fr->pars; !std::isnan(*p); ++p, ++nPar) {
                        fr->tf1->SetParameter(nPar, *p);
                        fr->tf1->SetParName(nPar, fr->parNames[nPar]);
                    }
                    #if 0
                    // ^^^ set here 1 to see initial
                    nBestFit = 3;
                    #else
                    // Fit amplitude frequency profile with function
                    auto tfitResult = nAmpProfile->Fit(fr->tf1, "SQLN+"
                            , "", fr->range[0], fr->range[1] );  // N for "don't draw"
                    if(outFormat == "json") {
                        if(nFit) os << ",\n            ";
                        os << "\"" << fr->nm << "\":";
                    }
                    if(tfitResult->IsValid()) {
                        fr->pValue = TMath::Prob(tfitResult->Chi2(), tfitResult->Ndf());
                        if(fr->pValue > bestPvalue) {
                            bestPvalue = fr->pValue;
                            nBestFit = nFit;
                            //printf("xxx %s, %s on %d: %e\n", tbname.Data(), fr->nm, nBin, fr->pValue);
                        }
                        if(outFormat == "json") os << "{\n              \"pvalue\":" << fr->pValue << ",\n              \"parameters\":{";
                        // copy parameters of succ. fit
                        nPar = 0;
                        for(Double_t * p = fr->pars; !std::isnan(*p); ++p, ++nPar) {
                            *p = fr->tf1->GetParameter(nPar);
                            if(outFormat == "json") {
                                if(nPar) os << ",";
                                os << "\"" << fr->tf1->GetParName(nPar) << "\":" << *p;
                            }
                        }
                        //std::cout << tbname << ", fit with " << fr->nm << ":"
                        //        << ", p-value=" << fr->pValue << std::endl;
                        if(outFormat == "json") os << "}\n            }";
                    } else {
                        if(outFormat == "json") os << "null";
                        fr->tf1->SetLineColorAlpha(kRed, .9);
                        fr->pValue = std::nan("0");
                        //std::cout << tbname << ", fit with " << fr->nm << ":"
                        //        << ", fit failed." << std::endl;
                    }

                    #endif
                }  // per-fit func. loop
                // draw a winner, if any
                if(cnv) {
                    if(nBestFit != -1) {
                        nAmpProfile->Draw();
                        fitResults[nBestFit].tf1->SetLineColorAlpha(kGreen, .9);
                        fitResults[nBestFit].tf1->Draw("SAME");
                    } else {
                        // 1st is gaussian
                        //std::cout << tbname << ", freq#" << nBin
                        //        << ", setting fallback gaussian: "
                        //        << meanAmp << ", " << stdDev << ", " << S << std::endl;
                        fitResults[0].tf1->SetParameters(meanAmp, stdDev, S);
                        fitResults[0].tf1->SetRange(meanAmp - 3*stdDev, meanAmp + 3*stdDev);
                        fitResults[0].tf1->SetLineColorAlpha(kRed, .9);
                        nAmpProfile->Draw();
                        fitResults[0].tf1->Draw("SAME");
                    }
                }
                // if no "winner" found, use mean and stddev
                Double_t freqMean = meanAmp
                       , freqVar  = stdDev
                       ;
                if(nBestFit != -1) {  // mu, sigma, alpha, S
                    freqMean = fitResults[1].pars[0];
                    freqVar  = fitResults[1].pars[1];
                }
                if(outFormat != "json") {
                    os << "," << std::scientific
                       << freqMean << ","
                       << freqVar;
                } else {
                    os << "\n          }\n        }";  // finalize per-freq object
                }
                os.flags( commonFlags );
            }  // freqs loop
            if(outFormat == "json") os << "\n      ]\n";  // finalize freqs sequence
            if(cnv) {
                char fnameBf[128];
                snprintf( fnameBf, sizeof(fnameBf), "./dft-fit-results/%s.pdf"
                        , hstPtr->GetName() );
                cnv->Print(fnameBf);
                delete cnv;
                cnv = nullptr;
            }

            //os << tbname << "," << pedType << ","
            //   << std::scientific;  // <== NOTE this!
        
            #if 0
            {
                TCanvas *c = new TCanvas();
                c->SetLogy();
                gStyle->SetOptStat(111111111);  // Set meaningful stats TPaveStat
                gStyle->SetOptFit(1111);

                Double_t 
                         #if 0
                         fitRange[2] = { hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ?   hstPtr->GetRMS()/2 : 30 )
                                       , hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ? 3*hstPtr->GetRMS()   : 100) }
                         #else
                         fitRange[2] = { 10
                                       , 600 }
                         #endif
                       , startValues[4] = { 1  // scaled width (Landau distribution)
                                          , hstPtr->GetMean()  // most prob value
                                          , hstPtr->GetSumOfWeights()  // area
                                          , hstPtr->GetRMS()  // sigma
                                          }
                       , lowParLims[4] = { 1e-6
                                         , 10
                                         , hstPtr->GetEntries()/10.
                                         , .05
                                         }
                       , upParLims[4] = { 1.
                                        , 600
                                        , hstPtr->GetEntries()*100  // for large number of event may be wrong
                                        , 10
                                        }
                       ;

                //std::cout << "Scaled width: " << lowParLims[0] << ", " << startValues[0] << ", " << upParLims[0] << std::endl
                //          << "         MPV: " << lowParLims[1] << ", " << startValues[1] << ", " << upParLims[1] << std::endl
                //          << "        area: " << lowParLims[2] << ", " << startValues[2] << ", " << upParLims[2] << std::endl
                //          << "       sigma: " << lowParLims[3] << ", " << startValues[3] << ", " << upParLims[3] << std::endl
                //          ;

                bool isGood = true;
                Double_t pars[4], errors[4], chiSq;
                Int_t ndf;
                TF1 * ff = na64dp::util::langausfit( hstPtr
                                                   , fitRange
                                                   , startValues
                                                   , lowParLims, upParLims
                                                   , NULL
                                                   , pars
                                                   , errors
                                                   , chiSq, ndf
                                                   );
                if( chiSq/ndf > 1e2
                 || fabs(hstPtr->GetMean() - pars[1]) > pars[3]*3 ) {
                    isGood = false;
                }

                if( useExtFmt ) {
                    if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                        for(int i = 0; i < 4; ++i) {
                            os << pars[i] << "," << errors[i] << ",";
                        }
                    } else {
                        for(int i = 0; i < 4; ++i) {
                            os << "nan,nan,";
                        }
                    }
                    os << chiSq;
                } else {
                    if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                        // use MVP of langaus fit as the pedestal value and
                        // sigma + Error_MVP as validity interval
                        os << std::scientific << pars[1] << ","
                           << errors[1] + pars[3] << ",";
                        os.flags( commonFlags );
                        os << hstPtr->GetEntries()
                           ;
                    } else {
                        for(int i = 0; i < 4; ++i) {
                            os << "nan,nan,0,";
                        }
                    }
                }

                if(ff) {
                    hstPtr->Draw();
                    ff->Draw("SAME");
                    char bf[256];
                    const char * pat = goodPathPat.c_str();
                    if( !isGood ) {
                        pat = badPathPat.c_str();
                    }
                    snprintf( bf, sizeof(bf), pat
                            , hstPtr->GetName() );
                    c->Print( bf );
                } else {
                    std::cerr << "No fit function returned for \""
                              << tbname
                              << "\"." << std::endl;
                }
                delete c;
            }
            #endif
            if(outFormat == "json") {
                os << "    }";  // finalize per-detector object
            } else os << std::endl;
        }
    } else if(className == "TProfile") {
        while((objPtr = it.get_match())) {
            TProfile * hstPtr = dynamic_cast<TProfile*>(objPtr);
            assert( hstPtr );
            
            TObjArray * subs = descRx.MatchS( hstPtr->GetTitle() );
            TString tbname, baseFreqLow, baseFreqUp;
            if (subs->GetLast()+1 == 4) {
                tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
                baseFreqLow = dynamic_cast<TObjString *>(subs->At(2))->GetString();
                baseFreqUp  = dynamic_cast<TObjString *>(subs->At(3))->GetString();
            } else if (subs->GetLast()+1 == 2) {
                tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
                baseFreqLow = "0";
                baseFreqUp  = "0";
            } else {
                std::cerr << "Description string \"" << hstPtr->GetTitle()
                          << "\" of TProfile \"" << hstPtr->GetName()
                          << "\" does not match regex. Ignored." << std::endl;
                continue;
            }

            if( hstPtr->GetSumOfWeights() == 0 ) {
                std::cerr << "TProfile \"" << hstPtr->GetName()
                          << "\" has zero sum of weights, ignored." << std::endl;
                continue;
            }
            ++objCount;

            os << tbname << "," << baseFreqLow << "," << baseFreqUp;
            for(Int_t nBin = 1; nBin < hstPtr->GetXaxis()->GetNbins()+1; ++nBin) {
                // os << std::scientific << pars[1] << ","
                // << errors[1] + pars[3] << ",";
                // os.flags( commonFlags );
                os << "," << std::scientific
                   << hstPtr->GetBinContent(nBin) << ","
                   << hstPtr->GetBinError(nBin)*sqrt(hstPtr->GetBinEffectiveEntries(nBin));
                os.flags( commonFlags );
            }

            //os << tbname << "," << pedType << ","
            //   << std::scientific;  // <== NOTE this!
        
            #if 0
            {
                TCanvas *c = new TCanvas();
                c->SetLogy();
                gStyle->SetOptStat(111111111);  // Set meaningful stats TPaveStat
                gStyle->SetOptFit(1111);

                Double_t 
                         #if 0
                         fitRange[2] = { hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ?   hstPtr->GetRMS()/2 : 30 )
                                       , hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ? 3*hstPtr->GetRMS()   : 100) }
                         #else
                         fitRange[2] = { 10
                                       , 600 }
                         #endif
                       , startValues[4] = { 1  // scaled width (Landau distribution)
                                          , hstPtr->GetMean()  // most prob value
                                          , hstPtr->GetSumOfWeights()  // area
                                          , hstPtr->GetRMS()  // sigma
                                          }
                       , lowParLims[4] = { 1e-6
                                         , 10
                                         , hstPtr->GetEntries()/10.
                                         , .05
                                         }
                       , upParLims[4] = { 1.
                                        , 600
                                        , hstPtr->GetEntries()*100  // for large number of event may be wrong
                                        , 10
                                        }
                       ;

                //std::cout << "Scaled width: " << lowParLims[0] << ", " << startValues[0] << ", " << upParLims[0] << std::endl
                //          << "         MPV: " << lowParLims[1] << ", " << startValues[1] << ", " << upParLims[1] << std::endl
                //          << "        area: " << lowParLims[2] << ", " << startValues[2] << ", " << upParLims[2] << std::endl
                //          << "       sigma: " << lowParLims[3] << ", " << startValues[3] << ", " << upParLims[3] << std::endl
                //          ;

                bool isGood = true;
                Double_t pars[4], errors[4], chiSq;
                Int_t ndf;
                TF1 * ff = na64dp::util::langausfit( hstPtr
                                                   , fitRange
                                                   , startValues
                                                   , lowParLims, upParLims
                                                   , NULL
                                                   , pars
                                                   , errors
                                                   , chiSq, ndf
                                                   );
                if( chiSq/ndf > 1e2
                 || fabs(hstPtr->GetMean() - pars[1]) > pars[3]*3 ) {
                    isGood = false;
                }

                if( useExtFmt ) {
                    if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                        for(int i = 0; i < 4; ++i) {
                            os << pars[i] << "," << errors[i] << ",";
                        }
                    } else {
                        for(int i = 0; i < 4; ++i) {
                            os << "nan,nan,";
                        }
                    }
                    os << chiSq;
                } else {
                    if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                        // use MVP of langaus fit as the pedestal value and
                        // sigma + Error_MVP as validity interval
                        os << std::scientific << pars[1] << ","
                           << errors[1] + pars[3] << ",";
                        os.flags( commonFlags );
                        os << hstPtr->GetEntries()
                           ;
                    } else {
                        for(int i = 0; i < 4; ++i) {
                            os << "nan,nan,0,";
                        }
                    }
                }

                if(ff) {
                    hstPtr->Draw();
                    ff->Draw("SAME");
                    char bf[256];
                    const char * pat = goodPathPat.c_str();
                    if( !isGood ) {
                        pat = badPathPat.c_str();
                    }
                    snprintf( bf, sizeof(bf), pat
                            , hstPtr->GetName() );
                    c->Print( bf );
                } else {
                    std::cerr << "No fit function returned for \""
                              << tbname
                              << "\"." << std::endl;
                }
                delete c;
            }
            #endif
            os << std::endl;
        }
    } else {
        std::cerr << "Don't know how to extract DFT baselines from \""
            << className << "\"" << std::endl;
    }
    if(outFormat == "json") os << "  ]\n}\n";
    if( osPtr != &std::cout ) {
        static_cast<std::ofstream&>(os).close();
        delete osPtr;
    }

    std::cout << objCount << " entries have been processed."
              << std::endl;
    return 0 == objCount ? -1: 0;
}  // }}} fft zeroes

//
// Single value fitting routine (draft) {{{
// The idea of this procedure is to provide a verataile TH1/TProfile fitting
// routine, capable to fit data within TFile based on type matching and regular
// expression. Use cases:
//  - per-detector fitting of pedestals (even, odd)
//  - fitting of linear sums for zero suppression
//  - various othe cases of per-detector binned data of normal, lognormal,
//    Landau-like etc distributions
//  - automated fitted of energy deposition most prob. peaks for calibs
// Resulting CSV output can be used in many applications.
#if 1
class GenericHstParameters {
private:
    Double_t _mean, _stddev, _S, _xlim[2], _maxValue, _nEntries;
public:
    GenericHstParameters( TH1* hst) {
        _mean = hst->GetMean();
        _stddev = hst->GetStdDev();
        _S = hst->GetSumOfWeights()*hst->GetXaxis()->GetBinWidth(1);
        _xlim[0] = hst->GetXaxis()->GetXmin();
        _xlim[1] = hst->GetXaxis()->GetXmax();
        _maxValue = hst->GetMaximum();
        _nEntries = hst->GetEntries();
    }
    GenericHstParameters(TProfile*);  // TODO

    Double_t mean() const       { return _mean; }
    Double_t stddev() const     { return _stddev; }
    Double_t S() const          { return _S; }
    Double_t x_low() const      { return _xlim[0]; }
    Double_t x_up() const       { return _xlim[1]; }
    Double_t max_value() const  { return _maxValue; }
    Double_t n_entries() const    { return _nEntries; }

    Double_t mu_minus_sigma_or_x_low() const
        { return _mean - _stddev > _xlim[0] ? _mean - _stddev : _xlim[0]; }
    Double_t mu_minus_15sigma_or_x_low() const
        { return _mean - 1.5*_stddev > _xlim[0] ? _mean - 1.5*_stddev : _xlim[0]; }
    Double_t mu_minus_3sigma_or_x_low() const
        { return _mean - 3*_stddev > _xlim[0] ? _mean - 3*_stddev : _xlim[0]; }

    Double_t mu_plus_sigma_or_x_up() const
        { return _mean + _stddev > _xlim[1] ? _mean + _stddev : _xlim[1]; }
    Double_t mu_plus_15sigma_or_x_up() const
        { return _mean + 1.5*_stddev > _xlim[1] ? _mean + 1.5*_stddev : _xlim[1]; }
    Double_t mu_plus_3sigma_or_x_up() const
        { return _mean + 3*_stddev > _xlim[1] ? _mean + 3*_stddev : _xlim[1]; }

    Double_t stddev01() const  {  return  0.1*_stddev; }
    Double_t stddev033() const {  return 0.33*_stddev; }
    Double_t stddev10() const  {  return   10*_stddev; }
    Double_t stddev3() const   {  return    3*_stddev; }

    Double_t S01() const { return 0.1*_S; }
    Double_t S05() const { return 0.5*_S; }
    Double_t S10() const { return 10*_S; }
    Double_t S15() const { return 1.5*_S; }
    Double_t S3() const  { return 3*_S; }
};
typedef Double_t (GenericHstParameters::*generic_hst_p_getter)() const;
static const struct {
    char name[64];
    generic_hst_p_getter getter;
} gGenericHstParametersGetters[] = {
    #define M_MTD(nm) { #nm,       &GenericHstParameters:: nm }
    M_MTD(mean),
    M_MTD(stddev),
    M_MTD(S),
    M_MTD(x_low),
    M_MTD(x_up),
    M_MTD(max_value),
    M_MTD(n_entries),
    M_MTD(mu_minus_sigma_or_x_low), M_MTD(mu_minus_15sigma_or_x_low),   M_MTD(mu_minus_3sigma_or_x_low),
    M_MTD(mu_plus_sigma_or_x_up),   M_MTD(mu_plus_15sigma_or_x_up),     M_MTD(mu_plus_3sigma_or_x_up),
    M_MTD(stddev01), M_MTD(stddev033), M_MTD(stddev10), M_MTD(stddev3),
    M_MTD(S01), M_MTD(S05), M_MTD(S10), M_MTD(S15), M_MTD(S3),
    // ...
    { "", NULL }
};

generic_hst_p_getter
generic_getter_by_name(const char * name) {
    for(auto * p = gGenericHstParametersGetters; '\0' != *p->name; ++p) {
        if(!strcmp(name, p->name)) return p->getter;
    }
    return NULL;
}

class FitFunc {
private:
    /// Fit function callback
    Double_t (*_f)(Double_t *, Double_t *);
    /// Parameters list
    struct Par {
        char name[16];
        Double_t staticValue, limits[2];
        generic_hst_p_getter initValueGetter, valueBoundaries[2];
        Par() : name("")
              , staticValue(std::nan("0"))
              , limits{std::nan("0"), std::nan("0")}
              , initValueGetter(nullptr)
              , valueBoundaries{nullptr, nullptr}
              {}
    } _pars[16], _xlim[2];
    int _nPars;
    const std::string _tag;
    TF1 * _fitFunc;
public:
    FitFunc( const std::string & tag
           , const YAML::Node & cfg);

    const std::string tag() const { return _tag; }

    TF1 * instantiate( const GenericHstParameters & ghp) {
        if(!_fitFunc) {
            char namebf[128];
            snprintf(namebf, sizeof(namebf), "fit_f_%s", _tag.c_str());
            _fitFunc = new TF1( namebf, _f
                   , std::isnan(_xlim[0].staticValue)
                        ? (ghp.*_xlim[0].initValueGetter)()
                        : std::isnan(_xlim[0].staticValue)
                   , std::isnan(_xlim[1].staticValue)
                        ? (ghp.*_xlim[1].initValueGetter)()
                        : std::isnan(_xlim[1].staticValue)
                   , _nPars
                );
            for(Int_t i = 0; i < _nPars; ++i) {
                _fitFunc->SetParName(i, _pars[i].name);
            }
        }
        for(Int_t i = 0; i < _nPars; ++i) {
            _fitFunc->SetParName(i, _pars[i].name);
            _fitFunc->SetParLimits( i
                           , std::isnan(_pars[i].limits[0]) ? (ghp.*_pars[i].valueBoundaries[0])() : _pars[i].limits[0]
                           , std::isnan(_pars[i].limits[1]) ? (ghp.*_pars[i].valueBoundaries[1])() : _pars[i].limits[1]
                           );
            _fitFunc->SetParameter(i, std::isnan(_pars[i].staticValue)
                    ? (ghp.*_pars[i].initValueGetter)()
                    : _pars[i].staticValue
                    );
        }
        return _fitFunc;
    }
};


static void
_static_value_or_getter( const YAML::Node & cfg
                       , Double_t & staticValueRef
                       , generic_hst_p_getter & getterRef
                       ) {
    if(!cfg.IsScalar()) throw("Scalar expected");
    const std::string asString = cfg.as<std::string>();
    if(std::isdigit(asString[0]) || '.' == asString[0]) {
        staticValueRef = cfg.as<Double_t>();
        getterRef = nullptr;
    } else {
        staticValueRef = std::nan("0");
        getterRef = generic_getter_by_name(cfg.as<std::string>().c_str());
    }
}

FitFunc::FitFunc( const std::string & tag
                , const YAML::Node & cfg)
        : _nPars(0)
        , _tag(tag)
        , _fitFunc(nullptr)
        {
    const std::string strType = cfg["type"].as<std::string>();  // required
    _xlim[0].initValueGetter = &GenericHstParameters::x_low;
    _xlim[1].initValueGetter = &GenericHstParameters::x_up;
    if(cfg["range"]) {
        _static_value_or_getter(cfg["range"][0], _xlim[0].staticValue, _xlim[0].initValueGetter);
        _static_value_or_getter(cfg["range"][1], _xlim[1].staticValue, _xlim[1].initValueGetter);
    }
    #define _M_set_if_given(nm, dftGetter, dftLow, dftUp)                   \
        strcpy(_pars[_nPars].name, #nm );                                   \
        _pars[_nPars].initValueGetter    = &GenericHstParameters:: dftGetter;  \
        _pars[_nPars].valueBoundaries[0] = &GenericHstParameters:: dftLow;  \
        _pars[_nPars].valueBoundaries[1] = &GenericHstParameters:: dftUp;   \
        if(cfg[ #nm ]) {                                                    \
            if(cfg[ #nm ]["value"])                                         \
                _static_value_or_getter(cfg[ #nm ]["value"], _pars[_nPars].staticValue, _pars[_nPars].initValueGetter);  \
            if(cfg[ #nm ]["range"]) {                                       \
                _static_value_or_getter(cfg[ #nm ]["range"][0], _pars[_nPars].limits[0], _pars[_nPars].valueBoundaries[0]);  \
                _static_value_or_getter(cfg[ #nm ]["range"][1], _pars[_nPars].limits[1], _pars[_nPars].valueBoundaries[1]);  \
            }                                                               \
        }                                                                   \
        ++_nPars;
    if(strType == "Gaussian") {
        // set function type
        _f = fitf_g;
        _M_set_if_given(mean, mean, x_low, x_up);
        _M_set_if_given(stddev, stddev, stddev01, stddev10);
        _M_set_if_given(S, S, S05, S15);
    } /*else if(strType == "CrystalBall") {
        _f = fitf_cb;
        // ...
    } else if(strType == "right-crystalball") {
        _f = fitf_rcb;
        // ...
    }*/
    #undef _M_set_if_given
    else {
        throw std::runtime_error(std::string("Unknown fit func. type"));
    }
}

template<typename T> std::unordered_map<std::string, TFitResultPtr>
_do_fit( TObject * hst_
       , std::unordered_map< std::string
                      , std::shared_ptr<FitFunc>
                      > & fitFuncs
       , TPRegexp & descRx, const YAML::Node & descGroups
       , na64dp::util::StrSubstDict & semantics
       ) {
    // downcast
    T * hst = dynamic_cast<T*>(hst_);
    if(!hst) {
        throw std::runtime_error("Wrong ROOT object type.");
    }
    // extract semantics from object description
    TObjArray * subs = descRx.MatchS( hst->GetTitle() );
    if( (!subs) || !subs->At(1) ) {
        throw std::runtime_error(na64dp::util::format("Description string \"%s\" of"
                    " object \"%s\" does not match description lookup"
                    " expression.", hst->GetTitle(), hst->GetName() ));
    }

    for(auto it = descGroups.begin(); it != descGroups.end(); ++it) {
        std::string groupName(it->first.as<std::string>());
        int nGroup = it->second.as<int>();
        if(nGroup >= subs->GetSize() || nGroup < 0) {
            throw std::runtime_error(na64dp::util::format("Description matching"
                    " expression results in only %d groups while #%d requested"
                    " for \"%s\"", subs->GetSize(), nGroup
                    , groupName.c_str()));
        }
        TObjString * tStr = nullptr;
        if(subs->At(nGroup)) {
            tStr = dynamic_cast<TObjString *>(subs->At(nGroup));
            assert(tStr);
        }
        auto ir = semantics.emplace(groupName
                    , tStr ? tStr->GetString().Data() : ""
                    );
        if(!ir.second) {
            throw std::runtime_error(na64dp::util::format("Description matching"
                        " expression group \"%s\" has non-unique number"
                        , groupName.c_str()));
        }
    }

    // dbg, XXX
    #if 0
    std::cout << "Object \"" << hst->GetName() << "\" with descr. \""
        << hst->GetTitle() << "\" resulted in:" << std::endl;
    for(const auto & item : semantics) {
        std::cout << "    - " << item.first << " -> \"" << item.second << "\"" << std::endl;
    }
    #endif

    // Get generic parameters from histogram
    GenericHstParameters genPars(hst);

    char valuebf[64];
    #define _M_set_hist_p(nm)                                       \
    snprintf(valuebf, sizeof(valuebf), "%e", genPars. nm ());    \
    semantics.emplace("hist." #nm, valuebf)
    _M_set_hist_p(mean);
    _M_set_hist_p(stddev);
    _M_set_hist_p(S);
    _M_set_hist_p(x_low);
    _M_set_hist_p(x_up);
    _M_set_hist_p(n_entries);
    #undef _M_set_hist_p

    // try to performs the fits
    std::unordered_map<std::string, TFitResultPtr> r;
    for(auto & fitFuncPair: fitFuncs) {
        TFitResultPtr frPtr;
        TF1 * rootTF1 = fitFuncPair.second->instantiate(genPars);
        frPtr = hst->Fit( rootTF1
                , "SQLN"
                , ""
                );
        r.emplace(fitFuncPair.first, frPtr);
        char parNameBf[128], valueBf[64];
        const bool isGood = frPtr->IsValid();
        for(Int_t nPar = 0; nPar < rootTF1->GetNpar(); ++nPar) {
            snprintf(parNameBf, sizeof(parNameBf), "%s.%s"
                    , fitFuncPair.second->tag().c_str(), rootTF1->GetParName(nPar));
            snprintf(valueBf, sizeof(valueBf), "%e", isGood ? rootTF1->GetParameter(nPar) : std::nan("0"));
            semantics[parNameBf] = valueBf;
        }
    }
    return r;
}

static int
_single_value_fit(const YAML::Node & cfg, TFile * f) {
    std::string nameRxs = cfg["nameLookup"]["expression"].as<std::string>()
              , descRxs
              ;
    if(cfg["description"]) {
        descRxs = cfg["description"]["expression"].as<std::string>();
    }
    // Create TFile iterator to obtain items
    na64dp::util::NamedFileIterator it( f, cfg["ROOTClassName"].as<std::string>().c_str(),
            nameRxs.c_str() );
    
    TPRegexp * descRx = descRxs.empty() ? nullptr : new TPRegexp(descRxs);

    TObject * objPtr;
    size_t objCount = 0;

    // Configure fitting with available parameters
    std::unordered_map< std::string
                      , std::shared_ptr<FitFunc>
                      > fitFIdxs; {
        const YAML::Node & fitFuncsNode = cfg["fitFuncs"];
        // iterate over fitting functions defined and fill vector with 
        // configured instances for further fitting
        for(auto it = fitFuncsNode.begin(); it != fitFuncsNode.end(); ++it) {
            const std::string fitFuncName = it->first.as<std::string>();
            fitFIdxs.emplace(fitFuncName, std::make_shared<FitFunc>(fitFuncName, it->second));
        }
    }

    // set (open) output stream
    std::ostream * osPtr;
    if(cfg["output"] && cfg["output"]["filePath"]) {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    } else {
        osPtr = &std::cout;
    }
    std::ostream & os = *osPtr;

    const std::string ROOTClassName = cfg["ROOTClassName"].as<std::string>();
    TObjArray * nmSubs;
    const std::string outFmtStr = cfg["output"]["format"].as<std::string>();
    while((objPtr = it.get_match(&nmSubs))) {
        na64dp::util::StrSubstDict semantics;
        // append subst dict with items obtained from name matching regex
        if(cfg["nameLookup"]["groups"]) {
            const YAML::Node & groups = cfg["nameLookup"]["groups"];
            for(auto it = groups.begin(); it != groups.end(); ++it) {
                std::string groupName(it->first.as<std::string>());
                int nGroup = it->second.as<int>();
                if(nGroup >= nmSubs->GetSize() || nGroup < 0) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression results"
                            " in only %d groups while #%d requested for"
                            " \"%s\"", nmSubs->GetSize(), nGroup
                            , groupName.c_str()));
                }
                TObjString * tStr = nullptr;
                if(nmSubs->At(nGroup)) {
                    tStr = dynamic_cast<TObjString *>(nmSubs->At(nGroup));
                    assert(tStr);
                    assert(tStr->GetString().Data());
                }
                auto ir = semantics.emplace(groupName
                            , tStr ? tStr->GetString().Data() : ""
                            );
                if(!ir.second) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression"
                            " group \"%s\" has non-unique number", groupName.c_str()));
                }
            }
        }
        // delete name matching results as TPRegexp::MatchS() results are owned
        // by user according to docs
        delete nmSubs;  // owned by user
        // do the fitting
        std::unordered_map<std::string, TFitResultPtr> fitResults;
        if(ROOTClassName == "TH1") {
            fitResults = _do_fit<TH1>(objPtr, fitFIdxs, *descRx
                    , (cfg["description"] && cfg["description"]["groups"])
                      ? cfg["description"]["groups"]
                      : YAML::Node(YAML::NodeType::Null)
                    , semantics);
        } /*else if(ROOTClassName == "TProfile") {
            fitResults = _do_fit<TProfile>(objPtr, fitFIdxs, *descRx
                    , (cfg["description"] && cfg["description"]["groups"])
                      ? cfg["description"]["groups"]
                      : YAML::Node(YAML::NodeType::Null)
                    , semantics);
        }*/
        else {
            throw std::runtime_error("Unkown ROOTClassName.");
        }
        // print results
        try {
            os << na64dp::util::str_subst(outFmtStr, semantics);
        } catch(na64dp::errors::StringIncomplete & e) {
            std::cerr << "Available string subst items:" << std::endl;
            for(const auto & p : semantics) {
                std::cerr << " - \"" << p.first << "\" -> \"" << p.second << "\""
                    << std::endl;
            }
            throw;
        }
        ++objCount;
    }  // while object exists in file
    
    if( osPtr != &std::cout ) {
        static_cast<std::ofstream&>(os).close();
        delete osPtr;
    }
    std::cout << objCount << " entries have been processed."
              << std::endl;
    return objCount ? 0 : -1;
}
#endif
// }}} (single value fit)

//
// DFT ECDF {{{
/**\brief Obtains ECDF for DFT amplitude distributions
 *
 * Designed to extract asjoin ECDF for (M)SADC detectors for subsequent
 * zero-denial procedures.
 */
static int
_get_dft_ecdf(const YAML::Node & cfg, TFile * f) {
    const std::string nameRxs = cfg["lookupRegex"].as<std::string>()
                    , descRxs = cfg["descriptionRegex"].as<std::string>()
                    ;

    std::cout << "Name lookup regex in use ... : " << nameRxs << std::endl
              << "Description regex in use ... : " << descRxs << std::endl
              << "Looking for objects ........ : THnSparse" << std::endl
              ;

    na64dp::util::NamedFileIterator it( f, "THnSparse",
            nameRxs.c_str() );

    TPRegexp descRx( descRxs );

    TObject * objPtr;
    size_t objCount = 0;

    // set (open) output stream
    #if 0
    std::ostream * osPtr;
    {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    }
    #endif

    while((objPtr = it.get_match())) {
        THnSparse * hstPtr = dynamic_cast<THnSparse*>(objPtr);
        assert( hstPtr );
        
        TObjArray * subs = descRx.MatchS( hstPtr->GetTitle() );
        TString tbname, baseFreqLow, baseFreqUp;
        if (subs->GetLast()+1 == 4) {
            tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
            baseFreqLow = dynamic_cast<TObjString *>(subs->At(2))->GetString();
            baseFreqUp  = dynamic_cast<TObjString *>(subs->At(3))->GetString();
        } else if (subs->GetLast()+1 == 2) {
            tbname      = dynamic_cast<TObjString *>(subs->At(1))->GetString();
            baseFreqLow = "0";
            baseFreqUp  = "0";
        } else {
            std::cerr << "Description string \"" << hstPtr->GetTitle()
                      << "\" of TH2 \"" << hstPtr->GetName()
                      << "\" does not match regex. Ignored." << std::endl;
            continue;
        }

        if( hstPtr->GetEntries() == 0 ) {
            std::cerr << "THnSparse \"" << hstPtr->GetName()
                      << "\" has no entries, ignored." << std::endl;
            continue;
        }
        ++objCount;

        std::cout << "Info: allocating " << hstPtr->GetNbins() + 1 << " elements for ECDF of "
            << hstPtr->GetName()
            << ", sparse fraction is "  << hstPtr->GetSparseFractionBins()
            << std::endl;  // XXX
        // ECDF is packed into (potentially huge) array of counts
        std::vector<float> ecdf(hstPtr->GetNbins()+1);
        ecdf[0] = 0.;
        // This code in general reproduces most of the content of
        // THnBase::ComputeIntegral(). We had to implement it ourselves because
        // ROOT does not provide public access to its fIntegral.
        Int_t * coord = new Int_t[hstPtr->GetNdimensions()];
        Long64_t i = 0;
        THnIter iter(hstPtr);
        while ((i = iter.Next(coord)) >= 0) {
            Double_t v = hstPtr->GetBinContent(i);
            // check whether the bin is regular
            bool regularBin = true;
            for (Int_t dim = 0; dim < hstPtr->GetNdimensions(); dim++) {
                if (coord[dim] < 1 || coord[dim] > hstPtr->GetAxis(dim)->GetNbins()) {
                    regularBin = false;
                    break;
                }
            }
            // if outlayer, count it with zero weight
            if (!regularBin) v = 0.;
            ecdf[i + 1] = ecdf[i] + v;
        }
        delete [] coord;
        // check sum of weights
        if (ecdf[hstPtr->GetNbins()] == 0.) {
            std::cerr << "Warning: empty sum (of regular bins) for "
                << hstPtr->GetName() << ", skept." << std::endl;
            ecdf.clear();
            continue;
        }
        // normalize the integral array
        for (Long64_t j = 0; j <= hstPtr->GetNbins(); ++j)
            ecdf[j] = ecdf[j] / ecdf[hstPtr->GetNbins()];

        // TODO/NOTE: at this moment I kind of give up with DFT analysis, since
        // multivariate ECDF gets complicated and performance-demanding. One
        // should think on PCA/covariance analysis in order to decrease
        // dimensions or to elaborate the CDF comparison...
        throw std::runtime_error("TODO: DFT analysis postponed.");

        // set status to valid
        //fIntegralStatus = kValidInt;
        //return fIntegral[GetNbins()];
        
        //double sum = hstPtr->GetEntries();
        //     , binVol = 1.;
        //for(Int_t nDim = 0; nDim < hstPtr->GetNdimensions(); ++nDim) {
        //    binVol *= (hstPtr->GetAxis(nDim)->GetXmax() - hstPtr->GetAxis(nDim)->GetXmin());
        //}
        //std::cout << "  " << binVol << std::endl;
        // ...
    }

    std::cout << objCount << " entries have been processed."
              << std::endl;
    return 0 == objCount ? -1: 0;
}  // }}} dft ecdf

//
// Threshold on 1D Quantile {{{
static int
_1d_ecdf(const YAML::Node & cfg, TFile * f) {
    std::string nameRxs = cfg["nameLookup"]["expression"].as<std::string>()
              , descRxs
              ;
    if(cfg["description"]) {
        descRxs = cfg["description"]["expression"].as<std::string>();
    }
    // Create TFile iterator to obtain items
    na64dp::util::NamedFileIterator it( f, cfg["ROOTClassName"].as<std::string>().c_str(),
            nameRxs.c_str() );
    
    TPRegexp * descRx = descRxs.empty() ? nullptr : new TPRegexp(descRxs);

    TObject * objPtr;
    size_t objCount = 0;

    // set (open) output stream
    std::ostream * osPtr;
    if(cfg["output"] && cfg["output"]["filePath"]) {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    } else {
        osPtr = &std::cout;
    }
    std::ostream & os = *osPtr;

    const std::string ROOTClassName = cfg["ROOTClassName"].as<std::string>();
    TObjArray * nmSubs;
    const std::string outFmtStr = cfg["output"]["format"].as<std::string>();
    while((objPtr = it.get_match(&nmSubs))) {
        na64dp::util::StrSubstDict semantics;
        // append subst dict with items obtained from name matching regex
        if(cfg["nameLookup"]["groups"]) {
            const YAML::Node & groups = cfg["nameLookup"]["groups"];
            for(auto it = groups.begin(); it != groups.end(); ++it) {
                std::string groupName(it->first.as<std::string>());
                int nGroup = it->second.as<int>();
                if(nGroup >= nmSubs->GetSize() || nGroup < 0) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression results"
                            " in only %d groups while #%d requested for"
                            " \"%s\"", nmSubs->GetSize(), nGroup
                            , groupName.c_str()));
                }
                TObjString * tStr = nullptr;
                if(nmSubs->At(nGroup)) {
                    tStr = dynamic_cast<TObjString *>(nmSubs->At(nGroup));
                    assert(tStr);
                    assert(tStr->GetString().Data());
                }
                auto ir = semantics.emplace(groupName
                            , tStr ? tStr->GetString().Data() : ""
                            );
                if(!ir.second) {
                    throw std::runtime_error(na64dp::util::format("Name matching expression"
                            " group \"%s\" has non-unique number", groupName.c_str()));
                }
            }
        }
        // delete name matching results as TPRegexp::MatchS() results are owned
        // by user according to docs
        delete nmSubs;  // owned by user
        // ...
    }

    std::cout << objCount << " entries have been processed."
              << std::endl;
    return 0 == objCount ? -1: 0;
}  // }}} 1D quantile ecdf ecdf

