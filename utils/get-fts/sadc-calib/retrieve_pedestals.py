"""
Based on pipeline .root output files, runs the `retrieve_pedestals.C` to obtain
.txt files containing important SADC features.
"""

import MultiProcMap
import argparse
import os.path
import sys
import pathlib
import subprocess
import pandas as pd
from pathlib import Path

def generate_txt_files( rootFiles ):
    outFiles = []
    cExec = os.path.join( pathlib.Path(__file__).parent.absolute(), 'retrieve_pedestals.C' )
    cExec = os.path.abspath(cExec)
    print(cExec)
    rootCmd = cExec
    for rootFile in rootFiles:
        if Path(rootFile).stat().st_size < 200*1024:
            sys.stderr.write( 'Warning: file "%s" has size'
                    ' less than 2kb. Omitting.\n'%rootFile )
            continue
        outputFile = '%s.txt'%rootFile
        if os.path.isfile(outputFile) and Path(outputFile).stat().st_size > 0:
            sys.stderr.write( 'Warning: file "%s" exists and is not empty.'
                    ' Omitting.\n'%outputFile )
            outFiles.append(outputFile)
            continue
        cmdList = [ 'root', '-q', '-l', '-b'
                  , rootCmd + r'("%s","%s",0xf)'%(rootFile, outputFile) ]
        subprocess.call( cmdList, shell=False )
        outFiles.append(outputFile)
    return outFiles

def main( inputFiles ):
    # Obtain pedestals in form of .txt files
    datFiles = generate_txt_files( inputFiles )
    print( 'NOTE: %d files have to be parsed into the'
            ' dataframes.'%len(datFiles) )
    # Parse .txt files into dataframes
    sdfs = {}
    for f in datFiles:
        sdfs[f] = pd.read_csv( f, index_col='name', delim_whitespace=True )
    df = pd.concat( [sdfs[k] for k in sorted(sdfs.keys())]
                  , keys=sorted(sdfs.keys())
                  , sort=True )
    # Pivot table (swap row index)
    #dft = df.T
    #dft.columns = dft.columns.swaplevel(0, 1)
    #dft.sort_index(axis=1, level=0, inplace=True)
    df = df.swaplevel()
    df.sort_index(level=0, inplace=True)
    #
    print(df)

if "__main__" == __name__:
    main( sys.argv[1:] )

