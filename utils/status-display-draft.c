#include <curses.h>
#include <stdlib.h>

struct na64pdsp_Layout {
    WINDOW * handlers
         , * brwsr
         , * msgs
         ;
};

struct na64pdsp_State {
    struct na64pdsp_Layout layout;
};

void
na64pdsp_draw_deco_handlers(na64pdsp_State * s) {
    struct na64pdsp_Layout * l = &(s->layout);
    WINDOW * w = l->handlers;

    int nRows, nCols;
    getmaxyx(w, nRows, nCols);
    // header
    whline(w, '_', nCols-1 );
    mvwprintw(w, 0, nRows - 12, "/ Handlers " );
    // right line (with scroll indicator)
    mvwvline(w, 0, nCols - 1, '|', nRows);
    // bottom line (with scroll indicator)
    mvwhline(w, nRows - 1, 0, '_', nCols - 1);
    // ...
    wrefresh(w);
}

void
na64pdsp_draw_deco_browser(na64pdsp_State * s) {
    struct na64pdsp_Layout * l = &(s->layout);
    WINDOW * w = l->brwsr;

    int nRows, nCols;
    getmaxyx(w, nRows, nCols);
    // header
    whline(w, '_', nCols);
    mvwprintw(w, 0, nCols - 24, "/ Handlers / Attributes ");
    // ...
    wrefresh(w);
}

void
na64pdsp_draw_deco_log(na64pdsp_State * s) {
    struct na64pdsp_Layout * l = &(s->layout);
    WINDOW * w = l->msgs;

    int nRows, nCols;
    getmaxyx(w, nRows, nCols);
    // header
    whline(w, '_', nCols);
    mvwprintw(w, 0, nCols - 6, "/ Log ");
    // ...
    wrefresh(w);
}

void
na64pdsp_layout_init( int baseY, int baseX, int scrh, int scrw
         		    , struct na64pdsp_State * s ) {
    struct na64pdsp_Layout * l = &(s->layout);
    // defines ratio of browser window dimensions to full screen width and
    // heights
    float fracs[] = {0.7, 0.6};
    // handlers list window
    l->handlers = newwin( (int) scrh*fracs[1]  // ............... nlines
                        , (int) scrw*(1 - fracs[0])  // ......... ncols
                        , baseY  // ............................. begin Y
                        , baseX  // ............................. begin X
                        );
    na64pdsp_draw_deco_handlers(s);
    // browser window
    l->brwsr = newwin( (int) scrh*fracs[1]  // .................. nlines
                     , (int) scrw*fracs[0]  // .................. ncols
                     , baseY  // ................................ begin Y
                     , baseX + (int) scrw*(1 - fracs[0])  // .... begin X
                     );
    na64pdsp_draw_deco_browser(s);
    // messages window
    l->msgs = newwin( (int) scrh*(1 - fracs[1]) // .................... nlines
                    , scrw // .................... ncols
                    , baseY + ((int) scrh*fracs[1])  // ............... begin Y
                    , baseX  // ......... begin X
                    );
    na64pdsp_draw_deco_log(s);
    // ... boxes?
    refresh();
}

void
na64pdsp_layout_destroy( struct na64pdsp_Layout * l ) {
    int i;
    // erase every box and delete each window
    //wborder(BOARD[i], ' ', ' ', ' ', ' ', ' ', ' ', '
    //', ' ');
    wrefresh(l->handlers);
    delwin(l->handlers);
    #if 0
    wrefresh(l->brwsr);
    delwin(l->brwsr);
    wrefresh(l->msgs);
    delwin(l->msgs);
    #endif
}


int main(void) {
    struct na64pdsp_State s;
    char key;
    initscr(); {  // curses mode
        noecho();
        cbreak();
        if( LINES < 24 || COLS < 80 ) {
            endwin();
            puts("Terminal too small");
            exit(2);
        }

        clear();
        refresh();
        na64pdsp_layout_init( 0, 0, 40, 80, &s );  // TODO: terminal width/height

        do {
            key = getch();
            // ...
        } while( ('q' != key) && ('Q' != key) );
        na64pdsp_layout_destroy(&s.layout);
    } endwin();

    return EXIT_SUCCESS;
}
