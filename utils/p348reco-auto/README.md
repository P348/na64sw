# Calibrations File Formatters

This directory contains various tools to generate .csv/.yaml/whatever
calibration files for `na64sw` from layouts provided by `p348reco` source
codes.

The goal is to leverage conversion from the hardcoded C/C++ values provided by
`p348reco` to portable formats.

Most of the data here provided as Python modules and/or text files for
automated conversion. The `generate.py` script provides rendering the inputs
into the protable format, see usage refrence for that script.
