#!/bin/bash
#
# Submits array of batch jobs on CERN HTCondor.
#
# Input parameters:
#   -t <tag> -- unique tag to denote this batch jobs
#   -r <run.yaml.in> -- pipeline file template (with `%()%` placeholders)
#   -i <jobs-input.txt> -- ASCII table with job parameters
#
# Tag is expected to be a unique ID of the job (e.g. "production-2023e-01",
# "psoitron-analysis-A", etc) and will be used to create directories, name
# jobs, etc.
#
# Run YAML file template is a runtime config used by NA64sw to describe
# processing pipeline with value placeholders denoted as `%(name)%` form. These
# placeholders are substituted within the job evaluation context by values
# taken from jobs input file.
#
# A jobs input file is a file with whitespace-spearated ASCII table,
# containing lines starting from `#` character. At least one #-started line
# should start with `# columns: ` prefix and contain space-separated list
# of column names which then will be interpreted by run wrapper script to
# extract values and substitute `%()%` placeholders in the runtime config.
#
# Output:
#   - logs and submission file are written to WS_DIR="/afs/.../$TAG" (`/afs/work/u/user/na64/batch/<tag>/`)
#   - output files are written to DATA_DIR="/eos/.../$TAG" (`/eos/u/user/na64/batch/<tag>/`)
#
# Environment variables
#   WS_DIR -- base dir to write HTCondor logs
#   DATA_DIR -- base dir to write output data
#   JON_RUN  -- job run script to run
# Note: one can directly override those variables.

#
# Parse input

while getopts "t:r:i:" opt; do
    case "${opt}" in
        t)
            TAG=${OPTARG}
            ;;
        r)
            PIPELINE_CONFIG=${OPTARG}
            ;;
        i)
            INPUT_PARAMETERS_FILE=${OPTARG}
            ;;
        *)
            echo "Error: unkown option \"${opt}\"" >&2
            usage
            ;;
    esac
done


# Check initial configuration validity and create directiories
#
# Tag
[ -z ${TAG} ] && >&2 echo "Error: no tag given." && exit 1
: ${WS_DIR:=/afs/cern.ch/work/${USER:0:1}/${USER}/na64/${TAG}}
if [ -d ${WS_DIR} ] ; then
    >&2 echo "Error: ${WS_DIR} exists (tag is not unique?)."
    exit 1
fi
mkdir -p ${WS_DIR}
if [ ! -d ${WS_DIR} ] ; then
    >&2 echo "Error: can't create ${WS_DIR}."
    exit 1
fi
: ${DATA_DIR:=/eos/user/${USER:0:1}/${USER}/na64/${TAG}}
if [ -d ${DATA_DIR} ] ; then
    >&2 echo "Error: ${DATA_DIR} exists (tag is not unique?)."
    exit 1
fi
mkdir -p ${DATA_DIR}
if [ ! -d ${DATA_DIR} ] ; then
    >&2 echo "Error: can't create ${DATA_DIR}."
    exit 1
fi

# input (per job) values file
[   -z ${INPUT_PARAMETERS_FILE} ] && >&2 echo "Error: no input file parameters file." && exit 1
[ ! -f ${INPUT_PARAMETERS_FILE} ] && >&2 echo "Error: input file parameters file does not exist." && exit 1
# check columns description line of the input parameters file: should list
# column names
read -r -a PARAMS_LIST <<< $(sed -nEe 's/\s*#\s*columns\s*:\s*(.*)/\1/p' ${INPUT_PARAMETERS_FILE})
if [ ${#errors[@]} -eq 0 ] ; then
    >&2 echo "Error: no \"#columns: ...\" string in input file ${INPUT_PARAMETERS_FILE} or empty list of columns"
    exit 1
fi
# Get number of jobs from file
NJOBS=$(grep -vE '^#.*' ${INPUT_PARAMETERS_FILE} | wc -l)

# pipeline config file
[   -z ${PIPELINE_CONFIG} ] && >&2 echo "Error: no pipeline config template file set." && exit 1
[ ! -f ${PIPELINE_CONFIG} ] && >&2 echo "Error: pipeline config template file \"${PIPELINE_CONFIG}\" does not exist." && exit 1
# collect template parameters from the file; TODO: ignore ones commented out
read -r -a REQUIRED_PARAMS <<< $(grep -Eo '%\([^(]+\)%' ${PIPELINE_CONFIG} | sed -nEe 's/^%\((.+)\)%$/\1/p')

# Check all the parameters can be resolved
#
UNRESOLVED_PARAMETERS=$(comm -23 <(echo ${REQUIRED_PARAMS[@]} | sort) <(${PARAMS_LIST[@]} | sort))
UNUSED_PARAMETERS=$(comm -23 <(echo ${PARAMS_LIST[@]} | sort) <(${REQUIRED_PARAMS[@]} | sort))

# Print configuration
#
cat <<EOF
Initial batch jobs configuration:
    tag . . . . . . . . . . . . . . . . . . "${TAG}"
    runtime pipeline config template  . . . "${PIPELINE_CONFIG}"
    job input parameters  . . . . . . . . . "${INPUT_PARAMETERS_FILE}"
Inferred parameters:
    Workspace directory . . . . . . . . . . ${WS_DIR}
    Output data directory . . . . . . . . . ${DATA_DIR}
    Number of jobs  . . . . . . . . . . . . ${NJOBS}
    Input parameters list . . . . . . . . . ${PARAMS_LIST[@]}
    Expected parameters list  . . . . . . . ${REQUIRED_PARAMS[@]}
EOF

if [ ! -z ${UNRESOLVED_PARAMETERS} ] ; then
    >&2 echo "Errors: unresolved parameters: %{UNRESOLVED_PARAMETERS}"
    exit 1
fi

if [ ! -z ${UNUSED_PARAMETERS} ] ; then
    >&2 echo "Warning parameters unused by input file: %{UNUSED_PARAMETERS}"
    exit 1
fi

