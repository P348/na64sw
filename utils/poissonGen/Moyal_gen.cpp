/*
This script generates the first set of Moyal function's
 parameters:
 - Area
 - The Maximum location
 - Width
 After that it generates time between two events
 and the second set of parameters
*/

#include <cmath>
#include <iostream>
#include <fstream>
#include <cstdlib> // Included for rand()
#include <iomanip> // Included for using ofs.precision()

// Generate name of TH1F
void name_gen( char* histName
             , char* TDirName
             , const int * cellID ) {
    // %s_%s - to compose to strings (TDirName and "2_3_1"
    // and to put "_" between them
    sprintf( histName, "%s_%d_%d_%d", TDirName, cellID[0], cellID[1], cellID[2]);
}

// Generate name of TDirectory
void dir_name_gen( char* dirName
                 , char* TDirName
                 , char* TSubDirName) {
    sprintf( dirName, "%s/%s", TDirName, TSubDirName );
}

// Create an instance of TH1F*
TH1F* hist_instance( TFile * file
                   , char * histName
                   , char * dirName) {
    
    // Get pointer on a TDirectory containing parameter's distribution
    TDirectory * dir = file->GetDirectory(dirName);
    if( !dir ) {
        std::cerr << "Error: unable to retrieve TDirectory instance \""
                  << dirName << "\" from file " << file << "." << std::endl;
        return NULL;
    }

    TH1F * hist = (TH1F*) dir->Get(histName);
    if (!hist) {
        std::cerr << "Error: unable to retrieve histogram \"" << histName << "\""
                  << " from directory \"" << dirName << "\" from file "
                  << file << "." << std::endl;
    }
    return hist;
}

// Generate the parameter distribution according to root histogram
float gen_value( TH1F * hist ) {
    float r1;  // parameter value
    int r2;  // parameter frequency value
    float r3;
    int binValue;  // Parameter frequency from histogram
    int bin_Width = hist->GetBinWidth(1);  // The size of histogram's bin
    float lower_Edge;  // Variable for genetating a parameter 
                       // without offset
    
    // Getting the lower edge of distribution
    lower_Edge = hist->GetBinLowEdge(1);
    
    // Getting the quantity of bins along X axis
    int binsNumber = hist->GetNbinsX();
    
    // Getting the maximum of histogram
    int max_Value = hist->GetBinContent(hist->GetMaximumBin());
        
    // Process of generating by Neiman's method
    do {
        r1 = (float)((rand()) % binsNumber + 1);
        r2 = (rand()) % max_Value + 1;
        binValue = hist->GetBinContent(r1);
    } while(r2 >= binValue);

    return (r1*bin_Width + lower_Edge)-bin_Width / 2.0;
}

// Generate time between two events
float timegen( double lambda )  {
    //srand (time(NULL)); // Setting another Seed
    double step = 450;
    //double lambda = 1000000;
    double lambdaL = lambda; // The collateral variable
    double par = exp(step);
    double k = 0.;  // The returnable number
    double p = 1.;

    // Generating a poisson's random number with average lambda
    // (k-1) is the distributed number
    do {
        k = k + 1;
        double u = (double)(rand())/RAND_MAX; // Generate random (double)number in range from 0 to 1
        p=p*u;
        while ((p < 1) && (lambdaL > 0)) {
            if (lambdaL > step) {
                p = p*par;
                lambdaL = lambdaL - step;
            } else {
                p = p * exp(lambdaL);
                lambdaL = 0;
            }
        }
    } while (p > 1.);
    
    // Calculating time
    return 1./(k-1);
} 

void Moyal_gen() {   
    char TFileName [] = "../ecalOnly.root";
    char TDir1Name [] = "PmtFitMoyal_area";
    char TDir2Name [] = "PmtFitMoyal_max";
    char TDir3Name [] = "PmtFitMoyal_width";
    char TSubDirName [] = "main";
    int cellID[3] = {2, 3, 1};
    float binValue;
    char histName [30];
    char dirName [30];
    TH1F * hist1;
    TH1F * hist2;
    TH1F * hist3;
    
    float area1;
    float max1;
    float width1;
    float time;
    float area2;
    float max2;
    float width2;
    
    // the value of tr (timeRatio) is 
    // responsible for parameterizability
    // of parameter's generation:
    // tr == 0 - no pile-up mode
    // tr > 0 && tr < infinity - pile-up only mode
    // tr == infinity - mixed mode
    float tr =  std::numeric_limits<double>::infinity();
    float tr1; // value, calculated from distribution
    
    // File for histogram.plot gnuplot script
    char HistFile [] = "../../../mydata/Poisson-generator/Moyal.txt";
    
    // Open file with histograms
    TFile * file = TFile::Open(TFileName);
    if (!file) {
        std::cerr << "Error: unable to open file \""
                  << TFileName << "\"." << std::endl;
    }
    
    std::ofstream ofs;
    ofs.open(HistFile);

    // Generating the Moyal Area parameter distribution
    name_gen(histName, TDir1Name, cellID);
    dir_name_gen(dirName, TDir1Name, TSubDirName);
    hist1 = hist_instance(file, histName, dirName);
    area1 = gen_value( hist1);
    
    // Generating the Moyal Maximum Position parameter distribution
    name_gen(histName, TDir2Name, cellID);
    dir_name_gen(dirName, TDir2Name, TSubDirName);
    hist2 = hist_instance(file, histName, dirName);
    max1 = gen_value( hist2);

    // Generating the Moyal Width parameter distribution
    name_gen(histName, TDir3Name, cellID);
    dir_name_gen(dirName, TDir3Name, TSubDirName);
    hist3 = hist_instance(file, histName, dirName);
    width1 = gen_value( hist3 );
    
    if(tr == 0) {
        ofs << area1 << std::endl << max1 
            << std::endl << width1 << std::endl;
    } 
    else {
        // Generating the Moyal Area parameter distribution
        area2 = gen_value( hist1 );
    
        // Generating the Moyal Maximum Position parameter distribution
        max2 = gen_value( hist2 );
        
        // Generating the Moyal Width parameter distribution
        width2 = gen_value( hist3 );
        
        do {
            time = (timegen(1000000))/(1.25*pow(10,-8));
            tr1 = max1/time; 
        } while ( tr1 > tr );
        
        ofs << area1 << std::endl << max1 << std::endl 
            << width1 << std::endl << time << std::endl << area2 
            << std::endl << max2 << std::endl << width2;
    }
    ofs.close();
}
 
