/*
This program gets waveform (digitalized values 
of Moyal function)
*/

#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>  // Included for rand()
#include <iomanip>  // Included for using ofs.precision() 

// get value of Moyal function with certain point in time
float get_waveform( double t
                  , float p0
                  , float p1
                  , float p2) {
    
    float value = (p0)*exp(-((t-p1)+exp(-(t-p1)))/(2*p2));
    return value;
}
    
int main () {
    double t;  // The arguement of Moyal function,
               // time is expressed in ADC intervals
    
    // Moyal function parameters
    float p0;
    float p1;
    float p2;
    
    float waveform[32];
    
    // Open file with generated parameters 
    std::ifstream ifs;
    ifs.open("../../../mydata/Poisson-generator/Moyal.txt");
    
    ifs >> p0 >> p1 >> p2 >> t;
    
    for (int i = 0; i < 32; i++) {
        t = i + 0.5;
        waveform[i] = get_waveform(t, p0, p1, p2);
        std::cout << t << "   " << waveform[i] << std::endl; 
    }
    return 0;
}
    
    
    
    
