#include "sdc.hh"

#include <getopt.h>
#include <iostream>

#ifndef SDC_NO_ROOT
#include <TSystem.h>
#endif

// Application configuration
struct AppCfg {
    std::string paths
              , acceptPatterns
              , rejectPatterns
              , keyType
              ;
    size_t minSize, maxSize;
    //std::string fileDiscoveryLog;  // xxx
    // ... default validity period?
    std::string typeName;
    bool incrementalUpdate
       , keepSourceInfo
       , doPrintCollection
       ;
    std::string runID;

    struct {
        sdc::ExtCSVLoader<p348reco::RunNo_t>::Grammar grammar;
        struct {
            std::string validityRange, dataType;
        } defaults;
        // ...
        void set(const std::string & expr) {
            size_t n = expr.find('=');
            if( std::string::npos == n ) {
                throw std::runtime_error(std::string("Unable to parse loader parameter"
                            " expression \"") + expr + "\"."
                        );
            }
            std::string key = expr.substr(0, n)
                      , val = expr.substr(n+1);
            if( key == "commentChar" ) {
                grammar.commentChar = val[0];
            } else if( key == "metadataMarker" ) {
                grammar.metadataMarker = val[0];
            } else if( key == "metadataKeyTag" ) {
                grammar.metadataKeyTag = val;
            } else if( key == "metadataTypeTag" ) {
                grammar.metadataTypeTag = val;
            } else if( key == "defaultDataType" ) {
                defaults.dataType = val;
            } else if( key == "defaultValidityRange" ) {
                defaults.validityRange = val;
            } else {
                throw std::runtime_error(std::string("Unknown key: \"")
                        + key + "\".");
            }
        }
    } loaderParams;
};

static void dump(const AppCfg & cfg, std::ostream & os) {
    os << "{"
       << "\"paths\":\"" << cfg.paths << "\","
       << "\"acceptPatterns\":\"" << cfg.acceptPatterns << "\","
       << "\"rejectPatterns\":\"" << cfg.rejectPatterns << "\","
       << "\"keyType\":\"" << cfg.keyType << "\","
       << "\"sizeRange\":[" << cfg.minSize << "," << cfg.maxSize << "],"
       << "\"loadUpdateFor\":{"
       << "\"incremental\":" << (cfg.incrementalUpdate ? "true" : "false") << ","
       << "\"keepSourceInfo\":" << (cfg.keepSourceInfo ? "true" : "false") << ","
       << "\"typeName\":\"" << cfg.typeName << "\","
       << "\"runID\":\"" << cfg.runID << "\""
       << "}," // "loadUpdateFor"
       << "\"loaderParams\":{"
       << "\"grammar\":{"
       << "\"commentChar\":"
       ;
    if( '\0' != cfg.loaderParams.grammar.commentChar ) {
        os << "\"" << cfg.loaderParams.grammar.commentChar << "\"";
    } else {
        os << "null";
    }
    std::cout << ",\"metadataMarker\":";
    if( '\0' != cfg.loaderParams.grammar.metadataMarker ) {
        os << "\"" << cfg.loaderParams.grammar.metadataMarker << "\"";
    } else {
        os << "null";
    }
    os << ",\"metadataKeyTag\":\"" << cfg.loaderParams.grammar.metadataKeyTag << "\","
       << "\"metadataTypeTag\":\"" << cfg.loaderParams.grammar.metadataTypeTag << "\""
       << "},\"defaults\":{"
       << "\"validityRange\":\"" << cfg.loaderParams.defaults.validityRange << "\","
       << "\"dataType\":\"" <<cfg.loaderParams.defaults.dataType << "\""
       << "}"  // "defaults"
       << "}"  // "loaderParams"
       ;
    os << "}";  // "appConfig"
}

// fwd (to be specialized)
template<typename T> struct JSONTraits;

// prints usage info (-h reference)
static void
usage_info( std::ostream & os
          , const AppCfg & cfg
          , const char * appName ) {
    os << "A testing tool for Self-Descriptive Calibration data API."
       << std::endl;
    os << "Usage:" << std::endl
       << "    $ " << appName << "[OPTIONS]" << std::endl
       << "This application is used to test SDC routines in conjunction with"
          " certain filesystem subtrees. Most of the output is designed to"
          " be consumed by automated analysis tools (thus, most of the output"
          " is JSON)."
       << std::endl
       ;
    os << "Parameters:" << std::endl
       << "  -h print usage (this) info and exit." << std::endl
       << "  -c prevents printing the collected document in index to output." << std::endl
       << "  -a <acceptPatterns=\"" << cfg.acceptPatterns << "\"> ..." << std::endl
       << "  -e <rejectPatterns=\"" << cfg.rejectPatterns << "\"> ..." << std::endl
       << "  -k <keyType=\"" << cfg.keyType << "\"> calib index data type"
            " to operate with. Choices are: \"runNo\" and \"datetime\". Affects"
            " way -r to be interpreted." << std::endl
       << "  -r <runID=\"" << cfg.runID << "\"> run identifier to query"
          " updates for." << std::endl
       << "  -t <typeID=\"" << cfg.typeName << "\"> calib data type to load" << std::endl
       //<< "  -T <typeID=\"" << cfg.defaultDataType << "\"> default data type"
       //         " to assume in calib docs" << std::endl
       << "  -p <paramKey>=<paramValue> -- sets grammar the parameter"
        " of ExtCSVLoader and its default values. Possible keys are:"
        " \"comment\", \"metadataMarker\", \"metadataKeyTag\","
        " \"metadataTypeTag\" (relate to grammar), \"defaultDataType\","
        " \"defaultValidityRange\" (default values)." //" Also, any metadata may be"
        //" appended with key prefixed with @ symbol (like -G @foo=bar)."  // TODO
       << std::endl
       //  ...
       ;
}

// configures app from cmd line
static int
configure_app(AppCfg & cfg, int argc, char * argv[]) {
    int opt;
    while((opt = getopt(argc, argv, "hisca:e:r:t:p:k:")) != -1) {
        switch(opt) {
            case 'h' :
                usage_info(std::cout, cfg, argv[0]);
                return 1;
            case 'i' :
                cfg.incrementalUpdate = true;
                break;
            case 's' :
                cfg.keepSourceInfo = true;
                break;
            case 'a' :
                cfg.acceptPatterns = optarg;
                break;
            case 'c' :
                cfg.doPrintCollection = false;
                break;
            case 'e' :
                cfg.rejectPatterns = optarg;
                break;
            case 'k' :
                if( strcmp(optarg, "runNo")
                 && strcmp(optarg, "datetime") ) {
                    std::cerr << "Bad key type specified with `-k': \""
                        << optarg
                        << "\". Possible choices are: runNo/keyType."
                        << std::endl;
                    return -1;
                }
                cfg.keyType = optarg;
                break;
            case 'r' :
                cfg.runID = optarg;
                break;
            case 't' :
                cfg.typeName = optarg;
                break;
            case 'p' :
                cfg.loaderParams.set(optarg);
                break;
            // ...
            default:  // '?'
                std::cerr << "optarg() returned " << opt << std::endl;  //?
                usage_info(std::cerr, cfg, argv[0]);
                return -1;
        }
    }
    if( optind != argc ) {
        cfg.paths.clear();
    }
    for( ; optind < argc ; ++optind ) {
        if(!cfg.paths.empty()) cfg.paths += ":";
        cfg.paths += std::string(argv[optind]);
    }
    return 0;
}

// JSON doesn't support NaN literal, so use it to print "null" instead
std::string double_or_null( double v ) {
    if( std::isnan(v) ) return "null";
    return std::to_string(v);
}

template<typename T>
struct JSONTraits< sdc::SrcInfo<T> > {
    static void datum_to_json( std::ostream & os
                      , const sdc::SrcInfo<T> & datumWithSrcInfo ) {
        os << "\"source\":{\"document\":\"" << datumWithSrcInfo.srcDocID
                << "\",\"line\":" << datumWithSrcInfo.lineNo
           << "},";
        JSONTraits<T>::datum_to_json(os, datumWithSrcInfo.data);
    }
    static void collection_to_json( std::ostream & os
            , const typename sdc::CalibDataTraits< sdc::SrcInfo<T> >::Collection & collection ) {
        os << "\"collectionType\":\"list\",\"updates\":[";
        bool firstEl = true;
        for( const sdc::SrcInfo<T> & el : collection ) {
            if( !firstEl ) os << ","; else firstEl = false;
            os << "{";
            datum_to_json(os, el);
            os << "}";
        }
        os << "]";
    }
};

template<>
struct JSONTraits<p348reco::CaloCellCalib> {
    static void datum_to_json( std::ostream & os
                      , const p348reco::CaloCellCalib & datum ) {
        os << "\"name\":\"" << datum.name << "\","
           << "\"z\":" << double_or_null(datum.z) << ","
           << "\"x\":" << double_or_null(datum.x) << ","
           << "\"y\":" << double_or_null(datum.y) << ","
           << "\"peakpos\":" << double_or_null(datum.peakpos) << ","
           << "\"time\":" << double_or_null(datum.time) << ","
           << "\"timesigma\":" << double_or_null(datum.timesigma) << ","
           << "\"have_time\":" << (datum.have_time ? "true" : "false")
           ;
    }

    static void collection_to_json( std::ostream & os
            , const typename sdc::CalibDataTraits< p348reco::CaloCellCalib >::Collection & collection ) {
        os << "\"collectionType\":\"list\",\"updates\":[";
        bool firstEl = true;
        for( const p348reco::CaloCellCalib & el : collection ) {
            if( !firstEl ) os << ","; else firstEl = false;
            os << "{";
            datum_to_json(os, el);
            os << "}";
        }
        os << "]";
    }
};

template<>
struct JSONTraits<p348reco::APVTimingCalib> {
    static void a_to_json( std::ostream & os
                         , const p348reco::APVTimingCalib::ParPack & pp ) {
        os << "\"r0\":" << pp.r0 << ",\"t0\":" << pp.t0 << ",\"a0\":" << pp.a0
           << ",\"r02\":" << pp.r02
           << ",\"t02\":" << pp.t02
           << ",\"a02\":" << pp.a02
           << ",\"r0t0\":" << pp.r0t0
           << ",\"r0a0\":" << pp.r0a0
           << ",\"t0a0\":" << pp.t0a0;
    }

    static void datum_to_json( std::ostream & os
                             , const p348reco::APVTimingCalib & datum ) {
        os << "\"a02\":{";
        a_to_json(os, datum.a02);
        os << "},\"a12\":{";
        a_to_json(os, datum.a12);
        os << "}";
    }

    static void collection_to_json( std::ostream & os,
            const typename sdc::CalibDataTraits< p348reco::APVTimingCalib >::Collection & collection ) {
        os << "\"collectionType\":\"object\",\"updates\":{";
        bool isFirst = true;
        for( const auto & p : collection ) {
            if( !isFirst ) os << ","; else isFirst = false;
            os << "\"" << p.first << "\":{";
            datum_to_json(os, p.second);
            os << "}";
        }
        os << "}";
    }
};

template<>
struct JSONTraits<p348reco::APVPedestalChannelEntry> {
    static void datum_to_json( std::ostream & os
                      , const p348reco::APVPedestalChannelEntry & datum ) {
        os << "\"channelflag\":" << datum.channelflag << ","
           << "\"pedestalamp\":" << datum.pedestalamp << ","
           << "\"pedestalsigma\":" << datum.pedestalsigma << ","
           << "\"calibrationamp\":" << datum.calibrationamp << ","
           << "\"calibrationsigma\":" << datum.calibrationsigma
           ;
    }

    static void collection_to_json( std::ostream & os
            , const typename sdc::CalibDataTraits< p348reco::APVPedestalChannelEntry >::Collection & collection ) {
        bool isFirst = true;
        os << "\"collectionType\":\"object\",\"updates\":{";
        for( const auto & p : collection ) {
            if( !isFirst ) os << ","; else isFirst = false;
            os << "\""
               << std::get<0>(p.first) << " "
               << std::get<1>(p.first) << " "
               << std::get<2>(p.first) << " "
               << std::get<3>(p.first) << " "
               << std::get<4>(p.first) << " "
               << "\":[";
            bool isFirstDatum = true;
            for( const auto & e : p.second ) {
                if(isFirstDatum) isFirstDatum = false; else os << ",";
                os << "{";
                datum_to_json(os, e);
                os << "}";
            }
            os << "]";
        }
        os << "}";
    }
};

template< typename KeyT
        , typename T
        > void
query_and_dump_updates( const AppCfg & cfg
                      , std::ostream & os
                      , sdc::Documents<KeyT> & docs ) {
    KeyT k = sdc::ValidityTraits<KeyT>::from_string(cfg.runID);
    auto c = cfg.incrementalUpdate
           ? docs.template load<T>(k)
           : docs.template get_latest<T>(k);
    os << "\"updateMode\":\""
       << (cfg.incrementalUpdate ? "incremental" : "single") << "\",";
    JSONTraits<T>::collection_to_json(os, c);
}

// generic function to query and dump the calibration data
template< typename KeyT
        , typename T
        > void
resolve_srcinf_query_and_dump_updates( const AppCfg & cfg
                                     , std::ostream & os
                                     , sdc::Documents<KeyT> & docs
                                     ) {
    if( cfg.keepSourceInfo ) {
        query_and_dump_updates<KeyT, sdc::SrcInfo<T>>( cfg, os, docs );
    } else {
        query_and_dump_updates<KeyT, T>( cfg, os, docs );
    }
}

// generic function to resolve, query and dump the calibration data
template< typename KeyT> void
resolve_updates_type_and_dump( const AppCfg & cfg
                             , std::ostream & os
                             , sdc::Documents<KeyT> & docs
                             );

template<> void
resolve_updates_type_and_dump<p348reco::RunNo_t>( const AppCfg & cfg
                                                , std::ostream & os
                                                , sdc::Documents<p348reco::RunNo_t> & docs
                                                ) {
    if( cfg.typeName == sdc::CalibDataTraits<p348reco::CaloCellCalib>::typeName ) {
        resolve_srcinf_query_and_dump_updates<p348reco::RunNo_t
                    , p348reco::CaloCellCalib>(cfg, os, docs);
    } else {
        throw std::runtime_error("Requested calibration data type is"
                " unknown or not indexed with keys of current type." );
    }
}

template<> void
resolve_updates_type_and_dump<p348reco::Datetime_t>( const AppCfg & cfg
                            , std::ostream & os
                            , sdc::Documents<p348reco::Datetime_t> & docs
                            ) {
    if( cfg.typeName == sdc::CalibDataTraits<p348reco::APVTimingCalib>::typeName ) {
        resolve_srcinf_query_and_dump_updates<p348reco::Datetime_t
                    , p348reco::APVTimingCalib >(cfg, os, docs);
    } else if( cfg.typeName == sdc::CalibDataTraits<p348reco::APVPedestalChannelEntry>::typeName ) {
        resolve_srcinf_query_and_dump_updates<p348reco::Datetime_t
                    , p348reco::APVPedestalChannelEntry >(cfg, os, docs);
    } else {
        throw std::runtime_error("Requested calibration data type is"
                " unknown or not indexed with keys of current type." );
    }
}

template<typename T> struct AppKeyTraits;

template<> struct AppKeyTraits<p348reco::RunNo_t> {
    typedef KeyTraits<p348reco::RunNo_t>::LookupObject LookupObject;

    static std::shared_ptr<LookupObject> instantiate_lookup_object(const AppCfg & cfg) {
        return p348reco::KeyTraits<p348reco::RunNo_t>::instantiate_lookup_object<p348reco::Datetime_t>(
                    cfg.paths, cfg.acceptPatterns, cfg.rejectPatterns,
                    cfg.minSize, cfg.maxSize );
    }

    template<template <class> class T> static std::shared_ptr<T<p348reco::RunNo_t>>
        instantiate_loader(const AppCfg & cfg) {
        auto p = std::make_shared<T<p348reco::RunNo_t>>();
        p->grammar = cfg.loaderParams.grammar;
        if( ! cfg.loaderParams.defaults.validityRange.empty() ) {
            p->defaults.validityRange
                = sdc::aux::lexical_cast<sdc::ValidityRange<p348reco::RunNo_t>>(
                        cfg.loaderParams.defaults.validityRange);
        } else {
            p->defaults.validityRange = { sdc::ValidityTraits<p348reco::RunNo_t>::unset
                                        , sdc::ValidityTraits<p348reco::RunNo_t>::unset
                                        };
        }
        p->defaults.dataType = cfg.loaderParams.defaults.dataType;
        return p;
    }
};

template<> struct AppKeyTraits<p348reco::Datetime_t> {
    typedef KeyTraits<p348reco::Datetime_t>::LookupObject LookupObject;

    static std::shared_ptr<LookupObject> instantiate_lookup_object(const AppCfg & cfg) {
        return p348reco::KeyTraits<p348reco::Datetime_t>::instantiate_lookup_object<p348reco::Datetime_t>(
                    cfg.paths, cfg.acceptPatterns, cfg.rejectPatterns,
                    cfg.minSize, cfg.maxSize );
    }

    template<template <class> class T> static std::shared_ptr<T<p348reco::Datetime_t>>
        instantiate_loader(const AppCfg & cfg) {
        auto p = std::make_shared<T<p348reco::Datetime_t>>();
        p->grammar.commentChar      = cfg.loaderParams.grammar.commentChar    ;
        p->grammar.metadataMarker   = cfg.loaderParams.grammar.metadataMarker ;
        p->grammar.metadataKeyTag   = cfg.loaderParams.grammar.metadataKeyTag ;
        p->grammar.metadataTypeTag  = cfg.loaderParams.grammar.metadataTypeTag;
        if( ! cfg.loaderParams.defaults.validityRange.empty() ) {
            p->defaults.validityRange
                = sdc::aux::lexical_cast<sdc::ValidityRange<p348reco::Datetime_t>>(
                        cfg.loaderParams.defaults.validityRange);
        } else {
            p->defaults.validityRange = { sdc::ValidityTraits<p348reco::Datetime_t>::unset
                                        , sdc::ValidityTraits<p348reco::Datetime_t>::unset
                                        };
        }
        p->defaults.dataType = cfg.loaderParams.defaults.dataType;
        return p;
    }
};

// specialize other than ExtCSV loaders like:
//template<>
//std::shared_ptr<sdc::Documents<p348reco::RunNo_t>::iLoader>
//KeyTraits<p348reco::RunNo_t>::instantiate_loader<sdc::ExtCSVLoader>(const AppCfg & cfg) {
//    return std::make_shared
//}

template<typename T>
void dump( const sdc::ExtCSVLoader<T> & l, std::ostream & os ) {
    os << "\"grammar\":{\"commentChar\":";
    if( '\0' != l.grammar.commentChar ) {
        os << "\"" << l.grammar.commentChar << "\"";
    } else {
        os << "null";
    }
    os << ",\"metadataMarker\":";
    if( '\0' != l.grammar.metadataMarker ) {
        os << "\"" << l.grammar.metadataMarker << "\"";
    } else {
        os << "null";
    }
    os << ",\"metadataKeyTag\":\"" << l.grammar.metadataKeyTag << "\""
       << ",\"metadataTypeTag\":\"" << l.grammar.metadataTypeTag << "\""
       ;
    os << "}";  // grammar
}

template< typename KeyT > int
testing_procedure_generic_local( const AppCfg & cfg, std::ostream * osPtr ) {
    // instantiate documents
    sdc::Documents<KeyT> docs;
    // add loader to documents
    auto loaderPtr = AppKeyTraits<KeyT>::template instantiate_loader<sdc::ExtCSVLoader>(cfg);
    docs.loaders.push_back(loaderPtr);  // TODO: other loaders
    // instantiate lookup object
    auto fs = AppKeyTraits<KeyT>::instantiate_lookup_object(cfg);
    // dump loader
    if( osPtr ) {
        auto & os = *osPtr;
        os << ",\"loaders\":{"
           << "\"" << loaderPtr.get() << "\":{"
           ;
        os << "\"defaults\":{"
           << "\"dataType\":\"" << loaderPtr->defaults.dataType  << "\","
           << "\"validityRange\":\"" << loaderPtr->defaults.validityRange << "\","
           << "\"baseMD\":{"
           ;
        bool isFirstMDEntry = true;
        for( auto p : loaderPtr->defaults.baseMD ) {
            if(!isFirstMDEntry) os << ","; else isFirstMDEntry = false;
            os << "\"" << p.first << "\":[" << p.second.first << ",\"" << p.second.second << "\"]";
        }
        os << "}},";  // "baseMD", ""defaults
        dump(*loaderPtr, *osPtr);
        (*osPtr) << "}}";
    }

    // collect files into documents using lookup object
    size_t nAdded = docs.add_from(*fs);
    if( osPtr ) {
        (*osPtr) << ",\"addedFiles\":" << nAdded;
    }
    // dump index of collected documents if need
    if( osPtr && cfg.doPrintCollection ) {
        auto & os = *osPtr;
        os << ",\"documentsCollection\":";
        docs.dump_to_json(*(osPtr));
    }

    // we shall deserialize and serialize key here to explicitly show app state
    // here
    if( osPtr ) {
        auto & os = *osPtr;
        os << ",\"lookupKey\":";
        if( cfg.runID.empty() ) {
            os << "null";
        } else {
            auto k = sdc::ValidityTraits<KeyT>::from_string(cfg.runID);
            if( !sdc::ValidityTraits<KeyT>::is_set(k) ) {
                os << "[\"" << cfg.runID << "\",null]";
            } else {
                os << "\"" << sdc::ValidityTraits<KeyT>::to_string(k) << "\"";
            }
        }
    }

    if( !( cfg.typeName.empty()
         && ( (!cfg.runID.empty())
            && sdc::ValidityTraits<KeyT>::is_set(sdc::ValidityTraits<KeyT>::from_string(cfg.runID))
            )
         )
      ) {
        if( osPtr ) {
            *osPtr << ",\"update\":{";
        }
        resolve_updates_type_and_dump<KeyT>( cfg, *osPtr, docs );
        if( osPtr ) {
            *osPtr << "}";
        }
    } else if(  cfg.typeName.empty()
             != ( cfg.runID.empty()  // XOR
                  || !sdc::ValidityTraits<KeyT>::is_set(sdc::ValidityTraits<KeyT>::from_string(cfg.runID)) )
             ) {
        std::cerr << "Error: calib data type name is set, but no key is"
                " set to query data." << std::endl;
            return -1;
    }
    return 0;
}

int
main(int argc, char * argv[]) {
    // disable silly ROOT sighandlers
    for( int sig = 0; sig < kMAXSIGNALS; sig++) {
        gSystem->ResetSignal((ESignals)sig);
    }
    AppCfg cfg = { "specs/01"  // path to discover
                 , "" //"*.txt:*.sdc" // accept patterns
                 , "" //"*.swp:*.swo"  // reject patterns
                 , "" //"runNo"  // key type to operate with
                 // ... default validity period?
                 , 0  // min size, bytes
                 , 10*1024*1024  // max size, bytes
                 , ""  // typename
                 , false // incremental update
                 , false // keep source info
                 , true  // do print collection
                 // ...
                 };
    int rc = configure_app(cfg, argc, argv);
    if( rc < 0 ) {
        std::cerr << "App config failed. Exit." << std::endl;
        exit(EXIT_FAILURE);
    } else if( rc != 0 ) {
        exit(EXIT_SUCCESS);
    }

    std::cout << "{\"appConfig\":";
    dump(cfg, std::cout);
    if( cfg.paths.empty() ) goto finish;
    // discover files
    // if no key type provided -- use basic loader and that's it
    if( cfg.keyType.empty() ) {
        sdc::aux::FS fs( cfg.paths
                       , cfg.acceptPatterns
                       , cfg.rejectPatterns
                       , cfg.minSize, cfg.maxSize
                       );
        std::cout << ",\"files\":[";
        {
            std::string filepath;
            bool isFirst = true;
            while(!(filepath = fs()).empty()) {
                if(!isFirst) std::cout << ","; else isFirst = false;
                std::cout << "\"" << filepath << "\"";
            }
        }
        std::cout << "]";
        goto finish;
    }
    // otherwise, if key type provided, forward to appropriate testing routine
    if( cfg.keyType == "runNo" ) {
        rc = testing_procedure_generic_local<p348reco::RunNo_t>(cfg, &std::cout);
    } else if( cfg.keyType == "datetime" ) {
        rc = testing_procedure_generic_local<p348reco::Datetime_t>(cfg, &std::cout);
    // } else if( cfg.keyType == ... ) { ...
    } else {
        throw std::runtime_error("Bad validity key.");  // assert(false)
    }
finish:
    std::cout << "}" << std::endl;  // entire app state object
    return rc;
}

