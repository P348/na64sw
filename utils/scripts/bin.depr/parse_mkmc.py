import sys, re, json

# File structure
# ~~~~~~~~~~~~~~
#
# M. Kirsanovs `CaloHits.d' files are ASCII serialization of the Monte-Carlo
# runs modelled with Geant4. File consists of the header and body, separated
# with tags. Each tag occupies single line and the value(s) are provided above.
# There are at least three types of tags:
#   1. VERSION has no closing tag and is immediately followed by plain integer
# value
#   2. RUN...ENDRUN is a section where model-dependent information is listed in
# form of key-value pairs. One may denote some effort to establish a folded
# structure with colon (`:') delimeter used within some of the names.
#   3. EVENT...ENDEVENT is an event structure declaration. Encompasses a list
# of arbitrary valued sub-tags like "ECAL", "HCAL", etc.
# Example:
#   VERSION
#   9
#   RUN
#   geometry_version=<int>
#   setup_number=<int>
#   BeamNominalEnergy=<int,GeV>
#   <NS:string>:<parName:string>=<value>
#   ...
#   ENDRUN
#   EVENT
#   ...
#   ENDEVENT

class Parser8(object):
    def __init__(self, f):
        self._f = f
        self._lineNo = 2
        self._rxRunKVPair = re.compile(r'([\w:]+)=(.+)')

    def rl(self, expectEnd=False):
        while True:
            self._lineNo += 1
            l = self._f.readline()
            if not len(l):
                if not expectEnd:
                    raise RuntimeError( 'Unexpected end of file.' )
                else:
                    return None
            l = l.strip()
            if l:
                #print('%5d <'%self._lineNo, l)
                break
        return l

    def to_dict(self):
        """
        Entry point -- parses file of certain version.
        """
        r = {'events':[]}
        runTag = self.rl()
        if 'RUN' != runTag:
            raise RuntimeError( 'Expected `RUN\' tag, got: "%s".'%runTag )
        # Read run info header
        runInfo = []
        while True:
            line = self.rl()
            if 'ENDRUN' == line: break  # <-- exit loop
            # parse key/value pair
            m = self._rxRunKVPair.match(line)
            if not m:
                raise RuntimeError( 'Line "%s" does not match key/value regex.'%line )
            runInfo.append( m.groups() )
        r['runInfo'] = dict(runInfo)
        # Read events
        while True:
            evInfo = self.read_event()
            if not evInfo: break  # <-- exit loop
            r['events'].append(evInfo)
        return r


    def read_event(self):
        """
        Reads individual events; dispatches reading of particular detector
        digits to appropriate methods.
        """
        evBgnTag, evNumbering = self.rl(expectEnd=True), self.rl(expectEnd=True)
        if evBgnTag is None:
            return None  # EOF
        if 'EVENT' != evBgnTag:
            raise RuntimeError('Expected "EVENT" tag, got "%s"'%evBgnTag)
        ei = { 'eventMeta' : dict(zip(('IEvent', 'IEventOut', 'AWeight'), tuple(int(n) for n in evNumbering.split()))) }
        while True:
            tag = self.rl()
            if 'ENDEVENT' == tag:
                break
            parsingMethod = getattr( self, 'read_det_' + tag.lower(), None )
            if parsingMethod is None:
                print( 'No method to parse "%s". Skipped.'%tag )
                continue
            ei[tag.lower()] = parsingMethod()
        return ei

    def read_det_mctruth(self):
        """
        MCTRUTH block consists of two lines, with 2 and 8 floating point
        numbers:
        XECAL, YECAL
        beamX0, beamY0, beamZ0, beamPDG, E_{beam total,GeV}, P_{beam, x}, P_{beam, y}, P_{beam, z}
        TODO: semantics?
        """
        r = dict(zip(('ecalX', 'ecalY'), tuple(float(x) for x in self.rl().split())))
        r.update(dict(zip( ('beamX', 'beamY', 'beamZ', 'beamPDG', 'Etot', 'beamPx', 'beamPy', 'beamPz')
                         , tuple(float(x) for x in self.rl().split()) )))
        r['beamPDG'] = int(r['beamPDG'])
        return r

    def read_det_ecal(self):
        """
        ECAL block is two lines with 36 floating point numbers in a an each
        line, corresponding to detector cells. Preshower, then main part.
        X-index changes within line, Y-index corresponds to relative
        line number.
        TODO: use order and semantics.
        """
        #l1, l2 = self.rl(), self.rl()
        r = []
        for isMainPart, l in ((0, self.rl().split()), (1, self.rl().split())):
            cnt = 0
            for yIdx in range(6):
                for xIdx in range(6):
                    edepInCell = float(l[cnt])
                    if edepInCell:
                        r.append([isMainPart, xIdx, yIdx, edepInCell])
                    cnt += 1
        return r

    def read_det_hcal(self):
        """
        HCAL is a single line with 3x3x4=36 floating point numbers
        corresponding to 4 3x3 cells modules.
        Indeces are (x, y, module) -- (1, 1, 1), (2, 1, 1), ... (3, 3, 4)
        TODO: use order and semantics
        """
        l = self.rl()
        nums = tuple(float(x) for x in l.split())
        r = []
        cnt = 0
        for nMod in range(4):
            for yIdx in range(3):
                for xIdx in range(3):
                    if nums[cnt]:
                        r.append( (nMod, xIdx, yIdx, nums[cnt]) )
                    cnt += 1
        return r

    def read_det_srd(self):
        """
        SRD is a single line of 8 floating point numbers -- energy deposition
        in SRD cell.
        TODO: meaning?
        """
        l = self.rl()
        return tuple(float(x) for x in l.split())

    def read_det_veto(self):
        """
        VETO is a single line of 3 floating point numbers.
        TODO: semantics?
        """
        l = self.rl()
        return tuple(float(x) for x in l.split())

    def read_det_mm(self):
        """
        MM has somewhat more complex format: one line of 8 integers +
        number of lines that is equal to sum of the numbers in a first line.
        XMM:float YMM:float ZMM:float EDep:float IDPDGMM:int IDTrackMM:int EKinMM:float
        """
        nHitsArr = tuple( int(x) for x in self.rl().split())
        r = []
        for nHits in nHitsArr:
            stHits = []
            for nHit in range(nHits):
                d = dict(zip( ('X', 'Y', 'Z', 'EDep', 'IDPDG', 'IDTrack', 'EKinMM' )
                            , (float(v) for v in self.rl().split())
                            )
                        )
                d['IDPDG'] = int(d['IDPDG'])
                d['IDTrack'] = int(d['IDTrack'])
                stHits.append( d )
            r.append(stHits)
        return r


gParsers = {
    8 : Parser8
}

def parse_mk_mc_file( f, forceVersionNo=None ):
    """
    Uses `gParsers' global index of parsers to delegate file content parsing to
    appropriate parser.
    """
    versionTag, versionNo = f.readline().strip(), f.readline().strip()
    if 'VERSION' != versionTag:
        raise RuntimeError('File does not start with `VERSION\' tag: "%s"'%versionTag)
    if not versionNo.isdigit():
        raise RuntimeError('Unable to interpret `VERSION\' tag value: "%s".'%versionNo)
    versionNo = int(versionNo)
    if forceVersionNo is None \
    and versionNo not in gParsers.keys():
        # TODO: print warning, find closest and continue instead of error
        raise RuntimeError('Unable to parse format of version %d'%versionNo)
    P = gParsers[versionNo] if forceVersionNo is None else gParsers[forceVersionNo]
    return P(f).to_dict()

if "__main__" == __name__:
    with open(sys.argv[1]) as f:
        data = parse_mk_mc_file(f)  # TODO: user may provide forceVersionNo to override
    print( json.dumps( data, indent=2, sort_keys=True ) )

