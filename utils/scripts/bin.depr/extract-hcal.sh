#!/bin/bash

for i in `seq 0 $1` ; do
    if [ -s CaloHit.$i.d ] ; then
        awk '/HCAL$/ { getline; print $0 }' CaloHit.$i.d > CaloHit.$i.HCAL.txt
    fi
done

