#!/bin/bash

# This example demonstrates a writing of compressed pre-processed events with
# `pipe' util. Usage:
#   $ write-compressed.sh <.bz2 filename> <pipe-args...>

# The "named pipe" location
export PIPEF=/tmp/plainout

# Set the BASH trap intercepting output; this will remove pipe in case of exit
trap "rm -f $PIPEF" EXIT

# Create the named pipe if it does exist
if [[ ! -p $PIPEF ]] ; then
    mkfifo $PIPEF
else
    (>&2 echo "Pipe \"$PIPEF\" exists.")
    exit
fi

# Run the compression util
bzip2 -c < $PIPEF > $1 &
COMPRESSOR_PID=$!
echo "Compressor process forked; PID=$COMPRESSOR_PID"

# Drop the first command line argument and run the data processing util with
# rest arguments
shift
./pipe $@ #3> $PIPEF

