import sys, re, gzip
import pandas as pd

rxTaggedLine = re.compile(r'^([a-zA-Z]\w*)\s([^\s]+)\s*$')
rxDetName = re.compile(r'^([A-Za-z]{1,5})(\d+)([a-zA-Z][12]?)?_*$')

def normalize_det_name(dn):
    """
    Some detector names in alignment data are formatted slightly
    differently than ones provided in placements.txt. This assures
    that names match.
    """
    m = rxDetName.match(dn)
    if not m:
        raise RuntimeError(f'"{dn}" does not match expected det name format.')
    kin, num, tail = m.groups()
    kin = kin.upper()
    if tail:
        tail = tail.upper()
    else:
        tail = ''
    num = '%02d'%int(num)
    return kin + num + tail

def alignment_data( filename, alpha=None ):
    """
    Generator function reading output of `CollectAlignmentData` handler.
    Returns a tuple to
    """
    eventID, trackID, chi2, ndf, pv = (None,)*5
    with (open(filename, 'r') if not filename.endswith('.gz') else gzip.open(filename, mode='rt')) as f:
        for lineNo, line in enumerate(f):
            # Check if this line is own track properties
            if line.startswith('chi2'):
                # this is a track parameters
                _, chi2, _, ndf, _, pv, _, mom = line.split()
                pv = float(pv)
                mom = float(mom)
                continue
            # Check if this line is tagged line
            taggedLineMatch = rxTaggedLine.match(line)
            if taggedLineMatch:
                tag, val = taggedLineMatch.groups()
                if 'Event' == tag:
                    eventID = val
                elif 'Track' == tag:
                    trackID = val
                else:
                    raise RuntimeError(f'{filename}:{lineNo}: nexpected tag: "{tag}"')
                continue
            # Otherwise, this must be a score line of te form:
            #   <detID:str> <measurement> <unbiased> <biased> [weight]
            splitted = line.split()
            if len(splitted) == 4:
                detID, msrmnt, unbiased, biased = line.split()
                weight = 1
            else:
                detID, msrmnt, unbiased, biased, weight = line.split()
            detID = normalize_det_name(detID)
            if not alpha or alpha <= pv:
                yield ( int(eventID), int(trackID)
                      , float(chi2), float(ndf)
                      , pv
                      , mom
                      , detID, float(msrmnt)
                      , float(unbiased), float(biased)
                      , float(weight)
                    )

def alignment_data_to_pd(filename, alpha=None):
    """
    Reads given output file produced by handler `CollectAlignmentData` into
    two dataframes returned: a tracks dataframe and scores dataframe.

    Alpha value can be set to restrict reading with certain level of
    significance.
    """
    # Read data into dataframe
    df = pd.DataFrame( alignment_data(filename, alpha=alpha)
                     , columns=('event', 'track', 'chi2', 'ndf', 'p', 'mom', 'det', 'u', 'du_u', 'du_b', 'weight')
                     )
    # convert length values to mm (original is cm)
    df.u *= 10
    df.du_u *= 10
    df.du_b *= 10
    #return None, df  # XXX
    # Make scores dataframe
    #scoresDF = df.pivot( index=['event', 'track']
    #                   , columns=['det']
    #                   , values=['u', 'du_b', 'du_u']
    #                   )
    scoresDF = pd.pivot_table(df, index=['event', 'track']
          , columns=['det']
          , values=['u', 'du_b', 'du_u', 'weight'])
    scoresDF = scoresDF.reorder_levels([1,0], 1).sort_index(axis=1)
    # Make tracks dataframe
    df.drop(columns=['det', 'u', 'du_b', 'du_u', 'weight'], inplace=True)
    df.set_index(['event', 'track'], inplace=True)
    df = df[~df.index.duplicated(keep='first')]
    return df, scoresDF

#tracksDF, scoresDF = alignment_data_to_pd(sys.argv[1], alpha=0.005)

