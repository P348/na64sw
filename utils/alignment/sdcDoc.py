"""
Drafts for SDC document reading handles.
"""

import math
import re, copy
import pandas as pd
import pandas.api
import pandas.api.types
import scipy.spatial.transform
import numpy as np

gRxFP = re.compile(r'^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$')
# Regular expression for command-line interface. Accpts expressions lik:
#   ECAL:x-10.3cm -> detName=ECAL, valueName=x, value=-10.3, units=cm
#   MM03X:U+.23mm ...
#   S1:az+2e-3rad
gRxModification = re.compile(r'^(?P<detName>[A-Za-z]\w*):(?P<valueName>a?[XYZxyzUV])(?P<value>[+-][\d]*\.?\d*(?:[Ee][+-]\d+)?)(?P<units>(?:deg|rad|(?:[mcu]?m)))$')
# Set of length values
gLengthValues = set(('x', 'y', 'z', 'sx', 'sy', 'sz', 'resolution'))
# In mm
gLengthFactors = { 'cm' : 10, 'mm' : 1, 'm' : 1000 }
gDefaultLengthUnit='cm'

class SDCFile(object):
    """
    Base class to handle SDC document from Python.
    """
    def __init__(self, docURI):  # TODO: support grammar and defaults as in C++
        self.md = {}  # metadata dictionary
        self.docURI = docURI
        self.rxMetadata = re.compile(r'^([a-zA-Z][\w.]*)\s*=\s*(.+[^\s]*)$')
        self.lines = []
        #self.cCols = None
        # elements of this list will be pairs of the metadata copy and
        # list of the data lines
        self.entries = []
        self.lastWasCSV = False
        # Read document
        self._read_sdc()

    def _parse_md(self, tag, value, nLine):
        # This (base) class is mostly interested in certain common
        # modifiers, like `columns`
        #if 'columns' == tag:
        #    self.cCols = tuple(cName.strip() for cName in value.split(','))
        # ... TODO: handle other common metadata
        self.md[tag] = value
        self.lastWasCSV = False

    def _parse_data_line(self, line, nLine):
        values = list(v.strip() for v in line.split())
        if not self.lastWasCSV:
            self.entries.append(( copy.deepcopy(self.md), [(nLine, values)] ))
        else:
            self.entries[-1][1].append((nLine, values))
        self.lastWasCSV = True


    def _read_sdc(self):
        """
        Reads SDC document properly addressing the calls.
        """
        with open(self.docURI) as f:
            for nLine, fullLine in enumerate(f):
                line = fullLine.split('#')[0].strip()
                metadataMatch = self.rxMetadata.match(line)
                if metadataMatch:  # metadata line
                    self.lines.append(('m', fullLine))
                    self._parse_md(*metadataMatch.groups(), nLine)
                elif not line or line.isspace():  # empty/whitespaces only line
                    self.lines.append(('w', fullLine))
                elif len(line.split()) > 2:  # apparently CSV
                    self.lines.append(('d', (nLine, fullLine)))
                    self._parse_data_line(fullLine, nLine)
                else:
                    raise RuntimeError(f'{self.docURI}:{nLine} can not parse expression "{line}"')



def _mk_df_from_entry( state, entries, columnTypes=None
                     , lengthFactor=10  # cm by default
                     , correctionSuffix='_corr'):
    assert 'columns' in state
    # Regular expression to match binary operations like <val>+/-<corr>
    rxBinExpr = re.compile(
        r'^([-+]?\s*[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\s*([-+]\s*[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)$')
    if not columnTypes:
        columnTypes = {'name': str}
    cols = (c.strip() for c in state['columns'].split(','))
    cols = [ (c, columnTypes[c] if c in columnTypes else float)
             for c in cols ]
    def _to_dict(values):
        # truncate values to columns number
        values = values[:len(cols)]
        rvs = {}
        for n, v in enumerate(values):
            col = cols[n]
            if col[1] is str:
                rvs[col[0]] = v
            else:
                assert col[1] in (float, int)
                # Check if the string value is floating point
                # number
                if gRxFP.match(v):
                    rvs[col[0]] = col[1](v)
                else:
                    # Then it must be an aritmetic expression, probably of
                    # the form <baseValue>[+-]<correction>, so we try to
                    # interpret it this way first
                    binOpMatch = rxBinExpr.match(v)
                    if binOpMatch:
                        rvs[col[0]], rvs[col[0] + correctionSuffix] \
                                = (col[1](tok) for tok in binOpMatch.groups())
                    else:
                        # It is apparently an arithmetic expression that we'll
                        # try to eval here
                        rvs[col[0]] = col[1](eval(v))
        return rvs
    lengthKeys = copy.copy(gLengthValues) \
               | set(c + correctionSuffix for c in gLengthValues)
    # make and yield dict
    for nLine, values in entries:
        #print(values)  # XXX
        if len(values) < len(cols):
            # Number of values in data block can not be less than
            # number of columns
            raise RuntimeError(f'Number of values {len(values)} in data block'
                               f' is less than current number of'
                               f' columns {len(cols)}'
                              )
            #gRxFP.match()
        dct = _to_dict(values)
        for k in lengthKeys & dct.keys():
            dct[k] *= lengthFactor
        dct['_line'] = nLine
        yield dct

def du_to_xy_shift( dU, dV=0., dW=0.
                  , ax=0., ay=0., az=0.
                  ):
    """
    Calculates (global) X/Y offset based on (local) dU offset taking into
    account rotation angles.
    """
    # Calculate full rotation matrix (global to local)
    rot = scipy.spatial.transform.Rotation.from_euler( 'zyx'
        , [az, ay, ax], degrees=True )
    # ^^^ NOTE: extrinsic (letters) rotations are used, as defined for Euler
    # angles.
    rotM = rot.as_matrix()
    # Get inverted matrix
    #invRotM = np.linalg.inv(rotM)
    # Get shift value by applying reversed rotation transform
    # to shift vector
    return rotM.dot(np.array([dU, dV, dW]))

class Placements(SDCFile):
    """
    Handler for placements SDC document.

    Offers a way to impose alignment variables into the
    placements document and save the modified copy.

    A special feature is to preserve columns with simple
    corrections, like "2+3" or "-1.34e-2+0.002".
    """
    def __init__( self, docURI
                , correctionSuffix='_corr'
                , columnTypes=None
                ):
        """
        Creates a handle for placements SDC document to impose
        alignment corrections.
        """
        super().__init__(docURI)
        if not columnTypes:
            self.columnTypes = { 'name': (str, '6'), 'nWires': (int, '5') }
        self.corrSuffix = correctionSuffix
        # We create dataframes here to keep values for detectors
        self.dfs = {}
        for state, entries in self.entries:
            # Skip irrelevant data types (TODO: warn user)
            if not state or state['type'] != 'placements': continue
            # Get the hashable tuple of runs range to address dataframes
            assert 'runs' in state
            runsRange = state['runs'].split('-')
            assert 2 == len(runsRange)
            if runsRange[0] == '...':
                runsRange[0] = -1
            else:
                runsRange[0] = int(runsRange[0])
            if runsRange[1] == '...':
                runsRange[1] = -1
            else:
                runsRange[1] = int(runsRange[1])
            runsRange = tuple(runsRange)
            newDF = pd.DataFrame(
                    _mk_df_from_entry( state, entries, correctionSuffix=self.corrSuffix
                        , lengthFactor=gLengthFactors[state.get('units.length', gDefaultLengthUnit)]
                        , columnTypes={ k: v[0] for k,v in self.columnTypes.items() }
                                     )).set_index('name')
            if runsRange not in self.dfs:
                self.dfs[runsRange] = []
            self.dfs[runsRange].append(newDF)
        # Concat collected DFs
        for runsRange in self.dfs.keys():
            self.dfs[runsRange] = pd.concat(self.dfs[runsRange])
            # fill "correction" columns with zeroes (as it
            # is intended by meaning)
            corrCols = list(c for c in self.dfs[runsRange] if c.endswith(self.corrSuffix))
            self.dfs[runsRange][corrCols] \
                = self.dfs[runsRange][corrCols].fillna(value=0)

    def _impose_correction( self, detName, var, value
                          , runsRange=None
                          ):
        """
        Imposes correction to a variable ("x", "y", "az", etc)
        of certain detector. It does NOT consider reference systems and just
        adds given value to appropriate column of certain dataframe.
        """
        # Assure the runs range is either given, or is a single one
        # within this document
        if not runsRange:
            if len(self.dfs) != 1:
                raise RuntimeError('Explicit runs range needed for correction.')
            runsRange = list(self.dfs.keys())[0]
        # Obtain list of detector to apply modification
        if type(detName) in (list, tuple, set):
            detNames = list(detName)
        elif type(detName) is str:
            detNames = list(nm for nm in self.dfs[runsRange].index if nm.startswith(detName))
        else:
            raise RuntimeError('Can not interpret %s as detector name'%str(detName))
        # Now decide which column actually to correct: if `..._corr`
        # exists, correct it
        assert type(var) is str
        if var + self.corrSuffix in self.dfs[runsRange].columns:
            colName = var + self.corrSuffix
        else:
            colName = var
        self.dfs[runsRange].loc[detNames, colName] += value

    def apply_corrections(self, fitResults, runsRange=None):
        """
        Takes dictionary of pairs (<detName:str>: [(<parName:str>, <value:float>, <err:float>), ...]).
        Somewhat compatible to the output of residuals fitting procedure.
        Here the <parName:str> is expected to be one of:
            - "angle" -- will affect `az' column of dataframe for the detector
            - "position" -- will affect `x', `y`, `z' columns
            (more may be added later)
        For "position" performs non-trivial transformation of local offset to
        global X/Y shift respecting rotation angles of the plane.
        """
        # Assure the runs range is either given, or is a single one
        # within this document
        if not runsRange:
            if len(self.dfs) != 1:
                raise RuntimeError('Explicit runs range needed for correction.')
            runsRange = list(self.dfs.keys())[0]
        # Iterate over detectors
        for detName, correctionsDict in fitResults.items():
            for corrType, (corrValue, corrErr) in correctionsDict.items():
                #print(f"Will correct {corrType} of {detName} by {corrValue} {corrErr}")
                if 'angle' == corrType:
                    # NOTE: angle is expected to be k (tan) here!
                    # To turn it into degrees, one has to take atan from it
                    corrValue = math.degrees(math.atan(corrValue)) 
                    # TODO: this hunk must be moved:
                    ax, ay, az = self.dfs[runsRange].loc[detName][["ax", "ay", "az"]]
                    rGlobal = scipy.spatial.transform.Rotation.from_euler( 'zyx', [az, ay, ax], degrees=True)
                    rLoc = scipy.spatial.transform.Rotation.from_euler( 'zyx', [corrValue, 0, 0], degrees=True)
                    mat = np.dot( rGlobal.as_matrix(), rLoc.as_matrix() )  # TODO: is order correct?
                    rRes = scipy.spatial.transform.Rotation.from_matrix(mat).as_euler( 'zyx', degrees=True )
                    # TODO: check sign here:
                    self._impose_correction( detName, 'az', rRes[0] - az, runsRange=runsRange )
                    self._impose_correction( detName, 'ay', rRes[1] - ay, runsRange=runsRange )
                    self._impose_correction( detName, 'ax', rRes[2] - ax, runsRange=runsRange )
                elif 'position' == corrType:
                    # Get detector entry and its angles, translate "position
                    # correction" from local to global and impose it
                    assert detName in self.dfs[runsRange].index, f'No det "{detName}"'
                    ax, ay, az = self.dfs[runsRange].loc[detName][["ax", "ay", "az"]]
                    for axis, cv in \
                            dict(zip('xyz', du_to_xy_shift( -corrValue  # <- du !
                                , ax=ax, ay=ay, az=az ))).items():
                        self._impose_correction(detName, axis, cv, runsRange=runsRange)
                elif 'pitch' == corrType:
                    # TODO: correct Z relying on pitch difference
                    print('WARNING: skipping pitch correction (not implemented)')

    def _make_data_line(self, le, columnFormats=None):
        """
        Special method to write the data line into the placements SDC
        doc: performs reverse lookup of line number into dataframe entry
        formats and writes data line.
        """
        if not columnFormats:
            columnFormats = { 'name' : (str, '') }
        nLine = le[0]
        dfe = None  # set to entry
        for runId, df in self.dfs.items():
            dfEntry = df.loc[df._line == nLine]
            if dfEntry.empty: continue
            assert dfEntry.shape[0] == 1
            dfe = dfEntry
            break
        assert dfe is not None, f'Integrity error: no DF entry for line' \
                f' number {nLine}'
        # We need to get the metadata instance for this line to retrieve
        # columns order. MD is saved in the `entries` list, but we did not
        # saved any helpful index to retrieve it fast.
        # TODO: optimize this?
        thisLineMD = None
        for md, nLineAndValues in self.entries:
            for cnLine, _ in nLineAndValues:
                if nLine == cnLine:
                    thisLineMD = md
                    break
            if thisLineMD is not None:
                break
        assert thisLineMD is not None, "Integrity error: no MD object for this line"
        colsOrder = list(c.strip() for c in thisLineMD['columns'].split(','))
        lengthFactor=gLengthFactors[thisLineMD.get('units.length', gDefaultLengthUnit)]
        lengthKeys = copy.copy(gLengthValues) \
                   | set(c + self.corrSuffix for c in gLengthValues)
        # Generate the format string (a template)
        fmtStr = ''
        for colName in colsOrder:
            fmtStr += '{' + colName
            if colName in columnFormats.keys():
                fmtStr += ':' + columnFormats[colName][1] + '}'
            else:
                # get datatype from series
                dt = dfe.dtypes[colName]
                if pd.api.types.is_string_dtype(dt):
                    fmtStr += ':6}'
                elif pd.api.types.is_float_dtype(dt):
                    fmtStr += ':9.4f}'
                    if colName + self.corrSuffix in dfe.columns:
                        fmtStr += '{%s:<+'%(colName + self.corrSuffix) + '6.4e}'
                elif pd.api.types.is_integer_dtype(dt):
                    fmtStr += ':6}'
                    if colName + self.corrSuffix in dfe.columns:
                        fmtStr += '{%s:<+'%(colName + self.corrSuffix) + '6}'
                else:
                    raise RuntimeError("Can't handle data type %s"%str(dt))
            fmtStr += ' '
        fmtStr = fmtStr[:-1]
        dct = dfe.reset_index().to_dict('records')
        assert len(dct) == 1
        dct = dct[0]
        for k in dct.keys() & lengthKeys:
            dct[k] /= lengthFactor
        return fmtStr.format(**dct)


    def save_as(self, fn, mergeCorrections=True):
        """
        Writes the values into another SDC document.
        """
        with open(fn, 'w') as f:
            for lineType, lineEntry in self.lines:
                if 'd' != lineType:
                    # keep non-data lines intact
                    f.write(lineEntry)
                    continue
                # write data line using a dedicated method
                f.write(self._make_data_line(lineEntry, columnFormats=self.columnTypes) + '\n')

def main():
    """
    Entry point for the module running script.

    Syntax:
        $ argv[0] <infile> [--no-match-centers] [OPTIONS] <outfile>

    Modifies placements provided in file <infile> writing the result to
    <outfile>. OPTIONS for "modify" must be list in form "det-name:var+delta",
    for instance:
        GM03X:U+10mm   -- moves GM03X plane by 10mm along U (local X) axis
        GM04:Y+1.32cm  -- moves GM04 station (both planes GM04X and GM04Y)
                          by +1.32 cm in global frame's Y
    Note: `--no-match-centers' may be given to prevent script from running
    centers matching procedure.
    """
    #import argparse
    #p = argparse.ArgumentParser(description"NA64sw Placements document"
    #        " parser/modifier tool (a Python script).")
    #p.add_argument

if "__main__" == __name__:
    main()

