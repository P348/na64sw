"""
Various plotting functions for alignment QA.
"""

import seaborn as sns
import statsmodels.formula.api as sm
import matplotlib
import matplotlib.pyplot as plt
import math as m
import numpy as np
import pandas as pd
from matplotlib.colors import LogNorm
from scipy.stats import norm

#                           * * *   * * *   * * *

def get_plane_name(df, n=0):
    """
    Returns first (or other of n is specified) name of the plane in the
    provided dataframe.
    If n is None, returns sorted list of the plane names in the dataframe
    """
    if n is None:
        return list(sorted(set([p[0] for p in df.columns if not p[0].startswith('track')])))
    else:
        return tuple(sorted(set([p[0] for p in df.columns if not p[0].startswith('track')])))[n]
#                           * * *   * * *   * * *

def plot1du( df, nm=None, bins=None, weights=None
           , duCut=3
           , resolut=None
           ):
    # Default args
    #if nm is None:
    #    nm = get_plane_name(df)
    if bins is None:
        bins = 40
    # Data preparation
    if nm is not None:
        slc = df[['track', nm]]
    else:
        slc = df
    slc.columns = slc.columns.droplevel(0)
    slc = slc.loc[slc.du.abs() < duCut]
    if weights:
        weights = get_value(slc, weights)
    # Create the figure
    fig, ax = plt.subplots(figsize=(3, 3))
    sns.histplot( x=slc.du
                , bins=80
                , stat='density'
                , element='step'
                , fill=True
                , weights=weights
                , ax=ax
                )
    # Plot the PDF.
    xmin, xmax = plt.xlim()
    x = np.linspace(slc.du.min(), slc.du.max(), 100)
    p = norm.pdf(x, slc.du.mean(), slc.du.std())
    plt.plot(x, p, 'g', linewidth=1)
    if resolut:
        p = norm.pdf(x, 0, resolut )
        plt.plot(x, p, 'r', linewidth=3, alpha=0.5)
    return fig

#                           * * *   * * *   * * *

def profileplot( df
               , nm=None
               , breakBy='vLx'
               , distribVar='du'
               , slices=6
               , breakMethod='u'
               , duCut=3
               , reg=True
               , drawRegError=True
               , resolut=None
               , title=None
               , figsize=None
               , logCounts=None  # dict with: slices, counts
               , weightBy=None
               ):
    """
    Produces combined 2D profile plot on the two variables distribution applies
    a linear fit (linear regression).
    `slices' can be
        - a number
        - a range and number (with None for auto): (min, max, nSlices)
        - a list of ranges: [v1, v2, ...]
        - a list of ranges with labels: [(v1, "lbl1"), (v2, "lbl2"), ...]
    `breakMethod' -- 'u' for uniform, 'j' for Jenks natural breaks
    Returns figure and a tuple of tuples: two parameters with their error estimation.
    This function was designed to be a part of unbiased alignment procedure,
    to accompany the correction values with illustrative and explainatory
    visualization for QA.
    """
    #if nm is None:
    #    nm = get_plane_name(df)
    # Data preparation
    if nm is not None:
        slc = df[['track', nm]]
        slc.columns = slc.columns.droplevel(0)
    else:
        slc = df
    #return slc  #XXX
    slc = slc.loc[slc[distribVar].abs() < duCut]
    # find min/max values for sliced variable
    if type(slices) is int:  # just a number -- slice from min to max
        s = (slc[breakBy].min(), slc[breakBy].max(), slices)
    elif type(slices) is tuple and 3 == len(slices):  # a tuple is given -- slice wrt given tpl
        s = copy.copy(slices)
        if s[0] is None:
            s[0] = slc[breakBy].min()
        if s[1] is None:
            s[1] = slc[breakBy].max()
        if s[2] is None:
            s[2] = 4
    elif type(slices) is list:
        raise NotImplementedError('Tuple slices is not yet implemented.')  # TODO
    else:
        raise TypeError(slices)
    if m.isnan(s[0]) or m.isnan(s[1]) or s[0] == s[1]:
        raise RuntimeError(f"Bad/empty range for value {breakBy}: [{s[0]}, {s[1]}]")
    #
    if breakMethod in ('u', 'uniform'):
        rng = tuple(np.linspace(s[0], s[1], s[2]+1))
    elif breakMethod in ('j', 'jenks'):
        raise NotImplementedError('Jenks breaks is not yet implemented.')  # TODO
    # Generate text labels for slicing
    rangeLabels = tuple('%.2f : %.2f'%p for p in zip(rng[:-1], rng[1:]))
    # Generate slice group column
    groups = pd.cut( slc[breakBy]
                   , rng
                   , labels=rangeLabels
                   )
    # Create figure and split it
    fig = plt.figure( constrained_layout=True
                    , figsize=figsize if figsize else (22.5, 7.5*1.5)
                    , dpi=50
                    )
    gs = fig.add_gridspec(nrows=5, ncols=2)
    axes = {
          'slicedProf' : fig.add_subplot(gs[0,0])  # Albert plot
        , 'twinSlices' : fig.add_subplot(gs[1:3,0])  # slices
        , 'population' : fig.add_subplot(gs[3,0])  # countplot
        , 'scatterplot': fig.add_subplot(gs[0:2,1])  # scatterplot
        , 'density'    : fig.add_subplot(gs[2:4,1])  # 2D density histogram
    }
    if title is not None:
        fig.suptitle( title, fontsize=24 )  #, y=.92
    # 1D profile, sliced
    sns.histplot( data=slc
                , x=distribVar
                , hue=groups
                , bins=40
                , element='step'
                , fill=False
                , common_norm=True
                , stat='density'
                , legend=False
                , ax=axes['slicedProf'] )
    if logCounts and logCounts.get('slices', False):
        axes['slicedProf'].set_yscale('log')
    # Add boxplot
    axes['slices'] = plt.twiny(axes['twinSlices'])
    sns.boxplot( data=slc, x=groups, y=distribVar
                    , notch=True
                    , showfliers=False
                    #, saturation=0.2
                    #, color='white'
                    #, boxprops={ 'linewidth' : 2
                    #           , 'edgecolor' : 'black'
                    #           }
                    , ax=axes['twinSlices'] )
    # Do fit and overimpose regression values
    if reg:
        if not weightBy:
            model = sm.ols(formula='%s ~ %s'%(distribVar, breakBy), data=slc).fit()  #cov_type='HC3')
        else:
            model = sm.wls(formula='%s ~ %s'%(distribVar, breakBy), data=slc
                          #, weights=weightBy  # can't because of bug: https://github.com/pandas-dev/pandas/issues/32904
                          , weights=slc[weightBy]
                          ).fit()  #cov_type='HC3')
        xPred = np.linspace(slc[breakBy].min() - 1, slc[breakBy].max() + 1, 10)
        if drawRegError:
            pred = model.get_prediction(exog=dict(**{breakBy:xPred}))
            predDF = pred.summary_frame(alpha=0.318)
            axes['slices'].fill_between(xPred, predDF.obs_ci_lower, predDF.obs_ci_upper, alpha=0.05)  # observation confidence interval
            axes['slices'].fill_between(xPred, predDF.mean_ci_lower, predDF.mean_ci_upper, alpha=0.3)  # mean confidence interval
        yPred = model.predict(exog=dict(**{breakBy:xPred}))
        sns.lineplot(x=xPred, y=yPred, ax=axes['slices'], linewidth=2, color='#993322')  # TODO
    # Add scatterplot
    sns.scatterplot( data=slc
                   , x=breakBy, y=distribVar
                   , edgecolor='none', s=1
                   #, hue=groups
                   , ax = axes['scatterplot']
                   )
    if reg:  # add regression results to scatterplot, if need
        axes['scatterplot'].fill_between(xPred, predDF.obs_ci_lower, predDF.obs_ci_upper, alpha=0.05)  # observation confidence interval
        axes['scatterplot'].fill_between(xPred, predDF.mean_ci_lower, predDF.mean_ci_upper, alpha=0.3)  # mean confidence interval
        sns.lineplot(x=xPred, y=yPred, ax=axes['scatterplot'])
    if reg:
        slope = m.atan(model.params[1])
        axes['scatterplot'].text( 0.05, 0.8
               , '#%d\ninter %.2e±%.2e cm\nslope %.2e±%.2e°'%( slc.shape[0]
                                , model.params[0], model.bse[0]
                                , m.degrees(slope)
                                , m.degrees(model.bse[1]/(1 + slope*slope) )  # tg'x = 1/(1+x*x)
                                )
               , fontsize=12
               , transform = axes['scatterplot'].transAxes
               , bbox={'alpha': .6, 'color': 'white'}
               )
    # Add resolution to scatterplot and boxplot, if need
    if resolut:
        for ax in (axes['scatterplot'], axes['slices']):
            ax.add_patch(
                matplotlib.patches.Rectangle( (s[0],  - resolut/2)
                                            ,  s[1] - s[0], resolut
                                            , edgecolor='black'
                                            , facecolor='none'
                                            , alpha=.55
                                            )
                )
    # Counts
    sns.countplot( data=slc, x=groups
                 , ax=axes['population'] )
    if logCounts and logCounts.get('counts', False):
        axes['population'].set_yscale('log')
    axes['population'].set_xticklabels(axes['twinSlices'].get_xticklabels(), rotation=40, ha="right")
    # Disable ticks for slices
    axes['twinSlices'].xaxis.set_major_formatter(plt.NullFormatter())
    dens2d = np.histogram2d(slc[distribVar], slc[breakBy], bins=(40, 80))
    #display(dens2d)
    sns.heatmap( dens2d[0]
               , norm=LogNorm()  if logCounts and logCounts.get('density', False) else None
               , cmap=sns.color_palette("Spectral_r", as_cmap=True)
               , ax=axes['density'] )
    axes['density'].invert_yaxis()
    axes['density'].yaxis.set_major_locator(plt.NullLocator())
    axes['density'].xaxis.set_major_formatter(plt.NullFormatter())

    return fig, ( (model.params[0], model.bse[0])  # intercept
                , (model.params[1], model.bse[1])  # slope (inclination)
                )

