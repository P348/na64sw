// This script used to generate static mapping for BMS

static void
_fill_BMS_mappings_1( float * dest, int & nChannels ) {
    nChannels = 64;
    for(int chn = 0; chn < nChannels; ++chn) {
        const int ll = chn + 1;
        if (ll<=4)
            dest[chn] = -87.5+5.*(ll-1);
        else if((ll>4)&&(ll<=60))
            dest[chn] = -67.5+5.*((ll-5)/2);
        else
            dest[chn] =  72.5+5.*(ll-61);
    }
}

static void
_fill_BMS_mappings_2( float * dest, int & nChannels ) {
    nChannels = 64;
    for(int chn = 0; chn < nChannels; ++chn) {
        const int ll = chn + 1;
        if(ll<=8)
            dest[chn] = -42.5+5.*((ll-1)/2);
        else if((ll>8 )&&(ll<=20))
            dest[chn] = -22.5+5.*((ll-9)/4);
        else if((ll>20)&&(ll<=44))
            dest[chn] =  -7.5+5.*((ll-21)/6);
        else if((ll>44)&&(ll<=56))
            dest[chn] =  12.5+5.*((ll-45)/4);
        else
            dest[chn] =  27.5+5.*((ll-57)/2);
    }
}

static void
_fill_BMS_mappings_3( float * dest, int & nChannels ) {
    nChannels = 64;
    for(int chn = 0; chn < nChannels; ++chn) {
        const int ll = chn + 1;
        if(ll<=12)
            dest[chn] = -47.5+5.*((ll-1)/2);
        else if((ll>12)&&(ll<=20))
            dest[chn] = -17.5+5.*((ll-13)/4);
        else if((ll>20)&&(ll<=44))
            dest[chn] =  -7.5+5.*((ll-21)/6);
        else if((ll>44)&&(ll<=54))
            dest[chn] =  12.5+5.*((ll-45)/4);
        else
            dest[chn] =  27.5+5.*((ll-55)/2);
    }
}

static void
_fill_BMS_mappings_4( float * dest, int & nChannels ) {
    nChannels = 64;
    for(int chn = 0; chn < nChannels; ++chn) {
        const int ll = chn + 1;
        if(ll<=14)
            dest[chn] =-112.5+5.*(ll-1);
		else if((ll>14)&&(ll<=50))
            dest[chn] = -42.5+5.*((ll-15)/2);
        else
            dest[chn] =  47.5+5.*(ll-51);
    }
}

static const float zCorr[64][4] = {
      {	   -50.0,	   -90.0,	   110.0,	   -50.0	},
      {	   -10.0,	    90.0,	  -110.0,	    90.0	},
      {	    50.0,	   -50.0,	   -90.0,	    30.0	},
      {	    10.0,	    50.0,	    90.0,	    10.0	},
      {	    30.0,	   -10.0,	   -50.0,	   110.0	},
      {	   -30.0,	    10.0,	    50.0,	   -70.0	},
      {	    70.0,	    30.0,	   -10.0,	   -90.0	},
      {	   -70.0,	   -30.0,	    10.0,	    50.0	},
      {	   110.0,	    70.0,	    30.0,	   -10.0	},
      {	  -110.0,	   -50.0,	   -30.0,	   -30.0	},
      {	   -90.0,	    50.0,	    70.0,	    70.0	},
      {	    90.0,	   -70.0,	   -70.0,	  -110.0	},
      {	   -50.0,	   110.0,	   -90.0,	   -90.0	},
      {	    50.0,	   -10.0,	   -10.0,	    50.0	},
      {	   -10.0,	    10.0,	    10.0,	   -10.0	},
      {	    10.0,	  -110.0,	    90.0,	    10.0	},
      {	    30.0,	   -50.0,	   -50.0,	    30.0	},
      {	   -30.0,	    30.0,	    30.0,	   -30.0	},
      {	    70.0,	   -30.0,	   -30.0,	    70.0	},
      {	   -70.0,	    50.0,	    50.0,	   -70.0	},
      {	   110.0,	   -10.0,	   -10.0,	   110.0	},
      {	  -110.0,	    70.0,	    70.0,	  -110.0	},
      {	   -90.0,	   -90.0,	   -90.0,	   -90.0	},
      {	    90.0,	    90.0,	    90.0,	    90.0	},
      {	   -50.0,	   -70.0,	   -70.0,	   -50.0	},
      {	    50.0,	    10.0,	    10.0,	    50.0	},
      {	   -10.0,	    30.0,	    30.0,	   -10.0	},
      {	    10.0,	   110.0,	   110.0,	    10.0	},
      {	    30.0,	   -50.0,	   -50.0,	    30.0	},
      {    -30.0,	    50.0,	    50.0,	   -30.0	},
      {    110.0,	  -110.0,	  -110.0,	   110.0	},
      {	  -110.0,	   -30.0,	   -30.0,	  -110.0	},
      {	    70.0,	   -10.0,	   -10.0,	    70.0	},
      {	   -70.0,	    70.0,	    70.0,	   -70.0	},
      {	    30.0,	   -90.0,	   -90.0,	    30.0	},
      {	   -30.0,	    90.0,	    90.0,	   -30.0	},
      {	   -10.0,	   -70.0,	   -70.0,	   -10.0	},
      {	    10.0,	    10.0,	    10.0,	    10.0	},
      {	   -50.0,	   -50.0,	   -50.0,	   -50.0	},
      {	    50.0,	    30.0,	    30.0,	    50.0	},
      {	   -90.0,	   110.0,	   110.0,	   -90.0	},
      {	    90.0,	  -110.0,	  -110.0,	    90.0	},
      {	   110.0,	   -30.0,	   -30.0,	   110.0	},
      {	  -110.0,	    50.0,	    50.0,	  -110.0	},
      {	    70.0,	   -90.0,	   -90.0,	    70.0	},
      {	   -70.0,	    70.0,	    70.0,	   -70.0	},
      {	    30.0,	   -70.0,	   -70.0,	    30.0	},
      {	   -30.0,	    90.0,	    90.0,	   -30.0	},
      {	   -10.0,	   110.0,	   -10.0,	   -10.0	},
      {	    10.0,	    30.0,	    30.0,	    10.0	},
      {	   -50.0,	   -30.0,	   -30.0,	    50.0	},
      {	    50.0,	  -110.0,	    10.0,	   -90.0	},
      {	   -90.0,	    70.0,	    70.0,	  -110.0	},
      {	    90.0,	   -10.0,	   -70.0,	    70.0	},
      {	   110.0,	    10.0,	    30.0,	   -30.0	},
      {	  -110.0,	   -70.0,	   -30.0,	   -10.0	},
      {	    70.0,	    30.0,	   -10.0,	    50.0	},
      {	   -70.0,	   -30.0,	    10.0,	   -90.0	},
      {	    30.0,	   -10.0,	   -50.0,	   -70.0	},
      {	   -30.0,	    10.0,	    50.0,	   110.0	},
      {	    10.0,	   -50.0,	   -90.0,	    10.0	},
      {	    50.0,	    50.0,	    90.0,	    30.0	},
      {	   -10.0,	   -90.0,	   110.0,	    90.0	},
      {	   -50.0,	    90.0,	  -110.0,	   -50.0	}
};


static void
_fill_BMS_mappings_5( float * dest, int & nChannels ) {
    nChannels = 64;
    for(int chn = 0; chn < nChannels; ++chn) {
        dest[chn] = (chn - 27.5) * 2.5;
    }
}

static void
_fill_BMS_mappings_6( float * dest, int & nChannels ) {
    nChannels = 128;
    for(int chn = 0; chn < nChannels; ++chn) {
        dest[chn] = (chn - 62.5) * 1.25;
    }
}

void bms_layout() {
    float map[128];
    int nChannels;
    _fill_BMS_mappings_1(map, nChannels);
    #if 0
    for(int i = 0; i < nChannels; ++i) {
        if(i) {
            std::cout << " ";
            if(!(i%8)) std::cout << std::endl;
            else if(!(i%4)) std::cout << "   ";
        }
        std::cout << std::setw(6) << map[i] << ",";
    }
    std::cout << std::endl;
    #else
    for(int i = 0; i < nChannels; ++i) {
        std::cout << map[i] << "\t" << zCorr[i][0] << std::endl;
    }
    #endif
}


