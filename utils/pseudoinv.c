#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>

/**\brief Pseudo-inverse matrix calculus
 *
 * For given set of N linear functions with coefficients a_i, b_i, i <- (i...N)
 * finds a point (x',y') with minimal distances to the lines. Uses
 * pseud-inverted matrix method.
 *
 * Idea proposed originally by I. Voronchikhin.
 * */

static void
_gsl_print_m( const gsl_matrix * m ) {
    for(int j = 0; j < m->size2; ++j) {
        for( int i = 0; i < m->size1; ++i ) {
            printf( "%.2e\t", gsl_matrix_get(m, i, j) );
        }
        fputs("\n", stdout);
    }
}

int
pseudoinv_mx( const double * a_
            , const double * b_
            , size_t N, double * r_ ) {
    /* make views based on the value given; we use matrix views for `a' as
     * we're going to apply matrix operations to it */
    gsl_matrix * a = gsl_matrix_calloc(N, 2); {
        gsl_matrix_set_all(a, -1);
        gsl_vector_const_view aView = gsl_vector_const_view_array(a_, N);
        gsl_matrix_set_col(a, 0, &aView.vector);
    }

    //gsl_matrix_const_view a = gsl_matrix_const_view_array(a_, 2, N);
    /* construct a a^T.a matrix */
    gsl_matrix * aTa = gsl_matrix_calloc(2, 2);
    gsl_blas_dgemm( CblasTrans, CblasNoTrans, 1.0
                  , a, a, 0.0
                  , aTa);

    /* do the LU decomposition */
    int signum;
    gsl_permutation * p = gsl_permutation_alloc(2);
    gsl_matrix * tmpM = gsl_matrix_calloc(2, 2);
    gsl_matrix_memcpy(tmpM, aTa);
    gsl_linalg_LU_decomp(tmpM, p, &signum);
    /* calc inverted matrix (aTa)^{-1} */
    //double det = gsl_linalg_LU_det(tmpM, signum);
    gsl_matrix * psInvA = gsl_matrix_alloc(2, N);
    gsl_linalg_LU_invert(tmpM, p, aTa);
    //_gsl_print_m(aTa);  // XXX
    gsl_permutation_free(p);
    /* pseudo-inverted m-x is (aTa)^{-1} \times A^T */
    gsl_blas_dgemm( CblasNoTrans, CblasTrans, 1.0
                  , aTa, a, 0.0
                  , psInvA );
    gsl_matrix_free(tmpM);
    /* compute coefficients as a product with b */
    // k = np.dot(a_pseud, b)
    gsl_matrix_const_view b = gsl_matrix_const_view_array(b_, 1, N);
    gsl_matrix_view r = gsl_matrix_view_array(r_, 2, 1);
    gsl_blas_dgemm( CblasNoTrans, CblasTrans, 1.0
                  , psInvA, &b.matrix, 0.0
                  , &r.matrix );
    return 0;
}



int
main(int argc, char * argv[]) {
    double a[] = {-15, -13,   3}
         , b[] = {  2, -10,  15}
         , k[2]
         ;
    const int N = sizeof(b)/sizeof(*b);
    int rc = pseudoinv_mx( a, b, N, k );

    char bf[1024], * c = bf;
    for(int i = 0; i < N; ++i) {
        c += sprintf(c, "%c(%e)*x+(%e) notitle", i != 0 ? ',' : ' '
                    , a[i], -b[i] );
    }

    printf( "set object circle at first %e,%e radius char 0.5\n", k[0], k[1] );
    fputs("plot", stdout);
    fputs(bf, stdout);
    putc('\n', stdout);
    return rc;
}

