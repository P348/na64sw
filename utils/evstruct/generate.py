# Snippet:
#   $ python3 utils/evstruct/generate.py --js-dump-all=event.json presets/event-struct/sadc.yaml presets/event-struct/apv.yaml presets/event-struct/stwTDC.yaml presets/event-struct/calo.yaml presets/event-struct/track.yaml presets/event-struct/event.yaml

import yaml, re, datetime, copy, sys, json
import os.path
import jinja2.exceptions
import traceback
from jinja2 import Environment, FileSystemLoader
from icecream import ic
from avroSchema import generate_avro_schema
from collections import OrderedDict

try:
    import pygments  # colored output for source code at some places
except ImportError:
    pygments = None

rxShortTypeDescr = re.compile(r'^([^\[\/]+)\s*((?:\[\d*\])*)?\s*@\s*(.+)\s*$')

def encode_collection_features(l):
    code = 0x0
    if 'ordered' in l:
        code |= 0x1
    if 'sparse' in l:
        code |= 0x2
    if 'ambig' in l:
        code |= 0x4
    return code

gCppContainers = {
  encode_collection_features(('ordered',)) : 'ordered',  # .................................. not ambig, not sparse
  encode_collection_features(('sparse',)) : 'sparse',  # .................................... not ambig, not ordered
  encode_collection_features(('ambig',)) : 'ambig',  # ...................................... not ordered, not sparse
  encode_collection_features(('ordered', 'sparse')) : 'ordered_sparse',  # .................. not ambig
  encode_collection_features(('ordered', 'ambig')) : 'ordered_ambig',  # .................... not sparse
  encode_collection_features(('sparse', 'ambig')) : 'sparse_ambig',  # ...................... not ordered
  encode_collection_features(('sparse', 'ambig', 'ordered')) : 'sparse_ambig_ordered',  # ... all features
}

#                           * * *   * * *   * * *

def read_document(docPath):
    """
    This function just reads .yaml documents.
    \\todo In future it probably will perform basic validation of this data
    """
    filename = os.path.splitext(os.path.basename(docPath))[0]
    with open(docPath, 'r') as f:
        doc = yaml.safe_load(f)
    # normalize type definitions of type's attributes (expand shortened
    # forms)
    for typeName, typeDef in doc.items():
        if typeName.startswith('$'): continue
        for attrName, attrDef in typeDef.items():
            if attrName.startswith('$'): continue
            typeDef[attrName] = _normalize_type_object(attrDef)
    if '$include-impl' not in doc:
        doc['$include-impl'] = []
    # TODO: ... here we should apply a schema validation
    return doc

def load_templates(d):
    #return { k : (Template(open(v, 'r').read()), v, design) for k,v,design in d }
    #return { k : ( Environment(loader=FileSystemLoader("utils/evstruct/templates")).from_string(open(v, 'r').read())
    #             , v, design) for k,v,design in d
    #       }
    r = {}
    for k, v, design in d:
        try:
            r[k] = ( Environment( loader=FileSystemLoader("utils/evstruct/templates")).from_string(open(v, 'r').read())
                   , v
                   , design
                   )
        except Exception as e:
            sys.stderr.write(f"An error occured while loading template \"{v}\":\n")
            sys.stderr.write(traceback.format_exc())
            raise
    return r

def resolve_version(ver):
    """
    Oly the major + minor version number pair is used to determine the
    declaration syntax API.
    """
    return '.'.join(ver.split('.')[0:2])

#                           * * *   * * *   * * *
def _normalize_type_object(attrDef):
    if type(attrDef) is not str:
        return attrDef
    m = rxShortTypeDescr.match(attrDef.strip())
    if not m:
        raise RuntimeError(f'Short type description "{attrDef}"'
                f' of an attribute "{attrName}" of type'
                f' "{typeName}" is not valid.')
    tp, staticSize = m.groups()[0].strip(), m.groups()[1].strip()
    attrDoc = { 'doc': m.groups()[2].strip() }
    if staticSize:
        attrDoc['type'] = {
                'setOf': tp,
                'staticSize': staticSize,
                'features': ['ordered']
            }
    else:
        attrDoc['type'] = {
                'cppType': tp
            }
    return attrDoc
#                           * * *   * * *   * * *
def _append_cpp_type_v01(tp, common, attrName='?', typeName='?'):
    requiredPoolTypes = set()
    if tp.get('cppType', False) \
    and tp['cppType'] in common['poolTypes']:
        # A referenced attribute (optionally associated single object)
        requiredPoolTypes.add(tp['cppType'])
        tp['referencedType'] = tp['cppType']
        tp['cppType'] = 'mem::Ref<%s>'%tp['cppType']
        tp['isPOD'] = False
        #if 'propagateGettersFrom' not in tp:
        #    tp['propagateGettersFrom'] = []
        #tp['propagateGettersFrom'].append(attrName)
    elif tp.get('setOf', False):
        tp['isPOD'] = False
        # this is a collection type
        attrFeatures = set(tp.get('features', []))
        cppContainerTags = gCppContainers[encode_collection_features(attrFeatures)]
        tp['cppContainerTags'] = cppContainerTags
        collectedType = tp['setOf']
        if type(collectedType) is dict:
            # A complex type
            collectedType, rqs = _append_cpp_type_v01( collectedType, common )
            assert type(rqs) is set
            requiredPoolTypes |= rqs
            #ic(collectedType)  # XXX
            assert type(collectedType['cppType']) is str
            tp['cppType'] = 'MapTraits<{keyType}, {collectedType}, MapTag::{containerTags}, NA64SW_EVMALLOC_STRATEGY>::type'.format(
                        keyType=tp.get('idxBy', 'int')
                      , containerTags=cppContainerTags
                      , collectedType=collectedType['cppType'])
        elif collectedType not in common['poolTypes']:
            # Collected type is not a subject of our pool allocators.
            if 'staticSize' in tp:
                # A plain array (e.g. uint16_t[12], double[4][4])
                tp['cppType'] = collectedType
            else:
                # User must refrain from heap-allocated attributes
                sys.stderr.write(f'Warning: {typeName}::{attrName}'
                        ' is an STL container (uses heap).\n')
                tp['cppType'] = 'MapTraits<{keyType}, {collectedType}, MapTag::{containerTags}, NA64SW_EVMALLOC_STRATEGY>::type'.format(
                          keyType=tp.get('idxBy', 'int')
                        , collectedType=collectedType
                        , containerTags=cppContainerTags )
        else:
            # Collected type is a subject of our pool allocators.
            if 'staticSize' in tp:
                # A plain array (of PoolRefs)
                tp['cppType'] = 'mem::Ref<{type_}>'.format(
                        type_=collectedType)
            else:
                # A collection of pool-allocated objects
                tp['cppType'] = 'event::Map<{keyType}, {collectedType}, MapTag::{containerTags}>'.format(
                        keyType=tp.get('idxBy', 'int')
                      , containerTags=cppContainerTags
                      , collectedType=collectedType)
                tp['mainAssociation'] = tp.get('mainAssociation', True)
            requiredPoolTypes.add(collectedType)
            # TODO: append association traits?
    elif tp.get('oneOf', False):
        tp['isPOD'] = False
        # This is a union-like entry permitting a single field from a some
        # pre-defined sets
        tp['cppType'] = 'std::variant<std::monostate '
        for fieldName, fieldDescription in tp['oneOf'].items():
            fieldDescription = _normalize_type_object(fieldDescription)
            fieldTp, fieldRq = _append_cpp_type_v01( fieldDescription['type']
                                                   , common
                                                   , attrName=fieldName )
            fieldDescription['type'] = fieldTp
            tp['oneOf'][fieldName] = fieldDescription
            requiredPoolTypes |= (fieldRq)
            tp['cppType'] += ', ' + fieldTp['cppType']
        tp['cppType'] += ' >'
        # TODO: append association traits?
    else:
        if tp.get('isPOD', True):
            tp['isPOD'] = True
        pass  # ordinary C/C++ type here
    #assert 'cppType' in tp
    return tp, requiredPoolTypes

def _v01( common
        , doc
        , source=None
        , cPoolID=0
        ):
    """
    Appends common template-rendering context with some data inferred from the
    document. Normalizes types definitions, produces file names to be generated
    and builds up the dependencies.
    """
    for typeName, typeDef in doc.items():
        if typeName.startswith('$'): continue
        # Add type definition to global index; just None to allow type to find
        # itself
        common['poolTypes'][typeName] = None
        rq = set()
        # This loop updates attribute type definition with features
        # specific for target language
        attrIndex = 0
        for attrName, attrDef in typeDef.items():
            if attrName.startswith('$'): continue
            try:
                _, rq_ = _append_cpp_type_v01( attrDef['type'], common
                                             , attrName=attrName, typeName=typeName )
            except Exception as e:
                sys.stderr.write(f' .. while processing attribute {typeName}::{attrName}\n')
                raise
            rq |= rq_
            if 'attrIndex' in attrDef:
                attrIndex = attrDef['attrIndex']
            else:
                attrDef['attrIndex'] = attrIndex
            attrIndex += 1
        inhsList = []
        for n, tp in enumerate(typeDef.get('$inherits', [])):
            tp, rq_ = _append_cpp_type_v01(tp, common, attrName='<inherit#%d>'%n, typeName=typeName)
            inhsList.append(tp)
            rq |= rq_
        common['poolTypes'][typeName] = { 'document' : source
                                        , 'poolID' : cPoolID
                                        , 'requiresTypes' : list(sorted(rq)) }
        cPoolID += 1
        if common['poolTypes'][typeName]['requiresTypes']:
            print(f' * {typeName} <-', ', '.join(
                tp + '[%s]'%common['poolTypes'][typeName]['document'] for tp in common['poolTypes'][typeName]['requiresTypes'])
                )
        typeDef['$inherits'] = inhsList
        # Constructor initialization; if current type requires some pool one's
        # we must forward local memory argument by ctr to inherited
        # superclasses and attributes
        if common['poolTypes'][typeName]['requiresTypes']:
            inhList = []
            for inhType in typeDef['$inherits']:
                if 'setOf' not in inhType: continue
                inhList.append(inhType['cppType'])
            for attrName, attr in typeDef.items():
                if attrName.startswith('$'): continue
                if 'staticSize' in attr['type'] or 'setOf' not in attr['type']: continue
                inhList.append(attrName)
            typeDef['$ctrInit'] = inhList


#                           * * *   * * *   * * *
def main():
    import argparse
    p = argparse.ArgumentParser( description="Generates C/C++ code for event"
            " data structure based on .yaml description.")
    p.add_argument( 'inputs', help="Input .yaml files to process. Provided"
            " files are expected to obey format of na64sw event data fields"
            " descrition."
            , nargs=argparse.REMAINDER )
    p.add_argument( '--cpp-include-dir', help="Include directory to resolve"
            " relative #include path."
            , default='include/' )
    p.add_argument( '-o', '--js-dump-all', help="Dump all the collected types to"
            " a JSON file given as an argument."
            , type=str )
    p.add_argument( '-p', '--js-dump-one', help="Dump certain entry to a"
            " file. Argument must be <obj-name>=<file-name>"
            , type=lambda a: a.split(':') )
    #p.add_argument( '-l', '--look-only', dest='sections', action='append'
    #        , help='Choose the section(s) ')
    args = p.parse_args()
    if len(args.inputs) < 1:
        raise RuntimeError( 'At least one file expected.' )
    ctxTranslators = {
        '0.1' : _v01
        # ...
    }
    templates = load_templates(
        [ # Lists template files, destination, and designation
            # C++ declaration of data strutures
            ( 'include/na64event/data/{document}.hh'
                , 'utils/evstruct/templates/cpp/decl.hh',  'cpp-decl'),
            # C++ implementation of data structures
            ( 'src/event/data/{document}.cc'
                , 'utils/evstruct/templates/cpp/impl.cc',  'cpp-impl' ),
            ( 'src/event/data/{document}-hdql.cc'
                , 'utils/evstruct/templates/cpp/impl-hdql.cc',  'cpp-hdql-impl' )
            # ... various serialization and stuff
            #({document}.proto,     'evstruct/definition.proto',            'capnp')
        ])  #, disableTemplate= ?
    # Reads the .yaml declarations -- no changes in this func
    docs = []
    for p in args.inputs:
        docs.append( (p, read_document(p)) )
    # Get common context from the set of documents
    commonCtx = {'this': {}, 'cpp': {}, 'poolTypes': OrderedDict()}
    entries = []
    for docPath, doc in docs:
        ver = resolve_version(doc.get('$version', sorted(ctxTranslators.keys())[-1]))
        if ver not in ctxTranslators:
            raise KeyError(f'Unknown API version for document "{docPath}": "{ver}".')
        ctxTranslators[ver](commonCtx, doc, source=docPath)
    # Generate the stuff
    fullSpec = {}
    allTypes = {}
    for docPath, doc in docs:
        ctx = copy.copy(commonCtx)
        ctx['this'].update({
                'filename': docPath,
                #'template': templateFilePath,
                'generated': datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                'version': doc.get('$version', 'any'),
                'doc': doc.get('$doc', 'any'),
                #'objGroup': doc  # TODO
            })
        ctx['cpp'].update({
                'include_decl': doc.get('$include-decl', []),
                'include_impl': doc.get('$include-impl', []),
                'preambule_decl': doc.get('$preambule-incl', None),
                'preambule_impl': doc.get('$preambule-impl', None),
            })
        ctx['types'] = {k:v for k,v in doc.items() if not k.startswith('$')}
        # Resolve includes
        cppHdrIncludes = set()
        for tpn_ in ctx['types'].keys():
            ctx['types'][tpn_]['$poolID'] = commonCtx['poolTypes'][tpn_]['poolID']
            for tpn in ctx['poolTypes'][tpn_]['requiresTypes'] + [tpn_]:
                # resolve type declaration file
                excerpt = commonCtx['poolTypes'].get(tpn, {})
                if not excerpt: continue
                if excerpt['document'] == docPath: continue
                target = excerpt['cppDecl']
                if 'cppDecl' not in excerpt:
                    raise RuntimeError(f'C++ type "{tpn}" is required'
                            ' before it\'s header'
                            ' generated.' )
                    # ^^^ error here indicates that header file for this type
                    # has not been yet rendered, that is probably due to wrong (or
                    # cyclic) type dependency.
                if target == filename: continue  # same file, omit self-include
                cppHdrIncludes.add(target)
        ctx['cpp']['include_decl'] += list( os.path.relpath(f, args.cpp_include_dir)
                                            for f in sorted(cppHdrIncludes))
        # this seem to be a little kludgy: the purpose is to supply root
        # object declaration/definition with the list of all compound types
        # defined by generated assets
        if 'Event' in ctx['types']:
            ctx['allCompoundTypes'] = allTypes
        for outPath, (template, templateFilePath, desgn) in templates.items():
            ctx['this']['template'] = templateFilePath
            ctx['this']['objGroup'] = os.path.splitext(os.path.basename(docPath))[0]
            filename = outPath.format(**{'document':ctx['this']['objGroup']})
            # Append type declaration details with obtained
            # file name, for C/C++ only
            #  # path to a wrt b
            for typeName in ctx['types'].keys():
                if 'cpp-decl' == desgn:
                    assert 'cppDecl' not in commonCtx['poolTypes'][typeName]
                    commonCtx['poolTypes'][typeName]['cppDecl'] = filename
                    assert 'cppDecl' in ctx['poolTypes'][typeName]
                    # C++ include header with own declarations into .cpp
                    ctx['cpp']['include_impl'].insert(0, os.path.relpath(filename, args.cpp_include_dir) )
                elif 'cpp-impl' == desgn:
                    assert 'cppImpl' not in commonCtx['poolTypes'][typeName]
                    commonCtx['poolTypes'][typeName]['cppImpl'] = filename
                    assert 'cppImpl' in ctx['poolTypes'][typeName]
                else:
                    pass  # other file type is generate currently
            #print(json.dumps(ctx, indent=2, sort_keys=True))  # XXX
            if args.js_dump_one:
                if 'cpp-decl' != desgn: continue
                if args.js_dump_one[0] in ctx['types'].keys():
                    js = json.dumps( ctx['types'][args.js_dump_one[0]], indent=2, sort_keys=True )
                    if pygments:
                        js = pygments.highlight( js
                                               , pygments.lexers.JsonLexer()
                                               , pygments.formatters.TerminalFormatter())
                    with open(args.js_dump_one[1], 'w') as f:
                        f.write(js)
            else:
                try:
                    generated = template.render(**ctx)
                except:
                    sys.stderr.write(f' .. while rendering template "{templateFilePath}" with doc "{docPath}"\n')
                    raise
                os.makedirs(os.path.split(filename)[0], exist_ok=True)
                with open(filename, 'w') as f:
                    f.write(generated)
                print(f'File {filename} written.')
        fullSpec[docPath] = ctx
        allTypes.update({k:copy.copy(v) for k,v in doc.items() if not k.startswith('$')})
    # Avro schema
    avroSchema = generate_avro_schema(commonCtx, allTypes)
    with open('extensions/avro/na64sw-event-avro.json', 'w') as f:  # TODO: configurable
        f.write(json.dumps(avroSchema, indent=2))
    if args.js_dump_all:
        with open(args.js_dump_all, 'w') as f:
            f.write( json.dumps(fullSpec, sort_keys=True) )

if "__main__" == __name__:
    main()

