import json, graphviz

def typeref_str( tpn ):
    """
    Returns hyperlink to a type description page.

    TODO: since we currently have no public documentation page the returned
    link is empty.
    """
    return f'#{tpn}'  # TODO: address

def attr_label( tn, td, poolTypes=None ):
    """
    For given type name and type description will render string on Graphviz's
    HTML dialect to be emplaced within a <TABLE/> node.
    Returns string to be put, the referenced type, if it is met within
    poolTypes. If no type is under reference the second will be None.
    """
    # if type is one of ours, make link
    #if attrTD['type'].get('isPOD', True): continue  # it's a simple type, no ref to other type need
    #attrEndType = attrTD['type'].get('setOf', None) or attrTD['type'].get('referencedType', None)
    #if attrEndType and attrEndType in doc['poolTypes'].keys():
    #    dot.edge(attrEndType, typeName)  # TODO: composition/aggregation
    if poolTypes is None: poolTypes = {}
    # tn -- type name, td -- type definition
    lbl, refdType = None, None
    if td['type'].get('isPOD', True):
        # a POD type -- just public type
        lbl = td['type']['cppType']
    else:
        if 'setOf' in td['type']:
            if 'staticSize' in td['type']:
                lbl = td['type']['cppType'] + '%s'%td['type']['staticSize']
        attrEndType = attrTD['type'].get('setOf', None) or attrTD['type'].get('referencedType', None)
        if attrEndType:
            if type(attrEndType) is str and attrEndType in doc['poolTypes'].keys():
                refdType = attrEndType
            elif type(attrEndType) is dict:
                lbl = '(oneof)'  # TODO: inject anonymous oneof diagram
    if not lbl:
        lbl = td['type'].get('cppType', '???')  # ...rest
    lbl = lbl.replace('<', '&lt;') \
             .replace('>', '&gt;') \
             .replace(' ', '&nbsp;')
    if not td['type'].get('isPOD', True):
        lbl = '<B>' + lbl + '</B>'
    lbl = '+&nbsp;<I>' + tn + '</I>:&nbsp;' + lbl
    if refdType is None:
        return '  <TR><TD ALIGN="text">' + lbl + '<BR ALIGN="left"/></TD></TR>', refdType
    else:
        return f'  <TR><TD ALIGN="text" HREF="{typeref_str(refdType)}" BGCOLOR="green">' \
              + lbl \
              + '<BR ALIGN="left"/></TD></TR>', refdType
        

with open('doc/event.json', 'r') as f:
    docs = json.load(f)

# Create graph
dot = graphviz.Digraph(comment='NA64SW Event Object Model'
        , node_attr={ 'shape': 'plaintext'
                    #, ''
                    }
        , format='svg'
        )

#digraph hierarchy {
#size="5,5"
#node[shape=record,style=filled,fillcolor=gray95]
#edge[dir=back, arrowtail=empty]

# find the root document -- the "event"; we assume it ends with "...event.yaml"
evDoc = None
# Walk through all the documents
for docName, doc in docs.items():
    # Memoize the root document
    #if docName.endswith('event.yaml'):
    #    evDoc = docs[docName]
    # Add nodes
    for typeName, typeDesc in doc['types'].items():
        strAttrs = ''  # caption string listing attributes in a type node
        for attrTN in sorted(typeDesc.keys()):
            if attrTN.startswith('$'): continue  # omit service JSON props
            attrTD = typeDesc[attrTN]
            s, refdType = attr_label( attrTN, attrTD, poolTypes=doc['poolTypes'] )
            strAttrs += s + '\n'  # append attr string
            if refdType is not None:
                # This node refers to one of the types declared within the
                # package (one of the other nodes) --- draw the link
                # (composition/agregation), annotate multiplicity and
                # association features
                linkAttrs = {
                        'arrowhead' : 'diamond' if attrTD['type'].get('mainAssociation', False) else 'odiamond',
                    }
                if 'setOf' in attrTD['type']:
                    if 'staticSize' in attrTD['type']:
                        linkAttrs['headlabel'] = attrTD['type']['staticSize']
                    elif 'features' in attrTD['type']:
                        linkAttrs['label'] = '{' + ', '.join(attrTD['type']['features']) + '}'
                        linkAttrs['headlabel'] = '1'
                        linkAttrs['taillabel'] = '*'
                else:
                    # referenced type
                    #linkAttrs['headlabel'] = '1'
                    linkAttrs['taillabel'] = '0...1'
                dot.edge( refdType, typeName, **linkAttrs)
        dot.node( typeName
                , '<<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0">\n'
                  + f'<TR><TD HREF="{typeref_str(typeName)}" BGCOLOR="grey"><B>{typeName}</B></TD></TR>\n' \
                  + '<TR><TD><TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0">\n' \
                  + strAttrs \
                  + "</TABLE></TD></TR>\n</TABLE>>"
                )
        if '$inherits' in typeDesc:
            for inhTD in typeDesc['$inherits']:
                inhRefType = inhTD.get('setOf', None) or inhTD.get('referencedType', None)
                if inhRefType in doc['poolTypes']:
                    dot.edge(inhRefType, typeName, dir='back', arrowtail='empty')  # TODO: inheritance
#print(dot.source)
dot.render('doc/event', view=False)
#print(json.dumps(evDoc, sort_keys=True, indent=8))

# Traverse all the classes
#dot.node(typeName)

