#include "na64event/hdql-augments.hh"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"
#include "na64event/hdql-augments.hh"

/**\file
 * \brief HDQL interfaces for {{types.keys()|join(', ')}}.
 * 
 * \warning This is a file generated for event's data structure integrating
 *          with HDQL.
 * \note Generated at {{this.generated}} with template {{this.template}}
 * \version {{this.version}}
 */

namespace na64dp {
namespace event {

void
define_{{this.objGroup}}_hdql_compounds( ::hdql::helpers::CompoundTypes & types
                           , hdql_Context * context
                           ) {
{%- for name, decl in types.items() %}
    types.new_compound<{{name}}>("{{name}}")
        {%- for attrName, attr in decl.items() if (not attrName.startswith('$') and ((not attr.hdqlOpts) or (not attr.hdqlOpts.disable))) %}
        .attr<&{{name}}::{{attrName}}>("{{attrName}}")
        {%- endfor %}
    .end_compound();
{%- endfor %}
}

}  // namespace ::na64dp::event
}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND

