/// {{decl['$doc'].strip().split('\n') | join('\n/// ')}}
///
/// **GETTER**:
{%- for attrName, attr in decl.items() if (not attrName.startswith('$')) %}
{%- if 'getter' not in attr and attr.type.isPOD %}
/// * `{{attrName}}` \-- {{attr.doc.strip().split('\n')[0] }}
{%- if attr.enable_by_macro %}
///   (controlled by macro `attr.enable_by_macro`)
{% endif %}
{%- elif 'referencedType' in attr.type %}
{%-     for subAttrName, subAttr in types[attr.type.referencedType].items() if not subAttrName.startswith('$') %}
{%-         if subAttr['getter'] or subAttr.type.isPOD %}
/// * `{{attrName}}.{{subAttrName}}` \-- {{subAttr.doc.strip().split('\n')[0] }}
///   (propagated from `{{attrName}}:{{attr.type.referencedType}}`, returns `{{subAttrName}}`)
{%-         endif %}
{%-     endfor %}
{%-     for subCustomGetterName, subCustomGetter in types[attr.type.referencedType].get('$customGetters', {}).items() %}
/// * `{{attrName}}.{{subCustomGetterName}}` \-- {{subCustomGetter.doc.strip().split('\n')[0] }}
///   (propagated from custom getter `{{attrName}}:{{attr.type.referencedType}}`, returns `{{subCustomGetterName}}`)
{%-     endfor %}
{%- elif not attr['getter'] %}
/// * getter for field `{{name}}::{{attrName}}` **disabled**
{%- else %}
/// * (?) a custom getter
{%- endif %}
{%- endfor %}
{%- for customGetterName, customGetter in decl.get('$customGetters', {}).items() %}
/// * `{{customGetterName}}` \-- {{customGetter.doc.strip().split('\n')[0] }}
{%- endfor %}
struct {{name}} {% for tp in decl.get('$inherits', []) %}{{ ":" if loop.first else "" }} public {{tp.cppType}} {{ ", " if not loop.last else "" }}{% endfor %}{
    {%- for attrName, attr in decl.items() if not attrName.startswith('$') %}
    {%- if attr.enable_by_macro %}
    #if defined({{attr.enable_by_macro}}) && {{attr.enable_by_macro}} {% endif %}
    /// {{attr.doc.strip().split('\n') | join('\n/// ') | indent(4)}}
    {{attr.type.cppType}} {{attrName}}{{attr.type.staticSize}};
    {% if attr.enable_by_macro %}
    #endif
    {%- endif -%}
    {%- endfor %}
    {%- if not poolTypes[name].requiresTypes%}
    /// Default ctr (creates uninitialized instance of {{name}}.
    {{name}}(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of {{name}}.
    /// Needed to simplify compatibility with template code.
    {{name}}( LocalMemory & ) {}
    {%else%}
    /// A ctr, needs the pool allocator instance to be bound with
    {{name}}( LocalMemory & );
    {%-endif%}
};  // struct {{name}}

#define M_for_every_{{name}}_collection_attribute( m, ... ) \
    {% for attrName, attr in decl.items() if (not attrName.startswith('$')) and (not attr.enable_by_macro) and "setOf" in attr.type -%}
    m({{attrName}}, {{attr.type.idxBy}}, {{attr.type.setOf}}, __VA_RGS__) \
    {%endfor-%}
    /* ... */
