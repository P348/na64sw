template<>
struct Traits<{{name}}> {
    static constexpr char typeName[] = "{{name}}";
    /// Callback function type, receiving an instance of type `{{name}}` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const {{name}} & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    {% if '$naturalKey' in decl %}
    /// Natural key type to index collection of {{name}} instances
    typedef {{decl['$naturalKey']}} NaturalKey;
    {% endif %}

    template<typename CallableT, typename ObjectT={{name}}>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        {%for inhType in decl['$inherits']-%}
        Traits<typename {{inhType.cppType}}>::for_each_attr(
                CallableTraits<CallableT>::inherited<typename {{inhType.cppType}}>(callable),
                static_cast<{{inhType.cppType}}&>(obj));
        {%endfor%}
        {%- for attrName, attrDef in decl.items() if not attrName.startswith('$') -%}
        {%- if attrDef.enable_by_macro %}#if defined({{attrDef.enable_by_macro}}) && {attrDef.enable_by_macro}} {% endif -%}
        callable( CallableTraits<CallableT>::attr_id({{attrDef.attrIndex}}, "{{attrName}}"), obj.{{attrName}} );
        {% if attrDef.enable_by_macro %}#endif // {{attrDef.enable_by_macro}}{% endif -%}
        {%- endfor %}
    }
};

