/**\file
 * {{this.doc.strip().split('\n') | join('\n * ')}}
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at {{this.generated}} with template {{this.template}}
 * \version {{this.version}}
 */
{% from 'cpp/impl-getters.cc' import IMPLEMENT_GETTERS, DEFINE_GETTERS_ENTRIES %}
{% for inc in cpp.include_impl %}
#include "{{inc}}"
{% endfor %}
#include <cmath>
#include "na64event/reset-values.hh"
{%if cpp.preambule_impl%}
{{cpp.preambule_impl}}
{%endif%}
namespace na64dp {

{% for name, decl in types.items() %}
{%if '$ctrInit' in decl%}
namespace event {
{{name}}::{{name}}( LocalMemory & lmem )
    {% for e in decl['$ctrInit'] %} {{ ":" if loop.first else "," }} {{e}}(lmem)
    {%endfor%}
{}
}  // namespace ::event::na64dp
{%endif%}
{%endfor%}

{% for name, decl in types.items() if not name.startswith('$') %}
namespace util {
void reset( event::{{name}} & obj ) {
    using namespace event;
    {%for inhType in decl['$inherits']%}
    // reset inherited {{inhType.cppType}}
    {% if "deftCpp" in inhType-%}
    {{inhType.deftCpp}}
    {%- elif "setOf" in inhType %}
    static_cast<{{inhType.cppType}}&>(obj).clear();
    {%- else %}
    ::na64dp::util::reset(static_cast<{{inhType.cppType}}&>(obj));
    {%- endif %}
    {%endfor%}
    {%- for attrName, attrDecl in decl.items() if not attrName.startswith('$') -%}
    {%- if attrDecl.enable_by_macro %}#if defined({{attrDecl.enable_by_macro}}) && {{attrDecl.enable_by_macro}} {% endif -%}
    {% if "deftCpp" in attrDecl-%}
    {{attrDecl.deftCpp}}
    {%- elif "deft" in attrDecl %}
    obj.{{attrName}} = {{attrDecl.deft}};
    {%- elif "setOf" in attrDecl.type and "staticSize" not in attrDecl.type %}
    obj.{{attrName}}.clear();
    {%- else %}
    ::na64dp::util::reset(obj.{{attrName}});
    {% endif -%}
    {% if attrDecl.enable_by_macro %}#endif // {{attrDecl.enable_by_macro}}{% endif -%}
    {%- endfor %}
}
}  // namespace ::na64dp::util
{% endfor %}

namespace event {

{% for name, decl in types.items() %}

{{ IMPLEMENT_GETTERS(name, decl, types) }}

const Traits<{{name}}>::Getters
Traits<{{name}}>::getters = {
    {{DEFINE_GETTERS_ENTRIES(name, decl, types)|indent}}
};

{% endfor %}

}  // namespace ::na64dp::event
}  // namespace na64dp

