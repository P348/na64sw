{#                                                                ____________
 # _____________________________________________________________/ Getter name
 # Using stack (list) of (name:str, type:obj) returns getter name
 #}
{% macro _GETTER_NAME(rootTypeName, stack) -%}
    _{{rootTypeName}}_get{% for nm, _ in stack %}_{{nm}}{% endfor %}
{%- endmacro %}


{% macro _JOIN_STACK(stack) -%}
{% for attrName, attrDef in stack %} / {{attrName}}:{{attrDef.type.cppType if attrDef else ''}}{% endfor %}
{%- endmacro %}

{#                                                      ______________________
 # ___________________________________________________/ Getter implementation
 # Using stack (list) of (name:str, type:obj) generates getter implementation
 #}
{% macro IMPLEM(rootTypeName, stack, getterCode=False) -%}
{% if stack|length > 1 -%}
// propagated from {{_JOIN_STACK(stack)}}
static StdFloat_t {{_GETTER_NAME(rootTypeName, stack)}}( const {{rootTypeName}} & obj_ ) {
    {%- for item in [('obj_', ''),] + stack[:-1] -%}
    {% if loop.last %}
    const auto & obj = {{item[0]}};
    {%- else %}
    if(!{{item[0]}}.{{loop.nextitem[0]}}) return std::nan("0");
    const auto & {{loop.nextitem[0]}} = *{{item[0]}}.{{loop.nextitem[0]}};
    {%- endif %}
    {%- endfor %}
    {% if not getterCode %}
    return static_cast<StdFloat_t>(obj.{{stack[-1][0]}});  // simple value
    {% else -%}
    {{getterCode.cpp}}  // custom getter
    {%- endif %}
}
{%- else -%}
static StdFloat_t {{_GETTER_NAME(rootTypeName, stack)}}( const {{rootTypeName}} & obj ) {
    {% if stack[0][1].getter -%}
    {{stack[0][1].getter}}  // explicit getter code
    {%- elif stack[0][1].type and stack[0][1].type.isPOD -%}
    return static_cast<StdFloat_t>(obj.{{stack[0][0]}});  // simple value
    {%- else -%}
    #warning "Unknown attribute getter."
    {%- endif %}
}
{%- endif %}
{%- endmacro %}

{#                                          __________________________________
 # _______________________________________/ Propagated Getters Implementation
 # Recursive macro expanding getters for one-to-one association.
 #}
{% macro PROPAGATE_GETTERS(rootTypeName, stack, types) %}
{%  for attrName, attrDefinition in types[stack[-1][1].type.referencedType].items() if not attrName.startswith('$') -%}
{%  if attrDefinition.enable_by_macro %}#if defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif -%}
{%  if attrDefinition.type.isPOD and 'getter' not in attrDefinition -%}
{{IMPLEM(rootTypeName, stack + [(attrName, attrDefinition),], attrDefinition.cpp)}}
{% elif attrDefinition.type.referencedType %}
{{PROPAGATE_GETTERS(rootTypeName, stack + [(attrName, attrDefinition),], types)}}
{%- else -%}
// no std getter for {{rootTypeName}}
{%- for nm, def in stack -%}/{{nm}}{%- endfor%}/{{attrName}}
{%- endif %}
{% if attrDefinition.enable_by_macro %}#endif //defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif %}
{%- endfor %}
{%- for customGetterName, customGetter in types[stack[-1][1].type.referencedType].get('$customGetters', {}).items() %}
{{ IMPLEM(rootTypeName, stack + [(customGetterName, {}),], customGetter) }}
{%- endfor %}
{% endmacro %}

{#                                    ________________________________________
 # _________________________________/ Automatic Object Getters implementation
 # Generates implementation for attribute <attrName:str> of object of type
 # <rootType:obj>
 #}
{% macro IMPLEMENT_GETTERS(typeName, type, types) %}
{%  for attrName, attrDefinition in type.items() if not attrName.startswith('$') -%}
{%  if attrDefinition.enable_by_macro %}#if defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif -%}
{%  if attrDefinition.type.isPOD and 'getter' not in attrDefinition -%}
{{IMPLEM(typeName, [(attrName, attrDefinition),], attrDefinition.cpp)}}
{% elif attrDefinition.type.referencedType %}
{{PROPAGATE_GETTERS(typeName, [(attrName, attrDefinition),], types)}}
{%  elif not attrDefinition.getter -%}
// getter for {{typeName}}::{{attrName}} disabled
{% else %}
// FIXME: unknown case for attribute {{typeName}}::{{attrName}}
{%- endif %}
{%- if attrDefinition.enable_by_macro %}#endif //defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif %}
{%- endfor %}
{%- for customGetterName, customGetter in type.get('$customGetters', {}).items() %}
static StdFloat_t _{{typeName}}_get_{{customGetterName}}( const {{typeName}} & obj ) { {{customGetter.cpp}} }
{%- endfor %}
{% endmacro %}

{% macro _GETTER_STR_NAME(stack) -%}
    {{stack|join('.', attribute=0)}}
{%- endmacro %}

{#                                                          __________________
 # _______________________________________________________/ Getter entry item
 # Using stack (list) of (name:str, type:obj) generates getter 
 #}
{% macro _REGISTER(rootTypeName, stack, getterCode=False) -%}
{% if stack|length > 1 -%}
// propagated from {{_JOIN_STACK(stack)}}
{% endif %}{ "{{_GETTER_STR_NAME(stack)}}", { {{(stack[-1][1].doc.strip().split('\n')[0] if stack[-1][1].doc else "propagated from " + _JOIN_STACK(stack)) |tojson}}, {{_GETTER_NAME(rootTypeName, stack)}} } },
{%- endmacro %}

{#                                                   _________________________
 # ________________________________________________/ Propagated Getters Entry
 # Recursive macro expanding getter into registered entry
 #}
{% macro _REGISTER_GETTERS(rootTypeName, stack, types) %}
{%  for attrName, attrDefinition in types[stack[-1][1].type.referencedType].items() if not attrName.startswith('$') -%}
{%  if attrDefinition.enable_by_macro %}#if defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif -%}
{%  if attrDefinition.type.isPOD and 'getter' not in attrDefinition -%}
{{_REGISTER(rootTypeName, stack + [(attrName, attrDefinition),])}}
{%- elif attrDefinition.type.referencedType %}
{{_REGISTER_GETTERS(rootTypeName, stack + [(attrName, attrDefinition),], types)}}
{%- else %}// no std getter for {{rootTypeName}}
{%- for nm, def in stack -%}/{{nm}}{%- endfor%}/{{attrName}}
{%- endif %}
{% if attrDefinition.enable_by_macro %}#endif //defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif %}
{%- endfor %}
{%- for customGetterName, customGetter in types[stack[-1][1].type.referencedType].get('$customGetters', {}).items() %}
{{_REGISTER(rootTypeName, stack + [(customGetterName, {}),], customGetter) }}
{%- endfor %}
{% endmacro %}

{#                                      ______________________________________
 # ___________________________________/ Automatic Object Getters registration
 # Generates array of getters structs for initialization
 #}
{% macro DEFINE_GETTERS_ENTRIES(typeName, type, types) %}
{%  for attrName, attrDefinition in type.items() if not attrName.startswith('$') -%}
{%  if attrDefinition.enable_by_macro %}#if defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif -%}
{%  if attrDefinition.type.isPOD and 'getter' not in attrDefinition -%}
{{_REGISTER(typeName, [(attrName, attrDefinition),])}}
{% elif attrDefinition.type.referencedType %}
{{_REGISTER_GETTERS(typeName, [(attrName, attrDefinition),], types)}}
{%  elif not attrDefinition.getter %}
// getter for {{typeName}}::{{attrName}} disabled
{% else %}
// FIXME: unknown case for attribute {{typeName}}::{{attrName}}
{%- endif %}
{%- if attrDefinition.enable_by_macro %}#endif //defined({{attrDefinition.enable_by_macro}}) && {{attrDefinition.enable_by_macro}}{% endif %}
{%- endfor %}
{%- for customGetterName, customGetter in type.get('$customGetters', {}).items() %}
{ "{{customGetterName}}", { {{customGetter.doc.strip().split('\n')[0] | tojson }}, _{{typeName}}_get_{{customGetterName}} } },
{%- endfor %}
{% endmacro %}

