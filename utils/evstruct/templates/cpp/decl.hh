#pragma once

/**\file
 * {{this.doc.strip().split('\n') | join('\n * ')}}
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at {{this.generated}} with template {{this.template}}
 * \version {{this.version}}
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
{% for inc in cpp.include_decl -%}
#include "{{inc}}"
{% endfor %}
{%if cpp.preambule_decl%}{{cpp.preambule_decl}}{%endif%}

namespace na64dp {
{% for name, decl in types.items() %}
namespace event {
{% include 'cpp/decl-type.hh' %}
{% include 'cpp/decl-traits.hh' %}
} // namespace ::na64dp::event

namespace util {
/// Sets instance of `{{name}}` to uninitialized state
void reset( event::{{name}} & );
}  // namespace ::na64dp::util

{%if '$poolID' in decl %}
namespace mem {
template<> struct PoolTraits<event::{{name}}> {
    static constexpr int id = {{decl['$poolID']}};
};
}  // namespace ::na64dp::mem
{% endif %}
{% endfor %}

namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist
{% for name, decl in types.items() %}
{%- for attrName, attr in decl.items() if (not attrName.startswith('$')) and attr.type.mainAssociation %}
{% if attr.type.setOf %}
{%- if attr.enable_by_macro %}#if defined({{attr.enable_by_macro}}) && {{attr.enable_by_macro}} {% endif %}
template<>
struct Association<event::{{name}}, event::{{attr.type.setOf}}> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::{{attr.type.cppContainerTags}};
    /// Type of (main) association of `{{attr.type.setOf}}` instances with
    /// instance `{{name}}`.
    typedef {{attr.type.cppType}} Collection;
    /// Returns (main) collection of `{{attr.type.setOf}}` instances associated
    /// with `{{name}}` by attribute `{{attrName}}`.
    static Collection & map(event::{{name}} & parent) { return parent.{{attrName}}; }
    /// Remove instance of `{{attr.type.setOf}}` from (main) collection
    /// associated it with `{{name}}` by attribute `{{attrName}}`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};
{% if attr.enable_by_macro %}#endif{% endif -%}
{% endif %}
{% endfor %}
{% endfor %}

{%- if allCompoundTypes is defined -%}
#define M_for_every_compound_type( m, ... ) \
{%-for typeName, typeDef in allCompoundTypes.items() %}
    m( {{typeName}}, __VA_ARGS__ ) \
{%-endfor%}
    /* ... */
{%endif%}

}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_{{this.objGroup}}_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif

