# Event Structure Generator

Generates source codes for event data object model.
Usage is summarized in `Makefile`. To (re-)generate run `make`
from main project's dir as `make -f utils/evstruct/Makefile`.

Snippets:

- generate event structure
    $ python3 utils/evstruct/generate.py \
        presets/event-struct/sadc.yaml presets/event-struct/apv.yaml \
        presets/event-struct/stwTDC.yaml presets/event-struct/calo.yaml \
        presets/event-struct/track.yaml presets/event-struct/event.yaml
- dump single type declaration as a JSON for inspection (uses `pygments` if
available for highlight):
    $ python3 utils/evstruct/generate.py --js-dump-one=APVHit=apv-hit.json \
        presets/event-struct/sadc.yaml presets/event-struct/apv.yaml \
        presets/event-struct/stwTDC.yaml presets/event-struct/calo.yaml \
        presets/event-struct/track.yaml presets/event-struct/event.yaml
- dump event structure to make diagram
    $ python3 utils/evstruct/generate.py --js-dump-all=event.json \
        presets/event-struct/sadc.yaml presets/event-struct/apv.yaml \
        presets/event-struct/stwTDC.yaml presets/event-struct/calo.yaml \
        presets/event-struct/track.yaml presets/event-struct/event.yaml
- draw the data types diagram
    $ python3 utils/evstruct/diagram.py

Packages needed by Python scripts:
    - jinja2
    - pyyaml
    - icecream
    - graphviz
