from collections import OrderedDict
import copy

gAvroType2CType = {
    # Avro type     : C equivalents
    "null"          : [],
    "boolean"       : ["bool"],
    "int"       : ["int", "int32_t", "int16_t", "uint16_t", "int8_t", "uint8_t"
                      , "APVDAQWire_t"  # uint16_t, TODO: configurable
                      , "APVPhysWire_t"  # uint16_t, TODO: configurable
                      ],
    "long"          : ["long", "int64_t", "uint32_t"],
    "float"         : ["float", "StdFloat_t"],  # TODO: StdFloat_t must be parameter!
    "double"        : ["double"],
    # ...
}

gFixedTypes = {
    "size_t": 8,    # TODO: retrieve from compiler?
    "EventID" : 8,  # uint64_t, TODO: parameter
    "DetID": 4,     # uint32_t, TODO: parameter
    "PlaneKey": 4,  # uint32_t, TODO: parameter
    "TrackID": 4,   # uint32_t, TODO: parameter
    "uin64_t": 8,
    "std::pair<time_t, uint32_t>": (12, "pair_time_t_uint32_t"),  # TODO: derive from C++?
}

# TODO: fill it from cstruct or something
# These values used to calculate array size for C arrays of fixed length in
# case schema represents these arrays as fixed.
gSizes = {
    'int8_t' : 1,   'uint8_t' : 1,  'char':     1,
    'int16_t': 2,   'uint16_t': 2,  'short':    2,
    'int32_t': 4,   'uint32_t': 4,  'int':      4,
    'int64_t': 8,   'uint64_t': 8,  'long':     8,
    'float'  : 4,   'StdFloat_t': 4,  # TODO: StdFloat_t
    'double' : 8,
}

gCType2AvroType = {}
for avroType, cTypes in gAvroType2CType.items():
    for cType in cTypes:
        gCType2AvroType[cType] = avroType


class AvroTypesRegistry(object):
    def __init__(self, allTypes, namespace="na64sw", refTypeSize=8):
        self.namespace = namespace
        self.types = {}
        self.allTypes = copy.deepcopy(allTypes)
        self.refTypeSize = refTypeSize

    def resolve_POD_as_avro_type(self, typeDef):
        """
        Assumes that type definition is of "plain old type": scalar values,
        C-structs, arrays of scalar values, etc. For simple scalar values,
        closest Avro type will be returned, for the rest, appropriate "fixed"
        type will be created (or its name returned).
        """
        cppType = typeDef['cppType']
        tpName = cppType
        staticSize = None
        if tpName in gFixedTypes.keys() \
        and type(gFixedTypes[tpName]) is tuple:
            tpName = gFixedTypes[tpName][1]
        if 'staticSize' in typeDef:
            staticSize = int(typeDef['staticSize'][1:][:-1])
            tpName = "array_%s_%d"%(tpName, staticSize)
        fullyQualifiedName = self.namespace + '.' + tpName if self.namespace else tpName
        if fullyQualifiedName in self.types.keys(): return fullyQualifiedName
        # otherwise, build a new type definition
        if cppType in gFixedTypes:
            typeName = cppType
            typeSize = gFixedTypes[cppType]
            if type(typeSize) is tuple: typeSize = typeSize[0]
            if 'staticSize' in typeDef:
                avroTypeDef = {
                        "type": "fixed",
                        "size": typeSize*staticSize,
                        "name": tpName,
                    }
                if self.namespace: avroTypeDef['namespace'] = self.namespace
            else:
                avroTypeDef = {
                        "type": "fixed",
                        "size": typeSize,
                        "name": tpName,
                    }
                if self.namespace: avroTypeDef['namespace'] = self.namespace
            self.types[fullyQualifiedName] = avroTypeDef
            return avroTypeDef
        if cppType not in gCType2AvroType.keys():
            raise RuntimeError("Can not resolve C type `%s' to Avro type"%typeDef['cppType'])
        if 'staticSize' in typeDef:
            avroTypeDef = {
                    "type": "fixed",
                    "size": gSizes[typeDef['cppType']]*staticSize,
                    "name": tpName,
                }
            if self.namespace: avroTypeDef['namespace'] = self.namespace
            self.types[fullyQualifiedName] = avroTypeDef
            return avroTypeDef
        else:
            return gCType2AvroType[typeDef['cppType']]

    def resolve_ref_as_avro_type(self, tp):
        """
        Returns a reference type.
        """
        assert tp in self.allTypes  # value type should be always compund type
        if self.namespace:
            fullyQualifiedName = f"{self.namespace}._ref{tp}"
        else:
            fullyQualifiedName = f"_ref{tp}"
        if fullyQualifiedName in self.types: return fullyQualifiedName
        self.types[fullyQualifiedName] = {
                "type": "fixed",
                "size": self.refTypeSize,
                "name": f"_ref{tp}",
                "doc": f"ID of an item in collection of {tp} instances"
            }
        if self.namespace: self.types[fullyQualifiedName]['namespace'] = self.namespace
        return self.types[fullyQualifiedName]

    def resolve_set_avro_type(self, td):
        """
        Creates/returns array type.
        """
        assert 'setOf' in td.keys()
        if type(td['setOf']) is not str:
            assert type(td['setOf']) is dict
            assert 'oneOf' in td['setOf'].keys()  # A union type is only permitted case here
            entryType = ["null"]
            for branchName, branchType in td['setOf']['oneOf'].items():
                tp = self.resolve_avro_type(branchType['type'])
                entryType.append(tp)
            # Returned is an array of pairs. A pair is defined as a record
            # with key and value fields, where value is actually a union.
            # ...
            return {
                    'type': 'array',
                    'items' : {
                        'type': 'record',
                        'name': f"UnionsBy{td['idxBy']}",  # TODO: unique name
                        'doc': "Union of %s"%(', '.join(td['setOf']['oneOf'].keys())),
                        'fields': [
                            {
                                'name': 'key',
                                'type': self.resolve_POD_as_avro_type({'cppType': td['idxBy']})
                            },
                            {
                                'name': 'value',
                                'type': entryType
                            }
                        ]  # fields
                    } # items
                }  # type
        # xxx, seems there is no such thing as "array type name"
        #arrayTypeName = f"{td['setOf']}By{td['idxBy']}"
        #fullyQualifiedName = f"{self.namespace}.{arrayTypeName}" if self.namespace else arrayTypeName
        #if fullyQualifiedName in self.types.keys(): return fullyQualifiedName
        # get value type
        valueType = self.resolve_ref_as_avro_type(td['setOf'])
        arrayEntryTypeName = f"{td['setOf']}By{td['idxBy']}Entry"
        fullyQualifiedName = f"{self.namespace}.{arrayEntryTypeName}" \
                if self.namespace else arrayEntryTypeName
        entryType = None
        if fullyQualifiedName in self.types:
            entryType = fullyQualifiedName
        else:
            # define array entry type (key+value pair)
            entryType = {
                    "type": "record",
                    "name": arrayEntryTypeName,
                    #"namespace": self.namespace,
                    "fields": [
                        {
                            "type": "record",
                            "name": "key",
                            # key is always POD:
                            "type": self.resolve_POD_as_avro_type({'cppType': td['idxBy']})
                        },{
                            "type": "record",
                            "name": "value",
                            "type": valueType
                        }
                    ]
                }
        if self.namespace: entryType['namespace'] = self.namespace
        self.types[fullyQualifiedName] = entryType
        arrayType = {
                "type": "array",
                #"name": arrayTypeName,
                #"namespace": self.namespace,
                "items": entryType
            }
        if 'doc' in td.keys(): arrayType['doc'] = td['doc']
        #if self.namespace: arrayType['namespace'] = self.namespace
        #self.types[fullyQualifiedName] = arrayType
        return arrayType

    def resolve_avro_type(self, typeDef):
        """
        Versatile type generator.
        """
        if 'isPOD' in typeDef.keys() and typeDef['isPOD'] \
        or (typeDef['cppType'] in gCType2AvroType.keys()):
            return self.resolve_POD_as_avro_type(typeDef)
        elif 'referencedType' in typeDef.keys():
            return self.resolve_ref_as_avro_type(typeDef['referencedType'])
        elif 'setOf' in typeDef.keys():
            return self.resolve_set_avro_type(typeDef)

    def record_field(self, attrName, attrDefinition):
        """
        Returns Avro record's field definition.
        """
        avroRecordField = {'name': attrName}
        if 'doc' in attrDefinition: avroRecordField['doc'] = attrDefinition['doc']
        avroRecordField['type'] = self.resolve_avro_type(attrDefinition['type'])
        if avroRecordField['type'] is None:
            return None  # TODO: raise exception instead
        return avroRecordField

    def record_type(self, typeName, typeDefinition):
        """
        Turns type defined by evstruct configuration into Avro's "record".
        """
        fullyQualifiedName = f'{self.namespace}.{typeName}'
        if fullyQualifiedName in self.types:
            return self.types[fullyQualifiedName]
        D = {
            "type": "record",
            "name": typeName,
            #"namespace": self.namespace,
            "fields": []
            }
        if self.namespace: D['namespace'] = self.namespace
        for attrName, attrDefinition in typeDefinition.items():
            if attrName.startswith('$'): continue
            avroAttrDef = self.record_field(attrName, attrDefinition)
            if avroAttrDef is None:
                continue  # TODO: raise exception instead
            D["fields"].append(avroAttrDef)
        return D


def generate_avro_schema( common, types
                        , collectionSuffix='_collection'
                        , namespace=None #'na64sw'
                        ):
    R = AvroTypesRegistry(types, namespace=namespace)
    # For every type create collection
    collections = []
    for typeName, typeDef in types.items():
        if typeName == 'Event' or typeName.startswith('$'): continue
        recordType = R.record_type(typeName, typeDef)
        collections.append({
                'name': '_collection' + typeName,
                'type': {
                    'type': 'array',
                    'items': recordType,
                }
            })
    eventSchema = R.record_type('Event', types['Event'])
    eventSchema['fields'] = collections + eventSchema['fields']
    eventSchema['namespace'] = namespace
    # For every pool type, create corresponding collection
    # ...
    return eventSchema
        

