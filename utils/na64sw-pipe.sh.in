#!/bin/bash

# This script is designed to run in the `na64sw.build/` directory and must not
# appear in the final (installed) destinations. Its goal is to override certain
# application's setting in order to allow users to run pipe app without
# actually installing it. One may find it in two forms -- a CMake template with
# ".in" suffix in the sources dir and output file in the build dir.

# NOTE: since one can not run shall script in gdb, this script provides
# additionally an option "--debug" launching pipeline app in gdb.

THIS_SCRIPT_LOC="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )" && pwd )"
# ^^^ alternatively one may use CMAKE_CURRENT_BINARY_DIR

export NA64SW_PREFIX="${THIS_SCRIPT_LOC}"
export @NA64SW_RUN_CFG_PATH_ENVVAR@=".:${THIS_SCRIPT_LOC}/share/na64sw/run/"
export @NA64SW_MODULES_PATH_ENVVAR@=".:${THIS_SCRIPT_LOC}/extensions/"

for arg do
  shift
  case $arg in
    (--debug) DEBUG=gdb ;;
    (--mem-debug) DEBUG=mem ;;
    (*) set -- "$@" "$arg" ;;
  esac
done

if [ -f supp.sh ] ; then
    source supp.sh
fi


# forward execution to actual application
if [ -z ${DEBUG+x} ] ; then
    ${THIS_SCRIPT_LOC}/utils/na64sw-pipe \
        --log-cfg="@CMAKE_CURRENT_SOURCE_DIR@/presets/logging.yaml" \
        --calibrations="@CMAKE_CURRENT_SOURCE_DIR@/presets/calibrations.yaml" \
        --run="${THIS_SCRIPT_LOC}/share/na64sw/run/default.yaml" \
        --input-cfg="${THIS_SCRIPT_LOC}/share/na64sw/source-ddd.yaml" \
        "$@"
elif [ ${DEBUG} == "gdb" ] ; then
    gdb --args ${THIS_SCRIPT_LOC}/utils/na64sw-pipe \
        --log-cfg="@CMAKE_CURRENT_SOURCE_DIR@/presets/logging.yaml" \
        --calibrations="@CMAKE_CURRENT_SOURCE_DIR@/presets/calibrations.yaml" \
        --run="@CMAKE_CURRENT_BINARY_DIR@/share/na64sw/run/default.yaml" \
        --input-cfg="@CMAKE_CURRENT_BINARY_DIR@/share/na64sw/source-ddd.yaml" \
        "$@"
else
    valgrind --suppressions=/home/rdusaev/Sw/etc/valgrind-root.supp \
        ${THIS_SCRIPT_LOC}/utils/na64sw-pipe \
        --log-cfg="@CMAKE_CURRENT_SOURCE_DIR@/presets/logging.yaml" \
        --calibrations="@CMAKE_CURRENT_SOURCE_DIR@/presets/calibrations.yaml" \
        --run="@CMAKE_CURRENT_BINARY_DIR@/share/na64sw/run/default.yaml" \
        --input-cfg="@CMAKE_CURRENT_BINARY_DIR@/share/na64sw/source-ddd.yaml" \
        "$@"
fi


