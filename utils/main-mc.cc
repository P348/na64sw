#include "na64app/app.hh"

// TODO: this C-macro was removed on Geant4.10.6, see:
//      https://geant4-forum.web.cern.ch/t/g4ui-use-and-g4vis-use-retired-in-4-10-06/2678/2
// We have to check Geant4 version here and unconditionally define it to
// provide compatibility with older versions of Geant4.
#define G4VIS_USE 1
#define G4UI_USE 1

#include <G4RunManager.hh>
#include <G4UImanager.hh>
#ifdef G4VIS_USE
    #include <G4VisExecutive.hh>
    #include <G4UIExecutive.hh>
#endif

#include "na64mc/messenger.hh"
#include "na64mc/g4api/detectorConstruction.hh"
#include "na64mc/g4api/UISession.hh"

#include "na64util/log4cpp-extras.hh"
#include "na64util/YAMLLog4cppConfigurator.hh"
#include "na64util/Geant4UIAppender.hh"

#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PropertyConfigurator.hh>

#if defined(ROOT_FOUND) && ROOT_FOUND
#include "na64util/ROOT-sighandlers.hh"
#include <TFile.h>
#endif

#include <getopt.h>

/// General application settings and options set from command line for MC app
/// Current command line interface is a rather simplistic one since the genuine
/// way of configuring the Geant4 application is its macro files scenario.
struct ApplicationConfig {
    /// Logging configuration file location
    std::string logCfg;
    /// Whether to run in batch mode
    bool batchMode;
    /// Path to scenario file to be executed
    std::string g4MacFile;
    /// Geant4 UI session type
    std::string g4UISessionType;
};

static void
initialize_defaults( ApplicationConfig & appCfg ) {
    auto cfgRootDirs = na64dp::util::expand_names("${" NA64SW_CONFIG_ROOT_ENVVAR "}");  // TODO: deprecated, rewrite
    std::string cfgRootDir;
    if(cfgRootDirs.size() == 1) {
        cfgRootDir = cfgRootDirs[0];
    }
    if( cfgRootDir.empty() ) {
        cfgRootDir = "../na64sw/";
    }

    appCfg.batchMode = false;
    appCfg.logCfg = cfgRootDir + "presets/logging.yaml";
}

static void
usage_info( const char * appName
          , std::ostream & os
          , const ApplicationConfig & appCfg ) {
    os << "NA64SW Monte-Carlo simulation executable." << std::endl
       << "Usage:" << std::endl
       << "  $ " << appName
       << " [-h,--help]"
          " [-b,--batch]"
          " [-J,--log-cfg <log4cppConfig>]"
          " [--force-ui-type <Geant4-UI-session-type>]"
          " <.mac-file>"
       << std::endl;
    os << "Runs the NA64SW Geant4 Monte-Carlo simulation application with"
          " macro provided in <.mac-file> argument in the (G)UI session"
          " choosen either automatically, or forced manually with"
          " <Geant4-UI-session-type> argument." << std::endl
       << "The -J,--log-cfg=" << appCfg.logCfg << " should refer to a file"
          " with logging configuration"
          " in a format of `log4cpp' library to steer the logging procedures."
       #if defined(ROOT_FOUND) && ROOT_FOUND
       << "ROOT is linked against this executable, its signal handlers are"
          " suppressed by default. One may keep them enabled by setting the"
          " KEEP_ROOT_SIGHANDLERS environment variable."
       #endif
       << std::endl;
}

static int
configure_app( int argc, char * argv[]
             , ApplicationConfig & appCfg ) {

    static struct option longOpts[] = {
        { "help",                   no_argument,        NULL, 'h' },
        { "batch",                  no_argument,        NULL, 'b' },
        { "force-ui-type",          required_argument,  NULL,  0  },
        { "log-cfg",                required_argument,  NULL, 'J' },
        { NULL, 0x0, NULL, 0x0 },
    };

    int c, optIdx;
    bool hadError = false;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt_long( argc, argv, "hlbJ:"
                          , longOpts, &optIdx )) != -1) {
        switch(c) {
            case '0' :
                std::cerr << "Unable to parse option \""
                          << longOpts[optIdx].name << "\""  << std::endl;
                break;
            case 'b' :
                appCfg.batchMode = true;
                break;
            case 'J' :
                appCfg.logCfg = optarg;
                break;
            case 'h' :
                usage_info( argv[0], std::cout, appCfg );
                return 1;
            case ':' :
                std::cerr << "Option -" << optopt << " requires an argument."
                          << std::endl;
                hadError = true;
                break;
            case '?' :
                std::cerr << "Unrecognized option '-" << optopt << "'."
                          << std::endl;
                hadError = true;
        }
    }
    if( hadError ) return -1;

    // Interpret positional arguments: consider first one as macro file, if
    // it is not `--'
    if( optind != argc - 1 ) {
        std::cerr << "No macro file argument provided." << std::endl;
        return -1;
    }
    appCfg.g4MacFile = argv[optind];

    return 0;
}

int
main(int argc, char * argv[]) {
    #if defined(ROOT_FOUND) && ROOT_FOUND
    // disable ROOT signal handlers
    {
        const char * v = getenv("KEEP_ROOT_SIGHANDLERS");
        if( !v || !(v && ('1' == v[0] && '\0' == v[1] )) ) {
            disable_ROOT_sighandlers();
        } else {
            std::cerr << "ROOT signal handlers are kept." << std::endl;
        }
    }
    #endif

    ApplicationConfig appCfg;
    initialize_defaults(appCfg);
    int rc = configure_app( argc, argv, appCfg );
    if( rc < 0 ) {
        usage_info( argv[0], std::cerr, appCfg );
        return EXIT_FAILURE;
    }
    if( rc > 0 ) {
        return EXIT_SUCCESS;
    }

    na64dp::util::inject_extras_to_log4cpp();

    //                                                                    _____
    // _________________________________________________________________/ Logs
    // Initialize logging subsystem
    log4cpp::Appender * consoleAppender = nullptr;
    na64dp::util::Geant4UISessionAppender * g4uisAppender = nullptr;
    if( ! appCfg.logCfg.empty() ) {
        // configure from file
        na64dp::util::YAMLLog4cppConfigurator::configure(appCfg.logCfg);
    } else {
        // no logging props file specified -- create basic logging
        consoleAppender = new log4cpp::OstreamAppender("console", &std::cout);
        g4uisAppender = new na64dp::util::Geant4UISessionAppender("Geant4 session");
        consoleAppender->setLayout(new log4cpp::BasicLayout());
        log4cpp::Category & L = log4cpp::Category::getRoot();
        //L.setPriority( appCfg.vLvl );
        L.addAppender( consoleAppender );
        L.addAppender( g4uisAppender );
        L.info( "Using zero-conf logging." );
    }
    log4cpp::Category & L = log4cpp::Category::getRoot();

    // If not in in batch mode, instantiate (G)UI prior to execution
    G4UIExecutive * uiPtr = nullptr;
    if( !appCfg.batchMode ) {
        uiPtr = new G4UIExecutive( argc, argv, appCfg.g4UISessionType );
        na64dp::util::Geant4UISessionAppender::session = uiPtr->GetSession();
    }
    G4UImanager* uiMgrPtr = G4UImanager::GetUIpointer();
    // Inject shimmering session to collect system Geant4 logs;
    na64dp::mc::UISession * uiSession = new na64dp::mc::UISession();
    uiMgrPtr->SetCoutDestination(uiSession);

    //                                   ______________________________________
    // ________________________________/ Main Geant4 API objects instantiation
    G4RunManager * runManager = new G4RunManager();
    #if 0
    // Messenger configuration instance target
    na64dp::mc::Messenger::Config cfg;
    // Standard Geant4 API interfaces
    na64dp::mc::DetectorConstruction * detectorConstruction;
    runManager->SetUserInitialization( detectorConstruction
            = new na64dp::mc::DetectorConstruction(cfg) );
    #else
    #endif
    // Instantiate modular configuration instance
    na64dp::mc::ModularConfig cfg;
    // Instantiate the main na64dp::mc messenger
    na64dp::mc::Messenger * msgr = new na64dp::mc::Messenger(cfg);

    // Visualization
    G4VisManager* visManager = nullptr;
    if( ! appCfg.batchMode ) {
        visManager = new G4VisExecutive;
        visManager->Initialize();
    }

    // Execute .mac file if given
    if( ! appCfg.g4MacFile.empty() ) {
        std::string macExecCmd = na64dp::util::format( "/control/execute %s"
                , appCfg.g4MacFile.c_str() );
        uiMgrPtr->ApplyCommand(macExecCmd);
    }
    if( uiPtr ) {
        uiPtr->SessionStart();  // blocks execution till user leaves (G)UI
        na64dp::util::Geant4UISessionAppender::session = nullptr;
        delete uiPtr;
    }

    cfg.notifier.notify(na64dp::mc::Notification::applicationDone);
    delete msgr;
    if(visManager) delete visManager;
    delete runManager;

    L.info("All done -- exiting normally.");
    log4cpp::Category::shutdown();

    if(consoleAppender) delete consoleAppender;
    if(g4uisAppender) delete g4uisAppender;

    return rc;
}

