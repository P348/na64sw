#include "na64app/app.hh"
#include "na64util/gsl-integration.hh"
//#include "na64event/data/event.hh"

//#include "na64dp/abstractEventSource.hh"
//#include "na64dp/pipeline.hh"
//#include "na64dp/processingInfo.hh"
//#include "na64util/runtimeDirs.hh"

#include "na64calib/manager.hh"

//#if defined(ZMQ_FOUND) && ZMQ_FOUND
//#   include "na64dp/processingInfoNw.hh"
//#endif

#include "na64util/log4cpp-extras.hh"
#include "na64util/YAMLLog4cppConfigurator.hh"

#include <log4cpp/Priority.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PropertyConfigurator.hh>

#if defined(ROOT_FOUND) && ROOT_FOUND
#include "na64util/ROOT-sighandlers.hh"
//#include <TFile.h>
#endif

//#include <EventDisplay.h>
//#include "na64app/GenFit_EvDisplay.hh"

#include <yaml-cpp/yaml.h>
#include <gsl/gsl_errno.h>

#include <unistd.h>
#include <getopt.h>

//#include <iostream>

// General application-relevant settings and options set from command line
struct ApplicationConfig {
    // Calibration config file
    std::string calibCfgURI;
    // Logger configuration file
    std::string logCfg;
    // Loadable modules list
    std::list<std::string> modules;
    /// Event ID to load calib data for
    na64dp::EventID eid;
    /// Date and time to load calib data for
    std::pair<time_t, uint32_t> dt;
    // ... more runtime options may be added here
};

// Sets some default values for the app
static void
initialize_defaults( ApplicationConfig & appCfg ) {
    na64dp::util::set_std_environment_variables();
    auto cfgRootDirs = na64dp::util::expand_names("${NA64SW_RUN_CFG_PATH}");

    #ifdef NA64SW_PREFIX
    appCfg.calibCfgURI     = NA64SW_PREFIX "/etc/na64sw/calibrations.yaml";
    appCfg.logCfg          = NA64SW_PREFIX "/etc/na64sw/logging.yaml";
    #endif
    // ^^^ TODO: default input has to become the stdin here
    appCfg.modules.push_back( "std-handlers" ); // extensions/handlers  // standard handlers
    //appCfg.modules.insert( "exp-handlers" );  // experimental handlers
    appCfg.modules.push_back( "ddd" );  // DDD data source
    appCfg.eid = na64dp::EventID(0);
    appCfg.dt.first = 0x0;
    appCfg.dt.second = 0x0;
    // ... more default values for runtime options may be added here
}

void
usage_info( const char * appName
          , std::ostream & os
          , const ApplicationConfig & appCfg ) {
    std::string dftModules;
    {
        std::ostringstream oss;
        for( auto mf : appCfg.modules ) {
            oss << mf << ",";
        }
        oss << "\b";
        dftModules = oss.str();
    }
    os << "\033[1mNA64SW command-line browser for calib data\033[0m" << std::endl
       << "Usage:" << std::endl
       << "  $ \033[1m" << appName
       << "\033[0m [-h,--help] "
          " [-c,--calibrations <calibCfgURI>]"
          " [-m,--load-module <module>]"
          " [-J,--log-cfg <file>]"
          " [-r,--run-no <runNumber>]"
          " [-t,--datetime <date,time>]"
          // ...
       << std::endl;
    os << "where:" << std::endl
       << "  \033[0;32m-h,--help\033[0m causes app to print out this message and exit." << std::endl
       << "  \033[0;32m-c,--calibrations\033[0m <calibCfgURI=\033[2m" << appCfg.calibCfgURI << "\033[0m> sets the YAML file"
          " configuring the calibration system." << std::endl
       << "  \033[0;32m-J,--log-cfg\033[0m <log4cppConfig=\033[2m" << appCfg.logCfg << "\033[0m> is log4cpp configuration"
          " \"properties\" file that sets up logging configuration (sinks,"
          " layouts, etc)." << std::endl
       << "  \033[0;32m-m,--load-module\033[0m <moduleFile=\033[2m" << dftModules
       << "\033[0m> dynamically loads an extending"
          " module file (typically, a shared object file). The module path may"
          " be given as follows: a path to the ELF file, the name of the"
          " loadable without leading `lib` and"
          " trailing .so to be found in one of the directories given by"
          " \033[0;32mNA64SW_MODULES_PATH\033[0m environment variable."
       << std::endl
       << "  \033[0;32m-R,--run-no\033[0m <runNo> run number to load calibs"
          " for (just a single integer, like 1234)."
       << std::endl
       << "  \033[0;32m-t,--datetime\033[0m <date,time> date and time to load"
          " calibs for (Geneva time)."
       << std::endl  // ^^^ TODO: document format
       << "  NOTE: The loadable module look-up paths may be provided by"
          " \033[0;32mNA64SW_MODULES_PATH\033[0m environment variable. By"
          " default this variable is set to \"\033[2m" NA64SW_MODULES_PATH_DEFAULT
          "\033[0m\" (a colon-separated list of paths)."
          " One may override it with colon-separated list of directories"
          " where modules look-up shall be performed. Empty tokens between"
          " colons will be ignored."
       << std::endl
       << "  NOTE: Runtime config lookup is done in the directories listed in "
          "\033[0;32mNA64SW_RUN_CFG_PATH\033[0m environment variable"
          " (default is \"\033[2m" NA64SW_RUN_CFG_PATH_DEFAULT "\033[0m\"),"
          " a colon-delimited list of directories paths."
       << std::endl
       ;
}

// Sets the application config based on command line arguments. Returns
// non-zero in case of problems.
int set_app_config( int argc, char * argv[]
                  , ApplicationConfig & appCfg ) {

    static struct option longOpts[] = {
        { "help",           no_argument,       NULL, 'h' },
        { "calibrations",   required_argument, NULL, 'c' },
        { "load-module",    required_argument, NULL, 'm' },
        { "log-cfg",        required_argument, NULL, 'J' },
        { "run-no",         required_argument, NULL, 'R' },
        { "datetime",       required_argument, NULL, 't' },
        { NULL, 0x0, NULL, 0x0 },
    };
    std::string listToks;
    int c, optIdx;
    bool hadError = false;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt_long( argc, argv, "c:m:J:h"
                          , longOpts, &optIdx )) != -1 ) {
        switch(c) {
            case '0' :
                //if(!strcmp( longOpts[optIdx].name
                //          , "event-buffer-size-kb")) {
                //    appCfg.evBufSizeKb = atol(optarg);
                //} else
                {
                    std::cerr << "Unable to parse option \""
                              << longOpts[optIdx].name << "\""  << std::endl;
                    hadError = true;
                }
                break;
            case 'c' :
                appCfg.calibCfgURI = optarg;
                break;
            case 'm' :
                appCfg.modules.push_back(optarg);
                break;
            case 'J' :
                appCfg.logCfg = optarg;
                break;
            case 'R' :
                throw std::runtime_error("TODO: parse run number");
                //appCfg.eid
                break;
            case 't' :
                throw std::runtime_error("TODO: parse date+time");
                //appCfg.dt (std::pair<time_t, uint32_t>)
                break;
            // ... more runtime options may be added here
            case 'h' :
                usage_info( argv[0], std::cout, appCfg );
                return -1;
            case ':' :
                std::cerr << "Option -" << optopt << " requires an argument."
                          << std::endl;
                hadError = true;
                break;
            case '?' :
                std::cerr << "Unrecognized option '-" << optopt << "'."
                          << std::endl;
                hadError = true;
        }
    }
    if( hadError )
        return -2;
    //for ( ; optind < argc; ++optind ) {  // TODO: set runno/datetime
    //    appCfg.inputs.push_back(argv[optind]);
    //}
    // Before any sybsystems initialized, load subsidiary modules
    setenv( "NA64SW_MODULES_PATH", NA64SW_MODULES_PATH_DEFAULT, 0 );
    int modulesLoadRC = na64dp::util::load_modules( appCfg.modules
                                                  , getenv("NA64SW_MODULES_PATH") );
    return modulesLoadRC ? modulesLoadRC : 0;
}

int
main(int argc, char *argv[]) {
    //                                                               __________
    // ____________________________________________________________/ App infra
    #if defined(ROOT_FOUND) && ROOT_FOUND
    // disable ROOT signal handlers
    {
        const char * v = getenv("KEEP_ROOT_SIGHANDLERS");
        if( !v || !(v && ('1' == v[0] && '\0' == v[1] )) ) {
            disable_ROOT_sighandlers();
        } else {
            std::cerr << "ROOT signal handlers are kept." << std::endl;
        }
    }
    #endif
    // Set custom exception-throwing error handler for GSL
    /*old_handler = */ gsl_set_error_handler( na64dp::errors::GSLError::na64sw_gsl_error_handler );

    ApplicationConfig appCfg;
    initialize_defaults( appCfg );
    int rc = set_app_config( argc, argv, appCfg );  // also loads modules
    if( -2 == rc ) {  // error occured during initial application configuration
        usage_info( argv[0], std::cerr, appCfg );
        return EXIT_FAILURE;
    }
    if( -1 == rc ) {  // one of the "single-action" options is given (-l, -h)
        return EXIT_SUCCESS;
    }
    //                                                                    _____
    // _________________________________________________________________/ Logs
    // Initialize logging subsystem
    na64dp::util::inject_extras_to_log4cpp();
    if( ! appCfg.logCfg.empty() ) {
        try {
            na64dp::util::YAMLLog4cppConfigurator::configure(appCfg.logCfg);
        } catch(std::exception & e) {
            std::cerr << "\033[1;31mError\033[0m: while configuring logging system on file \""
                      << appCfg.logCfg << "\": " << e.what() << std::endl;
            throw;
        }
    } else {
        log4cpp::Appender * consoleAppender
                = new log4cpp::OstreamAppender("console", &std::cout);
        consoleAppender->setLayout(new log4cpp::BasicLayout());
        log4cpp::Category & L = log4cpp::Category::getRoot();
        L.addAppender( consoleAppender );
        L.info( "Using zero-conf logging." );
    }
    log4cpp::Category & L = log4cpp::Category::getRoot();
    L.info("Logging initialized.");
    if(rc > 0) {
        L.error( "(There were errors during shared object loading before"
                " logging was initialized)" );
    }

    //                                                            _____________
    // _________________________________________________________/ Calibrations
    // Manager keeps type aliases that maps human-readable type name (C-string)
    // to pair of (RTTI type hash + arbitrary string). Aliases needed before
    // the calibration config file being loaded and usually brought up by
    // extension modules.
    na64dp::calib::Manager calibMgr;
    na64dp::util::setup_default_calibration(calibMgr, appCfg.calibCfgURI);

    if(!appCfg.eid) {
        if(appCfg.dt.first || appCfg.dt.second) {
            L.error("Run number is not set, but date/time is. The latter is"
                    " useless without run number.");
            log4cpp::Category::shutdown();
            return EXIT_FAILURE;
        }
        // ...
    } else {
        // ... TODO
        throw std::runtime_error("TODO: dump calibs for specific run/datetime");
    }

    //                                                                _________
    // _____________________________________________________________/ Clean up
    // That's all, folks
    L.info("All done -- exiting normally.");
    log4cpp::Category::shutdown();

    return EXIT_SUCCESS;
}


