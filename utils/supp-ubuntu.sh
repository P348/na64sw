# This supplements are addendum to environment specification for development
# builds in Ubuntu >20.04TLS with ROOT isntalled via snap.
# It is used only in dev local build environment.
# - preload libs ROOT forgets to load
export LD_PRELOAD=/snap/root-framework/current/usr/local/lib/libEve.so:/snap/root-framework/current/usr/local/lib/libRGL.so
# - define prefix to be CWD (for running from *.build):
export NA64SW_PREFIX=$(readlink -f .)
# - shut up CLING/ROOT's missing dictionaries error
export ROOT_INCLUDE_PATH=$(readlink -f ../../include/genfit)
# If you're using ROOT from snap, following shell snippet is useful to run from
# build dir to force it use proper mountpoint instead of numeric one that is
# changing from time to time:
# $ find . -type f -print0 | xargs -0 sed -i 's/root-framework\/632/root-framework\/current/g'

