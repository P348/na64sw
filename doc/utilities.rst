.. _utils lib:

Utilities Library
=================

These pages refer to various utility functions used across theframework. These
functions and types are the subject of a dedicated library and altogether,
besides of the third-party dependencies, represent a lowest level of the
framework's API.

.. doxygengroup:: numerical-utils
   :members:

.. doxygengroup:: numerical-utils-online-stats
   :members:

