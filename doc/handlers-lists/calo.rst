Calorimeter hit-related standard handlers
=========================================

This page lists handlers related to clusters of hits found on the signal from
(M)SADC-based detectors. Instances of ``SADCHit`` are collected into clusters
named here a ``CaloHit`` for further treatment in reconstruction procedures.

Subject: calorimeter hit struct
-------------------------------

All the handlers listed below operate with the instances of the ``CaloHit``
C/C++ structure that is defined as a subclass of (M)SADC hits collection:

.. doxygenstruct:: na64dp::event::CaloHit
    :members:

Note, that ``CaloHit`` sometimes may have its underlying hits collection empty
for certain data sources (like Monte-Carlo data, if fine granularity is not
taken into account).

Handlers list
-------------

This handlers deal with ``CaloHit``.

.. doxygengroup:: calo-handlers

