.. _apv detectors:

APV cluster-related standard handlers
=====================================

This page lists handlers related to clusters of hits found on the signal from
APV-based detectors (GEMs and Micromegas).

.. _apv cluster:

Subject: APV Cluster struct
---------------------------

All the handlers listed below operate with the instances of the ``APVCluster``
C/C++ structure that is defined as a subclass of APV hits collection:

.. doxygenstruct:: na64dp::event::APVCluster
    :members:

Note, that ``APVCluster`` may have its underlying hits collection empty for
certain data sources (like Monte-Carlo data).

Handlers list
-------------

This handlers deal with ``APVCluster``.

.. doxygengroup:: apv-cluster-handlers


