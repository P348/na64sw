APV hit-related standard handlers
=================================

This page lists handlers related to detectors based on APV chip (Micromegas
and GEMs).

Subjects: hit and APV raw data structs
--------------------------------------

All the handlers listed below operate with the instances of the ``APVHit``
C/C++ structure (and, optionally with ``RawDataAPV`` instance accessed by
``rawData`` attribute):

.. doxygenstruct:: na64dp::event::APVHit
    :members:

.. doxygenstruct:: na64dp::event::RawDataAPV
    :members:

Note, that ``APVHit`` may have its ``rawData`` reference unset. For instance,
when events are read from Monte-Carlo data source.

Handlers list
-------------

This handlers deal with APV hits at the very basic level of DAQ digits.

.. doxygengroup:: apv-handlers
   :members:


