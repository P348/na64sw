Generic Handlers
================

This page lists a *generic* handlers. These handlers must be specialized by
certain type and/or a value to operate with.

Access to the subject data is then performed via a special access function
referred further as *getter*.

*Getters* are listed in the corresponding section of the hit structure
descriptions.

.. doxygengroup:: generic-handlers
