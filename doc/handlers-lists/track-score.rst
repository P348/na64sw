Track Score standard handlers
=============================

This page lists handlers related to clusters of hits found on the signal from
various types of detectors. Instances of :cpp:class:`na64dp::event::APVCluster`,
:cpp:class:`na64dp::event::StwTDCHit`, etc. are grouped into an item named
here a :cpp:class:``na64dp::event::TrackScore`` for further treatment in track
reconstruction procedures. A *track* is defined as a composition of
*track scores* -- instances of a special `class of assotiation objects`_
referring to a particular hit or set of hits. One can also think on track score
as on something more commonly named *track point* or *spatial point* (yet, it
is usually not a point geometrically -- it can be line, line segment, a
surface, volume, etc).

.. _class of assotiation objects:  https://www.ibm.com/docs/en/rsm/7.5.0?topic=diagrams-association-classes

.. note::
   Conception of *event object model* (track and track scores are the subject of
   this model) in more abstract terms is described in :ref:`event structure`
   section of key concepts chapter.

This section is focused on score creation topic. Namely, NA64sw delibirately
foresees multiple ways to create a score since there is a plethora of various
detector entities that should contribute into the tracking in some form
(APV clusters, hodoscopic hits, time-dependent ambigious drift detector
measurements, etc even shower centers in calorimeters).

.. important::
    Essentially, a score object itself provides the tracking procedures with a
    common and uniform
    interface to all these data, in terms of spatial primitive and corresponding
    uncertainties.

Current API foresees score created in the following ways:

#. So-called "ad-hoc handlers" fully comprises score creation out of event
   object, with minimal intermediate C++ API.
#. Score created from a single hit (e.g. APV cluster)
#. Score created from combination of hits or other scores (combined or
   geometrically conjugated)

Last two ways are interconnected -- for instance, same procedure of
reconstructing geometrical information from single hit is useful also in the
latter case.

Besides of ad-hoc score-creation procedures that are usually individual for a
certain detector type, there is a *generic way* to combine layered detectors
with minimal effort (however its C++ API is really tedious,
see :ref:`combining hits in tracking`).

.. _track score:

Subject: track score struct
---------------------------

All the handlers listed below operate with the instances of the ``TrackScore``
C/C++ structure that is defined as a subclass hits collection:

.. doxygenstruct:: na64dp::event::TrackScore
    :members:

.. note::
   :cpp:struct:`na64dp::event::TrackScore` sometimes may have its underlying
   hits collections empty for certain data sources (like Monte-Carlo data).

Handlers list
-------------

This handlers deal with ``TrackScore``.

.. doxygengroup:: track-score-handlers
   :members:


.. _combining hits in tracking:

Hits combining
--------------

Basic idea of combining the hits is following:

 * certain *hit combination rule* yields lightweight stateful
   *generators* providing user code with iterable set of
   permitted combinations.
 * A *rule* instance is a stateful object as well; its lifecycle implies
   filling it with a series of hits, (optionally) extraction with *generator*
   and re-setting.

Base class for hit combination rule
is :cpp:struct:`na64dp::util::iHitCombinationRule` whose ``hits_generator()`` method should
create subclasses of :cpp:struct:`na64dp::util::iCombinedHitsGenerator`. It
exposes ``consider_hit()`` method to collect hits and ``reset()`` to drop the
collection.

All the subsequent machinery is based on this simple idea. For instance:

 * :cpp:class:`na64dp::util::iPerStationHitCombinationRule` rule subclass
   implements the rule
   in a most common way: as n-layered station yielding combined scores as
   cartesian product. This is still an interface meaning that what shall be
   included in a *station* should be decided by subclass.
 * :cpp:class:`na64dp::util::DSuLHitCombinationRule` is based on per-station
   rule, but adds support to on/off particular planes implementing per-station
   so that it
   relies on detector ID semantics (see :ref:`detectors naming`)
 * :cpp:class:`na64dp::util::TwoLayerScoreCombinationRule` involves geometrical
   information to produce scores for two-layered detectors

Then, there are some templated handlers based on the rules implemented:

 * :cpp:class:`na64dp::handlers::CreateTrackScores` is most primitive one
   designed for direct association of the hits with simple scores using
   primitive traits.
 * :cpp:class:`na64dp::handlers::ConjugateHits` is more elaborated one
   benefitting from rules.

For pipeline configuration both C++ classes are available under single
configuration: ``_type: CreateScores``, however their sets of parameters differ
(see corresponding references).

.. important::

   Key difference between :cpp:class:`na64dp::handlers::CreateTrackScores` and
   :cpp:class:`na64dp::handlers::ConjugateHits` is that the former one does
   not need the conjugation *conjugation rule* while second one requires it.

Combination/conjugation routines frequently use the traits, so one cas
think on traits as a base implementation for more generic reentrant rule(s):
introducing new hit type for custom detector and custom (local) geometry would
require only basic definitions in traits while combination of them immediately
becomes available by means of rules.

From API point of view both generic implementations exploit rules via partial
template specialization
:cpp:class:`na64dp::handlers::CombinedSubScoreProducer` shim which interfaces
traits struct :cpp:struct:`na64dp::aux::TrackScoreTraits` together with
relevant geometry informaton in order to perform scores creation. Detailed
relation is considered at section .

.. doxygengroup:: track-combining-hits

