.. _all-handlers:

Standard Handlers Library
=========================

These pages refer to all the handlers included in "standard handlers" extension
module in ``na64sw`` framework and loaded by default. Handlers are shown
together with their subject (SADC, APV, APV cluster, track score, etc).

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   sadc
   apv
   f1
   calo
   apv-cluster
   track-score
   tracking
   generic

