Handlers related to tracking
============================

This page lists handlers operating with track reconstructed from series of
track points.

.. warning::
    Most of the handlers seem to lack appropriate documentation here. This part
    of the standard library is under heavy development and many things are not
    yet well settled in terms of C/C++ API, key conceptions, etc.

Subject: a track object
-----------------------

A ``Track`` subclasses a collection of ``TrackPoint`` objects appending it with
some track-related information.

.. doxygenstruct:: na64dp::event::Track

Handlers list
-------------

.. doxygengroup:: tracking-handlers

