MSADC hit-related standard handlers
===================================

This page lists handlers related to detectors based on (M)SADC chip
(calorimeters, beam counters, etc).

Subjects: hit and SADC raw data structs
---------------------------------------

All the handlers listed below operate with the instances of the ``SADCHit``
C/C++ structure (and, optionally with ``RawDataSADC`` instance accessed by
``rawData`` attribute):

.. doxygenstruct:: na64dp::event::SADCHit
    :project: na64sw
    :members:

.. doxygenstruct:: na64dp::event::RawDataSADC
    :members:

Note, that ``SADCHit`` may have its ``rawData`` reference unset. For instance,
when events are read from Monte-Carlo data source.

Handlers list
-------------

This handlers deal with (M)SADC hits at the very basic level of DAQ digits.

.. doxygengroup:: sadc-handlers

