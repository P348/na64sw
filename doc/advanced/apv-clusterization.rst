.. _apv clusterization:

Notes on APV Detectors
======================

.. warning::

    This page is incomplete and contains references to deprecated C++ API.
    Must be revised.

In NA64 an APV chip is used within tracking detectors. To the moment, the two
types of APV-based detectors are used: Micromegas and GEMs.

APV chip itself plays a role of "analog multiplexer" -- it concatenates voltage
pulses coming from metalized read-out planes of the Micromegas and GEMs,
transmitting multiple inputs (64) into a single output.

Note, that technically, this output is then usually digitized by means of SADC
chip, but in DaqDataDecoding this fact is not reflected in any software
abstraction so it is not important from software point of view.

So, following the fundamental difference of typical SADC-only detector and APV
ones, we distinguish their hits as two individual hit classes calling them
a ``SADCHit`` and ``APVHit`` correspondingly. SADC-only detectors are typically
such classes like calorimeters, beam counters, veto detectors and so on.

APV-based detectors (Micromegas and GEMs) are typically used for beam tracking
purposes due to their outstanding spatial resolution.

A particle passing through the detector volume produces ionization of the gas
media filling the working volume that then propagates towards the electrodes.
Electrodes are usually a sensitive elements of the tracker detectors --
a charged avalanche, once hitting the electrode, produces a current pulse
which is then being measured by detector's DAQ.

Data acquizition software represents measurement on APV detector as a series of
individual _digits_. Each _digit_ corresponds to single triggered wire on the
detector plane and bears information of voltage amplitude on the wire.

The main subject of this note is the clusterization technique applied to set
of *digits* in order to group the into a *cluster* that corresponds particle
coordinates.

Read-out Planes Layout
----------------------

In the DAQ software the _digits_ contains a numeric field called
*channel* (returned by ``CS::ChipAPV::GetChannel()`` getter). Basically,
this _channel_ number corresponds to some physical _wire_ which has fixed
spatial position and orientation, and thus being useful to determine hit
coordinates in one projection.

Note: by *wire* we mean any sensitive metallized element of the read-out plane.
Technically, for Micromegas and GEMs they are actually thin metalized stripes.

However, the mapping of the *channel* into *wire* may not be trivial subject.
Sometimes detetector design imply galvanic connection of several wires into
one channel, so hit present at one channel can physically match multiple
physical wires.

Up to the moment all GEM detectors used in NA64 has on-to-one identity match,
while Micromegas APV channels are usually connected to 5 wires.

We implement both mappings in the ``src/NA64DPStrips/Layout.c`` file as hardcoded
mapping function, available as C callbacks to encompassing C++ code.

Note, that originally GEMs mapping is not identical. For reference purposes the
GEMs mapping was implemented as a function in ``src/NA64DPStrips/Layout.c``, but
it is not used. Instead, the identity function is used for GEMs (in current
NA64 DAQ GEMs channel-to-wire mapping is truly identical).

The mapping function callback type is defined as a function that receives
a channel number and pointer as an input argument and returns pointer to
an array with corresponding wire numbers and number of arrays elements as an
output. Since C does not support multiple return values, we had to provide
pointer to variable that has to contain the number of elements in the array as
second argument to a function.

In the ``include/NA64DPStrips/Layout.h`` C-header the following types are defined:
* ``NA64DP_APVStripNo_t`` to be a _wire_ number,
* ``NA64DP_APVWireNo_t`` to be a _channel_ number 
* ``NA64DP_APVNWires_t`` to be a number-of-elements types
* ``NA64DP_DemultiplexingMapping`` is a mapping callback type

Following the C++ type-traits technique, the choice of a particular callback
function is implemented within a ``EvFieldTraits<APVHit>::mapping_for()`` function.

Clustering Algorithms
---------------------

We (a bit misleadingly) call the DDD APV digit a *hit* meaning the single wire
measurement. Having multiple *hits* is not enough to derive a track point
for the track reconstruction -- one have to unify it into a weighted cluster
first to assure a real intersection.

It might be done (especially, concerning Micromegas multiple wire routing) in
few ways.

The most straightforward one is to look up for adjacent *hits* assuming that
charged plasma avalanche will cover multiple wires at once, without gaps.

.. todo::

    Faced a puzzling results with straightforward solution, need to check,
    re-think, communicate with experts

