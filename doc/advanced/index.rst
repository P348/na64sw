Advanced Topics
===============

.. toctree::
   repo-structure
   cpp-api/concepts
   cpp-api/handlers-dev
   cpp-api/detectors-naming
   cpp-api/memory-model
   cpp-api/calib
   apv-clusterization
   build-instructions
   alignment
   batch

