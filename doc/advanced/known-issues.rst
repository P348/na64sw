.. _known issues:

Known Issues
============

This page lists known issues and ways to resolve it (current workarounds or
plans).

1. ASCII event display shows handler in the random order. This is due to a
   missing information in the event display status data. It has to be fixed,
   but currently has low priority. Perhaps at some point we will impose a
   ncurses-based display to provide more elaborated view on the ongoing
   processing.

2. Missed APV pedestals for ``GM03Y`` while running with ``p348reco`` source
   For ``p348reco-wrapper`` source:
   
   .. code-block:: shell
  
      INFO: Read_APV_Pedestals() detname=GM02 fname=conddb/pedestal/2017/GM02Y1__~~start-2017-09-10-16:15:28~~finish-2017-12-31-23:59:59
      WARNING: FindApvPedestalFile() no pedestal file matching time
      INFO: Read_APV_Pedestals() detname=GM03 fname=conddb/pedestal/2017/GM03X1__~~start-2017-09-10-16:15:28~~finish-2017-12-31-23:59:59
      INFO: Read_APV_Pedestals() detname=GM03 fname=
      ERROR: Read_APV_Pedestals(): can't open calibration data file
      terminate called after throwing an instance of 'std::runtime_error'
      what():  ERROR: Read_APV_Pedestals(): can't open calibration data file
  
   Seen for 2017 data (run #3239 for instance). This is due to missed APV
   pedestals file for GEM plane Y. Currently ``p348reco`` does not provide way to
   mask certain plane, so only nasty workaround exists: create an empty file
   named just like one existing for X plane, but with Y instead. This leads to
   wrong time reconstruction for missing plane, but this result may be excluded
   further for within a pipeline.

3. Hardcoded shift for the master time in 2021mu and ``FIXME`` warning during
   compile time. This issue will be presumably cured by integrating some
   JIT-compiled formulae within the framework (TFormula, LLVM, Lua or
   whatever).
