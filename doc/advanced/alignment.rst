.. _alignment:

Notes on alignment in NA64
==========================

This page describes tracking and alignment concepts and utils for NA64. First,
a brief overview of the track reconstruction in NA64SW is given, followed by
instructive description of alignment procedure. Details on the conventions
are given at the end of the page.

NA64SW tracking in general
--------------------------

In NA64SW the setup geometry is described by *placements document* that
is an ASCII file of certain columnar format with metadata (SDC).

.. todo::
    Example of placements document content here.

This document defines positions and spatial orientation of detector
entities -- a kind of information typically used by tracking and Monte-Carlo
code to define setup geometry.

Track reconstruction procedures use *placements* to derive
track's *scores* [1]_, based on information of raw hits
(e.g. *placements document* + *raw hits* = *scores*).
Scores bring information only relevant to tracking, like its type (1D, 2D, 3D),
time, position and expected error estimation.

.. [1] More common term would be a *track point* or *measurement*, but we
   prefer to call it a *score* to avoid confusion with 2D/3D spatial points
   and raw hits.

Track fitting procedure operates with a set of scores to reconstruct the track
by fitting parameters of track's physical model to the measured values provided
by scores. Resulting fit is always a tradeoff because of internal detector
imperfections, stochastic nature of the measurement and, what is most important
for us, due to mistakes in placements. The latter is the subject of
*alignment procedure* described further which purpose is to mitigate or even
eliminate systematics introduced by survey uncertainties (typically .25-1mm).

Conventions
-----------

Below are some conventions settled for the placements and geometrical
descriptions in the setup.

Coordinate frames
~~~~~~~~~~~~~~~~~

We operate in right-hand coordinate frame with Z axis directed towards the
beam, X directed horizontally and Y vertically. For NA64 at H4/NA58
beamlines X is directed from Saleve to Jura.

.. figure:: https://cernbox.cern.ch/index.php/s/ReSFxImd0qsHvT1/download
    :alt: (drawings here)

    An emblmatic drawing showing global axes orientation, direction of rotation
    angles and "canonical" orientation of detectors by their kin.

In case of local, detector-based coordinate frames (DRS), the order of axes is
``X'Y'Z'``. For each projection od the detector there is also a wire reference
system (WRS) with axes ``UVW`` where ``U`` is *always* matching the first
projection axis in DRS.

Work in WRS is important for hit-to-track score conversion and the alignment
procedure and generally should not bother users much.

DRS is important for detector experts, to build efficiency maps and so on.

System of Units
~~~~~~~~~~~~~~~

NA64sw inherits its system of units from GenFit2 where following units are
taken:

* Length: cm
* Induction: kGauss
* Energy: GeV

Angles
~~~~~~

In *placements document* for every detector entity (like ``GM01X``, ``S1``,
``ECAL0`` etc) up to three angles can be defined as Tait-Bryan angles for
extrinsic CW-rotation in order "ZYX", corresponding to:

#. Around Z -- wire inclination wrt lab's horizon (roll)
#. Around Y -- plane's horizontal tilt defining difference between leftmost and
   rightmost wires
#. Around X -- plane's vertical tilt defining difference between upper and lower
   wire with respect to the beam outlet. This angle is usually named "pitch" in
   Tait-Bryan convention, though we recommend to avoid this name since
   it can be misleadingly interpreted as effective *stride* of the wires, as
   it is seen by the beam).

Extrinsic "ZYX" `rotation is known to be equivalent`_ to "XY'Z'" intrinsic
rotations.

.. _`rotation is known to be equivalent`: https://math.stackexchange.com/a/3314025

So far in the alignment we only deal with the rotation around Z as the impact
of the rest is relatively negligible and hard to correct.

Rationale comes from typical precision relations defined by technical reasons.
First, tracking is mostly sensitive to *roll* (Z) angle and usually seen on the
residuals distribution with the best absolute precision. Then comes Y rotation
as for fixed-target experiment geometry planes are positioned perpendicular to
undeflected beam direction that creates subtle distortions in tracking after
beam is deflected in vertical magnetic field (in horizontal plane). Up/down
differences are less probable and Y-deflection is used more rare, so rotation
around X must be of least priority.

