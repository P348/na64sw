.. _runtime extensions:

Types of Runtime Extensions
===========================

What makes NA64sw to be a software *framework* actually is a variety of
different runtime extensions provided by loadable *modules*. The
*module* is usually provided as just a shared object file (``.so``).

The module usually provides new object types becoming available at certain
places. Most common example of such an extension is the handlers. Other
typical subjects are listed in 


