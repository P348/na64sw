.. _memory model:

Efficient Memory Model
======================

.. todo::
    managing memory within this library is a quite advanced topic.
    I don't think the profound documentation will be needed at these days, so
    I postponed writing of this page. Instead I will collect here some valuable
    reference materials to help me to explain the approach in the future:

* Improving Performance with Custom ObjPool Allocators for STL: https://www.drdobbs.com/cpp/improving-performance-with-custom-pool-a/184406243
  comprehensively explains the rationale
* Towards a Better Allocator Model: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1850.pdf
  proposes a solution within the standard
* Boost's update on C++11/C++14/C++17 Conformance: https://www.boost.org/doc/libs/1_68_0/doc/html/container/cpp_conformance.html#container.cpp_conformance.alloc_traits_move_traits
  demonstrates the concept of stateful allocators.

Albeit the technical details of the implementation seems quite complicated,
it is still possible (hopefully) to embed the pool stateful allocators
seamlessly in the code, by means of modern C++ or whatever...
