Common Handler Parameters
=========================

This page covers parameters that are somewhat common for many handlers
(including generic ones).

.. _commonApplyTo:

``applyTo`` -- select a detector
--------------------------------

Many handlers that deal with certain entities within event's collections offer
an ``applyTo`` parameter that expects a string argument -- a special logical
expression that defines whether handler must be applied.

Considered collections are usually SADC or APV hits, clusters, track points,
etc. What collection is considered by handler is usually clear from handler's
function (for instance, ``SADCSubtractPedestals`` operates with collection of
``SADCHit``).

A syntax of logical expression used is pretty close to C/C++: one may use
``!=``, ``&&``, ``||``, ``!`` for logical expression, round brackets to
prioritize operations and exploit some "variables":

* ``kin`` is set to current lement's detector type (e.g. ``MM``, ``GM``,
  ``ECAL``, ``HCAL``, etc)
* ``number`` is set to station number and may be compared with integer.

There are also variables that will be defined for elements of certain type:

* ``projection`` is defined for APV detectors only, when hits or clusters are
  considered. Can be compared with ``X``, ``Y``, ``U``, ``V``
* ``xIdx``, ``yIdx`` are defined only for SADC hits.

There are also additional variables that rarely needed -- you may list them
with ``-l,--list`` option of ``na64sw-pipe`` in "detector-id-definitions"
paragraph.

With this simplistic expressions one may gain surprisingly flexible
functionality. For instance:

* ``applyTo: kin == GM`` will make handler to be applied only to GEMs
* ``applyTo: kin == HCAL && number == 2`` will apply handler to second HCAL
  module
* ``applyTo: kin == MM && X == projection`` will make handler to treat only X
  plane of all the micromegas
* ``applyTo: GM == kin && (number == 3 || projection != Y)`` will apply handler
  to all non-X GEMplanes except for 3rd station.

You may find a more complete reference on this domain specific language at the
page dedicated to :ref:`detectors naming` (advanced topic).

``path``, ``histName`` and variations
-------------------------------------

For most of the handlers that create objects in ROOT's ``TFile`` a following
set of parameters can be found usually:

* An object *path* that defines where the object is created in the hierarchy
  of ``TDirectory``'ies. It is often defined as a template string -- with
  placeholders in curly brackets, like ``{kin}/{number}/{hstName}``
* The *path* usually refers to a ``TObject``'s name created (so, the last token
  of this path is in fact a ``TObject`` name). For plotting handlers this path
  sometimes contains a ``{hist}`` that refers to auxiliary
  ``histName: ...`` YAML parameter.
* ``desc: ...`` YAML parameter is another string template commonly found
  for ``TObject``-creating handlers. This parameter is used to provide input
  to ``description`` string of various plots where one can specify the title
  and axes labels.

Full list of placeholders available for string interpolation
within ``path``/``desc`` parameters can be found in reference
of :cpp:func:`~na64dp::DetectorNaming::append_subst_dict_for()`.


Examples of common parameters usage
-----------------------------------

Following examples show various usage scenarios of the common parameters
described above in different handlers. Some of these handlers we did not cover
in this tutorial yet, but it shall be possible to get the grasp so far:

* 1D histogram applied to beam counters only (``S`` or ``T``) depicting linear
  SADC waveform sum by path like ``sums/S2/linear-sum``

  .. code-block:: yaml

    - _type: Histogram1D
      applyTo: "kin == S || kin == T"
      value: sadcHits.rawData.sum
      histName: "linear-sum"
      histDescr: "Linear waveform sum, {TBName}; sum, sample no ; samples"
      path: "sums/{kin}{statNum}/{hist}"
      nBins: 100
      range: [0, 25000]

* Sometimes ``applyTo: ...`` is anticipated for various analytic handlers. For
  instance, size requirement for clustering procedure differs for X and Y
  planes of MMs because of different distance from the galvanic mesh within a
  detector station. This way one can specify same handler twice,
  independently for X and Y projections:

  .. code-block:: yaml

      - _type: APVFindClusters
        applyTo: "kin == MM && projection == X"
        minWidth: 3
        minCharge: 100
        sparseness: 0
      - _type: APVFindClusters
        applyTo: "kin == MM && projection == Y"
        minWidth: 5
        minCharge: 100
        sparseness: 0

* Cross-typed generic handlers, like
  :cpp:class:`~na64dp::handlers::BiHitHistogr_Calo_Calo` requires
  hit-selection criterion to pick up certain hit(s) from the collections.
  For that purpose ``applyTo: ...`` can be used as well:

  .. code-block:: yaml

    - _type: BiHitHistogram_Calo_Calo
      histName: ECAL-vs-HCAL
      histDescr: "ECAL edep vs HCAL edep ; ECAL, GeV ; HCAL, GeV"
    
      applyToX: "kin == ECAL"
      valueX: eDep
      nBinsX: 150
      rangeX: [0, 150]
    
      applyToY: "kin == HCAL"
      valueY: eDep
      nBinsY: 150
      rangeY: [0, 150]

  This handler produces famous NA64's ECAL/HCAL biplot.

* Starting from 2018, NA64 often uses fourth module of HCAL as veto hadronic
  calorimeter. The *calorimeter hits* (instances of :cpp:class:`~na64dp::event::CaloHit`) are
  build up from set of :cpp:class:`~na64dp::event::SADCHit` according to certain rules by
  :cpp:class:`~na64dp::handlers::MakeCaloHit` handler. One may restrict HCAL
  hit composition with ``applyTo: ...`` like follows:

  .. code-block:: yaml

    - _type: MakeCaloHit
      applyTo: "kin == HCAL && number < 3"
      detectors: [HCAL]

