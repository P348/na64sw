.. _tutorial:

Tutorial
========

These pages cover basic usage principles of `na64sw` reconstruction framework.

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Contents:

   01-basics
   02-tweaking
   03-generics
   04-sources
   05-common-pars
   06-handler
   07-workflow
   08-alignment

