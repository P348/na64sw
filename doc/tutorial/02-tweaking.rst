.. _tutorial-tweaking:

Tweaking pipeline configuration
===============================

This chapter demonstrates how existing pipeline configuration can be
customized.

We use here a very basic level of DAQ digits got from MSADC detectors to
refrain from complex analysis and demonstrate only the gist.

Changing Parameters
-------------------

Let's pick up some basic *pipeline config* file and tweak its parameters. To
get a feeling we'll start with a very short pipeline that performs subtraction
of pedestals for MSADC detectors.

First, we copy a prepared (very short!) *pipeline config* file to a local dir
for a change.

.. code-block:: shell

    $ cp $NA64SW_PREFIX/etc/na64sw/share/tutorial/02-tweaking.yaml custom.yaml

One can edit this file with their favorite text editor. Its content shall
be:

.. code-block:: yaml

    pipeline:

        - _type: SADCPlotWaveform
          baseName: "amps-raw"
          ampNBins: 500
          ampRange: [0, 2400]

        - _type: SADCGetPedestalsByFront
          nSamples: 4

        - _type: SADCSubtractPedestals

        - _type: SADCPlotWaveform
          baseName: "amps-pedCorrected"
          ampNBins: 200
          ampRange: [0, 4000]

This text file has following features:

* it is written in YAML_ language. The YAML syntax
  is known as being pretty intuitive (comments start with ``#`` symbol, lists
  defined with ``-`` marker, objects are defined as ``key: value`` line, etc).

* it describes one *list of objects*: a sequence of smaller parameter sets,
  each related to logically-isolated procedure (handlers starts with
  ``- _type: ...`` line in this config, blank lines do not matter).

* individual procedure (we further call it a **handler**) is designed to
  perform a very simple and clear task each. The configuration document makes
  ``na64sw-pipe`` to create a set of this **handlers** that are applied to
  every event in a oerdered way, just as listed in this file.

* For instance, one handler finds pedestals in a SADC waveform,
  another, say, performs lookup for maxima. A handler, depending on its C++
  class (referred in config as ``_type: ...``) can modify event information or
  can build a plot.

* The pipeline defined in your config file will mean that once NA64sw
  reads an event, this event will be handled passed by four procedures:

    1. :cpp:class:`~na64dp::handlers::SADCPlotWaveform` -- plots an MSADC waveform
    2. :cpp:class:`~na64dp::handlers::SADCGetPedestalsByFront` -- finds a pedestal in the sample
    3. :cpp:class:`~na64dp::handlers::SADCSubtractPedestals` -- subtracts a pedestal from waveform
    4. :cpp:class:`~na64dp::handlers::SADCPlotWaveform` -- plots an MSADC waveform again

* Note, that same **handler** may be applied multiple times, like
  :cpp:class:`~na64dp::handlers::SADCPlotWaveform` in this example -- first it
  will build SADC waveform plot before pedestals subtraction and second after.
  Technically, it is just two instances of the same class configured
  differently.

* **Handlers** pretty often anticipate some configuration parameters. A common case
  is the plotting handler that must be parameterised with name of the output
  histogram in ROOT ``TFile``, limits and number of bins. You can see such an
  example in a :cpp:class:`~na64dp::handlers::SADCPlotWaveform` handler of
  your ``custom.yaml``.

.. _YAML: https://yaml.org/

So, for instance the :cpp:class:`~na64dp::handlers::SADCPlotWaveform` handler
has at least three parameters that must be provided by its configuration,
besides of its *class*:

* The name of histogram that shall appear in the output ROOT file (called
  ``baseName``),
* The number of vertical bins (``ampNBins``, is always 32 for MSADC detectors)
* the amplitude range to plot a waveform (``ampRange``).

You may change ``ampNBins`` in :cpp:class:`~na64dp::handlers::SADCPlotWaveform`
histogram to see how binning changes for appropriate histogram: change the
number, save the file and re-run the application:

.. code-block:: shell

    $ na64sw-pipe -N 1000 --run custom.yaml \
            /eos/experiment/na64/data/cdr/cdr01002-003292.dat

You can see the difference now in ``amps-raw`` or ``amps-pedCorrected`` histograms
in ROOT's ``TBrowser``.

.. important::
  To get usage information of certain handler, one may visit our handlers
  reference pages. The links like :cpp:class:`~na64dp::handlers::SADCPlotWaveform`
  in this tutorial lead to this helpful pages.

Adding a handler to the pipeline
--------------------------------

There are a number of handler classes provided by NA64sw by default. One may
get the list of known handlers by calling ``na64sw-pipe`` application with ``-l``
option. It will make the application to dump list of known handlers (as well as
other runtime extensions) into console.

.. code-block:: shell

    $ na64sw-pipe -l

For instance, if you are intersted with, say, seeing the sum of SADC
waveform, you may find appropriate handler with following command:

.. code-block:: shell

    $ na64sw-pipe -l | grep sum

It will provide you with at least two results:
:cpp:class:`~na64dp::handlers::SADCDirectSum` and
:cpp:class:`~na64dp::handlers::SADCLinearSum`. They are almost
identical, the only difference is that "linear" sums up the mean values between
two adjacent samples while "direct" just sums up all the sample values.

One can add this handlers in the pipeline: just *append* ``pipeline:`` block
with a line ``- _type: SADCDirectSum`` in ``custom.yaml`` file.

If one run the pipeline then, this handler *will* modify data in the event
object (it will append every MSADC hit information with sum value), however no
changes will be observable yet in ``processed.root`` output since none of the
two histograms are affected by sum value appeared.

This is an important part of modular design. Each handler does a very simple,
logically separable job, and tasks like calculating a sum and plotting a sum
must be splitted across two individual handlers.

So, how to visualize sum distribution and what actually was affected by our
change? One can think on a special handler that builds a plot like
:cpp:class:`~na64dp::handlers::SADCPlotWaveform`, but for sum value of
MSADC hit. And there is one! Add the following block to the end of handlers
list:

.. code-block:: yaml

    - _type: Histogram1D
      value: sadcHits.rawData.sum
      histName: "sum"
      histDescr: "Amplitude sum, {TBName} ; time, ns ; Events"
      nBins: 100
      range: [0, 5000]

.. important::

  Beware of breaking up the indent. YAML is sensitive to that. Each handler
  *parameters block* shall start from ``- _type: ...`` line of the same indent
  and shall have its parameters listed on bigger indent.

You can observe the new histogram appeared in the ``processed.root`` output
file.

.. figure:: https://cernbox.cern.ch/index.php/s/jSbDt765MYuk20g/download

In next section we will explain a bit technical aspect directly related to this
plotting handler, that may appear a bit tedious for understanding. However,
using these *generic plotting handlers* (together with shallow understanding of
event-building) will save enormous amount of time for the analysis.

