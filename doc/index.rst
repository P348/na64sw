NA64 Data Processing Software Framework
=======================================

This pages contain a reference to the software framework developed for data
processing at the NA64 collaboration (CERN, SPS).

* Start with :ref:`tutorial` if you would like to get feeling on this software
  (access to LXPLUS is needed).
* Refer to :ref:`all-handlers` to find reference on a particular handler or data
  structure for the analysis

.. toctree::
   :maxdepth: 3
   :caption: Tutorial

   tutorial/tutorial

.. toctree::
   :maxdepth: 1
   :caption: Reference

   handlers-lists/all
   advanced/troubleshooting
   advanced/known-issues
   utilities

.. toctree::
   :maxdepth: 2
   :caption: Other

   advanced/index
   advanced/example-stwtdc

