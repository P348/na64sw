Notes on Donskov reconstruction
-------------------------------
line #1661 -- find time function
lines #2423, #2425 -- ftime()/htime() -- finding time
line #2451 -- applying calibration to MSADC det based on found time
line #7539 -- loading calibrations for ECAL, factor, timing, etc

Format of "ref file" (``.las``):

    first line:
        SpillSelLimit, E9part, minAmp, maxAmp, maxEhcal3, VHcalCut, VetoCut,
        0              1       2       3       4          5         6
        #ifdef VISIBLE
    		minEecal, maxEecal, minEwcal, maxEwcal, statLimit
            7         8         9         10        11
        #else
    		minEecal, maxEecal, minEhcal, maxEhcal, statLimit
            7         8         9         10        11
        #endif
    types of first line: {0,2,3,11}:int, rest:float
    
    second line:
        dump_flag = flag;         0
        tdc_flag = flag;          1
        save_flag = flag;         2
        sample_flag = flag;       3
        sample_select = flag;     4
        write_cuts = flag;        5
        analysis_flag = flag;     6
        event_flag = flag;        7
        cut_flag = flag;          8
        cal_yes = trigger_mask&2; phys_all = trigger_mask&1; 9
    types of second line: all int, last is in hex form
    
    
    lines (up to `n_cells'):
    
     adcch dev z x y zxy loc Ped0 Ped1 Cal Time TimeHw LedSp LedTimeSp evtstat calstat select
     0     1   2 3 4 5   6   7    8    9   10   11     12    13        14      15      16
     types:
        {0-6}:int
        {7-13}:float
        {14-16}:int


