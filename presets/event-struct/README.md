# Event Definition

These `.yaml` files are essentially important to the entire `na64sw` framework
as they define the main `Event` data structure. They are used at the very
beginning stage of project build to generate crucial declarations and
implementation code for the C/C++ project.


This files will be parsed in order. We distinguish between `Event` (main
structure) and *fields* (any other declaration).

...

Top-level field node syntax:
- `~meta` may keep the
    * `defaultBankSize` number of pre-allocated entries at the startup
    * `typeDesc` a Doxygen string to document the type
- any other field, not starting with `~` will be interpreted as attribute node.

Attribute node syntax:
- `type` refers to a C/C++ type this attribute be declared of
- `desc` a Doxygen string to document the attribute
- `deft` default value set on `reset()` call. If not provided, for any `int`
type (`int`, `short`, `uint16_t`, etc.) it will be 0. For any floating-point
value it will be `nan`.

...

Available fields:
    setOf: CaloHit       # Subject of collection
    idxBy: DetID_t       # Index of collection -- a C type or "integer"
    traits: [ambigious]  # see below
    limit: null          # (default is null)

Disclaimer: the generator does not perform any checks for POD-ness of provided
type.

Collection types (arrays, maps, vectors) have `traits` field that effectively
define which container will be used to maintain the collection of objects.
 - `sparse` allows for non-contiguous keys (e.g. `1`, `2`, `152`, `511`);
 - `ordered` for user side affects only the order of keys appearance during
the iteration over collection.
 - `ambig` allows for non-unique keys to appear.
In case the `setOf` value refers to a type defined in the *documents*, the
derived type will be used to specify certain `HitsMap`
implementation.

There are additional features that may be specified to collection types:
 - `idxBy` refers to key type. Must be `"integer"` (default) or any plain C
type;
 - `staticSize` a N-dimensional index (e.g. `[12]` or `[12][35][12]`).
Currently, makes sense only for non-sparse, ordered and unambiguous collection.
Makes the type to be a plain C array.


Examples:
- plain C type field (`int foo;`):
    foo:
        type: int
        doc: The Foo value
- POD C type field (`struct Vector rapidity;`)
    rapidity:
        type: Vector
        doc: A particle rapidity vector
- array of plain C type (`double x[12];`):
    x:
        type:
            setOf: double
            features: [ordered]
            staticSize: 12
        doc: An array of 12 x values
equivalently
    x: double[12]@An array of 12 x values
- multidimensional array of plain C type (`double matrix[4][4];`)
    matrix:
        type:
            setOf: double
            features: [ordered]
            staticSize: [4, 4]
        doc: A transformation matrix
equivalently
    matrix: double[4,4] @ A transformation matrix

