# Start from somewhat default CERN's GitLab image
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
RUN yum -q -y update 
RUN yum -q -y install sudo
RUN groupadd collector
RUN useradd collector -g collector && echo -e "collector\tALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN mkdir /var/src -p && chown collector:collector /var/src
USER collector:collector
WORKDIR /var/src
# Install additional system packages
#RUN yum -q -y install libglvnd-devel motif-devel mesa-libGLU-devel
# Apparently, we can not use CVMFS in the docker container:
#RUN yum -y install locmap && locmap --enable cvmfs && locmap --configure cvmfs
# TODO: will it work?
#RUN source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96b x86_64-centos7-gcc62-opt
# Install log4cpp
# Install Rave
# Install GenFit2
# ...
