---
# This run-config is meant to be ran in beforehead of some accurate MSADC
# reconstruction procedures applied. It collects pedestal information and
# DFFT-based frequency distributions. No energy or timing calibrations are
# used here (so this config is usually something we run before calibration
# procedure).
#
# This config will produce output .root file which is then used
# by `get_preambule.C` script to generate calibration files.
#
# NOTE: this config uses only phyiscs events, no event type or trigger type
#       selection done here.

pipeline:
  # Obtain trigger TDC distributions (used from 2023)
  # "Wires" (channels) of STT in 2023:
  #   - 33 for "physics"
  #   - 41 for "beam"
  #   - 37 for "random"
  - _type: Histogram1D
    # TODO: must it be ".correctedTime" instead of "stwtdcHits.rawData.timeDecoded"?
    value: stwtdcHits.rawData.time
    applyTo: "kin == STT && wireNo == 33"
    histName: "trigger-tdc-ph"
    histDescr: "Physics trigger TDC time distribution; decoded time, ns; Counts"
    nBins: 510
    range: [-10, 500]
  - _type: Histogram1D
    value: stwtdcHits.rawData.time
    applyTo: "kin == STT && wireNo == 41"
    histName: "trigger-tdc-beam"
    histDescr: "Beam trigger TDC time distribution; decoded time, ns; Counts"
    nBins: 510
    range: [-10, 500]
  - _type: Histogram1D
    value: stwtdcHits.rawData.time
    applyTo: "kin == STT && wireNo == 37"
    histName: "trigger-tdc-rnd"
    histDescr: "Random trigger TDC time distribution; decoded time, ns; Counts"
    nBins: 500
    range: [-10, 500]

  # Plots 2D distribution of the SADC waveforms. Accepts parameters:
  # `string:baseName' -- a base nmae of the histogram
  # `ampNBins' -- number of bins for amplitude channel (other is alwys 32)
  # `ampRange' -- range of amplitude channel (other is always 32)
  - _type: SADCPlotWaveform
    profile: false
    applyTo: "kin == ECAL"
    baseName: "amps-raw"
    ampNBins: 120
    ampRange: [0, 2400]

  # Obtain pedestals
  - _type: SADCGetPedestalsByFront
    applyTo: "kin == ECAL"
    nSamples: 3  # applied spearetely to even and odd samples
  # Subtract pedestals obtained before; not very accurate, but still can
  # provide subsequent handlers with pulses "decent enough" in the sense of
  # mean values
  - _type: SADCSubtractPedestals
    applyTo: "kin == ECAL"
    nullifyNegative: false  # keep negative waveforms
  # Put pedestals in histograms.
  - _type: Histogram1D
    applyTo: "kin == ECAL"
    value: sadcHits.rawData.pedestalOdd
    histName: "pedestals-odd"
    histDescr: "Odd pedestal values distribution for {TBName}"
    nBins: 600
    range: [0, 1000]
  - _type: Histogram1D
    applyTo: "kin == ECAL"
    value: sadcHits.rawData.pedestalEven
    histName: "pedestals-even"
    histDescr: "Even pedestal values distribution for {TBName}"
    nBins: 600
    range: [0, 1000]

  # Apply DFFT (appends hits with FFT decomposition)
  - _type: MSADCFFTRadix2
    applyTo: "kin == ECAL"
    window: rect
    #applyTo: "kin == ECAL"
    #applyTo: "kin == ECAL && xIdx == 2 && yIdx == 3"
  - _type: Subpipe
    _label: fft-ppl
    pipeline:
      # Pass only "physics", omitting calibration events
      # ... TODO: event selection by type
      # Plot (D)FFT spectra in 2D histogram
      #- _type: SADCPlotDFTSpectra
      #  baseName: spectra-dist
      #  #baseFreqs: [15, 16]
      #  ampRange: [-120, 10]
      #  ampNBins: 600
      #  profile: false

      # dBFS plot
      - _type: SADCPlotDFTSpectra
        #applyTo: "kin == ECAL && xIdx == 1 && yIdx == 3"
        applyTo: "kin == ECAL"
        baseName: spectra-dist-dBFS
        ampRange: [-160, 10]
        ampNBins: 300
        dBUnits: true
        profile: false
      # dB plot, relative to some freq domain
      - _type: SADCPlotDFTSpectra
        applyTo: "kin == ECAL"
        baseName: spectra-dist-dB
        baseFreqs: [16, 17]
        ampRange: [-120, 120]
        ampNBins: 240
        dBUnits: true
        profile: false
      # abs. values plot
      - _type: SADCPlotDFTSpectra
        applyTo: "kin == ECAL"
        baseName: spectra-dist-abs-amp
        ampRange: [0., 0.025]
        ampNBins: 1000
        dBUnits: false
        profile: false
      # abs. values, relative to some freq domain
      - _type: SADCPlotDFTSpectra
        applyTo: "kin == ECAL"
        baseName: spectra-dist-rel-amp
        baseFreqs: [16, 17]
        ampRange: [0, 10]
        ampNBins: 500
        dBUnits: false
        profile: false

      # creates objects for calib. info extraction
      #- _type: SADCPlotDFTSpectra
      #  baseName: spectra-profile
      #  #baseFreqs: [16, 17]
      #  baseFreqs: [0, 0]
      #  dBUnits: true
      #  profile: true
...
