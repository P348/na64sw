---
pipeline:

  # Infer trigger bits and correct time for TDC trigger measurements
  - _type: InferTriggerBitsTDC
    #nBit: 9     # phys bit, #
    bits: 0x100  # phys bit
    applyTo: "kin == STT && wireNo == 33"
    #offset: 49.7
    offset: 49.2
  - _type: InferTriggerBitsTDC
    #nBit: 10    # beam bit, #
    bits: 0x200  # beam bit
    applyTo: "kin == STT && wireNo == 41"
    #offset: 50.6
    offset: 48.00
  - _type: InferTriggerBitsTDC
    #nBit: 11    # random trig bit, #
    bits: 0x400  # random trig bit
    applyTo: "kin == STT && wireNo == 37"
    #offset: 50.26
    offset: 48.75
  - _type: PlotDistinctValuesCounts
    histName: triggers
    histDescr: "Trigger bit combinations"
    value: trigger
  # This is ad-hoc handler available in `exp-handlers' module; sets master
  # time based on some hardcoded assumptions, valid only for 2023e
  - _type: ExpSetMasterTimeByTrigger2023e

  - _type: Histogram1D
    value: masterTime
    histName: "mt"
    histDescr: "Master time; ns ; N"
    nBins: 100
    range: [-50, 50]


  # --- Common: find and subtract pedestals ---------------------------------
  # Performs dynamic extraction of SADC pedestal values assuming that first
  # N values of the waveform are close to zero. Number of samples and
  # permitted pedestal spanning range are set to what is used by p348reco.
  #- _type: SADCGetPedestalsByFront
  #  #applyTo: "chip != SADC && kin != ECAL"
  #  nSamples: 4
  #  useMinIfDeltaExceeds: 20
  #^^^ alternatively:
  - _type: SADCAssignPedestals  # Loads pedestals
    _label: pedestalsSVD.reading
  - _type: MSADCWfPedestalsSVD  # should use Donskov's approach for pedestals finding
    _label: pedestalsSVD.calculus
  #- _type: SADCSubtractPedestals  # not used if SVD
  #  applyTo: "kin != VHCAL"
  #  applyTo: "chip == SADC && kin == ECAL"
  # Calc time
  # [experimental] S.V.Donskov's waveform reconstruction,
  - _type: MSADCWfRecoSVD
    ledTrigger: false

  - _type: ShowMSADCHits
    applyTo: "kin == ECAL && zIdx == 1 && xIdx != 0 && xIdx != 4 && yIdx != 0 && yIdx != 5"
    destination: default

  #- _type: SADCCompare
  #  _label: pedestalsCmp
  #  dump: ../donskov-apps/rd-dump.txt

  # drop off-time hits
  #- _type: SADCCutOutOfTimeHits
  #  nSigma: 3
  - _type: Histogram1D
    value: sadcHits.time
    histName: "time"
    histDescr: "Peak time"
    nBins: 400
    range: [0, 400]  # from 0 to (12.5ns*32)

  # --- SADC energy calibration ---------------------------------------------
  - _type: ApplySADCCalibsType1
    _label: sadcCalib
    requireAllDefined: false
    applyLEDType1To: all
  - _type: DumpSADCHits  # XXX
  - _type: SADCCutOutOfTimeHits
    nSigma: 4

  # --- Assemble "calorimeter hits" -----------------------------------------
  - _type: MakeCaloHit
    detectors: [ECAL]
    #detectors: [ECAL, SRD, BGO, VETO0, VETO1, VETO2]  # TODO: other calos...
  - _type: MakeCaloHit
    applyTo: "kin == HCAL && number < 3"
    detectors: [HCAL] 

  - _type: Subpipe
    pipeline: std/qa-plots-sadc-raw
  - _type: Subpipe
    pipeline: std/qa-plots-common
...
