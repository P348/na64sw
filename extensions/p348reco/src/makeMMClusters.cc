#include "makeMMClusters.hh"

#if (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

#include "na64calib/loaders/yaml-document.hh"
#include "na64common/calib/sdc-integration.hh"

namespace na64dp {
namespace p348reco_handlers {

std::unordered_map<DetID, MicromegaPlane> &
PlanesCache::planes() {
    if(!_planesCachesValid) {
        _recreate_planes();
    }
    assert(_planesCachesValid);
    return _planes;
}

void
PlanesCache::_recreate_planes() {
    _L << log4cpp::Priority::DEBUG << "(Re-)creating "
        << plane_calibs().size() <<
        " reentrant p348reco's `MicromegaPlane' instances for clustering";
    _planes.clear();
    for(auto & [planeID, planeCalib] : plane_calibs()) {
        // assure calibration is fully initialized, otherwise keep going
        // without this plane
        bool skip = false;
        if(!planeCalib.hasTime()) {
            _L << log4cpp::Priority::WARN
               << "No timing calibrations for " << naming()[planeID];
            skip = true;
        }
        if(planeCalib.sigma.empty()) {
            _L << log4cpp::Priority::WARN
               << "No channel pedestals calibrations for " << naming()[planeID];
            skip = true;
        }
        if(planeCalib.MM_map.empty()) {
            _L << log4cpp::Priority::WARN
               << "No layout scheme assigned for " << naming()[planeID];
            skip = true;
        }
        if(skip) {
            _L << log4cpp::Priority::WARN
               << "Micromega plane " << naming()[planeID] << " will be ignored."
               ;
            continue;
        }
        assert(!planeCalib.MM_reverse_map.empty());  // reverse map built
        // create plane, bind calibs and add to reentrant map
        MicromegaPlane plane;
        plane.Init(planeCalib);
        //plane.calib = &planeCalib;
        _planes.emplace(planeID, plane);
        _L << log4cpp::Priority::DEBUG
           << "Reentrant `MicromegaPlane' instance " << naming()[planeID]
           << " created.";
    }
    _planesCachesValid = true;
}

// Clustering and copying

bool
MakeMMCluster::process_hit(EventID, DetID wireDID, event::APVHit & apvHit) {
    if(!apvHit.rawData) return true;
    PlaneKey planeID(wireDID);

    // find plane struct to append data
    auto planeIt = planes().find(planeID);
    if(planes().end() == planeIt) {
        auto ir = _missingCalibs.emplace(planeID, 1);
        if(ir.second) {
            _log << log4cpp::Priority::WARN
                << "Missing calibration for Micromega plane "
                << MMCalibAdapter::naming()[planeID] << ". Other occurencies will be ignored.";
        } else {
            ++(ir.first->second);
        }
        return true;  // keep going
    }

    // append data
    if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
        _log << log4cpp::Priority::DEBUG << "adding APV hit "
             << MMCalibAdapter::naming()[wireDID]
             << " at #" << (int) apvHit.rawData->wireNo << " : "
             << apvHit.rawData->samples[0] << ", "
             << apvHit.rawData->samples[1] << ", "
             << apvHit.rawData->samples[2];
    }
    DoHitAccumulation( apvHit.rawData->wireNo //wireDID.payload_as<WireID>()->wire_no() // const uint16_t chan
                     , apvHit.rawData->samples  // const uint32_t *raw_q
                     , planeIt->second  // MicromegaPlane &plane
                     );
    return true;
}

AbstractHandler::ProcRes
MakeMMCluster::process_event(event::Event & event) {
    // MicromegaPlane::Init(calib) should reset the reentrant structures for
    // cluster collection
    for(auto & [planeID, p348recoPlane] : planes()) {
        auto calibIt = plane_calibs().find(planeID);
        assert(plane_calibs().end() != calibIt);  // plane calib / plane item syncronized
        p348recoPlane.strips.clear(); // IMPORTANT
        p348recoPlane.Init(calibIt->second);
    }

    //dump_current_calibs(std::cout);  // XXX

    // forward exec to parent method for selective reading
    ProcRes pr = AbstractHitHandler<event::APVHit>::process_event(event);
    if(pr != kOk) return pr;  // abrupt on failure

    // Now all the APV hit (strip) data must be copied to p348reco structs.
    // Perform clustering and retrieve clusters
    for(auto & [planeID, p348recoPlane] : planes()) {
        if(p348recoPlane.strips.empty()) continue;  // omit empty planes
        // calibration shall exist for plane as previous calls won't add the
        // plane object w/out the calibs
        assert(p348recoPlane.hasCalibration());  // guaranteed by `handle_update()`
        // do the clustering
        p348recoPlane.DoMMClustering();
        if(!p348recoPlane.hasHit()) {
            // TODO: reset plane?
            continue;
        }
        // retrieve clusters and copy to event struct
        for(const auto & p348recoCluster : p348recoPlane.clusters) {
            // alloc cluster in event's local memory
            auto clusRef = lmem().create<event::APVCluster>(lmem());
            util::reset(*clusRef);

            // fill the cluster's values
            clusRef->charge = p348recoCluster.charge_total;
            clusRef->position = p348recoCluster.position();
            //clusRef->positionError = p348recoCluster. ...;  // TODO
            clusRef->time = p348recoCluster.cluster_time();
            clusRef->timeError = p348recoCluster.cluster_time_error();

            // check values: must be positive real numbers
            #define _M_check_number(nm)                                     \
            if( std::isnan(nm) || nm < 0 || !std::isfinite(nm) ) {          \
                NA64DP_RUNTIME_ERROR("Bad value on p348reco cluster " # nm "=%e", nm );    \
            }
            _M_check_number(clusRef->charge);
            _M_check_number(clusRef->position);
            //_M_check_number(clusRef->time);  // TODO
            //_M_check_number(clusRef->timeError);
            #undef _M_check_number

            if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
                _log << log4cpp::Priority::DEBUG << "retrieving APV cluster "
                          << " <<< size=" << p348recoCluster.size()
                          << ", charge=" << p348recoCluster.charge_total
                          << ", time=" << p348recoCluster.cluster_time()
                          << ", position=" << p348recoCluster.position();
            }

            if(_doBackrefHits) {
                // link cluster with APV hits (bind references to hits stored in
                // an event with cluster instance).
                for(auto & p348recoStrip : p348recoCluster.strips) {
                    // retrieve wire ID based on "bin"
                    auto revIt = p348recoPlane.calib->MM_reverse_map.find(p348recoStrip.bin);
                    assert(p348recoPlane.calib->MM_reverse_map.end() != revIt);
                    // reconstruct full hit ID by appending plane ID with strip
                    // number (wire ID)
                    DetID wireDID(planeID);
                    wireDID.payload_as<WireID>()->wire_no(revIt->second);
                    // find hit in the event using full hit ID
                    auto hitIt = event.apvHits.find(wireDID);
                    if(hitIt == event.apvHits.end()) {
                        // this should be considered as error: strip number
                        std::ostringstream oss;
                        bool isFirst = true;
                        for(auto & apvHit: event.apvHits) {
                            if(PlaneKey(apvHit.first) != planeID) continue;
                            if(isFirst) isFirst = false; else oss << ", ";
                            //oss << apvHit.first.payload_as<WireID>().wire_no();
                            oss << PlanesCache::naming()[apvHit.first];
                        }
                        _log << log4cpp::Priority::WARN
                             << "Could not find hit on wire #"
                             << revIt->second
                             << " for plane " << MMCalibAdapter::naming()[planeID]
                             << ", available in an event (for this plane): "
                             << oss.str() << ".";
                        continue;
                    }
                    clusRef->hits.emplace( wireDID.payload_as<WireID>()->wire_no()
                                , hitIt->second
                                );
                }
            }
            event.apvClusters.emplace(planeID, clusRef);
        }
        // p348recoPlane.Init(...);  // TODO: reset!?
    }
    return kOk;
}

void
MakeMMCluster::finalize() {
    if(!_missingCalibs.empty()) {
        std::ostringstream oss;
        bool isFirst = true;
        for(auto p : _missingCalibs) {
            if(isFirst) isFirst = false; else oss << ", ";
            oss << PlanesCache::naming()[p.first] << " (" << p.second
                << " entries)";
        }
        _log << log4cpp::Priority::WARN
             << "Following (Micromega) detectors were ignored because of"
             " missing calibrations: " << oss.str() << ".";
    }
}

}  // namespace ::na64dp::p348reco_handlers
}  // namespace na64dp

REGISTER_HANDLER( MakeMMCluster, cdsp, cfg
                , "Native p348reco micromega clustering (creates scores)" ) {
    cdsp.get_loader<na64dp::calib::YAMLDocumentLoader>("yaml-doc")
            .define_conversion(
                      "MuMegaLayout"  //< this must coincide with 2nd arg of REGISTER_CALIB_DATA_TYPE()
                    , ::na64dp::calib::convert_YAML_description_to_MuMegaLayout
                    );
    na64dp::calib::CIDataAliases::self().add_dependency("MuMegaLayout", "naming");

    #if 1  // NOTE: tobe moved to app.cc due to aliases clash
    // Register calibration data type, instantiate SDC conversion (relies
    // on traits defined above), establish deps
    cdsp.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVChannelEntry>();
    cdsp.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVTimingCalib>();
    na64dp::calib::CIDataAliases::self()
            .add_dependency("GEMMonitor/APVPedestals", "MuMegaLayout");
    na64dp::calib::CIDataAliases::self()
            .add_dependency("GEMMonitor/APVTiming",    "MuMegaLayout");
    #endif

    return new na64dp::p348reco_handlers::MakeMMCluster(cdsp
            , cfg["mmNameSuffix"] ? cfg["mmNameSuffix"].as<std::string>() : "^MM[0-9]{1,2}$"
            , na64dp::aux::retrieve_det_selection(cfg)
            , na64dp::aux::get_logging_cat(cfg)
            , cfg["backreferenceHits"] ? cfg["backreferenceHits"].as<bool>() : true
            , na64dp::aux::get_naming_class(cfg)
            , cfg["timingCalibClass"] ? cfg["timingCalibClass"].as<std::string>() : "default"
            , cfg["pedestalsCalibClass"] ? cfg["pedestalsCalibClass"].as<std::string>() : "default"
            , cfg["wireLayoutCalibClass"] ? cfg["wireLayoutCalibClass"].as<std::string>() : "default"
            );
}

#endif  // (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

