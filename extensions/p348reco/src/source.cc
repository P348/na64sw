#include "na64calib/dispatcher.hh"
#include "na64detID/TBName.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64event/data/event.hh"
#include "na64calib/evType.hh"
#include "na64util/array-unwinding.hh"
#include "na64detID/cellID.hh"
// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
// P348 reco
#include "na64util/str-fmt.hh"
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "badburst.h"
#include "timecut.h"
#include <limits>

namespace na64dp {
namespace p348reco {

/**\brief Wraps `p348reco` routines in the `na64sw` data source
 *
 * Converts event data from `RecoEvent` struct object (p348reco) structure
 * into `na64sw` event instance.
 * */
class SourceWrapper : public AbstractNameCachedEventSource
                    , public calib::Handle<EventBitTags> {
protected:
    /// Handle to DDD events manager
    CS::DaqEventsManager _dddMgr;
    /// Forwards execution to by-kin copying methods: SADC, APV, etc
    ///
    /// Assembles event ID
    void _copy_event_data( event::Event & dest
                         , const RecoEvent & re
                         , event::LocalMemory & lmem );
    /// By-kin copying method: SADC detectors, except for hodoscopes
    void _copy_MSADC_data( event::Event & dest
                         , const RecoEvent & re
                         , event::LocalMemory & lmem );
    /// By-kin copying method: Micromegas detectors
    void _copy_MM_data( event::Event & dest
                      , const RecoEvent & re
                      , event::LocalMemory & lmem );
    /// By-kin copying method: GEMs detectors
    void _copy_GM_data( event::Event & dest
                      , const RecoEvent & re
                      , event::LocalMemory & lmem );
    /// Time cut variable used by `p348reco`
    float _timeCut;
    /// Whether or not to rely on "bad spill" list to pass events from certain
    /// spills
    bool _useBadSpill;

    /// Semantic bits caches for event type
    decltype(event::Event::evType) _serviceTypeCode
                                 , _physicsTypeCode
                                 , _inSpillTypeCode
                                 ;
    /// Semantic bits caches for trigger type
    decltype(event::Event::evType) _physTrigger
                                 , _randomTrigger
                                 , _beamTrigger;

    /// Updates _serviceTypeCode and _physicsTypeCode;
    void handle_update(const EventBitTags &) override;
public:
    SourceWrapper( calib::Manager & mgr
                 , const std::string & mapsDir
                 , const std::vector<std::string> inputs
                 , log4cpp::Category & logCat
                 , float timeCut=4.
                 , bool omitBadSpills=true
                 , std::unordered_set<std::string> omitDetectors={}
                 , const std::string & detNamingClass="default"
                 , iEvProcInfo * epi=nullptr
                 );
    /// Performs initial cuts, somewhat default for p348reco
    bool read(event::Event &, event::LocalMemory & lmem) override;
};  // class SourceWrapper


/**\brief copies `p348reco::Cell` into SADC hit and insert it into
 *        an event.
 *
 * Created hits will be attributed to detectors of kin defined in name. The
 * station number, if not set ...
 */
class SADCTranslate {
private:
    /// Dest event to copy to.
    event::Event & _dest;
    /// Memory object to allocate new hits
    event::LocalMemory & _lmem;
    /// Base detector ID cache
    DetID _baseID;
    /// Index anges to restrict copying with
    size_t _idxRng[16][2];
    /// If set, first index will be identified as X instead of station number
    /// (mutually exclusive option with `_appendID`)
    bool _firstIsX;

    DetID _did;

    /// Detector ID conversion function; when defined, allows non-trivial
    /// conversions of the indexes to detector ID
    std::function<DetID(DetID, size_t *, int)> _appendID;
public:
    SADCTranslate( DetID baseDetID
                 , event::Event & dest
                 , event::LocalMemory & lmem
                 , bool firstIsX=false
                 ) : _dest(dest)
                   , _lmem(lmem)
                   , _baseID(baseDetID)
                   , _firstIsX(firstIsX)
                   {
        for(size_t nd = 0; nd < 16; ++nd) {
            _idxRng[nd][0] = std::numeric_limits<size_t>::min();
            _idxRng[nd][1] = std::numeric_limits<size_t>::max();
        }
    }

    SADCTranslate( event::Event & dest
                 , event::LocalMemory & lmem
                 , DetID baseDetID
                 , decltype(_appendID) appendID
                 ) : _dest(dest)
                   , _lmem(lmem)
                   , _baseID(baseDetID)
                   , _appendID(appendID)
                   {
        for(size_t nd = 0; nd < 16; ++nd) {
            _idxRng[nd][0] = std::numeric_limits<size_t>::min();
            _idxRng[nd][1] = std::numeric_limits<size_t>::max();
        }
    }

    static void copy_data( event::SADCHit & hit, const Cell & src, LocalMemory & lmem ) {
        // Copy data from p348reco::Cell to SADC hit
        hit.eDep = src.energy;
        hit.time = src.t0ns();  // t0?
        // Copy some raw data that p348reco::Cell brings
        // - pedestals
        hit.rawData->pedestals[0] = src.pedestal0;
        hit.rawData->pedestals[1] = src.pedestal1;
        // - raw waveform (corrected by pedestals)
        assert(src.raw.size() == 32);
        for(int i = 0; i < 32; ++i) {
            hit.rawData->wave[i] = src.wave(i);
        }
        // - peaks found
        {
            int n;
            for(n = 0; n < (int) src.peaks.size(); ++n) {
                //hit.rawData->maxima[n] = src.peaks[n];
                auto peakPtr = lmem.create<event::MSADCPeak>(lmem);
                util::reset(*peakPtr);
                hit.rawData->maxima.emplace(src.peaks[n], peakPtr);
                //hit.rawData.maximaTimes[] = ...  // NOT provided
            }
            //hit.rawData->maxima[n] = std::numeric_limits<uint8_t>::max();
        }
        // ? src.amplitude:double
        // ? src.amplitude_sample:int
        // ? src.amplitude_peak_index:int
        // ...
    }

    /// (Copying) operator called by unwinding
    void operator()( const Cell & src
                   , size_t * idxs
                   , int ND
                   ) {
        // check index in range
        assert( ND < 16 );
        for(int nd = 0; nd <ND; ++nd) {
            if( idxs[nd] < _idxRng[nd][0] ) return;  // idx below rng
            if( idxs[nd] > _idxRng[nd][1] ) return;  // idx above rng
        }
        // Weird thing: some hits have empty waveform (TODO)
        if(src.raw.empty()) return;
        #if 0
        for( uint16_t i = 0; i < ND; ++i ) {
            std::cout << " " << idxs[i];
        }
        std::cout << std::endl;
        #endif
        // Compose detector ID
        DetID did = _baseID;
        if( _appendID ) {
            did = _appendID(did, idxs, ND);
        } else {
            int idxOffs = 0;
            if(!did.is_number_set()) {
                //if( ND < 1 ) NA64DP_RUNTIME_ERROR("Station number is not set");  // TODO: dedicated err
                if(!_firstIsX) {
                    did.number(idxs[0]);
                    ++idxOffs;
                }
            }
            if( idxOffs < ND ) {
                // Compose ID payload
                CellID cid( idxs[idxOffs], 0, 0 );
                if( idxOffs < ND ) {
                    cid.set_y(idxs[idxOffs++]);
                } else {
                    cid.set_y(idxs[0]);
                }
                if( idxOffs < ND ) {
                    cid.set_z(idxs[idxOffs++]);
                }
                assert( ND - idxOffs < 3 );  // 3 dims max currently
                did.payload(cid.cellID);
            } else {
                // this is to mimic DDD data source, where x/y indexes are
                // always set. Unclear, whether it is good strategy, though.
                CellID cid;
                cid.set_x(0);
                cid.set_y(0);
                did.payload(cid.cellID);
            }
        }
        // Create new SADC hit and add it to event
        mem::Ref<event::SADCHit> hit = _lmem.create<event::SADCHit>(_lmem);
        _dest.sadcHits[did] = hit;  // todo check result?
        util::reset(*hit);
        // create "raw data" section of the hit
        hit->rawData = _lmem.create<event::RawDataSADC>(_lmem);
        util::reset(*(hit->rawData));
        copy_data(*hit, src, _lmem);
    }
};

//
// IMPLEMENTATION

SourceWrapper::SourceWrapper( calib::Manager & mgr
                            , const std::string & mapsDir
                            , const std::vector<std::string> inputs
                            , log4cpp::Category & logCat
                            , float timeCut
                            , bool omitBadSpills
                            , std::unordered_set<std::string> omitDetectors  // TODO : not used!
                            , const std::string & detNamingClass
                            , iEvProcInfo * epi
                            ) : AbstractNameCachedEventSource(mgr, logCat, detNamingClass, epi)
                              , calib::Handle<EventBitTags>("default", mgr)
                              , _timeCut(timeCut)
                              , _useBadSpill(omitBadSpills)
                              , _serviceTypeCode(0x0), _physicsTypeCode(0x0)
                              {
    _dddMgr.SetMapsDir(mapsDir);
    for(const auto & src : inputs) _dddMgr.AddDataSource(src);
}

void
SourceWrapper::handle_update(const EventBitTags & ebt) {
    calib::Handle<EventBitTags>::handle_update(ebt);
    auto it = ebt.types.find("service");
    if(it == ebt.types.end()) {
        _log.warn("p348reco data source wrapper requires \"service\" type code to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _serviceTypeCode = 0x0;
    } else {
        _serviceTypeCode = it->second;
    }

    it = ebt.types.find("phys");
    if(it == ebt.types.end()) {
        _log.warn("p348reco data source wrapper source requires \"phys\" type code to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _physicsTypeCode = 0x0;
    } else {
        _physicsTypeCode = it->second;
    }

    it = ebt.triggers.find("phys");
    if(it == ebt.types.end()) {
        _log.warn("p348reco data source wrapper source requires \"phys\" trigger to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _physTrigger = 0x0;
    } else {
        _physTrigger = it->second;
    }

    it = ebt.triggers.find("rand");
    if(it == ebt.types.end()) {
        _log.warn("p348reco data source wrapper source requires \"rand\" trigger to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _randomTrigger = 0x0;
    } else {
        _randomTrigger = it->second;
    }

    it = ebt.triggers.find("beam");
    if(it == ebt.types.end()) {
        _log.warn("p348reco data source wrapper source requires \"beam\" trigger to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _beamTrigger = 0x0;
    } else {
        _beamTrigger = it->second;
    }
}

bool
SourceWrapper::read( event::Event & dest
                   , event::LocalMemory & lmem
                   ) {
    bool gotEvent = false;
    while(!!(gotEvent = _dddMgr.ReadEvent())) {
        if(!_dddMgr.DecodeEvent()) {
            // TODO: warn on decoding failure here
            continue;
        }
        gotEvent = true;
        const RecoEvent p348Event0 = RunP348Reco(_dddMgr);
        RecoEvent p348Event = p348Event0;
        timecut(p348Event, _timeCut);
        #if 0
        if(!p348Event.isPhysics) continue;  // TODO: sort events by type

        if (p348Event.isCalibration)        continue;  //etypeplot.Fill("LED", 1.);
        //if (e.isPhysics)            etypeplot.Fill("PHYS", 1.);
        if (!p348Event.isTriggerPhysics)     continue;  //etypeplot.Fill("TRG.PHYS", 1.);
        //if (p348Event.isTriggerBeam)        continue;  //etypeplot.Fill("TRG.BEAM", 1.);
        //if (p348Event.isTriggerBeamOnly())  continue;  //etypeplot.Fill("TRG.BEAMO", 1.);
        if (p348Event.isTriggerRandom)      continue;  //etypeplot.Fill("TRG.RAND", 1.);
        #endif

        if(_useBadSpill && IsBadBurst(p348Event.run, p348Event.spill)) continue;
        // copy event
        _copy_event_data( dest, p348Event, lmem );
        break;
    }
    return gotEvent;
}

void
SourceWrapper::_copy_MSADC_data( event::Event & dest
                               , const RecoEvent & re
                               , event::LocalMemory & lmem ) {
    const nameutils::DetectorNaming & nm = Handle<nameutils::DetectorNaming>::get();
        // NOTE: in p348reco many detectors uses X-index of DAQ digits as a station
    // number.
    { // ECAL
        //std::cout << "ECAL ";
        SADCTranslate c( dest, lmem, nm["ECAL"],
                [](DetID ecalID, size_t * idxs, int ND){
                    assert(ND == 3);
                    CellID cid(idxs[1], idxs[2], idxs[0]);
                    ecalID.number(0);  // both parts are from single station
                    ecalID.payload(cid.cellID);
                    return ecalID;
                });
        util::for_each_in_array<const decltype(RecoEvent::ECAL)>(re.ECAL, c);
    } {
        SADCTranslate c(nm["HCAL"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::HCAL)>(re.HCAL, c);
    } {
        SADCTranslate c(nm["BGO"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::BGO)>(re.BGO, c);
    } {
        SADCTranslate c(nm["VETO"], dest, lmem, true);
        util::for_each_in_array<const decltype(RecoEvent::VETO)>(re.VETO, c);
    } {
        SADCTranslate c(nm["MUON"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::MUON)>(re.MUON, c);
    } {
        // TODO: possibly incompatible with native DDD data source; indexing
        // scheme is unclear
        SADCTranslate c(nm["ECALSUM"], dest, lmem, true);
        util::for_each_in_array<const decltype(RecoEvent::ECALSUM)>(re.ECALSUM, c);
    } {
        SADCTranslate c(nm["ATARG"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::ATARG)>(re.ATARG, c);
    } {
        SADCTranslate c(nm["SRD"], dest, lmem, true);
        util::for_each_in_array<const decltype(RecoEvent::SRD)>(re.SRD, c);
    } {
        // TODO: LYSO segmentation is nor very clear... 27 pieces of what?
        // It has to be a calorimeter of transversal MxN segmentation, but
        // in p348reco it is described as just and array of 27 cells.
        SADCTranslate c(nm["LYSO"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::LYSO)>(re.LYSO, c);
    }
    // S1, S2, S3, S4, T2, V1, V2, ZC below
    {
        SADCTranslate c(nm["WCAL"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::WCAL)>(re.WCAL, c);
    }
    // WCAL, VTEC, VTWC below
    {
        SADCTranslate c(nm["DM"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::DM)>(re.DM, c);
    } {
        SADCTranslate c(nm["ZDCAL"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::ZDCAL)>(re.ZDCAL, c);
    } /*{  // TODO
        SADCTranslate c(nm["VHCAL"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::VHCAL0)>(re.VHCAL0, c);
    }*/ {
        SADCTranslate c(nm["PKRCAL0"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::PKRCAL0)>(re.PKRCAL0, c);
    } {
        SADCTranslate c(nm["PKRCAL1"], dest, lmem);
        util::for_each_in_array<const decltype(RecoEvent::PKRCAL1)>(re.PKRCAL1, c);
    }
    // individual MSADC detectors (or individual from PoV of p348reco)
    struct {
        const Cell & src;
        const char name[16];
    } _msadcIndiv[] = { 
        {re.S1, "S1"}, {re.S2, "S2"}, {re.S3, "S3"}, {re.S4, "S4"},
        {re.T2, "T2"}, {re.V1, "V1"}, {re.V2, "V2"}, {re.ZC, "ZC"},
        {re.VTEC, "VTEC"}, {re.VTWC, "VTWC"}, {re.WCAT, "WCAT"}
    };
    for( size_t i = 0; i < sizeof(_msadcIndiv)/sizeof(_msadcIndiv[0]); ++i ) {
        if(_msadcIndiv[i].src.raw.empty()) continue;
        DetID did = nm[_msadcIndiv[i].name];
        if(!did.is_number_set()) did.number(0);
        // Inherit x/y index from digit
        CellID cID;
        cID.set_x(0);
        cID.set_y(0);
        //cID.set_x(_msadcIndiv[i].src.?.GetX());
        //cID.set_y(_msadcIndiv[i].src.?.GetY());  // TODO: does reco's Cell provide raw digit?
        did.payload(cID.cellID);
        // Create new SADC hit and add it to event
        mem::Ref<event::SADCHit> hit = lmem.create<event::SADCHit>(lmem);
        dest.sadcHits[did] = hit;  // todo check result?
        util::reset(*hit);
        // create "raw data" section of the hit
        hit->rawData = lmem.create<event::RawDataSADC>(lmem);
        util::reset(*(hit->rawData));
        SADCTranslate::copy_data(*hit, _msadcIndiv[i].src, lmem);
    }
}

void
SourceWrapper::_copy_MM_data( event::Event & dest
                            , const RecoEvent & re
                            , event::LocalMemory & lmem ) {
    const nameutils::DetectorNaming & nm = Handle<nameutils::DetectorNaming>::get();
    // - Micromegas
    struct {
        int n;
        const Micromega * mm;
    } mmArray[] = { {1, &re.MM1}, {2, &re.MM2}
                  , {3, &re.MM3}, {4, &re.MM4}
                  , {5, &re.MM3}, {6, &re.MM4}
                  , {7, &re.MM3}, {8, &re.MM4}
                  , {0, nullptr}
                  // ...
                  };
    const DetID mmKinID = nm["MM"];
    for( auto * p = mmArray; p->mm; ++p ) {
        // Copy pre-selected APV hits from MuMega planes into APV hits
        // structure from p->xplane, p->yplane:
        struct {
            const MicromegaPlane * planePtr;
            APVPlaneID::Projection projection;
        } planes[] =  { {&(p->mm->xplane), APVPlaneID::kX}
                      , {&(p->mm->yplane), APVPlaneID::kY}
                      , {nullptr}
                      };
        // Make basic detector ID
        DetID stationID = mmKinID;
        stationID.number(p->n);
        // this will be needed after to associate hits with clusters
        std::unordered_map<const MMCluster *, PlaneKey> p348recoClusters;
        for( const auto * pp = planes
           ; pp->planePtr
           ; ++pp
           ) {
            const MicromegaPlane & plane = *(pp->planePtr);
            const WireID baseWireID;
            // iterate over hits recorded for current plane and copy them into
            // na64sw's event as new instances of `APVHit` struct
            for(size_t nHit = 0; nHit < plane.strips.size(); ++nHit) {
                #if 0  // xxx, obsolete?
                // skip "empty" channels in the array; p348reco assumes it to
                // be empty if nHit < MM_size and strip[nHit] is zero according
                // to its clustering algorithm. The nHit < MM_size seems to be
                // rather an assertion...
                if( 0 == plane.strips[nHit] ) continue;
                #endif
                // get DAQ (electronic) channel for MuMega plane wrt current
                // p348reco wire layout. This value is returned by native DDD's
                // `CS::ChipAPV::Digit::GetChannel()`
                CS::uint16 daqChanNo;
                {
                    auto it = plane.calib->MM_reverse_map.find(nHit);
                    assert(plane.calib->MM_reverse_map.end() != it);
                    daqChanNo = it->second;
                }
                // assemble fully-qualified hit ID using DAQ strip number
                DetID hitID = stationID;
                WireID wid(pp->projection, daqChanNo );
                hitID.payload(wid.id);

                // Try to find this hit among existing ones
                auto it = dest.apvHits.find(hitID);
                if( dest.apvHits.end() != it ) continue;  // known one, skip it
                // Create a hit and copy data
                auto ref = lmem.create<event::APVHit>(lmem);
                util::reset(*ref);
                // copy hit data
                ref->maxCharge = plane.strips[nHit].charge;
                //ref->time      = plane.strips[nHit].time();  // TODO
                ref->t02       = plane.strips[nHit].t02;
                ref->t12       = plane.strips[nHit].t12;
                ref->t02sigma  = plane.strips[nHit].sigma02;
                ref->t12sigma  = plane.strips[nHit].sigma12;
                // put hit copy to an event
                dest.apvHits.emplace(hitID, ref);
            }
            // iterate over clusters created for current plane and build their
            // counterparts as new instances of `APVCluster`
            int nCluster = 0;
            for( const MMCluster & p348ClusterRef : plane.clusters ) {
                mem::Ref<event::APVCluster> ref
                        = lmem.create<event::APVCluster>(lmem);
                util::reset(*ref);
                // connect DAQ APV hits collected above with current cluster
                // vector in clusters are just kinda static arrays, allocated
                // to size of the plane but never resized. They're filled up
                // to `size` N of elements.
                for( size_t nEl = 0; nEl < p348ClusterRef.size(); ++nEl ) {
                    int nWire = p348ClusterRef.strips[nEl].bin;
                    // build hit ID
                    CS::uint16 daqChanNo;
                    {
                        auto it = plane.calib->MM_reverse_map.find(nWire);
                        assert(plane.calib->MM_reverse_map.end() != it);
                        daqChanNo = it->second;
                    }
                    // assemble fully-qualified hit ID using DAQ strip number
                    DetID hitID = stationID;
                    WireID wid(pp->projection, daqChanNo);
                    hitID.payload(wid.id);
                    // find hit; consider not found case as basic algorithm
                    // failure, a fatal inconsistency
                    auto it = dest.apvHits.find(hitID);
                    if(dest.apvHits.end() == it) {
                        // NA64DP_RUNTIME_ERROR
                        log4cpp::Category::getInstance("sources.p348reco")
                            .error( "P348RecoEvent integrity error: "
                                " cluster #%d on plane %s refers to hit on"
                                " wire %d (by element #%zu) that is not defined"
                                " for this plane."
                                , nCluster
                                , nm[hitID].c_str()
                                , (int) daqChanNo
                                , nEl
                                );
                        continue;
                    }
                    ref->hits.emplace(hitID, it->second);
                }
                // copy inferred cluster values
                ref->charge     = p348ClusterRef.charge_total;
                ref->position   = p348ClusterRef.position();
                ref->time       = p348ClusterRef.cluster_time();
                ref->timeError  = p348ClusterRef.cluster_time_error();
                // put new cluster to an event
                PlaneKey pk( stationID.chip()
                           , stationID.kin()
                           , stationID.number()
                           , pp->projection
                           );
                dest.apvClusters.emplace(pk, ref);
                // remember cluster ptr -> plane key
                p348recoClusters.emplace(&p348ClusterRef, pk);
                ++nCluster;
            }
            // ... TODO: "bests"?
        }  // iterate over MuMega planes (X, Y for a single station)
        // A "Hit" of p348reco's Micromega has backref to x/y cluster it
        // consists of. This "hit" is somewhat similar to na64sw "TrackScore"
        // structure
        for( const auto & hit : p->mm->hits ) {
            auto tp = lmem.create<event::TrackScore>(lmem);
            util::reset(*tp);
            struct {
                const MMCluster * c;
                APVPlaneID::Projection projection;
            } clusters[] = { {hit.xcluster, APVPlaneID::kX}
                           , {hit.ycluster, APVPlaneID::kY}
                           , {nullptr} };
            for( auto c = clusters; c->c; ++c ) {
                // look for cluster
                auto it = p348recoClusters.find(c->c);
                if(it == p348recoClusters.end()) {
                    NA64DP_RUNTIME_ERROR( "p348reco event integrity error for"
                            " MuMega #%d: p348reco's mm \"Hit\" instance %p"
                            " refers to unknown %c-cluster %p (no match"
                            " among known %zu clusters)"
                            , stationID.number()
                            , &hit
                            , APVPlaneID::proj_label(c->projection)
                            , c->c
                            , p348recoClusters.size()
                            );
                }
                dest.apvClusters.emplace( it->second
                                        , dest.apvClusters.find(it->second)->second
                                        );
            }
            // Get position as provided by p348reco
            {  // ...local
                TVector3 lR = hit.pos();
                tp->lR[0] = lR[0];
                tp->lR[1] = lR[1];
                tp->lR[2] = lR[2];
            }
            #if 0
            {  // ...global
                TVector3 gR = hit.abspos();
                tp->gR[0] = gR[0];
                tp->gR[1] = gR[1];
                tp->gR[2] = gR[2];
            }
            #endif
            // put the trackpoint into an event
            dest.trackScores.emplace(stationID, tp);
        }  // iterate over hits
    }  // iterate over stations
}

void
SourceWrapper::_copy_GM_data( event::Event & dest
                            , const RecoEvent & re
                            , event::LocalMemory & lmem ) {
    const nameutils::DetectorNaming & nm = Handle<nameutils::DetectorNaming>::get();
    struct {
        gem::GEM * gm;
        int n;
    } gms[] = { {re.GM01, 1}
              , {re.GM02, 2}
              , {re.GM03, 3}
              , {re.GM04, 4}
              // ...
              , {nullptr, 0}
              };
    DetKinKey detKinKey(nm["GM"]);
    for( auto c = gms; c->gm; ++c ) {  // for each GM station
        StationKey sk(detKinKey.chip(), detKinKey.kin(), c->n);
        gem::GEM & gm = *(c->gm);
        struct {
            const CsGEMPlane * plane;
            const std::vector<gem::CsGEMClusterWrapper> * clustersOnPlane;
            const APVPlaneID::Projection projection;
        } planes[] = { {&(gm.xPlane), &(gm.xplane), APVPlaneID::kX}
                     , {&(gm.yPlane), &(gm.yplane), APVPlaneID::kY}
                     , {nullptr}
                     };
        // keep the clusters ID within this map to build backrefs from
        // trackpoint to clusters further; note, that for brevity we mix
        // up X/Y clusters in this container.
        std::unordered_map<const CsGEMCluster *, PlaneKey> clustersByCsPtr;
        for( auto cc = planes; cc->plane; ++cc ) {  // for each GM plane
            for(const CsGEMHit * csHitPtr : cc->plane->GetHits()) {  // for each hit
                // allocate and re-set new APV hit instance
                mem::Ref<event::APVHit> apvHit = lmem.create<event::APVHit>(lmem);
                util::reset(*apvHit);
                // allocate, re-set and connect raw APV hit data instance with
                // APV hit instance 
                mem::Ref<event::RawDataAPV> rawAPVHit = lmem.create<event::RawDataAPV>(lmem);
                util::reset(*rawAPVHit);
                apvHit->rawData = rawAPVHit;
                {// copy "raw" hit data
                    const CsGEMChan * gmChan = csHitPtr->GetChan();
                    const CsGEMChanId * gmChanId = gmChan->GetId();
                    rawAPVHit->wireNo = gmChanId->GetDetectorChannel();  // TODO: check
                    // srcID?
                    // adcID?
                    rawAPVHit->chip = gmChanId->GetAPVChip();
                    rawAPVHit->chipChannel = gmChanId->GetAPVChannel();
                    // timeTag?
                    assert(3 == csHitPtr->GetAmp().size());
                    // ^^^ returns vector<float>&, APV amplitude
                    rawAPVHit->samples[0] = csHitPtr->GetAmp()[0];
                    rawAPVHit->samples[1] = csHitPtr->GetAmp()[1];
                    rawAPVHit->samples[2] = csHitPtr->GetAmp()[2];
                }
                // ...->GetXTalk(), double, a cross talk probability
                // ...->GetNrClusters(), returns int, a number of clusters this hit contributes to
                // time of hit, calcd by three samples + error
                double t, tErr;
                csHitPtr->GetTime(t, tErr);
                apvHit->time = t;
                apvHit->timeError = tErr;
                // complete detector id (full wire signature)
                WireID wid( cc->projection, rawAPVHit->wireNo );
                DetID hitID( sk.chip(), sk.kin(), sk.number()
                           , wid.id );
                // put a hit into the event, check it's really inserted
                auto ir = dest.apvHits.emplace(hitID, apvHit);
                if(!ir.second) {
                    NA64DP_RUNTIME_ERROR( "GEM hit collision: %s",
                            nm[hitID].c_str() );
                }
            }  // for each hit
            for( const gem::CsGEMClusterWrapper & cluster_ : *(cc->clustersOnPlane) ) {
                // create new APV cluster instance
                mem::Ref<event::APVCluster> dstCluster
                    = lmem.create<event::APVCluster>(lmem);
                util::reset(*dstCluster);
                const CsGEMCluster & cluster = *cluster_.orig;
                // iterate over hits associated with cluster to fill up the
                // cluster with refs to hit copies
                for(const CsGEMHit * csHitPtr : cluster.GetHits() ) {
                    WireID wid( cc->projection, csHitPtr->GetChan()->GetId()->GetDetectorChannel() );
                    DetID hitID( sk.chip(), sk.kin(), sk.number()
                           , wid.id );
                    auto it = dest.apvHits.find(hitID);
                    if(dest.apvHits.end() == it) {
                        NA64DP_RUNTIME_ERROR( "GEM cluster integrity error:"
                                " cluster refers to unknown hit %s.",
                                nm[hitID].c_str() );
                        continue;
                    }
                    dstCluster->hits.emplace( hitID, it->second);
                }
                // copy some reconstructed cluster values
                dstCluster->position  = cluster_.position();
                // ... cluster_.position_err() or cluster.GetPositionErr() would be nice to copy here
                //dstCluster->charge    = cluster_.???;
                dstCluster->time      = cluster_.time();
                dstCluster->timeError = cluster_.time_err();
                // add cluster
                PlaneKey pk(sk.chip(), sk.kin(), sk.number(), cc->projection);
                dest.apvClusters.emplace( pk
                                        , dstCluster
                                        );
                // memoize the cluster ptr for track point
                clustersByCsPtr.emplace(&cluster, pk);
            }  // for each cluster
        }  // for each GEM plane in station
        // Curently (Mar 2022) p348reco gem::GEM provides only a single "hit",
        // so we convert it to a track point. Note, that track point is added
        // only when BOTH x,y clusters are defined within this "bestHit"
        // attribute (according to its to-bool operator).
        if( gm.bestHit ) {
            // alloc an reset new track point instance
            mem::Ref<event::TrackScore> tp
                = lmem.create<event::TrackScore>(lmem);
            util::reset(*tp);
            const gem::GemHitPoint & ghp = gm.bestHit;
            // p348reco GEM's hit point stores copies of cluster wrappers, but
            // references to original CsCluster must be the same...
            auto xIt = clustersByCsPtr.find(ghp.x.orig)
               , yIt = clustersByCsPtr.find(ghp.y.orig)
               ;
            if( ghp.x.orig && xIt == clustersByCsPtr.end() ) {
                NA64DP_RUNTIME_ERROR("p348reco event integrity error:"
                        " CsGEMCluster referenced by \"bestHit\" is unknown"
                        " for X plane.");
            }
            if( ghp.y.orig && yIt == clustersByCsPtr.end() ) {
                NA64DP_RUNTIME_ERROR("p348reco event integrity error:"
                        " CsGEMCluster referenced by \"bestHit\" is unknown"
                        " for Y plane.");
            }
            // build backref to clusters from track point
            if(ghp.x.orig)
                tp->hitRefs.emplace( xIt->second
                                   , dest.apvClusters.find(xIt->second)->second
                                   );
            if(ghp.y.orig)
                tp->hitRefs.emplace( yIt->second
                                   , dest.apvClusters.find(yIt->second)->second
                                   );
            // copy hit point data (positions)
            {  // ...local
                TVector3 lR = ghp.rel_pos();
                tp->lR[0] = lR[0];
                tp->lR[1] = lR[1];
                tp->lR[2] = lR[2];
            }
            #if 0
            {  // ...global
                TVector3 gR = ghp.abs_pos();
                tp->gR[0] = gR[0];
                tp->gR[1] = gR[1];
                tp->gR[2] = gR[2];
            }
            #endif
            // add track point to an event
            dest.trackScores.emplace(sk, tp);
        }  // "bestHit"
    }  // for each GM station
}

void
SourceWrapper::_copy_event_data( event::Event & dest
                               , const RecoEvent & re
                               , event::LocalMemory & lmem ) {
    // copy event ID
    dest.id.run_no(re.run);
    dest.id.spill_no(re.spill);
    dest.id.event_no(re.spillevent);
    // This may cause updates on all calibration subscribers including
    // the current data source object:
    calib_manager().event_id( dest.id, _dddMgr.GetEvent().GetTime() );

    // set event type and trigger bits respecting the semantics
    dest.trigger = 0x0;
    if(!re.isCalibration) {
        if(re.isTriggerBeam)        dest.trigger |= _beamTrigger;
        if(re.isTriggerPhysics)     dest.trigger |= _physTrigger;
        if(re.isTriggerRandom)      dest.trigger |= _randomTrigger;
    }
    dest.evType = 0x0;
    if(re.isPhysics)
        dest.evType |= _physicsTypeCode;
    else
        dest.evType |= _serviceTypeCode;
    if(re.isCalibration)
        dest.evType |= _serviceTypeCode;
    if(re.isOnSpill)
        dest.evType |= _inSpillTypeCode;

    // master time
    dest.masterTime = re.masterTime;
    _copy_MSADC_data(dest, re, lmem);
    // ... TODO: hodoscopes
    _copy_MM_data(dest, re, lmem);
    _copy_GM_data(dest, re, lmem);
    // ... TODO: straws
    // ... TODO: MCTruth?
}

}  // namespace ::na64dp::p348reco
}  // namespace na64dp

REGISTER_SOURCE( p348RecoWrapperSource
               , calibManager
               , yamlNode
               , ids
               , "A wrapper around original p348reco providing event data in"
                 " `na64sw` format"
               ) {
    if( ! yamlNode["CORALMapsDir"] ) {
        NA64DP_RUNTIME_ERROR("Mandatory \"CORALMapsDir\" parameter is not"
                " provided for DaqDataDecoding data source object");
    }
    #ifdef CORAL_MAPS_DIR
    setenv("CORAL_MAPS_DIR", CORAL_MAPS_DIR, 0);
    #endif
    auto mapsDirs = na64dp::util::expand_names(yamlNode["CORALMapsDir"].as<std::string>());
    if( mapsDirs.size() != 1 ) {
        NA64DP_RUNTIME_ERROR( "CORAL maps dir \"%s\" expanded to %zu path strings; has to be"
                              " exactly one."
                , yamlNode["CORALMapsDir"].as<std::string>().c_str()
                , mapsDirs.size() );
    }
    std::string coralMapsDir = mapsDirs[0];

    std::unordered_set<std::string> omitDetectors;
    if( yamlNode["omitDetectors"] ) {
        auto v = yamlNode["omitDetectors"].as<std::vector<std::string>>();
        omitDetectors = std::unordered_set<std::string>(v.begin(), v.end());
    }

    const std::string detNamingClass = yamlNode["namingScheme"]
                                     ? yamlNode["namingScheme"].as<std::string>()
                                     : "default"
                                     ;
    log4cpp::Category & logCat = log4cpp::Category::getInstance(yamlNode["_log"] ? yamlNode["_log"].as<std::string>():"source");
    na64dp::iEvProcInfo * epi = nullptr;  // TODO: provide in ctr or set it aposteriori with some method...

    return new na64dp::p348reco::SourceWrapper( calibManager
                  , coralMapsDir
                  , ids
                  , logCat
                  , yamlNode["timeCut"] ? yamlNode["timeCut"].as<float>() : 4.
                  , yamlNode["omitBadSpills"] ? yamlNode["omitBadSpills"].as<bool>() : true
                  , omitDetectors
                  , detNamingClass
                  , epi
                  );
}

