#include "na64app/extension.hh"

#include <iostream>
#include <cerrno>
#include <cstring>

namespace na64dp {

/**\brief Extension class injecting some runtime environments
 *
 * This class sets `P348_CONDDB_DIR`` wrt what has been found during build
 * process.
 * */
class P348Reco : public iRuntimeExtension {
public:
    P348Reco() {
        #ifdef P348_CONDDB_DIR
        if(-1 == setenv("P348_CONDDB_DIR", P348_CONDDB_DIR, 0)) {
            // ^^^ overwrite=0 means that if variable is set, its value
            // will be left intact
            int ec = errno;
            std::cerr << "Failed to set P348_CONDDB_DIR env var: "
                    << strerror(ec) << std::endl; 
        }
        if(-1 == setenv("CONDDB", P348_CONDDB_DIR, 0)) {
            // ^^^ overwrite=0 means that if variable is set, its value
            // will be left intact
            int ec = errno;
            std::cerr << "Failed to set CONDDB env var: "
                    << strerror(ec) << std::endl; 
        }
        #endif
    };
    void init(calib::Manager & cmgr, iEvProcInfo * ) final {
    }
    void set_parameter(const std::string &, const std::string &) final {}
    void finalize() final {}
};  // class P348RecoExtension

REGISTER_EXTENSION(P348Reco);

}  // namespace na64dp

