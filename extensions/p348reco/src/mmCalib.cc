#include "mmCalib.hh"

#if (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

#include "na64detID/wireID.hh"

namespace na64dp {
namespace p348reco_handlers {

MMCalibAdapter::MMCalibAdapter( calib::Dispatcher & cdsp
                              , const std::string & mmPedestalSuffixPattern
                              , const std::string & selectionStrExpr
                              , log4cpp::Category & logCat
                              , const std::string & detNameClass
                              , const std::string & timingCalibClass
                              , const std::string & pedestalsCalibClass
                              , const std::string & wireDesignClass
                              ) : SelectiveHandlerMixin(cdsp, selectionStrExpr, logCat, detNameClass)
                                , _rxMMName(mmPedestalSuffixPattern)
                                , _L(logCat)
                                {
    //cdsp.subscribe<typename T>(typename util::Observable<T>::iObserver &client, const std::string &ciName)
    cdsp.subscribe<Collection<APVTimingCalib>>(*this, timingCalibClass);
    cdsp.subscribe<Collection<APVChannelEntry>>(*this, pedestalsCalibClass);
    cdsp.subscribe<calib::MuMegaWiresDesignEntry>(*this, wireDesignClass);
    // ^^^ todo: calib classes to ctr parameter?
    _L << log4cpp::Priority::DEBUG
       << "p348reco calib adapter initialized";
}

MM_plane_calib &
MMCalibAdapter::get_entry(DetID did) {
    auto ir = _planeCalibs.emplace(did, MM_plane_calib());
    if(ir.second) {
        _L << log4cpp::Priority::DEBUG
           << "Created p348reco APV calib item \"" << naming()[did] << "\"";
    }
    return ir.first->second;
}

static void _copy_timing_values( MM_time_point & dest
                               , const P348_RECO_NAMESPACE::APVTimingCalibEntry::ParPack & src ) {
    dest.parameters.r0 = src.r0;
    dest.parameters.t0 = src.t0;
    dest.parameters.a0 = src.a0;

    dest.errors.r02 = src.r02;
    dest.errors.t02 = src.t02;
    dest.errors.a02 = src.a02;

    dest.errors.r0t0 = src.r0t0;
    dest.errors.r0a0 = src.r0a0;
    dest.errors.t0a0 = src.t0a0;
}

void
MMCalibAdapter::_set_layout( DetID did
                           , MM_plane_calib & planeCalib
                           ) {
    calib::MuMegaWiresDesign * designPtr = nullptr;
    for(auto it = _designs.rbegin(); it != _designs.rend(); ++it) {
        if( (!it->second.first.empty()) && !it->first) {
            // compile selector
            it->first = new DetSelect( it->second.first
                                     , util::gDetIDGetters
                                     , naming() );
        }
        if( it->first && ! it->first->matches(did) ) continue;  // doesn't match
        _L << log4cpp::Priority::DEBUG
           << "Item " << naming()[did] << " matches design selector \""
           << it->second.first << "\", setting design of "
           << it->second.second.size()
           << " wires";
        designPtr = &(it->second.second);
        break;
    }
    if(!designPtr) {
        NA64DP_RUNTIME_ERROR("No design set for APV-based detector \"%s\""
                , naming()[did].c_str());
    }
    calib::MuMegaWiresDesign & design = *designPtr;

    planeCalib.MM_Nchannels = design.size();
    // Note: following works only for case when one DAQ channel always
    // corresponds to N physical strips. So far it seems to be true for
    // all MMs in use...
    planeCalib.MM_Nstrips = design.size()*(design.rbegin()->size());
    planeCalib.MM_strip_step = .25;  // true so far
    planeCalib.MM_min_cluster_size = 2;  // true so far
    //planeCalib.sigma.assign()  // omit it here as to be set in APVTimingCalib handle function

    // copy layout (instead of planeCalib.Read_Multiplex_mapping())
    size_t nDaqWire = 0;
    for(const auto & arr : design) {
        planeCalib.MM_map[nDaqWire++]
            = decltype(planeCalib.MM_map)::mapped_type(arr.begin(), arr.end());
    }
    planeCalib.Create_MM_reverse_map();
}

void
MMCalibAdapter::handle_update(const Collection<APVTimingCalib> & calibs) {
    //calib::Handle<Collection<APVTimingCalib>>::handle_update(calibs);
    for( const auto & p : calibs ) {
        _L << log4cpp::Priority::DEBUG
           << "Copying APV time calibration data for \"" << p.first << "\" from "
           << p.second.srcDocID << ":" << p.second.lineNo
           ;
        DetID planeID = naming()[p.first];
        if(!SelectiveHandlerMixin::matches(planeID)) {
            _L << log4cpp::Priority::DEBUG
               << "APV time calibration data for \"" << p.first << "\" declined "
               " because of selection."
               ;
            continue;
        }
        MM_plane_calib & planeCalib = get_entry(planeID);
        _set_layout(planeID, planeCalib);
        const P348_RECO_NAMESPACE::APVTimingCalibEntry & calib = p.second.data;

        _copy_timing_values(planeCalib.timing.a02, calib.a02);
        _copy_timing_values(planeCalib.timing.a12, calib.a12);

        planeCalib.time = planeCalib.timing.t0();
        planeCalib.time_err = planeCalib.timing.t0_err();
    }
}

void
MMCalibAdapter::handle_update(const Collection<APVChannelEntry> & calibs) {
    for( const auto & p : calibs ) {
        const std::tuple<std::string, int, int, int, int> & channelKey
            = p.first;
        // ^^^ key semantics:
        //      <name:str> <ldc:int> <eq:int> <subeq:int> <ch:int>
        //      ldc:int  -- dummy (?)
        //      eq:int -- DAQ source ID
        //      subeq:int -- DAQ ADC ID
        //      ch:int -- DAQ chip ID
        std::string stationName = std::get<0>(channelKey);
        if(stationName.substr(0, 2) == "MX") {
            _L << log4cpp::Priority::DEBUG
               << "`MX' pedestal file suffix assumed to denote `MM', fixing it"
                  "(\"" << stationName << "\")";
            // For some reason MM detectors are defined as "MX" in some runs
            // (2016, 2022mu, etc)
            stationName[1] = 'M';
        }
        if(!std::regex_match(stationName, _rxMMName)) {
            // todo: perhaps, some of them (like "full", "_v3" and whatsoever
            //       can be useful)
            _L << log4cpp::Priority::DEBUG
               << "Skipping pedestal calibration \""
               << stationName
               << "\" as this name does not seem to be MM channel calibrations"
               ;
            continue;
        }
        DetID stationID;
        try {
            stationID = naming()[stationName];
        } catch( std::runtime_error & e ) {
            _L.error( "While treating station \"%s\":"
                      " \"%s\". APV pedestal channel calibration update skipped."
                    , stationName.c_str()
                    , e.what()
                    );
            continue;
        }
        if(stationID.is_payload_set()) {
            // This is to ignore GEMS
            _L << log4cpp::Priority::WARN
               << "Projection seem to be set for what is to be assumed"
                  " channel entry calibration: \"" << stationName
               << "\". Skipped"
                ;
            continue;
            //NA64DP_RUNTIME_ERROR("Projection is set for what is assumed to be"
            //        " micromega pedestal calibration data: \"%s\""
            //        , stationName.c_str() );
        }

        if( _L.getPriority() >= log4cpp::Priority::DEBUG ) {
            std::unordered_set<std::string> sources;
            std::transform( p.second.begin(), p.second.end()
                          , std::inserter(sources, sources.begin())
                          , [](const auto & p){return p.srcDocID;});
            _L << log4cpp::Priority::DEBUG
               << "Copying APV channel calibration data for \""
               << stationName << "\" (both projections), sources(s): "
               << util::str_join(sources.begin(), sources.end())
               ;
        }

        // CAVEAT: so far only 2-projection MM detectors in the experiment
        DetID xDid(stationID)
            , yDid(stationID)
            ;
        xDid.payload(WireID(WireID::kX).id);
        yDid.payload(WireID(WireID::kY).id);
        //xDid.payload_as<WireID>()->proj(WireID::kX);
        //yDid.payload_as<WireID>()->proj(WireID::kY);
        // TODO: apply selection
        MM_plane_calib * xPlaneCalibPtr = SelectiveHandlerMixin::matches(xDid) ? &get_entry(xDid) : nullptr
                     , * yPlaneCalibPtr = SelectiveHandlerMixin::matches(yDid) ? &get_entry(yDid) : nullptr
                     ;
        if((!xPlaneCalibPtr) && (!yPlaneCalibPtr)) {
            _L << log4cpp::Priority::DEBUG
               << "Channel entry update for \""
               << stationName << "\" declined due to selector expression"
                " (both planes)."
               ;
        }
        if(xPlaneCalibPtr) {
            _set_layout(xDid, *xPlaneCalibPtr);
        } else {
            _L << log4cpp::Priority::DEBUG
               << "Channel entry update for X plane of \""
               << stationName << "\" declined due to selector expression."
               ;
        }
        if(yPlaneCalibPtr) {
            _set_layout(yDid, *yPlaneCalibPtr);
        } else {
            _L << log4cpp::Priority::DEBUG
               << "Channel entry update for Y plane of \""
               << stationName << "\" declined due to selector expression."
               ;
        }
        if(!(xPlaneCalibPtr || yPlaneCalibPtr)) return;
        // assure `sigma` vector provides enough space
        if(yPlaneCalibPtr)
            yPlaneCalibPtr->sigma.resize(64, std::nan("0"));
        if(xPlaneCalibPtr) {
            if(320 == xPlaneCalibPtr->MM_Nstrips) {
                xPlaneCalibPtr->sigma.resize(64, std::nan("0"));
            } else if(960 == xPlaneCalibPtr->MM_Nstrips) {
                xPlaneCalibPtr->sigma.resize(192, std::nan("0"));
            } else {
                NA64DP_RUNTIME_ERROR("Unexpected number of items given in APV time"
                        " calibrations: %zu", p.second.size());
            }
        }
        // CAVEAT: NA64sw calib data provided here with full DAQ key `channelKey'
        // (see top of this function). It can be matched wrt APV hit's
        // data by attributes: std::tuple<uint16_t, uint16_t, uint16_t> hitKey = {
        //          cHit.rawData->srcID, cHit.rawData->adcID, cHit.rawData->chip };
        // (see for instance native reconstruction), but `p348reco' relies
        // on line number to determine the projection. We somehow mimic it
        // here by exploiting the fact that line number for X plane starts
        // from 2.
        bool secondPart = p.second.begin()->lineNo > 2;
        size_t counter = secondPart ? 128 : 0;
        // somewhat tricky stuff, yet only sigma is used (see `ReadSigmaCalib()`).
        //throw std::runtime_error("TODO: DAQID-based layout");  // TODO
        for(const auto & item : p.second) {
            const auto sigma = item.data.pedestalsigma;
            if(counter < 128) {  // small MM, even is for X, odd for Y
                if(0 == counter%2) {
                    if(xPlaneCalibPtr)
                        xPlaneCalibPtr->sigma[counter >> 1] = sigma;
                } else {
                    if(yPlaneCalibPtr)
                        yPlaneCalibPtr->sigma[counter >> 1] = sigma;
                }
            } else if(xPlaneCalibPtr) {
                if(0 == counter%2) {  // x, #64-127
                    xPlaneCalibPtr->sigma[191 - (counter >> 1)] = sigma;
                } else {  // x, #128-191
                    xPlaneCalibPtr->sigma[255 - (counter >> 1)] = sigma;
                }
            }
            ++counter;
        }
    }
}

void
MMCalibAdapter::handle_update(const calib::MuMegaWiresDesignEntry & design) {
    _designs.push_back( std::pair<DetSelect *, calib::MuMegaWiresDesignEntry>(
                nullptr, design) );
}

void
MMCalibAdapter::dump_current_calibs(std::ostream & ofs) const {
    for(auto & [planeID, planeCalib] : _planeCalibs) {
        ofs << naming()[planeID] << std::endl
            << planeCalib;
    }
}

}  // namespace ::na64dp::p348reco_handlers
}  // namespace na64dp

#endif  // (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

