#pragma once

#if (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/apv.hh"

#include <TVector3.h>  // header missed in mm.h
#include "mm.h"

#include "mmCalib.hh"

namespace na64dp {
namespace p348reco_handlers {

/// \brief Aux class maintaining synchronicity between planes calibrations and
///        set of planes objects
class PlanesCache : public MMCalibAdapter {
public:
    PlanesCache( calib::Dispatcher & cdsp
               , const std::string & mmPedestalSuffixPattern
               , const std::string & selStrExpr
               , log4cpp::Category & logCat
               , const std::string & detNamingClass="default"
               , const std::string & timingCalibClass="default"
               , const std::string & pedestalsCalibClass="default"
               , const std::string & wireLayoutCalibClass="default"
               )
            : MMCalibAdapter( cdsp, mmPedestalSuffixPattern, selStrExpr, logCat
                            , detNamingClass
                            , timingCalibClass
                            , pedestalsCalibClass
                            , wireLayoutCalibClass
                            )
            , _planesCachesValid(false)
            {}
    std::unordered_map<DetID, MicromegaPlane> & planes();
protected:
    /// (Re-)sets timing calibrations
    void handle_update(const Collection<APVTimingCalib> & c) override {
        _invalidate_planes_cache();
        MMCalibAdapter::handle_update(c);
    }
    /// (Re-)sets pedestal calibrations
    void handle_update(const Collection<APVChannelEntry> & c) override {
        _invalidate_planes_cache();
        MMCalibAdapter::handle_update(c);
    }
    /// (Re-)sets wire design entries
    void handle_update(const calib::MuMegaWiresDesignEntry & c) override {
        _invalidate_planes_cache();
        MMCalibAdapter::handle_update(c);
    }

    /// Called on calib update to set `_planesCachesValid` to `false`
    void _invalidate_planes_cache() { _planesCachesValid = false; }
private:
    /// `true` when `_planes` have valid calibs (ones from ``)
    bool _planesCachesValid;
    /// Plane data structs by IDs
    std::unordered_map<DetID, MicromegaPlane> _planes;

    /// Inits `_planes` wrt calibs provided by 
    void _recreate_planes();
};  // class PlanesCache

/**\brief A p348reco-based clusters creating handler for Micromegas
 *
 * This variant exploits clustering code written by ETH(Z) group in 2022-2023
 * providing much better quality and efficiency of clustering technique than
 * native NA64sw APV clustering algorithm.
 * */
class MakeMMCluster
        : public AbstractHitHandler<event::APVHit>
        , public PlanesCache
        {
public:
    /// Sets up handler to interface `p348reco` APV MM collecting structs
    MakeMMCluster( calib::Dispatcher & cdsp
                 , const std::string & mmPedestalSuffixPattern
                 , const std::string & sel
                 , log4cpp::Category & logCat
                 , bool backreferenceHits=true
                 , const std::string & detNamingClass="default"
                 , const std::string & timingCalibClass="default"
                 , const std::string & pedestalsCalibClass="default"
                 , const std::string & wireLayoutCalibClass="default"
                 ) : AbstractHitHandler<event::APVHit>(cdsp, sel, logCat, detNamingClass)
                   , PlanesCache( cdsp, mmPedestalSuffixPattern, sel, logCat
                                , detNamingClass
                                , timingCalibClass
                                , pedestalsCalibClass
                                , wireLayoutCalibClass
                                )
                   , _doBackrefHits(backreferenceHits)
                   {}
    /// Populates `strips` of p348reco struct for selected items
    bool process_hit(EventID, DetID, event::APVHit &) override;
    ///\brief Extens APV hits iteration with p348reco cluster collecting routines
    ///
    /// Forwards call to parent, once all APV hits are collected, invokes
    /// p348reco's `DoHitAccumulation()` and collects resulting clusters
    ProcRes process_event(event::Event &) override;
    /// Prints warning on missing calibrations
    void finalize() override;
private:
    /// Counter of hits for planes with missing calibration to warn in log
    std::unordered_map<DetID, size_t> _missingCalibs;
    ///\brief When set, clusters will be back-referenced to hit objects
    ///       from an event.
    ///
    /// Native p348reco does not keep this asscotiation directly, so some
    /// additional steps need to be performed to resolve backward match.
    const bool _doBackrefHits;
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

#endif  // (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

