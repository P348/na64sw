#pragma once

#if (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

#include "na64calib/dispatcher.hh"  // from framework's calib lib
#include <TVector3.h>  // header missed in mm.h
#include "mm.h"  // from p348reco
#include "na64common/calib/apvTimeType1.hh"  // from na64common
#include "na64common/calib/mm-design.hh"  // from na64common
#include "na64detID/TBName.hh"
#include "na64dp/abstractHitHandler.hh"  // for SelectiveHandlerMixin

namespace na64dp {
namespace p348reco_handlers {

using calib::Collection;
using calib::APVTimingCalib;
using calib::APVChannelEntry;

/**\brief Calibration data subscriber class maintaining set of
 *        p348reco's `MM_plane_calib` instances
 *
 * Subscribed to time, pedestal and wire design calibration updates. Will
 * modify set of maintained micromegas calibrations accordingly.
 *
 * \note layout entries are defined using selectors (rather than plane
 *       keys), so the class does not immediately apply design to
 *       calibrations, instead it gets resolved when timing/pedestals
 *       calibrations are provided (design should be loaded at this moment).
 *
 * \warning The problem is that there are some junk / technical files in those
 *          pedestal calibration directories. They apparently used by detector
 *          experts for detector maintenance. Even though our
 *          `GEMMonitorFilenameSemantics` provides some loose discrimination,
 *          those files still can sometimes get in here. Several "lines of
 *          defence" are foreseen in pedestal update function to prevent
 *          wrong data getting here.
 * */
class MMCalibAdapter : public util::Observable<Collection<APVTimingCalib>>::iObserver
                     , public util::Observable<Collection<APVChannelEntry>>::iObserver
                     , public util::Observable<calib::MuMegaWiresDesignEntry>::iObserver
                     , public SelectiveHandlerMixin
                     {
private:
    /// A subject: plane calibrations indexed by IDs
    std::unordered_map<DetID, MM_plane_calib> _planeCalibs;
    /// List of MM design data (with selectors)
    ///
    /// \todo clean on invalidation
    std::list<std::pair<DetSelect *, calib::MuMegaWiresDesignEntry>> _designs;
    /// Pattern for pedestal detector IDs
    ///
    /// Introduces to skip GMs and various junk often found in the calibration
    /// directories with pedestals.
    const std::regex _rxMMName;
protected:
    log4cpp::Category & _L;
    /// Creates entry if not exists, returns reference to plane calib instance
    MM_plane_calib & get_entry(DetID did);

    /// (Re-)sets timing calibrations
    void handle_update(const Collection<APVTimingCalib> &) override;
    /// (Re-)sets pedestal calibrations
    void handle_update(const Collection<APVChannelEntry> &) override;
    /// (Re-)sets wire design entries
    void handle_update(const calib::MuMegaWiresDesignEntry &) override;

    void _set_layout(DetID, MM_plane_calib &);
public:
    MMCalibAdapter( calib::Dispatcher & cdsp
                  , const std::string & mmPedestalSuffixPattern
                  , const std::string & selectionStrExpr
                  , log4cpp::Category & logCat
                  , const std::string & detNameClass="default"
                  , const std::string & timingCalibClass="default"
                  , const std::string & pedestalsCalibClass="default"
                  , const std::string & wireDesignClass="default"
                  );

    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::get(); }
    /// Read-only public getter for MM plane calibrations expressed
    /// as `p348reco` classes
    const std::unordered_map<DetID, MM_plane_calib> & plane_calibs()
        { return _planeCalibs; }

    /// Prints actual calibration values -- layouts, channel sigma, etc
    void dump_current_calibs(std::ostream &) const;
};

}  // namespace ::na64dp::p348reco_handlers
}  // namespace na64dp

#endif  // (defined(P348RECO_MM_LIB_FOUND) && P348RECO_MM_LIB_FOUND)

