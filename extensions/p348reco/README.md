# NA64sw adapters for p348reco

This extension module provides an adapter code for `p348reco` reconstruction
code for NA64sw's interfaces.

## Handlers

A few handlers based on `p348reco`'s code are available in this extension.

## Data source

P348reco can provide reconstructed event to NA64sw using data source class
implementation. It is not a "pure" event source (contrary to `source-ddd` for
instance) meaning that it converts event already being reconstructed by
`p348reco` code.

## Usage Note

Because `p348reco` has its calibration paths hardcoded, one have to run the
pipeline application from within the `p348-daq/p348reco/` dir.

## Supported detectors list

- MSADC detectors except for hodoscopes (ECAL, HCAL, SRD, BGO, LYSO, etc)
- Micromega clusters and track point(s), no raw data
- GEM clusters and its (single) track point, raw partially provided (some DAQ
IDs missing)

