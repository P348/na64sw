#include "source.hh"

#include "na64detID/TBName.hh"
#include "na64detID/TBNameErrors.hh"
#include "na64util/str-fmt.hh"

#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"
#include "na64event/data/event.hh"

#include <stdexcept>
#include <fstream>

namespace na64dp {
namespace mkmc {

using namespace event;

Source::Source( calib::Manager & mgr
              , const std::vector<std::string> & inputs
              , log4cpp::Category & logCat
              , const std::string & namingClass
              , iEvProcInfo * epi
              ) : AbstractNameCachedEventSource(mgr, logCat, namingClass, epi)
                , _paths(std::deque<std::string>(inputs.rbegin(), inputs.rend()))
                , _ifsPtr(nullptr)
                , _csbPtr(nullptr)
                , _isPtr(nullptr)
                , _fmtVer(0)  // files must provide this tag first
                , _lmem(nullptr)
                {
    _log.info( "MK/MC source initialized on %zu files.", inputs.size() );
    // Add callbacks for tags
    _readers["VERSION"]     = &Source::_read_version;
    _readers["RUN"]         = &Source::_read_run_metadata;
    _readers["EVENT"]       = &Source::_read_event_data;
    _readers["MCTRUTH"]     = &Source::_read_mctruth;
    _readers["DMTRUTH"]     = &Source::_read_dmtruth;
    _readers["ECAL"]        = &Source::_read_evdat_ECAL;
    _readers["HCAL"]        = &Source::_read_evdat_HCAL;
    _readers["SRD"]         = &Source::_read_evdat_SRD;
    _readers["WCAL"]        = &Source::_read_evdat_WCAL;
    _readers["VETO"]        = &Source::_read_evdat_VETO;
    _readers["VTWC"]        = &Source::_read_evdat_VTWC;
    _readers["VTEC"]        = &Source::_read_evdat_VTEC;
    _readers["S2EE"]        = &Source::_read_evdat_S2EE;  // same for S2/S2EE
    _readers["S2"]          = &Source::_read_evdat_S2EE;  // same for S2/S2EE
    _readers["WCOUNTERS"]   = &Source::_read_evdat_WCOUNTERS;
    _readers["ZDCAL"]       = &Source::_read_evdat_ZDCAL;
    _readers["VHCAL"]       = &Source::_read_evdat_VHCAL;
    _readers["MM"]          = &Source::_read_evdat_MM;
    _readers["STRAWTUBE"]   = &Source::_read_evdat_STRAWTUBE;
    _readers["STRAW"]       = &Source::_read_evdat_STRAW;
    _readers["ENDEVENT"]    = &Source::_read_endevent;
}

bool
Source::_open_next_stream() {
    // Keep getting next path from stack until opened if no file associated and
    // paths stack is not empty
    while( (!_ifsPtr) && !_paths.empty() ) {
        std::ifstream * ifs = new std::ifstream( _paths.top() );
        if( ! ifs->is_open() ) {
            _log.error( "Error opening file \"%s\", skip.", _paths.top().c_str() );
            // ^^^ TODO: details
            delete ifs;
        } else {
            _log.info( "Reading MK/MC file \"%s\"", _paths.top().c_str() );
            _ifsPtr = ifs;
            _ifName = _paths.top();
        }
        _paths.pop();
    }
    if( _paths.empty() && !_ifsPtr ) {
        _log.info( "MK/MC source depleted." );
        _lmem = nullptr;
        return false;
    }

    _csbPtr = new util::CountingStreamBuffer( _ifsPtr->rdbuf() );
    _isPtr = new std::istream( _csbPtr );
    return true;
}

void
Source::_skip_rest_of_the_line() {
    // skip the rest of line (format implies some reserver or meaningless
    // digits on the remainder of the line consumed by tag parser)
    _isPtr->ignore( std::numeric_limits<std::streamsize>::max()
                  , '\n');
}

bool
Source::read( Event & evRef, event::LocalMemory & lmem ) {
    _lmem = &lmem;

    if( (!_isPtr) && ! _open_next_stream() ) return false;  // no files available

    assert(!evRef.mcTruth);  // event is not clean
    evRef.mcTruth = lmem.create<event::MCTruthInfo>();
    assert(evRef.mcTruth);  // mcti is not allocated
    
    assert( evRef.sadcHits.empty() );

    assert(_isPtr);  // null stream ptr
    std::istream & is = *_isPtr;
    std::string tag;
    size_t lcol = 0, lline = 0;
    bool isGood = is.good();
    while(getline(is, tag)) {
        util::trim(tag);
        if( tag.empty() ) continue;
        if( "ENDEVENT" == tag ) break;
        auto readerIt = _readers.find(tag);
        if( _readers.end() == readerIt ) {
            _lmem = nullptr;
            char errbf[256];
            snprintf( errbf, sizeof(errbf)
                    , "%s:%zu,%zu: unknown MK/MC tag \"%s\""
                    , _ifName.c_str(), lline, lcol
                    , tag.c_str()
                    );
            throw std::runtime_error(errbf);  // TODO unspported tag spec exception
        } 

        if( !(this->*(readerIt->second))(is, evRef) ) {
            _lmem = nullptr;
            char errbf[256];
            snprintf( errbf, sizeof(errbf)
                    , "%s:%zu,%zu: failed to parse tag \"%s\""
                    , _ifName.c_str(), lline, lcol
                    , tag.c_str()
                    );
            throw std::runtime_error(errbf);  // TODO tagged parsing failure spec exception
        } else {
            _log.debug( "Tag \"%s\" parsed.", tag.c_str() );
        }

        lline = _csbPtr->line_no();
        lcol = _csbPtr->column();
    }
  
    #if 0
    // check file state
    //std::cout << "eof()=" << is.eof() << " fail()=" << is.fail() << endl;
    if (is.eof()) {
        mct.reset();
    } else if (is.fail()) {
        throw std::runtime_error("MC file IO or format error");
    }
    #endif
    _lmem = nullptr;
    return isGood;
}

//                          * * *   * * *   * * *

bool
Source::_read_version( std::istream & is, Event &) {
    is >> _fmtVer;
    if (_fmtVer < 3 || _fmtVer > 8) {
        _log.error( "Format version tag %d is not supported.", _fmtVer );
        throw std::runtime_error("Unsupported MC file format version.");  // TODO: custom exc type
    }
    return true;
}

bool
Source::_read_run_metadata( std::istream & is, Event &) {
    // read file line by line
    std::string line;
    while (getline(is, line)) {
        if(line == "ENDRUN") break;
        // section properties
        const size_t eqpos = line.find("=");
        if (eqpos != std::string::npos) {
            // if there is '=' sign in line, this is a property string: key=value
            const std::string key = line.substr(0, eqpos);
            const std::string value = line.substr(eqpos + 1);
            _log.debug( "  run info entry: \"%s\"=\"%s\"", key.c_str(), value.c_str() );
            _metadata[key] = value;
        }
    }
    return true;
}

bool
Source::_read_event_data(std::istream & is, Event & evRef) {
    event::MCTruthInfo & mct = *evRef.mcTruth;
    int IEvent, ITrig;
    is >> IEvent >> ITrig >> mct.weight;
    if( mct.weight == 0. ) {
        mct.weight = 1.;
    }
    evRef.id = EventID( 1  // MC run NO
                      , 1  // MC spill NO
                      , IEvent // event number
                      );
    // This may cause updates on all calibration subscribers including
    // the current data source object:
    calib_manager().event_id(evRef.id, {0, 0});  // TODO configurable mock time
    _log.debug("Event " + evRef.id.to_str() + " reading started");
    return true;
}

bool
Source::_read_mctruth(std::istream & is, Event & evRef) {
    event::MCTruthInfo & mct = *evRef.mcTruth;
    is >> mct.ecalEntryPoisition[0]
       >> mct.ecalEntryPoisition[1]
       // mct.ecalEntryPoisition[2]  // format doesn't provide it
       ;
    if(_fmtVer >= 4) {
        is >> mct.beamPosition[0] >> mct.beamPosition[1] >> mct.beamPosition[2]
           >> mct.beamPDG
           >> mct.beamE0
           >> mct.beamMomentum[0] >> mct.beamMomentum[1] >> mct.beamMomentum[2]
           ;
    }
    return true;
}

bool
Source::_read_dmtruth(std::istream & is, Event & evRef) {
    if(_fmtVer < 8) {
        double ea, adx, ady, adz, aa;
        is >> ea >> adx >> ady >> adz >> aa;
        // ^^^ wut is this, Misha?
    } else {
        auto & v = _new_entry<Vertex>(evRef.vertices.size(), evRef, "Vertex");
        is >> v.E0
           >> v.initPDG
           >> v.E
           >> v.position[0] >> v.position[1] >> v.position[2]  // production
           >> v.decayPosition[0] >> v.decayPosition[1] >> v.decayPosition[2]  // decay
           >> v.lvEm[0] >> v.lvEm[1] >> v.lvEm[2]  // e- decay Lorentz vec components
           >> v.lvEp[0] >> v.lvEp[1] >> v.lvEp[2]  // e+ decay Lorentz vec components
           >> v.DMTRID1 >> v.DMTRID2  // wut is this, Misha?
           ;
    }
    return true;
}

bool
Source::_read_evdat_ECAL(std::istream & is, Event & evRef) {
    DetID did(naming().kSADC, naming().kECAL);
    int NCellsX = _metadata["ECAL:NCellsECALX"].as<int>(6);
    for(int imod=0; imod < 2; imod++) { // Preshower (imod=0), Main ECAL (imod=1)
        did.number(0);  // both parts has #0, this is done intentionally
        //did.number(imod);  // instead of this
        CellID cid;
        cid.set_z( 0 == imod ? 0 : 1 );  // this is special for ECAL: duplicates prs/rest
        for(int iy=0; iy < 6; iy++) { // todo: isn't it affected by ECAL:NCellsECALX?
            cid.set_y(iy);  // set y-index
            for(int ix=0; ix < NCellsX; ix++) {
                cid.set_x(ix);  // set x-index
                did.payload(cid.cellID);
                // emplace hit and assign the energy deposition (it is
                // only value provided by MC)
                auto & hit = _new_entry<SADCHit>(did, evRef, "ECAL");
                is >> hit.eDep;
            }
        }
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_HCAL(std::istream & is, Event & evRef) {
    DetID did(naming().kSADC, naming().kHCAL);
    for(int imod=0; imod < 4; imod++) { // HCAL
        did.number(imod);
        CellID cid;
        for(int iy=0; iy < 3; iy++) {
            cid.set_y(iy);
            for(int ix=0; ix < 3; ix++) {
                cid.set_x(ix);
                did.payload(cid.cellID);
                auto & hit = _new_entry<SADCHit>(did, evRef, "HCAL");
                is >> hit.eDep;
            }
        }
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_SRD(std::istream & is, Event & evRef) {
    DetID did( naming().kSADC, naming().kSRD
             , 0
             , CellID(0, 0, 0).cellID
             );
    // note: experiment SRD nmodules = 4 starting from August 2021
    const int nmod = _metadata["SRD:NModules"].as<int>(3);
    CellID cid;
    for(int i = 0; i < nmod; ++i) {
        did.number(i);
        auto & hit = _new_entry<SADCHit>(did, evRef, "SRD");
        is >> hit.eDep;
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_WCAL(std::istream & is, Event & evRef) {
    DetID wcalDid( naming().kSADC, naming().kWCAL
                 , 0
                 , CellID(0, 0, 0).cellID
                 )
        , wcatDid( naming().kSADC, naming().kWCAT
                 , 0
                 , CellID(0, 0, 0).cellID
                 );

    if(_fmtVer < 5) {
        wcalDid.number(1);
        auto & hit = _new_entry<SADCHit>(wcalDid, evRef, "WCAL, #0");
        is >> hit.eDep;
    } else {
        wcalDid.number(1);
        auto & wcal1Hit = _new_entry<SADCHit>(wcalDid, evRef, "WCAL, #1");
        wcalDid.number(0);
        auto & wcal0Hit = _new_entry<SADCHit>(wcalDid, evRef, "WCAL, #2");

        is >> wcal1Hit.eDep
           >> _new_entry<SADCHit>(wcatDid, evRef, "WCAT").eDep
           >> wcal0Hit.eDep;

        wcal1Hit.eDep -= wcal0Hit.eDep;
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_VETO(std::istream & is, Event & evRef) {
    //is >> e.VETO[0].energy >> e.VETO[2].energy >> e.VETO[4].energy;
    //e.VETO[1].energy = e.VETO[0].energy;
    //e.VETO[3].energy = e.VETO[2].energy;
    //e.VETO[5].energy = e.VETO[4].energy;
    // ^^^ Units in MC are now GeV
    DetID did(naming().kSADC, naming().kVETO);
    for(int i = 0; i < 3; ++i) {
        did.number(2*i);
        // this is to keep the payload set, since SRD seems to not have
        // any fine segmentation
        CellID cid(0, 0, 0);
        did.payload(cid.cellID);
        auto & vetoHit = _new_entry<SADCHit>(did, evRef, "VETO, #1");
        is >> vetoHit.eDep;
        did.number(2*i+1);
        _new_entry<SADCHit>(did, evRef, "VETO, #2").eDep = vetoHit.eDep;
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_VTWC(std::istream & is, Event & evRef) {
    // assemble det ID
    DetID did( naming().kSADC  // set chip type to SADC
             , naming().kVTWC  // set kin to VTWC
             , 0  // set station # to 0
             , CellID(0, 0, 0).cellID  // set payload to 0-0-0
             );
    // read edep
    is >> _new_entry<SADCHit>(did, evRef, "VTWC").eDep;
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_VTEC(std::istream & is, Event & evRef) {
    // assemble det ID
    DetID did( naming().kSADC  // set chip type to SADC
             , naming().kVTEC  // set kin to VTEC
             , 0  // set station # to 0
             , CellID(0, 0, 0).cellID  // set payload to 0-0-0
             );
    // read edep
    is >> _new_entry<SADCHit>(did, evRef, "VTEC").eDep;
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_S2EE(std::istream & is, Event & evRef) {
    DetID did( naming().kSADC  // set chip type to SADC
             , naming().kS  // set kin to S
             , 2  // set station # to 2
             , CellID(0, 0, 0).cellID  // set payload to 0-0-0
             );
    is >> _new_entry<SADCHit>(did, evRef, "S2EE").eDep;
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_WCOUNTERS(std::istream & is, Event & evRef) {
    DetID didV2( naming().kSADC  // set chip type to SADC
               , naming().kV  // set kin to V
               , 2  // set station # to 2
               , CellID(0, 0, 0).cellID  // set payload to 0-0-0
               )
        , didS4( naming().kSADC  // set chip type to SADC
               , naming().kS  // set kin to S
               , 4  // set station # to 4
               , CellID(0, 0, 0).cellID  // set payload to 0-0-0
               )
        ;
    auto v2hitRef = _new_entry<SADCHit>(didV2, evRef, "V2")
       , s4hitRef = _new_entry<SADCHit>(didS4, evRef, "S4")
       ;

    if( _fmtVer < 7
     || _metadata["SetupNumber"].as<int>(-1) == 18) {
        DetID didDM( naming().kSADC  // set chip type to SADC
                   , naming().kDM  // set kin to DM
                   , 0  // set station # to 0
                   , CellID(0, 0, 0).cellID  // set payload to 0-0-0
                   );
        auto & hitDM0 = _new_entry<SADCHit>(didDM, evRef, "DM0");
        is >> v2hitRef.eDep
           >> hitDM0.eDep
           >> s4hitRef.eDep
           ;
        didDM.number(1);
        _new_entry<SADCHit>(didDM, evRef, "DM").eDep = hitDM0.eDep;
    } else {
        DetID didWCAL2( naming().kSADC  // set chip type to SADC
                      , naming().kWCAL  // set kin to WCAL
                      , 2  // set station # to 2
                      , CellID(0, 0, 0).cellID  // set payload to 0-0-0
                      );
        auto & hit = _new_entry<SADCHit>(didWCAL2, evRef, "WCAL2");
        is >> hit.eDep
           >> v2hitRef.eDep
           >> s4hitRef.eDep
           ;
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_ZDCAL(std::istream & is, Event & evRef) {
    DetID did( naming().kSADC  // set chip type to SADC
             , naming().kZDCAL  // set kin to ZDCAL
             , 0  // set station # to 0
             , CellID(0, 0, 0).cellID  // set payload to 0-0-0
             );

    for(int i = 0; i < 2; ++i) {
        did.number(i);
        is >> _new_entry<SADCHit>(did, evRef, "ZDCAL").eDep;
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_VHCAL(std::istream & is, Event & evRef) {
    DetID did( naming().kSADC  // set chip type to SADC
             , naming().kVHCAL  // set kin to VHCAL
             , 0  // set station # to 0
             , CellID(0, 0, 0).cellID  // set payload to 0-0-0
             );
    CellID cid;
    for(int iy=0; iy < 4; iy++) {
        cid.set_y(iy);
        for(int ix=0; ix < 4; ix++) {
            cid.set_x(ix);
            did.payload(cid.cellID);
            is >> _new_entry<SADCHit>(did, evRef, "VHCAL").eDep;
        }
    }
    _skip_rest_of_the_line();
    return true;
}

bool
Source::_read_evdat_MM(std::istream & is, Event & evRef) {
    int nHits[8];
    int nStations = _fmtVer < 8 ? 4 : 8;
    for( int i = 0; i < nStations; ++i ) {
        is >> nHits[i];
    }
    DetID did( naming().kAPV  // chip is APV for all MM
             , naming().kMM  // kin is MM for all MM
             , 0  // station number will vary
             , 0x0  // no projection/wire number is set for track point
             );

    double xxx;
    if(_fmtVer < 5) {
        // formats before #5 provide only X/Y of hit on 4 micromegas
        for(int i = 0; i < 4; ++i) {
            for(int j = 0; j < nHits[i]; ++j) {
                did.number(i);
                auto & tp = _add_entry<TrackScore>(did, evRef, "MM, #0");
                //is >> tp.gR[0] >> tp.gR[1];
                is >> tp.lR[0] >> tp.lR[1];
            }
        }
    } else if(_fmtVer == 5)  {
        // format #5 provides x,y of hit, energy deposition, [eV]
        for(int i = 0; i < 4; ++i) {
            for(int ihit=0; ihit < nHits[i]; ihit++) {
                did.number(i);
                auto & tp = _add_entry<TrackScore>(did, evRef, "MM, #1");
                is //>> tp.gR[0] >> tp.gR[1]
                   >> tp.lR[0] >> tp.lR[1]
                   >> xxx >> xxx  // wut is this?
                   >> tp.eDep
                   ;
            }
        }
    } else if(_fmtVer == 6 || _fmtVer == 7)  {
        // 4 MuMegas of the form:
        // - hit position X,Y,Z [mm]
        // - hit energy deposition [eV]
        // - hit particle PDG ID code
        // - geant4 track ID
        // - track kinetic energy [GeV]
        int pdg;  // TODO: unused
        for(int i = 0; i < 4; ++i) {
            for(int ihit=0; ihit < nHits[i]; ihit++) {
                did.number(i);
                auto & mcTP = _add_entry<TrackScore>(did, evRef, "MM, #2");
                mcTP.mcTruth = _lmem->create<event::MCTrueTrackScore>(*_lmem);
                util::reset(*mcTP.mcTruth);
                is //>> mcTP.gR[0] >> mcTP.gR[1] >> mcTP.gR[2]  // hit position, mm
                   >> mcTP.lR[0] >> mcTP.lR[1] >> mcTP.lR[2]
                   >> mcTP.eDep  // hit energy deposition, eV
                   //>> mcTP.particlePDG  // hit particle PDG
                   >> pdg
                   >> mcTP.mcTruth->geant4TrackID  // Geant4 track ID
                   >> mcTP.trackE  // track kinetic energy, GeV
                   ;
                // Recalculate units
                // - length to cm
                mcTP.lR[0] /= 10;
                mcTP.lR[1] /= 10;
                mcTP.lR[2] /= 10;
                // - energy to MeV
                mcTP.eDep /= 1e3;
                mcTP.trackE *= 1e3;
            }
        }
    } else if(_fmtVer >= 8)  {
        // 4 MuMegas + 4 GEMs of the form
        // - hit position X,Y,Z [mm]
        // - hit energy deposition [eV]
        // - hit particle PDG ID code
        // - geant4 track ID
        // - track kinetic energy [GeV]
        for(int i = 0; i < 8; ++i) {
            if( i < 4 ) {
                did.number(i);
            } else {
                did.number(i - 4);
                did.kin(naming().kGEM);
            }
            int pdg;   // TODO: unused
            for(int ihit=0; ihit < nHits[i]; ihit++) {
                auto & mcTP = _add_entry<TrackScore>(did, evRef, "MM/GM");

                mcTP.mcTruth = _lmem->create<event::MCTrueTrackScore>(*_lmem);
                util::reset(*mcTP.mcTruth);

                is //>> mcTP.gR[0] >> mcTP.gR[1] >> mcTP.gR[2]  // hit position, mm
                   >> mcTP.lR[0] >> mcTP.lR[1] >> mcTP.lR[2]  // hit position, mm
                   >> mcTP.eDep  // hit energy deposition, eV
                   //>> mcTP.particlePDG  // hit particle PDG
                   >> pdg
                   >> mcTP.mcTruth->geant4TrackID  // Geant4 track ID
                   >> mcTP.trackE  // track kinetic energy, GeV
                   ;
                // Recalculate units
                // - length to cm
                mcTP.lR[0] /= 10;
                mcTP.lR[1] /= 10;
                mcTP.lR[2] /= 10;
                // - energy to MeV
                mcTP.eDep /= 1e3;
                mcTP.trackE *= 1e3;
            }
        }
    }
    return true;
}

bool
Source::_read_evdat_STRAWTUBE(std::istream & is, Event & evRef) {
    int nHits[3];
    is >> nHits[0] >> nHits[1] >> nHits[2];
    for(int i = 0; i < 3; ++i) {
        // For straw station #i
        DetID did( naming().kStwTDC
                 , naming().kSt
                 , i
                 , 0x0 );
        int pdg;  // TODO: unused
        for(int ihit=0; ihit < nHits[i]; ihit++) {
            auto & mcTP = _add_entry<TrackScore>(did, evRef, "MM, #0");
            mcTP.mcTruth = _lmem->create<event::MCTrueTrackScore>(*_lmem);
            util::reset(*mcTP.mcTruth);
            is //>> mcTP.gR[0] >> mcTP.gR[1] >> mcTP.gR[2]  // hit position, mm
               >> mcTP.lR[0] >> mcTP.lR[1] >> mcTP.lR[2]  // hit position, mm
               >> mcTP.eDep  // hit energy deposition, eV
               //>> mcTP.particlePDG  // hit particle PDG
               >> pdg
               >> mcTP.mcTruth->geant4TrackID  // Geant4 track ID
               >> mcTP.trackE  // track kinetic energy, GeV
               >> mcTP.time  // global hit time
               ;
            // Recalculate units
            // - length to cm
            mcTP.lR[0] /= 10;
            mcTP.lR[1] /= 10;
            mcTP.lR[2] /= 10;
            // - energy to MeV
            mcTP.eDep /= 1e3;
            mcTP.trackE *= 1e3;
        }
    }
    return true;
}

bool
Source::_read_evdat_STRAW(std::istream & is, Event & evRef) {
    int nHits;
    is >> nHits;
    for( int i = 0; i < nHits; ++i ) {
        // Format is:
        //      station#, plane#, wire#  => DetID
        //      start{x,y,z} - start coordinates  => StrawMCTruth/entryPoint
        //      PDG - PDG particle identifier => StrawMCTruth/exitPoint
        //      trackID - track ID  => g4TrackID
        //      EPart - particle energy  => trackE
        //      EDep - energy deposition => eDep
        //      strawCProj - 0 or 1 depending on whether projection is X or Y
        //                   (duplicates projection info) => strawMCTruth/???
        //      distance - distance to the particle track (R) => strawHit/R
        //      cwire[cproj](?) - local coordinates of wire (coordinate measured by plane) => strawHit/lR[0]
        //      [end{x,y,z} - end coordinates] => StrawMCTruth/exitPoint
        // see: https://gitlab.cern.ch/P348/na64-simulation/-/blob/master/geant4/simulation/src/Core/NA64SD.cc#L872
        int stationNo, planeNo;
        is >> stationNo >> planeNo;
        DetID did( naming().kStwTDC, naming().kSt, stationNo, 0x0 );
        WireID wid;
        // Quoting comment from MKMC:
        //      Geometry of Straw Station consists of two planes(X (0 degree,
        //      perpendicular to beam axis) and Y(90 degree))
        //      Eache plane consists of two layers (u and d) shifted on radius
        //      Straw tube.
        // => it seems that plane# encodes the layer number. We then presume
        //    the following:
        APVPlaneID::Projection prj;
        switch(planeNo) {
            case(1): prj = APVPlaneID::kX; break;
            case(2): prj = APVPlaneID::kU; break;
            case(3): prj = APVPlaneID::kY; break;
            case(4): prj = APVPlaneID::kV; break;
        };
        wid.proj(prj);
        int wireNo;
        is >> wireNo;
        wid.wire_no(wireNo);
        did.payload(wid.id);
        int auxProjIdx;
        // Insert new hit:
        event::StwTDCHit & stwTDCHit = _new_entry<event::StwTDCHit>(did, evRef, "STRAW");
        stwTDCHit.mcInfo = _lmem->create<StwMCInfo>(*_lmem);
        is >> stwTDCHit.mcInfo->entryPoint[0]
           >> stwTDCHit.mcInfo->entryPoint[1]
           >> stwTDCHit.mcInfo->entryPoint[2]
           ;
        is >> stwTDCHit.mcInfo->particlePDG
           >> stwTDCHit.mcInfo->geant4TrackID
           >> stwTDCHit.mcInfo->trackE
           >> stwTDCHit.mcInfo->eDep
           >> auxProjIdx
           >> stwTDCHit.mcInfo->r
           >> stwTDCHit.mcInfo->u
           ;
        assert( ((prj == APVPlaneID::kX || prj == APVPlaneID::kU) && auxProjIdx == 0)
             || ((prj == APVPlaneID::kY || prj == APVPlaneID::kV) && auxProjIdx == 1) );
        #if 0
        if( _metadata["???"] == 2 ) {  // ModeOutputStraw?
            is >> stwTDCHit.mcInfo.exitPoint[0]
               >> stwTDCHit.mcInfo.exitPoint[1]
               >> stwTDCHit.mcInfo.exitPoint[2]
               ;
        }
        #endif
    }

    return true;
}

bool
Source::_read_endevent(std::istream & is, Event & evRef) {
    // ...
    return true;
}

}  // namespace mkmc
}  // namespace na64dp

using na64dp::mkmc::Source;
REGISTER_SOURCE( MKMC
               , calibManager
               , cfg
               , ids
               , "Event data source of NA64 official MC (ASCII)."
               ) {
    Source * src;
    log4cpp::Category & logCat
        = log4cpp::Category::getInstance(cfg["_log"] ? cfg["_log"].as<std::string>() : "source");
    try {
        src = new Source( calibManager
                        , ids
                        , logCat
                        , cfg["namingScheme"] ? cfg["namingScheme"].as<std::string>() : "default"
                        //, epi
                        );
    } catch( std::exception & e ) {
        log4cpp::Category::getInstance("sources").error( "Unable to instantiate"
                " MKMC data source object." // with arguments:"
                );
        throw;
    }
    log4cpp::Category::getInstance("sources").info( "Instantiated"
                " MKMC data source object." // with arguments:"
                );
    return src;
}

