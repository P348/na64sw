#pragma once

#include "na64sw-config.h"
#include "na64dp/abstractEventSource.hh"
#include "na64event/data/event.hh"
#include "na64util/counting-streambuf.hh"

#include <yaml-cpp/yaml.h>
#include <stack>

namespace na64dp {
namespace mkmc {

using event::Event;

/**\brief An event source for NA64 official MC
 *
 * Official MC package for NA64 produces plain ASCII files with events data.
 * This event source interface implementation performs event reading of one
 * or multiple such files.
 *
 * \todo Verify units; we use [MeV], [cm]
 * \todo What is WCAT?
 * \todo What is DM?
 * \todo support for cin/named pipes
 * */
class Source : public AbstractNameCachedEventSource {
protected:
    /// List of file paths to iterate
    std::stack<std::string> _paths;

    /// Ptr to currently opened file
    std::ifstream * _ifsPtr;
    /// Ptr to helper streambuf
    util::CountingStreamBuffer * _csbPtr;
    /// Ptr to currently active stream
    std::istream * _isPtr;
    /// Name of the file currently being read
    std::string _ifName;

    /// Format version
    int _fmtVer;
    /// MC run metadata
    YAML::Node _metadata;
    /// Event structure allocator instance
    event::LocalMemory * _lmem;

    typedef bool (Source::*Reader)(std::istream &, Event &);
    std::unordered_map<std::string, Reader> _readers;

    /// `VERSION` tag parser; version affects many other callbacks
    bool _read_version(std::istream &, Event &);
    /// `RUN` tag parser; reads run metainformation that affects other
    /// callbacks behaviour (normalization, segmentation, etc)
    bool _read_run_metadata(std::istream &, Event &);
    /// `EVENT` tag parser
    bool _read_event_data(std::istream &, Event &);
    /// `MCTRUTH` tag parser
    bool _read_mctruth(std::istream &, Event &);
    /// `DMTRUTH` tag parser
    bool _read_dmtruth(std::istream &, Event &);

    /// `ECAL` tag parser; electromagnetic calorimeter data
    /// 
    /// X-segmentation is defined by  ECAL:NCellsECALX run metainfo, y is always 6.
    bool _read_evdat_ECAL(std::istream &, Event &);
    /// `HCAL` tag parser; hadronic calorimeter
    ///
    /// 4 modules of 3x3 SADC detector
    bool _read_evdat_HCAL(std::istream &, Event &);
    /// `SRD` tag parser
    ///
    /// Number of modules defined by `SRD:NModules` run metainfo
    bool _read_evdat_SRD(std::istream &, Event &);
    /// `WCAL` tag parser
    ///
    /// For version <5 supplies only 1st module energy deposition, afterwards
    /// provides energy for 1st and 2nd modules + WCAT (?) energy
    /// \note The 2nd module is also written by from within `WCOUNTERS` tag
    bool _read_evdat_WCAL(std::istream &, Event &);
    /// `VETO` tag parser
    ///
    /// Provides energy deposition in 3 individual modules that assumed be the
    /// same of other 3: (0, 1), (2, 3), (4, 5).
    bool _read_evdat_VETO(std::istream &, Event &);
    /// `VTWC` tag parser
    bool _read_evdat_VTWC(std::istream &, Event &);
    /// `VTEC` tag parser
    bool _read_evdat_VTEC(std::istream &, Event &);
    /// `S2EE` tag parser
    bool _read_evdat_S2EE(std::istream &, Event &);
    /// `WCOUNTERS` tag parser; misc MC counters
    bool _read_evdat_WCOUNTERS(std::istream &, Event &);
    /// `ZDCAL` tag parser; zero-degree calorimeter
    bool _read_evdat_ZDCAL(std::istream &, Event &);
    /// `VHCAL` tag parser; veto hadronic calorimeter
    bool _read_evdat_VHCAL(std::istream &, Event &);
    /// `MM` tag parser; micromega detector
    bool _read_evdat_MM(std::istream &, Event &);
    /// `STRAWTUBE` tag parser; straw detectors
    bool _read_evdat_STRAWTUBE(std::istream &, Event &);
    /// `STRAW` tag parser; straw detectors (v)
    bool _read_evdat_STRAW(std::istream &, Event &);
    /// `ENDEVENT` tag parser (assures event is closed)
    bool _read_endevent(std::istream &, Event &);
private:
    bool _open_next_stream();
    void _skip_rest_of_the_line();
protected:
    /// Tries to add a hit or field in collection within the event, raises
    /// runtime error on detector ID collision, returns reference to newly
    /// added entry.
    template<typename T>
    T & _new_entry( typename event::Association<Event, T>::Collection::key_type key
                  , Event & evRef
                  , const char * nm
                  ) {
        auto ir = event::Association<Event, T>::map(evRef)
                    .emplace( key
                            , _lmem->create<T>(*_lmem) );
        if( ! ir.second ) {
            char bf[128];
            snprintf(bf, sizeof(bf), "Hit insertion failed (%s);"
                    " hit is not unique.", nm );
            // TODO: more details (detID to name or something...)
            throw std::runtime_error(bf);
        }
        assert(ir.first->second);  // hit is not allocated (allocators error)
        T & hit = *ir.first->second;
        util::reset(hit);
        return hit;
    }
    /// Tries to add a hit or field in collection within the event, permitting
    /// collisions
    template<typename T>
    T & _add_entry( typename event::Association<Event, T>::Collection::key_type key
                  , Event & evRef
                  , const char * nm
                  ) {
            auto it = event::Association<Event, T>::map(evRef)
                        .emplace( key
                                , _lmem->create<T>(*_lmem) );
            assert(it->second);  // hit is not allocated (allocators error)
            T & hit = *it->second;
            util::reset(hit);
            return hit;
    }
public:
    ///
    Source( calib::Manager & mgr
          , const std::vector<std::string> & inputs
          , log4cpp::Category & logCat
          , const std::string & detNamingClass="default"
          , iEvProcInfo * epi=nullptr
          );
    /// Interface implementation: reads an event from current file.
    virtual bool read( Event &, event::LocalMemory & lmem ) override;
};

}  // namespace mkmc
}  // namespace na64dp
