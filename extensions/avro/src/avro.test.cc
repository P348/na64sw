#include "avro-event-streams.hh"
#include "na64event/data/track.hh"
#include "na64utest/testing-fixture.hh"

#include <fstream>
#include <cmath>
#include <gtest/gtest.h>
#include <variant>

namespace na64dp {
namespace test {

class TestAvroEventSerialization
        : public BasicEventDataOperations {
protected:
    char _avroMemorybuffer[5*1024];
    avro_reader_t _avReader;
    avro_writer_t _avWriter;
    std::string _schemaStrJSON;

    void SetUp() override {
        const char * schemaLocPath = getenv("NA64SW_AVRO_EVENT_SCHEMA");
        if(!schemaLocPath) {
            NA64DP_RUNTIME_ERROR("NA64SW_AVRO_EVENT_SCHEMA environment variable is"
                    " not set, can not initialize testing fixture.");
        }
        std::ifstream ifs(schemaLocPath);
        std::stringstream buffer;
        buffer << ifs.rdbuf();
        _schemaStrJSON = buffer.str();
    }

    void TearDown() override {
        BasicEventDataOperations::TearDown();
        // ...
    }
    // ...
};

// Trivial test, checks (de)serialization of event with only 1st level
// attribute(s) set
TEST_F(TestAvroEventSerialization, SerializeEmptyEvent) {
    _reallocate_event_buffer(5*1024);

    event::Event eOut(this->lmem());
    {
        util::reset(eOut);
        event::avro::OutStream os(_schemaStrJSON
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        eOut.id = EventID(1000, 100, 123);
        os << eOut;
    }

    event::Event eIn(this->lmem());
    {
        event::avro::InStream is(_schemaStrJSON, &lmem()
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        is >> eIn;
    }
    
    EXPECT_EQ(eIn.id, eOut.id)
        << "Empty event's attribute deserialized with wrong event ID"
        << eOut.id << " instead of " << eIn.id;
    EXPECT_FALSE(eOut.mcTruth);
    EXPECT_TRUE(eOut.sadcHits.empty());
}

// A bit elaborated test checking basic hit collections within an event being
// (de)serialized
TEST_F(TestAvroEventSerialization, SerializeEventWithSimpleCollectionsElements) {
    _reallocate_event_buffer(5*1024);
    event::Event eOut(this->lmem()); {
        util::reset(eOut);

        eOut.id = EventID(3456, 123, 321);

        mem::Ref<event::SADCHit> hitRef = lmem().create<event::SADCHit>(lmem());
        util::reset(*hitRef);

        hitRef->eDep = 1.23;
        hitRef->rawData = lmem().create<event::RawDataSADC>(lmem());
        util::reset(*hitRef->rawData);
        hitRef->rawData->sum = 3.21;

        eOut.sadcHits.emplace(DetID(0x23), hitRef);

        event::avro::OutStream os(_schemaStrJSON
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        os << eOut;
    }

    event::Event eIn(this->lmem());
    {
        event::avro::InStream is(_schemaStrJSON, &lmem()
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        is >> eIn;
    }

    EXPECT_EQ(eIn.id, eOut.id)
        << "Non-empty event's attribute deserialized with wrong event ID"
        << eIn.id << " instead of " << eOut.id;
    ASSERT_EQ(eIn.sadcHits.size(), 1)
        << "Event lost single collection item.";
    ASSERT_TRUE(eIn.sadcHits.begin()->second)
        << "Collection item deserialized to empty object.";
    mem::Ref<event::SADCHit> sadcHit = eIn.sadcHits.begin()->second;
    EXPECT_FALSE(std::isnan(sadcHit->eDep))
        << "Collection item deserialized with wrong value"
        " (NaN instead of numerical value)";
    EXPECT_NEAR(sadcHit->eDep, 1.23, 1e-3)
        << "Collection item deserialized with wrong value"
        " (" << sadcHit->eDep << " instead of 1.23)";
    EXPECT_TRUE(sadcHit->rawData)
        << "An item associated with collection item lost.";
    EXPECT_NEAR(sadcHit->rawData->sum, 3.21, 1e-3)
        << "Attribute of item associated with collection item has error (wrong value).";
    EXPECT_TRUE(std::isnan(sadcHit->rawData->maxAmp))
        << "Attribute of item associated with collection item has error (set).";
}

// Elaborated test assuring linked list/union types of hits collection get
// (de)serialized
TEST_F(TestAvroEventSerialization, SerializeEventWithUnionCollectionsElements) {
    _reallocate_event_buffer(5*1024);
    event::Event eOut(this->lmem()); {
        util::reset(eOut);

        eOut.id = EventID(1, 1, 1);

        // create couple of hits of distinct types
        auto apvCluster = lmem().create<event::APVCluster>(lmem());
        util::reset(*apvCluster);

        auto caloHit = lmem().create<event::CaloHit>(lmem());
        util::reset(*caloHit);

        // create unions (for instance, scores):
        auto scoreRef1 = lmem().create<event::TrackScore>(lmem());
        util::reset(*scoreRef1);
        auto scoreRef2 = lmem().create<event::TrackScore>(lmem());
        util::reset(*scoreRef2);
        auto scoreRef3 = lmem().create<event::TrackScore>(lmem());
        util::reset(*scoreRef3);
        auto scoreRef4 = lmem().create<event::TrackScore>(lmem());
        util::reset(*scoreRef4);

        eOut.trackScores.emplace(DetID(3, 1), scoreRef1);
        eOut.trackScores.emplace(DetID(4, 1), scoreRef2);
        eOut.trackScores.emplace(DetID(5, 1), scoreRef3);
        eOut.trackScores.emplace(DetID(2, 1), scoreRef4);

        // 1st score shall contain nothing
        //scoreRef1->hitRefs.emplace();
        // 2nd score shall contain two hits
        scoreRef2->hitRefs.emplace(DetID(4, 1, 1), apvCluster);
        scoreRef2->hitRefs.emplace(DetID(5, 1, 1), caloHit);
        // 3rd score shall contain one hit
        scoreRef3->hitRefs.emplace(DetID(5, 1, 1), caloHit);
        // 4th score is somewhat perculiar topology: refers to scores #2, #1
        scoreRef4->hitRefs.emplace(DetID(4, 1, 1), scoreRef2);
        scoreRef4->hitRefs.emplace(DetID(2, 1, 1), scoreRef1);

        ASSERT_TRUE(std::holds_alternative<mem::Ref<event::CaloHit>>(
                scoreRef3->hitRefs.begin()->second));

        event::avro::OutStream os(_schemaStrJSON
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        os << eOut;
    }

    event::Event eIn(this->lmem());
    {
        event::avro::InStream is(_schemaStrJSON, &lmem()
                , _avroMemorybuffer, sizeof(_avroMemorybuffer));
        is >> eIn;
    }

    EXPECT_EQ(eIn.id, eOut.id)
        << "Non-empty event's with unions attribute deserialized with wrong event ID"
        << eIn.id << " instead of " << eOut.id;
    EXPECT_EQ(eIn.trackScores.size(), eOut.trackScores.size())
        << "Wrong number of track scores deserialized: " << eIn.trackScores.size()
        << " instead of " << eOut.trackScores.size();

    auto score1It = eIn.trackScores.find(DetID(3, 1));
    ASSERT_NE(score1It, eIn.trackScores.end())
        << "Score #1 not found by its ID";
    EXPECT_TRUE(score1It->second->hitRefs.empty())
        << "Score #1 contains hits (it should not)";

    auto score2It = eIn.trackScores.find(DetID(4, 1));
    ASSERT_NE(score2It, eIn.trackScores.end())
        << "Score #2 not found by its ID";
    ASSERT_FALSE(score2It->second->hitRefs.empty());
    EXPECT_EQ(score2It->second->hitRefs.size(), 2);
    auto & score2HitRefs = score2It->second->hitRefs;
    auto hit411It = score2HitRefs.find(DetID(4, 1, 1));
    ASSERT_NE(hit411It, score2HitRefs.end());
    EXPECT_TRUE(std::holds_alternative<mem::Ref<event::APVCluster>>(hit411It->second))
        << "Hit 4-1-1 of score #2 is of branch #"
        << hit411It->second.index();
    auto hit511It = score2HitRefs.find(DetID(5, 1, 1));
    ASSERT_NE(hit511It, score2HitRefs.end());
    EXPECT_TRUE(std::holds_alternative<mem::Ref<event::CaloHit>>(hit511It->second))
        << "Hit 5-1-1 of score #2 is of branch #"
        << hit411It->second.index();

    auto score3It = eIn.trackScores.find(DetID(5, 1));
    ASSERT_NE(score3It, eIn.trackScores.end())
        << "Score #3 not found by its ID";
    ASSERT_FALSE(score3It->second->hitRefs.empty());
    EXPECT_EQ(score3It->second->hitRefs.size(), 1);
    EXPECT_TRUE(std::holds_alternative<mem::Ref<event::CaloHit>>(
                score3It->second->hitRefs.begin()->second))
        << "Score #3 is of branch #"
        << score3It->second->hitRefs.begin()->second.index();

    // Score 4 refers to other scores
    auto score4It = eIn.trackScores.find(DetID(2, 1));
    ASSERT_NE(score4It, eIn.trackScores.end())
        << "Score #4 not found by its ID";
    ASSERT_FALSE(score4It->second->hitRefs.empty());

    auto subScore411It = score4It->second->hitRefs.find(DetID(4, 1, 1));
    ASSERT_NE(score4It->second->hitRefs.end(), subScore411It)
        << "Sub-score 4-1-1 not found in the score 2-1";
    ASSERT_TRUE(std::holds_alternative<mem::Ref<event::TrackScore>>(subScore411It->second));
    auto subScore411 = std::get<mem::Ref<event::TrackScore>>(subScore411It->second);
    ASSERT_EQ(subScore411, score2It->second)
        << "Score refs are not correctly resolved (sub-score #1) (duplicated?)";

    auto subScore211It = score4It->second->hitRefs.find(DetID(2, 1, 1));
    ASSERT_NE(score4It->second->hitRefs.end(), subScore211It)
        << "Sub-score 2-1-1 not found in the score 2-1";
    ASSERT_TRUE(std::holds_alternative<mem::Ref<event::TrackScore>>(subScore211It->second));
    auto subScore211 = std::get<mem::Ref<event::TrackScore>>(subScore211It->second);
    ASSERT_EQ(subScore211, score1It->second)
        << "Score refs are not correctly resolved (sub-score #2) (duplicated?)";
}

}
}
