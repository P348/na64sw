
#include "na64dp/abstractHandler.hh"
#include "avro-event-streams.hh"
#include "na64util/str-fmt.hh"

#include <fstream>
#include <log4cpp/Priority.hh>

namespace na64dp {
namespace handlers {

/**\brief Saves event stream into files of Apache Avro format
 *
 * \code{.yaml}
 *     - _type: AvroSaveEvents
 *       # Refers to Avro schema file (.json); str, required
 *       schema: ...
 *       # Name of the output file; str, required
 *       file: ...
 *       # Native Avro codec in use; str, optional
 *       codec: "null"
 *       # Block size in use; int, optional
 *       blockSizeKb: 0
 * \endcode
 *
 * */
class AvroSaveEvents : public AbstractHandler
                     , public event::avro::OutStream
                     {
private:
    FILE * _destFilePtr;
    size_t _nEventsWritten;
public:
    AvroSaveEvents( log4cpp::Category & logCat
            , const std::string & schemaStrJSON
            , FILE * fp
            , const std::string & destFilePath
            , const std::string & codec
            , size_t blockSize=0
            ) : AbstractHandler(logCat)
              , event::avro::OutStream(schemaStrJSON, fp, destFilePath, codec, blockSize )
              , _destFilePtr(fp)
              , _nEventsWritten(0)
              {
    }

    ProcRes process_event(event::Event &) override;
    void finalize() override;
};

AbstractHandler::ProcRes
AvroSaveEvents::process_event(event::Event & evRef) {
    (*this) << evRef;
    ++_nEventsWritten;
    return kOk;
}

void
AvroSaveEvents::finalize() {
    assert(_destFilePtr);
    event::avro::OutStream::flush();
    event::avro::OutStream::close();
    fclose(_destFilePtr);
    log().info("%zu events written.", _nEventsWritten);
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER(AvroSaveEvents, cdsp, cfg
        , "Serializes event stream to Apache Avro files" ) {
    auto & L = na64dp::aux::get_logging_cat(cfg);

    // Schema
    const auto schemaPath = na64dp::event::avro::get_schema_path();
    std::string schema; {
        std::ifstream ifs(schemaPath);
        std::stringstream buffer;
        buffer << ifs.rdbuf();
        schema = buffer.str();
        assert(!schema.empty());
    }

    // Output file
    std::string filePath;
    try {
        filePath = cfg["file"].as<std::string>();
    } catch(YAML::Exception & e) {
        NA64DP_RUNTIME_ERROR("YAML error on required string"
                " parameter \"file\": %s", e.what());
    }
    if(filePath.empty()) {
        NA64DP_RUNTIME_ERROR("Empty output file path (\"file\" parameter).");
    }
    FILE * fp = fopen(filePath.c_str(), "w");  // todo: does Avro support appending?
    if(!fp) {
        int erc = errno;
        NA64DP_RUNTIME_ERROR("Error %d opening file \"%s\": %s"
                , erc, filePath.c_str(), strerror(erc) );
    }

    // Avro codec
    std::string codec = cfg["codec"] ? cfg["codec"].as<std::string>() : "null";

    // Block size
    size_t blockSize = cfg["blockSizeKb"] ? cfg["blockSizeKb"].as<size_t>()*1024 : 0;

    L << log4cpp::Priority::INFO
        << "Avro output handler "
        << ( cfg["_label"] ? "\"" + cfg["_label"].as<std::string>() + "\" " : "" )
        << "set up to write into \""
        << filePath << "\" based on schema " << schemaPath
        << " using codec \"" << codec << "\" with block size " << blockSize
        << "b";
    return new na64dp::handlers::AvroSaveEvents( L
                , schema
                , fp
                , filePath
                , codec
                , blockSize
            );
}

