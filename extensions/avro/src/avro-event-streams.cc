#include "avro-event-streams.hh"
#include "na64event/stream-collections.hh"
#include "na64util/str-fmt.hh"
#include "na64util/uri.hh"
#include <avro/errors.h>
#include <avro/io.h>
#include <avro/schema.h>
#include <cstdlib>
#include <stdexcept>

namespace na64dp {
namespace event {
namespace avro {

#ifndef NA64AVRO_EVENT_SCHEMA_ENVVAR
#   define NA64AVRO_EVENT_SCHEMA_ENVVAR "NA64AVRO_EVENT_SCHEMA"
#endif

#ifndef NA64AVRO_EVENT_SCHEMA_DEFAULT_PATH
#   define NA64AVRO_EVENT_SCHEMA_DEFAULT_PATH "$NA64SW_PREFIX/share/na64sw/na64sw-event-avro.json"
#endif

std::string
get_schema_path(const char * default_) {
    const char * path = getenv(NA64AVRO_EVENT_SCHEMA_ENVVAR);
    if(!path){
        if(NULL != default_)
            path = default_;
        else
            return path = NA64AVRO_EVENT_SCHEMA_DEFAULT_PATH;
    }
    auto r = util::expand_name(path);
    {  // check file exists
        struct stat bf;
        if( 0 != stat(r.c_str(), &bf) ) {
            char errbf[256];
            snprintf(errbf, sizeof(errbf), "File \"%s\" does not exist (assumed"
                    " to be Apacha Avro schema for NA64 event.", r.c_str());
            throw std::runtime_error(errbf);
        }
    }
    return r;
}

const BaseStream::CollectedItemID BaseStream::emptyItemID
    = {typeid(void), std::numeric_limits<size_t>::max()};

void
BaseStream::_init_collections() {
    // tries to init collections. Note, that in schema collections are
    // (potentially) defined not for every compound type, so it is ok for
    // `avro_value_get_by_name()` to return error from time to time
    #define M_init_avro_collection(typeName, ...) {     \
        avro_value_t av;                                \
        auto ir = _collectionsByType.emplace(typeid(event:: typeName), av); \
        if( avro_value_get_by_name( &_cDef.av, "_collection" # typeName , &(ir.first->second), NULL ) ) { \
            _collectionsByType.erase(ir.first);         \
        }                                               \
    }
    M_for_every_compound_type(M_init_avro_collection);
    #undef M_init_avro_collection
}

BaseStream::BaseStream(const std::string & schemaStrJSON) {
    //if(avro_schema_from_json_literal(schemaStrJSON.c_str(), &_eventSchema))
    avro_schema_error_t schemaError;
    if(avro_schema_from_json( schemaStrJSON.c_str(), schemaStrJSON.size()
                            , &_eventSchema
                            , &schemaError
                            )){
        NA64DP_RUNTIME_ERROR("Failed to parse Avro schema: %s", avro_strerror());
    }

    #if 0
    // xxx, experimental:
    #if 0
    if(NULL == avro_schema_record(_eventSchema, "APVHitByAPVPhysWire_tEntry")) {
        NA64DP_RUNTIME_ERROR("subschema error: %s", avro_strerror());
    } else {
        std::cout << "xxx subschema ok" << std::endl;
    }
    #else
    avro_schema_t arraySchema;
    if(NULL == (apvArraySchema = avro_schema_get_subschema(_eventSchema, "sadcHits"))) {
        NA64DP_RUNTIME_ERROR("subschema error #1: %s", avro_strerror());
    } else {
        std::cout << "xxx subschema #1 ok: " << avro_schema_type_name(apvArraySchema) << std::endl;
    }

    avro_schema_t apvArrayPairSchema;
    if(NULL == (apvArrayPairSchema = avro_schema_array_items(apvArraySchema))) {
        NA64DP_RUNTIME_ERROR("subschema error #2: %s", avro_strerror());
    } else {
        std::cout << "xxx subschema #2 ok: "
                  << avro_schema_type_name(apvArrayPairSchema) << ":"
                  << avro_schema_name(apvArrayPairSchema)
                  //<< avro_schema_union_size(apvArrayPairSchema) << "]"
                  << std::endl;
    }

    #if 0
    avro_schema_t apvArrayUnionItemSchema;
    if(NULL == (apvArrayUnionItemSchema = avro_schema_union_branch(apvArrayPairSchema, 0))) {
        NA64DP_RUNTIME_ERROR("subschema error #3: %s", avro_strerror());
    } else {
        std::cout << "xxx subschema #3 ok: "
            << avro_schema_type_name(apvArrayUnionItemSchema)
            << std::endl;
    }
    #else
    avro_schema_t apvArrayFirstSchema;
    if(NULL == (apvArrayFirstSchema = avro_schema_record_field_get(apvArrayPairSchema, "key"))) {
        NA64DP_RUNTIME_ERROR("subschema error #3: %s", avro_strerror());
    } else {
        std::cout << "xxx subschema #3 ok" << std::endl;
    }
    #endif
    #endif
    #endif
}

BaseStream::ObjectDefinition
BaseStream::_init(const char * typeName) {
    assert(!strcmp("Event", typeName));  // avro streams defined only for `Event`
    _cDef.iface = _eventIFace = avro_generic_class_from_schema(_eventSchema);
    assert(_cDef.iface);
    int rc;
    if( 0 != (rc = avro_generic_value_new(_cDef.iface, &_cDef.av) ) ) {
        throw errors::AvroError(na64dp::util::format("Couldn't init avro value for writing (%d):"
                    "%s", rc, avro_strerror() ));
    }
    _init_collections();
    return &_cDef;
}

void
BaseStream::_finalize(ObjectDefinition defPtr, const char * typeName) {
    assert(&_cDef == defPtr);
    avro_value_decref(&defPtr->av);
    avro_value_iface_decref(defPtr->iface);
}

BaseStream::~BaseStream() {
    avro_schema_decref(_eventSchema);
}

//
// CollectedItemID

void
Traits<BaseStream::CollectedItemID>::set(
                  const BaseStream::CollectedItemID & v
                , avro_value_t & av
                , OutStream & ctx
                ) {
    int rc;
    //assert(COLLECTION_REF_SIZE == sizeof(v.second))  // TODO
    if(0 != (rc = avro_value_set_fixed( &av
                        , const_cast<void*>(reinterpret_cast<const void *>(&v.second))  // TODO: why non-const?
                        , sizeof(v.second)
                        ))) {
        throw errors::AvroError(util::format("Avro error occured while"
                    " setting CollectedItemID as fixed of size %zub, %d: %s"
                    , sizeof(v.second)
                    , rc
                    , avro_strerror()
                    ));
    }
}

void
Traits<BaseStream::CollectedItemID>::get(
                  const avro_value_t & av
                , BaseStream::CollectedItemID & v
                , InStream & ctx
                ) {
    int rc;
    size_t sz;
    v.first = typeid(void);
    const void * buf;
    if(0 != (rc = avro_value_get_fixed(&av, &buf, &sz))) {
        throw errors::AvroError(util::format("Avro error occured while"
                    " getting CollectedItemID %d: %s"
                    , rc, avro_strerror() ));
    }
    if(ctx.checkFixedSizes) {
        if(sz != sizeof(v.second)) {
            NA64DP_RUNTIME_ERROR("Fixed size mismatch: read %zub,"
                    " expected %zub (CollectedItemID)", sz, sizeof(v.second)
                    );
        }
    }
    memcpy(&v.second, static_cast<const char*>(buf), sizeof(v.second));
}


//
// Output stream

OutStream::OutStream( const std::string & schemaStrJSON
                    , FILE * destFile
                    , const std::string & path
                    , const std::string & codec
                    , size_t blockSize
                    ) : BaseStream(schemaStrJSON)
                      , _fileDestination(destFile)
                      {
    int rc = avro_file_writer_create_with_codec_fp( _fileDestination // FILE *, fp
            , path.c_str()  // const char *, path
            , 0  // int, should close
            , _event_schema()  // avro_schema_t, schema
            , &_dest.fileWriter // avro_file_writer_t *, writer
            , codec.c_str()  // const char *, codec
            , blockSize // size_t, block_size
            );
    if(0 != rc) {
        NA64DP_RUNTIME_ERROR("Failed to initialize Avro file writer by path"
                " \"%s\" with codec \"%s\" and block size %zub: rc=%d, %s"
                , path.c_str(), codec.c_str(), blockSize, rc, avro_strerror()
                );
    }
}

OutStream::OutStream( const std::string & schemaStrJSON
                    , char * buf, size_t bufSize )
        : BaseStream(schemaStrJSON) {
    _fileDestination = NULL;
    _dest.memWriter = avro_writer_memory(buf, bufSize);
}

void
OutStream::_append_event_value() {
    int rc;
    if(_fileDestination) {
        if(0 != (rc = avro_file_writer_append_value(_dest.fileWriter, &_cDef.av))) {
            NA64DP_RUNTIME_ERROR("Avro failed to append file writer value, %d: %s"
                    , rc, avro_strerror() );
        }
    } else {
        if(0 != (rc = avro_value_write(_dest.memWriter, &_cDef.av))) {
            NA64DP_RUNTIME_ERROR("Avro failed to append in-memory writer value, %d: %s"
                    , rc, avro_strerror() );
        }
    }
}

void
OutStream::flush() {
    if(_fileDestination) {
        avro_file_writer_flush(_dest.fileWriter);
    }
}

void
OutStream::close() {
    if(_fileDestination) {
        avro_file_writer_close(_dest.fileWriter);
        _fileDestination = NULL;
    }
}

void
OutStream::_finalize(ObjectDefinition defPtr, const char * typeName) {
    BaseStream::_finalize(defPtr, typeName);
    WritableCollections<BaseStream>::reset();
}

OutStream::~OutStream() {
    close();
}

//
// Input stream

InStream::InStream( const std::string & schemaStrJSON
                  , LocalMemory * lmemPtr
                  , FILE * fp
                  , const std::string & path
                  ) : BaseStream(schemaStrJSON)
                    , _isGood(true)
                    , _fileSource(fp)
                    , _lmemPtr(lmemPtr)
                    , checkSyntetic(true)  // TODO: parameter
                    , checkFixedSizes(true)  // TODO: parameter
                    {
    int rc = avro_file_reader_fp( _fileSource  // FILE*, file ptr
                                , path.c_str()  // const char *, path
                                , 0  // int, should close
                                , &_src.fileReader // avro_file_reader_t, reader ptr
                                );
    if(0 != rc) {
        NA64DP_RUNTIME_ERROR("Failed to initialize Avro file reader by path"
                " \"%s\", %d: %s", path.c_str(), rc, avro_strerror());
    }
}

InStream::InStream( const std::string & schemaStrJSON
                  , LocalMemory * lmemPtr
                  , char * buf, size_t bufSize )
        : BaseStream(schemaStrJSON)
        , _isGood(true)
        , _lmemPtr(lmemPtr)
        , checkSyntetic(true)  // TODO: parameter
        , checkFixedSizes(true)  // TODO: parameter
        {
    _fileSource = NULL;
    _src.memReader = avro_reader_memory(buf, bufSize);
}

void
InStream::_finalize(ObjectDefinition defPtr, const char * typeName) {
    BaseStream::_finalize(defPtr, typeName);
    ReadableCollections<BaseStream>::reset();
}

bool
InStream::_retrieve_event_value() {
    // NOTE: albeit it seem not to be defined explicitly in the docs, Avro C
    // API `avro*read_value` seem to literally return EOF when it is reached.
    int rc;
    if(_fileSource) {
        if(0 != (rc = avro_file_reader_read_value(_src.fileReader, &_cDef.av))) {
            _isGood = false;
            if(EOF == rc) {
                return false;
            }
            NA64DP_RUNTIME_ERROR("Avro error %d while reading event"
                    " from file: %s"
                    , rc, avro_strerror() );
        }
    } else {
        if(0 != (rc = avro_value_read(_src.memReader, &_cDef.av))) {
            _isGood = false;
            if(EOF == rc) {
                return false;
            }
            NA64DP_RUNTIME_ERROR("Avro error %d while reading event from"
                    " memory buffer: %s"
                    , rc, avro_strerror() );
        }
    }
    return true;
}

void
InStream::close() {
    if(_fileSource) {
        avro_file_reader_close(_src.fileReader);
        _fileSource = NULL;
    }
}

}  // namespace ::na64dp::event::avro
}  // namespace ::na64dp::event
}  // namespace na64dp
