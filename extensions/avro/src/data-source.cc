
#include "na64dp/abstractEventSource.hh"
#include "avro-event-streams.hh"

#include <fstream>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

namespace na64dp {

/**\brief Implements simple plain events files reader
 * */
class AvroSource : public AbstractEventSource {
private:
    /// Schema used when no schema provided for file
    const std::string _defaultSchema;
    /// List of file paths
    std::list<std::string> _filesPaths;
    /// Iterator referencing current file path
    std::list<std::string>::const_iterator _cFileIt;
    /// Pointer to event stream currently in use
    event::avro::InStream * _cStreamPtr;
    /// File ptr to file being currently read
    FILE * _cFilePtr;
    /// Ref to calib manager used to consider new event IDs
    calib::Manager & _cMgr;
public:
    AvroSource( calib::Manager & cMgr
              , log4cpp::Category & logCat
              , const std::vector<std::string> & filePaths
              , const std::string & defaultSchema=""
              , iEvProcInfo * epi=nullptr
              )
        : AbstractEventSource(epi, logCat)
        , _defaultSchema(defaultSchema)
        , _filesPaths(filePaths.begin(), filePaths.end())
        , _cFileIt(_filesPaths.begin())
        , _cStreamPtr(nullptr)
        , _cFilePtr(NULL)
        , _cMgr(cMgr)
        {
        if(_filesPaths.empty()) {
            log() << log4cpp::Priority::WARN << "Empty inputs list.";
        }
    }

    bool read( event::Event &, event::LocalMemory & lmem ) override;
};  // class AvroSource

bool
AvroSource::read(event::Event & e, event::LocalMemory & lmem) {
    while(true) {  // until event read or sources depleted
        if(_cFileIt == _filesPaths.end()) return false;  // depleted
        // [another] source available
        if(!_cStreamPtr) {  // no current stream
            // till valid file opened and ources list is not exhausted
            while((!_cFilePtr) && _cFileIt != _filesPaths.end()) {
                _cFilePtr = fopen(_cFileIt->c_str(), "r");
                if(!_cFilePtr) {
                    int erc = errno;
                    log() << log4cpp::Priority::ERROR
                        << "Error opening file \""
                        << *_cFileIt << "\" (" << erc << "): "
                        << strerror(erc);
                    ++_cFileIt;
                } else {
                    log() << log4cpp::Priority::NOTICE
                        << "Reading file \""
                        << *_cFileIt << "\"...";
                }
            }
            if(!_cFilePtr) {
                log() << log4cpp::Priority::ERROR
                        << "No file(s) in list available for reading an event.";
                return false;
            }
            std::string schema = _defaultSchema;  // TODO: look for adjacent .json file?
            _cStreamPtr = new event::avro::InStream(schema, &lmem, _cFilePtr
                    , _cFileIt->c_str());
        }
        assert(_cStreamPtr);
        _cStreamPtr->set_lmem(lmem);
        try {
            (*_cStreamPtr) >> e;
        } catch(std::exception & err) {
            log() << log4cpp::Priority::ERROR
                << "While reading event from \""
                << *_cFileIt << "\": " << err.what();
            ++_cFileIt;
            delete _cStreamPtr;
            _cStreamPtr = nullptr;
            fclose(_cFilePtr);
            _cFilePtr = nullptr;
        }
        if(_cStreamPtr->is_good()) {
            _cMgr.event_id(e.id, e.time);
            return true;
        } else {
            ++_cFileIt;
            delete _cStreamPtr;
            _cStreamPtr = nullptr;
            fclose(_cFilePtr);
            _cFilePtr = nullptr;
        }
    }
}

}  // namespace na64dp

REGISTER_SOURCE(AvroDST, cmgr, cfg, filePaths
        , "Event source based on Apache Avro"
        ) {
    na64dp::iEvProcInfo * epi = nullptr;  // TODO: provide in ctr or set it aposteriori with some method...
    log4cpp::Category & L = log4cpp::Category::getInstance(
                cfg["_log"] ? cfg["_log"].as<std::string>() : "source.avro"
            );

    const auto schemaPath = na64dp::event::avro::get_schema_path();
    std::string defaultSchema;
    {
        std::ifstream ifs(schemaPath);
        std::stringstream buffer;
        buffer << ifs.rdbuf();
        defaultSchema = buffer.str();
        assert(!defaultSchema.empty());
    }
    L.debug("Event (Apache Avro) schema in use: %s", schemaPath.c_str());

    return new na64dp::AvroSource( cmgr
                                 , L
                                 , filePaths
                                 , defaultSchema
                                 , epi
                                 );
}

