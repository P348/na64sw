#pragma once

#include "na64event/stream-collections.hh"
#include "na64event/data/event.hh"

#include <avro.h>

/**\brief Various C++ helpers to simplify integration with C avro lib
 * 
 * This header defines serialization traits in namespace `avro`. This traits
 * are utilized by complex object serialization. Entry point for all the
 * operations are bitshift operators (`<<` and `>>`) defined for `event::Event`
 * structure and corresponding stream types.
 * */

namespace na64dp {
namespace errors {
class AvroError : public GenericRuntimeError {
public:
    AvroError() : GenericRuntimeError("Avro (de)serialization error occured.") {}
    AvroError(const std::string & msg) : GenericRuntimeError(msg.c_str()) {}
};  // class AvroError
}  // namespace ::na64dp::errors
namespace event {
namespace avro {

///\brief Returns schema file path.
std::string get_schema_path(const char * default_=NULL);

#define AVRO_CHCK(what, when) if(0!=what) \
    {throw errors::AvroError("Avro error occured while " + std::string(when) + ":" + avro_strerror());}

class InStream;
class OutStream;

template<typename T, typename T2=void>
struct Traits;

///\brief Common base for Avro streaming classes
///
/// Maintains Avro instances common for data serialization -- schema iface,
/// value objects etc.
class BaseStream {
public:
    ///\brief Current insertion node target.
    struct ObjectDefinition_t {
        avro_value_t av;
        avro_value_iface_t * iface;
    };
    /// Used by shim
    typedef ObjectDefinition_t * ObjectDefinition;
    /// Type used to identify an attribute (field within a record)
    typedef const char * AttrID;
    /// Type used to identify item within a collection
    typedef std::pair<std::type_index, size_t> CollectedItemID;
    /// Special value denoting no item
    static const CollectedItemID emptyItemID;
    /// Template to use to keep collection index for writables (of certain type)
    template<typename T> using WritableCollectionIndex
        = std::unordered_map<const T *, CollectedItemID>;
    /// Template to use to keep collection index for writables (of certain type)
    template<typename T> using ReadableCollectionIndex
        = std::unordered_map<uint32_t, mem::Ref<T>>;
private:
    avro_schema_t _eventSchema;
    avro_value_iface_t * _eventIFace;
protected:
    avro_schema_t _event_schema() { return _eventSchema; }
    /// Root object (event) definition
    ObjectDefinition_t _cDef;
    /// Collection arrays to fill or read
    std::unordered_map<std::type_index, avro_value_t> _collectionsByType;
    /// Initializes `_collectionsByType` index
    void _init_collections();
    /// Sets up Avro schema for root object (`event::Event`)
    virtual ObjectDefinition _init(const char * typeName);
    /// `decref`'s Avros schema and iface for root object
    virtual void _finalize(ObjectDefinition defPtr, const char * typeName);

    BaseStream(const std::string & schemaStrJSON);
    virtual ~BaseStream();
public:
    /// Adds new record item to an event
    ///
    /// Gets called when new sub-object (hit) within main structure (event)
    /// should be added to the tables. Shall add the item to appropriate array
    /// and return index.
    template<typename ObjectT> CollectedItemID
    add_collectible(const ObjectT * objPtr);

    template<typename T> avro_value_t &
    get_avro_collection_item() {
        auto it = _collectionsByType.find(typeid(T));
        assert(it != _collectionsByType.end());
        return it->second;
    }
};


///\brief Implementation of data output stream
///
/// Wraps native Avro C functions providing uniform interface for output
/// operations with in-memory or file destination (see ctrs).
class OutStream : public BaseStream
                , public WritableCollections<BaseStream>
                {
private:
    /// If set, the `fileWriter` field of `_dest` attr will be used
    FILE * _fileDestination;
    union {
        avro_writer_t       memWriter;
        avro_file_writer_t  fileWriter;
    } _dest;
protected:
    void _append_event_value();
    void _finalize(ObjectDefinition defPtr, const char * typeName) override;
public:
    /**\brief Sets up stream to write events by C file pointer
     *
     * Parameterised with schema string, 
     * Forwards call to generic Avro C API's
     * `avro_file_writer_create_with_codec_fp()` to set up Avro writer structs,
     * so meaning of most of the arguments is inherited.
     *
     * Set of available codecs can vary depending on target system's Avro lib
     * build, but typically it should be one of "snappy", "defalte", "lzma".
     *
     * \note Avro C defines somewhat default block size of about 16kb. This
     *       value is used if `blockSize` is specified to 0.
     *
     * \note Albeit it is not explicitly written anywhere in Avro's docs, it
     *       seems from C code, that Avro mostly uses the file path parameter
     *       for diagnostics if file pointer is provided (so one can, for
     *       instance, benefit from UNIX in-memory files).
     * */
    OutStream( const std::string & schemaStrJSON
             , FILE * destFile
             , const std::string & path
             , const std::string & codec
             , size_t blockSize=0
             );

    /**\brief Constructor for in-memory destination
     *
     * Must be parameterised with buffer of proper size. Internally forwards
     * call to `avro_writer_memory()` to initialize the buffer. */
    OutStream(const std::string & schemaStrJSON, char * buf, size_t bufSize);

    /// For files -- closes the block (forwards to `avro_file_writer_flush()`
    void flush();

    ///\brief Finalizes the recording
    ///
    /// Does not closes underlying file, if file writer is used, just
    /// finalizes the reading (forwards to `avro_file_writer_close()`)
    ///
    ///\warning Further attempts to write the file leads to unpredicted
    ///         behaviour.
    void close();

    ~OutStream();

    /**\brief Method gets called for simple (leaf) data types to copy data
     *        from event's struct into Avro value
     *
     * POD attributes are simple scalar values or structs. They should have
     * direct correspondance in the schema. This method relies on Avro traits
     * defined for POD structure to retrieve and add the data from attribute
     * reference provided.
     */
    template<typename AttrT> void
    pod_attr(ObjectDefinition ownerDef, AttrID attrID, const AttrT & v) {
        avro_value_t av;
        int rc;
        if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &av, NULL))) {
            NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d for attr \"%s\": %s"
                    , attrID, rc, attrID, avro_strerror() );
        }
        avro::Traits<AttrT>::set(v, av, *this);
    }

    /**\brief Gets called with attributes of map values
     *
     * Designed for maps with values of certain hit type -- the ones
     * referenced with `mem::Ref<>` or union of such types (std::variant<>).
     * */
    template<typename MapT> void
    map_attr(ObjectDefinition ownerDef, AttrID attrID, const MapT & m);

    /**\brief Method called for attributes of compound type(s)
     *
     * For Avro stream implem this method gets called for attributes of Avro
     * `record` type. For output stream this will either add new element to a
     * typed collection or retrieve the index of the existing one and set
     * attribute (which should be `fixed` of size `sizeof(size_t)`) to that
     * index.
     * */
    template<typename ObjectT> void
    typed_attr( ObjectDefinition ownerDef
              , AttrID attrID
              , const mem::Ref<ObjectT> & ref
              );

    friend OutStream & operator<<(OutStream & os, const event::Event & obj);
};

inline OutStream & operator<<(OutStream & os, const event::Event & obj) {
    auto shim = StreamingShim<OutStream>(os);

    auto odef = os._init(event::Traits<event::Event>::typeName);
    shim.current_object(odef);
    event::Traits<event::Event>::for_each_attr(shim, obj);
    os._append_event_value();
    os._finalize(odef, event::Traits<event::Event>::typeName);

    return os;
}


///\brief Implementation of events input stream
///
/// Wraps native Avro C functions providing uniform interface for input
/// operations with in-memory or file source (see ctrs).
class InStream : public BaseStream
               , public ReadableCollections<BaseStream>
               {
private:
    bool _isGood;
    FILE * _fileSource;
    union {
        avro_reader_t       memReader;
        avro_file_reader_t  fileReader;
    } _src;
    LocalMemory * _lmemPtr;
protected:
    bool _retrieve_event_value();
    void _finalize(ObjectDefinition defPtr, const char * typeName) override;
public:
    const bool checkSyntetic  ///< Turns on some runtime checks for value sizes
             , checkFixedSizes  ///< Turns on some redundant checks for fixed sizes
             ;

    /**\brief Constructor for file-based source
     *
     * Forwards execution to `avro_file_reader_fp()` to init the native reader
     * struct, so meaning of `fp` and `path` parameters are simply inherited.
     * */
    InStream( const std::string & schemaStrJSON
            , LocalMemory * lmemPtr
            , FILE * fp
            , const std::string & path
            );

    /**\brief Constructor for in-memory source
     *
     * Must be parameterised with buffer of proper size keeping
     * Avro-serialized event(s). Internally forwards
     * call to `avro_reader_memory()` to init avro reader on the buffer. */
    InStream( const std::string & schemaStrJSON
            , LocalMemory * lmemPtr
            , char * buf, size_t bufSize);

    void close();
    bool is_good() const { return _isGood; }

    LocalMemory & lmem() { return *_lmemPtr; }
    void set_lmem(LocalMemory & lmem) {_lmemPtr = &lmem;}

    /**\brief Method gets called for simple (leaf) data types to set the
     *        struct's attribute from Avro record's value
     *
     * POD attributes are simple scalar values or structs. They should have
     * direct correspondance in the schema. This method relies on Avro traits
     * defined for POD structure to read the data from Avro value and copy
     * it to event attribute.
     */
    template<typename AttrT> void
    pod_attr(ObjectDefinition ownerDef, AttrID attrID, AttrT & v)  {
        avro_value_t av;
        int rc;
        if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &av, NULL))) {
            NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d for attr \"%s\": %s"
                    , attrID, rc, attrID, avro_strerror() );
        }
        avro::Traits<AttrT>::get(av, v, *this);
    }

    /**\brief Gets called with attributes of map values
     *
     * Designed for maps with values of certain hit type -- the ones
     * referenced with `mem::Ref<>` or union of such types (std::variant<>).
     * */
    template<typename MapT> void
    map_attr(ObjectDefinition ownerDef, AttrID attrID, MapT & m);

    /**\brief Method called for attributes of compound type(s)
     *
     * For Avro stream implem this method gets called for attributes of Avro
     * `record` type. For input stream this will set `mem::Ref<>`
     * provided here by reference argument to collection element being read
     * previously.
     * */
    template<typename ObjectT> void
    typed_attr(ObjectDefinition defPtr, AttrID attrID, mem::Ref<ObjectT> & ref);

    friend InStream & operator>>(InStream & is, event::Event & obj);
};

inline InStream & operator>>(InStream & is, event::Event & obj) {
    auto shim = StreamingShim<InStream>(is);

    auto odef = is._init(event::Traits<event::Event>::typeName);
    shim.current_object(odef);
    if( is._retrieve_event_value() ) {
        event::Traits<event::Event>::for_each_attr(shim, obj);
    }
    is._finalize(odef, event::Traits<event::Event>::typeName);
    return is;
}

//
// Avro native types

template<> struct Traits<bool> {
    static void set(const bool v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_boolean(&av, v ? 1 : 0)
                , "setting boolean value");
    }
    static void get(const avro_value_t & av, bool & v, InStream & ctx) {
        int vv = -1;
        AVRO_CHCK(avro_value_get_boolean(&av, &vv)
                , "getting boolean value");
        assert(-1 != vv);
        v = (vv != 0);
    }
};

template<> struct Traits<int32_t> {
    static void set(const int32_t & v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_int(&av, v)
                , "setting integer value");
    }
    static void get(const avro_value_t & av, int32_t & v, InStream & ctx) {
        AVRO_CHCK(avro_value_get_int(&av, &v)
                , "getting integer value");
    }
};

template<> struct Traits<int64_t> {
    static void set(const int64_t & v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_long(&av, v)
                , "setting long integer value");
    }
    static void get(const avro_value_t & av, long & v, InStream & ctx) {
        AVRO_CHCK(avro_value_get_long(&av, &v)
                , "getting long integer value");
    }
};

template<> struct Traits<float> {
    static void set(const float v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_float(&av, v)
                , "setting float value");
    }
    static void get(const avro_value_t & av, float & v, InStream & ctx) {
        AVRO_CHCK(avro_value_get_float(&av, &v)
                , "getting float value");
    }
};

template<> struct Traits<double> {
    static void set(const double & v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_double(&av, v)
                , "setting double value");
    }
    static void get(const avro_value_t & av, double & v, InStream & ctx) {
        AVRO_CHCK(avro_value_get_double(&av, &v)
                , "getting double value");
    }
};

//
// Type mocking
//
// These types are not supported by Avro natively, but we cast these values to
// bigger ones to keep compatibility

template<> struct Traits<int16_t> {
    static void set(const int16_t v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_int(&av, v)
                , "setting int16_t value as integer");
    }
    static void get(const avro_value_t & av, int16_t & v, InStream & ctx) {
        int32_t v_;
        AVRO_CHCK(avro_value_get_int(&av, &v_)
                , "getting int16_t value as integer");
        if(ctx.checkSyntetic) {
            if( v_ < std::numeric_limits<int16_t>::min()
             || v_ > std::numeric_limits<int16_t>::max()
             )
            NA64DP_RUNTIME_ERROR("int16_t value range under-/overflow (%d)"
                    " -- type incompatibility.", v_);
        }
        v = static_cast<int16_t>(v_);
    }
};

template<> struct Traits<uint16_t> {
    static void set(const uint16_t v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_int(&av, v)
                , "setting uint16_t value as integer");
    }
    static void get(const avro_value_t & av, uint16_t & v, InStream & ctx) {
        int32_t v_;
        AVRO_CHCK(avro_value_get_int(&av, &v_)
                , "getting uint16_t value as integer");
        if(ctx.checkSyntetic) {
            if( v_ < std::numeric_limits<uint16_t>::min()
             || v_ > std::numeric_limits<uint16_t>::max()
             )
            NA64DP_RUNTIME_ERROR("uint16_t value range under-/overflow (%d)"
                    "-- type incompatibility.", v_);
        }
        v = static_cast<uint16_t>(v_);
    }
};

template<> struct Traits<uint8_t> {
    static void set(const uint8_t v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_int(&av, v)
                , "setting uint8_t value as integer");
    }
    static void get(const avro_value_t & av, uint8_t & v, InStream & ctx) {
        int32_t v_;
        AVRO_CHCK(avro_value_get_int(&av, &v_)
                , "getting uint8_t value as integer");
        if(ctx.checkSyntetic) {
            if( v_ < std::numeric_limits<uint8_t>::min()
             || v_ > std::numeric_limits<uint8_t>::max()
             )
            NA64DP_RUNTIME_ERROR("uint16_t value range under-/overflow (%d)"
                    "-- type incompatibility.", v_);
        }
        v = static_cast<int8_t>(v_);
    }
};

template<> struct Traits<int8_t> {
    static void set(const int8_t v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_int(&av, v)
                , "setting int8_t value as integer");
    }
    static void get(const avro_value_t & av, int8_t & v, InStream & ctx) {
        int32_t v_;
        AVRO_CHCK(avro_value_get_int(&av, &v_)
                , "getting int8_t value as integer");
        if(ctx.checkSyntetic) {
            if( v_ < std::numeric_limits<int8_t>::min()
             || v_ > std::numeric_limits<int8_t>::max()
             )
            NA64DP_RUNTIME_ERROR("uint16_t value range under-/overflow (%d)"
                    "-- type incompatibility.", v_);
        }
        v = static_cast<int8_t>(v_);
    }
};

template<> struct Traits<uint32_t> {
    static void set(const uint32_t & v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_long(&av, v)
                , "setting long integer value");
    }
    static void get(const avro_value_t & av, uint32_t & v, InStream & ctx) {
        long v_;
        AVRO_CHCK(avro_value_get_long(&av, &v_)
                , "getting long integer value");
        assert(v_ < std::numeric_limits<uint32_t>::max());
        v = static_cast<uint32_t>(v_);
    }
};

template<> struct Traits<uint64_t> {
    static void set(const uint64_t v, avro_value_t & av, OutStream & ctx) {
        AVRO_CHCK(avro_value_set_fixed(&av,
                const_cast<void *>(static_cast<const void *>(&v)), sizeof(v))  // TODO: why non-const?
            , "setting uint64_t value as fixed");
    }
    static void get(const avro_value_t & av, uint64_t & v, InStream & ctx) {
        size_t sz;
        const void * v_;
        AVRO_CHCK(avro_value_get_fixed(&av, &v_, &sz)
                , "getting uint64_t value as fixed");
        if(ctx.checkFixedSizes) {
            if( sz != sizeof(v) )
            NA64DP_RUNTIME_ERROR("Fixed size mismatch for uint64_t: read %zub,"
                        " expected %zub", sz, sizeof(v) );
        }
        memcpy(&v, v_, sizeof(v));
    }
};

//
// POD-type arrays, serialized as avro's `fixed'
template<typename T, size_t TN>
struct Traits< T[TN]
             , typename std::enable_if<std::is_pod<T>::value>::type
             > {
    static void set(const T (& v)[TN], avro_value_t & av, OutStream & ctx) {
        if(0 != avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(v))  // TODO: why non-const?
                            , sizeof(v)
                            )) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " setting C array of type %s with %zu elements: %s"
                        , typeid(T[TN]).name(), TN, avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, T (& v)[TN], InStream & ctx) {
        size_t sz;
        const void * v_;
        if(0 != avro_value_get_fixed(&av, &v_, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting C array of type %s with %zu elements: %s"
                        , typeid(T[TN]).name(), TN, avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != sizeof(v)) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch: read %zub,"
                        " expected %zub", sz, sizeof(v) );
            }
        }
        memcpy(v, v_, sizeof(v));
    }
};

//
// EventID
template<>
struct Traits<EventID> {
    static void set(const EventID & v, avro_value_t & av, OutStream & ctx) {
        na64sw_EventID_t eid = v;
        int rc;
        assert(av.iface->set_fixed);
        if(0 != (rc = avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(&eid))  // TODO: why non-const?
                            , sizeof(na64sw_EventID_t)
                            ))) {
            throw errors::AvroError(util::format("Avro error %d occured while"
                        " setting EventID as fixed type of size %zu: %s"
                        , rc, sizeof(na64sw_EventID_t), avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, EventID & eid, InStream & ctx) {
        size_t sz;
        const void * v_;
        if(0 != avro_value_get_fixed(&av, &v_, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting EventID: %s"
                        , avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != sizeof(na64sw_EventID_t)) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch (eventID): read %zub,"
                        " expected %zub", sz, sizeof(na64sw_EventID_t) );
            }
        }
        eid = EventID(*reinterpret_cast<const na64sw_EventID_t*>(v_));
    }
};

//
// DetID
template<>
struct Traits<DetID> {
    static void set(const DetID & v, avro_value_t & av, OutStream & ctx) {
        int rc;
        assert(av.iface->set_fixed);
        if(0 != (rc = avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(&v.id))  // TODO: why non-const?
                            , sizeof(DetID_t)
                            ))) {
            throw errors::AvroError(util::format("Avro error %d occured while"
                        " setting DetID as fixed type of size %zu: %s"
                        , rc, sizeof(DetID_t), avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, DetID & v, InStream & ctx) {
        size_t sz;
        const void * buf;
        if(0 != avro_value_get_fixed(&av, &buf, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting DetID: %s"
                        , avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != sizeof(DetID_t)) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch (eventID): read %zub,"
                        " expected %zub", sz, sizeof(DetID_t) );
            }
        }
        v.id = *reinterpret_cast<const DetID_t*>(buf);
    }
};

template<>
struct Traits<PlaneKey> {
    static void set(const PlaneKey & v, avro_value_t & av, OutStream & ctx) {
        int rc;
        assert(av.iface->set_fixed);
        if(0 != (rc = avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(&v.id))  // TODO: why non-const?
                            , sizeof(DetID_t)
                            ))) {
            throw errors::AvroError(util::format("Avro error %d occured while"
                        " setting DetID as fixed type of size %zu: %s"
                        , rc, sizeof(DetID_t), avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, PlaneKey & v, InStream & ctx) {
        size_t sz;
        const void * buf;
        if(0 != avro_value_get_fixed(&av, &buf, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting DetID: %s"
                        , avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != sizeof(DetID_t)) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch (eventID): read %zub,"
                        " expected %zub", sz, sizeof(DetID_t) );
            }
        }
        v.id = *reinterpret_cast<const DetID_t*>(buf);
    }
};

template<>
struct Traits<TrackID> {
    static void set(const TrackID & tID, avro_value_t & av, OutStream & ctx) {
        int rc;
        TrackID_t v = tID.number();
        assert(av.iface->set_fixed);
        if(0 != (rc = avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(&v))  // TODO: why non-const?
                            , sizeof(TrackID_t)
                            ))) {
            throw errors::AvroError(util::format("Avro error %d occured while"
                        " setting TrackID as fixed type of size %zu: %s"
                        , rc, sizeof(TrackID_t), avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, TrackID & v, InStream & ctx) {
        size_t sz;
        const void * buf;
        if(0 != avro_value_get_fixed(&av, &buf, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting DetID: %s"
                        , avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != sizeof(TrackID_t)) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch (trackID): read %zub,"
                        " expected %zub", sz, sizeof(TrackID_t) );
            }
        }
        v.number(*reinterpret_cast<const TrackID_t*>(buf));
    }
};

//
// Pair
template<typename T1, typename T2>
struct Traits<std::pair<T1, T2>> {
    static void set(const std::pair<T1, T2> & v, avro_value_t & av, OutStream & ctx) {
        const size_t size = sizeof(T1) + sizeof(T2);
        char * buf = static_cast<char*>(alloca(size));
        memcpy(buf,              &v.first,      sizeof(T1));
        memcpy(buf + sizeof(T1), &v.second,     sizeof(T2));
        if(0 != avro_value_set_fixed( &av
                            , const_cast<void*>(reinterpret_cast<const void *>(buf))  // TODO: why non-const?
                            , size
                            )) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " setting std::pair<%s,%s>: %s"
                        , /*util::demangle_cpp(*/typeid(T1).name()//)
                        , /*util::demangle_cpp(*/typeid(T2).name()//)
                        , avro_strerror() ));
        }
    }
    static void get(const avro_value_t & av, std::pair<T1, T2> & v, InStream & ctx) {
        const size_t size = sizeof(T1) + sizeof(T2);
        size_t sz;
        //char * buf = static_cast<char*>(alloca(size));
        const void * buf;
        if(0 != avro_value_get_fixed(&av, &buf, &sz)) {
            throw errors::AvroError(util::format("Avro error occured while"
                        " getting std::pair<%s,%s>: %s"
                        , typeid(T1).name(), typeid(T2).name(), avro_strerror() ));
        }
        if(ctx.checkFixedSizes) {
            if(sz != size) {
                NA64DP_RUNTIME_ERROR("Fixed size mismatch: read %zub,"
                        " expected %zub (std::pair<%s, %s>)", sz, size
                        , typeid(T1).name(), typeid(T2).name() );
            }
        }
        memcpy(&v.first,  buf, sizeof(T1));
        memcpy(&v.second, static_cast<const char*>(buf) + sizeof(T1), sizeof(T2));
    }
};

//
// CollectedItemID, very special case
template<>
struct Traits<BaseStream::CollectedItemID> {
    static void set(const BaseStream::CollectedItemID & v, avro_value_t & av, OutStream & ctx);
    static void get(const avro_value_t & av, BaseStream::CollectedItemID & v, InStream & ctx);
};

template<typename T> struct MappedValueTraits;

template<typename T> struct MappedValueTraits<mem::Ref<T>> {
    /// Directly sets `avTarget` value to collected item link
    static BaseStream::CollectedItemID
    collect_mapped_value( OutStream & os
                        , const mem::Ref<T> & v
                        , avro_value_t & avTarget
                        ) {
        
        BaseStream::CollectedItemID itemID
            = os.collection_of<T>().id_of(v.get(), os);
        // `avTarget` is typed
        avro::Traits<BaseStream::CollectedItemID>::set(itemID, avTarget, os);
        return itemID;
    }

    static mem::Ref<T>
    retrieve_mapped_value( InStream & is
                         , avro_value_t & v
                         ) {
        BaseStream::CollectedItemID itemID = BaseStream::emptyItemID;
        int rc;
        avro::Traits<BaseStream::CollectedItemID>::get(v, itemID, is);
        if(itemID == BaseStream::emptyItemID) {
            return mem::Ref<T>();
        }
        // for now we just ignore typeID part of itemID, however this object
        // can be elaborated further to provide additional assurance for type
        // validity
        assert(itemID.second != BaseStream::emptyItemID.second);
        auto & rCol = is.collection_of<T>();
        auto it = rCol.find(itemID.second);
        if(it == rCol.end()) {
            // item is not found in readable collection cache, so read it,
            // add to cache, and return
            mem::Ref<T> newObj = is.lmem().create<T>(is.lmem());
            avro_value_t & array = is.get_avro_collection_item<T>();
            avro_value_t av;
            if(0 != (rc = avro_value_get_by_index(&array, itemID.second, &av, NULL))) {
                NA64DP_RUNTIME_ERROR("avro_value_get_by_index() of %s"
                        " collection returned %d: %s"
                        , event::Traits<T>::typeName
                        , rc, avro_strerror()
                        );
            }
            StreamingShim<InStream> shim(is);
            BaseStream::ObjectDefinition_t odef = {av, nullptr};
            shim.current_object(&odef);
            event::Traits<T>::for_each_attr(shim, *newObj);
            it = rCol.emplace(itemID.second, newObj).first;
        }
        //return is.collection_of<T>().value_by(itemID);
        return it->second;
    }
};

template<> struct MappedValueTraits<std::monostate> {
    /// Sets N-th branch to empty value
    static BaseStream::CollectedItemID
    collect_mapped_value( OutStream & os
                        , const std::monostate &
                        , avro_value_t & avTarget
                        ) {
        int rc = avro_value_set_null(&avTarget);
        if(0 != rc) {
            NA64DP_RUNTIME_ERROR("Error %s setting Avro Null in map traits: %s"
                    , avro_strerror() );
        }
        return BaseStream::emptyItemID;
    }
    static std::monostate
    retrieve_mapped_value( InStream & is
                         , avro_value_t & av
                         ) {
        int rc = avro_value_get_null(&av);
        if(0 != rc) {
            NA64DP_RUNTIME_ERROR("Error %s getting Avro Null in map traits: %s"
                    , avro_strerror() );
        }
        return std::monostate();
    }
};

// Variant unwind to branch #N for MappedValueTraits<> {{{
namespace detail {
template<size_t nT, typename ...Ts>
struct UnwindVariantToN {
    static void unwind_variant( std::variant<Ts...> & v, size_t n
                              , InStream & is
                              , avro_value_t & av
                              ) {
        if(nT == n) {
            v = MappedValueTraits<typename std::variant_alternative<nT, std::variant<Ts...>>::type>
                ::retrieve_mapped_value(is, av);
        }
        else UnwindVariantToN<nT-1, Ts...>::unwind_variant(v, n, is, av);
    }
};

template<typename ...Ts>
struct UnwindVariantToN<0, Ts...> {
    static void unwind_variant( std::variant<Ts...> & v, size_t n
                              , InStream & is
                              , avro_value_t & av
                              ) {
        if(0 == n) {
            v = MappedValueTraits<typename std::variant_alternative<0, std::variant<Ts...>>::type>
                ::retrieve_mapped_value(is, av);
        }
        throw std::logic_error("Variant types exceeded.");  // should not be possible
    }
};
}  // namespace ::na64dp::util::avro::detail

template<typename ...Ts> void
unwind_variant( size_t n, std::variant<Ts...> & v
              , InStream & is
              , avro_value_t & av
              ) {
    if(n >= std::variant_size_v<std::variant<Ts...>>) {
        NA64DP_RUNTIME_ERROR("Variant branch #%zu exceeds static capacity of"
                " actual type (%zu)", n, std::variant_size_v<std::variant<Ts...>> );
    }
    detail::UnwindVariantToN< std::variant_size_v<std::variant<Ts...>>-1
                            , Ts...
                            >::unwind_variant(v, n, is, av);
}
// }}}

template<typename ... Ts> struct MappedValueTraits<std::variant<Ts...>> {
    static BaseStream::CollectedItemID
    collect_mapped_value( OutStream & os
                        , const std::variant<Ts...> & vv
                        , avro_value_t & avTarget
                        ) {
        assert(AVRO_UNION == avro_value_get_type(&avTarget));
        assert(vv.index() != std::variant_npos);
        assert(vv.index() < std::numeric_limits<int>::max());
        return std::visit([&](const auto & v) -> BaseStream::CollectedItemID {
                    assert(AVRO_UNION == avro_value_get_type(&avTarget));
                    avro_value_t vBranch;
                    int rc;
                    if(0 != (rc = avro_value_set_branch(&avTarget, vv.index(), &vBranch))) {
                        NA64DP_RUNTIME_ERROR("Failed to set Avro union branch to #%zu, %d: %s"
                                , vv.index(), rc, avro_strerror() );
                    }
                    // todo: here some sort of type assertion can be implemented as
                    // schema values are named. If we would have knowledge of name
                    // match b/w variant's types and Avro union branches we could
                    // benefit here...
                    return MappedValueTraits<
                            typename std::remove_const<typename std::remove_reference<decltype(v)>::type>::type
                       >::collect_mapped_value(os, v, vBranch);
            }, vv);
    }

    static std::variant<Ts...>
    retrieve_mapped_value( InStream & is
                         , avro_value_t & avUnion
                         // ...?
                         ) {
        assert(AVRO_UNION == avro_value_get_type(&avUnion));
        int rc, n;
        if(0 != (rc = avro_value_get_discriminant(&avUnion, &n))) {
            NA64DP_RUNTIME_ERROR("Avro error %d getting union discriminant: %s"
                    , rc, avro_strerror() );
        }
        avro_value_t vBranch;
        if(0 != (rc = avro_value_set_branch(&avUnion, n, &vBranch))) {
            NA64DP_RUNTIME_ERROR("Avro error %d setting union branch #%d: %s"
                    , rc, n, avro_strerror() );
        }
        std::variant<Ts...> v;
        unwind_variant(n, v, is, vBranch);
        return v;
    }
};

//
// Base stream deferred implementations

template<typename ObjectT> BaseStream::CollectedItemID
BaseStream::add_collectible(const ObjectT * objPtr) {
    OutStream & this_ = static_cast<OutStream&>(*this);
    // special value for nulls
    if(!objPtr) return emptyItemID;
    // otherwise serialize new item
    auto shim = StreamingShim<OutStream>(this_);

    auto collIt = _collectionsByType.find(typeid(ObjectT));
    assert(collIt != _collectionsByType.end());
    
    size_t nEl;
    ObjectDefinition_t odef;
    odef.iface = nullptr;
    // append certain collection with new (serialized) value
    int rc;
    if(0 != (rc = avro_value_append(&collIt->second, &odef.av, &nEl))) {
        NA64DP_RUNTIME_ERROR("Couldn't append collection item,"
                " avro_value_append() returned %d: %s", rc
                , avro_strerror() );
    }

    shim.current_object(&odef);
    event::Traits<ObjectT>::for_each_attr(shim, *objPtr);
    return BaseStream::CollectedItemID(typeid(ObjectT), nEl);
}

//
// Event output stream

template<typename MapT> void
OutStream::map_attr(ObjectDefinition ownerDef, AttrID attrID, const MapT & m) {
    avro_value_t avArray;
    int rc;
    if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &avArray, NULL))) {
        NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d for array \"%s\": %s"
                , attrID, rc, attrID, avro_strerror() );
    }
    size_t nEl;
    for(const auto & p : m) {
        avro_value_t avPair;
        // TODO: below does not work for unions
        //avro_generic_value_new(avPairClassPtr, &avPair);
        if(0 != (rc = avro_value_append(&avArray, &avPair, &nEl))) {
            NA64DP_RUNTIME_ERROR("Failed to append setOf mapping with value,"
                    " avro_value_append() returned %d: %s"
                    , rc, avro_strerror()
                    );
        }
        avro_value_t first, second;
        if(0 != (rc = avro_value_get_by_index(&avPair, 0, &first,  NULL))) {
            NA64DP_RUNTIME_ERROR("Failed to set key of setOf item,"
                    " avro_value_get_by_index(0) returned %d: %s"
                    , rc, avro_strerror()
                    );
        }
        if(0 != (rc = avro_value_get_by_index(&avPair, 1, &second, NULL))) {
            NA64DP_RUNTIME_ERROR("Failed to obtain mapped value of setOf item"
                    " (for setting it),"
                    " avro_value_get_by_index(1) returned %d: %s"
                    , rc, avro_strerror()
                    );
        }
        // set first pair el -- a key
        avro::Traits<typename MapT::key_type>::set(p.first, first, *this);
        // retrieve item ID and set it as a second pair el
        // The tricky part is that the map can be not only of compund type
        // here, but also of union (`std::variant<>`) so we resolve
        // particular implementation using traits
        MappedValueTraits<typename MapT::mapped_type>::collect_mapped_value(
                    *this, p.second, second);
    }
}

template<typename ObjectT> void
OutStream::typed_attr( ObjectDefinition ownerDef
          , AttrID attrID
          , const mem::Ref<ObjectT> & ref
          ) {
    CollectedItemID id = emptyItemID;
    id.first = typeid(ObjectT);
    if(ref) {
        // get ID of item
        id = this->collection_of<ObjectT>().id_of(ref.get(), *this);
    }

    avro_value_t av;
    int rc;
    if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &av, NULL))) {
        NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d: %s"
                    , attrID, rc, avro_strerror() );
    }
    if(0 != (rc = avro_value_set_fixed(&av, &id.second, sizeof(id.second)))) {
        NA64DP_RUNTIME_ERROR("avro_value_set_fixed() returned %d for"
                " attribute \"%s\" (ref to foreign item): %s"
                    , rc, attrID, avro_strerror() );
    }
}

//
// Event input stream


template<typename MapT> void
InStream::map_attr(ObjectDefinition ownerDef, AttrID attrID, MapT & m) {
    avro_value_t avArray;
    int rc;
    if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &avArray, NULL))) {
        NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d: %s"
                , attrID, rc, avro_strerror() );
    }
    assert(AVRO_ARRAY == avro_value_get_type(&avArray));
    size_t collectionSize;
    if(0 != (rc = avro_value_get_size(&avArray, &collectionSize))) {
        NA64DP_RUNTIME_ERROR("avro_value_get_size() on array attribute \"%s\""
                " returned %d: %s"
                , attrID, rc, avro_strerror() );
    }
    for(size_t nEl = 0; nEl < collectionSize; ++nEl) {
        avro_value_t avPair;
        if(0 != (rc = avro_value_get_by_index(&avArray, nEl, &avPair, NULL))) {
            NA64DP_RUNTIME_ERROR("Failed to get item #%zu, error code %d: %s"
                    , nEl, rc, avro_strerror());
        }
        avro_value_t first, second;
        if(0 != (rc = avro_value_get_by_index(&avPair, 0, &first,  NULL))) {
            NA64DP_RUNTIME_ERROR("Failed to get key of item #%zu, error code %d: %s"
                    , nEl, rc, avro_strerror());
        }
        if(0 != (rc = avro_value_get_by_index(&avPair, 1, &second, NULL))) {
            NA64DP_RUNTIME_ERROR("Failed to get value of item #%zu, error code %d: %s"
                    , nEl, rc, avro_strerror());
        }
        typename MapT::key_type key;
        avro::Traits<typename MapT::key_type>::get(first, key, *this);

        m.emplace( key, MappedValueTraits<typename MapT::mapped_type>::retrieve_mapped_value(
                    *this, second ));
        #if 0
        // if item ID is null, do not allocate object
        if(itemID.second == emptyItemID.second) {
            m.emplace( key, typename MapT::mapped_type() );
            // NOTE: if `mapped_type` is of `std::variant<>` with `monostate`,
            // it shall get initialized to `monostate` still. If it is of
            // `mem::Ref<>`, it should be set to nullptr (correct behaviour in
            // both cases)
        } else {
            // otherwise, retrieve read item from readable collection OR read
            // item and add it to readable collection. To correctly resolve
            // std::variant type delegated to MappedValueTraits
            m.emplace( key, MappedValueTraits<typename MapT::mapped_type>::retrieve_mapped_value(
                    *this, itemID ));
        }
        #endif
    }
}

template<typename ObjectT> void
InStream::typed_attr( ObjectDefinition ownerDef
          , AttrID attrID
          , mem::Ref<ObjectT> & ref
          ) {
    int rc;
    avro_value_t avRef;
    if(0 != (rc = avro_value_get_by_name(&ownerDef->av, attrID, &avRef, NULL))) {
        NA64DP_RUNTIME_ERROR("avro_value_get_by_name(\"%s\", ...) returned %d: %s"
                , attrID, rc, avro_strerror() );
    }
    #if 1
    ref = MappedValueTraits<mem::Ref<ObjectT>>::retrieve_mapped_value(*this, avRef);
    #else
    CollectedItemID id = emptyItemID;
    Traits<CollectedItemID>::get(avRef, id, *this);
    if(id.second == emptyItemID.second) {
        // empty ref
        ref.reset();
        return;
    }
    ref = MappedValueTraits<mem::Ref<ObjectT>>::retrieve_mapped_value(*this, id);
    #endif
}

}  // namespace ::na64dp::event::avro
}  // namespace ::na64dp::event
}  // namespace na64dp

