cmake_minimum_required( VERSION 2.6 )

#
# Sensitive detectors, actions and other MC extensions module
set( na64mcext_LIB mc )
set( na64mcext_LIB_SOURCES
        # Main Geant4 API implementation sources ------------------------------
        src/g4api/detectorConstruction.cc
        src/g4api/primaryGeneratorAction.cc
        src/g4api/eventAction.cc
        src/g4api/runAction.cc
        src/g4api/steppingAction.cc
        src/g4api/physicsList.cc
        #
        src/g4api/run.cc
        src/g4api/eventInformation.cc
        src/g4api/sensitiveDetector.cc
        src/g4api/UISession.cc
        # Custom processes & physics
        src/g4api/processes/stepMaxLimiter.cc
        src/g4api/phLists/emShower.cc
        src/gdml/processor.cc
        src/gdml/parallelWorld.cc
        # MC-related utilities
        src/genericMessenger.cc
        src/messenger.cc
        src/eventProcessor.cc
        src/eventBuilder.cc
        src/partsIndex.cc
        src/notifications.cc
        src/stepClassifier.cc
        src/stepDispatcher.cc
        # ...
        # Modules -------------------------------------------------------------
        # Utility files
        src/stepping/scalarGetter.cc
        # GDML
        src/gdml/visStyle.cc
        src/gdml/sensDet.cc
        src/gdml/exportDefinition.cc
        src/gdml/segmentID.cc
        # Stepping assets
        src/stepping/handlers/dump.cc
        src/stepping/filters/worldLeaving.cc
        src/stepping/classifiersCommon.cc
        src/stepping/classifierSegmSD.cc
        # Sensitive detectors
        src/sd/segmented.cc
        #mc-extensions/src/sd/segmented.cc
        #mc-extensions/src/sd/multiple.cc
        #mc-extensions/src/sd/composite.cc
        # Composite SD extensions
        #mc-extensions/src/sd/composite/scorers.cc
        #mc-extensions/src/sd/composite/ROOTHist1D.cc
        #src/na64mc/g4api/sd/stepsDump.cc
        #mc-extensions/src/sd/emCylCalo.cc
        #mc-extensions/src/sd/segmentScoring.cc
        #mc-extensions/src/sd/ECAL.cc
        # ...
    )

#    add_library(${na64mc_LIB} SHARED ${na64mc_LIB_SOURCES})
#    target_include_directories(${na64mc_LIB} SYSTEM PUBLIC ${na64_COMMON_SYS_INCLUDE_DIRS} )
#    target_include_directories(${na64mc_LIB} PUBLIC ${na64_COMMON_INCLUDE_DIRS})
#    set_target_properties(${na64mc_LIB} PROPERTIES VERSION ${NA64DP_VERSION}
#                           SOVERSION ${NA64DP_VERSION_MAJOR}
#                           #INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib/na64
#                           )
#
#    target_compile_definitions( ${na64mc_LIB} PUBLIC ${Geant4_DEFINITIONS} )
#    target_include_directories( ${na64mc_LIB} PUBLIC SYSTEM ${Geant4_INCLUDE_DIRS} )
#
#    # Get rid of the meaningless "-pedantic" flag and flag forcing the c++
#    # standard of certain version that Geant4 offers among its compiling options
#    string(REPLACE "-pedantic" "" Geant4_CXX_FLAGS_ ${Geant4_CXX_FLAGS} )
#    string(REGEX REPLACE "-std=(c|gnu)\\+\\+[^\\s]+" "" Geant4_CXX_FLAGS_ ${Geant4_CXX_FLAGS_} )
#    separate_arguments(Geant4_CXX_FLAGS_ UNIX_COMMAND "${Geant4_CXX_FLAGS_}")
#    target_compile_options( ${na64mc_LIB} PUBLIC ${Geant4_CXX_FLAGS_} )
#    target_link_libraries( ${na64mc_LIB} ${Geant4_LIBRARIES} ${na64dp_LIB} )
#    install(TARGETS ${na64mc_LIB} EXPORT ${na64mc_LIB} LIBRARY )

add_library(${na64mcext_LIB} SHARED ${na64mcext_LIB_SOURCES})
target_include_directories(${na64mcext_LIB} PUBLIC ${na64_COMMON_INCLUDE_DIRS}
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> )
set_target_properties(${na64mcext_LIB} PROPERTIES VERSION ${na64sw_VERSION}
                                                SOVERSION ${na64sw_SOVERSION}
                   INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib/na64/modules )
target_compile_definitions( ${na64mcext_LIB} PUBLIC ${Geant4_DEFINITIONS} )
target_include_directories( ${na64mcext_LIB} PUBLIC SYSTEM ${Geant4_INCLUDE_DIRS} )
target_compile_options( ${na64mcext_LIB} PUBLIC ${Geant4_CXX_FLAGS_} )
install(TARGETS ${na64mcext_LIB} EXPORT ${na64mcext_LIB} LIBRARY
        DESTINATION ${NA64SW_STDEXT_DIR}
    )
# This is to provide working configuration in the build directory
add_custom_target( mc-ext-bld-symlink ALL
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_BINARY_DIR}/lib${na64mcext_LIB}.so ../${na64mcext_LIB} )
