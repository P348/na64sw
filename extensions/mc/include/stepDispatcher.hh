#pragma once

#include "na64util/vctr.hh"
#include "na64mc/stepClassifier.hh"

#include <unordered_map>

class G4Step;
class G4String;

namespace na64dp {
namespace mc {

class Notifier;

/**\brief An MC step destination interface
 *
 * This interface is used as an extension point by our runtime-configured
 * stepping action and sensitive detector classes to handle information brought
 * by Geant4 Monte-Carlo steps.
 */
struct iStepHandler {
    virtual void handle_step(const G4Step *) = 0;
    virtual ~iStepHandler() {}
};

/// Step filtering interface
struct iStepFilter {
    virtual bool passes(const G4Step *) const = 0;
    virtual ~iStepFilter() {}
};

/**\brief A basic Geant4 Monte-Carlo step dispatching template.
 *
 * This class defines common logic for the set of handler, indexed with some
 * kind of "key". The "key" might be the PDG code, energy sub-interval,
 * detector multipart index and so on.
 *
 * This class maintains a subset of sub-handlers which are addressed using
 * the "key" instance derived from `G4Step` instance. The STL `unordered_map`
 * is used as a container for (key, sub-handler) pairs. At this level no
 * assumption is done, how the key is derived from step (`derive_key()`
 * abstract method is responsible).
 *
 * Runtime-specified combination of multiple keys is
 * foreseen by descendant class `AbstractRuntimeStepDispatcher`, by some
 * performance cost.
 *
 * The `iStepHandler` instances to be constructed in a lazy way when key that
 * was not met in a simulation process does not cause the handler creation.
 *
 * The class implements `iStepHandle` interface.
 * */
template< typename KeyT, typename KeyHashT=std::hash<KeyT> >
class AbstractStepDispatcher
    : public iStepHandler
    , public std::unordered_map<KeyT, iStepHandler *, KeyHashT> {
public:
    /// Filter pointer (set to filter instance or null)
    iStepFilter * filterPtr;
protected:
    /// Creates a "key" instance using the step information
    virtual KeyT derive_key(const G4Step *) = 0;
    /// Creates a new step handler instance using key and the step information
    virtual iStepHandler * new_handler(const KeyT &, const G4Step *) = 0;
public:
    AbstractStepDispatcher(iStepFilter * filterPtr_=nullptr)
            : filterPtr(filterPtr_) {}

    /**\brief Handles a step by dispatching it deep in hierarchy
     *
     * Implements the following logic:
     *  - derive the "key" from step
     *  - tries to locate sub-handler using derived key
     *  - if no destination found, create new one
     *  - forward the step to sub-handler
     * */
    virtual void handle_step( const G4Step * aStep ) override {
        typedef std::unordered_map<KeyT, iStepHandler *, KeyHashT>
                    Container;  // Used locally, for short
        if( filterPtr && ! filterPtr->passes(aStep) ) return;
        auto k = derive_key(aStep);
        auto it = Container::find(k);
        if( Container::end() == it ) {
            it = Container::emplace( k, new_handler(k, aStep) ).first;
        }
        it->second->handle_step(aStep);
    }
};

/**\brief Combines numerical codes of step features in a single hash
 *
 * Internally relies on naive XOR-based implementation with repeated left
 * shifts in a sequence.
 *
 * \todo Optimization/performance studies may be needed
 * */
struct StepFeaturesHash {
    std::size_t operator()(const StepHandlerKey &) const;
};

/**\brief Categorizes step handlers by dynamic set of features
 *
 * This class partically implements interface from `AbstractStepDispatcher` to
 * accomplish dispatching of steps by dynamic set of features. Uses reference
 * to an instance of `StepClassifier` managed externally at a runtime to
 * derive keys.
 *
 * The step handler instances are constructed in a lazy way.
 * */
class AbstractRuntimeStepDispatcher
        : public AbstractStepDispatcher<StepHandlerKey, StepFeaturesHash> {
private:
    /// Extractors set to be filled by subclasses
    StepClassifier & _keyComposer;
protected:
    /// Creates key using runtime specification from `StepClassifier`
    virtual StepHandlerKey derive_key(const G4Step *) override;
public:
    /// Binds the step dispatcher instance with a certain classifier
    AbstractRuntimeStepDispatcher( StepClassifier & kc
                                 , iStepFilter * filterPtr_=nullptr);

    StepClassifier & step_classifier() { return _keyComposer; }
    const StepClassifier & step_classifier() const { return _keyComposer; }
};


}  // namespace na64dp::mc

template<> struct CtrTraits<mc::iStepHandler> {
    typedef mc::iStepHandler * (*Constructor)
        ( const G4String &
        , const G4String &
        , mc::Notifier &
        );
};

template<> struct CtrTraits<mc::AbstractRuntimeStepDispatcher> {
    typedef mc::AbstractRuntimeStepDispatcher * (*Constructor)
        ( const G4String &
        , const G4String &
        , mc::Notifier &
        , mc::StepClassifier &
        );
};

template<> struct CtrTraits<mc::iStepFilter> {
    typedef mc::iStepFilter * (*Constructor)
        ( const G4String &
        , const G4String &
        // ...
        );
};

}  // namespace na64dp

/**\brief Registers new steps handler type for SD/SA
 * \ingroup vctr-defs
 *
 * Registers subclasses of `mc::iStepHandler` base to be created with `VCtr`.
 *
 * \param clsName The name of the handler type to be used
 * \param name The name for "name" variable passed to a ctr; names the instance
 * \param path The name for "path" variable; Geant4 messenger UI command path
 * \param notifier The name for "notifier" variable; instance of `mc::Notifier`
 * \param desc Representative description of this steps handler type
 */
# define REGISTER_MC_STEP_HANDLER( clsName, name, path, notifier, desc ) \
static na64dp::mc::iStepHandler * _new_ ## clsName ## _instance( const G4String &  \
                                                      , const G4String &        \
                                                      , na64dp::mc::Notifier &  \
                                                      );  \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::mc::iStepHandler>( # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::mc::iStepHandler * _new_ ## clsName ## _instance(    \
                __attribute__((unused)) const G4String & name \
              , __attribute__((unused)) const G4String & path \
              , __attribute__((unused)) na64dp::mc::Notifier & notifier \
              )

/**\brief Registers new classifying steps handler type for SD/SA
 * \ingroup vctr-defs
 *
 * Registers subclasses of `mc::AbstractRuntimeStepDispatcher` base to be
 * created with `VCtr`.
 *
 * \param clsName The name of the handler type to be used
 * \param name The name for "name" variable passed to a ctr; names the instance
 * \param path The name for "path" variable; Geant4 messenger UI command path
 * \param notifier The name for "notifier" variable; instance of `mc::Notifier`
 * \param kcPtr The name for classifier instance (`StepClassifier`)
 * \param desc Representative description of this steps handler type
 */
# define REGISTER_CLASSIFYING_MC_STEP_HANDLER( clsName, name, path, notifier, kcPtr, desc ) \
static na64dp::mc::AbstractRuntimeStepDispatcher * \
        _new_ ## clsName ## _instance( const G4String &  \
                                     , const G4String &        \
                                     , na64dp::mc::Notifier &  \
                                     , na64dp::mc::StepClassifier & \
                                     );  \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::mc::AbstractRuntimeStepDispatcher>( \
                # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::mc::AbstractRuntimeStepDispatcher * _new_ ## clsName ## _instance(    \
                __attribute__((unused)) const G4String & name \
              , __attribute__((unused)) const G4String & path \
              , __attribute__((unused)) na64dp::mc::Notifier & notifier \
              , __attribute__((unused)) na64dp::mc::StepClassifier & kc \
              )


/**\brief Registers new steps handler type for SD/SA
 * \ingroup vctr-defs
 *
 * Registers subclasses of `mc::iStepFilter` base to be created with `VCtr`.
 *
 * \todo Probably, will need more arguments for filters...
 *
 * \param clsName The name of the filter type to be used
 * \param name The name for "name" variable passed to a ctr; names the instance
 * \param path Messenger path
 * \param desc Representative description of this steps handler type
 */
# define REGISTER_MC_STEP_FILTER( clsName, name, path, desc )       \
static na64dp::mc::iStepFilter * _new_ ## clsName ## _instance( const G4String &  \
                                                              , const G4String & );  \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::mc::iStepFilter>( # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::mc::iStepFilter * _new_ ## clsName ## _instance(    \
                __attribute__((unused)) const G4String & name \
              , __attribute__((unused)) const G4String & path )

