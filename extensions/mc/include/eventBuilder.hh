#pragma once

#include "na64dp/abstractEventSource.hh"

#include <cassert>

class G4Event;
class G4VHit;

namespace na64dp {
namespace mc {

/**\brief Implements an event source for MC event data processing pipelines
 *
 * This class facilitates conversion of the MC data provided within the
 * sensitive detector isntances (and, probably, other Geant4 actions) into
 * an `na64sw::Event` for further processing within a `na64sw` pipeline.
 *
 * From `na64sw` point of view, this builder implements `AbstractEventSource`
 * interface providing access to event-generation procedures.
 *
 * From Geant4 API's point of view this class defines a handle that has to be
 * associated with current `G4Event` in order to rpovide event assembling and
 * hit accumulation methods for sensitive detectors and various MC pipeline
 * hooks ("actions").
 *
 * The lifetime of this instance is typically managed within the Geant4
 * "run action" methods since its lifespan is run-wide.
 *
 * \note There is a small redundancy connected to the `read()` implementation
 * method does not provide a real "reading".
 * */
class EventBuilder : public AbstractEventSource
                   , public calib::Handle<nameutils::DetectorNaming> {
protected:
    /// Updates current naming
    void handle_update( const nameutils::DetectorNaming & naming) override;
public:
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::get(); }
    /// Initializes new event builder instance
    EventBuilder( calib::Manager & mgr
                , log4cpp::Category & logCat  // TODO: make use of it
                , iEvProcInfo * epi=nullptr
                );
    /// Assures that current event instance is the same as the provided object
    virtual bool read(event::Event &, event::LocalMemory & lmem) override;
};

}
}

