#pragma once

#include "sd/composite/scorers.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND
#   include <TProfile.h>

namespace na64dp {
namespace mc {

/// Cumulative histogram (typically, charge or energy)
struct ROOTHist1DValue {
    /// Per-event summation cache
    numerical::KleinScorer fromThisEvent;
    /// Supervised histogram
    TH1F * hst;

    ROOTHist1DValue() = delete;  // forbid implicit instances

    /// Explicit copy ctr  XXX?
    //ROOTHist1DValue(const ROOTHist1DValue & o) : fromThisEvent(o.fromThisEvent)
    //                                           , hst(o.hst) {}

    /// Constructs a 1D ROOT histogram accumulating values from the event
    ROOTHist1DValue( const char * name
                   , const char * title
                   , size_t nBins, double vMin, double vMax
                   );

    ~ROOTHist1DValue() {
        // intentionally no delete operator here for `hst' -- the destination's
        // instances are returned using copy ctr
    }
};

/**\brief A set of histograms as a destination for the composite detector
 *
 * This is a set of ROOT histograms exploited by "composite SD" instance. All
 * the ROOT entites must have an unique name, so we expect the histograms to be
 * instantiated according some general rule. To generate unique name and
 * representative title, the textual template is used with following context
 * entries:
 *      ...
 */
class ROOT1DHists : public SparseDestination<ROOTHist1DValue>
                  , public StepGetterMixin
                  , public geode::GenericG4Messenger
                  {
protected:
    /// Destination name
    std::string _name;
    /// Common name prefix
    std::string _nameTemplate;
    /// Common title
    std::string _titleTemplate;
    /// Number of bins for histogram instantiation
    size_t _nBins;
    /// Values range for the histogram
    double _range[2];
    /// Units of the histogram
    double _unit;
public:
    ROOT1DHists(const G4String & name, const G4String & msgrPath);

    //  const std::string & nameTemplate
    //, const std::string & titleTemplate
    //, size_t nBins, double vMin, double vMax

    friend struct DestinationTraits<ROOTHist1DValue>;

    /// Sets getter to use
    static void ui_cmd_set_getter( geode::GenericG4Messenger*
                                 , const G4String & );
    /// Sets the name pattern for ROOT histogram
    static void ui_cmd_set_name( geode::GenericG4Messenger*
                               , const G4String & );
    /// Sets the title pattern for ROOT histogram
    static void ui_cmd_set_title( geode::GenericG4Messenger*
                                , const G4String & );
    /// Sets the histogram parameters
    static void ui_cmd_set_hst_pars( geode::GenericG4Messenger*
                                   , const G4String & );
    /// Writes histograms to currently open ROOT file
    static void ui_cmd_write( geode::GenericG4Messenger*
                            , const G4String & );
};

template<>
struct DestinationTraits<ROOTHist1DValue> {
    /// Allocates new histogram entry
    static ROOTHist1DValue create( const SparseDestination<ROOTHist1DValue> & dst
                                 , const PartsIndex::Index i
                                 , const G4Step * aStep
                                 );

    static void account( const SparseDestination<ROOTHist1DValue> & dst
                       , ROOTHist1DValue & cell
                       , const PartsIndex::Index //i
                       , const G4Step * aStep
                       ) {
        cell.fromThisEvent +=
            static_cast<const ROOT1DHists&>(dst).get_value(aStep);
    }

    static void finalize( const SparseDestination<ROOTHist1DValue> & dst
                        , PartsIndex::Index
                        , ROOTHist1DValue & cell ) {
        double v = cell.fromThisEvent;
        cell.fromThisEvent.reset();
        cell.hst->Fill(v/static_cast<const ROOT1DHists&>(dst)._unit);
    }
};

}
}

#endif  // ROOT_FOUND
