#pragma once

#include "stepping/scalarGetter.hh"
#include "sd/composite.hh"

namespace na64dp {
namespace mc {

/**\brief Scalar values for the `CompositeSensitiveDetector`
 *
 * Provides interface for the scalar getter implementation over the `G4Step`
 * instance to fill the composite destination.
 * */
class ScalarScorers : public SparseDestination<CumulativeValue>
                    , public StepGetterMixin
                    , public geode::GenericG4Messenger
                    {
private:
    const G4String _name;
    size_t _nEvents;
public:
    // (no parameters for simple Klein scorers)
    ScalarScorers( const G4String & name
                 , const G4String & msgrPath );


    virtual void finalize_event() override {
        ++_nEvents;
        SparseDestination<CumulativeValue>::finalize_event();
    }

    /// Sets getter to use
    static void ui_cmd_set_getter( geode::GenericG4Messenger*
                                 , const G4String & );

    /// Dumps the accumulated values to ASCII file
    static void ui_cmd_write_ascii( geode::GenericG4Messenger*
                                  , const G4String & );
};

/// Specialization for scalar summation interface
template<>
struct DestinationTraits<CumulativeValue> {
    static CumulativeValue create( const SparseDestination<CumulativeValue> & //dst
                                 , const PartsIndex::Index //i
                                 , const G4Step * //aStep
                                 ) {
        return CumulativeValue();
    }

    static void account( const SparseDestination<CumulativeValue> & dst
                       , CumulativeValue & cell
                       , const PartsIndex::Index //i
                       , const G4Step * aStep
                       ) {
        cell.fromThisEvent +=
            static_cast<const ScalarScorers&>(dst).get_value(aStep);
    }

    static void finalize( const SparseDestination<CumulativeValue> &
                        , PartsIndex::Index
                        , CumulativeValue & cv ) {
        double v = cv.fromThisEvent;
        cv.fromThisEvent.reset();
        cv.overall += v;
        cv.sqOverall += v*v;
    }
};

}
}

