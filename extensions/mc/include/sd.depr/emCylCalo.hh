#pragma once

#include "sd/segmented.hh"

#include "na64util/numerical/sums.h"

class TH1F;
class G4Material;

namespace na64dp {
namespace mc {

/**\brief An E/M-shower testing sensitive detector
 *
 * This SD facilitates a cylindric calorimeter geometry. Intended for studying
 * of electromagnetic shower profiles in a cylinindric readout scheme defined
 * in some testing fixtures of our geomlib.
 *
 * This SD does nothing with na64sw event and may operate without our event
 * post-processing routines. It produces a 3D histogram of type TH3D containing
 * energy deposition in layers and segments of cylindric readout.
 * */
class EmCylCaloTestSD : public SegmentedSensitiveDetector {
public:
    // Collection of scorers used for certain cell
    struct Cell {
        TH1F * edep;
        Cell(Index);
        ~Cell();
    };
private:
    std::unordered_map<Index_t, na64dp_KleinScorer_t> _scorers;
    std::unordered_map<Index_t, Cell> _cells;
protected:
    virtual void process_hits_for( const Index_t & idx, G4Step * aStep ) override;
public:
    EmCylCaloTestSD(const std::string & nm)
        : SegmentedSensitiveDetector(nm) {}
    virtual void EndOfEvent(G4HCofThisEvent*) override;
};

}
}

