#pragma once

#include <string>
#include <cassert>

class G4Event;

namespace na64dp {

class Event;
class EvProcInfoDispatcher;

namespace calib {  // fwd only
class Manager;
class RangeOverrideRunIndex;
class GenericLoader;
}

class Pipeline;

namespace mc {  // fwd only
class EventBuilder;
}

/**\brief Processes the `na64event` produced within the MC simulation
 *
 * The processing is done using the very same pipelining technique as it is
 * performed for real data.
 *
 * The rationale behind of attaching a functional data processing pipeline
 * to the MC event source is to facilitate a complex feedback from the data,
 * i.e. various early-time cuts, lookup for rare events of specific
 * configuration, without storing the intermediate bulky data, etc.
 *
 * \todo Calibration management seems to be complex for MC case; may be changed
 * in near future.
 * */
class MCEventProcessor {
private:
    /// Calibrations manager instance
    //calib::Manager * _calibMgr;
    /// Ranges definitions instance for calibrations
    //calib::RangeOverrideRunIndex * _fsi;
    /// Generic calibration data loader
    //calib::GenericLoader * _genericCalibLoader;
    /// Online monitoring instance
    EvProcInfoDispatcher * _epi;
    /// Event processing pipeline
    Pipeline * _handlers;
    /// File descriptor for online monitor
    int _evDispFD;
    /// Calibration manager reference
    calib::Manager & _calibMgr;
public:
    /// Instantiates processor with configuration provided in YAML files
    MCEventProcessor( const std::string & runConfigFile
                    , const std::string & onlineDisplay
                    , calib::Manager & mgr
                    );

    ~MCEventProcessor();

    /// Processes MC event into `na64sw` event applying the pipeline
    virtual bool process_event(const G4Event *);

    /// Return ptr to online monitoring instance or null
    EvProcInfoDispatcher * online_monit_ptr() { return _epi; }

    /// Returns reference to calibration manager in use
    calib::Manager & calib_mgr() { return _calibMgr; }
};

}

