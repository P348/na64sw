#pragma once

#include "na64mc/gdml/processor.hh"
#include "na64mc/genericMessenger.hh"
#include "na64mc/g4api/detectorConstruction.hh"

#include <regex>

namespace na64dp {
namespace mc {

/**\brief Used for "style" type of `<auxinfo>` GDML tags, sets visual attribs
 *
 * Volume may have multiple styling tags -- they will be concatenated.
 *
 * \todo consider changing of this grammar: instead of `<style> [color]`, use the
 * list of comma-delimited attributes of following set:
 *  - `hidden`/`surface`/`wireframe`/`undefined`
 *  - `#color`
 *  - `[lineWidth|lw]=2`
 *  - `[radialSegments|rs]=35`
 *  - `[in]visibleAuxEdges`
 * */
class SetVisualAttributes : public iAuxInfoProcessor
                          , public GenericG4Messenger {
public:
    /// Regular expression for a single expression token of the styles string
    static const std::regex rxStyle;

    struct VolumeStyle {
        /// Setup name this drawing style will be applied to. May be `*` for all
        //std::string setupName;
        /// Defines way the volume should be drawn
        enum DrawingStyle {
            undefined = 0,
            hidden = 1,
            wireframe = 2,
            surface = 3,
        } drawingStyle;
        /// Color of thee volume; note that 0x0 corresponds to "unset" -- use
        /// "hidden" drawing style to disable visibility.
        uint32_t rgba;
    };

    /// Parses volume style token into `VolumeStyle` structure. On parsing
    /// failure returns pair instance with empty first element (`setupName`)
    static std::pair<std::string, VolumeStyle> parse(const std::string &);
    /// Collection of styles for certain volume
    typedef std::map<std::string, VolumeStyle> StylesDict;
    /// Parses the styles string expression into styles dictionary
    static StylesDict parse_styles(const std::string &);
    /// Overrides `target` style with defined fields of `new_`
    static void override_style( VolumeStyle & target, const VolumeStyle & new_ );
    /// Returns style for certain `setupName`; takes into account `*` style if
    /// given.
    static VolumeStyle get_style_for( const StylesDict & stylesDict
                                    , const VolumeStyle & defaultStyle
                                    , const std::string & setupName );

    /// Applies the visual style to logical volume
    static void apply_style_to( const VolumeStyle & style, G4LogicalVolume * target );
private:
    const G4String * _defaultSetupPtr;
    VolumeStyle _default;
    std::map<G4LogicalVolume *, StylesDict> _index;
public:
    SetVisualAttributes( const G4String & uiRoot
                       , const G4String * defaultSetupPtr );

    void set_default( const VolumeStyle & );

    /// Adds the logic volume styles, called by `DetectorConstruction` at the
    /// GDML processing stage
    virtual void process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                , G4LogicalVolume * lvPtr ) override;

    /**\brief Applies styles for volumes matching the given name
     *
     * `setupName` shall denote the selector name provided in head of the
     * styles section.
     * The `lvNamePat` must be either a string denoting name of certain logic
     * volume, or `*`.
     */
    void apply_styles_for( const std::string & setupName
                         , const std::string & lvNamePat);

public:
    static void ui_cmd_set_default_style( GenericG4Messenger *, const G4String & );
    static void ui_cmd_apply_styles( GenericG4Messenger *, const G4String & );
};

}
}

