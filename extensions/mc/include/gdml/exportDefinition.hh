#pragma once

#include "na64mc/gdml/processor.hh"
#include "na64mc/g4api/detectorConstruction.hh"

#include <G4ThreeVector.hh>
#include <G4GDMLReadDefine.hh>  // for G4GDMLMatrix

class G4GDMLParser;

namespace na64dp {
namespace mc {

class DetectorConstruction;

// TODO
#if 0
/**\brief Exports definitions from GDML file.
 *
 * Native Geant4 GDML parser provides a convenient mechanism to retrieve parsed
 * GDML document definitions (constants, variables, quantities, position and
 * rotation vectors, matrices), but their lifetime and accounting is
 * rudimentary. With this class we exploit auxinfo tag to "export" named
 * definitions.
 *
 * The purpose is to handle the GDML module's definitions within a dictionary
 * with persistent lifetime for subsequent usage.
 *
 * \note The structure is somewhat similar to the `G4GDMLReadDefine`, that might
 *       be further used in custom GDML read structure.
 */
class ExportGDMLDefinition : public iAuxInfoProcessor
                           , public GDMLDefinitions {
private:
    /// Reference to the object owning parser instance
    iGDMLParserOwner & _parserOwner;
    /// Reference to the definition index under consideration
    iGDMLDefinitionsIndexes & _defIdxs;
protected:
    /// Dispatches the definition extraction to the proper definitions map
    static void copy_definition( G4GDMLParser & parser
                               , const std::string & name
                               , const std::string & definitionType
                               , ExportIndex & index );
public:
    ExportGDMLDefinition( iGDMLParserOwner &, iGDMLDefinitionsIndexes & );
    ~ExportGDMLDefinition();

    /// Evaluates string and units pair provided by the auxinfo tag
    virtual void process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                , G4LogicalVolume * lvPtr ) override;
};
#endif

}
}

