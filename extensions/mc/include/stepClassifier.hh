#pragma once

#include "na64mc/genericMessenger.hh"

#include "na64util/vctr.hh"
#include "na64util/str-fmt.hh"
#include "na64mc/partsIndex.hh"

class G4Step;

namespace na64dp {
namespace mc {

// TODO: an error to be thrown for empty extractors

/**\brief A substitution string formatting assets for Geant4 strings
 *
 * Geant4 macros unavoidably interprets the `{}`-enclosed expressions as their
 * aliases. Thus, contrary to the rest of our library, we must use another
 * syntax for string interpolation.
 * */
extern const util::StringSubstFormat gGeant4StrPatFormat;

/// Single token in composite key
union EncodedStepFeature {
    std::size_t code;
    const void * pointer;
    PartsIndex::Index_t segmentIndex;

    bool operator==(EncodedStepFeature v) const {
        return code == v.code && pointer == v.pointer;
    }
};

/// A key type for runtime ...
typedef std::vector<EncodedStepFeature> StepHandlerKey;

/**\brief Extracts a features from Geant4 MC step to compose a unique key
 *
 * Uses the set of feature extractors to produce a vector of numerical IDs for
 * given step. Extractors specification is provided at the runtime via Geant4
 * UI commands.
 *
 * \todo Dedicated G4-compatible exception in case of empty extractors
 * \todo implement `ui_cmd_add_multipart()`
 * */
class StepClassifier : public GenericG4Messenger {
public:
    /// Feature extractor interface
    struct iStepFeatureExtractor {
        virtual EncodedStepFeature get_feature(const G4Step &) = 0;
        /// Writes textual token corresponding to a key and writes to dest
        /// buffer
        virtual void key_to_str(EncodedStepFeature id, char *bf, std::size_t bfLen) = 0;
    };
private:
    const std::string _name;
    std::vector<iStepFeatureExtractor *> _extractors;
public:
    /// Adds Geant4 UI commands steering the classifier instance
    StepClassifier( const G4String & name
                  , const G4String & path );
    /// Retrieves a features wrt extractors and returns a composed key
    StepHandlerKey operator()(const G4Step & step);

    ///\brief Returns a string formatted according to `fmt` with values from `key`
    ///
    /// Assuming that `key` with the same composition of extractors
    /// (equivalently, same object) this function produces a string from
    /// special "format".
    std::string format_string(const std::string & fmt, const StepHandlerKey & key);

    /// Adds a value type (extractor) to the key tuple produced by the classifier
    static void ui_cmd_add_extractor( GenericG4Messenger *
                                    , const G4String & );
    /// Adds a multipart volume to the classifier
    static void ui_cmd_add_multipart( GenericG4Messenger *
                                    , const G4String & );
};

/// A helper step classifiers dictionary
///
/// Does not create any UI cmd dir on creation, so may be safely
/// instantiated before any Gean4 UI cmd dir is created. Creating a
/// classifier, however, may rely on all the UI cmd dirs to exist.
struct StepClassifiers : public std::map<std::string, StepClassifier> {
    /// Immutable common Geant4 UI command prefix for the classifiers
    const std::string uiCmdPathPrefix;
    /// Creates an instance with common Geant4 UI prefix set.
    ///
    /// Does not create any UI cmd dir when called.
    StepClassifiers(const std::string & uiCmdPathPrefix_)
            : uiCmdPathPrefix(uiCmdPathPrefix_) {}
    /// Adds new classifier and returns reference to it.
    ///
    /// \throws GenericRuntimeError if classifier with same name exists
    StepClassifier & add_classifier(const std::string & name);
};

}

template<> struct CtrTraits<mc::StepClassifier::iStepFeatureExtractor> {
    typedef mc::StepClassifier::iStepFeatureExtractor * (*Constructor)
        ( const G4String &
        , const G4String &
        );
};

}

/**\brief Registers new MC steps extracted type for steps indexing within SD/SA
 * \ingroup vctr-defs
 *
 * Registers subclasses of `mc::StepClassifier::iStepFeatureExtractor` base to
 * be created with `VCtr`.
 *
 * \param clsName The name of the handler type to be used
 * \param name The name for "name" variable passed to a ctr; names the instance
 * \param path The name for "path" variable; Geant4 messenger UI command path
 * \param desc Representative description of this steps handler type
 */
# define REGISTER_MC_STEP_FEATURE( clsName, name, path, desc )                  \
static na64dp::mc::StepClassifier::iStepFeatureExtractor *                      \
        _new_ ## clsName ## _instance( const G4String &                         \
                                     , const G4String &                         \
                                     );                                         \
static bool _regResult_ ## clsName =                                            \
    ::na64dp::VCtr::self().register_class<na64dp::mc::StepClassifier::iStepFeatureExtractor>( \
                        # clsName, _new_ ## clsName ## _instance, desc );       \
static na64dp::mc::StepClassifier::iStepFeatureExtractor *                      \
    _new_ ## clsName ## _instance(                                              \
                __attribute__((unused)) const G4String & name                   \
              , __attribute__((unused)) const G4String & path                   \
              )

