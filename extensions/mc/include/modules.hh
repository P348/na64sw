#pragma once

#include "na64mc/genericMessenger.hh"
#include "na64mc/notifications.hh"

#include <unordered_map>
#include <typeinfo>
#include <typeindex>
#include <list>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

class ModularConfig;

template<typename T> struct UIModuleTraits;  // default is not defined

struct GenericUIModule { virtual ~GenericUIModule() {} };

template<typename T>
class UIModule : public GenericG4Messenger
               , public GenericUIModule
               , public UIModuleTraits<T>::Config {
private:
    /// Type name of the object in use
    G4String _inUse;
    /// Pointer to an object in use
    T * _objPtr;
    /// Reference to a modular configuration to operate with
    ModularConfig & _cfg;
protected:
    /// Ctr; requires a base UI path to use
    UIModule( const G4String & rootPath
            , ModularConfig & cfg
            ) : GenericG4Messenger(rootPath)
              , _objPtr(nullptr)
              , _cfg(cfg) {
        cmd<G4String>( UIModuleTraits<T>::command
                     , UIModuleTraits<T>::description
                     , "typeName"
                     , UIModuleTraits<T>::default_
                     , UIModule<T>::ui_cmd_use
                     , preinit
                     );
    }
public:
    /// Returns pointer to object under control
    T * get_ptr_in_use() { return _objPtr; }
    /// Returns pointer to object under control (const)
    const T * get_ptr_in_use() const { return _objPtr; }
    /// Returns name of the object under control
    const G4String & get_name_in_use() const { return _inUse; }

    /// Set the object to use
    static void ui_cmd_use( GenericG4Messenger * msgr_
                          , const G4String & typeName ) {
        UIModule<T> & m = dynamic_cast<UIModule<T> &>(*msgr_);
        m._objPtr = UIModuleTraits<T>::instantiate(typeName, m._cfg
                , log4cpp::Category::getInstance("na64mc.Geant4API")  // TODO: better cat, based on UIModuleTraits<T>::?
                );
        if( m._objPtr ) m._inUse = typeName;
    }

    friend class ModularConfig;
};


/**\brief General data structure for applications using the NA64-sw mc library
 *
 * Contains pointers to common data structure steered at a runtime by macro
 * files, GDML and so on.
 *
 * User side applications that require partial functions from `na64sw::mc`
 * routines may set only some pointers here to gain access without
 * instantiating the main messenger itself.
 */
class ModularConfig {
public:
    /// MC events notifier instance
    Notifier notifier;
private:
    /// Dictionary of modules by type index
    std::unordered_map<std::type_index, GenericUIModule *> _modules;
    /// List of modules that are owned by this config
    std::list<GenericUIModule *> _ownModules;
public:
    ModularConfig() {}
    ModularConfig(const ModularConfig &) = delete;

    #if 0
    na64dp::calib::RangeOverrideRunIndex _fsi; // filesystem index
    na64dp::calib::GenericLoader _genericCalibLoader;
    /// Calibration data manager instance to use
    calib::Manager calibrationsManager;
    #endif

    /// Returns module of type
    template<typename T> UIModule<T> & get_module() {
        auto it = _modules.find(typeid(T));
        if( _modules.end() == it ) {
            throw std::runtime_error("No module in runtime config.");  // TODO
        }
        return dynamic_cast<UIModule<T>&>(*(it->second));
    }

    /// Adds module of type
    template<typename T> void add_module( UIModule<T> & m ) {
        auto ir = _modules.emplace(typeid(T), &m);
        if(!ir.second) {
            throw std::runtime_error("Failed to insert runtime config module.");  // TODO
        }
    }

    /// Creates module of type and adds a pointer
    template<typename T> void enable_module( const G4String & path ) {
        UIModule<T> * m = new UIModule<T>( path, *this );
        add_module(*m);
        _ownModules.push_back(m);
    }
};

}
}

