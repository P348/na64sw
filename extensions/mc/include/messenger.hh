#pragma once

#include "na64mc/genericMessenger.hh"
#include "na64mc/modules.hh"

#include <log4cpp/Category.hh>

class G4VModularPhysicsList;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;

#if defined(ROOT_FOUND) && ROOT_FOUND
class TFile;
#endif

namespace na64dp {
namespace mc {

#if 0
class PrimaryGeneratorAction;
class RunAction;
class EventAction;
class SteppingAction;
class SensitiveDetector;
class AuxInfoIndex;
class HCDefsRegister;
class iStepHandler;
class iStepFilter;
#endif

/**\brief Main `na64sw::mc` Geant4 messenger
 *
 * Provides a number of custom commands on MC chain manipulation within the
 * `na64sw`. This class provides the runtime access to all the routines defined
 * in a library.
 *
 * Implements a notification source for runtime-instantiated apps. Run/event
 * actions uses its `notify()` method with particular arguments.
 *
 * \todo The particular mechanism of Geant4 actions instantiatiation done by
 *       this messenger class will most probably be changed in future. Namely,
 *       the `instantiate_*_action` callbacks will apparently operate with
 *       `VCtr` to instantiate a particular action. Since some of the actions
 *       are in strong connection with another, there might be another type
 *       of modular extensibility.
 *
 * \todo Implement the access class for step handler/filter index.
 *      The SA (as well as SD) must not be able to modify the content of
 *      step handlers/filters maps provided in ctr in terms of adding/removing
 *      elements (see https://stackoverflow.com/a/14816671/1734499).
 * */
class Messenger : public GenericG4Messenger {
#if 0
public:
    /// A runtime Geant4 config for NA64SW
    struct Config {
        /// An event processing configuration document location
        std::string runConfig;
        /// An online monitoring destination
        std::string onlineMonitDest;
    };
#endif
protected:
    //na64dp::calib::RangeOverrideRunIndex _fsi; // filesystem index
    //na64dp::calib::GenericLoader _genericCalibLoader;
    ModularConfig & _cfg;
public:
    /**\brief Creates the messenger instance.
     *
     * This ctr is typically called from `main()` of the C++ app. Once called
     * creates the UI cmd dirs and basic commands immediately. The
     * implementation also performs the instantiation of UI cmd modules.
     *
     * \param cfg Reference to a modular configuration instance to operate with
     * \param thisDir Geant4 UI dir name to create the commands
     * \param rootUIPath Geant4 UI base path to create dir with commands in
     * */
    Messenger( ModularConfig & cfg
             , const G4String & thisDir="na64extensions"
             , const G4String & rootUIPath="/"
             );
    virtual ~Messenger();
protected:
    //G4VModularPhysicsList * _physicsList;
    //na64dp::mc::DetectorConstruction * _detectorConstruction;

#if 0
    na64dp::mc::PrimaryGeneratorAction * _pga;
    na64dp::mc::EventAction * _eventAction;
    na64dp::mc::RunAction * _runAction;
    na64dp::mc::SteppingAction * _stepAction;
#endif
    
    #if defined(ROOT_FOUND) && ROOT_FOUND
    TFile * _rootFile;
    #endif

    log4cpp::Category & _L;

    //std::map<std::string, iStepHandler *> _stepHandlers;
    //std::map<std::string, iStepFilter *> _stepFilters;
public:
    // General system --------------------------------------------------------
    /// Loads a shared object file
    static void ui_cmd_load_module( GenericG4Messenger *, const G4String & );
    /// Set HEP random seed
    static void ui_cmd_set_heprandom_seed( GenericG4Messenger *, const G4String & );

    // Event processing ------------------------------------------------------
    /// Set the pipeline processing calibration config
    static void ui_cmd_use_calibrations( GenericG4Messenger *, const G4String & );

    // Geant4 interfaces -----------------------------------------------------
    /// Set the run action to use
    static void ui_cmd_use_run_action( GenericG4Messenger *, const G4String & );
    /// Creates physics instances for parallel worlds
    static void ui_cmd_create_parallel_physics(GenericG4Messenger *, const G4String &);
    /// Injects physics to current physics list
    static void ui_cmd_add_physics(GenericG4Messenger *, const G4String &);
    /// Assigns a physics list
    static void ui_cmd_use_ph_list(GenericG4Messenger*, const G4String &);
    /// Instantiate PGA
    static void ui_cmd_instantiate_primary_action(GenericG4Messenger*, const G4String &);
    /// Instantiate event action for data processing
    static void ui_cmd_instantiate_event_action(GenericG4Messenger*, const G4String &);

    // Geometry steering -----------------------------------------------------
    /// Sets whether the geometry must be checked vs overlaps
    static void ui_cmd_set_do_overlap_check( GenericG4Messenger *, const G4String & );
    #if 0
    /// Loads a geometry from GDML file
    static void ui_cmd_set_gdml_file( GenericG4Messenger *, const G4String & );
    /// Sets whether the GDML XML must be validated
    static void ui_cmd_set_gdml_do_validate( GenericG4Messenger *, const G4String & );
    /// Sets the setup to be used from GDML file
    static void ui_cmd_set_gdml_setup( GenericG4Messenger *, const G4String & );
    /// Sets the remote for GDML
    static void ui_cmd_set_remote( GenericG4Messenger *, const G4String & );
    #endif
    /// Prints list of available aux info processors
    static void ui_cmd_list_auxinfo( GenericG4Messenger *, const G4String & );
    /// Enable aux info tag processing command
    static void ui_cmd_enable_auxinfo_processor( GenericG4Messenger *, const G4String & );
    /// Puts the processor type in "to-be-ignored" list
    static void ui_cmd_ignore_auxinfo_processor( GenericG4Messenger *, const G4String & );

    #if defined(ROOT_FOUND) && ROOT_FOUND
    // ROOT file -------------------------------------------------------------
    /// Opens ROOT file
    static void ui_cmd_open_root_file( GenericG4Messenger *, const G4String & );
    /// Closes ROOT file
    static void ui_cmd_close_root_file(GenericG4Messenger *, const G4String &);
    #endif
};

}
}

