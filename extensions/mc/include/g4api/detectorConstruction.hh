#pragma once

#include "na64mc/genericMessenger.hh"
#include "na64mc/gdml/processor.hh"
#include "na64mc/modules.hh"

#include <G4VUserDetectorConstruction.hh>
#include <G4GDMLReadDefine.hh>  // for Matrix type
#include <G4GDMLParser.hh>

#include <stdexcept>

class G4GDMLParser;
class G4VModularPhysicsList;
class G4ParallelWorldPhysics;

namespace na64dp {
namespace mc {

#if 0
/// Index of exported GDML definitions
class GDMLDefinitions {
public:
    template<typename T> using ExportedGDMLDefinitions = std::map<std::string, T>;

    /**\brief A dictionary of definitions exported from GDML module
     *
     * \todo Unclear, whether we have to preserve constant/variable/quantity
     *       and postion/rotation categories since GDML processign collapses
     *       this info...
     * */
    struct ExportIndex {
        ExportedGDMLDefinitions<G4double> scalars;
        ExportedGDMLDefinitions<G4ThreeVector> vectors;
        ExportedGDMLDefinitions<G4GDMLMatrix> matrices;
        ExportedGDMLDefinitions<G4LogicalVolume *> volumes;
    };

    /**\brief Returns index by name
     *
     * The name of the definitions index may be used to keep track of what
     * particular module exported the definitions. Needed for complex
     * definitions import like modular GDML documents.
     */
    struct iGDMLDefinitionsIndexes {
        virtual ExportIndex & get_index( const std::string & ) = 0;
    };

    /**\brief Returns current parser instance for variables retrieval
     *
     * Common usecase implies short lifespan of the parser instance, so it is
     * decoupled from aux info processor lifespan.
     */
    struct iGDMLParserOwner {
        virtual G4GDMLParser & parser() = 0;
    };
};
#endif

template<>
struct UIModuleTraits<G4VUserDetectorConstruction> {
    static const std::string command
                           , description
                           , default_;
    typedef GDMLDocumentParameterization Config;
    static G4VUserDetectorConstruction * instantiate( const G4String & name 
                                                    , ModularConfig & msgr
                                                    , log4cpp::Category &
                                                    );
};

/**\class StandaloneGDMLDetectorConstruction
 * \brief A simple GDML detector construction class for NA64SW MC applications
 *
 * This class performs basic reading of GDML file constructing a setup as it
 * is defined in GDML, with single physical volume.
 *
 * Having pretty basic interface it accomplishes the basic needs in case of
 * standalone testing fixtures.
 */
class StandaloneGDMLDetectorConstruction : public GenericG4Messenger
                                         , public G4VUserDetectorConstruction
                                         , protected G4GDMLParser
                                         , protected AuxInfoIndex
                                         {
private:
    UIModuleTraits<G4VUserDetectorConstruction>::Config & _cfg;
public:
    /// Default ctr
    StandaloneGDMLDetectorConstruction( const G4String & rootPath
                                      , UIModuleTraits<G4VUserDetectorConstruction>::Config & cfg );
    /// Dtr deletes objects allocated on-heap: parser and messenger.
    ~StandaloneGDMLDetectorConstruction();

    /// Returns constructed geometry's world
    virtual G4VPhysicalVolume * Construct() override;

    /// Loads the GDML data from file
    ///
    /// \todo load from URI
    static void ui_cmd_load_gdml(GenericG4Messenger *, const G4String &);
    /// Sets the option for doing the checks for overlaps
    static void ui_cmd_set_overlap_check(GenericG4Messenger *, const G4String &);
};

}
}

#if 0
/* Disclaimer: declarations here are related to Geode package that is, in some
 * sense, more generic one than `na64sw', they will be migrated at some point
 * to another library. Bu now, they are compiled within `na64sw::mc` lib, but
 * live in their own namespace.
 */

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;

namespace geode {

struct ClientConfig {
    bool fCheckForOverlaps, fValidateGDML;
    /// Used for the geometry read from local file, no remote connection needed
    G4String fReadURI;
    /// Setup name in use
    G4String fSetupName;
    /// Address of the remote server performing geometry conversions
    G4String remoteAddr;
    /// Parallel worlds bound to current geometry and their setup names; it is
    /// user's responsibility to create corresponding parallel physics, if
    /// needed
    std::map<std::string, std::string> parallelWorlds;
    // ...

    ClientConfig() : fCheckForOverlaps(true)
                   , fValidateGDML(true)
                   , fSetupName("default") {}
};

/**\brief A messenger class defining commands for `/geode/...` commands family
 *
 * Provides Geant4 interface to the Geode REST API. Usage scenarios:
 *  1. One may use local GDML file directly, with the
 *  command `/geode/use-gdml-file`. This option does not require a running
 *  Geode instance.
 *  2. TODO Setup may be created on the remote server for immediate usage without
 *  indexing it (no persistent data will be left on the server) from placements
 *  file with `/geode/remote/build-setup`
 *  3. TODO Setup may be created on the remote server for reentrant usage. The
 *  lifecycle implies creation with `/geode/remote/submit-setup` command and
 *  use with `/geode/remote/use-setup` command. This scenario requires running
 *  and reachable Geode server.
 *
 * Commands (draft):
 *  - /geode/useGDMLFile <gdml-file:string>
 *      Use a local GDML file to construct the geometry. No Geode remote
 *      connection is required.
 *  - /geode/validateXML <do:bool>
 *      Makes XercesC XML parser to validate GDML against its schema. Default
 *      is `true`.
 *  - /geode/checkForOverlaps <do:bool>
 *      Makes Geant4 geometry processor to perform overlaps check once the
 *      geometry is built. Default is `true`.
 *  - /geode/useSetup <setupName:string> [parallelWorldName]
 *      Sets name of the setup to use. Default is `"default"`. If parallel
 *      world name is given, the setup will construct parallel world's
 *      geometry.
 *  - TODO: /geode/remote/set <geode-URL:string>
 *      Sets the Geode remote target to perform various geometry processing
 *      operations for other commands.
 *  - TODO: /geode/remote/setup/build <YAML-placements-file:string>
 *      Submits a YAML document describing placements for geometrical items
 *      for the immediate use. Requires a remote target (Geode server) to be
 *      set.
 *  - TODO: /geode/remote/item/list ...
 *      Fetch and print the geometry library items, with pagination.
 *  - TODO: /geode/remote/setup/list ...
 *      Fetch and print pre-defined setups available on the remote, with
 *      pagination.
 *  - TODO: /geode/remote/setup/submit <YAML-placements-file> <runID:int> [overwrite:bool=false]
 *      Submits the placements file to the server, labeled with run ID for
 *      further usage. Generates error, if placements ...?
 *  - TODO: /geode/remote/setup/build <runID:int>
 *      ...
 *
 * \note Each parallel world shall have its own physics to accomplish G4
 *      transportation routines. This is why the method `RegisterParallelPhysics`
 *      Albeit for most common cases it must be probably default one
 *      (`G4ParallelWorldPhysics`) it is apparently
 *      possible (and encouraged for some applications) to override this
 *      class. Currently, the particular mechanism is not clear, so we
 *      keep instances here, but it will probably change in future.
 *
 *  \todo Move parallel physics list out of here.
 */
class GeodeMessenger : public GenericG4Messenger {
public:
    GeodeMessenger(ClientConfig *);
    virtual ~GeodeMessenger();
    //virtual void SetNewValue(G4UIcommand*, G4String) override;
private:
    /// Target detector construction instance to operate with
    ClientConfig * fTargetConfig;
protected:  // callable targets
    static void ui_cmd_set_gdml_file( GenericG4Messenger *, const G4String & );
    static void ui_cmd_set_gdml_do_validate( GenericG4Messenger *, const G4String & );
    static void ui_cmd_set_gdml_do_overlap_check( GenericG4Messenger *, const G4String & );
    static void ui_cmd_set_gdml_setup( GenericG4Messenger *, const G4String & );
    static void ui_cmd_set_remote( GenericG4Messenger *, const G4String & );
};

}
#endif

