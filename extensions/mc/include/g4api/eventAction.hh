#pragma once

#include "na64mc/notifications.hh"
#include "na64mc/modules.hh"

#include <G4UserEventAction.hh>

namespace na64dp {

class MCEventProcessor;

namespace mc {

/**\brief Customized event action
 *
 * Dispatches the begin/end of event calls to `na64sw::mc` routines when
 * instantiated.
 *
 * Injects `na64sw::Event` info to `G4Event` if postprocessing enabled.
 * `eventSimulationStarted` notification issued *after* the event info
 * instantiated.
 *
 * \todo retrieve the event's random generators seeds stored somewhere else
 */
class EventAction : public G4UserEventAction {
private:
    Notifier & _mcNotifier;
public:
    EventAction( Notifier & nfr ) : _mcNotifier(nfr) {}

    /// Injects the `EventInformation` instance to newly produced event.
    ///\todo add the information about primary verteces to the event
    virtual void BeginOfEventAction(const G4Event* anEvent) override;

    // Called on the event processing done
    virtual void EndOfEventAction(const G4Event* anEvent) override;
};

template<>
struct UIModuleTraits<G4UserEventAction> {
    static const std::string command
                           , description
                           , default_;
    struct Config {};
    static G4UserEventAction * instantiate( const G4String & name 
                                          , ModularConfig & msgr
                                          , log4cpp::Category &
                                          );
};

}
}

