#pragma once

#include "na64mc/modules.hh"
#include "na64mc/notifications.hh"
#include "na64calib/manager.hh"

#include <G4UserRunAction.hh>

namespace na64dp {
namespace mc {

class Notifier;

/**\brief Instantiates the run object and manages its lifecycle
 *
 * When new run has to be created, instantiates a new event processing pipeline
 * according to configuration reference provided in ctr.
 *
 * \todo instantiate run post-processing configuration here instead of `Run()`
 * \todo instantiate monitoring here instead of `Run()`
 * */
class RunAction : public G4UserRunAction
                , public GenericG4Messenger {
private:
    ///\brief  Path to the postprocessing pipeline configuration file.
    ///\details Forwarded to the `runConfig` argument of the `na64dp::mc::Run` ctr
    std::string _runConfig;
    ///\brief Description string of the monitoring
    ///\details Forwarded to the `onlineMonitDest` argument of the `na64dp::mc::Run` ctr
    std::string _onlineMonitDest;
    /// Reference to the calibration manager
    calib::Manager & _calibMgr;
    /// A "run number" for generated MC data
    G4int _runNo;
    /// Reference to the MC run notifier
    Notifier & _mcNotifier;

    log4cpp::Category & _L;
public:
    /**\brief Instantiates the data processing handles wrt r/o config instance
     *
     * \param path defines the base path for run action UI commands
     * \param calibMgrPtr a reference to calibration manager instance in use
     * \param nfr a reference to MC norifier instance in use
     * \param logCat logging category instance reference
     */
    RunAction( const G4String & path
             , calib::Manager & calibMgrPtr
             , Notifier & nfr
             , log4cpp::Category & logCat
             );

    /// Returns calib manager associated
    calib::Manager & calib_manager() { return _calibMgr; }

    /**\brief Instantiates the `Run` (Geant4 API)
     *
     * Constructs a data processing pipeline, if any.
     * If `_runConfig` is empty, a basic `BaseMCRun` will be instantiated
     * instead of `na64dp::mc::Run`.
     */
    virtual G4Run* GenerateRun() override;

    /**\brief Called at run start (Geant4 API);
     *
     * Increases run number causing updating all the calibration data, sets up
     * run instance for data processing. Uses notifier to dispatch "simulation
     * started" signal.
     */
    virtual void BeginOfRunAction(const G4Run* aRun) override;

    /**\brief Called at run end (Geant4 API);
     *
     * Uses notifier to dispatch "simulation ended" signal.
     */
    virtual void EndOfRunAction(const G4Run* aRun) override;

    /// Sets the "run number" to identify current MC run's events
    ///\todo implement
    static void ui_cmd_set_run_number(GenericG4Messenger *, const G4String &);
    /// Sets the post-processing pipeline configuration
    ///\todo implement
    static void ui_cmd_set_postprocessing_config(GenericG4Messenger *, const G4String &);
    /// Sets the online monitoring destination
    ///\todo implement
    static void ui_cmd_set_monitoring(GenericG4Messenger *, const G4String &);
};

template<>
struct UIModuleTraits<G4UserRunAction> {
    static const std::string command
                           , description
                           , default_;
    struct Config {
        calib::Manager manager;
    };
    static G4UserRunAction * instantiate( const G4String & name 
                                        , ModularConfig & msgr
                                        , log4cpp::Category & logCat
                                        );
};

}
}


