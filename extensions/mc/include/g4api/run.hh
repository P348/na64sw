#pragma once

#include "na64mc/eventProcessor.hh"
#include "na64mc/eventBuilder.hh"
#include "na64mc/notifications.hh"

#include <G4Run.hh>

namespace na64dp {

class MCEventProcessor;  // fwd

namespace mc {

class BaseMCRun : public G4Run {
public:
    enum RunType {
        dry = 0,
        dataProcessingRun = 1
    };
public:
    virtual RunType run_type() const { return dry; }
};

/**\brief A common data processing handle
 *
 * The G4Run provides handles to the data processing routines during the
 * ongoing MC simulation. It is extended to keep the `na64sw` pipeline handlers
 * within. The inheritance mixing responsible for this is `MCEventProcessor`.
 *
 * \todo move run post-processing configuration instantiation to `RunAction`
 * \todo move monitoring handle instantiation to `RunAction`
 * */
class Run : public BaseMCRun
          , public MCEventProcessor
          , public EventBuilder {
private:
    Notifier & _mcNotifier;
public:
    /// Constructs the run instance with processing associated optionally
    Run( const std::string & runConfig
       , const std::string & onlineMonitDest
       , calib::Manager & mgr
       , Notifier & nfr
       , log4cpp::Category & logCat
       );
    virtual ~Run() {}

    /// Dispatches event processing, if pipeline is bound, increments
    /// `G4Run::numberOfEvent` and calibration manager's event ID, if
    /// `MCEventProcessor::process_event()` returned `true`
    virtual void RecordEvent(const G4Event * mcEvent) override;

    virtual RunType run_type() const { return dataProcessingRun; }
};

}
}

