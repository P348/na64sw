#pragma once

#include "na64mc/eventBuilder.hh"

#include <G4VUserEventInformation.hh>

namespace na64dp {
namespace mc {

class EventBuilder;
class EventAction;

/**\brief Appendix to `G4Event` instance providing handle to `na64dp::Event`
 *
 * This class provides access to current instance of `na64dp::Event` during
 * MC event simulation procedure. Primary use case is sensitive detector
 * implementation.
 *
 * Objects must be assigned to an event when processing shall be done via the
 * `G4EventManager::SetUserEventInformation()` at the very beginning of the
 * Geant4 event processing loop.
 *
 * \note Lifetime is bound to `G4Event`, G4 kernel is responsible for deletion.
 * */
class EventInformation : public G4VUserEventInformation {
public:
    /// Reference to MC event builder object
    EventBuilder & eventBuilder;
protected:
    /**\brief Protected ctr
     *
     * Event information instance is created and assigned to an MC event by
     * `EventAction::BeginOfEventAction()` and deleted by Geant4 kernel.
     * */
    EventInformation( EventBuilder & evBuilder);
public:
    #if 0  // TODO
    /// Inserts a new hit of certain type identified by detector ID returning
    /// pool reference object
    template<typename HitT> HitT & new_hit( DetID did ) {
        return EvFieldTraits<HitT>::inserter(_evBuilderRef)( event(), did,
                _namingRef );
    }
    #endif

    /// Prints instances association information
    virtual void Print() const override;

    friend class EventAction;
};

}
}

