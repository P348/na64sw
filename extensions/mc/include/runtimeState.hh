#pragma once

#include "na64calib/range-override-run-index.hh"
#include "na64mc/notifications.hh"
#include "na64mc/stepClassifier.hh"
#include "na64calib/loader-generic.hh"
#include "na64mc/modules.hh"

namespace na64dp {
namespace mc {

class DetectorConstruction;

/**\brief General data structure for applications using the NA64-sw mc library
 *
 * Contains pointers to common data structure steered at a runtime by macro
 * files, GDML and so on.
 *
 * User side applications that require partial functions from `na64sw::mc`
 * routines may set only some pointers here to gain access without
 * instantiating the main messenger itself.
 */
struct ModularConfiguration {
    #if 0
    na64dp::calib::RangeOverrideRunIndex _fsi; // filesystem index
    na64dp::calib::GenericLoader _genericCalibLoader;
    /// Calibration data manager instance to use
    calib::Manager calibrationsManager;
    /// MC events notifier instance
    Notifier notifier;
    #endif

    /// Dictionary of modules
    std::unordered_map<std::type_index, GenericUIModule *> modules;

    /// Returns module of type
    template<typename T> UIModule<T> & get_module() {
        auto it = modules.find(typeid(T));
        if( modules.end() == it ) {
            throw std::runtime_error("No module in runtime config.");  // TODO
        }
        return dynamic_cast<UIModule<T>&>(*(it->second));
    }

    /// Adds module of type
    template<typename T> void add_module( UIModule<T> & m ) {
        auto ir = modules.emplace(typeid(T), &m);
        if(!ir.second) {
            throw std::runtime_error("Failed to insert runtime config module.");  // TODO
        }
    }

    ModularConfiguration() {}
};

#if 0
// TODO? use this interface as provider for various entities needed by VCtr.
struct iRuntimeState {
    // gdml provider, gdml index, etc
    virtual DetectorConstruction & detector_construction() = 0;
    virtual calib::Manager & calibrations_manager() = 0;
    virtual Notifier & notifier() = 0;
    virtual StepClassifiers & classifiers() = 0;
};
#endif

}
}
