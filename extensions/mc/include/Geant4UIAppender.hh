#pragma once

#include "na64sw-config.h"

#if defined(Geant4_FOUND) && Geant4_FOUND
class G4UIsession;
#endif

namespace na64dp {
namespace util {

#if defined(Geant4_FOUND) && Geant4_FOUND
/**\brief A logging appender implementation forwarding output to Geant4 session
 *
 * Implements an interface defined with `log4cpp::Appender` to forward logging
 * messaging into the Geant4 "cout/cerr destinations". Requires an intevention
 * to Geant4 logging procedures with `na64sw::mc::UISession` class as it is
 * described in "7.2.4 How to control the output of G4cout/G4cerr" in Geant4
 * manual for application developers.
 *
 * This class represents a "logging sink" or destination for messages stream
 * that corresponds to current Gant4 UI session. For instance, working Qt
 * interface session has a dedicated text box on the bottom of the window to
 * print the messages.
 *
 *  * \note This appender ignores any log4cpp's parameters as it unconditionally
 *          bound to Geant4 UI session referenced in static pointer.
 * */
class Geant4UISessionAppender : public log4cpp::LayoutAppender {
public:
    static G4UIsession * session;
public:
    Geant4UISessionAppender(const std::string & name)
            : log4cpp::LayoutAppender(name) {}
    virtual bool reopen() override { return true; }
    virtual void close() override {;}

    /// A creator function (virtual constructor) to be added to log4cpp's
    /// appendders factory
    /// \note `std::auto_ptr` deprecated, but it is still a part of log4cpp API
    static Log4cppAppendersFactoryResult_t create(const log4cpp::AppendersFactory::params_t &);
protected:
    virtual void _append(const log4cpp::LoggingEvent &) override;
};
#endif

}
}

