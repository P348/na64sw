#include "na64mc/partsIndex.hh"

#include <G4VPhysicalVolume.hh>
#include <G4LogicalVolume.hh>
#include <G4Step.hh>

namespace na64dp {
namespace mc {

void
PartsIndex::define_indexing_level( const G4LogicalVolume * lvPtr
                                 , const G4String & idxStrID
                                 , const G4String & idxAppenderArg ) {
    size_t numTokEnd = 0;

    // Check that index ID is unique
    {
        auto it = std::find(_names.begin(), _names.end(), idxStrID);
        if( _names.end() != it ) {
            char errbf[128];
            snprintf(errbf, sizeof(errbf), "Multipart segmentation identifier"
                    " \"%s\" has already been defined."
                    , idxStrID.c_str() );
            G4Exception( __FUNCTION__, "NA64SW156", FatalErrorInArgument, errbf);
        }
    }
    // Appends names list
    G4int idxNum = _names.size();
    _names.push_back(idxStrID);

    iIndexAppender * app;
    if( idxAppenderArg == "#" ) {  // reserved for replica number
        app = new NReplicaIndexAppender(idxNum);
    } else {
        G4int idxCst = std::stoi( idxAppenderArg, &numTokEnd );
        if( numTokEnd != idxAppenderArg.size() ) {
            char errbf[128];
            snprintf(errbf, sizeof(errbf), "Could not interpret \"%s\" as integer"
                    " literal (index constant)."
                    , idxAppenderArg.c_str() );
            G4Exception( __FUNCTION__, "NA64SW157", FatalErrorInArgument, errbf);
        }
        app = new ConstantIndexAppender(idxNum, idxCst);
    }

    auto ir = _idxAppenders.emplace(lvPtr, app);
    assert(ir.second);
}

// redundant declaration (definition in C++ 2014), deprecated since >=C++17
constexpr PartsIndex::Index_t PartsIndex::indexMax;
constexpr uint8_t PartsIndex::nDimMax;

uint16_t
PartsIndex::Index::get(uint8_t nDim) const {
    assert(nDim <= PartsIndex::nDimMax);
    assert(is_set(nDim));
    return ((code & ((Index_t)0x3ff << (nDim*10))) >> (nDim*10)) - 1;
}

bool
PartsIndex::Index::is_set(uint8_t nDim) const {
    assert(nDim <= PartsIndex::nDimMax);
    return code & ((Index_t)0x3ff << (nDim*10));
}

void
PartsIndex::Index::reset(uint8_t nDim) {
    assert(nDim <= PartsIndex::nDimMax);
    code &= ~(((Index_t) 0x3ff << (nDim*10)));
}

void
PartsIndex::Index::set(uint8_t nDim, uint16_t value) {
    assert(nDim <= PartsIndex::nDimMax);
    assert(value <= PartsIndex::indexMax);
    reset(nDim);
    code |= ((((Index_t) value)+1) << (nDim*10));
}

//
// Appenders
///////////

void
ConstantIndexAppender::append_index( PartsIndex::Index & idx
                                   , const G4VPhysicalVolume *
                                   , G4int ) const {
    idx.set(_nDim, _N);
}

void
NReplicaIndexAppender::append_index( PartsIndex::Index & idx
                             , const G4VPhysicalVolume * phVol
                             , G4int replicaNo ) const {
    if( -1 == replicaNo /*! phVol->IsReplicated() */) {
        char errbf[256];
        snprintf(errbf, sizeof(errbf), "Volume %p \"%s\" (logical %p"
                " \"%s\") is not replicated, but expected to be by"
                " index completion routines."
                , phVol, phVol->GetName().c_str()
                , phVol->GetLogicalVolume()
                , phVol->GetLogicalVolume()->GetName().c_str() );
        G4Exception( __FUNCTION__, "NA64SW156", FatalErrorInArgument, errbf);
    }
    assert(phVol->IsReplicated());
    assert(replicaNo != -1);
    idx.set(_nDim, replicaNo);
}

//
// Simple index conversion
/////////////////////////

const std::regex PartsIndex::Index::Conversion::rxExpression("ignore|keep|\\/\\d+");

static std::pair<bool, PartsIndex::SubIndex_t> 
_identity_cnv_f(PartsIndex::SubIndex_t i, size_t) {
    return std::make_pair(true, i);
}

static std::pair<bool, PartsIndex::SubIndex_t> 
_divide_cnv_f(PartsIndex::SubIndex_t i, size_t pl) {
    return std::make_pair(true, (PartsIndex::SubIndex_t) i/pl);
}

std::vector<std::string>
PartsIndex::Index::Conversion::tokenize_expression(const std::string & expr) {
    static const std::regex tokRx(R"(,\s*)");
    return std::vector<std::string>(
            std::sregex_token_iterator{ std::begin(expr), std::end(expr), tokRx, -1 },
            std::sregex_token_iterator{}
        );
}

PartsIndex::Index::Conversion::Conversion( const std::string & expr ) {
    for( int i = 0; i < nDimMax; ++i ) {
        cnvFs[i].first = nullptr;
        cnvFs[i].second = 0x0;
    }
    auto toks = tokenize_expression(expr);
    if( toks.size() > PartsIndex::nDimMax )
        throw std::runtime_error("Conversion rule extent overflow.");  // TODO: dedicated exception?
    uint8_t nDim = 0;
    std::smatch smatch;
    for( const auto & tok : toks ) {
        if( ! std::regex_match(tok, smatch, rxExpression) )
            throw std::runtime_error("Bad token in expression.");  // TODO: dedicated exception?
        if( "keep" == tok ) {
            cnvFs[nDim] = std::make_pair(_identity_cnv_f, 0);
        } else if( '/' == tok[0] ) {
            cnvFs[nDim] = std::make_pair(_divide_cnv_f
                    , std::stoi(tok.substr(1)) );
        } else if("ignore" == tok) {
            cnvFs[nDim] = std::make_pair(nullptr, 0);
        }
        ++nDim;
    }
}

bool
PartsIndex::Index::Conversion::operator==(const Conversion & a) const {
    for( uint8_t nDim = 0; nDim < PartsIndex::nDimMax; ++nDim ) {
        if( cnvFs[nDim].first != a.cnvFs[nDim].first
         || cnvFs[nDim].second != a.cnvFs[nDim].second ) {
            return false;
        }
    }
    return true;
}

PartsIndex::Index::Conversion::Conversion() {
    for( uint8_t nDim = 0; nDim < PartsIndex::nDimMax; ++nDim ) {
        cnvFs[nDim] = std::make_pair(_identity_cnv_f, 0);
    }
}

size_t
PartsIndex::Index::Conversion::Hash::operator()(const Conversion & c) const {
    size_t hashVal = 0;
    for( uint8_t nDim = 0; nDim < PartsIndex::nDimMax; ++nDim ) {
        if( c.cnvFs[nDim].first ) {
            hashVal ^= ((size_t) c.cnvFs[nDim].first << nDim);
        }
        if( c.cnvFs[nDim].second ) {
            hashVal ^= ((size_t) c.cnvFs[nDim].second >> nDim);
        }
    }
    return hashVal;
}

std::pair<bool, PartsIndex::Index>
PartsIndex::Index::Conversion::operator()(PartsIndex::Index i) const {
    bool matches = true;
    PartsIndex::Index r;
    for( uint8_t nDim = 0; nDim < PartsIndex::nDimMax; ++nDim ) {
        if( cnvFs[nDim].first ) {
            // handle the special case of "keep the unset"
            if( (!i.is_set(nDim)) && cnvFs[nDim].first == _identity_cnv_f ) {
                r.reset(nDim);
                continue;
            }
            assert(i.is_set(nDim));
            auto p = cnvFs[nDim].first(i.get(nDim), cnvFs[nDim].second);
            if(p.first)
                r.set(nDim, p.second);
            matches &= p.first;
        }
    }
    return std::make_pair(matches, r);
}

std::string
PartsIndex::subst_index( const std::string fmt
                       , Index i
                       , char unset ) {
    std::string r(fmt);
    for( NDim_t n = 0; n < nDimMax; ++n ) {
        char strv[8];
        snprintf(strv, sizeof(strv), "$%d", (int) n);
        std::string key(strv);
        std::string value;
        size_t pos;
        while( std::string::npos != (pos = r.find(key)) ) {
            if(value.empty()) {
                if(!i.is_set(n)) {
                    if( unset == '\0' ) {
                        char errbf[128];
                        snprintf( errbf, sizeof(errbf), "Can not substitute"
                                " `$%d' in pattern \"%s\" as this dimension is"
                                " not set in index."
                                , (int) n, fmt.c_str() );
                        throw std::runtime_error(errbf);
                    } else {
                        snprintf(strv, sizeof(strv), "%c", unset );
                    }
                } else {
                    snprintf(strv, sizeof(strv), "%d", (int) i.get(n) );
                }
                value = strv;
            }
            r = r.replace( pos, key.length(), strv );
        }
    }
    return r;
}

PartsIndex::Index
PartsIndex::get_index(const G4Step * aStep) const {
    return obtain_index( *((G4TouchableHistory*) (aStep->GetPreStepPoint()->GetTouchable()))
                           , _idxAppenders);
}

PartsIndex::Index_t
PartsIndex::obtain_index( G4TouchableHistory thst
                        , const std::map<const G4LogicalVolume *, iIndexAppender *> & appenders ) {
    Index index;
    // NOTE: traversing history here mutates the step object in a way that
    // messes up the whole procedure, leaving the ghost step valid by shape,
    // but distracted by meaning. TODO: get rid of copying the touchable
    // history somehow
    while( thst.GetHistoryDepth() ) {
        // Get the touchable (physical volume)
        const G4VPhysicalVolume * phVolPtr = thst.GetVolume();
        // Get the logical volume for current touchable to obtain accessor
        const G4LogicalVolume * lVolPtr = phVolPtr->GetLogicalVolume();
        // Lookup for accessor
        auto igxGetterIt = appenders.find(lVolPtr);
        if( appenders.end() == igxGetterIt ) {
            // current logical volume is not meaningful for indexing, skip
            thst.MoveUpHistory();
            continue;
        }
        const iIndexAppender * appender = igxGetterIt->second;
        // append indexing object with information from the current touchable
        G4int replicaNo = -1;
        if( thst.GetVolume()->IsReplicated() ) {
            replicaNo = thst.GetReplicaNumber();
        }
        assert(phVolPtr);
        assert(appender);
        appender->append_index( index, phVolPtr, replicaNo );
        // move to higher touchable in hierarchy
        thst.MoveUpHistory();
    }
    return index;
}


}
}
