#include "na64mc/stepDispatcher.hh"

namespace na64dp {
namespace mc {

std::size_t
StepFeaturesHash::operator()(const StepHandlerKey & k) const {
    int i = 0;
    std::size_t h = 0x0;
    for( auto u : k ) {
        std::size_t v = u.code;
        if( (i++)%2 ) {
            v <<= 1;
        } else {
            v >>= 1;
        }
        h ^= v;
    }
    return h;
}

AbstractRuntimeStepDispatcher::AbstractRuntimeStepDispatcher( StepClassifier & kc
                                                            , iStepFilter * filterPtr_)
            : AbstractStepDispatcher<StepHandlerKey, StepFeaturesHash>(filterPtr_)
            , _keyComposer(kc) {}

StepHandlerKey
AbstractRuntimeStepDispatcher::derive_key(const G4Step * aStep){
    return _keyComposer(*aStep);
}

}
}

