#include "na64mc/eventBuilder.hh"

#include "na64detID/TBName.hh"

#include <G4Exception.hh>

namespace na64dp {
namespace mc {

void
EventBuilder::handle_update( const nameutils::DetectorNaming & naming) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(naming);
}

EventBuilder::EventBuilder( calib::Dispatcher & cdsp
                          , log4cpp::Category & logCat
                          , iEvProcInfo * epi )
            : AbstractEventSource(hitBanks, epi)
            , _cEvent(hitBanks)
            {
    cdsp.subscribe<nameutils::DetectorNaming>(*this, "default");
    // ...
}

bool
EventBuilder::read(Event & eve) {
    if( &_cEvent != &eve ) {
        G4Exception( __FUNCTION__
                   , "NA64SW215" 
                   , FatalErrorInArgument
                   , "Integrity error: the event instance managed by event"
                     " builder does not match to reentrant one."
                   );
    }
    return true;
}

}
}

