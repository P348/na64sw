#include "na64sw-config.h"

#if defined(Geant4_FOUND) && Geant4_FOUND
#include "na64mc/partsIndex.hh"

#include <gtest/gtest.h>

namespace na64dp {

// Test basic matching
TEST(MCPartsIndex, indexCoding) {
    mc::PartsIndex::Index indexObj;

    for( uint8_t i = 0; i < mc::PartsIndex::nDimMax; ++i ) {
        EXPECT_FALSE( indexObj.is_set(i) ) << "i=" << (int) i;
        indexObj.set(i, mc::PartsIndex::indexMax);
        for( uint8_t j = 0; j <= i; ++j ) {
            EXPECT_EQ( indexObj.get(j), mc::PartsIndex::indexMax ) << "i=" << (int) i << ", j=" << (int) j;
        }
        for( uint8_t p = i+1; p < mc::PartsIndex::nDimMax; ++p ) {
            EXPECT_FALSE( indexObj.is_set(p) ) << "i=" << (int) i << ", p=" << (int) p;
        }
    }
}

TEST(MCPartsIndex, acceptsValidStrings) {
    auto v = mc::PartsIndex::Index::Conversion::tokenize_expression("ignore, /1, /23, =11|142");
    EXPECT_EQ( v.size(), 4 );
    EXPECT_EQ(v[0], "ignore");
    EXPECT_EQ(v[1], "/1");
    EXPECT_EQ(v[2], "/23");
    EXPECT_EQ(v[3], "=11|142");
}

TEST(MCPartsIndex, throwsExcOnBadExpression) {
    EXPECT_THROW( mc::PartsIndex::Index::Conversion("/5, blah, keep")
                , std::runtime_error );
    EXPECT_THROW( mc::PartsIndex::Index::Conversion("keep, keep, keep, keep, /2, keep, /3")
                , std::runtime_error );
}

TEST(MCPartsIndex, buildsAValidConversion) {
    auto conversion = mc::PartsIndex::Index::Conversion("/5, keep, ignore");
    for( int i = 0; i < 20; ++i ) {
        mc::PartsIndex::Index idxIn;
        idxIn.set(0, i);
        for( int j = 0; j < 3; ++j ) {
            idxIn.set(1, j);
            for( int k = 0; k < 5; ++k ) {
                idxIn.set(2, k);
                std::pair<bool, mc::PartsIndex::Index> p = conversion(idxIn);
                EXPECT_EQ( p.second.get(0), i/5 );
                EXPECT_EQ( p.second.get(1), j );
                EXPECT_FALSE( p.second.is_set(2) );
            }
        }
    }
}

TEST(MCPartsIndex, convertsToStringByPattern) {
    mc::PartsIndex::Index i;
    i.set(1, 2);
    i.set(4, 5);
    EXPECT_EQ( "5x2", mc::PartsIndex::subst_index("$4x$1", i) );
    EXPECT_THROW( mc::PartsIndex::subst_index("$3x$1", i)
                , std::runtime_error );
}

}
#endif

