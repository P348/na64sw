#include "stepping/filters/worldLeaving.hh"

#include <G4Step.hh>

namespace na64dp {
namespace mc {

bool
WorldLeavingStepFilter::passes(const G4Step * aStep) const {
    return ! aStep->GetPostStepPoint()->GetTouchableHandle()->GetVolume();
}

// useful snippets:
// check that particle has just entered in the current volume:
//      if (point1->GetStepStatus() == fGeomBoundary)
// particle is leaving the current volume
//      if (point2->GetStepStatus() == fGeomBoundary)
// In the above situation, get touchable of the next volume
//      G4TouchableHandle touch2 = point2->GetTouchableHandle();

}
}

REGISTER_MC_STEP_FILTER( WorldLeaving, name, path
        , "Passes steps that leave the World volume."
            "Useful for determing leaks, hermiticity analysis and so on." ) {
    return new na64dp::mc::WorldLeavingStepFilter();
}

