#include "na64mc/g4api/eventAction.hh"
#include "na64mc/g4api/run.hh"
#include "na64mc/g4api/eventInformation.hh"

#include <G4RunManager.hh>

namespace na64dp {
namespace mc {

const std::string UIModuleTraits<G4UserEventAction>::command
    = "useEventAction";
const std::string UIModuleTraits<G4UserEventAction>::description
    = "Creates a Geant4 event action instance of one of predefined"
      " types: \"na64sw\" is the default for this library.";
const std::string UIModuleTraits<G4UserEventAction>::default_
    = "na64sw";

G4UserEventAction *
UIModuleTraits<G4UserEventAction>::instantiate( const G4String & name 
                                              , ModularConfig & cfg
                                              , log4cpp::Category &
                                              ) {
    if( "na64sw" != name )
        throw std::runtime_error( "Currently, only \"na64sw\" event"
                " action is supported" );
    return new EventAction( cfg.notifier );
}



void
EventAction::BeginOfEventAction(const G4Event*) {
    BaseMCRun * runPtr_ = static_cast<BaseMCRun*>(
            G4RunManager::GetRunManager()->GetNonConstCurrentRun() );
    if( BaseMCRun::dry != runPtr_->run_type() ) {
        Run * runPtr = static_cast<Run*>(runPtr_);
        EventInformation * einfo = new EventInformation( static_cast<EventBuilder&>(*runPtr) );
        G4EventManager::GetEventManager()->SetUserInformation(einfo);
        // ^^^ according to G4VUserEventManager doc, the Geant4 kernel is
        //     responsible for deletion of this object after it was forwarded to
        //     G4EventManager::SetUserEventInformation()
    }
    log4cpp::Category::getInstance("na64mc.notifications").debug(
            "Notifying of \"event started\".");
    _mcNotifier.notify(Notification::eventSimulationStarted);
}

void
EventAction::EndOfEventAction(const G4Event*) {
    log4cpp::Category::getInstance("na64mc.notifications").debug(
            "Notifying of \"event ended\".");
    _mcNotifier.notify(Notification::eventSimulationEnded);
}

}
}

