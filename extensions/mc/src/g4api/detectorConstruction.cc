#include "na64mc/g4api/detectorConstruction.hh"
#include "na64mc/gdml/parallelWorld.hh"

#include <G4RunManager.hh>
#include <G4GDMLParser.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4VModularPhysicsList.hh>
#include <G4ParallelWorldPhysics.hh>

namespace na64dp {
namespace mc {

const std::string UIModuleTraits<G4VUserDetectorConstruction>::command
    = "useDetectorConstruction";
const std::string UIModuleTraits<G4VUserDetectorConstruction>::description
    = "Creates a Geant4 detector construction instance of one of predefined"
      " types: \"StandaloneGDML\" is the default for this library.";
const std::string UIModuleTraits<G4VUserDetectorConstruction>::default_
    = "StandaloneGDML";

G4VUserDetectorConstruction *
UIModuleTraits<G4VUserDetectorConstruction>::instantiate( const G4String & name 
                                                        , ModularConfig & cfg
                                                        , log4cpp::Category & //logCat
                                                        ) {
    if( "StandaloneGDML" != name )
        throw std::runtime_error( "Currently, only \"StandaloneGDML\" detector"
                " construction is supported" );
    auto & thisModule = cfg.get_module<G4VUserDetectorConstruction>();
    auto dcPtr = new StandaloneGDMLDetectorConstruction(
            thisModule.GetRootPath(),
            thisModule
            );
    G4RunManager::GetRunManager()->SetUserInitialization(dcPtr);
    return dcPtr;
}


StandaloneGDMLDetectorConstruction::StandaloneGDMLDetectorConstruction(
                const G4String & uiCmdPath, UIModuleTraits<G4VUserDetectorConstruction>::Config & cfg )
        : GenericG4Messenger(uiCmdPath)
        , _cfg(cfg)
        {
    //fGDMLParser = new G4GDMLParser();  // TODO: read/write structures here
    dir("detector", "Commands and switches to steer the geometry build-up of"
               " `na64sw' detector construction instance.")
        .cmd<G4bool>( "setOverlapCheck"
                , "Enables or disables check for overlaps within the loaded"
                  " geometry."
                , "flag"
                , ui_cmd_set_overlap_check )
        .cmd( "loadGDML", "Load the GDML file.", ui_cmd_load_gdml )
            .par<G4String>( "uri", "GDML document URI" )
            .par<G4String>( "setup", "A setup name to take from GDML", "Default" )
            .par<G4bool>( "doValidateXML"
                        , "Enables or disables the cheking of XML code conformity"
                          " with respect to GDML schema. Ususally, requires an"
                          " active internet connection."
                        , false )
        .end( "loadGDML" )
    .end("detector");
}

StandaloneGDMLDetectorConstruction::~StandaloneGDMLDetectorConstruction() {
}

G4VPhysicalVolume *
StandaloneGDMLDetectorConstruction::Construct() {
    return _cfg.worldPtr;
}

void
StandaloneGDMLDetectorConstruction::ui_cmd_load_gdml( GenericG4Messenger * msgr_
                                                    , const G4String & strExpr
                                                    ) {
    StandaloneGDMLDetectorConstruction & dc
        = dynamic_cast<StandaloneGDMLDetectorConstruction&>(*msgr_);
    std::string uri;
    G4bool doValidate; {
        G4String doValidate_;
        std::istringstream iss(strExpr);
        iss >> uri >> dc._cfg.setupName >> doValidate_;
        doValidate = G4UIcmdWithABool::GetNewBoolValue(doValidate_);
    }
    dc.G4GDMLParser::Read( uri, doValidate );
    // process logical volume auxinfo
    for( auto lvPtr : *G4LogicalVolumeStore::GetInstance() ) {
        const auto & info = dc.G4GDMLParser::GetVolumeAuxiliaryInformation(lvPtr);
        if( info.empty() ) continue;  // no aux info
        dc.process_auxinfo( lvPtr, info );
    }
    // process the "global" aux info
    if( ! dc.G4GDMLParser::GetAuxList()->empty() ) {
        dc.process_auxinfo(nullptr, *(dc.G4GDMLParser::GetAuxList()));
    }
    dc._cfg.worldPtr = dc.G4GDMLParser::GetWorldVolume(dc._cfg.setupName);
}

void
StandaloneGDMLDetectorConstruction::ui_cmd_set_overlap_check( GenericG4Messenger * msgr_
                                              , const G4String & arg
                                              ) {
    StandaloneGDMLDetectorConstruction & dc = dynamic_cast<StandaloneGDMLDetectorConstruction&>(*msgr_);
    dc.G4GDMLParser::SetOverlapCheck(G4UIcmdWithABool::GetNewBoolValue(arg));
}

#if 0
void
DetectorConstruction::ui_cmd_add_parallel_world( GenericG4Messenger * 
                                               , const G4String & ) {
    DetectorConstruction & dc = dynamic_cast<DetectorConstruction&>(*msgr_);

    GDMLParallelWorld * pWorld = new GDMLParallelWorld(
                worldName, setupName, fGDMLParser );

    // add the parallel worlds
    for( auto pWorldPair : fGeodeCfg.parallelWorlds ) {
        const std::string & worldName = pWorldPair.first
                        , & setupName = pWorldPair.second;
        GDMLParallelWorld * pWorld = new GDMLParallelWorld(
                worldName, setupName, fGDMLParser );
        log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.Geant4API"));
        L << log4cpp::Priority::DEBUG
            << "New parallel world instance \"" << pWorld->GetName()
            << "\" created.";
        RegisterParallelWorld(pWorld);
    }
}
#endif

}
}

#if 0
#include "na64mc/geode/messenger.hh"
#include "na64mc/g4api/detectorConstruction.hh"

#include <G4UIdirectory.hh>
#include <G4UIcmdWithAString.hh>
#include <G4UIcmdWithABool.hh>
#include <G4Exception.hh>

#include <G4VUserParallelWorld.hh>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

namespace geode {

GeodeMessenger::GeodeMessenger(ClientConfig * cfg)
            : GenericG4Messenger()
            , fTargetConfig(cfg) {
    dir( "geode", "Geode detector construction API commands." )
        .cmd<G4String>( "useGDMLFile"
                        , "Loads local GDML file to build up geometry."
                        , "filePath"
                        , ui_cmd_set_gdml_file
                        , preinit | init | idle )
        .cmd<G4bool>( "validate"
                        , "Enables/disables the GDML document validation."
                        , "validationFlag"
                        , ui_cmd_set_gdml_do_validate
                        , preinit | init | idle
                        )
        .cmd<G4bool>( "checkForOverlaps"
                        , "Sets whether to check the constructed geometry for overlaps"
                        , "overlapCheckFlag"
                        , true
                        , ui_cmd_set_gdml_do_overlap_check
                        , preinit | init | idle
                        )
        .cmd( "useSetup"
            , "Uses setup from current GDML."
            , ui_cmd_set_gdml_setup
            , preinit | init | idle 
            )
            .par<G4String>( "setupName"
                          , "Name of the setup declared in GDML"
                            " document to be used." )
            .par<G4String>( "parallelWorldName"
                          , "Name of the parallel world to create."
                            "Put \"-\" or omit this parameter for mass world."
                          , "-" )
            .par<G4String>( "parallelWorldPhysicsName"
                          , "Name of the parallel world physics"
                            " to bind. Use \"default\" or omit this parameter for"
                            " default one (standard Geant4 parallel physics)."
                          , "default" )
        .end( "useSetup" )
        .dir( "remote", "Geode remote server interaction commands." )
            .cmd<G4String>( "set"
                          , "Sets the remote Geode URL to use."
                          , "url"
                          , ui_cmd_set_remote
                          , preinit | init | idle
                          )
        .end( "remote" )
    .end( "geode" );
}

GeodeMessenger::~GeodeMessenger() {
}

void
GeodeMessenger::ui_cmd_set_gdml_file( GenericG4Messenger * msgr_
                                    , const G4String & strVal ) {
    GeodeMessenger & msgr = *static_cast<GeodeMessenger*>(msgr_);
    {   // assure the file is reachable
        struct stat bf;
        int f = open( strVal, O_RDONLY );
        int status = fstat(f, &bf);
        if( 0 != status ) {  // file access failed
            int eac = errno;
            char errBf[128];
            snprintf( errBf, sizeof(errBf)
                    , "Bad local GDML file \"%s\": (%d) %s"
                    , strVal.data()
                    , eac
                    , strerror(eac)
                    );
            G4Exception( __FUNCTION__
                       , "Geode001" 
                       , FatalErrorInArgument
                       , errBf
                       );
        }
        close(f);
    }
    // Set URI to the local file path
    msgr.fTargetConfig->fReadURI = strVal;
}

void
GeodeMessenger::ui_cmd_set_gdml_do_validate( GenericG4Messenger * msgr_
                                           , const G4String & strVal ) {
    GeodeMessenger & msgr = *static_cast<GeodeMessenger*>(msgr_);
    msgr.fTargetConfig->fValidateGDML = G4UIcmdWithABool::GetNewBoolValue(strVal);
}

void
GeodeMessenger::ui_cmd_set_gdml_do_overlap_check( GenericG4Messenger * msgr_
                                                , const G4String & strVal ) {
    GeodeMessenger & msgr = *static_cast<GeodeMessenger*>(msgr_);
    msgr.fTargetConfig->fCheckForOverlaps = G4UIcmdWithABool::GetNewBoolValue(strVal);
}

void
GeodeMessenger::ui_cmd_set_gdml_setup( GenericG4Messenger * msgr_
                                     , const G4String & strVal ) {
    GeodeMessenger & msgr = *static_cast<GeodeMessenger*>(msgr_);
    std::string setupName
              , worldName
              , physicsName
              ;
    {
        std::istringstream iss(strVal);
        iss >> setupName >> worldName >> physicsName;
    }
    if( "-" == worldName || worldName.empty() ) {
        msgr.fTargetConfig->fSetupName = setupName;
    } else {
        msgr.fTargetConfig->parallelWorlds[worldName] = setupName;
    }
}

void
GeodeMessenger::ui_cmd_set_remote( GenericG4Messenger * //msgr_
                                 , const G4String & //strVal
                                 ) {
    throw std::runtime_error( "\"set server\" is not yet implemented" );
    //GeodeMessenger & msgr = *static_cast<GeodeMessenger*>(msgr_);
}

}
#endif

