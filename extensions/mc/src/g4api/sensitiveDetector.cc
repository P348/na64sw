#include "na64mc/g4api/sensitiveDetector.hh"
#include "na64mc/g4api/eventInformation.hh"

#include <G4EventManager.hh>

namespace na64dp {
namespace mc {

SensitiveDetector::SensitiveDetector( const G4String & sdName )
        : G4VSensitiveDetector( sdName ) {
}

EventInformation *
SensitiveDetector::get_einfo() {
    EventInformation * einfo = nullptr;
    {  // downcast
        G4VUserEventInformation * einfoPtr_
            = G4EventManager::GetEventManager()->GetUserInformation();
        if(einfoPtr_) {
            einfo = static_cast<EventInformation *>(einfoPtr_);
        }
    }
    return einfo;
}

}
}

