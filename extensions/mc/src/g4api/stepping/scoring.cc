#include "na64mc/g4api/steppingAction.hh"

namespace na64dp {
namespace mc {

// TODO: see note in scalarGetter.hh

/**\brief A Geant4 steps handler collecting step value per some track criterion
 *
 * Being added, collects value integral sorted by some step feature (material,
 * particle charge, PDG code) in the entire Geant4 world. Besides of direct sum
 * of some scalar value, accumulates the sum of squared value per event thus
 * giving meaningful info on fluctuations.
 */
template<typename KeyT>
class CumulativeScalar : public SteppingAction::iHandler
                       , public geode::GenericG4Messenger
                       , public iNotificationDestination
                       , public StepGetterMixin
                       {
private:
    /// Scorers index
    std::unordered_map<KeyT, CumulativeValue> _scorers;
public:
    CumulativeScalar( const G4String & name
                    , const G4String & path
                    , Notifier & nfr );

    virtual void account(const G4Step * aStep) override;

    /// Writes collected data to file
    static void ui_cmd_dump_ascii(GenericG4Messenger*, const G4String &);
};

//
// Implemenation

template<typename KeyT>
CumulativeScalar::CumulativeScalar( const G4String & name
                                  , const G4String & path
                                  , Notifier & nfr
                                  )
        : GenericG4Messenger( path )
        , iNotificationDestination( Notification::eventSimulationStarted
                                  | Notification::eventSimulationEnded
                                  | Notification::runSimulationEnded
                                  , nfr )
{

    cmd<G4String>( "write"
                 , "Write collected numbers in file with name provided."
                   " The output file has 3 columns: material name, value"
                   " collected and sum of squared value per event collected."
                 , "filename"
                 , ui_cmd_dump_ascii
                 , anyAppState
                 )
    ;
}

template<typename KeyT> void
CumulativeScalar::account(const G4Step * aStep) {
    auto k = StepGroupingTraits<KeyT>::get_key(aStep);
    auto it = _scorers.find(k);
    if( _scorers.end() == it ) {
        // ...
    }
}

template<typename KeyT> void
CumulativeScalar::ui_cmd_dump_ascii(GenericG4Messenger*, const G4String &) {
    // ...
}

}
}

