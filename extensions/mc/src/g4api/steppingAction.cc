#include "na64mc/g4api/steppingAction.hh"
#include "na64util/str-fmt.hh"

#include <G4Step.hh>
#include <G4UnitsTable.hh>
#include <G4RunManager.hh>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

//
// UI module

UIModule<G4UserSteppingAction>::UIModule( const G4String & rootPath
                                        , ModularConfig & cfg
                                        ) : GenericG4Messenger(rootPath)
                                          , StepClassifiers( rootPath + NA64SW_MC_DIRNAME_STEPPING + "/" + NA64SW_MC_DIRNAME_STEPPING_CLASSIFIERS + "/" )
                                          , _objPtr(nullptr)
                                          , _cfg(cfg) {
    cmd<G4String>( "useSteppingAction"
                 , "Creates a Geant4 stepping action instance of one of predefined"
                   " types: \"na64sw\" is the default for this library."
                   " Must be called after physics list is assigned."
                 , "typeName"
                 , ui_cmd_instantiate_stepping_action
                 , preinit )
    .dir( NA64SW_MC_DIRNAME_STEPPING
        , "Control over various stepping routines. Create and configure"
          " handlers, filters and classifiers for sensitive detectors and"
          " stepping action."
          " Created objects will be added to appropriate UI-cmd subdir with"
          " their own subset of commands available for configure."
          " These assets may be used with `na64sw' stepping action or with"
          " (generalized) sensitive detectors of special kind.")
        .dir(NA64SW_MC_DIRNAME_STEPPING_HANDLERS, "MC step handlers.")
        .end(NA64SW_MC_DIRNAME_STEPPING_HANDLERS)
        .dir(NA64SW_MC_DIRNAME_STEPPING_FILTERS, "MC step filters.")
        .end(NA64SW_MC_DIRNAME_STEPPING_FILTERS)
        .dir(NA64SW_MC_DIRNAME_STEPPING_CLASSIFIERS, "MC step classifiers.")
        .end(NA64SW_MC_DIRNAME_STEPPING_CLASSIFIERS)
        .cmd( "createHandler"
            , "Creates new steps handler of named type for subsequent"
              " usage. Name is used then to identify and configure the"
              " steps handler."
            , ui_cmd_create_step_handler
            )
            .par<G4String>( "handlerType"
                  , "Handler type identifier (one of available by /list)"
                  )
            .par<G4String>( "handlerName"
                  , "Name of the handler instance."
                  )
        .end( "createHandler" )
        .cmd( "createClassifyingHandler"
            , "Creates new classifying handler of named type for subsequent"
              " usage. Name is used then to identify and configure the"
              " steps handler."
            , ui_cmd_create_classifying_step_handler
            )
            .par<G4String>( "handlerType"
                  , "Handler type identifier (one of available by /list)"
                  )
            .par<G4String>( "classifierName"
                  , "Name of existing classifier to bind the handler with."
                  )
            .par<G4String>( "handlerName"
                  , "Name of the handler instance."
                  )
        .end( "createClassifyingHandler" )
        .cmd_nopar( "listAvailableHandlers"
                  , "Prints list of available step handler classes."
                  , ui_cmd_list_step_handlers_classes
                  , anyAppState
                  )
        .dir( "handlers", "Commands related to step handlers." ).end( "handlers" )
        // filters -------------------------------------------------------
        .cmd( "createFilter"
            , "Creates new step filter of named type to restrict"
              " consideration with certain sort of steps: PDG code,"
              " energy threshold, charge, etc."
            , ui_cmd_create_step_filter
            )
            .par<G4String>( "filterType"
                  , "Filter type identifier (one of available by /list)"
                  )
            .par<G4String>( "filterName"
                  , "Name of the filter instance."
                  )
        .end( "createFilter" )
        .cmd_nopar( "listAvailableFilters"
                  , "Prints list of available step handler classes."
                  , ui_cmd_list_step_filters_classes
                  , anyAppState
                  )
        .dir( "filters", "Commands related to step filters." ) .end( "filters" )
        // classifiers ---------------------------------------------------
        .cmd<G4String>( "defineClassifier"
                , "Defines a new classifier instance to be used in step"
                  " handlers that support it. Once created with certain"
                  " name, the classifier will become available for"
                  " parameterisation using this path + name/ commands."
                , "classifierName"
                , "An unique name for the classifier."
                , ui_cmd_define_step_classifier )
        .dir( "classifiers", "Step classifiers definitions and settings." ).end( "classifiers" )
    .end( NA64SW_MC_DIRNAME_STEPPING )
    ;
}

void
UIModule<G4UserSteppingAction>::ui_cmd_instantiate_stepping_action(
        GenericG4Messenger * msgr_, const G4String & name ) {
    if( "na64sw" != name )
        throw std::runtime_error( "Currently, only \"na64sw\" stepping"
                " action is supported" );
    auto & thisModule = dynamic_cast<UIModule<G4UserSteppingAction>&>(*msgr_);
    thisModule._objPtr = new SteppingAction( thisModule.GetRootPath()
                                           , thisModule.handlers
                                           , thisModule.filters
                                           );
    thisModule._inUse = name;
    G4RunManager::GetRunManager()->SetUserAction(thisModule._objPtr);
}


void
UIModule<G4UserSteppingAction>::ui_cmd_list_step_handlers_classes( GenericG4Messenger *
                                                                 , const G4String &
                                                                 ) {
    auto reg = VCtr::self().registry<iStepHandler>();
    for( auto & p : reg ) {
        G4cout << " * \"" << p.first << "\" : " << p.second.second
               << G4endl;
    }
}

void
UIModule<G4UserSteppingAction>::ui_cmd_create_step_handler( GenericG4Messenger * msgr_
                                                          , const G4String & strExpr
                                                          ) {
    assert(msgr_);
    UIModule<G4UserSteppingAction> & thisModule
                = dynamic_cast<UIModule<G4UserSteppingAction> &>(*msgr_);
    std::string clsName, name;
    {
        std::istringstream iss(strExpr);
        iss >> clsName >> name;
    }
    const G4String handlersRoot = thisModule.GetRootPath()
                                + NA64SW_MC_DIRNAME_STEPPING
                                + "/"
                                + NA64SW_MC_DIRNAME_STEPPING_HANDLERS
                                ;
    iStepHandler * newHandler = VCtr::self().make<iStepHandler>( clsName
               , name
               , handlersRoot
               , thisModule._cfg.notifier
               // ...
               );
    assert(newHandler);
    {  // warning if eponymous exists
        auto it = thisModule.handlers.find(name);
        if( it != thisModule.handlers.end() ) {
            log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::WARN
                << "Re-assigning the existing step handler with name \""
                << name << "\".";
        }
    }
    thisModule.handlers[name] = newHandler;
    log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::INFO
        << "Step handler \"" << name << "\" of type " << clsName << " created.";
}

void
UIModule<G4UserSteppingAction>::ui_cmd_create_classifying_step_handler( GenericG4Messenger * msgr_
                                                                      , const G4String & strExpr
                                                                      ) {
    assert(msgr_);
    UIModule<G4UserSteppingAction> & thisModule
                = dynamic_cast<UIModule<G4UserSteppingAction> &>(*msgr_);
    std::string clsName, classifierName, name;
    {
        std::istringstream iss(strExpr);
        iss >> clsName >> classifierName >> name;
    }
    auto chIt = thisModule.StepClassifiers::find(classifierName);
    if( thisModule.StepClassifiers::end() == chIt ) {
        throw std::runtime_error("Non-existing classifier requested.");  // TODO: G4-error
    }
    
    const G4String handlersRoot = thisModule.GetRootPath()
                                + NA64SW_MC_DIRNAME_STEPPING
                                + "/"
                                + NA64SW_MC_DIRNAME_STEPPING_HANDLERS
                                ;
    iStepHandler * newHandler
        = VCtr::self().make<AbstractRuntimeStepDispatcher>( clsName
                    , name
                    , handlersRoot
                    , thisModule._cfg.notifier
                    , chIt->second
                    );
    assert(newHandler);
    {  // warning if eponymous exists
        auto it = thisModule.handlers.find(name);
        if( it != thisModule.handlers.end() ) {
            log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::WARN
                << "Re-assigning the existing step handler with name \""
                << name << "\".";
        }
    }
    thisModule.handlers[name] = newHandler;
    log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::INFO
        << "Step handler \"" << name << "\" of type " << clsName << " created.";
}


void
UIModule<G4UserSteppingAction>::ui_cmd_list_step_filters_classes( GenericG4Messenger *
                                                                , const G4String &
                                                                ) {
    auto reg = VCtr::self().registry<iStepFilter>();
    for( auto & p : reg ) {
        G4cout << " * \"" << p.first << "\" : " << p.second.second
               << G4endl;
    }
}

void
UIModule<G4UserSteppingAction>::ui_cmd_create_step_filter( GenericG4Messenger * msgr_
                                                         , const G4String & strExpr
                                                         ) {
    assert(msgr_);
    UIModule<G4UserSteppingAction> & thisModule
                = dynamic_cast<UIModule<G4UserSteppingAction> &>(*msgr_);
    std::string clsName, name;
    {
        std::istringstream iss(strExpr);
        iss >> clsName >> name;
    }
    const G4String filtersRoot = thisModule.GetRootPath()
                               + "/"
                               + NA64SW_MC_DIRNAME_STEPPING_FILTERS
                               ;
    iStepFilter * newFilter = VCtr::self().make<iStepFilter>(
                                                         clsName
                                                       , name
                                                       , filtersRoot
                                                       //, msgr
                                                       // ...
                                                       );
    assert(newFilter);
    {  // warning if eponymous exists
        auto it = thisModule.filters.find(name);
        if( it != thisModule.filters.end() ) {
            log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::WARN
                << "Re-assigning the existing step filter with name \""
                << name << "\".";
        }
    }
    thisModule.filters[name] = newFilter;
    log4cpp::Category::getInstance("na64mc.stepping") << log4cpp::Priority::INFO
        << "Step filter \"" << name << "\" of type " << clsName << " created.";
}


void
UIModule<G4UserSteppingAction>::ui_cmd_define_step_classifier( GenericG4Messenger * msgr_
                                                             , const G4String & strExpr
                                                             ) {
    assert(msgr_);
    UIModule<G4UserSteppingAction> & thisModule
                = dynamic_cast<UIModule<G4UserSteppingAction> &>(*msgr_);
    thisModule.add_classifier(strExpr);
}


//
// Stepping Action

SteppingAction::SteppingAction( const std::string & macroCmdPrefix
                              , std::map<std::string, iStepHandler *> & handlers
                              , std::map<std::string, iStepFilter *> & filters
                              )
        : GenericG4Messenger( macroCmdPrefix )
        , _handlers(handlers)
        , _filters(filters)
        {
    log4cpp::Category::getInstance("na64mc.g4api").info(
            "Stepping action %p instantiated.", this );
    cmd<G4String>( "handle"
                 , "Adds a handler to stepping action."
                 , "handlerName"
                 , "Name of (existing) handler."
                 , ui_cmd_add_handler
       )
    .cmd( "handleFilteredSteps"
        , "Adds new filter+handler pair to stepping action."
        , ui_cmd_add_filtered_handler
        )
        .par<G4String>( "filterName"
                      , "Name of (existing) filter to apply to the handler." )
        .par<G4String>( "handlerName"
                      , "Name of (existing) handler to apply." )
    .end( "handleFilteredSteps" )
    ;
}

void
SteppingAction::UserSteppingAction(const G4Step * aStep) {
    // TODO: optimize filter appliocation using the `equal_range`
    for( const auto & p : _destinations ) {
        if( p.first && ! p.first->passes(aStep) )
            continue;  // discriminated by filter
        p.second->handle_step(aStep);
    }
}

void
SteppingAction::ui_cmd_add_handler( GenericG4Messenger * msgr_
                                  , const G4String & strExpr
                                  ) {
    SteppingAction & sa = dynamic_cast<SteppingAction &>(*msgr_);
    auto it = sa._handlers.find(strExpr);
    if( sa._handlers.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Handler \"%s\" does not exist."
                            , strExpr.c_str()
                            );
    }
    sa._destinations.emplace(nullptr, it->second);
}

void
SteppingAction::ui_cmd_add_filtered_handler( GenericG4Messenger * msgr_
                                           , const G4String & strExpr
                                           ) {
    SteppingAction & sa = dynamic_cast<SteppingAction &>(*msgr_);
    std::string filterName, handlerName;
    {
        std::istringstream iss(strExpr);
        iss >> filterName >> handlerName;
    }
    auto fIt = sa._filters.find(filterName);
    if( sa._filters.end() == fIt ) {
        NA64DP_RUNTIME_ERROR( "Filter \"%s\" does not exist."
                            , filterName.c_str()
                            );
    }
    auto hIt = sa._handlers.find(handlerName);
    if( sa._handlers.end() == hIt ) {
        NA64DP_RUNTIME_ERROR( "Handler \"%s\" does not exist."
                            , handlerName.c_str()
                            );
    }
    sa._destinations.emplace(fIt->second, hIt->second);
}

}  // namespace ::na64dp::mc
}  // namespace ::na64dp

