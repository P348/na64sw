#include "na64mc/g4api/processes/stepMaxLimiter.hh"

#include <G4VPhysicalVolume.hh>
#include <G4TransportationProcessType.hh>

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

StepMax::StepMax()
        : G4VEmProcess("UserMaxStep", fGeneral)
        , fMaxChargedStep(DBL_MAX)
        , isInitialised(false) {
    SetProcessSubType(static_cast<G4int>(STEP_LIMITER));
}

StepMax::~StepMax() {}

G4bool
StepMax::IsApplicable(const G4ParticleDefinition& part) {
    return (part.GetPDGCharge() != 0. && !part.IsShortLived());
}

void
StepMax::PreparePhysicsTable(const G4ParticleDefinition&) {
    if(isInitialised) {
        isInitialised = false;
    }
}

void
StepMax::BuildPhysicsTable(const G4ParticleDefinition&) {
    if(!isInitialised) {
        fMaxChargedStep = 1*CLHEP::mm;  // fMessenger->GetMaxChargedStep();
        isInitialised = true;
        if(fMaxChargedStep < DBL_MAX) {
            log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.Geant4API"));
            L << log4cpp::Priority::INFO
                << GetProcessName() << ":  SubType= " << GetProcessSubType()
                << "  Step limit(mm)= " << fMaxChargedStep;
        }
    }
}

void StepMax::InitialiseProcess(const G4ParticleDefinition*) {}

G4double
StepMax::PostStepGetPhysicalInteractionLength( const G4Track&
        , G4double
        , G4ForceCondition* condition ) {
    // condition is set to "Not Forced"
    *condition = NotForced;
    return fMaxChargedStep;
}

G4VParticleChange *
StepMax::PostStepDoIt(const G4Track& aTrack, const G4Step&) {
    // do nothing
    aParticleChange.Initialize(aTrack);
    return &aParticleChange;
}

}
}

