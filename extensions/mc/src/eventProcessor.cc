#include "na64app/app.hh"
#include "na64mc/eventProcessor.hh"
#include "na64mc/eventBuilder.hh"
#include "na64mc/g4api/eventInformation.hh"

#include "na64dp/pipeline.hh"
#include "na64dp/processingInfo.hh"

#include "na64dp/processingInfo.hh"
//#if defined(ZMQ_FOUND) && ZMQ_FOUND
//#   include "na64dp/processingInfoNw.hh"
//#endif

#include <G4Exception.hh>
#include <G4Event.hh>

#include <unistd.h>

namespace na64dp {

MCEventProcessor::MCEventProcessor( const std::string & runConfigFile
                                  , const std::string & onlineDisplayDestination
                                  , calib::Manager & mgr
                                  )
        : _epi(nullptr)
        , _handlers(nullptr)
        , _evDispFD(-1)
        , _calibMgr(mgr) {
    assert(!runConfigFile.empty());
    // Load to the YAML::Node object describing pipeline
    YAML::Node cfgRoot;
    try {
        cfgRoot = YAML::LoadFile( runConfigFile );
    } catch( std::exception & e ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Error occured while accessing, reading or parsing file \"%s\": %s"
                , runConfigFile.c_str()
                , e.what() );
        G4Exception( __FUNCTION__
                   , "NA64SW300"
                   , JustWarning
                   , errBf
                   );
        throw;
    }

    //                                                       __________________
    // ____________________________________________________/ Online monitoring
    _epi = nullptr;
    _evDispFD = -1;  // keeps file descriptor for ASCII display, if any
    #if 0  // TODO
    if( ! onlineDisplayDestination.empty() ) {
        if( '@' == onlineDisplayDestination[0] ) {
            //#if defined(ZMQ_FOUND) && ZMQ_FOUND
            //_epi = new na64dp::NwPubEventProcessingInfo(
            //          atoi( onlineDisplayDestination.substr(1).c_str() )
            //        , 0
            //        , *_banks
            //        );
            //# else
            throw std::runtime_error( "Network monitoring is not supported"
                    " by this build since it is configured without ZeroMQ." );
            //# endif
        } else {
            int fd;
            if( "-" == onlineDisplayDestination ) {
                _evDispFD = fd = open( onlineDisplayDestination.c_str()
                               , O_CREAT | O_WRONLY | O_TRUNC
                               , S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
                               );
                
            } else {
                fd = STDIN_FILENO;
            }
            _epi = new na64dp::PrintEventProcessingInfo( fd
                        //, *_banks
                        , 0
                        );
        }
    }
    #endif
    
    //                                              ___________________________
    // ___________________________________________/ Create the pipeline object
    _handlers = new na64dp::Pipeline( cfgRoot["pipeline"]
                                    //, *_banks
                                    , _calibMgr
                                    , _epi );
    if( _handlers->empty() ) {
        G4Exception( __FUNCTION__
                   , "NA64SW303" 
                   , JustWarning
                   , "Empty data processing pipeline has been built."
                   );
    }

    //if(_epi) _epi->start();
}

MCEventProcessor::~MCEventProcessor() {
    if( _handlers ) delete _handlers;  // shall call the finalize() of handlers
    if( _epi ) {
        _epi->finalize();
        delete _epi;
    }
    if( _evDispFD > 0 && !isatty(_evDispFD) ) {
        close(_evDispFD);
    }
    //if(_banks) delete _banks;
}

bool
MCEventProcessor::process_event(const G4Event * mcEventPtr) {
    G4VUserEventInformation * einfo_ = mcEventPtr->GetUserInformation();
    if( !einfo_ ) {
        G4Exception( __FUNCTION__
                   , "NA64SW216" 
                   , FatalErrorInArgument
                   , "Unable to post-process the event: no user information"
                     " associated with G4Event instance."
                   );
    }
    mc::EventInformation & einfo = *static_cast<mc::EventInformation *>(einfo_);
    // Run the pipeline
    NA64DP_RUNTIME_ERROR("TODO");
    //_mcSource->read(einfo.event());  // asures we use the same event instance
    //Event & eventRef = einfo.eventBuilder.current_event();
    //_handlers->process(eventRef);
    //eventRef.clear();
    //_banks->clear();
    return true;
}

}

