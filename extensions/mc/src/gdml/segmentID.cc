#include "na64mc/runtimeState.hh"
#include "gdml/segmentID.hh"

// TODO
#if 0
#include <G4LogicalVolume.hh>
#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

const std::regex
SetSegmentID::rxSegmentID = std::regex(R"~(([\w-]+)\[(\w+)\]=(#|\w+))~");

bool
SetSegmentID::parse_segmentation_level_description( const std::string & expr
            , std::string & classifierName
            , std::string & variableName
            , std::string & value ) {
    std::smatch m;
    auto mr = std::regex_match( expr, m, rxSegmentID );
    if(!mr) return false;  // bad match

    classifierName = m[1];
    variableName = m[2];
    value = m[3];

    return true;
}

SetSegmentID::SetSegmentID( const G4String & uiCmdClassifiersRoot )
    : _uiCmdClassifiersRoot(uiCmdClassifiersRoot) {
}

void
SetSegmentID::process_auxinfo( const G4GDMLAuxStructType & auxStruct
                             , G4LogicalVolume * lvPtr ) {
    assert(lvPtr);
    if( !auxStruct.unit.empty() ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unused \"unit\" attribute for sensitive detector auxinfo"
                  " of volume \"%s\""
                , lvPtr->GetName().c_str() );
        G4Exception( __FUNCTION__, "NA64SW004", JustWarning, errbf);
    }
    std::string classifierName
              , variableName
              , value
              ;
    if( ! parse_segmentation_level_description( auxStruct.value
                , classifierName, variableName, value ) ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Can not parse the expression \"%s\" as segmentation level"
                  " description for volume \"%s\"."
                , auxStruct.value.c_str()
                , lvPtr->GetName().c_str() );
        G4Exception( __FUNCTION__, "NA64SW004", FatalErrorInArgument, errbf);
    }
    log4cpp::Category & L
        = log4cpp::Category::getInstance(std::string("na64mc.gdml.auxinfo"));
    // Find or create new feature extractor to be modified
    auto it = find(classifierName);
    if( end() == it ) {
        it = emplace( std::piecewise_construct
                    , std::forward_as_tuple( classifierName )
                    , std::forward_as_tuple( classifierName
                                           , _uiCmdClassifiersRoot
                                           ) ).first;
        L << log4cpp::Priority::INFO
          << "Created new feature extracting instance \""
          << classifierName
          << "\" for stepping classifiers.";
    }
    // Add the indexing level to feature extractor
    it->second.define_indexing_level( lvPtr, variableName, value );
    L << log4cpp::Priority::INFO
      << "Defined indexing level \"" << variableName << "\""
      << " of value \"" << value << "\" for feature extracting instance \""
      << classifierName << "\".";
}

}
}

REGISTER_GDML_AUX_INFO_TYPE( segmentID, msgrPath, state
        , "Creates and appends the segmentation classifiers for multipart"
          " sensitive detectors." ) {
    return new na64dp::mc::SetSegmentID( msgrPath );
}

#if 0
const std::regex SetSD::rxSensitiveDetector(
    R"~((\/[\w\/-]+)(\[(\w+):([\d#])\])?)~" );

SetSD::Assignment
SetSD::parse_sd_expr(const G4String & expr) {
    std::smatch m;
    auto mr = std::regex_match( expr, m, rxSensitiveDetector );
    if(!mr) return Assignment{ "", "", "" };  // bad match
    return Assignment{ m[1], m[3], m[4] };
}
#endif

    #if 0
    if( ! assignment.indexPath.empty() ) {
        // We expect the sensitive detector to contain the indexing
        // composer with `BySegmentID` extractor (classifier). Code below
        // will assign appropriate index appender bound to certain logical
        // volume to the classifier
        auto cSDPtr = dynamic_cast<SegmentedSensitiveDetector*>(sdPtr);
        if( !cSDPtr ) {
            char errbf[256];
            snprintf(errbf, sizeof(errbf), "Sensitive detector instance %p"
                    " referenced by name \"%s\" is not a subclass of"
                    " SegmentedSensitiveDetector. Unable to specify"
                    " index completion rule \"%s:%s\" for volume %p \"%s\"."
                    , sdPtr, sdPtr->GetName().c_str()
                    , assignment.indexPath.c_str()
                    , assignment.indexNumber.c_str()
                    , lvPtr, lvPtr->GetName().c_str() );
            G4Exception( __FUNCTION__, "NA64SW155", FatalErrorInArgument, errbf);
        }
        #if 1
        throw std::runtime_error("TODO: assign new indexing level to SD");
        #else
        // Introduce indexing level into existing composite sensitive
        // detector's hierarchy
        cSDPtr->define_indexing_level( lvPtr
                                     , assignment.indexPath
                                     , assignment.indexNumber );
        L << log4cpp::Priority::DEBUG
            << "Sensitive detector indexing completion rule \""
            << auxStruct.value << "\" is added to volume \""
            << lvPtr->GetName() << "\" (" << (void*) lvPtr << ")"
            ;
        #endif
    }
    #endif
#endif
