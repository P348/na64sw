#include "gdml/visStyle.hh"

#include <gtest/gtest.h>

namespace na64dp {

static const struct StylesExprExample {
    std::string expr
              , setupName
              , drawingStyle
              , color
              ;
    mc::SetVisualAttributes::VolumeStyle vs;
} gStylesExamples[] = {
    { "*: wireframe #AAA",            "*",        "wireframe","#AAA",
        {mc::SetVisualAttributes::VolumeStyle::wireframe, 0xaaaaaaff } },
    { "Default : hidden",             "Default",  "hidden",   "",
        {mc::SetVisualAttributes::VolumeStyle::hidden, 0x0 } },
    { "detailed:surface#1B34",  "detailed", "surface",  "#1B34",
        {mc::SetVisualAttributes::VolumeStyle::surface, 0x11bb3344 } },
    { "simplified: #abcd",       "simplified","",        "#abcd",
        {mc::SetVisualAttributes::VolumeStyle::undefined, 0xaabbccdd } },
    { "", "", "", "", {} }
};

// Test basic matching
TEST(MCLVSelector, basicRegexMatches) {
    std::smatch m;
    for( const StylesExprExample * sample = gStylesExamples
       ; ! sample->expr.empty()
       ; ++sample ) {
        EXPECT_TRUE( std::regex_match( sample->expr, m, mc::SetVisualAttributes::rxStyle ) );
        EXPECT_EQ( m[1], sample->setupName );
        EXPECT_EQ( m[2], sample->drawingStyle );
        EXPECT_EQ( m[3], sample->color );
    }
}

// Tast basic matching failure
TEST(MCLVSelector, basicRegexMatchFailures) {
    const char ms[][64] = { ":wireframe#333"  // no setup name
                          , "one:face#333"  // bad token
                          , "five:surface#112233445"  // bad number of color digits (9)
                          , "six:surface#11z233"  // bad color code
                          , ""
                          };
    std::smatch m;
    for( const char (*s)[64] = ms; **s != '\0'; ++s ) {
        std::string ss(*s);
        EXPECT_FALSE( std::regex_match( ss, m, mc::SetVisualAttributes::rxStyle ) )
            << "Expression \"" << ss << "\" passes regular expression (while it must not)."
            << std::endl;
    }
}

// Test token parsing validity
TEST(MCLVSelector, stylesParsing) {
    std::smatch m;
    for( const StylesExprExample * sample = gStylesExamples
       ; ! sample->expr.empty()
       ; ++sample ) {
        std::pair<std::string, mc::SetVisualAttributes::VolumeStyle> vsp
            = mc::SetVisualAttributes::parse( sample->expr );

        EXPECT_EQ( sample->setupName,       vsp.first );
        EXPECT_EQ( vsp.second.drawingStyle, sample->vs.drawingStyle );
        EXPECT_EQ( vsp.second.rgba,         sample->vs.rgba );
    }
    // test parsing failure that is not error for the rgex
    auto p = mc::SetVisualAttributes::parse( "four:surface#3a122" );  // 5-digit color
    EXPECT_EQ("", p.first);
}

// Test correct style derivation from dict
TEST(MCLVSelector, stylesDerivation) {
    // Declare default style
    mc::SetVisualAttributes::VolumeStyle dft = {
            mc::SetVisualAttributes::VolumeStyle::undefined, 0x333333ff };
    // Parse the styles set for some volume into dict
    auto dict = mc::SetVisualAttributes::parse_styles(
            "*:hidden; Default:wireframe; detailed:surface#3864");

    // obtain dicts for different cases

    // - for some undefined setup
    auto vs = mc::SetVisualAttributes::get_style_for(dict, dft, "");
    // -- drawing style taken from '*'
    EXPECT_EQ( vs.drawingStyle, mc::SetVisualAttributes::VolumeStyle::hidden );
    // -- color taken from default
    EXPECT_EQ( vs.rgba, 0x333333ff );

    // - for `Default' setup
    vs = mc::SetVisualAttributes::get_style_for(dict, dft, "Default");
    // -- drawing style taken from 'Default'
    EXPECT_EQ( vs.drawingStyle, mc::SetVisualAttributes::VolumeStyle::wireframe );
    // -- color taken from default
    EXPECT_EQ( vs.rgba, 0x333333ff );

    // - for `Default' setup
    vs = mc::SetVisualAttributes::get_style_for(dict, dft, "detailed");
    // -- drawing style taken from 'detailed'
    EXPECT_EQ( vs.drawingStyle, mc::SetVisualAttributes::VolumeStyle::surface );
    // -- color taken from 'detailed'
    EXPECT_EQ( vs.rgba, 0x33886644 );
}

};

