#include "gdml/exportDefinition.hh"
#include "na64mc/g4api/detectorConstruction.hh"

#include <G4GDMLParser.hh>

namespace na64dp {
namespace mc {

// TODO
#if 0
ExportGDMLDefinition::ExportGDMLDefinition( iGDMLParserOwner & po
                                          , iGDMLDefinitionsIndexes & di
                                          )
        : _parserOwner(po)
        , _defIdxs(di) {
}

ExportGDMLDefinition::~ExportGDMLDefinition() {
}

void
ExportGDMLDefinition::copy_definition( G4GDMLParser & parser
                                     , const std::string & name
                                     , const std::string & definitionType
                                     , ExportIndex & index ) {
    #if 0  // does not work for vectors, matrices and other complex definitions
    if( ! parser.IsValid(name) ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Can not export undefined entry \"%s\"."
                , name.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW903"
                   , FatalErrorInArgument
                   , errBf
                   );
    }
    #endif
    bool inserted = false;
    // these vars used for printing:
    std::ostringstream ss;
    ss << std::scientific;
    G4double scalar;
    G4ThreeVector vec;
    G4GDMLMatrix mx;
    G4LogicalVolume * volPtr;
    if( "constant" == definitionType ) {
        inserted = index.scalars.emplace( name, scalar = parser.GetConstant(name) ).second;
        ss << "(" << scalar << ")";
    } else if( "variable" == definitionType ) {
        inserted = index.scalars.emplace( name, scalar = parser.GetVariable(name) ).second;
        ss << "(" << scalar << ")";
    } else if( "quantity" == definitionType ) {
        inserted = index.scalars.emplace( name, scalar = parser.GetQuantity(name) ).second;
        ss << "(" << scalar << ")";
    } else if( "position" == definitionType ) {
        inserted = index.vectors.emplace( name, vec = parser.GetPosition(name) ).second;
        ss << "{" << vec[0] << ", " << vec[1] << ", " << vec[2] << "}";
    } else if( "rotation" == definitionType ) {
        inserted = index.vectors.emplace( name, vec = parser.GetRotation(name) ).second;
        ss << "{" << vec[0] << ", " << vec[1] << ", " << vec[2] << "}";
    } else if( "scale" == definitionType ) {
        inserted = index.vectors.emplace( name, vec = parser.GetScale(name) ).second;
        ss << "{" << vec[0] << ", " << vec[1] << ", " << vec[2] << "}";
    } else if( "matrix" == definitionType ) {
        inserted = index.matrices.emplace( name, mx = parser.GetMatrix(name) ).second;
        ss << "of shape " << mx.GetRows() << "x" << mx.GetCols();
    } else if( "volume" == definitionType ) {
        inserted = index.volumes.emplace( name, volPtr = parser.GetVolume(name) ).second;
        ss << volPtr;
    } else {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Unknown definition type \"%s\" provided for export."
                , definitionType.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW904"
                   , FatalErrorInArgument
                   , errBf
                   );
    }
    if( !inserted ) {
        char errBf[256];
        snprintf( errBf, sizeof(errBf)
                , "Failed to export definition \"%s\" of type \"%s\". Probably"
                  " an entity with same name was shadowing export."
                , name.c_str()
                , definitionType.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW905"
                   , JustWarning
                   , errBf
                   );
    }
    log4cpp::Category & root = log4cpp::Category::getInstance(std::string("na64mc.gdml.auxinfo"));
    root << log4cpp::Priority::DEBUG
              << "Exported \"" << name << "\" " << definitionType << " "
              << ss.str()
              << "."
              ;
}

void
ExportGDMLDefinition::process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                     , G4LogicalVolume * /*lvPtr*/ ) {
    #if 0  // dev output, to test new definition types
    std::cout << "(xxx export) value=\"" << auxStruct.value
              << "\", unit=\"" << auxStruct.unit
              << "\", lvPtr=" << lvPtr
              << std::endl;
    #endif

    if( ! auxStruct.auxList ) return;

    for( auto sp : *auxStruct.auxList ) {
        copy_definition( _parserOwner.parser()
                       , sp.value
                       , sp.type
                       , _defIdxs.get_index(auxStruct.value)
                       );
    }
}
#endif

}
}

// TODO
#if 0
REGISTER_GDML_AUX_INFO_TYPE( export, msgrPath, state
        , "Exports definitions (variables, quantities, matrices etc) from GDML" ) {
    assert( state.detectorConstructionPtr );
    return new na64dp::mc::ExportGDMLDefinition( *state.detectorConstructionPtr
                                               , *state.detectorConstructionPtr );
}
#endif

