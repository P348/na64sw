#include "sd/composite.hh"

namespace na64dp {
namespace mc {

#if 0
ScalarValueGetterMixin::ui_cmd_use_value( geode::GenericG4Messenger* msgr_
                                        , const G4String & nm ) {
    CellScorers & msgr = dynamic_cast<CellScorers&>(*msgr_);
    auto it = getters.find(nm);
    if( getters.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Has no getter \"%s\" available"
                " (destination \"%s\")"
                , nm.c_str()
                , msgr._name.c_str() );
    }
    msgr._getter = it->second;
    // TODO: write log about value being set
}
#endif

/* Usage example:
    # Create composite SD "/na64sw-sd/calo/emTestWCAL"
    /na64sw/gdmlAuxInfo/sd/create Composite /na64sw-sd/calo/emTestWCAL
    # - add per-cell summators
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/add Scorer perCellEDep
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/useValue edep
    # - add "*=***" edep histograms
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/add Hist1D preshowerAndMain
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/setIndexConversionRule preshowerAndMain "ignore, keep, ignore, ignore"
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/preshowerAndMain/setHistogram edep 600 0 150 GeV
    # - add total edep histogram for the entire detector
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/add Hist1D total
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/setIndexConversionRule total "ignore, ignore, ignore, ignore"
    /na64sw/gdmlAuxInfo/sd/emTestWCAL/total/setHistogram edep 600 0 150 GeV
 */

void
CompositeSensitiveDetector::FilteredDestination::operator()( Index i, G4Step * aStep ) {
    for( util::Cached<iFilter*> * fltr : *this ) {
        if( ! fltr->get(i, aStep) ) return;
    }
    dest.account_step(i, aStep);
}

CompositeSensitiveDetector::CompositeSensitiveDetector( const std::string & sdName
                                                      , const std::string & msgrPath
                                                      )
        : SegmentedSensitiveDetector(sdName)
        , geode::GenericG4Messenger(msgrPath) {
    // Make UI cmd subdir named by this SD's name
    std::string thisSDUIDir = GetName();  // or better just "name" ?
    _basePath = msgrPath + thisSDUIDir;  // note: msgrPath already has trailing "/"
    // Substitute '/' with '-' (for "name")
    //std::replace( thisSDUIDir.begin(), thisSDUIDir.end(), '/', '-');
    dir( thisSDUIDir
       , "Commands related to \"" + sdName + "\" composite SD." )
        .cmd( "add"
            , "Adds named steps destination"
            , ui_cmd_add_destination
            , init | preinit | idle
            )
            .par<G4String>( "clsName",  "Sub-detector class name" )
            .par<G4String>( "instName", "A name of the destination instance" )
        .end( "add" )
        .cmd( "setIndexConversionRule"
            , "Assigns index conversion rule to the destination"
            , ui_cmd_make_converted
            , init | preinit | idle
            )
            .par<G4String>( "dstName", "Destination name" )
            .par<G4String>( "idxConv", "Index conversion rule" )
        .end( "setIndexConversionRule" )
        // ... TODO: filters management
    .end( thisSDUIDir )
    ;

    log4cpp::Category::getInstance("na64mc.sd")
            << log4cpp::Priority::INFO << "New composite SD"
            " instance \"" << GetName() << "\" has been created.";
}

void
CompositeSensitiveDetector::process_hits_for( const Index_t & idx
                                            , G4Step * aStep
                                            ) {
    for( auto & p : _dests ) {
        auto cnvResult = p.first(idx);
        if( ! cnvResult.first ) continue;  // discriminated by i conversion rule
        (*p.second)( cnvResult.second, aStep );
    }
    // Invalidate filter caches
    for( auto & fp : _filters ) {
        fp.second.invalidate();
    }
}

void
CompositeSensitiveDetector::set_conversion_rule_for( const std::string & dstName
                                                   , const Index::Conversion & cnv
                                                   ) {
    auto dstIt = _destsByName.find( dstName );
    if( _destsByName.end() == dstIt ) {
        NA64DP_RUNTIME_ERROR( "Destination \"%s\" does not exist in"
                " composite SD \"%s\"", dstName.c_str(), GetName().c_str() );
    }
    // todo: prevent double accounting by checking if the destination has been
    // already added?
    _dests.emplace( cnv, &(dstIt->second) );
}

void
CompositeSensitiveDetector::add_destination( const std::string & clsName
                                           , const std::string & dstName
                                           ) {
    std::string np = _basePath + "/";
    iDestination * newDst = VCtr::self().make<iDestination>( clsName, dstName, np );
    _destsByName.emplace( dstName, *newDst );
}

void
CompositeSensitiveDetector::EndOfEvent(G4HCofThisEvent*) {
    for( auto & fdestPair : _destsByName ) {
        fdestPair.second.dest.finalize_event();
    }
}

void
CompositeSensitiveDetector::ui_cmd_add_destination(
        geode::GenericG4Messenger * msgr_, const G4String & strExpr) {
    auto msgr = dynamic_cast<CompositeSensitiveDetector *>(msgr_);
    assert(msgr);
    std::string destClsName, destName; {
        std::stringstream iss(strExpr);
        iss >> destClsName >> destName;
    }
    msgr->add_destination( destClsName, destName );
}

void
CompositeSensitiveDetector::ui_cmd_make_converted(
        geode::GenericG4Messenger * msgr_, const G4String & strVal ) {
    auto msgr = dynamic_cast<CompositeSensitiveDetector *>(msgr_);
    assert(msgr);
    auto tokens = util::tokenize_quoted_expression( strVal );
    assert(2 == tokens.size());
    msgr->set_conversion_rule_for( tokens[0], tokens[1] );
}

#if 0
//
// Manager
/////////

void
DefinitionsManager::Definition::operator()( PartsIndex::Index i, G4Step * aStep ) {
    for( EntriesByIndex & esByIdx : records ) {
        auto it = esByIdx.find(i);
        if( esByIdx.end() == it ) {
            it = esByIdx.emplace(i, createNewEntry(i, commonParameters)).first;
        }
        iEntry * entry = it->second;
        entry->account_step(aStep);
    }
}

void
DefinitionsManager::Definition::finalize_event() {
    for( EntriesByIndex & esByIdx : records ) {
        for( auto entryPair : esByIdx ) {
            entryPair.second->finalize_event();
        }
    }
}

DefinitionsManager::Definition
DefinitionsManager::define_entries( const std::string & name
                                  , const std::string & strExpr ) {
    auto it = _defs.find(name);
    if( _defs.end() == it ) {
        NA64DP_RUNTIME_ERROR("No SD sub-detector type named \"%s\".", name.c_str());
    }
    Definition newDef { it->second.first
                      , it->second.second ? it->second.second(strExpr) : nullptr
                      , Records()
                      };
    return newDef;
}

void
DefinitionsManager::add_definition( const std::string & name
                                  , EntryConstructor ctr
                                  , void * (*f)(const std::string &) ) {
    auto ir = _defs.emplace( name, std::make_pair(ctr, f) );
    assert(ir.second);
}

//
// Composite SD
//////////////

DefinitionsManager CompositeSensitiveDetector::definitionsManager;

CompositeSensitiveDetector::CompositeSensitiveDetector( const std::string & name
                                                      , const std::string & msgrPath ) 
        : SegmentedSensitiveDetector(name)
        , geode::GenericG4Messenger(msgrPath) {
    // Make UI cmd subdir named by this SD's name
    std::string thisSDUIDir = GetName();  // or better just "name" ?
    // Substitute '/' with '-' (for "name")
    //std::replace( thisSDUIDir.begin(), thisSDUIDir.end(), '/', '-');
    dir( thisSDUIDir
       , "Commands related to \"" + name + "\" SD." )
        .cmd( "add"
            , "Adds sub-detector instance"
            , ui_cmd_add
            , init | preinit | idle
            )
            .par<G4String>( "name", "Sub-detector type name" )
            .par<G4String>( "args", "Arguments for a sub-detector", "" )
        .end( "add" )
        .cmd( "addWithConversion"
            , "Adds sub-detector instance with index conversion"
            , ui_cmd_add_converted
            , init | preinit | idle
            )
            .par<G4String>( "idxConv", "Index conversion rule" )
            .par<G4String>( "name", "Sub-detector type name" )
            .par<G4String>( "args", "Arguments for a sub-detector", "" )
        .end( "addWithConversion" )
    .end( thisSDUIDir )
    ;

    log4cpp::Category::getInstance("na64mc.sd")
            << log4cpp::Priority::INFO << "New composite SD"
            " instance \"" << GetName() << "\" has been created.";
}

void
CompositeSensitiveDetector::process_hits_for( const Index_t & idx
                                            , G4Step * aStep ) {
    // fill entries without fragment index conversion
    for( auto & defs : _plain ) {
        defs( idx, aStep );
    }
    // convert index and address entries by converted index
    for( auto & convPair : _converted ) {
        auto cnvResult = convPair.first(idx);
        if(!cnvResult.first) continue;  // segment masked by conversion rule
        Index i = cnvResult.second;  // i is now a converted index
        for( auto & defs : convPair.second ) {
            defs(i, aStep);
        }
    }
}

void
CompositeSensitiveDetector::converted_entries( const std::string & name
                                             , const std::string & strArgs
                                             , const std::string & convString ) {
    DefinitionsManager::Definitions * defs;
    if( convString.empty() ) {
        defs = &_plain;
    } else {
        auto cnv = Index::Conversion(convString);
        auto it = _converted.find(cnv);
        if( it == _converted.end() ) {
            // Create new conversion+definitions pair
            it = _converted.emplace( cnv
                                   , DefinitionsManager::Definitions()
                                   ).first;
            log4cpp::Category::getInstance("na64mc.sd")
                << log4cpp::Priority::DEBUG << "New subdetector instance has"
                " been created for composite SD \"" << GetName() << "\" with"
                " type name \"" << name << "\" and arguments \""
                << convString << "\"; new conversion was defined wrt pattern"
                " \"" << convString << "\".";
        } else {
            log4cpp::Category::getInstance("na64mc.sd")
                << log4cpp::Priority::DEBUG << "New subdetector instance has"
                " been created for composite SD \"" << GetName() << "\" with"
                " type name \"" << name << "\" and arguments \""
                << convString << "\"; existing conversion was used for pattern"
                " \"" << convString << "\".";
        }
        defs = &(it->second);
    }
    defs->push_back( definitionsManager.define_entries(name, strArgs) );
}

void
CompositeSensitiveDetector::ui_cmd_add_converted(
            geode::GenericG4Messenger * msgr_,
            const G4String & strVal ) {
    auto * msgr = static_cast<CompositeSensitiveDetector *>(msgr_);
    auto tokens = utils::tokenize_quoted_expression( strVal );
    assert(tokens.size() > 1);  // must be guaranteed by macro interpreter
    assert(tokens.size() < 4);  // must be guaranteed by macro interpreter
    msgr->converted_entries( tokens[1]
                           , tokens.size() > 2 ? tokens[2] : ""
                           , tokens[0]
                           );
}

void
CompositeSensitiveDetector::ui_cmd_add(
            geode::GenericG4Messenger * msgr_,
            const G4String & strVal ) {
    auto * msgr = static_cast<CompositeSensitiveDetector *>(msgr_);
    auto tokens = utils::tokenize_quoted_expression( strVal );
    assert(!tokens.empty());  // must be guaranteed by macro interpreter
    assert(tokens.size() < 3);  // must be guaranteed by macro interpreter
    msgr->converted_entries( tokens[0], tokens.size() > 1 ? tokens[1] : "" );
}
#endif

}

REGISTER_SENSITIVE_DETECTOR( Composite
                           , name, msgrPath, dsp, nfr
                           , "Sensitive detector for segmented detectors"
                             " collecting various quantities per segment." ) {
    return new mc::CompositeSensitiveDetector( name, msgrPath );
}

}
