#include "na64util/Geant4UIAppender.hh"

#include <log4cpp/LoggingEvent.hh>

#if defined(Geant4_FOUND) && Geant4_FOUND
#include <G4UIsession.hh>

namespace na64dp {
namespace util {

G4UIsession * Geant4UISessionAppender::session = nullptr;

void
Geant4UISessionAppender::_append(const log4cpp::LoggingEvent & le) {
    if(!session) return;  // no session associated, skip
    if( le.priority <= log4cpp::Priority::WARN ) {
        session->ReceiveG4cerr(_getLayout().format(le));
    } else {
        session->ReceiveG4cout(_getLayout().format(le));
    }
}

Log4cppAppendersFactoryResult_t
Geant4UISessionAppender::create(const log4cpp::AppendersFactory::params_t & p) {
    std::string name;
    p.get_for("Geant4 session appender").required("name", name);
    return Log4cppAppendersFactoryResult_t(new Geant4UISessionAppender(name));
}

}
}
#endif

