# Extensions dir for CMake

NA64sw uses CMake as a primary build system. Customs extensions are needed
to provide tedious lookup procedures for third party packages that do not
provide CMake modules, uninstall target is also extremely useful thing, etc.

## Notes on extensions

To-be install extensions must put their `.so` files
at `<prefix>/lib[64]/na64/modules`, standard headers location
is `<prefix>/include/na64sw/extensions/<extension>/`.

Proper way to configure install target is to utilize CMake features with
`set_target_properties()` for extension lib:

 1. `SOVERSION` must be set to NA64sw major version indicating API compatibility,
 2. `INSTALL_RPATH` must specified for lookup,
 3. `PUBLIC_HEADER` should refer to headers location.

Example:

    # collect headers recursively by wildcard; used only by install target
    file(GLOB_RECURSE na64std_HEADERS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "*.hh" "*.h" "*.tcc" "*.hpp" )
    # ...
    set_target_properties( ${na64std_LIB} PROPERTIES VERSION ${na64sw_VERSION}
                           SOVERSION ${na64sw_SOVERSION}
                           INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib/na64/modules
                           PUBLIC_HEADER "${na64std_HEADERS}"
                         )
    set_target_properties (${na64std_LIB} PROPERTIES PUBLIC_HEADER
        "include/one.hh;include/two.hh")
    # ...
    install( TARGETS ${na64std_LIB} EXPORT ${na64std_LIB} LIBRARY
             DESTINATION ${NA64SW_STDEXT_DIR}
             PUBLIC_HEADER DESTINATION include/na64sw/extensions/std
           )


