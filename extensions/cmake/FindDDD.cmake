cmake_minimum_required( VERSION 3.5 )

include(FindPackageHandleStandardArgs)

#
# DaqDataDecoding library lookup (from CORAL)
find_path( DaqDataDecoding_INCLUDE_DIR
           NAMES DaqEventsManager.h
           PATHS $ENV{p348daq_PREFIX}/coral/src/DaqDataDecoding/src
                 $ENV{CORAL}/src/DaqDataDecoding/src
                 $ENV{CORAL_DIR}/src/DaqDataDecoding/src
                 /usr/include
                 /usr/include/coral
                 /usr/include/na64
                 ${CMAKE_INSTALL_PREFIX}/include
                 ${CMAKE_INSTALL_PREFIX}/include/coral
                 ${CMAKE_INSTALL_PREFIX}/include/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/coral/src/DaqDataDecoding/src
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/coral/src/DaqDataDecoding/src
    )
find_library( date_monitor_LIB
           NAMES libmonitor.a
           PATHS $ENV{p348daq_PREFIX}/date/lib
                 $ENV{p348daq_PREFIX}/date/lib64
                 ${CMAKE_INSTALL_PREFIX}/lib
                 ${CMAKE_INSTALL_PREFIX}/lib64
                 ${CMAKE_INSTALL_PREFIX}/lib/coral
                 ${CMAKE_INSTALL_PREFIX}/lib64/coral
                 ${CMAKE_INSTALL_PREFIX}/lib/na64
                 ${CMAKE_INSTALL_PREFIX}/lib64/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/date/monitoring/Linux
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/date/monitoring/Linux
    )
find_library( DaqDataDecoding_LIB
           NAMES libDaqDataDecoding.a
           PATHS $ENV{CORAL}/src/DaqDataDecoding/src
                 $ENV{p348daq_PREFIX}/coral/src/DaqDataDecoding/src
                 ${CMAKE_INSTALL_PREFIX}/lib
                 ${CMAKE_INSTALL_PREFIX}/lib64
                 ${CMAKE_INSTALL_PREFIX}/lib/na64
                 ${CMAKE_INSTALL_PREFIX}/lib64/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/coral/src/DaqDataDecoding/src
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/coral/src/DaqDataDecoding/src
    )
find_package_handle_standard_args( DaqDataDecoding
    REQUIRED_VARS DaqDataDecoding_LIB
                  DaqDataDecoding_INCLUDE_DIR
                  date_monitor_LIB
    )
if( DaqDataDecoding_FOUND )
    find_package( EXPAT REQUIRED )
endif( DaqDataDecoding_FOUND )
