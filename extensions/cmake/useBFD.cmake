find_library( BFD_LIB bfd 
    PATHS
        /usr/include/
        /usr/local/include
    PATH_SUFFIXES
        x86_64-linux-gnu
    )
find_package_handle_standard_args(BFD REQUIRED_VARS BFD_LIB )
find_library( IBERTY_LIB iberty
    PATHS
        /usr/lib
        /usr/local/lib
    PATH_SUFFIXES
        x86_64-linux-gnu
    )
find_path( IBERTY_INCLUDE_DIR libiberty.h
    PATHS
        /usr/include/
        /usr/local/include/
        /usr/include/x86_64-linux-gnu/
    PATH_SUFFIXES
        libiberty
        x86_64-linux-gnu
    )
find_package_handle_standard_args(IBERTY REQUIRED_VARS IBERTY_LIB IBERTY_INCLUDE_DIR )
if( IBERTY_FOUND AND BFD_FOUND )
    set( NA64DP_ENABLE_EXC_STACKTRACE TRUE )
endif( IBERTY_FOUND AND BFD_FOUND )
