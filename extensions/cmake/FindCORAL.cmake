cmake_minimum_required( VERSION 3.5 )

#
# COmpass Reconstruction and Analysis Library (CORAL)

find_path( CORAL_INCLUDE_DIR
           NAMES Coral.h CsGEMPlane.h
           PATHS $ENV{CORAL}/include
                 $ENV{CORAL_DIR}/include
                 $ENV{p348daq_PREFIX}/coral/include
                 /usr/include
                 /usr/include/coral
                 /usr/include/na64
                 ${CMAKE_INSTALL_PREFIX}/include
                 ${CMAKE_INSTALL_PREFIX}/include/coral
                 ${CMAKE_INSTALL_PREFIX}/include/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/coral/include
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/coral/include
    )
find_library( CsGEM_LIB
           NAMES libCsGEM.a
           PATHS ${CORAL_DIR}/lib
                 ${CORAL_DIR}/lib/Linux
                 $ENV{CORAL}/lib
                 $ENV{CORAL}/lib/Linux
                 $ENV{CORAL_DIR}/lib
                 $ENV{CORAL_DIR}/lib/Linux
                 $ENV{p348daq_PREFIX}/coral/lib
                 $ENV{p348daq_PREFIX}/coral/lib/Linux
                 ${CMAKE_INSTALL_PREFIX}/lib
                 ${CMAKE_INSTALL_PREFIX}/lib64
                 ${CMAKE_INSTALL_PREFIX}/lib/coral
                 ${CMAKE_INSTALL_PREFIX}/lib64/coral
                 ${CMAKE_INSTALL_PREFIX}/lib/na64
                 ${CMAKE_INSTALL_PREFIX}/lib64/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/coral/lib/Linux
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/coral/lib/Linux
    )
find_library( CsMumega_LIB
    NAMES libCsMumega.a
           PATHS ${CORAL_DIR}/lib
                 ${CORAL_DIR}/lib/Linux
                 $ENV{CORAL}/lib
                 $ENV{CORAL}/lib/Linux
                 $ENV{CORAL_DIR}/lib
                 $ENV{CORAL_DIR}/lib/Linux
                 $ENV{p348daq_PREFIX}/coral/lib
                 $ENV{p348daq_PREFIX}/coral/lib/Linux
                 ${CMAKE_INSTALL_PREFIX}/lib
                 ${CMAKE_INSTALL_PREFIX}/lib64
                 ${CMAKE_INSTALL_PREFIX}/lib/coral
                 ${CMAKE_INSTALL_PREFIX}/lib64/coral
                 ${CMAKE_INSTALL_PREFIX}/lib/na64
                 ${CMAKE_INSTALL_PREFIX}/lib64/na64
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq/coral/lib/Linux
                 ${CMAKE_INSTALL_PREFIX}/opt/p348-daq.current/coral/lib/Linux
    )
message( DEBUG "CORAL libs in use: ${CsGEM_LIB} ${CsMumega_LIB} ${CORAL_INCLUDE_DIR}" )
find_package_handle_standard_args( CORAL
    REQUIRED_VARS CsGEM_LIB
                  CORAL_INCLUDE_DIR
                  CsMumega_LIB
    )
