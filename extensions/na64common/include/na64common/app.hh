/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"

#include "na64calib/manager.hh"
#include "na64common/calib/sdc-integration.hh"
#include <list>

namespace na64dp {
namespace util {

/**\brief Sets NA64sw standard environment variables
 *
 * Sets variables like `P348_CONDDB_DIR` or `NA64SW_PREFIX` as they were
 * specified during the build. Note, that variable will not be set if it
 * is already defined (this way users may override default settings).
 * */
void set_std_environment_variables();

/// Prints list of registered runtime entities (uses `VCtr`)
void list_registered( std::ostream & os, const char * type );

/// Appends YAML node with registered extensions
///
/// Useful to cope with external API. Appends loaded handlers, sources,
/// getters.
void list_registered_extensions(YAML::Node &);

/**\brief Loads shared object files
 *
 * Uses runtime loading, native to the system to load extension modules.
 * May generate various diagnostic messages that by default written to the
 * standard output and appended to a static `stringstream` for further
 * retrieval by logging subsystem (logging must be typically initialized after
 * extensions are loaded).
 *
 * \returns number of module loading failures or 0
 * \todo Retrieve diagnostic log of module loading.
 * */
int load_modules( const std::list<std::string> & modules_
                , const char * paths
                , bool quiet=false
                );

/**\brief Sets up common calibration routines for the app.
 *
 * This function is convenient to use for somewhat "standard" initialization
 * procedure, useful for applications. Based on the "calibration" string
 * parameter will instantiate indexes, add default generic loaders:
 * YAML file ("yaml-document") and CSV ("csv"), etc.
 *
 * \param mgr A manager instance to configure
 * \param cfgURI A string identifying calibration config URI
 *
 * \todo Currently, only YAML file of certain form is supported
 */
void
setup_default_calibration( na64dp::calib::Manager & mgr
                         , const std::string & cfgURI
                         );

/// Prints application banner (some ASCII art)
void
print_banner( std::ostream & os, const char * appDescr );

}  // namespace ::na64dp::util
}  // namespace na64dp


