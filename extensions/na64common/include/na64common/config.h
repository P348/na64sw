#pragma once

#include "na64sw-config.h"  // TODO: perhaps, must not be included in common lib

#ifndef NA64COMMON_FLOAT
#   define NA64COMMON_FLOAT float
#endif

/*
 * For pure C routines
 */

/** Standard floating point number type to use in the lib, pure C */
typedef NA64COMMON_FLOAT na64common_Float_t;


#ifdef __cplusplus

//
// C++
//

namespace na64common {

/// Standard floating point number type to use in the lib, C++
typedef ::na64common_Float_t Float_t;

}
#endif  /* __cplusplus */

