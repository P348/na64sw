#pragma once

#include <vector>
#include <sstream>
#include <iostream>
#include <memory>
#include <limits>
#include <algorithm>

#include "sdc.hh"

/**\brief SDC adapters for S.Donskov (semi-).xml files
 *
 * This header defines custom loader type and related routines coping S.Donskov
 * calibration files with SDC parser. Please, note that this handler along with
 * the corresponding implementation .cc file can be distributed standalone as
 * well.
 * */

/// Common (base) error class for sXML routines
class sXMLRuntimeError : public std::runtime_error {
public:
    sXMLRuntimeError(const char * errorMessage)
        : std::runtime_error(errorMessage)
        {}
};

/// Parsing error subclass for sXML routines
class sXMLParsingError : public sXMLRuntimeError {
private:
    mutable std::string _whatBuf;
public:
    std::string filePath;
    size_t lineNo;
    sXMLParsingError(const char * msg, const char * path="", size_t lineNo_=0)
        : sXMLRuntimeError(msg), filePath(path), lineNo(lineNo_)
        {}

    const char * what() const throw() override {
        std::ostringstream oss;
        if(!filePath.empty()) oss << filePath;
        if(lineNo) oss << ":" << lineNo << ": ";
        oss << sXMLRuntimeError::what();
        _whatBuf = oss.str();
        return _whatBuf.c_str();
    }
};


/// Key to identify calibration data
///
/// Supports run number, provides rudimentary support for per-burst (spill)
/// identification (not fully works as it is not yet clear whether we really
/// need it)
struct sXMLCalibID {
    /// Run number
    uint32_t runNo;
    /// Spill number
    uint32_t spillNo;

    bool operator<(const sXMLCalibID b) const {
        if(runNo > b.runNo) return false;
        if(runNo < b.runNo) return true;
        return spillNo < b.spillNo;
    }
    bool operator==(const sXMLCalibID b) const { return runNo == b.runNo && spillNo == b.spillNo; }
};

namespace sdc {
/// Traits specialization for "sXMLCalibID" validity key
template<>
struct ValidityTraits<sXMLCalibID> {
    static constexpr sXMLCalibID unset = {0, 0};
    static constexpr char strRangeDelimiter = '-';
    static inline bool is_set(sXMLCalibID id) {
        return !(unset.runNo == id.runNo && unset.spillNo == id.spillNo);
    }
    static inline bool less(sXMLCalibID a, sXMLCalibID b) { return a < b; }
    static inline std::string to_string(sXMLCalibID rn) {
        std::ostringstream oss;
        oss << rn.runNo;
        if(rn.spillNo != unset.spillNo) oss << "/" << unset.spillNo;
        return oss.str();
    }
    static inline void advance(sXMLCalibID & rn) {
        if(rn.spillNo != std::numeric_limits<decltype(sXMLCalibID::spillNo)>::max()) {
            ++rn.spillNo;
        } else {
            ++rn.runNo;
            rn.spillNo = 0;
        }
    };
    static inline sXMLCalibID from_string(const std::string & stre);  // implemented below
    using Less = std::less<sXMLCalibID>;
};
}  // namespace sdc

/**\brief Loader for SVD's semi-XML format
 *
 * This class implements SDC document loader interface for S.Donskov's semi-XML
 * ASCII files containing various calibrations. Despite of XML-like markup
 * those files are generally a CSV ASCII tables. Their metadata information
 * is provided in a form close to XML (tihin attributes of "XML node"),
 * comments are generally marked with `<!--`, `/>`, however node definition
 * can contain spaces, tags are not always properly closed (missing trailing
 * slashes, closing tag mismatch, etc).
 *
 * According to original code, detector type is defined based on filename and
 * the only crucial metadata entry is the validity range, that is handled by
 * `scanf("<runs %c \"%d - %d\"\n", ...)`.
 */
class sXMLLoader : public sdc::Documents<sXMLCalibID>::iLoader {
public:
    typedef sdc::Documents<sXMLCalibID>::DataBlock DataBlock;
    struct DefaultColumns {
        const char detName[32];
        const char columnsList[256];
    };
protected:
    const DefaultColumns * _dftColumns;
    std::ostream * _logStream;
public:
    sXMLLoader(const DefaultColumns * defaultColumns, std::ostream * logStream=nullptr)
        : _dftColumns(defaultColumns)
        , _logStream(logStream)
        {}

    /// Returns default columns based on filename or NULL if failed to assume
    std::pair<const char *, std::string> guess_default_columns_for_file(const char * filePath) const;
    /// Pre-parses sXML document and returns its metadata
    std::list<DataBlock> get_doc_struct( const std::string & docID ) override;
    /// Reads certain data block
    void read_data( const std::string & docID
                  , sXMLCalibID k
                  , const std::string & forType
                  , sdc::IntradocMarkup_t acceptFrom
                  , sdc::Documents<sXMLCalibID>::iLoader::ReaderCallback cllb
                  ) override;
};

