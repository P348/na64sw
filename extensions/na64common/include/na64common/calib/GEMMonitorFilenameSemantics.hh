#pragma once

#include "na64common/calib/sdc-integration.hh"

namespace na64dp {
namespace util {

/**\brief Asume APV file meaning by its filename
 *
 * At least following types of files produced by GEMMonitor app can be found in
 * p48reco:
 *  - pedestals file: `GM01X1__~~start-2016-06-25-15:02:23~~finish-2016-07-01-19:11:22`
 *  - timing file: `GM01X1__timing~~start-2016-01-01-00:00:01~~finish-2016-12-31-23:59:59`
 *  - ???: `GM02Y1__.ped`
 *  - ???: `MM01X__timing`
 *  - ???: `MM01.ped`
 * Paths of these files mst be interpreted in a following semantics:
 *  - GEMs pedestals and timing files
 *
 * Added metainfo keys:
 *  - "detector" TBname of the detector entity under consideration
 *  - "filename" filename (without dir path by "directory")
 *  - "directory" directory containing file (may be empty)
 *  - "documentFullPath" full path to the file
 *  - "timeFrom" (opt) astronomical time str expr of beginning of validity period
 *  - "timeTo" (opt) astronomical time str expr of beginning of validity period
 *  - "period" (opt) validity period shortcut, if defined in path (like 2021mu)
 *  - "extension" (opt) file extension, if any
 *  - "directory"
 *
 * \returns `true` if paths seems like an APV detector
 * */
int
interpret_GEMMonitor_filename( const std::string & path
                             , sdc::aux::MetaInfo & mi
                             );

/**\brief A highly specialized FS-traversing generator for GEMMonitor files
 *
 * Specialized for GEMMonitor pedestal and timing files that typically match
 * wildcards:
 *      - "*~~start-*~~finish* (like "MX05~~start-2021-11-05-07:24:52~~finish-2021-11-07-23:28:39")
 *      - "*.ped" (like "GM21MUON.ped" or "MM34_.ped")
 *
 * These files are of simple CSV format and their filenames must be interpreted
 * as validity period prescription. This is done by extended callable semantics
 * compatible with `sdc::Documents::add_from()` method of SDC.
 * */
struct GEMMonitorFilenameSemantics : protected sdc::aux::FS {
    using FS::set_logstream;
    typedef P348_RECO_NAMESPACE ::Datetime_t Datetime_t;

    /// Ptr to the loader instance used to load the APV calibration data
    std::shared_ptr<sdc::Documents<Datetime_t>::iLoader> loaderInstance;

    /// List of aliases referencing certain periods
    P348_RECO_NAMESPACE ::Aliases * aliases;

    /// Constructs the lookup object for given path and search patterns
    GEMMonitorFilenameSemantics( const std::string & paths_
                               , const std::string & acceptPatterns="*~~start-*~~finish*:*.ped"
                               , const std::string & rejectPatterns="~*.ped:*.ped~"
                               , ::off_t fileSizeMin=1, ::off_t fileSizeMax=10*1024*1024
                               ) : FS( paths_
                                     , acceptPatterns, rejectPatterns
                                     , fileSizeMin, fileSizeMax
                                     )
                                 , aliases(nullptr)
                                 {}

    ///\brief Call operator returning valid document ID
    /// 
    /// Returns document ID fitting the given criteria with modified (by ref)
    /// context switches. Compatible with `sdc::Documents::add_from()` call
    std::string operator()( std::pair<bool, std::string> & dftType
                          , std::pair<bool, sdc::ValidityRange<Datetime_t>> & dftVR
                          , std::pair<bool, sdc::aux::MetaInfo> & mdAppendix
                          , std::shared_ptr<sdc::Documents<Datetime_t>::iLoader> & loader
                          ) {
        std::string docID;
        do {
            docID = FS::operator()();
            if(docID.empty()) return docID;
            int rc = interpret_GEMMonitor_filename(docID, mdAppendix.second);
            if(rc <= 0) {
                SDC_WARN_LOG_STREAM << "Warning: skipping file \"" << docID << "\""
                        " as failed to interpret its filename as one of"
                        " the GEMMonitor calibration files." << std::endl;
                continue; 
            }

            auto & mi = mdAppendix.second;
            mdAppendix.first = true;
            // Figure out document validity range; if any limit given, force
            // overload
            const std::string strFrom = mi.get<std::string>("timeFrom", "(none)")
                            , strTo = mi.get<std::string>("timeTo", "(none)")
                            ;
            if( strFrom != "(none)" ) {
                dftVR.first = true;
                dftVR.second.from
                    = sdc::ValidityTraits<Datetime_t>::from_string(strFrom);
            }
            if( strTo != "(none)" ) {
                dftVR.first = true;
                dftVR.second.to
                    = sdc::ValidityTraits<Datetime_t>::from_string(strTo);
            }
            // Aside of exactly dated files (ones with "~~start" and
            // "~~finish" considered above) there are fallback ones without
            // exact date/time markers that we relate to certain datetime
            // periods. To figure out corresponding validity periods for such
            // files we rely on the aliases.
            if(  (!sdc::ValidityTraits<Datetime_t>::is_set( dftVR.second.from ) )
              || (!sdc::ValidityTraits<Datetime_t>::is_set( dftVR.second.to ) )
              ) {
                std::list<P348_RECO_NAMESPACE ::Aliases::AliasEntry> ps;
                if( (!aliases) || (ps = aliases->assume_validity(docID)).empty() ) {
                    SDC_WARN_LOG_STREAM << "Warning: skipping file \"" << docID << "\""
                        " -- unable to assume it's validity period." << std::endl;
                    continue;
                }
                #if 0  // useless since 2021mu will match both 2021 and 2021mu
                if( ps.size() > 1 ) {
                    SDC_WARN_LOG_STREAM << "Warning: file \"" << docID << "\" "
                        " matches multiple data taking period aliases." << std::endl;
                }
                #endif
                #if 0
                {
                    std::cout << "xxx Assuming validity period by path for \""
                        << docID << "\":" << std::endl;
                    for( const auto & p : ps ) {
                        std::cout << "  xxx: " << p.first << std::endl;
                    }
                }
                #endif
                dftVR.second.from = ps.begin()->second.time.from;
                dftVR.second.to   = ps.begin()->second.time.to;
                dftVR.first = true;
            }
            // We assume this file to be
            //  - of `APVTimingCalib` if there is "timing" suffix
            //  - of a pedestals file if it has `.ped' suffix ("extension")
            if( mi.get<std::string>("isTiming", "no") == "yes" ) {
                dftType.first = true;
                dftType.second = "GEMMonitor/APVTiming";
                // (this file format provides no metadata)
            } else if( mi.get<std::string>("extension", "(none)") == "ped"
                    || mi.get<std::string>("timeFrom", "(none)") != "(none)" ) {
                dftType.first = true;
                dftType.second = "GEMMonitor/APVPedestals";
            } else {
                // TODO: warning?
                continue;
            }
            break;
        } while(true);
        loader = loaderInstance;
        return docID;
    }
};

}  // namespace ::na64dp::util
}  // namespace na64dp

