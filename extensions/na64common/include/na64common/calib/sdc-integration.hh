/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

/**\file
 * \brief Appends type traits for SDC in the context of NA64sw
 *
 * This file defines classes and types to customize certain SDC routines to be
 * used in NA64sw: datetime key (to index calibration data by astronomical
 * time), NA64-specific types, etc.
 *
 * - A calibration data update type wrapper is defined to combine NA64sw
 *   update type (`na64dp::calib::iUpdate`) and update from
 *   SDC (`sdc::Documents<KeyT>::Update`) as a template class parameterised
 *   with index key: `SDCUpdateWrapper`
 * - An adaptor class is defined combining NA64sw calibration data index and
 *   loader `SDCWrapper`.
 */

#include "na64calib/loader.hh"
#include "na64calib/index.hh"
#include "na64calib/manager.hh"
#include "na64util/streambuf-redirect.hh"
#include <type_traits>

#ifndef NA64SW_SDC_LOG_CATEGORY
#   define NA64SW_SDC_LOG_CATEGORY "calib.sdc"
#endif

namespace na64dp {
namespace calib {
namespace aux {
/// Logging adapter to interface SDC and NA64sw logging system (log4cpp)
class SDCLogstream : public std::ostream {
private:
    util::Log4Cpp_StreambufRedirect * _streambuf;
    static SDCLogstream * _self;
protected:
    SDCLogstream( log4cpp::Category & logCat )
        : std::ostream( _streambuf = new util::Log4Cpp_StreambufRedirect(logCat, log4cpp::Priority::WARN) )
        {}
    ~SDCLogstream() { delete _streambuf; }
public:
    static SDCLogstream & self();
};  // class SDCLogstream
}  // namespace ::na64dp::calib::aux
}  // namespace ::na64dp::calib
}  // namespace na64dp

#define SDC_WARN_LOG_STREAM ::na64dp::calib::aux::SDCLogstream::self()
#   include <sdc.hh>

#define P348_RECO_NAMESPACE p348reco

#include "na64common/calib/sdc-extensions.hh"

namespace YAML { class Node; };  // fwd

namespace na64dp {
namespace calib {
// templated alias SDC collection for set of SADCCalib items read by SDC
template<typename T>
    using Collection = typename sdc::CalibDataTraits<T>::template Collection<>;
typedef sdc::SrcInfo<p348reco::CaloCellCalib> SADCCalib;  // fwd
class SDCWrapper;  // fwd

///\brief Wrapper for SDC update type representing it as `na64sw` update
///
/// Wraps given SDC update ref with supplementary info to cope with `na64sw`.
/// Pretty simple idea of this struct is to redirect index/loader calls to
/// a SDC wrapper class.
template<typename KeyT>
struct SDCUpdateWrapper
    : public iSpecificUpdate<typename sdc::Documents<KeyT>::Update> {
    /// Native (SDC's C-struct) update type
    typedef typename sdc::Documents<KeyT>::Update NativeUpdate;
    /// Ptr to owning index/loader wrapper struct
    SDCWrapper * owner;
    /// Name (alias) of the update type
    const std::string typeName;

    /// Key for which update was created
    KeyT forKey;
    
    SDCUpdateWrapper( const NativeUpdate & orig
                    , SDCWrapper * owner_
                    , const std::string & typeName_
                    , KeyT forKey_
                    )
        : iSpecificUpdate<NativeUpdate>(orig)
        , owner(owner_)
        , typeName(typeName_)
        , forKey(forKey_)
        {}
    /// Implements `na64sw` API requirement
    ///
    /// Forwards call to `na64sw` aliases dict with `typeName` as an argument
    /// to resolve "string+typeinfo" data.
    Dispatcher::CIDataID subject_data_type() const override {
        auto it = CIDataAliases::self().type_id_by_name().find(typeName);
        if(it == CIDataAliases::self().type_id_by_name().end()) {
            NA64DP_RUNTIME_ERROR( "SDC wrapper: calibration data type aliases"
                    " integrity failure on type \"%s\" -- update object"
                    " created, but type is not known to native aliases dict."
                    , typeName.c_str() );
        }
        return it->second;
    }
    /// Return owner ptr as a recommended loader.
    iLoader * recommended_loader() const override;
};

/**\brief Implements `iIndex` and `iLoader` interface as wrapper around `sdc::Indexes`
 *
 * This class interfaces SDC. It combines functions from both
 * `na64sw::calib::iLoader` and `na64sw::calib::iIndex` classes for both key
 * types: by run and by datetime.
 * */
class SDCWrapper : public iIndex, public iLoader
                 , public sdc::Documents<EventID>
                 , public sdc::Documents<P348_RECO_NAMESPACE::Datetime_t>
                 {
private:
    /// Reference to the standard logger handle ("category")
    log4cpp::Category & _log;
    /// Instance of `sdc::ExtCSV` data loader for data indexed by date and time
    std::shared_ptr<sdc::ExtCSVLoader<P348_RECO_NAMESPACE::Datetime_t>> _extCSVLoaderByDatetime;
    /// Instance of `sdc::ExtCSV` data loader for data indexed by run number
    //std::shared_ptr<sdc::ExtCSVLoader<P348_RECO_NAMESPACE::RunNo_t>> _extCSVLoaderByRunNo;
    std::shared_ptr<sdc::ExtCSVLoader<EventID>> _extCSVLoaderByEventID;
    /// If enabled, makes wrapper to produce additional diagnostic output
    bool _debug;
    /// List of (temporary) update objects to be deleted
    std::vector<iUpdate *> _toCleanup;

    /// (internal template function) Collects updates b/w two validity keys and
    /// transforms it into `na64sw::calib` update
    template<typename KeyT> size_t _translate_updates(const KeyT &, const KeyT &, Updates &);

    template<typename KeyT> using ConversionDict = std::unordered_map< Dispatcher::CIDataID
                      //, std::function<void(const iUpdate &, Dispatcher &)>
                      , void (SDCWrapper::*)(const iUpdate &, Dispatcher &, KeyT)
                      , util::PairHash
                      >;

    /// Set of conversion callbacks, by calibration type ID
    ConversionDict<EventID> _conversions_EventID;
    ConversionDict<P348_RECO_NAMESPACE::Datetime_t> _conversions_Datetime;

    //template<typename KeyT> ConversionDict<KeyT> & _conversions_for();  // TODO

    template<typename KeyT> typename
    std::enable_if<std::is_same<KeyT, P348_RECO_NAMESPACE::Datetime_t>::value
        , ConversionDict<P348_RECO_NAMESPACE::Datetime_t>>::type &
        _conversions_for() { return _conversions_Datetime; }

    template<typename KeyT> typename
    std::enable_if<std::is_same<KeyT, EventID>::value
        , ConversionDict<EventID>>::type &
        _conversions_for() { return _conversions_EventID; }
protected:
    ///\brief Returns calibration type conversion by calib type ID
    ///
    /// Just a shortcut to retrieve item from `_conversions`.
    template<typename KeyT, typename DataT> void
    _conversion_template( const iUpdate & upd_
                        , Dispatcher & dsp
                        , KeyT key
                        ) {
        // downcast NA64sw update type to SDC wrapper's update type
        const auto & upd = static_cast<const SDCUpdateWrapper<KeyT>&>(upd_);
        // instantiate concrete update collection type
        typename sdc::CalibDataTraits<DataT>::template Collection<> dest;
        // load update
        this->sdc::Documents<KeyT>::template load_update_into<DataT>(upd.payload, dest, key);
        // debug-report on applying update
        if(_debug) {
            auto & updc = dynamic_cast< const iSpecificUpdate<
                                typename sdc::Documents<KeyT>::Update
                            >&>(upd);
            _log.debug( "Dispatching collected %zu updates from %s document..."
                      , dest.size()
                      , updc.payload.second->docID.c_str()
                      );
        }
        // dispatch loaded update
        dsp.set<typename sdc::CalibDataTraits<DataT>::template Collection<>>("default", dest);
    }
    ///\brief Collects SDC documents (items with calibration data)
    ///
    /// `T` should be one of defined key types (e.g. run number, datetime, etc),
    ///
    /// Uses current wrapper configuration to discover and collect docs.
    /// Expands paths from `paths_` argument, perform recursive scan using
    /// lookup object defined by `KeyTraits<T>::instantiate_lookup_object()`,
    /// and adds every discovered document to `docs`
    /// with `sdc::Documents::add_from()`.
    template<typename T> size_t
    _collect_docs( sdc::Documents<T> & docs
                 , const std::string & paths_
                 , const std::string & acceptPatterns
                 , const std::string & rejectPatterns
                 , size_t minSize, size_t maxSize );
public:
    /// Adds certain calibration data type conversion
    template<typename KeyT, typename DataT> void
    enable_sdc_type_conversion( const std::string auxStrTypeID="default" ) {
        auto typeId = calib::CIDataAliases::self()
                .add_alias_of<typename sdc::CalibDataTraits<DataT>::template Collection<>>(
                    sdc::CalibDataTraits<DataT>::typeName, auxStrTypeID );
        _conversions_for<KeyT>().emplace( typeId
                    , &SDCWrapper::_conversion_template<KeyT, DataT> );
    }

    ///\brief configures SDC loaders and documents indices
    ///
    /// From SDC point of view this constuctors sets up and registers SDC
    /// grammar, loaders, creates documents index adds loaders to index.
    /// It also performs documents collection (pre-parsing of SDC documents).
    ///
    ///\todo Document config object.
    SDCWrapper( const YAML::Node & sdcConfig
              , const P348_RECO_NAMESPACE::Aliases & periodAliases );

    ///\brief Uses SDC `documents` instance to query for the updates
    ///
    /// Collects updates from both `Documents<>` instances of the
    /// `sdc::Indexes`.
    void append_updates( EventID oldEventID, EventID newEventID
                       , const std::pair<time_t, uint32_t> & oldKey
                       , const std::pair<time_t, uint32_t> & newKey
                       , Updates & ) override;
    ///\brief Performs actual loading of the calibration data using SDC
    void load( const iUpdate &, Dispatcher & ) override;
    ///\brief Always returns `false`
    ///
    /// This method is checked only if update didn't bring up a "recommended
    /// loader", while this wrapper's updates always bring it. By always
    /// returning `false` we avoid unnecessary downcasts/runtime checks to
    /// save some performance.
    bool can_handle( const iUpdate & ) override { return false; }

    /// Dumps SDC documents as NA64sw's index
    void to_yaml( YAML::Node & ) const override;
    /// Dumps SDC documents as NA64sw's loader
    void to_yaml_as_loader( YAML::Node & ) const override;
    /// Deletes temporary update instances (update wrappers)
    void clear_tmp_update_objects() override;
};  // class SDCWrapper

template<typename KeyT> iLoader *
SDCUpdateWrapper<KeyT>::recommended_loader() const {
    return owner;
}

}  // namespace calib

namespace nameutils {
class DetectorNaming;  // fwd
///\brief A special treatment of SDC detector naming scheme
///
/// This is NA64-related routine tunring detector name given in `vame-z`
/// notation widely used in p348reco routines into `DetID` instance. The main
/// difference with usual naming scheme (provided by `nameutils::DetectorNaming`
/// is the index splitting -- for SADC detectors x,y indeces are given in a
/// separate columns, not in the detector entry per se.
///
/// SDC calibration entries usually provides some additional
/// identification information.
///
/// \todo move to experiment-specific lib
DetID sdc_id( const nameutils::DetectorNaming & nm
            , const std::string & nameZ
            , uint16_t xIdx, uint16_t yIdx, uint16_t zIdx
            , const std::string & docID=""
            , size_t lineNo=0
            );

}  // namespace ::na64dp::nameutils
}  // namespace na64

