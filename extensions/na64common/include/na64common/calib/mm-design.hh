#pragma once

#include <vector>
#include <string>
#include <array>

namespace YAML { class Node; }  // fwd

namespace na64dp {
namespace calib {

class Dispatcher;  // fwd

/// MuMega wire multiplexing scheme (design) as calibration data tuple -- 
/// detector selection and wires connectivity itself.
typedef std::vector<std::array<int, 5>> MuMegaWiresDesign;

///\brief Intermediate type used to update the multiplexing scheme.
///
/// This type is transmitted from calibration data loader to a consumer in
/// order the latter to build a cached result, optimized for subsequent
/// usage (with compiled selector).
typedef std::pair<std::string, MuMegaWiresDesign> MuMegaWiresDesignEntry;

/**\brief This function loads MuMega wire layout from a file into data
 *        structure and dispatches it to whoever interested.
 *
 * Use it when instantiating subscribers assuring conversion registered with
 *
 * \code{.cpp}
 *  mgr.get_loader<na64dp::calib::YAMLDocumentLoader>("yaml-doc")
 *         .define_conversion(
 *                   "MuMegaLayout"  //< this must coincide with 2nd arg of REGISTER_CALIB_DATA_TYPE()
 *                 , ::na64dp::calib::convert_YAML_description_to_MuMegaLayout
 *                  );
 * \endcode
 */
void convert_YAML_description_to_MuMegaLayout( const YAML::Node & plNode
                                             , calib::Dispatcher & dsp );

}  // namespace ::na64dp::calib
}  // namespace na64dp

