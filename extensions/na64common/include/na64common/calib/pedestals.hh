/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/mean-value.hh"
#include "na64detID/detectorID.hh"
#include "na64calib/data-indexes/csv.hh"

#include <unordered_map>

namespace na64dp {
namespace calib {

/// Indexes SADC pedestal pairs by detector ID -- calibration data type
typedef std::unordered_map< DetID_t, std::pair<NDValue, NDValue> > Pedestals;

/**\brief Pedestals CSV indexes file reader
 *
 * ASCII CSV files referenced by YAML nodes shall be in format:
 *  - Full name of SADC detector entity (e.g. "ECAL0:0-2-0")
 *  - "Odd"/"Even" token denoting ADC channel
 *  - Pedestal value (float)
 *  - Pedestal error (sigma for normal distribution)
 *  - Number of entries
 * */
class PedestalsCSVDataIndex :
    public iCSVFilesDataIndex< Pedestals
                             , std::string, std::string
                             , double, double, size_t> {
protected:
    /// Naming handle to dereference detector name IDs into numerical ones
    Handle<nameutils::DetectorNaming> _naming;
    /// Logger reference
    log4cpp::Category & _log;
public:
    /// Binds name handle to dispatcher
    PedestalsCSVDataIndex( Dispatcher & cdsp )
            : _naming("default", cdsp)
            , _log(log4cpp::Category::getInstance("calibrations.pedestals")) {}
    /// Appends data index with tuple read from CSV file
    virtual void append_index_from_tuple( Pedestals &
                                        , const Tuple & t ) override;
};

}
}
