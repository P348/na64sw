#include "na64common/calib/sdc-integration.hh"
#include "na64calib/placements.hh"

namespace sdc {
template<>
struct CalibDataTraits<na64dp::calib::Placement> {
    /// Type name alias
    static constexpr auto typeName = "placements";
    /// A collection type of the parsed entries
    template<typename T=na64dp::calib::Placement>
        using Collection=std::list<T>;
    //typedef std::list<P348_RECO_NAMESPACE::CaloCellCalib> Collection;
    /// An action performed to put newly parsed data into collection
    template<typename T=na64dp::calib::Placement>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t lineNo
                              ) { col.push_back(e); }

    /**\brief Parses the CSV line into `CaloCellCalib` data object
     *
     * Parsing can be affected by the parser state to handle changes within
     * a file. Namely, metadata "columns=name1,name2..." may denote number of
     * token with certain semantics. By default, "name,x,y,peakpos" is assumed
     *
     * The `lineNo` argument can be used for diagnostic purposes (e.g.
     * to report a format error). */
    static na64dp::calib::Placement
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      );
};  // struct CalibDataTraits<na64dp::calib::Placement>
}  // namespace sdc

#if 0
namespace na64dp {
namespace calib {

//typedef Placement PlacementEntry;
typedef sdc::SrcInfo<Placement> PlacementEntry;
typedef sdc::CalibDataTraits<PlacementEntry>::Collection<PlacementEntry> Placements;

}  // namespace ::na64dp::calib
}  // namespace na64dp
#endif
