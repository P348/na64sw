/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64common/calib/sdc-integration.hh"
#include "na64util/str-fmt.hh"

namespace YAML { class Node; }  // fwd

namespace na64dp {

namespace errors {

class NoAliasDefined : public na64dp::errors::GenericRuntimeError {
protected:
    mutable char _errBf[128];
public:
    NoAliasDefined()
        : na64dp::errors::GenericRuntimeError("No alias defined.")
        {}
};  // class NoAliasDefined

class NoAliasDefinedForRun : public NoAliasDefined {
public:
    na64dp::EventID rn;
    NoAliasDefinedForRun( na64dp::EventID rn_ ) : rn(rn_) {}
    const char * what() const throw() final;
};

class NoAliasDefinedForTime : public NoAliasDefined {
public:
    const P348_RECO_NAMESPACE::Datetime_t dtm;
    NoAliasDefinedForTime( P348_RECO_NAMESPACE::Datetime_t dtm_ ) : dtm(dtm_) {}
    const char * what() const throw() final;
};

}  // namespace ::na64dp::errors

namespace calib {

/**\brief Global singleton for the run periods aliases
 *
 * This helper class keeps index of data taking dates and run numbers for
 * easier reading of diagnostic messages and more flexible calibration validity
 * referencing.
 *
 * Since not every run can be referenced to a particular data taking period,
 * user code must not expect this either from this object.
 * */
class Periods : public P348_RECO_NAMESPACE::Aliases {
public:
    /// Returned type containing alias instance, label and comment (for queries)
    struct EntryProxy {
        const std::string & name;
        const std::string & comment;
        const P348_RECO_NAMESPACE::PeriodAlias * alias;
    };
protected:
    static Periods * _self;
    std::unordered_map<const P348_RECO_NAMESPACE::PeriodAlias *, std::string> _comments;
public:
    /// Returns global instance
    static Periods & self();
    /// Appends period aliases dictionary with entries from YAML node
    ///
    /// \todo document expected YAML node
    void add_from_YAML_node(const YAML::Node &);
    /// Returns period alias entry by run number
    EntryProxy operator[](na64dp::EventID) const;
    /// Returns period alias entry by time
    EntryProxy operator[](P348_RECO_NAMESPACE::Datetime_t) const;
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

