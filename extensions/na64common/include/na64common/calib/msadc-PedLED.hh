#include "na64common/calib/sdc-integration.hh"
#include "na64detID/TBName.hh"
#include <log4cpp/Category.hh>

/**\file
 * \brief Defines (M)SADC calibrations type #1, provides pedestals and LEDs
 *
 * File defines calibration types and conversions for SDC module.
 * The handled calibration data becomes available for NA64sw subscribers
 * including data processing handlers.
 *
 * S.Donskov's applications for (M)SADC detectors use fixed memory layout of
 * all the items corresponding to each detector entity being arranged in linear
 * array. Length of this array (`n_cells`) is defined as global constant. In
 * principle, depending on the year, this value and corresponding layout can
 * change. Linear array is composed from sections of hardcoded sizes, each
 * individual section has its own index (un)mapping functions
 * called (`lin2...()`).
 */

namespace P348_RECO_NAMESPACE {

/**\brief Calibrations type info implemented by S.Donskov, used by p348reco
 *
 * This is a type of per-spill valibrations data introduced for MSADC detectors
 * implemented in 2015-2023 by S.Donskov. Originally implies per-run data files
 * with per-spill sections, contains averaged LED and pedestals data, ...
 * */
struct MSADCCalibPedLedType1 {
    /// Data layout, as it was specified in file's header
    std::vector<std::pair<std::string, int>> layout;
    /// Values being read in the line
    std::vector<float> values;
    /// Statistics, as specified at the spill header
    size_t nEventsOn, nEventsOff;
    /// Timestamp, as specified at the spill header
    time_t timestamp;
};

/**\brief Maintains lines semantics
 *
 * In this format each line has its own semantics, a special collection class
 * is introduced to handle it.
 * */
template<typename T> class MSADCCalibPedLedType1Collection;

/**\brief Set of values valid per-spill
 *
 * Includes pedestals and LEDs arrays of certain layout. Tracks line number
 * info to infer semantics (see `MSADCCalibPedLedType1Loader` description).
 * */
struct MSADCCalibPedLedType1CollectionBase {
    //std::unordered_map<std::string, std::vector<float>> pedestals
    //                                                  , offSpillLEDs
    //                                                  , inSpillLEDs;
    std::vector<float> pedestals
                     , offSpillLEDs
                     , inSpillLEDs;
    /// Aggregate type
    struct Item { float pedestals[2], offSpillLED, inSpillLED; };
protected:
    /// Current line number, affects semantics
    int _nLine;
    /// Layout in use
    std::vector<std::pair<std::string, int>> _layout;
    /// Debug: returns `true` if layout was not set or not changed
    bool _assure_layout(const MSADCCalibPedLedType1 &);
    /// Adds values (used by subclasses with/without source info, from
    /// `add_values()`)
    void _add_values(const MSADCCalibPedLedType1 & calibs);

    void _copy_values_to_item(Item &, size_t, const std::string &) const;
public:
    MSADCCalibPedLedType1CollectionBase() : _nLine(0) {}
    /// Required by SDC integration, for logging
    size_t size() const { return _layout.size(); }

    /// Based on detector naming and layout information, splits up values onto
    /// individual detector items. Unset values are set to NaN in items.
    std::unordered_map<na64dp::DetID, Item>
        get_mapped( const na64dp::nameutils::DetectorNaming &
                  , log4cpp::Category * logCatPtr=nullptr
                  ) const;
};  // class MSADCCalibPedLedType1CollectionBase

/// Specialization for calibrations without source info
template<>
class MSADCCalibPedLedType1Collection<MSADCCalibPedLedType1>
        : public MSADCCalibPedLedType1CollectionBase {
public:
    void add_values(const MSADCCalibPedLedType1 & calibs)
        { _add_values(calibs); }
};

/// Specialization for calibrations with source info
template<>
class MSADCCalibPedLedType1Collection<sdc::SrcInfo<MSADCCalibPedLedType1>>
        : public MSADCCalibPedLedType1CollectionBase {
public:
    void add_values(const sdc::SrcInfo<MSADCCalibPedLedType1> & v);
};

/**\brief Loader handling the Pedestals+LEDs files
  *
  * S.Donskov created custom format for Pedestals+LEDs information. This loader
  * interfaces SDC with these files. Format consists of:
  *
  *  1. File header at 1st line, defining certain columnar layout of subsequent
  *     whitespace-separated blocks.
  *  2. Repeating per-burst section containing its own header of certain
  *     format. It provides information about numbers of LED events being used
  *     to calculate per-burst mean values.
  *
  * Pedestals and LEDs are given in per-burst section as whitespace-separated
  * set of numbers. Every line corresponds to certain type of values: pedestals,
  * off-spill LEDs or in-spill LEDs, depending on whether non-zero number of
  * events was defined in per-burst header. Within a line every value
  * corresponds to certain detector item, except for pedestals case, where
  * values are interlaced and one should consider concatenation of lines.
  *
  * Main file header seem to be of rather arbitrary form, but partially defines
  * layout of numbers in CSV-like lines.
  */
class MSADCCalibPedLedType1Loader : public sdc::Documents<na64dp::EventID>::iLoader {
protected:
    /// Paths matching this pattern are considered as subject
    std::regex _pathPattern;
public:
    /// Sets up a loader instance based on file path pattern (regexp)
    MSADCCalibPedLedType1Loader(const std::string & filePathPattern)
        : _pathPattern(filePathPattern)
        {}
    /// Returns `true` for paths matching expected pattern
    bool can_handle(const std::string & docID) const override;
    ///\brief Pre-reads calibration file
    ///
    /// Walks over file's content, splitting structure onto per-burst blocks.
    /// Returned list is then consumed by SDC.
    std::list<sdc::Documents<na64dp::EventID>::DataBlock>
        get_doc_struct( const std::string & docID ) override;
    ///\brief Reads requested Pedestals+LEDs section from S.Donskov file
    ///
    /// Namely:
    /// * Creates `sdc::MetaInfo` object
    /// * Reads file's 1st line (header containing layout), puts it
    ///   into `"header"` metainfo entry
    /// * Goes to line refrred by `acceptFrom`, reads spill header into
    ///   metainfo entries: `statOff`, `statOn`, `runNo`, `spillNo` and
    ///   `timestamp`
    /// * Reads lines afterwards, before next line containing `"stat_off"`
    ///   token, calling given callback for every line
    void read_data( const std::string & docID
                  , na64dp::EventID k
                  , const std::string & forType
                  , /*IntradocMarkup_t -> */ size_t acceptFrom
                  , ReaderCallback cllb
                  ) override;
};  // class MSADCCalibPedLedType1Loader

}  // namespace P348_RECO_NAMESPACE

namespace sdc {
/**\brief Implements traits for `MSADCCalibPedLedType1` calibrations
 *
 * Traits assumes somewhat standard distribution format: per-year directories
 * filled with ASCII files named afer particular run, with per-spill sections
 * within the files.
 */
template<>
struct CalibDataTraits<P348_RECO_NAMESPACE::MSADCCalibPedLedType1> {
    /// Type name alias
    static constexpr auto typeName = "donskov/PedLED";
    template<typename T=P348_RECO_NAMESPACE::MSADCCalibPedLedType1>
        using Collection=p348reco::MSADCCalibPedLedType1Collection<T>;
    
    ///\brief Adds spill calibration info items
    ///
    /// ...
    template<typename T=P348_RECO_NAMESPACE::MSADCCalibPedLedType1>
    static inline void collect( Collection<T> & c
                              , const T & entry
                              , const aux::MetaInfo & mi
                              , size_t lineNo
                              ) { c.add_values(entry); }

    /**\brief Parses CSV-like line
     *
     * Has rather simplistic implementation: just splits
     * whitespace-delimited line, converts all tokens to float type and puts
     * returns new instance of `MSADCCalibPedLedType1`. Layout is extracted
     * from metadata, but layout splitting is done at this level.
     * */
    static P348_RECO_NAMESPACE::MSADCCalibPedLedType1
    parse_line( const std::string & line
              , size_t lineNo
              , const aux::MetaInfo & m
              , const std::string & srcID
              , sdc::aux::LoadLog * loadLogPtr=nullptr
              );

};  // struct CalibDataTraits<P348_RECO_NAMESPACE::MSADCCalibPedLedType1>

}  // namespace sdc

