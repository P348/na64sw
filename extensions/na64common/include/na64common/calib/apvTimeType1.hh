#pragma once

#include "na64common/calib/sdc-integration.hh"
//#include "na64calib/sdc-integration.hh"

/**\file
 * \brief Defines APV timing calibration type #1
 *
 * File defines calibration types and conversions for SDC module.
 * The handled calibration data becomes available for NA64SW subscribers
 * including data processing handlers.
 */

namespace P348_RECO_NAMESPACE {

/**\brief A timing calibration data type for APV detectors
 *
 * A somewhat full set of the time calibration data defined for APV detector
 * plane. Consists of the parameters triplet ($r_0$, $t_0$, $a_0$) with meaning
 * as described at https://arxiv.org/pdf/1708.04087.pdf (and reference 28 in
 * particular).
 *
 * The errors have the following meaning:
 *  - first three are just std.dev. squared of the corresponding parameters
 *  - the rest are covariance
 * */
struct APVTimingCalibEntry {
    /// Aux structure referencing an amplitude group of the APV timing calibs
    struct ParPack {
        double r0, t0, a0;  // time fitting pars
        double r02, t02, a02  // squared std.dev.
             , r0t0, r0a0, t0a0  // cov
             ;
    } a02, a12;
};

/**\brief APV channel pedestal calibration data type
 *
 * ...
 * */
struct APVPedestalChannelEntry {
    int channelflag;
    double pedestalamp
         , pedestalsigma
         , calibrationamp
         , calibrationsigma
         ;
};

}  // namespace <P348_RECO_NAMESPACE>

namespace sdc {

extern const char * gAPVTimingCalibEntryLabelsOrder[];

/// Calibration data traits for APV timing calibration in CSV format
template<>
struct CalibDataTraits<P348_RECO_NAMESPACE::APVTimingCalibEntry> {
    /// Type name alias
    static constexpr auto typeName = "GEMMonitor/APVTiming";
    /// APV timing calibration information collection type
    template<typename T=P348_RECO_NAMESPACE::APVTimingCalibEntry>
        using Collection=std::unordered_map<std::string, T>;
    ///\brief Assumes that detector name is defined in meta information
    ///
    /// Since the detector name and validity period are provided by file path
    /// rather than by file content, this function relies on the metainfo
    template<typename T=P348_RECO_NAMESPACE::APVTimingCalibEntry>
    static inline void collect( Collection<T> & c
                              , const T & entry
                              , const aux::MetaInfo & mi
                              , size_t ll
                              ) {
        const std::string detName = mi.get<std::string>("detector", "");
        if( detName.empty() ) {
            throw errors::ParserError("No detector name specified for"
                    " timing entry.");
        }
        c[detName] = entry;
    }

    /**\brief Parses CSV-like line within a APV timing calibration file
     *
     * Those files are (apparently) produced by GEMMonitor as a result of time
     * calibration procedure. These files contains a single line with
     * space-spearated value of very strict semantics, with no metadata
     * steering semantics of columns implementation, so one can directly
     * assign the tokens.
     * */
    static P348_RECO_NAMESPACE::APVTimingCalibEntry
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      ) {
        // TODO: utilize loadLogPtr
        P348_RECO_NAMESPACE::APVTimingCalibEntry obj;
        const auto toks = aux::tokenize(line);
        double * const ptrs[] = {  // in semantical order:
            &obj.a02.r0, &obj.a02.t0, &obj.a02.a0,
            &obj.a12.r0, &obj.a12.t0, &obj.a12.a0,
            // a02 errors
            &obj.a02.r02, &obj.a02.t02, &obj.a02.a02,
            &obj.a02.r0t0, &obj.a02.r0a0, &obj.a02.t0a0,
            // a12 errors
            &obj.a12.r02, &obj.a12.t02, &obj.a12.a02,
            &obj.a12.r0t0, &obj.a12.r0a0, &obj.a12.t0a0,
            nullptr
        };
        {
            size_t n = 0;
            const char  ** tokLabel = gAPVTimingCalibEntryLabelsOrder;
            for( auto tok : toks ) {
                if(!ptrs[n]) {
                    // TODO: this seems rather usual, ask ppl for 24 vs 18
                    // values
                    break;
                    #if 0
                    char errbf[256];
                    snprintf( errbf, sizeof(errbf)
                            , "APV timing calibration CSV line has more tokens"
                            " than expected (found at least %d, expected %d)."
                            , (int) n, (int) (sizeof(ptrs)/sizeof(*ptrs) - 1) );
                    throw errors::ParserError( errbf );
                    #endif
                }
                *ptrs[n++] = sdc::aux::lexical_cast<double>(tok);
                if(loadLogPtr) {
                    assert(tokLabel);
                    loadLogPtr->add_entry(*tokLabel, tok);
                }
                ++tokLabel;
            }
            if( ptrs[n] ) {
                if(!ptrs[n]) {
                    throw errors::ParserError( "APV timing calibration CSV line"
                            " has less tokens than expected." );
                }
            }
        }
        return obj;
    }
};  // struct CalibDataTraits<APVTiming>

/// Calibration data traits for APV pedestals CSV format
template<>
struct CalibDataTraits<P348_RECO_NAMESPACE::APVPedestalChannelEntry> {
    /// Type name alias
    static constexpr auto typeName = "GEMMonitor/APVPedestals";

    typedef std::tuple<std::string, int, int, int, int> CollectionKey;

    ///\brief  Hash for key type of APV pedestal collection
    ///
    /// Defines hash sum function for the tuple consisting of detector name
    /// and DAQ identifiers. This hash sum is needed to put the pedestals in
    /// the map.
    struct CollectionKeyHash /*: public std::unary_function<CollectionKey, std::size_t>*/ {
        std::size_t operator()(const CollectionKey & k) const {
            return std::hash<std::string>{}(std::get<0>(k))
                 ^ (std::get<1>(k) >> 1)
                 ^ (std::get<2>(k) << 1)
                 ^ (std::get<3>(k) >> 2)
                 ^ (std::get<3>(k) << 2)
                 ;
        }
    };

    /// APV timing pedestals information collection type
    template<typename T=P348_RECO_NAMESPACE::APVPedestalChannelEntry>
        using Collection=std::unordered_map< std::tuple<std::string, int, int, int, int>
                                           , std::list<T>
                                           , CollectionKeyHash
                                           >;
    //typedef std::unordered_map< std::tuple<std::string, int, int, int, int>
    //                          , std::list<P348_RECO_NAMESPACE::APVPedestalChannelEntry>
    //                          , CollectionKeyHash
    //                          > Collection;
    ///\brief Assumes that detector name is defined in meta information
    ///
    /// Since the detector name and validity period are provided by file path
    /// rather than by file content, this function relies on the metainfo.
    ///
    /// Pedestal files generated by GEMMontitor have some kind of DAQ-related
    /// data delimiting CSV blocks. Those lines look like `#ChipAPV 0 622 0 14`.
    /// We collect this data as metadata with empty key (retrieved by `""`) and
    /// use it to fill different blocks.
    template<typename T=P348_RECO_NAMESPACE::APVPedestalChannelEntry>
    static inline void collect( Collection<T> & c
                              , const T & entry
                              , const aux::MetaInfo & mi
                              , size_t ll
                              ) {
        const size_t lineNo = mi.get<size_t>("@lineNo");
        // Obtain key for the entries
        CollectionKey k; {
            std::get<0>(k) = mi.get_strexpr("detector", lineNo);
            const auto toks_ = sdc::aux::tokenize( mi.get_strexpr("", lineNo) );
            if( *toks_.begin() != "ChipAPV" ) {
                throw sdc::errors::ParserError( "Expected first token for APV"
                        " pedestal file metadata is \"ChipAPV\"", mi.get_strexpr("", lineNo) );
            }
            std::vector<std::string> toks(toks_.begin(), toks_.end());
            std::get<1>(k) = sdc::aux::lexical_cast<int>(toks[1]);
            std::get<2>(k) = sdc::aux::lexical_cast<int>(toks[2]);
            std::get<3>(k) = sdc::aux::lexical_cast<int>(toks[3]);
            std::get<4>(k) = sdc::aux::lexical_cast<int>(toks[4]);
        }
        auto ir = c.emplace( k
                           //, std::list<P348_RECO_NAMESPACE::APVPedestalChannelEntry>()
                           , typename Collection<T>::mapped_type()
                           );
        ir.first->second.push_back(entry);
    }
    /**\brief Parses CSV-like line within a APV pedestals calibration file
     *
     * Those files are produced by GEMMonitor as a result of
     * pedestals scanning procedure. The CSV part of those files has 5 columns
     * of rigid predefined semantics:
     *  - channelflag
     *  - pedestalamp
     *  - pedestalsigma
     *  - calibrationamp
     *  - calibrationsigma
     *
     * Ref: https://gitlab.cern.ch/compass-gem/gemMonitor/blob/master/src/gemAnalyze.cc:71
     * */
    static inline P348_RECO_NAMESPACE::APVPedestalChannelEntry parse_line(
                        const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * logPtr=nullptr
                      ) {
        // TODO: utilize load log ptr
        P348_RECO_NAMESPACE::APVPedestalChannelEntry obj;
        const auto toks = aux::tokenize(line);
        auto it = toks.begin();

        assert(it != toks.end());
        if(logPtr) logPtr->add_entry("channelflag", *it);
        obj.channelflag = sdc::aux::lexical_cast<int>(*(it++));

        assert(it != toks.end());
        if(logPtr) logPtr->add_entry("pedestalamp", *it);
        obj.pedestalamp = sdc::aux::lexical_cast<double>(*(it++));

        assert(it != toks.end());
        if(logPtr) logPtr->add_entry("pedestalsigma", *it);
        obj.pedestalsigma = sdc::aux::lexical_cast<double>(*(it++));

        assert(it != toks.end());
        if(logPtr) logPtr->add_entry("calibrationamp", *it);
        obj.calibrationamp = sdc::aux::lexical_cast<double>(*(it++));

        assert(it != toks.end());
        if(logPtr) logPtr->add_entry("calibrationsigma", *it);
        obj.calibrationsigma = sdc::aux::lexical_cast<double>(*(it++));
        assert(it == toks.end());

        return obj;
    }
};  // struct CalibDataTraits<APVTiming>

}  // namespace sdc

namespace na64dp {
namespace calib {

typedef sdc::SrcInfo<P348_RECO_NAMESPACE ::APVTimingCalibEntry> APVTimingCalib;
typedef sdc::SrcInfo<P348_RECO_NAMESPACE ::APVPedestalChannelEntry> APVChannelEntry;

}  // namespace ::na64dp::calib
}  // namespace na64dp

