/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include <memory>
#include <sdc-base.hh>
#ifndef P348_RECO_NAMESPACE
#   error "This header is intended to be included from within sdc.hh"
#endif

// If macro is enabled, only declarations remain here. Use it for linkage
// compatibility (inherited from p348reco's version of SDC to keep
// compatibility)
#if defined(SDC_NO_IMPLEM) && SDC_NO_IMPLEM
#   define SDC_INLINE /* no inline */
#   define SDC_ENDDECL ;
#else
#   define SDC_ENDDECL /*;*/
#   ifndef SDC_INLINE
#       define SDC_INLINE inline
#   endif
#endif

// Define SDC traits around NA64sw's event type
namespace sdc {
template<>
struct ValidityTraits<na64dp::EventID> {
    static constexpr na64sw_EventID_t unset = 0x0;
    static constexpr char strRangeDelimiter = '-';
    static inline bool is_set(na64dp::EventID id) { return 0x0 != id; }
    static inline bool less(na64dp::EventID a, na64dp::EventID b) { return a < b; }
    /// Makes a string from event ID of form
    /// "<run>[/<spill>][.<event-in-spill>]", where
    /// <spill> and <event-in-spill> are optional (won't appear if set to 0)
    static std::string to_string(na64dp::EventID eid);
    static inline void advance(na64dp::EventID & rn) {
        na64sw_EventID_t nID = rn;
        ++nID;
        rn = na64dp::EventID(nID);
    }
    /// Parses event ID of form "<run>[/<spill>][.<event-in-spill>]", where
    /// <spill> and <event-in-spill> are optional (considered as 0 if omitted)
    static na64dp::EventID from_string(const std::string & stre);
    using Less = std::less<na64dp::EventID>;
};
}  // namespace sdc

namespace P348_RECO_NAMESPACE {

//
// This appendix is included into official p348reco lib
//////////////////////////////////////////////////////

// Shared with p348reco {{{

#if 0
/// Run number type
//typedef size_t RunNo_t;
struct RunNo_t {
    /// Actual value of the run number
    size_t v;
    /// Returns true if both run number match
    bool operator==(const RunNo_t & b) const { return v == b.v; }
    /// (trivial) less comparison
    bool operator<(const RunNo_t & b) const { return v < b.v; }
};
#endif

/// Standard NA64 SADC calibration entry
struct CaloCellCalib {
    std::string name;  ///< Name of the station + Z/station No
    int z;  ///< z-index/station no. of the entity (cell)
    int x;  ///< x-index of the entity (cell)
    int y;  ///< y-index of the entity (cell)
  
    float time;  ///< timing calibration of the entity (cell)
    float timesigma;  ///< sigma of the timing calibration of the entity (cell)
    bool have_time;  ///< `true` when time calibration is available

    double factor;  ///< multiplication factor for peak position of the entity (cell)
    double peakpos;  ///< peak position of the entity (cell)
    double refLED;  ///< LED reference value

    bool is_relative;  ///< `time` is relative to master?
    //... (other fields?)
};

//
// NA64 UTILITY TYPES AND FUNCTIONS

/**\brief returns a pair of detector kin name and station number by joint str
 *
 * For instance, "ECAL0" input will yield ("ECAL", 0) pair.
 * */
inline std::pair<std::string, int> split_name(const std::string & nm) {
    // extract detector kin from its name
    size_t n;
    for( n = 0; n < nm.size() && ! isdigit(nm[n]); ++n ) {;}
    if( !n ) {
        // note, that event though file is not specified here in exception
        // constructor, it will be appended to exception body higher on
        // stack
        throw sdc::errors::ParserError( "Detector name expected in first column"
                , nm );
    }
    const std::string kin = nm.substr(0, n);
    if( n != nm.size() ) {
        // remaining suffix is "z"
        const std::string suffix = nm.substr(n);
        return {kin, sdc::aux::lexical_cast<int>(suffix)};
    }
    return {kin, 0};
}

}  // namespace }  // P348_RECO_NAMESPACE

//
// Traits

namespace sdc {

#if 0
/**\brief Validity traits specialization for `RunNo_t` key type */
template<>
struct ValidityTraits<P348_RECO_NAMESPACE::RunNo_t> {
    static constexpr P348_RECO_NAMESPACE::RunNo_t unset = {0};
    static constexpr char strRangeDelimiter = '-';
    static inline bool is_set( P348_RECO_NAMESPACE::RunNo_t id)
        { return unset.v != id.v; }
    static inline bool less( P348_RECO_NAMESPACE::RunNo_t a
                           , P348_RECO_NAMESPACE::RunNo_t b)
        { return a.v < b.v; }
    static inline std::string to_string(P348_RECO_NAMESPACE::RunNo_t rn)
        { return std::to_string(rn.v); }
    static inline void advance(P348_RECO_NAMESPACE::RunNo_t & rn) { ++rn.v; }
    static inline P348_RECO_NAMESPACE::RunNo_t from_string(const std::string & stre) {
        return {aux::lexical_cast<size_t>(stre)};
    }
    using Less = std::less<P348_RECO_NAMESPACE::RunNo_t>;
};
#endif

/** Traits implementing parser logic for `CaloCellCalib` struct
 *
 * This is the data structure from the original Anton's specification. It
 * expects lines provided within a special format:
 * 
 *   <name:str><z-index:int> <x:int> <y:int> ...
 *
 * Special treatment to be applied to interpret first three columns:
 *
 *      HCAL3 1 2 => name="HCAL3" z=3, x=1, y=2
 *
 * meaning that station number goes into data structure as "z-index". The rest
 * of line columns:
 *  - `<Amp 100 GeV:float> <sigma 100 GeV:float>` -- mean and sigma values for
 *      electron calibration peak
 *  - `<Amp 100 GeV:float> <sigma 100 GeV:float>` -- mean and sigma values for
 *      muon calibration peak
 *  - `<run:int>` -- calibration run number
 * 
 * Metadata used (besides of `runs` and `type`:
 *  - `factor.<name>` -- a special factor $f$ used to calculate `K`. The result
 *      is $K=f/peakpos$ where $peakpos$ is taken from column
 *  - `columns` -- denotes meaning of columns (like "name,x,y,peakpos")
 *
 */
template<>
struct CalibDataTraits<P348_RECO_NAMESPACE::CaloCellCalib> {
    /// Type name alias
    static constexpr auto typeName = "p348reco/CaloCellCalib";
    /// A collection type of the parsed entries
    template<typename T=P348_RECO_NAMESPACE::CaloCellCalib>
        using Collection=std::list<T>;
    //typedef std::list<P348_RECO_NAMESPACE::CaloCellCalib> Collection;
    /// An action performed to put newly parsed data into collection
    template<typename T=P348_RECO_NAMESPACE::CaloCellCalib>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t ll
                              ) { col.push_back(e); }

    /// This way one could define the data type name right in the structure
    /// defining a parser for data line. However, p348reco forbids
    /// doing this because of "header-only" politics that doesn't allow
    /// to define static data members for reentrant usage.
    //static constexpr auto typeName = "CaloCell";

    /// Parses the CSV line into `CaloCellCalib` data object
    ///
    /// Parsing can be affected by the parser state to handle changes within
    /// a file. Namely, metadata "columns=name1,name2..." may denote number of
    /// token with certain semantics. By default, "name,x,y,peakpos" is assumed
    static P348_RECO_NAMESPACE ::CaloCellCalib
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & filename
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      ) {
        // create object and set everything to zero
        P348_RECO_NAMESPACE::CaloCellCalib obj;
        obj.have_time = false;
        #ifndef NDEBUG
        const size_t lineNo_ = m.get<size_t>("@lineNo");
        assert(lineNo_ == lineNo);
        #endif
        obj.is_relative = m.get<bool>("timeIsRelativeToMaster", true, lineNo);
        // Create and validate columns order
        auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
                //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
                .interpret(aux::tokenize(line), loadLogPtr);
        // ^^^ note: every CSV block must be prefixed by columns order,
        // according to Anton's specification

        // Get columns
        obj.name = csv("name");
        obj.x = csv("x", 0);
        obj.y = csv("y", 0);
        // get name from line, extract z-index and station kin
        auto kinNoPair = P348_RECO_NAMESPACE::split_name(obj.name);
        obj.z = kinNoPair.second;

        // apply arithmetics to peakpos, if some special fields are defined in the
        // metadata for current line. We try first the particular name+Z
        // (e.g. "factor.ECAL0"), otherwise kin (like "factor.WCAL")
        obj.peakpos = csv("peakpos", std::nan("0"));
        // ^^^ TODO: Anton asked K=1 by default, however it is not the same
        //     as `peakpos'. Clarify it.
        {
            // check, if the metadata entry was deined prior to current line
            obj.factor = m.get<double>("factor." + obj.name, std::nan("0"), lineNo );
            if( std::isnan(obj.factor) )
                obj.factor = m.get<float>("factor." + kinNoPair.first, std::nan("0"), lineNo );
        }
        // Get other optional columns
        obj.time = csv("time", std::nan("0"));
        obj.timesigma = csv("timesigma", std::nan("0"));
        obj.refLED = csv("ledref", std::nan("0"));
        //if( std::isnan(obj.time) ^ std::isnan(obj.sigma) )
        //    throw errors::DataError( "Time/sigma must be both given or none." );
        if( !std::isnan(obj.time) )
            obj.have_time = true;
        // ... other columns
        return obj;
    }
};  // struct CalibDataTraits<::CaloCellCalib>

}  // namespace sdc

namespace P348_RECO_NAMESPACE {

///\brief Function requested by initial specification: straightforward load of
///       calorimeter calibration data
///
/// Highly specialized one. Loads list of `CaloCellCalib` structures for
/// certain run using the path provided here.
SDC_INLINE std::vector<CaloCellCalib>
load_calib(std::string rootpath, int run) SDC_ENDDECL
#if (!defined(SDC_NO_IMPLEM)) || !SDC_NO_IMPLEM
{
    // 1. SETUP
    // create calibration document index by run number
    sdc::Documents<na64dp::EventID> docs;
    // Create a loader object and add it to the index for automated binding.
    // This type of loader (ExtCSVLoader) is pretty generic one and implies
    // a kind of "extended" grammar for CSV-like files.
    auto extCSVLoader = std::make_shared< sdc::ExtCSVLoader<na64dp::EventID> >();
    docs.loaders.push_back(extCSVLoader);
    // ^^^ one can customize this loader's grammar by modifying its public
    //     `grammar` attribute or its defaults. For instance, we assume all the
    //     discovered files to have `CaloCellCalib` data type by default:
    extCSVLoader->defaults.dataType = "p348reco/CaloCellCalib";

    // create filesystem iterator to walk through all the dirs and their
    // subdirs looking for files matching certain wildcards and size criteria
    sdc::aux::FS fs( rootpath
                   , "" // (opt) accept patterns
                   , "*.swp:*.swo:*.bak:*.BAK:*.bck:~*:*-orig.txt:*.dev" // (opt) reject patterns
                   , 10  // (opt) min file size, bytes
                   , 128*1024*1024  // (opt) max file size, bytes
                   );
    // use this iterator to fill documents index by recursively traversing FS
    // subtree and pre-parsing all matching files
    docs.add_from(fs);

    // 2. LOAD DATA FOR CERTAIN RUN ID
    // We no load the data into collection.
    // Usually, when no origin info is required for the data being fetched,
    // one can simply call `docs.load<CustomDataType>(runNo)` and that's it.
    // However, for `CaloCellCalib` it is requested to apply additional check
    // on the origin, so we use a templated wrapper `SrcInfo<T>` here to
    // gain some info on the source document for every entry.
    auto l_ = docs.load< sdc::SrcInfo<CaloCellCalib> >(na64dp::EventID( run, 0, 0 ));

    // assure we have no same "name" defined in distinct files (specification
    // policy addendum)
    std::list<CaloCellCalib> l;
    // collection of file sources by name
    std::unordered_map<std::string, std::string> locs;
    {
        for(const sdc::SrcInfo<CaloCellCalib> & cloc : l_) {
            const auto p = P348_RECO_NAMESPACE::split_name(cloc.data.name);
            auto ir = locs.emplace(p.first, cloc.srcDocID);
            if((!ir.second) && ir.first->second != cloc.srcDocID) {
                char errbf[1024];
                snprintf(errbf, sizeof(errbf), "Multiple definitions of"
                        " calibration data entry `CaloCellCalib' for same"
                        " detector kin (%s) revealed for run %d; current"
                        " definition \"%s\" at %s:%zu is conflicting with"
                        " previous one of same kin defined in %s for same run"
                        , p.first.c_str()
                        , run
                        , cloc.data.name.c_str()
                        , cloc.srcDocID.c_str()
                        , cloc.lineNo
                        , ir.first->second.c_str()
                        );
                throw std::runtime_error(errbf);
            }
            l.push_back(cloc.data);
        }
    }

    // filter out duplicating entries (spcification policy addendum)
    // From specification:
    //  For every individual entry only the last cell must be taken, so we
    //  have to remove the previous entries. Unique entry is identified by
    //  name(z)+x+y
    typedef std::tuple<std::string, int, int> CellKey;  // cell key
    // key hashing operator
    struct CellKeyHash
            /*: public std::unary_function<CellKey, std::size_t>*/ {
        std::size_t operator()( const CellKey & k ) const {
            return std::hash<std::string>{}(std::get<0>(k))
                 ^ (std::get<1>(k) << 1)
                 ^ (std::get<1>(k) << 2)
                 ;
        }
    };
    // collection of unique cells
    std::unordered_map< CellKey
                      , CaloCellCalib
                      , CellKeyHash
                      > uniqEntries;
    // Set data avoiding NaNs to provide incremental assignment
    for( auto eIt = l.begin(); eIt != l.end(); ++eIt) {
        const CaloCellCalib & cur = *eIt;
        auto ir = uniqEntries.emplace(CellKey{eIt->name, eIt->x, eIt->y}, cur);
        if( ir.second ) continue;
        // insertion failed => this is aduplicate, set struct attrs
        // avoiding NaNs
        CaloCellCalib & dest = ir.first->second;
        assert(dest.name == cur.name && dest.x == cur.x && dest.y == cur.y);
        // ^^^ guaranteed by map
        if( !std::isnan(cur.peakpos) ) dest.peakpos = cur.peakpos;
        if( cur.have_time ) {
            assert(!std::isnan(cur.time));
            //assert(!std::isnan(cur.sigma));
            dest.time = cur.time;
            dest.timesigma = cur.timesigma;
            dest.have_time = true;
        }
        // ... (other fields?)
    }
    // nice side effect of collecting entries by detector kin -- since current
    // p348 policy explicitly restricts one file per single detector kin, we
    // can provide user with brief info on calibration files used
    for( const auto & loc : locs) {
        size_t nEntriesOfKin = 0;
        for( auto e : uniqEntries ) {
            if(0 == std::get<0>(e.first).rfind(loc.first, 0)) {
                ++nEntriesOfKin;
            }
        }
        std::cout << "Info: sdc(): run=" << run << ", file=\""
            << loc.second << "\", "
            << loc.first
            << " (" << nEntriesOfKin << " entries)."
            << std::endl;
    }
    std::vector<CaloCellCalib> res;
    std::transform( uniqEntries.begin(), uniqEntries.end()
                  , std::back_inserter(res)
                  , [](const decltype(uniqEntries)::value_type & p) { return p.second; } );
    return res;
}  // load_calib()
#endif

}  // P348_RECO_NAMESPACE

// }}} p348reco


//
// This appendix is not included into official p348reco lib
//////////////////////////////////////////////////////////

namespace P348_RECO_NAMESPACE {

/// Calendar date type
struct Datetime_t {
    /// Actual value of the astronomical date and time
    time_t v;
    /// Returns true if both date+time values match (with full precision)
    bool operator==(const Datetime_t & b) const { return v == b.v; }
    /// (trivial) less comparison
    bool operator<(const Datetime_t & b) const { return v < b.v; }
};

/**\brief Provides GEMMonitor-compatible date+time from-string conversion
 *
 * Routine providing 
 * */
time_t parse_GEMMonitor_timedate_strexpr(const char *c);

#if 0
/**\brief Parses string expression commonly found in the file names produced by
 *      GEMMonitor app
 *
 * ...like "2018-05-14-20:14:57", "2016-07-09-18:50:42"
 *
 * \note technically, returned value is a real timestamp only when current
 * timezone
 * */
inline time_t
parse_GEMMonitor_timedate_strexpr( const char * c ) {
    // Parse timedate string into GNU POSIX ::tm instance
    // Note, that these time strings are given in local CERN-Prevessin EHN1
    // location's timezone (Europe/Paris). To get the true timestamp (UTC)
    // one need to convert it using timezone info
    ::tm tm;
    memset(&tm, 0, sizeof(tm));
    strptime(c, "%Y-%m-%d-%H:%M:%S", &tm);
    //printf( "xxx %s\n", tm.tm_zone );
    // Not set by strptime(); tells mktime() to determine whether daylight
    // saving time is in effect:
    tm.tm_isdst = -1;
    // Force the measurement timezone
    //tm.tm_zone = "Europe/Paris";  // does not work
    // Convert to time_t
    time_t t = mktime(&tm);
    if(-1 == t) {
        return 0;
    }
    //printf( "%s => year=%d, month=%d, date=%d, time=%d:%d:%d ; timestamp=%ld\n"
    //          , c
    //          , 1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday
    //          , tm.tm_hour, tm.tm_min, tm.tm_sec
    //          , t
    //          );  // xxx
    return t;
}  // parse_GEMMonitor_timedate_strexpr()
#endif

}  // namespace <P348_RECO_NAMESPACE>

namespace sdc {
///\brief Specialization for `time_t` type as validity period key
///
/// Most of this traits implementation are standard ones, except for to-/from-
/// string conversions where timedate string is involved instead of just long
/// integer
///
/// \warning To provide compatibility with GEMMonitor-generated library,
///          to-/from-string `Datetime_t` converion utilities must match
///          the notation used by GEMMonitor.
template<>
struct ValidityTraits<P348_RECO_NAMESPACE::Datetime_t> {
    static constexpr P348_RECO_NAMESPACE::Datetime_t unset = {0};
    static constexpr char strRangeDelimiter = '~';
    static inline bool is_set( P348_RECO_NAMESPACE::Datetime_t id)
        { return unset.v != id.v; }
    static inline bool less( P348_RECO_NAMESPACE::Datetime_t a
                           , P348_RECO_NAMESPACE::Datetime_t b)
        { return a.v < b.v; }
    static inline std::string to_string(P348_RECO_NAMESPACE::Datetime_t rn) {
        char bf[128];
        struct tm tm;
        gmtime_r(&rn.v, &tm);
        strftime( bf, sizeof(bf)
                , "%Y-%m-%d-%H:%M:%S"
                , &tm
                );
        return bf;
    }
    static inline void advance(P348_RECO_NAMESPACE::Datetime_t & rn) { ++rn.v; }
    static inline P348_RECO_NAMESPACE::Datetime_t from_string(const std::string & stre) {
        time_t t = P348_RECO_NAMESPACE::parse_GEMMonitor_timedate_strexpr(stre.c_str());
        if( 0 == t ) {
            throw sdc::errors::ParserError( "Unable to parse datetime expression"
                    , stre );
        }
        return {t};
    }
    using Less = std::less<P348_RECO_NAMESPACE::Datetime_t>;
};
}  // namespace sdc

namespace P348_RECO_NAMESPACE {

// NOTE: defined here and not in "utils" section to avaoid "specialization
// after instantiation" of `sdc::ValidityTraits<time_t>' struct

/// Alias of the data taking period (e.g. 2016, 2017b, 2021mu, etc)
struct PeriodAlias {
    sdc::ValidityRange<na64dp::EventID> runs;
    sdc::ValidityRange<Datetime_t> time;

    PeriodAlias()
        : runs{ na64dp::EventID(sdc::ValidityTraits<na64dp::EventID>::unset)
              , na64dp::EventID(sdc::ValidityTraits<na64dp::EventID>::unset) }
        , time{ sdc::ValidityTraits<Datetime_t>::unset
              , sdc::ValidityTraits<Datetime_t>::unset }
        {}

    PeriodAlias( const std::string & timeStart, int firstRun
               , const std::string & timeEnd,   int lastRun )
        : runs{na64dp::EventID(firstRun), na64dp::EventID(lastRun)}
        , time{ parse_GEMMonitor_timedate_strexpr(timeStart.c_str())
              , parse_GEMMonitor_timedate_strexpr(timeEnd.c_str())
              } {}
};

/**\brief List of aliases used for calibration data periodisation in NA64
 *
 * This collection must be filled with ordered list of pairs of "alias name"
 * (e.g. "2015", "2016e") vs run number and astronomical timespan. Used to
 * derive validity period by path wherever standard means of validity period
 * assumption do not work.
 * */
struct Aliases : public std::list<std::pair<std::string, PeriodAlias>> {
    /// Shortcut for the entries in the aliases list; represents alias name
    /// and corresponding period
    typedef std::pair<std::string, PeriodAlias> AliasEntry;

    /// Constructs wrapper container (this type) over ordered list of aliases
    Aliases( const std::list<std::pair<std::string, PeriodAlias>> & aliases )
        : std::list<std::pair<std::string, PeriodAlias>>(aliases) {}
    /// Trivial ctr for postponed fill
    Aliases() {}

    ///\brief Returns list of aliases, presumably corresponding to given path
    ///
    /// Iterates backwards by the aliases list performing revers lookup for
    /// aliases keys in the path and fills list of the matching alias
    /// instances.
    std::list<AliasEntry> assume_validity(const std::string & docID) const {
        std::list<AliasEntry> matches;
        for( auto it = this->rbegin(); it != this->rend(); ++it ) {
            size_t n = docID.rfind(it->first);
            if(std::string::npos == n) continue;
            matches.push_back(*it);
        }
        return matches;
    }
};

}  // namespace <P348_RECO_NAMESPACE>

namespace na64dp {
namespace util {
struct GEMMonitorFilenameSemantics;
}  // namespace ::na64dp::util
}  // namespace na64dp

namespace P348_RECO_NAMESPACE {

// This specializations are useful for applications

template<typename T> struct KeyTraits;

template<> struct KeyTraits<na64dp::EventID> {
    typedef sdc::aux::FS LookupObject;

    static std::shared_ptr<LookupObject>
    instantiate_lookup_object( const std::string & paths
                             , const std::string & acceptPatterns
                             , const std::string & rejectPatterns
                             , size_t minSize, size_t maxSize
                             ) {
        return std::make_shared<LookupObject>
                               ( paths
                               , acceptPatterns, rejectPatterns
                               , minSize, maxSize
                               );
    }
    // ...
};

template<> struct KeyTraits<Datetime_t> {
    typedef na64dp::util::GEMMonitorFilenameSemantics LookupObject;

    static std::shared_ptr<LookupObject>
    instantiate_lookup_object( const std::string &
                             , const std::string &
                             , const std::string &
                             , size_t, size_t
                             );
};

}  // namespace <P348_RECO_NAMESPACE>

