#pragma once

#include <stdexcept>

namespace na64common {
namespace errors {

/**\brief Standard generic runtime error class for na64common lib
 *
 * Assumed usage in try/catch block to find out lib-related errors.
 * Foreseen errors in the library are subclasses of this common parent.
 * */
class GenericRuntimeError : public std::runtime_error {
public:
    GenericRuntimeError() throw() : std::runtime_error("Generic na64common lib error") {}
    GenericRuntimeError(const char * msg) throw() : std::runtime_error(msg) {}
};  // class GenericRuntimeError

/**\brief Thrown in case of invalid input arguments */
class ArgumentError : public GenericRuntimeError {
public:
    ArgumentError(const char * msg) throw() : GenericRuntimeError(msg) {}
};  // class ArgumentError

}  // namespace ::na64common::errors
}  // namespace na64common

