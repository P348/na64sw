# Routines common for NA64-specific applications and extensions

Main NA64sw framework code is moving towards experiment-agnostic approach. At
some point it should be freed from experiment-specific features and renamed.

This library slowly adsobs experiment-specific features of NA64.

## sXML

sXML-related files contain SDC adapters for S.Donskov (semi) `.xml` calibration
files. Note, the project builds a dedicated static library to link with
native Sergei's applications and source files are structured in a way that not
all the routines are included into `libna64common.so` by default.

To prepare standalone distributable archive, do in this dir:

    $ make -f Makefile.sXML-redist sXML.tar.bz2

To build the static library, you'll need [SDC lib](https://github.com/CrankOne/sdc)
built as static lib and installed by prefix. Assuming this prefix to be
`/path/to/sdc` then one can build the `libsXML.a` library as follows:

    $ make -f Makefile.sXML-redist SDC_DIR=/path/to/sdc

Copy `sXMLLoader.hh` by your convenience then.

