#include "na64common/calib/apvTimeType1.hh"
#include "na64common/calib/sdc-integration.hh"

#include "na64dp/abstractHitHandler.hh"
#include "na64calib/manager-utils.hh"

//
// Traits

namespace sdc {
const char * gAPVTimingCalibEntryLabelsOrder[] = {
    "a02_r0",     "a02_t0",    "a02_a0",
    "a12_r0",     "a12_t0",    "a12_a0",
    "a02_r02",    "a02_t02",   "a02_a02",
    "a02_r0t0",   "a02_r0a0",  "a02_t0a0",
    "a12_r02",    "a12_t02",   "a12_a02",
    "a12_r0t0",   "a12_r0a0",  "a12_t0a0",
    nullptr
};
}  // namespace sdc

namespace na64dp {
namespace handlers {

using na64dp::event::APVHit;
using calib::Collection;
using calib::APVTimingCalib;
using calib::APVChannelEntry;

/**\brief Calculates hit times using type#1 calibration data
 *
 * Usage:
 * \code{.yaml}
 *     - _type: APVCalcHitTimeRatios
 *       # optional, hit selection expression
 *       applyTo: null
 *       # Handler behaviour on no calibration data available for certain
 *       # detector entry. Possible values: "ignore", "warn", "error"; opt, str
 *       onNoCalib: warn
 * \endcode
 *
 * Relies on standard p348reco/GEMMonitor-produced timing calibrations.
 *
 * \note This handler does *not* calculate time of APV hit, just a set of
 *       values required. For time calculation based on these values, consider
 *       `APVCalcTime` handler that may be applied afterwards.
 *
 * \see APVCalcTime
 * \ingroup handlers apv-handlers calib-handlers
 * \todo few error reports and meaningful assertions
 * */
class APVCalcHitTimeRatios
        : public AbstractHitHandler<APVHit>
        , public calib::Handle<Collection<APVTimingCalib>>
        , public calib::Handle<Collection<APVChannelEntry>>
        {
public:
    enum OnNoCalib {
        kIgnore,
        kWarn,
        kError
    };
protected:
    /// Behaviour on missing calibration
    const OnNoCalib _onNoCalib;
    /// Used only for "warn" mode
    std::unordered_set<DetID> _missingCalibIDs;
    /// Detector kins caches
    DetKin_t _gmKinID
           , _mmKinID
           ;
    /// Cache of the relevant timing calibrations
    std::unordered_map<PlaneKey, P348_RECO_NAMESPACE::APVTimingCalibEntry> _timingCalibs;

    /// Hash function for the APV channel calib cache
    struct APVChHash /*: public std::unary_function< std::tuple<uint16_t, uint16_t, uint16_t>
                                                 , std::size_t>*/ {
        std::size_t operator()(const std::tuple<uint16_t, uint16_t, uint16_t> & k) const {
            return std::get<0>(k) << 8
                 ^ std::get<1>(k) << 4
                 ^ std::get<2>(k)
                 ;
        }
    };
    /// APV channel calib cached entry
    struct APVChCalib {
        std::string planeName;
        std::vector<P348_RECO_NAMESPACE::APVPedestalChannelEntry> data;
    };
    /// Cache of the relevant per-channel timing calibrations
    std::unordered_map< std::tuple<uint16_t, uint16_t, uint16_t>  // (daq, adc, chip)
                      , APVChCalib
                      , APVChHash
                      > _channelCalibs;
    /// Collection of the unknown channels to let user know
    std::unordered_map< std::tuple<uint16_t, uint16_t, uint16_t>  // (daq, adc, chip)
                      , std::pair<std::string, event::APVDAQWire_t>  // TBname, raw hit channel
                      , APVChHash
                      > _unknownChannels;
    /// Files currently in use (xxx?)
    std::unordered_map<std::string, std::pair<std::string, size_t>> _sources;
protected:
    /// Forwards to parent's
    void handle_update( const nameutils::DetectorNaming & nm ) override {
        AbstractHitHandler<APVHit>::handle_update(nm);
        _gmKinID = nm.kin_id("GM").second;
        _mmKinID = nm.kin_id("MM").second;
    }

    /// Recaches dictionary of timing calibrations
    void handle_update( const Collection<APVTimingCalib> & calibs ) override {
        //calib::Handle<Collection<APVTimingCalib>>::handle_update(calibs);
    	for( const auto & p : calibs ) {
            const std::string & planeName = p.first;
            const P348_RECO_NAMESPACE::APVTimingCalibEntry & calib = p.second.data;
            try {
                _timingCalibs[PlaneKey(naming()[planeName])] = calib;
            } catch( na64dp::errors::GenericRuntimeError & e ) {
                _log.error( "While treating plane name \"%s\" from \"%s\":"
                            " \"%s\". APV timing calibration update skipped."
                          , planeName.c_str()
                          , p.second.srcDocID.c_str()
                          , e.what()
                          );
                return;
            }
            //auto ir = _timingCalibs.emplace(naming()[planeName], calib);
            // ^^^ todo: check insertion result?
            //     todo: append list of sources?
            //std::cout << "(xxx " << p.second.srcDocID << ")" << std::endl;  // XXX
        }
    }
    /// Recaches dictionary of per-wire calibrations
    ///
    /// Accoriding to p348reco, only 3rd value (called "sigma") is
    /// important for timing. Ibid. he X/Y distinction is made by
    /// condition (isX = (counter%2 == 0)). Note, that for large MMs
    /// no Y wires have count > 128.
    ///
    /// \note There are a lot of junk in the original calibrations set of
    ///       p348reco. Currently, only the files started with MM and GM are
    ///       taken into account.
    /// \todo Foresee usage of SrcID/Chip ID from the tuple identifying chips
    ///       as right now assumptions of number of chips and channel
    ///       enumeration is hardcoded
    void handle_update( const Collection<APVChannelEntry> & calibs ) override {
        //calib::Handle<Collection<APVChannelEntry>>::handle_update(calibs);
        for( const auto & p : calibs ) {
            const std::tuple<std::string, int, int, int, int> & originalKey
                = p.first;
            // ^^^ key semantics:
            //      <name:str> <ldc:int> <eq:int> <subeq:int> <ch:int>
            //      ldc:int  -- dummy (?)
            //      eq:int -- DAQ source ID
            //      subeq:int -- DAQ ADC ID
            //      ch:int -- DAQ chip ID
            std::tuple<uint16_t, uint16_t, uint16_t> key(
                    std::get<2>(originalKey)
                  , std::get<3>(originalKey)
                  , std::get<4>(originalKey)
                  );  // key used in cache
            APVChCalib apvChCalib{ std::get<0>(originalKey) };
            for( auto srcEntry : p.second ) {
                apvChCalib.data.push_back(srcEntry.data);
                _sources[apvChCalib.planeName]
                    = std::pair<std::string, size_t>(srcEntry.srcDocID, srcEntry.lineNo);
            }
            _channelCalibs[key] = apvChCalib;
        }
    }
public:
    /// Applies calibrations to the calculated ratio to return the estimated
    /// time
    static double calc_hit_time_calibrated( const P348_RECO_NAMESPACE::APVTimingCalibEntry::ParPack & c
                                          , double ratio  // a02 or a12
                                          ) {
        return c.t0 + c.a0 * ::log((c.r0 / ratio) - 1);
    }
    /// Applies calibrations for the hit sample data to calculate the estimated
    /// sigma (RMS) error of the hit time value. The `APVCalibDataType1Wire<>`
    /// calibration data is required at this point
    static double calc_hit_time_error_calibrated( const P348_RECO_NAMESPACE::APVTimingCalibEntry::ParPack & c
                                                , const double sigmaN  // wire calib data
                                                , double ratio  // a02 or a12
                                                , double A  // sample#0/1 for a02/a12
                                                , double B  // sample#2
                                                ) {
        const double sigmaR = ( sigmaN + 1. / sqrt(12.) )
                      * ( sqrt(pow(A, 2) + pow(B, 2)) )
                      / pow(B, 2)
                   , dtdr = c.a0 * c.r0
                           / pow( ratio, 2 )
                           * ( c.r0 / ratio - 1 )
                   , dtda0 = ::log(( c.r0 / ratio )-1)
                   , dtdt0 = 1
                   , dtdr0 = c.a0 / ( ratio * ( c.r0 / ratio - 1 ) )
                   , sigmaT = sqrt( ( pow(dtdr,2) * pow(sigmaR,2) ) + ( pow(dtdr0,2) * c.r02 )
                            + ( pow(dtdt0,2) * c.t02 ) + ( pow(dtda0,2) * c.a02 ) 
                            + 2 * ( dtdt0 * dtdr0 * c.r0t0 ) + 2 * ( dtda0 * dtdr0 * c.r0a0 )
                            + 2 * ( dtda0 * dtdt0 * c.t0a0 ) )
                   ;
        return sigmaT;
    }
protected:
    /// Forwards execution to `calc_hit_time_calibrated()` and
    /// `calc_hit_time_error_calibrated()` for appropriate ratios. For a-ratio
    /// being NaN or below the `r0` parameter, the appropriate `t*` and
    /// `t*sigma` won't be set.
    virtual void calc_time( DetID did, APVHit & cHit ) {
        if( ! cHit.rawData ) return;  // no raw data
        if( cHit.rawData->samples[2] <= 0 ) return;  // leave time not initialized
        // Get channel sigma:
        // - find calibration data block for this hit
        std::tuple<uint16_t, uint16_t, uint16_t> hitKey = {
                cHit.rawData->srcID, cHit.rawData->adcID, cHit.rawData->chip };
        auto chCalibIt = _channelCalibs.find(hitKey);
        if( chCalibIt == _channelCalibs.end() ) {
            _unknownChannels.emplace( hitKey,
                    std::pair<std::string, event::APVDAQWire_t>(
                        naming()[did], cHit.rawData->wireNo) );
            return;  // leave time not initialized
        }
        // - get channel-specific sigma (depends on the detector type)
        if( cHit.rawData->chipChannel > chCalibIt->second.data.size() ) {
            NA64DP_RUNTIME_ERROR( "Chip channel number of APV hit #%d exceeds"
                    " number of entries within calibration data block (>=%d)."
                    " Plane: \"%s\", DAQ ID for the hit: <chip=%d, srcID=%d,"
                    " adcID=%d>"
                    , (int) cHit.rawData->chipChannel
                    , (int) chCalibIt->second.data.size()
                    , naming()[did].c_str()
                    , (int) cHit.rawData->chip
                    , (int) cHit.rawData->srcID
                    , (int) cHit.rawData->adcID
                    );
        }
        // todo: compare chCalibIt->second.planeName with naming()[detID] to
        // check for the DAQ vs naming integrity?
        double sigmaN
            = chCalibIt->second.data[cHit.rawData->chipChannel].pedestalsigma;
        // find "timing" calibrations
        PlaneKey pk(did);
        auto it = _timingCalibs.find(pk);
        if( _timingCalibs.end() == it ) {
            if( kError == _onNoCalib ) {
                std::ostringstream oss;
                oss << "Detectors on record: ";
                bool isFirst = true;
                for( auto & p : _timingCalibs ) {
                    if(isFirst) isFirst = false; else oss << ", ";
                    oss << naming()[p.first];
                }
                if( _timingCalibs.empty() ) oss << "(none)";
                _log.error( oss.str() );
                NA64DP_RUNTIME_ERROR( "Can not find calibration data type#1 for"
                        " APV detector plane %s.", naming()[pk].c_str() );
            } else if( kIgnore == _onNoCalib ) {
                return;  // leave time uninit
            } else {  // warn
                // TODO: store detector IDs AND names for verbose printing as
                //       `naming()` at `finalize()` can differ from what is
                //       used currently
                auto ir = _missingCalibIDs.emplace(pk);
                if( ! ir.second )  return;  // known, just leave
                log().warn( "No APV timing calibration for %s."
                        " This is the first occurence, other occurences will"
                        " be quetly ignored."
                        , naming()[pk].c_str()
                        );
                return;
            }
        }
        const P348_RECO_NAMESPACE::APVTimingCalibEntry & tc = it->second;  // ref to APV time calib type #1
        // NOTE: conditions below will fail if a02/a12 are set to NaN meaning
        // that this handler won't produce any value if ratios aren't
        // calculated
        if( tc.a02.r0 > cHit.a02 && cHit.a02 ) {
            cHit.t02 = calc_hit_time_calibrated( tc.a02, cHit.a02 );
            cHit.t02sigma = calc_hit_time_error_calibrated( tc.a02
                    , sigmaN  // sigmaN
                    , cHit.a02  // ratio
                    , cHit.rawData->samples[0] // A
                    , cHit.rawData->samples[2] // B
                    );
        }
        if( tc.a12.r0 > cHit.a12 && cHit.a12 ) {
            cHit.t12 = calc_hit_time_calibrated( tc.a12, cHit.a12 );
            cHit.t12sigma = calc_hit_time_error_calibrated( tc.a12
                    , sigmaN  // sigmaN
                    , cHit.a12  // ratio
                    , cHit.rawData->samples[1] // A
                    , cHit.rawData->samples[2] // B
                    );
        }
    }
public:
    APVCalcHitTimeRatios( calib::Dispatcher & cdsp
                        , const std::string & selection
                        , OnNoCalib onNoCalib
                        , log4cpp::Category & logCat
                        , const std::string & calibClass="default"
                        , const std::string & wireCalibClass="default"
                        , const std::string & namingSubclass="default"
                        ) : AbstractHitHandler<APVHit>(cdsp, selection, logCat, namingSubclass)
                          , calib::Handle<Collection<APVTimingCalib>>(calibClass, cdsp)
                          , calib::Handle<Collection<APVChannelEntry>>(wireCalibClass, cdsp)
                          , _onNoCalib(onNoCalib)
                          //APVTime<calib::APVCalibDataType1>(cdsp, selection, calibClass, namingClass)
                          //, calib::ByDetectorNames<calib::APVCalibDataType1Wire<TWiresNo> >(cdsp, wireCalibClass, namingClass)
                          {}

    bool process_hit( EventID, DetID did, APVHit & cHit ) override {
        if( ! cHit.rawData ) return true;  // no raw data in hit, continue processing
        double wCharge = 0.
             , charge = 0.
             ;
        cHit.maxCharge = cHit.rawData->samples[0];
        // Iterate over analog samples    
        for (int i = 0; i < 3; ++i) {
            double q = cHit.rawData->samples[i];
            wCharge += q * (i);
            charge  += q;
            if (cHit.maxCharge < q) cHit.maxCharge = q;
        }
        // Calculate A ratios, xxx -- dedicated handler
        //cHit.a02 = cHit.rawData->samples[0]/cHit.rawData->samples[2];
        //cHit.a12 = cHit.rawData->samples[1]/cHit.rawData->samples[2];
        // ^^^ xxx -- done by dedicated handler

        calc_time( did, cHit );

        // xxx: below is for dedicated handler
        #if 0
        if ( cHit.t02 != 0 && cHit.t12 != 0 ) {
            cHit.time = cHit.t02 / pow(cHit.t02sigma,2)
                      + cHit.t12 / pow(cHit.t12sigma,2)
                      ;
            cHit.timeError = 1 / pow(cHit.t02sigma,2)
                           + 1 / pow(cHit.t12sigma,2)
                           ;
            cHit.time = cHit.time / cHit.timeError;
            cHit.timeError = 1 / sqrt( cHit.timeError );
            if (cHit.timeError > 100 ) {
                // Bad hit time error estimation -- erase the hit
                _set_hit_erase_flag();
            }
        } else {
            // Failed to calculate hit time -- drop the hit
            _set_hit_erase_flag();
        }
        #endif
        return true;
    }
    void finalize() override {
        if( ! _unknownChannels.empty() ) {
            std::ostringstream oss;
            oss << "There were unaddressed channels for APV pedestal timing "
                   " (missing \"GEMMonitor/APVPedestals\" calib info)"
                   ", format is [DAQ, ADC, chip -> TBname, raw hit channel]: ";
            bool isFirst = true;
            for( auto nm : _unknownChannels ) {
                if( !isFirst ) oss << ", "; else isFirst = false;
                oss << "["  << std::get<0>(nm.first)
                    << ", " << std::get<1>(nm.first)
                    << ", " << std::get<2>(nm.first)
                    << " -> "
                    << nm.second.first << ", " << (int) nm.second.second
                    << "]";
            }
            oss << ".";

            _log.warn(oss.str());
        }
        if( ! _missingCalibIDs.empty() ) {
            std::ostringstream oss;
            oss << "There were APV detectors with missing timing calibrations "
                   " (missing \"GEMMonitor/APVTiming\" calib info):"
                ;
            bool isFirst = true;
            for( auto did : _missingCalibIDs ) {
                if( !isFirst ) oss << ", "; else isFirst = false;
                oss << naming()[did];
            }
            oss << ".";
            _log.warn(oss.str());
        }
    }
};  // class APVCalcHitTimeRatios

}  // namespace ::na64dp::handlers


REGISTER_HANDLER( APVCalcHitTimeRatios, mgr, cfg
                , "Calculates time ratios and errors for APV-based detectors"
                  " using APV calib info of type #1 (yet, does not calculate"
                  " time itself!). Relies on standard p348reco/GEMMonitor"
                  " pedestals and timing calibration data." ) {
    #if 1  // NOTE: moved to app.cc due to aliases clash
    // Register calibration data type, instantiate SDC conversion (relies
    // on traits defined above), establish deps
    mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVChannelEntry>();
    mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVTimingCalib>();
    na64dp::calib::CIDataAliases::self()
            .add_dependency("GEMMonitor/APVPedestals", "naming");
    na64dp::calib::CIDataAliases::self()
            .add_dependency("GEMMonitor/APVTiming",    "naming");
    #endif
    //
    handlers::APVCalcHitTimeRatios::OnNoCalib onNoCalib;
    if( !cfg["onNoCalib"] ) {
        onNoCalib = handlers::APVCalcHitTimeRatios::kWarn;  // default
    } else {
        const std::string & mode = cfg["onNoCalib"].as<std::string>();
        if( "warn" == mode )
            onNoCalib = handlers::APVCalcHitTimeRatios::kWarn;
        else if( "error" == mode )
            onNoCalib = handlers::APVCalcHitTimeRatios::kError;
        else if( "ignore" == mode )
            onNoCalib = handlers::APVCalcHitTimeRatios::kIgnore;
    }
    return new handlers::APVCalcHitTimeRatios( mgr
                                             , aux::retrieve_det_selection(cfg)
                                             , onNoCalib
                                             , aux::get_logging_cat(cfg)
                                             , "default"  // calibClass
                                             , "default"  // wire calib class
                                             , aux::get_naming_class(cfg)
                                             );
}

}  // namespace na64dp
