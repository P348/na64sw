#include "na64common/calib/sXMLLoader.hh"

#include <unordered_set>

namespace sdc {
sXMLCalibID
ValidityTraits<sXMLCalibID>::from_string(const std::string & stre) {
    sXMLCalibID r;
    size_t n = stre.find('/');
    std::string runNoStr = stre.substr(0, n);
    int iRunNo;
    try {
        iRunNo = std::stoul(runNoStr);
    } catch (std::invalid_argument & e) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Can't interpret run number in expression \"%s\": %s"
                , runNoStr.c_str(), e.what() );
        throw sXMLParsingError(errbf);
    } catch (std::out_of_range & e) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Run number \"%s\" overflows standard long integer type: %s"
                , runNoStr.c_str(), e.what() );
        throw sXMLParsingError(errbf);
    }
    if( static_cast<long>(iRunNo) < static_cast<long>(std::numeric_limits<decltype(sXMLCalibID::runNo)>::min())
     || static_cast<long>(iRunNo) > static_cast<long>(std::numeric_limits<decltype(sXMLCalibID::runNo)>::max())
      ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Run number \"%s\" is out of permitted range for run numbers"
                , runNoStr.c_str() );
        throw sXMLParsingError(errbf);
    }
    r.runNo = iRunNo;
    // TODO: further code should parse spill number after delimiter `/', but
    // it is not clear whether it is needed so far
    if(n != std::string::npos) {
        throw sXMLParsingError("Delimiter for spill number found, but"
                " current implementation does not support parsing it."
                );
    }
    r.spillNo = 0;
    return r;
}
}  // namespace sdc

// loader implem

static const std::regex
        // Regular expression to handle line of the form `<runs="123-456"`.
        //  permits for optional leading `<`, some spaces, etc. Two capture
        //  groups are for runs range.
        gRxValidityRange(R"~(^(?:<\s*)?runs?\s*=\s*"(\d+)-(\d+)")~", std::regex_constants::icase),
        // Metadata entry
        //  should be applied *only* if "run=..." pattern did not match. Permits
        //  for leading and trailing `<>` signs, capture groups extracts
        //  name/value pair.
        gRxMetadataItem(R"~(^(?:<)?(\w+)\s*=\s*"([^"]+)">?$)~"),
        // "Not important" line
        //  Matches line that considered as "not important" by S.Donskov. This
        //  lines are remnants mimicing actual XML grammar, yet with some
        //  illegal additions, like arbitrary spaces, forgotten `/` etc. We
        //  identify such a lines by presence of leading `<` and trailing `>`
        //  requiring only alphanumerical signs and spaces and optional leading
        //  `/`. MUST be considered after comments, and metadata entries.
        gRxNIMP(R"~(^<\/?[\w\s]+>$)~"),
        // Begin of comment marker pattern
        //  This pattern matches natural XML `<!--` start-of-comment markup,
        //  but permits arbitrary spaces between symbols and tolerate absent 
        //  excaimation sign and trailing `-` (so, for instance "<-" can
        //  start a comment too).
        gRxCommentBgn(R"~(<\s*!?\s*--?)~"),
        // End of comment marker pattern
        //  This pattern matches natural XML `-->` end-of-comment markup,
        //  but also permits for arbitrary spaces in between of symbol and one
        //  missing minus sign
        gRxCommentEnd(R"~(-?-\s*>)~"),
        // CSV delimiter (whitespace)
        gRxCSVDelimiter(R"~(\s+)~"),
        // CSV column token: permitted are usual word characters,
        // case-insensitive [A-Z0-9_], minus and dots to denote real numbers
        gRxCSVToken(R"~(^[a-z0-9_\-+.]+$)~", std::regex_constants::icase)
        ;

struct isXMLState {
    size_t lineNo;

    isXMLState() : lineNo(0) {}
    /// Called from default implementation of `consider_metadata()`
    /// if `"runs"="..."` met, with range parameters
    virtual void handle_validity_mdata(sXMLCalibID, sXMLCalibID) {}
    /// Called from default implementation of `consider_metadata()`
    /// if `"options"="..."` met, with options tring
    ///
    /// \todo Seem to not affect anything in the original code
    virtual void handle_option(const std::string &) {}

    /// Called when metadata item is met in the file
    virtual void consider_metadata(const std::string & key, const std::string & value) {
        if(!strcasecmp(key.c_str(), "runs")) {
            // this is runs="..." meta entry, use it
            size_t n = value.find(sdc::ValidityTraits<sXMLCalibID>::strRangeDelimiter);
            if(std::string::npos == n) {
                char errbf[128];
                snprintf( errbf, sizeof(errbf)
                        , "Validity range format error -- "
                        " delimiter `%c' not found (expected for runs range)"
                        , sdc::ValidityTraits<sXMLCalibID>::strRangeDelimiter
                        );
                throw sXMLParsingError(errbf);
            }
            handle_validity_mdata(sdc::ValidityTraits<sXMLCalibID>::from_string(value.substr(0, n))
                    , sdc::ValidityTraits<sXMLCalibID>::from_string(value.substr(n+1))
                    );
        } else if(!strcasecmp(key.c_str(), "options")) {
            auto optTokens_ = sdc::aux::tokenize(value);
            std::unordered_set<std::string> optTokens(optTokens_.begin(), optTokens_.end());
            for(const auto & strOpt: optTokens) {
                handle_option(strOpt);
            }
        } else return;  // ignore other metainfo while pre-parsing
    }
    /// Called with (tokenized) CSV item
    virtual void consider_CSV(const std::string &) {}
};

static std::list<sXMLLoader::DataBlock>
_process_sXML_file( const std::string & docID
                  , isXMLState & state
                  ) {
    std::ifstream ifs(docID);
    if(!ifs) {
        throw sXMLParsingError("Can't open file", docID.c_str(), 0);
    }
    // read file, line by line, use regex to identify file's content
    state.lineNo = 0;
    std::string line_;
    std::list<sXMLLoader::DataBlock> r;
    size_t inCommentFromLine = 0;
    while(std::getline(ifs, line_)) {
        ++state.lineNo;
        // apply various regular expressions to handle the line
        std::smatch sm;
        // - if we're in comment mode, look for end-of-comment marker
        if(inCommentFromLine) {
            if(std::regex_search(line_, sm, gRxCommentEnd)) {
                // strip part of the line belonging to lasting comment
                line_ = sm.suffix().str();
                inCommentFromLine = 0;  // reset "in-comment" mode
            } else {
                continue;  // otherwise, keep skipping lines
            }
        }

        std::string line = sdc::aux::trim(line_);
        if(line.empty()) continue;  // skip empty/blank lines
        // - look for start-of-comment marker
        while(std::regex_search(line, sm, gRxCommentBgn)) {
            // begin-of-comment found, look for end-of comment marker, or start
            // to skip the lines
            std::smatch sm2;
            std::string smSuffix = sm.suffix().str();
            if(std::regex_search(smSuffix, sm2, gRxCommentEnd)) {
                // end-of-comment at the same line -- re-compose the line
                // getting rid of comment
                line = sdc::aux::trim(sm.prefix().str() + sm2.suffix().str());
            } else {
                // end-of-comment not found
                try {
                    inCommentFromLine = state.lineNo;
                } catch( sXMLParsingError & e ) {
                    e.filePath = docID;
                    e.lineNo = state.lineNo;
                    throw e;
                }
                break;
            }
        }
        if(inCommentFromLine) continue;  // break, if we're in comment
        if(line.empty()) continue;  // skip empty/blank lines (after trimming comments)

        // - check for metadata
        if(std::regex_match(line, sm, gRxMetadataItem)) {
            try {
                state.consider_metadata(sm[1].str(), sm[2].str());
            } catch( sXMLParsingError & e ) {
                e.filePath = docID;
                e.lineNo = state.lineNo;
                throw e;
            }
            continue;
        }

        // - check if line is ASCII block
        bool isCSV = true;
        std::list<std::string> toks; {
            std::sregex_token_iterator iter(line.begin(), line.end()
                    , gRxCSVDelimiter, -1), end;
            for(; isCSV && iter != end; ++iter) {
                if(std::regex_match(iter->str(), gRxCSVToken)) {
                    toks.push_back(iter->str());
                    continue;
                }
                isCSV = false;
            }
        }
        if(isCSV) {
            // that's a CSV block, do CSV stuff here
            try {
                state.consider_CSV(line);
            } catch( sXMLParsingError & e ) {
                e.filePath = docID;
                e.lineNo = state.lineNo;
                throw e;
            }
            continue;
        }
        // check for "not important" and skip if matches
        if(std::regex_match(line, gRxNIMP)) continue;
        throw sXMLParsingError( "Unrecognized line type"
                              , docID.c_str(), state.lineNo
                              );
    }
    if(inCommentFromLine) {
        // warn the user that comment is not closed since line #
        std::cerr << "WARNING " << docID << ":" << state.lineNo << " unterminated comment."
            << std::endl;
    }
    return r;
}

/// internal class implementing preparsing state, extracts document markup in
/// terms of metadata and tabular data blocks
struct sXMLPreparserState : isXMLState {
private:
    bool inCSV;
    sXMLLoader::DataBlock _cBlock;
    const std::string _defaultDataType;
public:
    std::list<sXMLLoader::DataBlock> blocks;

    sXMLPreparserState(const std::string & defaultDataType)
            : inCSV(false)
            , _defaultDataType(defaultDataType)
            {
        _cBlock.blockBgn = 0;  // used to identify uninitialized block
    }

    void handle_option(const std::string & optStr) override {
        if(optStr == "data_format3") {
            _cBlock.dataType = "MSADCCalib";  // todo: use from traits?
        }
        // NOTE: ignore other options? There is "xy" at least, but it is not
        // clear what it supposed to mean.
    }

    void handle_validity_mdata(sXMLCalibID from, sXMLCalibID to) override {
        _cBlock.validityRange.from = from;
        _cBlock.validityRange.to = to;
    }

    void consider_metadata(const std::string & k, const std::string & v) override {
        inCSV = false;
        isXMLState::consider_metadata(k, v);
    }

    void consider_CSV(const std::string &) override {
        // if current state is "in CSV", return
        if(inCSV) return;
        // otherwise, finalize current markup block
        _cBlock.blockBgn = lineNo;  // mark block start from certain line
        if(_cBlock.dataType.empty()) {
            _cBlock.dataType = _defaultDataType;
            //throw sXMLParsingError("Unable to assume calibration data type"
            //        " for tabular data starting from here");
        }
        if(!( sdc::ValidityTraits<sXMLCalibID>::is_set(_cBlock.validityRange.from)
           && sdc::ValidityTraits<sXMLCalibID>::is_set(_cBlock.validityRange.from)
           )) {
            throw sXMLParsingError("Failed to assume validity range"
                    " for tabular data starting from here"
                    " (no \"runs\"=\"...\" beforehead?)");
        }
        blocks.push_back(_cBlock);
        // reset current block
        _cBlock.validityRange.from
            = _cBlock.validityRange.to
            = sdc::ValidityTraits<sXMLCalibID>::unset;
        _cBlock.dataType.clear();
        _cBlock.blockBgn = 0;
        // set flag ignoring immediate subsequent CSV lines
        inCSV = true;
    }
};  // sXMLPreparserState

/// Exploits sXMLPreparserState to read out markup
std::list<sXMLLoader::DataBlock>
sXMLLoader::get_doc_struct( const std::string & docID ) {
    sXMLPreparserState state(defaults.dataType);
    defaults.baseMD.set("@docID", docID);
    _process_sXML_file(docID, state);
    defaults.baseMD.drop("@docID");
    return state.blocks;
}

/// internal class implementing parsing state, extracts document markup in
/// terms of metadata and reads certain tabular (CSV) data block
struct sXMLParserState : isXMLState {
private:
    sdc::Documents<sXMLCalibID>::iLoader::ReaderCallback _cllb;
    size_t _acceptFrom;
    const std::string & _requestedCalibDataType;
    sXMLCalibID _requestedFor;
    //const sXMLLoader::DefaultColumns * _dftColumns;  // xxx?

    bool inCSV;
    sdc::ValidityRange<sXMLCalibID> _cValidityRange;
    sdc::aux::MetaInfo md;
public:
    std::list<sXMLLoader::DataBlock> blocks;

    sXMLParserState( sdc::Documents<sXMLCalibID>::iLoader::ReaderCallback cllb
                   , size_t acceptFrom
                   , const std::string & requestedCalibDataType
                   , sXMLCalibID requestedFor
                   /*, const sXMLLoader::DefaultColumns * dftColumns*/
                   ) : _cllb(cllb)
                     , _acceptFrom(acceptFrom)
                     , _requestedCalibDataType(requestedCalibDataType)
                     , _requestedFor(requestedFor)
                     /*, _dftColumns(dftColumns)*/
                     , inCSV(false)
                     {}

    void handle_option(const std::string & optStr) override {
        // it seems Donskov does not use actually his "options"...
        //if(optStr == "data_format3") {
        //    // ...?
        //}
        // NOTE: ignore other options? There is "xy" at least, but it is not
        // clear what it supposed to mean.
    }

    void handle_validity_mdata(sXMLCalibID from, sXMLCalibID to) override {
        _cValidityRange.from = from;
        _cValidityRange.to = to;
    }

    void consider_metadata(const std::string & k, const std::string & v) override {
        inCSV = false;
        isXMLState::consider_metadata(k, v);
        md.set(k, v, lineNo);
    }

    void consider_CSV(const std::string & line) override {
        // TODO: various checks for general validity of current line parsing
        // here...
        //assert((bool) cVal);
        //assert(!cType.empty());
        //if( cType != forType ) return true;  // other type
        //if( ValidityTraits<KeyT>::is_set(cVal.from)
        //  && forKey < cVal.from ) return true;  // not valid yet
        //if( ValidityTraits<KeyT>::is_set(cVal.to)
        //  && (cVal.to < forKey || cVal.to == forKey ) ) return true;  // not valid already
        char bf[32];
        snprintf(bf, sizeof(bf), "%zu", lineNo);
        md.set("@lineNo", bf);
        assert(md.get<size_t>("@lineNo") == lineNo);
        /*bool ret = */_cllb(md, lineNo, line);
        assert(md.get<size_t>("@lineNo") == lineNo);
        md.drop("@lineNo");
        //return ret;
    }
};  // sXMLPreparserState

static const std::regex
    // Regexp to obtain detector name from filename string (without dir path)
    //  Assumes detector name (or whatever string has to be used to identify
    //  default columns set) is provided as sequence of uppercase letters at
    //  the beginning of the filename. First character of not [A-Z] set will
    //  break this sequence and delimit the identifier.
    //  1st capture group should contain extracted detector name then.
    gRxDetNameInFile1(R"~(^([A-Z]+).*\.xml)~")
    ;

std::pair<const char *, std::string>
sXMLLoader::guess_default_columns_for_file(const char * filePath) const {
    assert(filePath);
    assert(*filePath != '\0');
    char * pathCpy = strdup(filePath)
       , * fileName = basename(pathCpy)
       ;
    assert(fileName);
    std::cmatch cm;
    if(!std::regex_match(fileName, cm, gRxDetNameInFile1)) {
        // does not match the pattern
        free(pathCpy);
        return std::pair<const char *, std::string>(NULL, "");
    }
    // look for default columns
    for(auto * c = _dftColumns; '\0' != *(c->detName); ++c) {
        if(strcmp(c->detName, cm[1].str().c_str())) continue;
        free(pathCpy);
        return std::pair<const char *, std::string>(c->columnsList, c->detName);
    }
    free(pathCpy);
    return std::pair<const char *, std::string>(NULL, "");
}

void
sXMLLoader::read_data( const std::string & docID
                     , sXMLCalibID k
                     , const std::string & forType
                     , sdc::IntradocMarkup_t acceptFrom
                     , sdc::Documents<sXMLCalibID>::iLoader::ReaderCallback cllb
                     ) {
    sXMLParserState state(cllb, acceptFrom, forType, k/*, _dftColumns*/);
    state.consider_metadata("@docID", docID);
    // set default "columns" metadata based on filename suffix
    auto p = guess_default_columns_for_file(docID.c_str());
    if(!p.second.empty()) {
        state.consider_metadata("fileBaseName", p.second);
    }

    const char * defaultColumns = p.first;
    if(defaultColumns) {
        state.consider_metadata("columns", defaultColumns);
    } else if(_logStream) {
        *_logStream << "Failed to gues default columns order for \""
            << docID << "\", rexpecting explicit \"columns\"=\"...\" item in file."
            << std::endl;
    }
    _process_sXML_file(docID, state);
}

