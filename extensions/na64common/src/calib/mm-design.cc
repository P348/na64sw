#include "na64common/calib/mm-design.hh"
#include "na64common/calib/sdc-integration.hh"  // for string routines

namespace na64dp {
namespace calib {

// This function loads MuMega wire layout from a file into data
// structure and dispatches it to whoever interested.
void convert_YAML_description_to_MuMegaLayout( const YAML::Node & plNode
                                             , calib::Dispatcher & dsp ) {
    for(const auto & node : plNode) {
        // get selection (`applyTo' node of calibration info)
        std::string selection = node["applyTo"] ? node["applyTo"].as<std::string>()
                                                : std::string("")
                                                ;
        // retrieve file ("source") field
        const std::string filename = util::expand_name(node["source"].as<std::string>());
        log4cpp::Category::getInstance("calibrations.yamlDoc").debug(
                "Reading MuMega design entry from \"%s\".", filename.c_str() );
        std::ifstream ifs(filename);
        if(!ifs)
            NA64DP_RUNTIME_ERROR( "Unable to open file \"%s\" to load wires"
                    " layout scheme (MuMega design).", filename.c_str() );
        // Read file into "layout"
        calib::MuMegaWiresDesign layout;
        std::string line;
        size_t nLine = 0;
        while(std::getline(ifs, line)) {
            ++nLine;
            // omit comments
            if( 0 == line.find('#') ) continue;
            // split line by comma & trim spaces
            auto toks = sdc::aux::tokenize(line, ',');
            if( toks.size() != 6 ) {
                NA64DP_RUNTIME_ERROR( "At %s:%zu wrong number of tokens in the"
                        " line to be considered as wire multiplication scheme."
                        " Expected 6, got %d."
                        , filename.c_str(), nLine
                        , (int) toks.size()
                        );
            }
            std::array<int, 5> entry;
            auto tokIt = toks.begin();
            ++tokIt;  // first one is always trivial
            for( int i = 0; i < 5; ++i, ++tokIt ) {
                entry[i] = sdc::aux::lexical_cast<int>(*tokIt);
            }
            layout.push_back(entry);
        }
        dsp.set( "default", calib::MuMegaWiresDesignEntry(selection, layout));
    }
}

}  // namespace ::na64dp::calib
}  // namespace na64dp

// Register calibration data type alias
REGISTER_CALIB_DATA_TYPE( na64dp::calib::MuMegaWiresDesignEntry
                        , MuMegaLayout  //< this must coincide with (#1)
                        , "default"
                        );
