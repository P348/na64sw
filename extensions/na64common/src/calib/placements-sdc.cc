#include "na64common/calib/placements-sdc.hh"

namespace sdc {

#define M_copy_to_c_char_array(src, dest, parName)                          \
    if(dest + sizeof(dest) <= strncpy(dest, src, sizeof(dest))) {           \
        auto errStr = na64dp::util::format("%s:%zu error, -- string parameter " \
                parName " is too long.",                                    \
                filename.c_str(), lineNo );                                 \
    }

na64dp::calib::Placement
CalibDataTraits<na64dp::calib::Placement>::parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr
                      ) {
    #ifndef NDEBUG
    const size_t lineNo_ = m.get<size_t>("@lineNo");
    assert(lineNo_ == lineNo);
    #endif
    const std::string filename = m.get<std::string>("@docID");
    // Physically keeps names for irregular mappings
    static std::unordered_set<std::string> irregularMappingNames;
    // create object and set everything to zero
    na64dp::calib::Placement obj;
    // Create and validate columns order
    auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
            //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
            .interpret(aux::tokenize(line), loadLogPtr);
    // ^^^ note: every CSV block must be prefixed by columns order,
    // according to Anton's specification

    // NA64sw uses cm, following GenFit2 tracking utility. Yet, it is more
    // common in mechanics to use mm. By default mm are assumed.
    float lengthUnits = ::na64dp::util::Units::get_units_conversion_factor(
                m.get<std::string>("units.length", "mm"), "length" )
        , angularUnits = ::na64dp::util::Units::get_units_conversion_factor(
                m.get<std::string>("units.angle", "deg"), "angle" )
        ;

    // Get columns
    obj.name = csv("name");

    obj.center[0] = csv("x", std::nan("0"))*lengthUnits;
    obj.center[1] = csv("y", std::nan("0"))*lengthUnits;
    obj.center[2] = csv("z", std::nan("0"))*lengthUnits;

    obj.size[0] = csv("sizeX", std::nan("0"))*lengthUnits;
    obj.size[1] = csv("sizeY", std::nan("0"))*lengthUnits;
    obj.size[2] = csv("sizeZ", std::nan("0"))*lengthUnits;

    {  // get rotation order
        const std::string rotationOrder = m.get<std::string>("rotationOrder", "default");
        if(rotationOrder == "default") {
            obj.rotationOrder = na64dp::util::gStdRotationOrder;
        } else {
            if( 0 != na64sw_rotation_order_from_str( rotationOrder.c_str()
                                                   , &obj.rotationOrder ) ) {
                NA64DP_RUNTIME_ERROR("Couldn't interpret \"%s\" as rotation"
                        " order.", rotationOrder.c_str() );
            }
        }
    }

    obj.rot[0] = csv("r1", 0)*angularUnits;
    obj.rot[1] = csv("r2", 0)*angularUnits;
    obj.rot[2] = csv("r3", 0)*angularUnits;

    obj.zone = csv("zone", 0);
    if(!obj.zone)
        obj.zone = m.get<int>("zone", 0);

    std::string entryType = m.get<std::string>("entryType", "detector/regular1DWiredPlane");
    std::string itemClass; {
        size_t n = entryType.find('/');
        if(std::string::npos == n) {
            NA64DP_RUNTIME_ERROR("%s:%zu -- failed to get item class"
                    " from entryType=\"%s\""
                    , filename.c_str(), lineNo, entryType.c_str() );
        }
        itemClass = entryType.substr(0, n);
        entryType = entryType.substr(n+1);
    }

    if("detector" == itemClass) {
        if(entryType == "regular1DWiredPlane") {
            obj.suppInfoType = na64dp::calib::Placement::kRegularWiredPlane;
            obj.suppInfo.regularPlane.nWires = csv("nWires", 0);
            obj.suppInfo.regularPlane.resolution = csv("resolut", std::nan("0"))*lengthUnits;
        } else if( entryType == "irregular1DWirePlane" ) {
            obj.suppInfoType = na64dp::calib::Placement::kIrregularWiredPlane;
            std::string mapping = csv("irregularMappingType");
            auto it = irregularMappingNames.insert(mapping).first;
            M_copy_to_c_char_array( it->c_str()
                                  , obj.suppInfo.irregularPlane.layoutName
                                  , "\"irregularMappingType\"" );
        } else if( entryType == "volumetricDetector" ) {
            obj.suppInfoType = na64dp::calib::Placement::kVolumetricDetector;
            obj.suppInfo.volumetricDetector.localUncertainties[0]
                = csv("resX", std::nan("0"))*lengthUnits;
            obj.suppInfo.volumetricDetector.localUncertainties[1]
                = csv("resY", std::nan("0"))*lengthUnits;
            obj.suppInfo.volumetricDetector.localUncertainties[2]
                = csv("resZ", std::nan("0"))*lengthUnits;
        } else {
            NA64DP_RUNTIME_ERROR("Unable to interpret detector placements entry of type"
                    " \"%s\", at %s:%zu", entryType.c_str()
                    , filename.c_str(), lineNo);
        }
    } else if("magnet" == itemClass) {
        // NOTE: `kFieldMap` may correspond to multiple C++ classes!
        //
        // List of strings in this if()-clause can be extended as we
        // add more magnetic field types. Still, classes can
        // share payload for the placements. We distinguish them by
        // `fieldClass` field of supp.info payload at the level of calibration
        // data subscribers.
        if( "fieldMap" == entryType
         || "TrackingTools_FieldMap" == entryType
         ) {
            obj.suppInfoType = na64dp::calib::Placement::kFieldMap;
            M_copy_to_c_char_array( m.get<std::string>("magFieldMap.mapURI").c_str()
                                  , obj.suppInfo.fieldMap.mapFilePath
                                  , "\"magFieldMap.mapURI\" (metadata)"
                                  );
            M_copy_to_c_char_array( m.get<std::string>("fieldMap.units.length").c_str()
                                  , obj.suppInfo.fieldMap.lengthUnits
                                  , "\"fieldMap.units.length\" (metadata)"
                                  );
            M_copy_to_c_char_array( m.get<std::string>("fieldMap.units.field").c_str()
                                  , obj.suppInfo.fieldMap.fieldUnits
                                  , "\"fieldMap.units.field\" (metadata)"
                                  );
            M_copy_to_c_char_array( csv("techLabel", std::string()).c_str()
                                  , obj.suppInfo.fieldMap.techLabel
                                  , "\"techLabel\""
                                  );
            M_copy_to_c_char_array( entryType.c_str()
                                  , obj.suppInfo.fieldMap.fieldClass
                                  , "m.field class at 'entryType=\"magnets/*\" metadata')"
                                  );
        } else {
            NA64DP_RUNTIME_ERROR("Unable to interpret magnet entry of type"
                    " \"%s\", at %s:%zu", entryType.c_str()
                    , filename.c_str(), lineNo);
        }
    } else {
        NA64DP_RUNTIME_ERROR("Unable to interpret placement class"
                    " \"%s\", at %s:%zu", itemClass.c_str()
                    , filename.c_str(), lineNo);
    }
    // ...

    obj.material = csv.operator()<std::string>("material", "");
    if(obj.material.empty())
        obj.material = m.get<std::string>("tracking.material", "");

    auto & L = log4cpp::Category::getInstance("calib.placements");
    if(L.getPriority() >= log4cpp::Priority::DEBUG) {
        L << log4cpp::Priority::DEBUG << filename << ":" << lineNo
            << "read entry \"" << obj.name << "\" of sizes "
              << obj.size[0] << "x" << obj.size[1] << "x" << obj.size[2]
              << ", rotated by " << obj.rot[0] << ", " << obj.rot[1] << ", "
              << obj.rot[2] << " (length fact is " << lengthUnits
              << ", ang fact is" << angularUnits << ")"
              ;
        // ^^^ todo: other debug info there?
    }

    return obj;
}

}  // namespace sdc
