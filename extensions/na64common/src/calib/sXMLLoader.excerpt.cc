#include "na64common/calib/sXMLLoader.hh"

/**\brief Common excerpt copied to S.Donskov apps
 *
 * This file contains definitions that can be of use for Sergei. Initially, it
 * drafts the definitions that were copied to his application(s), verbatim.
 * We assume then that he will change this code to fit his needs and we will
 * synchronize it periodically to maintain the compatibility with his
 * inventions.
 *
 * This scenario should free him from version control system/library
 * maintenance, etc. and seem to be least tedious trade-off.
 * */

//
// --- VERBATIM BEGIN

// Loaded calibration data structure
struct MSADCCalib {
    /// Detector name, without Z/station index (e.g. ECAL, VETO, HCAL, etc)
    char name[32];
    /// Needed to identify detectors group based on calibration files
    char fileBaseName[32];
    /// Standard (M)SADC calibration coefficient (peak position)
    float Cal;
    /// Time calibration coefficient (mean time peak position)
    float Time;
    /// Root mean square error for time
    float TimeRMS;
    /// LED amplitude peak position
    float LedCal;
    /// Detector ID indices (if negative -- unset)
    int x, y, z;
    // ... (other fields here, if needed)
};

namespace sdc {
// Traits struct, doubtly needs to be changed
template<>
struct CalibDataTraits<MSADCCalib> {
    static constexpr char typeName[] = "MSADCCalib";
    template<typename T=MSADCCalib>
            using Collection=std::list<T>;

    /// An action performed to put newly parsed data into collection
    template<typename T=MSADCCalib>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t lineNo
                              ) { col.push_back(e); }

    /// Parses tabular line
    static MSADCCalib
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      );
};

// Main function performing parsing of .XML tabular sections
MSADCCalib
CalibDataTraits<MSADCCalib>::parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & mi
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr
                      ) {
    // TODO: utilize loadLogPtr
    // Since we're using somewhat hardcoded semantics for columns in the CSV block,
    // defined mostly by detector type (VETO, ECAL, BEAMCOUNTERS, etc). We
    // did some precautions to gues default columns order at loder level,
    // but it is still can be possible that columns semantics is not provided.
    if(!mi.has("columns")) {
        throw sXMLParsingError("No information about columns provided for"
                " parsing columnar data. Please, provide explicit \"columns\"=\"...\""
                " attribute or append defaults in the C++ code for"
                " corresponding detector.");
    }
    // Instance to fill with data being read
    MSADCCalib r;
    // Use "columns" attribute to read out the data
    auto csv = mi.get<aux::ColumnsOrder>("columns")
            //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
            .interpret(aux::tokenize(line), loadLogPtr)
            ;
    // set attributes in destination object with values from appropriate
    // columns. Use value given as second argument if columns is not set
    r.Cal = csv("Cal", std::nan("0"));
    r.Time = csv("Time", std::nan("0"));
    r.TimeRMS = csv("TimeRms", std::nan("0"));
    r.x = csv("x", -1);
    r.y = csv("y", -1);
    r.z = csv("z", -1);
    r.LedCal = csv("LedCal", std::nan("0"));
    // ... (add here new columns, if needed)

    // "name" is somewhat tricky. It can be provided either per se ("name"
    // column exists) or as sub-token in "namez" or "namex" columns:
    {
        int * idxPtr = nullptr;  // ptr to index attribute to write in case of "namez" or "namex"
        auto itName  = csv.find("name");
        if(itName != csv.end()) {
            r.x = r.y = r.z = -1;
            strcpy(r.name, itName->second.c_str());
        } else if( csv.end() != (itName = csv.find("namez")) ) {
            idxPtr = &r.z;
        } else if( csv.end() != (itName = csv.find("namex")) ) {
            idxPtr = &r.x;
        } else {
            throw sXMLParsingError("Columnar data has no name[xz] column");
        }
        assert(itName != csv.end());
        if(idxPtr) {  // then its namex/namez, split it into name + index
            const char * nameStr = itName->second.c_str()
                     , * c;
            char * destC;
            for(c = nameStr, destC = r.name; *c != '\0'; ++c, ++destC) {
                if(isdigit(*c)) break;
                assert(destC - r.name < static_cast<long int>(sizeof(r.name))-1);  // name array attr of insufficient length
                *destC = *c;
            }
            *destC = '\0';
            if(*c == '\0') {
                #if 0  // seem to be the case, practically...
                char errBf[128];
                snprintf( errBf, sizeof(errBf)
                        , "At token \"%s\": provided name without"
                          " integer index, but expected format \"%s\" assumes"
                          " trailing integer index.", nameStr
                        , itName->first.c_str() );
                throw sXMLParsingError(errBf);
                #endif
            } else {
                char * endPtr = NULL;
                auto idx = strtol(c, &endPtr, 10);
                if(*endPtr != '\0') {
                    char errBf[128];
                    snprintf( errBf, sizeof(errBf)
                            , "At token \"%s\": failed to convert index part of"
                              " name expression (extra trailing character after"
                              " digits)", nameStr );
                    throw sXMLParsingError(errBf);
                }
                // todo: check for overflow?
                *idxPtr = idx;
            }
        }
    }
    // "basename"
    assert(mi.has("fileBaseName"));
    const std::string fileBaseName = mi.get<std::string>("fileBaseName", "");
    assert(!fileBaseName.empty());
    strncpy(r.fileBaseName, fileBaseName.c_str(), sizeof(r.fileBaseName));
    return r;
}

}  // namespace sdc

// Loads sequence of calibration objects identified by run number
sdc::CalibDataTraits<sdc::SrcInfo<MSADCCalib>>::template Collection<sdc::SrcInfo<MSADCCalib>>
load_msadc_calibs_xml(
              const std::string & rootpath  // path to calib files dir
            , sXMLCalibID k  // run+spill numbers to look for
            , const std::string & acceptPatterns="*.xml:*.XML"  // accepted filenames patterns
            , const std::string & rejectPatterns="*.swp:*.swo:*.bak:*.BAK:*.bck:~*:*_2023.xml"  // rejected filenames patterns
            , size_t upSizeLimitBytes=10*1024*1024  // consider only size up to 10kb
            , std::ostream * logStreamPtr=nullptr  // pointer to log stream if needed
            ) {
    // 1. SETUP
    // create calibration document index by run number
    sdc::Documents<sXMLCalibID> docs;
    // Auxiliary dictionary for default columns order, by detector name. These
    //  columns are used if "columns"="..." metadata item is not specified for
    //  tabular block being read. Note, that only following columns are supported:
    //      - "name" -- literal name of the detector (ECAL, HCAL, VETO)
    //      - "namez" -- name + z-index of the detector (ECAL0, HCAL2)
    //      - "namex" -- name + x-index of the detector (SRD1, VETO1, etc)
    //      - "Cal" -- amplitude calib.coeff. (mean peak position)
    //      - "Time" -- timing calib.coeff. (mean time peak position)
    //      - "TimeRms" -- root mean square error of timing calib.coeff.
    //      - "LedCal" -- (reference) mean LED peak position
    const sXMLLoader::DefaultColumns gDefaultColumnsByFileName[] = {
        { "BEAMCOUNTERS",   "namex,Cal,Time,TimeRms"              },
        { "ECAL",           "namez,x,y,Cal,Time,TimeRms,LedCal"   },
        { "HCAL",           "namez,x,y,Cal,Time,TimeRms,LedCal"   },
        { "WCAL",           "namex,Cal,Time,TimeRms,LedCal"       },
        { "SRD",            "namex,Cal,Time,TimeRms"              },
        { "VETO",           "namex,Cal,Time,TimeRms"              },
        { "VHCAL",          "namez,x,y,Cal,Time,TimeRms,LedCal"   },
        { "TRIGGERS",       "name,Time,TimeRms"                   },
        // ...(add here more detectors, if needed)
        // this files are questionable, seem not to be maintained anymore
        { "VTEC",           "name,Cal,Time,TimeRms"               },
        { "SRDT",           "namex,Cal,Time,TimeRms"              },
        { "" },  // sentinel, delimits end of the array, required
    };
    // Create a loader object and add it to the index for automated binding.
    // sXMLLoader is for S.Donskov's .xml files, Initialized with default
    // columns by detector index and logging stream
    auto sXMLLoaderInstance
        = std::make_shared< sXMLLoader >(gDefaultColumnsByFileName, logStreamPtr);
    docs.loaders.push_back(sXMLLoaderInstance);
    // Set default data type to be `MSADCCalib'
    sXMLLoaderInstance->defaults.dataType
        = sdc::CalibDataTraits<MSADCCalib>::typeName;

    // create filesystem iterator to walk through all the dirs and their
    // subdirs looking for files matching certain wildcards and size criteria
    sdc::aux::FS fs( rootpath
                   , acceptPatterns // (opt) accept patterns
                   , rejectPatterns // (opt) reject patterns
                   , 10  // (opt) min file size, bytes
                   , upSizeLimitBytes  // (opt) max file size, bytes
                   );
    if(logStreamPtr) {
        fs.set_logstream(logStreamPtr);
    }
    // use this iterator to fill documents index by recursively traversing FS
    // subtree and pre-parsing all matching files
    size_t nDocsOnIndex = docs.add_from(fs);
    if(logStreamPtr) {
        *logStreamPtr << "Indexed " << nDocsOnIndex << " document(s) at "
            << rootpath
            << " (accept=\"" << acceptPatterns << "\", reject=\""
            << rejectPatterns << ", size=(10-" << upSizeLimitBytes << ")."
            << std::endl;
    }

    // 2. LOAD DATA FOR CERTAIN RUN ID
    // Returned `list' consists of objects with their source information (file
    // and line number where item was read). This information is useful, but
    // to simplify usage we convert it to vector of structs here.
    return docs.template load< sdc::SrcInfo<MSADCCalib> >(k);
}

// --- VERBATIM END
//

#if defined(STANDALONE_BUILD) && STANDALONE_BUILD
int
main(int argc, char * argv[]) {
    auto calibData
        = load_msadc_calibs_xml(argv[1], {8463, 0});

    for(const auto & item_ : calibData) {
        const auto & item = item_.data;
        std::cout << item.name
                  << item.x << "x" << item.y << "x" << item.z << " cal="
                  << item.cal << ", time=" << item.time << ", rms=" << item.timeRMS
                  << ", led=" << item.LedCal
                  << ", from " << item_.srcDocID << ":" << item_.lineNo
                  << std::endl;  // XXX
    }
}
#endif
