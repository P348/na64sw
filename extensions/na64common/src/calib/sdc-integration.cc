#include "na64common/calib/sdc-integration.hh"
#include "na64calib/index.hh"
#include "na64util/demangle.hh"
#include "na64util/str-fmt.hh"
#include "na64util/uri.hh"
#include "na64calib/manager.hh"
#include "na64common/calib/sdc-extensions.hh"
#include "na64common/calib/GEMMonitorFilenameSemantics.hh"
#include "na64common/calib/placements-sdc.hh"  // "placements" calib data type

#include "na64common/calib/msadc-PedLED.hh"  // for loader, TODO: to extension

#include "na64detID/TBName.hh"
#include "na64detID/cellID.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace calib {
namespace aux {
SDCLogstream * SDCLogstream::_self = nullptr;

SDCLogstream &
SDCLogstream::self() {
    if(!_self) _self = new SDCLogstream(log4cpp::Category::getInstance(NA64SW_SDC_LOG_CATEGORY));
    return *_self;
}
}  // namespace ::na64dp::calib::aux

// Aux internal function to set loader's defaults from YAML node
template<typename T> static void
_configure_loader_defaults( std::shared_ptr<sdc::ExtCSVLoader<T>> p, const YAML::Node & cfg ) {
    p->grammar.commentChar      = cfg["commentChar"].as<std::string>()[0];
    p->grammar.metadataMarker   = cfg["metadataMarker"].as<std::string>()[0];
    p->grammar.metadataKeyTag   = cfg["metadataKeyTag"].as<std::string>();
    p->grammar.metadataTypeTag  = cfg["metadataTypeTag"].as<std::string>();
    if( cfg["validityRange"] ) {
        p->defaults.validityRange
            = sdc::aux::lexical_cast<sdc::ValidityRange<T>>(
                    cfg["validityRange"].as<std::string>());
    } else {
        p->defaults.validityRange = { T(sdc::ValidityTraits<T>::unset)
                                    , T(sdc::ValidityTraits<T>::unset)
                                    };
    }
    p->defaults.dataType = cfg["dataType"].as<std::string>();
}

template<typename T> size_t
SDCWrapper::_collect_docs( sdc::Documents<T> & docs
                         , const std::string & paths_
                         , const std::string & acceptPatterns
                         , const std::string & rejectPatterns
                         , size_t minSize, size_t maxSize ) {
    auto & L = log4cpp::Category::getInstance( NA64SW_SDC_LOG_CATEGORY );
    auto pathsList = na64dp::util::expand_names(paths_);
    if( pathsList.empty()
     || (  pathsList.size() == 1
        &&( '\0' == pathsList[0][0]
          ||'\0' == pathsList[0][1])
        )
     ) {
        L.warn( "Bad/empty path(s) expanded for SDC documents by key `%s'"
                " from: \"%s\". Failed to construct SDC calibration data index."
              , ::na64dp::util::demangle_cpp(typeid(T).name()).get()
              , paths_.c_str()
              );
        return 0;
    }
    // split paths parameter to list of strings, expanding envvars
    std::string paths; {
        std::ostringstream oss;
        bool isFirst = true;
        for( auto tok : pathsList ) {
            if( tok.empty() ) continue;
            if( isFirst ) isFirst = false; else oss << ":";
            oss << tok;
        }
        paths = oss.str();
    }
    L.debug( "Expaned paths for SDC documents lookup: \"%s\" -> \"%s\""
           , paths_.c_str(), paths.c_str()
           );
    // use every item from paths list to perform (recursive) lookup for the
    // (M)SADC docs
    size_t nDocs;
    {
        std::ostringstream oss;
        auto lo = P348_RECO_NAMESPACE::KeyTraits<T>::instantiate_lookup_object(
                        paths, acceptPatterns, rejectPatterns, minSize, maxSize );
        if(_debug) {
            auto ls = lo->set_logstream(&oss);
            assert(ls);
        }
        nDocs = docs.add_from(*lo);
        std::string logCpy(oss.str());
        if(_debug) {
            L.debug( "Lookup log for key `%s':\n%s(end of lookup log dump)"
                   , ::na64dp::util::demangle_cpp(typeid(T).name()).get()
                   , logCpy.c_str()
                   );
        }
    }
    // warn user if no documents found; may indicate faulty configuration
    if(nDocs) {
        L.debug("%zu calibration data docs indexed by key type `%s'."
               , nDocs
               , ::na64dp::util::demangle_cpp(typeid(T).name()).get()
               );
    } else {
        L.warn("No calibration data documents discovered by key `%s'."
              , ::na64dp::util::demangle_cpp(typeid(T).name()).get()
              );
        return 0;
    }
    return nDocs;
}

SDCWrapper::SDCWrapper( const YAML::Node & sdcConfig
                      , const p348reco::Aliases & periodAliases
                      )
        : _log(log4cpp::Category::getInstance( NA64SW_SDC_LOG_CATEGORY ))
        , _extCSVLoaderByDatetime(std::make_shared<sdc::ExtCSVLoader<P348_RECO_NAMESPACE::Datetime_t>>())
        //, _extCSVLoaderByRunNo(   std::make_shared<sdc::ExtCSVLoader<p348reco::RunNo_t>>())
        , _extCSVLoaderByEventID(std::make_shared<sdc::ExtCSVLoader<EventID>>())
        , _debug(sdcConfig["debug"] ? sdcConfig["debug"].as<bool>() : false)
        {
    { // configure loaders
        if(sdcConfig["byDatetime"])
            _configure_loader_defaults( _extCSVLoaderByDatetime
                                      , sdcConfig["byDatetime"]["defaults"] );
        //if(sdcConfig["byRunNo"])
        //    _configure_loader_defaults( _extCSVLoaderByRunNo
        //                              , sdcConfig["byRunNo"]["defaults"] );
        if(sdcConfig["byEventID"])
            _configure_loader_defaults( _extCSVLoaderByEventID
                                      , sdcConfig["byEventID"]["defaults"] );
        // register loaders in `sdc::Documents`
        // TODO: must be provided by application extension anticipated for SDC
        if( sdcConfig["pedestalAndLEDsPattern"] ) {  // enabled by default
            auto pedLEDLoader
                = std::make_shared<P348_RECO_NAMESPACE::MSADCCalibPedLedType1Loader>(
                            sdcConfig["pedestalAndLEDsPattern"].as<std::string>()
                        );
            sdc::Documents<EventID>::loaders.push_back(pedLEDLoader);
        }
        if(sdcConfig["byDatetime"])
            sdc::Documents<p348reco::Datetime_t>::loaders.push_back(_extCSVLoaderByDatetime);
        if(sdcConfig["byEventID"])
            sdc::Documents<EventID>::loaders.push_back(_extCSVLoaderByEventID);
        // ...
    }
    // Collect documents using appropriate lookup object
    if(sdcConfig["byDatetime"])
        _collect_docs<p348reco::Datetime_t>( *this
                     , sdcConfig["byDatetime"]["paths"].as<std::string>()
                     , sdcConfig["byDatetime"]["accept"].as<std::string>()
                     , sdcConfig["byDatetime"]["reject"].as<std::string>()
                     , sdcConfig["byDatetime"]["sizeLimits"][0].as<size_t>()
                     , sdcConfig["byDatetime"]["sizeLimits"][1].as<size_t>()
                     );
    if(sdcConfig["byEventID"])
        _collect_docs<EventID>( *this
                     , sdcConfig["byEventID"]["paths"].as<std::string>()
                     , sdcConfig["byEventID"]["accept"].as<std::string>()
                     , sdcConfig["byEventID"]["reject"].as<std::string>()
                     , sdcConfig["byEventID"]["sizeLimits"][0].as<size_t>()
                     , sdcConfig["byEventID"]["sizeLimits"][1].as<size_t>()
                     );
    // add this unconditionally to inject type aliases and prevent "unknown
    // dependee type alias" exception.
    enable_sdc_type_conversion<EventID,    SADCCalib>();
    enable_sdc_type_conversion<EventID,    na64dp::calib::Placement>();
    //enable_sdc_type_conversion<p348reco::Datetime_t, APVChannelEntry>();
    //enable_sdc_type_conversion<p348reco::Datetime_t, APVTimingCalib>();
    // ...
}

template<typename KeyT> size_t
SDCWrapper::_translate_updates( const KeyT & old
                              , const KeyT & new_
                              , Updates & upds ) {
    // Most of the updates are loaded incrementally; yet 
    // return list of pairs: (key, DocumentEntry*)
    size_t n = 0;
    // For every type in SDC index
    for( const auto & p : sdc::Documents<KeyT>::validityIndex.entries() ) {
        // Get SDC updates for this type
        auto sdcUpdates = sdc::Documents<KeyT>::validityIndex.updates(
                p.first, old, new_ );
        // Transform SDC update into na64sw update
        if(sdcUpdates.empty()) continue;
        for( auto & sdcUpdate : sdcUpdates ) {
            // Check that this SDC type is known to NA64SW (it is
            // aliased)
            if( CIDataAliases::self().type_id_by_name().end()
                    == CIDataAliases::self().type_id_by_name().find(p.first) ) {
                _log.debug("Omitting update of SDC type %s since it is not"
                        " aliased in NA64sw."
                        , p.first.c_str() );
                // we consider this situation as kinda normal even though we
                // do not know whether there are subscribers waiting of this
                // data because without matching str-aliases and NA64SW data
                // type it is not possible to resolve this eager.
                continue;
            }
            auto updPtr = new SDCUpdateWrapper<KeyT>(sdcUpdate, this, p.first, new_); // TODO: delete!
            upds.push_back(updPtr);
            _toCleanup.push_back(updPtr);
            ++n;
        }
    }
    return n;
}

void
SDCWrapper::clear_tmp_update_objects() {
    for(auto tPtr : _toCleanup) {
        delete tPtr;
    }
    _toCleanup.clear();
}

void
SDCWrapper::append_updates( EventID oldEventID, EventID newEventID
                          , const std::pair<time_t, uint32_t> & oldKey
                          , const std::pair<time_t, uint32_t> & newKey
                          , Updates & upds ) {
    size_t nCollected = 0;
    if( oldEventID != newEventID ) {
        nCollected = _translate_updates<EventID>(
                            oldEventID
                          , newEventID
                          , upds );
        msg_debug( _log, "Collected %zu SDC updates for run number #%d->%d."
                  , nCollected, oldEventID.run_no(), newEventID.run_no() );
    }
    if( oldKey.first != newKey.first ) {
        nCollected = _translate_updates<p348reco::Datetime_t>(
                            p348reco::Datetime_t{oldKey.first}
                          , p348reco::Datetime_t{newKey.first}
                          , upds );
        msg_debug( _log, "Collected %zu SDC updates for time %s -> %s."
                  , nCollected
                  , sdc::ValidityTraits<p348reco::Datetime_t>::to_string(
                        p348reco::Datetime_t{oldKey.first}).c_str()
                  , sdc::ValidityTraits<p348reco::Datetime_t>::to_string(
                        p348reco::Datetime_t{newKey.first}).c_str()
                  );
    }
}

void
SDCWrapper::load( const iUpdate & upd
                , Dispatcher & dsp
                ) {
    const SDCUpdateWrapper<P348_RECO_NAMESPACE::Datetime_t> * datetimeUpdatePtr
            = dynamic_cast<const SDCUpdateWrapper<P348_RECO_NAMESPACE::Datetime_t>*>(&upd);
    const SDCUpdateWrapper<EventID> * runNoUpdatePtr
            = nullptr;

    if(datetimeUpdatePtr) {
        auto it = _conversions_Datetime.find(upd.subject_data_type());
        if( it == _conversions_Datetime.end() ) {
            NA64DP_RUNTIME_ERROR("Unsupported calibration data type requested"
                    " from SDC data loader: update subject data type is \"%s\","
                    " but no conversion of this name defined in SDC wrapper class."
                    , ::na64dp::util::calib_id_to_str(upd.subject_data_type()).c_str()
                    );  // TODO: details
        }
        (this->*it->second)(upd, dsp, datetimeUpdatePtr->forKey);
    } else if( !!(runNoUpdatePtr
            = dynamic_cast<const SDCUpdateWrapper<EventID>*>(&upd)) ) {
        auto it = _conversions_EventID.find(upd.subject_data_type());
        if( it == _conversions_EventID.end() ) {
            NA64DP_RUNTIME_ERROR("Unsupported calibration data type requested"
                    " from SDC data loader: update subject data type is \"%s\","
                    " but no conversion of this name defined in SDC wrapper class."
                    , ::na64dp::util::calib_id_to_str(upd.subject_data_type()).c_str()
                    );  // TODO: details
        }
        (this->*it->second)(upd, dsp, runNoUpdatePtr->forKey);
    } else {
        assert(false);
        //NA64DP_RUNTIME_ERROR("Unrecognized update type: %s"
        //        , util::demangle_cpp(typeof(upf).name));
    }
}

void
SDCWrapper::to_yaml(YAML::Node & node) const {
    char bf[64];
    node["_type"] = "SDCWrapper";
    snprintf(bf, sizeof(bf), "%p", this);
    node["_ptr"] = bf;
    /* Dump SDC Documnts object to JSON and use yaml-cpp to parse into YAML
     * nodes since JSON is compatible to YAML */
    {
        std::stringstream jsStr;
        sdc::Documents<EventID>::dump_to_json(jsStr);
        node["byEventID"] = YAML::Load(jsStr);
    }
    {
        std::stringstream jsStr;
        sdc::Documents<p348reco::Datetime_t>::dump_to_json(jsStr);
        node["byDatetime"] = YAML::Load(jsStr);
    }
}

void
SDCWrapper::to_yaml_as_loader( YAML::Node & node) const {
    char bf[64];
    node["_type"] = "SDCWrapper";
    snprintf(bf, sizeof(bf), "%p", this);
    node["_ptr"] = bf;
}

}  // namespace ::na64dp::calib

namespace nameutils {

DetID
sdc_id( const nameutils::DetectorNaming & nm
      , const std::string & nameZ
      , uint16_t xIdx, uint16_t yIdx, uint16_t zIdx
      , const std::string & docID
      , size_t lineNo
      ) {
    // convert `namez' to detector ID using usual name TBname conversion
    DetID did = nm[nameZ];
    if( did.chip() == nm.chip_id("SADC") ) {
        // append payload with indices
        CellID cid;  //(xIdx, yIdx, zIdx);
        cid.set_x(xIdx);
        cid.set_y(yIdx);
        // This is a kind of kludge preventing the unset number in payload.
        // It was not in original specification, but it seems that this
        // semantic is used and 0 is implied...
        if( !did.is_number_set() ) {
            did.number(0);
        }

        if( nm.kin_id("ECAL").second == did.kin()
         && nm.kin_id("ECAL").first == did.chip()
          ) {
            did.number(0);  // always
            cid.set_z(zIdx);
        } else if( did.is_number_set() && (did.number() != zIdx) ) {
            // Development warning: some detectors probably will still require
            // z-index (e.g. hodoscopes, ECAL, etc). This warning is to notify
            // developer on this inconsistency. If you see it, one must append
            // if-conditions above to correctly resolve this situation.
            if(docID.empty()) {
                log4cpp::Category::getInstance(NA64SW_SDC_LOG_CATEGORY)
                    .warn( "Naming inconsistency: SDC name-z conversion \"%s\" has"
                           " incompatible z-index %d."
                           , nameZ.c_str(), (int) zIdx );
            } else {
                log4cpp::Category::getInstance(NA64SW_SDC_LOG_CATEGORY)
                    .warn( "%s:%zu: naming inconsistency: SDC name-z"
                           " conversion \"%s\" has incompatible z-index %d."
                         , docID.c_str(), lineNo
                         , nameZ.c_str(), (int) zIdx );
            }
        }
        // set payload
        did.payload(cid.cellID);
    } else if( did.chip() == nm.chip_id("APV") ) {
        // no special treatment is required here as APV detectors are
        // identified by their DAQ src ID anyway.
    } else {
        NA64DP_RUNTIME_ERROR("Can't convert SDC detector name \"%s\" to"
                " `na64dp::DetID' -- conversion for chip \"%s\" is not"
                " defined."
                , nameZ.c_str()
                , nm.chip_features(did.chip()).name.c_str()
                );
    }

    return did;
}

}  // namespace ::na64dp::nameutils
}  // namespace na64dp

