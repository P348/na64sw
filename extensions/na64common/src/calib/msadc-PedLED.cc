#include "na64common/calib/msadc-PedLED.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64util/na64/event-id.hh"
#include "na64event/reset-values.hh"
#include "na64util/str-fmt.hh"
#include <limits>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <regex>

static const std::regex gRxHeaderValuableTokens(R"~(([A-Za-z]+)\:([0-9]+))~");

namespace sdc {

P348_RECO_NAMESPACE::MSADCCalibPedLedType1
CalibDataTraits<P348_RECO_NAMESPACE::MSADCCalibPedLedType1>::parse_line(
              const std::string & line
            , size_t lineNo
            , const aux::MetaInfo & m
            , const std::string & srcID
            , sdc::aux::LoadLog * loadLogPtr
            ) {
    P348_RECO_NAMESPACE::MSADCCalibPedLedType1 r;
    #if 1  // parse layout, TODO: cache it in MD.
    std::vector<std::pair<std::string, size_t>> layout;
    std::string headerLine = m.get<std::string>("header");
    // We extract header tokens tokens from beginning of the file
    // assuming it can only appear as meaningful info (markup)
    std::string::const_iterator searchStart( headerLine.cbegin() );
    std::smatch smatch;
    while(std::regex_search(searchStart, headerLine.cend(), smatch, gRxHeaderValuableTokens)) {
        std::vector<std::string> subTokens(smatch.begin(), smatch.end());
        //std::cout << " - \"" << smatch.str()
        //          << "\" (" << subTokens.size() << "): \""
        //          << subTokens[1] << "\", \""
        //          << subTokens[2] << "\""
        //          << std::endl;  // XXX
        r.layout.push_back(std::pair<std::string, int>(subTokens[1],
                    std::stoi(subTokens[2])));
        searchStart = smatch.suffix().first;
    }
    #endif

    auto toks = na64dp::util::tokenize_quoted_expression(line);
    for(const auto & tok : toks) {
        r.values.push_back(std::stof(tok));
    }

    #warning "FIXME: use loadLogPtr for Donskov's pedled calib type (to provide extra debug info)"

    r.nEventsOff = m.get<size_t>("statOff");
    r.nEventsOn  = m.get<size_t>("statOn");

    return r;
}

}  // namespace sdc

namespace P348_RECO_NAMESPACE {

bool
MSADCCalibPedLedType1CollectionBase::_assure_layout(const MSADCCalibPedLedType1 & entries) {
    if(_layout.empty()) {
        // set layout
        _layout = entries.layout;
        return true;
    } else {
        bool matches = true;
        auto it = _layout.begin();
        for(const auto & p: entries.layout) {
            if( *it != p) {
                matches = false;
            }
            ++it;
        }
        return matches;
    }
}

void
MSADCCalibPedLedType1CollectionBase::_add_values(
        const MSADCCalibPedLedType1 & calibs) {
    if(!_assure_layout(calibs)) {  // make sure layout did not changed between lines
        NA64DP_RUNTIME_ERROR("Layout mismatch.");
    }
    assert(!_layout.empty());
    assert(calibs.nEventsOn || calibs.nEventsOff);  // at least some stats was collected
    // read values
    std::vector<float> * destPtr;
    #if 1
    switch(_nLine) {
        case 0:
            if(calibs.nEventsOff) {
                // pedestals, first line
                destPtr = &pedestals;
                assert(destPtr->empty());
            } else {
                // on-spill LEDs, single line
                destPtr = &inSpillLEDs;
                assert(destPtr->empty());
            }
            break;
        case 1:
            if(calibs.nEventsOff) {
                // (still) pedestals, 2nd line
                destPtr = &pedestals;
                assert(!destPtr->empty());
            } else {
                // if nEventsOff == 0, there must be no 2nd line or above
                NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                        " #events =%zu, on-spill #events =%zu"
                    , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
            }
            break;
        case 2:
            if(calibs.nEventsOff) {
                // 3rd line with off-spill stats enabled should always be
                // off-spill LED
                destPtr = &offSpillLEDs;
                assert(destPtr->empty());
            } else {
                NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                        " #events =%zu, on-spill #events =%zu"
                    , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
            }
            break;
        case 3:
            if(calibs.nEventsOff && calibs.nEventsOn) {
                destPtr = &inSpillLEDs;
                assert(destPtr->empty());
            } else {
                NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                        " #events =%zu, on-spill #events =%zu"
                    , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
            }
            break;
        default:
            NA64DP_RUNTIME_ERROR("Line #%d within spill section has undefined"
                    " semantics in PedLED format (off-spill events #%zu,"
                    " on-spill #%zu)", _nLine, calibs.nEventsOff, calibs.nEventsOn );
    };
    destPtr->insert(destPtr->end(), calibs.values.begin(), calibs.values.end());
    #else
    for(const auto & layoutPair : _layout ) {
        size_t cOffset = 0;
        std::pair<decltype(pedestals)::iterator, bool> ir;
        // lines format is somewhat tricky one. The thing is the pedestals
        // are written always in two lines, but in paired format (every couple
        // of numbers corresponds to even/odd pair), while rest of the lines
        // are per kind. So, following situations are possible regarding
        // nEventsOn/nEventsOff numbers:
        // 1) nEventsOff != 0 && nEventsOn == 0 -- 3 lines (2peds + 1 off-LED)
        // 2) nEventsOff != 0 && nEventsOn != 0 -- 4 lines (2peds + 1 off-LED + 1 on-LED)
        // 3) nEventsOff == 0 && nEventsOn != 0 -- 1 line (1 on-LED)
        if( 0 == calibs.nEventsOff && 0 == calibs.nEventsOn ) {
            NA64DP_RUNTIME_ERROR("Empty pedestals/LEDs data.");
        }
        //bool append = false;  // todo: should assure we're writing vs from scratch
        switch(_nLine) {
            case 0:
                if(calibs.nEventsOff) {
                    // pedestals, first line
                    ir = pedestals.emplace(layoutPair.first, std::vector<float>());
                    assert(ir.second);
                } else {
                    // on-spill LEDs, single line
                    assert(calibs.nEventsOn);
                    ir = inSpillLEDs.emplace(layoutPair.first, std::vector<float>());
                    assert(ir.second);
                }
                break;
            case 1:
                if(calibs.nEventsOff) {
                    // (still) pedestals, 2nd line
                    assert(calibs.nEventsOff);
                    ir = pedestals.emplace(layoutPair.first, std::vector<float>());
                    assert(!ir.second);  // should append
                    //append = true;
                } else {
                    // if nEventsOff == 0, there must be no 2nd line or above
                    NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                            " #events =%zu, on-spill #events =%zu"
                        , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
                }
                break;
            case 2:
                if(calibs.nEventsOff) {
                    // 3rd line with off-spill stats enabled should always be
                    // off-spill LED
                    ir = offSpillLEDs.emplace(layoutPair.first, std::vector<float>());
                    assert(ir.second);
                } else {
                    NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                            " #events =%zu, on-spill #events =%zu"
                        , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
                }
                break;
            case 3:
                if(calibs.nEventsOff && calibs.nEventsOn) {
                    ir = inSpillLEDs.emplace(layoutPair.first, std::vector<float>());
                    assert(ir.second);
                } else {
                    NA64DP_RUNTIME_ERROR("Line %d is given, though off-spill"
                            " #events =%zu, on-spill #events =%zu"
                        , _nLine+1, calibs.nEventsOff, calibs.nEventsOn );
                }
                break;
            default:
                NA64DP_RUNTIME_ERROR("Line #%d within spill section has undefined"
                        " semantics in PedLED format (off-spill events #%zu,"
                        " on-spill #%zu)", _nLine, calibs.nEventsOff, calibs.nEventsOn );
        };
        for(int n = 0; n < layoutPair.second; ++n) {
            ir.first->second.push_back(calibs.values[n + cOffset]);
            //std::cout << "  <" << layoutPair.first << "> [" << n << "]: "
            //    << calibs.values[n + cOffset] << std::endl;  // XXX
        }
        cOffset += layoutPair.second;
    }
    #endif
    ++_nLine;
}

void
MSADCCalibPedLedType1CollectionBase::_copy_values_to_item(
          MSADCCalibPedLedType1CollectionBase::Item & item
        , size_t offset, const std::string & nm
        ) const {
    #if 1
    bool matched = false;
    size_t srcOffset = 0;
    for(const auto & li : _layout) {
        if(li.first == nm) { matched = true; break; }
        srcOffset += li.second;
    }
    if(!matched) {
        NA64DP_RUNTIME_ERROR("No \"%s\" defined in the layout", nm.c_str());
    }
    if(!pedestals.empty()) {
        assert(2*(srcOffset + offset) + 1 < pedestals.size());
        item.pedestals[0] = pedestals[2*(srcOffset + offset)    ];
        item.pedestals[1] = pedestals[2*(srcOffset + offset) + 1];
    } else {
        item.pedestals[0] = item.pedestals[1] = std::nan("0");
    }
    if(!offSpillLEDs.empty()) {
        assert(srcOffset + offset < offSpillLEDs.size());
        item.offSpillLED = offSpillLEDs[srcOffset + offset];
    } else {
        item.offSpillLED = std::nan("0");
    }
    if(!inSpillLEDs.empty()) {
        assert(srcOffset + offset < inSpillLEDs.size());
        item.inSpillLED = inSpillLEDs[srcOffset + offset];
    } else {
        item.inSpillLED = std::nan("0");
    }
    #else
    if(!pedestals.empty()) {
        auto it = pedestals.find(nm);
        assert(pedestals.end() != it);
        assert(2*offset + 1 < it->second.size());
        item.pedestals[0] = it->second[2*offset     ];
        item.pedestals[1] = it->second[2*offset + 1 ];
    } else {
        item.pedestals[0] = item.pedestals[1] = std::nan("0");
    }
    if(!offSpillLEDs.empty()) {
        auto it = offSpillLEDs.find(nm);
        assert(offSpillLEDs.end() != it);
        assert(offset < it->second.size());
        item.offSpillLED = it->second[offset];
    } else {
        item.offSpillLED = std::nan("0");
    }
    if(!inSpillLEDs.empty()) {
        auto it = inSpillLEDs.find(nm);
        assert(inSpillLEDs.end() != it);
        assert(offset < it->second.size());
        item.inSpillLED = it->second[offset];
    } else {
        item.inSpillLED = std::nan("0");
    }
    #endif
}

std::unordered_map<na64dp::DetID, MSADCCalibPedLedType1CollectionBase::Item>
MSADCCalibPedLedType1CollectionBase::get_mapped(
        const na64dp::nameutils::DetectorNaming & naming,
        log4cpp::Category * logCatPtr
        ) const {
    int nSection = 0;
    char namebf[32];
    std::unordered_map<na64dp::DetID, Item> r;
    if(_layout.empty()) return r;
    //size_t secOffset = 0;  // denotes section start
    // Layout, as at June, 2023
    // - first section is assumed to be ECAL (ec_loc=0), 6x5x2 cells
    //   original dimensions: ec_col, ec_row, ec_sec, => ec_cells
    if(_layout[nSection].first != "ECAL" || _layout[nSection].second != 6*5*2) {
        char bf[128];
        snprintf(bf, sizeof(bf), "Layout error: section #%d is \"%s\" of"
                " size %d, while expected \"ECAL\" of size 60", nSection
                , _layout[nSection].first.c_str(), _layout[nSection].second );
        throw std::runtime_error(bf);
    }
    for(int xIdx = 0; xIdx < 5; ++xIdx) {
        for(int yIdx = 0; yIdx < 6; ++yIdx) {
            for(int zIdx = 0; zIdx < 2; ++zIdx) {
                snprintf( namebf, sizeof(namebf)  // TODO: check
                        , "ECAL0:%d-%d-%d", xIdx, yIdx, zIdx );
                na64dp::DetID itemID = naming[namebf];
                // ((z*ec_col+x)*ec_row)+y, ec_col=5, ec_row=6
                size_t offset = ((zIdx*5 + xIdx)*6)+yIdx;
                    Item item;
                    _copy_values_to_item(item, offset, "ECAL");
                    r[itemID] = item;
                if(logCatPtr) {
                    *logCatPtr << log4cpp::Priority::DEBUG
                         << "ECAL cell " << xIdx << "x" << yIdx << "x" << zIdx
                         << " -> \"" << naming[itemID].c_str() << "\", offset="
                         << offset << "; mean.ped=("
                         << item.pedestals[0] << ", " << item.pedestals[1]
                         << "), off-burst LED=" << item.offSpillLED
                         << ", in-burst LED=" << item.inSpillLED;
                }
            }
        }
    }
    //secOffset += _layout[nSection].second;
    // - second section is assumed to be HCAL (hc_loc = ec_loc + ec_cells),
    //   3x3x4 cells, original dimensions: hc_col, рс_row, hc_sec, => hc_cells
    ++nSection;
    if(_layout[nSection].first != "HCAL" || _layout[nSection].second != 4*3*3) {
        char bf[128];
        snprintf(bf, sizeof(bf), "Layout error: section #%d is \"%s\" of"
                " size %d, while expected \"HCAL\" of size 36", nSection
                , _layout[nSection].first.c_str(), _layout[nSection].second );
        throw std::runtime_error(bf);
    }
    for(int xIdx = 0; xIdx < 3; ++xIdx) {
        for(int yIdx = 0; yIdx < 3; ++yIdx) {
            for(int zIdx = 0; zIdx < 4; ++zIdx) {
                snprintf( namebf, sizeof(namebf)  // TODO: check
                        , "HCAL%d:%d-%d--", zIdx, xIdx, yIdx );
                na64dp::DetID itemID = naming[namebf];
                // ((z*hc_col+x)*hc_row)+y, hc_col=3, hc_row=3
                size_t offset = (zIdx*3 + xIdx)*3 + yIdx;
                Item item;
                _copy_values_to_item(item, offset, "HCAL");
                r[itemID] = item;
                if(logCatPtr) {
                    *logCatPtr << log4cpp::Priority::DEBUG
                         << "HCAL cell " << xIdx << "x" << yIdx << "x" << zIdx
                         << " -> \"" << naming[itemID].c_str() << "\", offset="
                         << offset << "; mean.ped=("
                         << item.pedestals[0] << ", " << item.pedestals[1]
                         << "), off-burst LED=" << item.offSpillLED
                         << ", in-burst LED=" << item.inSpillLED;
                }
            }
        }
    }
    //secOffset += _layout[nSection].second;
    // - third section is assumed to be WCAL (wc_loc = hc_loc + hc_cells),
    //   NOTE: it is only enabled for visible setup (ifdef VISIBLE), we rely on
    //   file layout
    //   just 3 cells, optional
    ++nSection;
    if(_layout[nSection].first == "WCAL") {
        if(_layout[nSection].second != 3) {
            char bf[128];
            snprintf(bf, sizeof(bf), "Layout error: section #%d is \"WCAL\" of"
                    " size %d, while expected only size 3 cells", nSection
                    , _layout[nSection].second );
            throw std::runtime_error(bf);
        }
        for(int i = 0; i < 3; ++i) {
            snprintf( namebf, sizeof(namebf)  // TODO: check
                    , "WCAL%d", i );
            na64dp::DetID itemID = naming[namebf];
            // ((z*hc_col+x)*hc_row)+y, hc_col=3, hc_row=3
            Item item;
            _copy_values_to_item(item, i, "WCAL");
            r[itemID] = item;
        }
        //secOffset += _layout[nSection].second;
    }
    // - next section is always a 3-cell SRD
    if(_layout[nSection].first != "SRD" || _layout[nSection].second != 3) {
        char bf[128];
        snprintf(bf, sizeof(bf), "Layout error: section #%d is \"%s\" of"
                " size %d, while expected \"SRD\" of size 3", nSection
                , _layout[nSection].first.c_str(), _layout[nSection].second );
        throw std::runtime_error(bf);
    }
    for(int i = 0; i < 3; ++i) {
        snprintf( namebf, sizeof(namebf)  // TODO: check
                , "SRD%d", i );
        na64dp::DetID itemID = naming[namebf];
        // ((z*hc_col+x)*hc_row)+y, hc_col=3, hc_row=3
        Item item;
        _copy_values_to_item(item, i, "SRD");
        r[itemID] = item;
    }
    //secOffset += _layout[nSection].second;
    // - next section is always a 6 items of VETO
    ++nSection;
    if(_layout[nSection].first != "VETO" || _layout[nSection].second != 6) {
        char bf[128];
        snprintf(bf, sizeof(bf), "Layout error: section #%d is \"%s\" of"
                " size %d, while expected \"VETO\" of size 6", nSection
                , _layout[nSection].first.c_str(), _layout[nSection].second );
        throw std::runtime_error(bf);
    }
    for(int i = 0; i < 6; ++i) {
        snprintf( namebf, sizeof(namebf)  // TODO: check
                , "VETO%d", i );
        na64dp::DetID itemID = naming[namebf];
        // ((z*hc_col+x)*hc_row)+y, hc_col=3, hc_row=3
        Item item;
        _copy_values_to_item(item, i, "VETO");
        r[itemID] = item;
    }
    //secOffset += _layout[nSection].second;
    return r;
}


//
// Loader

void
MSADCCalibPedLedType1Collection<sdc::SrcInfo<MSADCCalibPedLedType1>>::add_values(
        const sdc::SrcInfo<MSADCCalibPedLedType1> & calibs) {
    //try {  // todo
    _add_values(calibs.data);
    //} catch(...)
}

bool
MSADCCalibPedLedType1Loader::can_handle(const std::string & docID) const {
    # if 1
    return std::regex_match(docID, _pathPattern);
    # else
    assert(!docID.empty());
    char * pathCpy = strdup(docID.c_str());
    char * fName = basename(pathCpy);
    std::regex pedLEDPathPat(R"~(run_\d{1-8})~");
    bool r = std::regex_match(fName, pedLEDPathPat);
    free(pathCpy);
    return r;
    # endif
}

std::list<sdc::Documents<na64dp::EventID>::DataBlock>
MSADCCalibPedLedType1Loader::get_doc_struct( const std::string & docID ) {
    // pre-parse the file, providing DataBlock items (data type, validity
    std::list<sdc::Documents<na64dp::EventID>::DataBlock> blocks;

    std::string line;
    std::ifstream ifs(docID);
    size_t nLine = 0;
    while(std::getline(ifs, line)) {
        if( !(nLine++) ) {
            // parse file header, expected format is the line started from
            // "channel sequance ...", having meaningful metadata like
            // "ECAL:30,HCAL:60...". We omit this line at pre-parsing stage
            continue;
        }
        if(line.empty()) continue;
        auto toks = na64dp::util::tokenize_quoted_expression(line);
        if(toks.empty()) continue;
        if(toks[0] == "stat_off") {
            assert(toks.size() > 9);
            // this is spill header, similar to:
            // stat_off   651 stat_on   142 RUN     7899 Spill     1 Time 1663639003 Tue Sep 20 03:56:43 2022
            // 0          1   2         3   4       5    6         7 8    9 ...
            na64sw_runNo_t runNo;  // TODO: assure matches filename
            na64sw_spillNo_t nSpill;
            //try {  TODO
            runNo = std::stoul(toks[5]);  // TODO: assure matches filename
            nSpill = std::stoul(toks[7]);
            //} catch(...)  // invalid_argument, out_of_range
            blocks.push_back(sdc::Documents<na64dp::EventID>::DataBlock{
                    "donskov/PedLED",
                    sdc::ValidityRange<na64dp::EventID>{
                            na64dp::EventID(runNo, nSpill,   0)
                          , na64dp::EventID(runNo, nSpill+1, 0)
                          },
                    nLine
                });
        }
    }

    return blocks;
}

void
MSADCCalibPedLedType1Loader::read_data( const std::string & docID
                  , na64dp::EventID k
                  , const std::string & forType
                  , /*IntradocMarkup_t -> */ size_t acceptFrom
                  , ReaderCallback cllb
                  ) {
    std::string line;
    std::ifstream ifs(docID);
    size_t nLine = 0;
    sdc::aux::MetaInfo mi;
    mi.set("@docID", docID, std::numeric_limits<size_t>::min());
    while(std::getline(ifs, line)) {
        if( !(nLine++) ) {  // read layout from first line
            mi.set("header", line, nLine);
            continue;
        }
        #if 1
        if(nLine < acceptFrom)
            continue;  // omit lines prior to the one specific for current spill
        if(line.empty()) continue;
        auto toks = na64dp::util::tokenize_quoted_expression(line);
        if(toks.empty()) continue;
        if(nLine == acceptFrom) {
            // use this line to initialize spill header
            // stat_off   651 stat_on   142 RUN     7899 Spill     1 Time 1663639003 Tue Sep 20 03:56:43 2022
            // 0          1   2         3   4       5    6         7 8    9 ...
            mi.set("statOff",   toks[1], nLine);
            mi.set("statOn",    toks[3], nLine);
            mi.set("runNo",     toks[5], nLine);
            mi.set("spillNo",   toks[9], nLine);
            mi.set("timestamp", toks[8], nLine);
            // ... timestamp, whatever?
            continue;
        }
        #endif
        if( nLine > acceptFrom
         && line.find("stat_off") != std::string::npos) break;  // exit loop as it's next spill header
        cllb(mi, mi.get<size_t>("@lineNo"), line);
        //cllb(toks);
    }
    // mi.set("@lineNo", lineNoStr, std::numeric_limits<size_t>::min());
}


}  // namespace P348_RECO_NAMESPACE

