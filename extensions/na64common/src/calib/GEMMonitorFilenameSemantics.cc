#include "na64common/calib/GEMMonitorFilenameSemantics.hh"

namespace na64dp {
namespace util {

int
interpret_GEMMonitor_filename( const std::string & path
                             , sdc::aux::MetaInfo & mi
                             ) {
    size_t n;
    // split path and filename
    std::string dirPath, filename;
    n = path.rfind('/');
    if( n == path.size()-1 ) return -1;  // last char is '/' => junk
    if( std::string::npos == n ) {
        filename = path;
    } else {
        dirPath = path.substr(0, n);
        filename = path.substr(n+1);
    }
    // guess the file type by name
    // - if has "~~start-", assume it is a validity period marker
    const std::string validityMarkers[2]={"~~start-", "~~finish-"};
    std::string head;
    if( std::string::npos != (n = filename.find(validityMarkers[0])) ) {
        head = filename.substr( 0, n );
        const std::string tail = filename.substr( n + validityMarkers[0].size() );
        n = tail.find(validityMarkers[1]);
        if( std::string::npos == n ) return -2;  // no "~~finish" marker
        mi.set("timeFrom", tail.substr(0, n));
        size_t finishPos = n + validityMarkers[1].size();
        // get the extension after "to"-marker
        n = tail.rfind('.');
        mi.set("timeTo", tail.substr( finishPos
                            , n == std::string::npos
                            ? std::string::npos
                            : n - finishPos ) );
        if( n != std::string::npos ) {
            mi.set( "extension", tail.substr(n+1) );
        }
    } else if( std::string::npos != (n = filename.rfind('.')) ) {
        // no validity period semantics in filename => jsut look for extension
        head = filename.substr(0, n);
        mi.set("extension", filename.substr(n+1));
    } else {
        // no validity period, no extension
        head = filename;
    }
    // now, try to interpret `head' as either a detector + "_timing", or just
    // a detector name
    const std::string timeMarker = "timing";
    bool isTiming = (std::string::npos != (n = head.rfind(timeMarker)));
    if( isTiming ) {  // get rid of suffix and add "isTiming" meta info
        mi.set("isTiming", "yes");
        head = head.substr(0, n);
    }
    // Check match for detector name (COMPASS TBname)
    // - starts with capital letter
    // - consists only of the capital letters, digits and '_'
    if( head.empty() ) return -3;  // no filename after all the cuts...
    if( ! isupper(head[0]) ) return -4;  // doesn't start with capital letter
    for( char c : head ) {
        bool r = false;
        r |= isalpha(c);
        r |= isdigit(c);
        r |= c == '_';
        if( !r ) return -5;
    }
    mi.set("detector", head);

    // try to assume validity period from path if it is not empty
    if(!dirPath.empty()) {
        // iterate over directory files from back and look up for token
        // consisting of four digits and optionally a suffix. This way we
        // can extract year/period ID from paths like
        //  - "./foo/../bar/2016b/" => 2016b
        size_t prevN = dirPath.size() - 1;
        do {
            std::string tok;
            if( std::string::npos != (n = dirPath.rfind('/', prevN)) ) {
                tok = dirPath.substr(n + 1, prevN - n);
            } else {
                tok = dirPath.substr(0, prevN);
            }
            // check if this token matches the "ddd" pattern
            {
                bool looksLikeYear = true;
                for( int i = 0; i < 4; ++i ) {
                    if( isdigit(tok[i]) ) continue;
                    looksLikeYear = false;
                    break;
                }
                if( looksLikeYear ) {
                    mi.set("period", tok);
                    break;  // found a year/period notion
                }
            }
            prevN = n - 1;
        } while( n && std::string::npos != n );
    }
    // Finalize metadata with some document descriptions
    mi.set("documentFullPath", path);
    mi.set("filename", filename);
    mi.set("directory", dirPath);
    return 1;  // ok, looks like an APV calibrations file
}

}  // namespace ::na64dp::util
}  // namespace na64dp

