// - include general project's configuration
#include "na64sw-config.h"
// repeat what is defined in na64calib/sdc.hh except for SDC_NO_IMPLEM
#ifdef SDC_NO_IMPLEM
#   undef SDC_NO_IMPLEM
#endif
#define SDC_INLINE /*no inline*/
#define P348_RECO_NAMESPACE p348reco
//#include "na64calib/sdc-base.hh"

//
// aux utils

#include "na64detID/TBName.hh"
#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

#include <log4cpp/Category.hh>

namespace na64dp {
namespace nameutils {

DetID
sdc_id( const nameutils::DetectorNaming & nm
      , const std::string & nameZ
      , uint16_t xIdx, uint16_t yIdx, uint16_t zIdx
      , const std::string & docID
      , size_t lineNo
      ) {
    // convert `namez' to detector ID using usual name TBname conversion
    DetID did = nm[nameZ];
    if( did.chip() == nm.chip_id("SADC") ) {
        // append payload with indices
        CellID cid;  //(xIdx, yIdx, zIdx);
        cid.set_x(xIdx);
        cid.set_y(yIdx);
        // This is a kind of kludge preventing the unset number in payload.
        // It was not in original specification, but it seems that this
        // semantic is used and 0 is implied...
        if( !did.is_number_set() ) {
            did.number(0);
        }

        if( nm.kin_id("ECAL").second == did.kin()
         && nm.kin_id("ECAL").first == did.chip()
          ) {
            did.number(0);  // always
            cid.set_z(zIdx);
        } else if( did.is_number_set() && (did.number() != zIdx) ) {
            // Development warning: some detectors probably will still require
            // z-index (e.g. hodoscopes, ECAL, etc). This warning is to notify
            // developer on this inconsistency. If you see it, one must append
            // if-conditions above to correctly resolve this situation.
            if(docID.empty()) {
                log4cpp::Category::getInstance("calib.sdc")
                    .warn( "Naming inconsistency: SDC name-z conversion \"%s\" has"
                           " incompatible z-index %d."
                           , nameZ.c_str(), (int) zIdx );
            } else {
                log4cpp::Category::getInstance("calib.sdc")
                    .warn( "%s:%zu: naming inconsistency: SDC name-z"
                           " conversion \"%s\" has incompatible z-index %d."
                         , docID.c_str(), lineNo
                         , nameZ.c_str(), (int) zIdx );
            }
        }
        // set payload
        did.payload(cid.cellID);
    } else if( did.chip() == nm.chip_id("APV") ) {
        // no special treatment is required here as APV detectors are
        // identified by their DAQ src ID anyway.
    } else {
        NA64DP_RUNTIME_ERROR("Can't convert SDC detector name \"%s\" to"
                " `na64dp::DetID' -- conversion for chip \"%s\" is not"
                " defined."
                , nameZ.c_str()
                , nm.chip_features(did.chip()).name.c_str()
                );
    }

    return did;
}

}  // namespace ::na64dp::nameutils
}  // namespace na64dp

