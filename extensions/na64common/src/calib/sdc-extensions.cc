#define SDC_NO_IMPLEM 0  // "0" has special meaning
#define SDC_INLINE /*inline*/
#   include "na64common/calib/sdc-integration.hh"
#undef SDC_NO_IMPLEM
#undef SDC_INLINE

#include "na64common/calib/periods.hh"
#include "na64common/calib/GEMMonitorFilenameSemantics.hh"

namespace sdc {
std::string
ValidityTraits<na64dp::EventID>::to_string(na64dp::EventID eid) {
    std::ostringstream oss;
    oss << eid.run_no();
    if(eid.spill_no() || eid.event_no()) oss << "/" << (int) eid.spill_no();
    if(eid.event_no()) oss << "." << (int) eid.event_no();
    return oss.str();
}

na64dp::EventID
ValidityTraits<na64dp::EventID>::from_string(const std::string & stre) {
    const char * bf = stre.c_str();

    na64sw_EventID_t r = 0x0;
    char * runEnd
       , * spillEnd
       , * eventEnd
       ;
    /* parse run number */
    long int parsed = strtol( bf, &runEnd, 10 );
    if( (*runEnd != '\0' && *runEnd != '/')
     || parsed > na64sw_g_runNo_max ) {
        NA64DP_RUNTIME_ERROR("Run number %ld > %ld", parsed, na64sw_g_runNo_max );
    }
    na64sw_set_runNo( &r, parsed );
    if(*runEnd == '\0') {
        return na64dp::EventID(r);
    }

    /* parse spill number */
    parsed = strtol( runEnd + 1, &spillEnd, 10 );
    if( (*spillEnd != '\0' && *spillEnd != '.')
     || parsed > na64sw_g_spillNo_max ) {
        NA64DP_RUNTIME_ERROR("Spill number %ld > %ld", parsed, na64sw_g_spillNo_max );
    }
    na64sw_set_spillNo( &r, parsed );
    if(*spillEnd == '\0') return na64dp::EventID(r);

    /* parse event number */
    parsed = strtol( spillEnd + 1, &eventEnd, 10 );
    if( parsed > na64sw_g_eventNo_max ) {
        NA64DP_RUNTIME_ERROR("Event number %ld > %ld", parsed, na64sw_g_runNo_max );
    }
    na64sw_set_eventNo( &r, parsed );

    return na64dp::EventID(r);
}
}  // namespace sdc

namespace P348_RECO_NAMESPACE {

time_t
parse_GEMMonitor_timedate_strexpr(const char *c) {
    // NOTE: to the time, matches to-string conversion provided by
    // `na64dp::util::parse_timedate_strexpr()`, but one should not rely on
    // this as the latter one is framework-standard conversion while this
    // function is written specifically to match GEMMonitor notation.

    // Parse timedate string into GNU POSIX ::tm instance
    // Note, that these time strings are given in local CERN-Prevessin EHN1
    // location's timezone (Europe/Paris). To get the true timestamp (UTC)
    // one need to convert it using timezone info
    ::tm tm;
    memset(&tm, 0, sizeof(tm));
    strptime(c, "%Y-%m-%d-%H:%M:%S", &tm);
    //printf( "xxx %s\n", tm.tm_zone );
    // Not set by strptime(); tells mktime() to determine whether daylight
    // saving time is in effect:
    tm.tm_isdst = -1;
    // Force the measurement timezone
    //tm.tm_zone = "Europe/Paris";  // does not work
    // Convert to time_t
    time_t t = mktime(&tm);
    if(-1 == t) {
        return 0;
    }
    //printf( "%s => year=%d, month=%d, date=%d, time=%d:%d:%d ; timestamp=%ld\n"
    //          , c
    //          , 1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday
    //          , tm.tm_hour, tm.tm_min, tm.tm_sec
    //          , t
    //          );  // xxx
    return t;
}

std::shared_ptr<KeyTraits<Datetime_t>::LookupObject>
KeyTraits<Datetime_t>::instantiate_lookup_object( const std::string & paths
                                                , const std::string & acceptPatterns
                                                , const std::string & rejectPatterns
                                                , size_t minSize, size_t maxSize
                                                ) {
    // Rely on global period aliases
    auto lo = std::make_shared<LookupObject>
                           ( paths
                           , acceptPatterns, rejectPatterns
                           , minSize, maxSize
                           );
    lo->aliases = &na64dp::calib::Periods::self();
    return lo;
}

}  // namespace P348_RECO_NAMESPACE

