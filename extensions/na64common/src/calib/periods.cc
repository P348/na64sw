#include "na64common/calib/periods.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {

namespace errors {

const char *
NoAliasDefinedForRun::what() const throw() {
    snprintf( _errBf, sizeof(_errBf)
            , "No alias defined for run number %s"
            , sdc::ValidityTraits<na64dp::EventID>::to_string(rn).c_str()
            );
    return _errBf;
}

const char *
NoAliasDefinedForTime::what() const throw() {
    snprintf( _errBf, sizeof(_errBf)
            , "No alias defined for date/time %s"
            , sdc::ValidityTraits<p348reco::Datetime_t>::to_string(dtm).c_str()
            );
    return _errBf;
}

}  // namespace ::na64dp::errors

namespace calib {

Periods * Periods::_self = nullptr;

Periods &
Periods::self() {
    if( ! _self ) _self = new Periods();
    return *_self;
}

void
Periods::add_from_YAML_node( const YAML::Node & node ) {
    for( auto it = node.begin()
       ; node.end() != it
       ; ++it ) {
        const YAML::Node & sn = it->second;
        p348reco::Aliases::emplace_back( it->first.as<std::string>()
                , p348reco::PeriodAlias( sn["datetime"][0].as<std::string>()
                                       , sn["runs"][0].as<int>()
                                       , sn["datetime"][1].as<std::string>()
                                       , sn["runs"][1].as<int>()
                                       )
                );
        _comments.emplace( &(p348reco::Aliases::rbegin()->second)
                         , sn["comment"].as<std::string>()
                         );
    }
}

Periods::EntryProxy
Periods::operator[](na64dp::EventID rn) const {
    for( auto it = rbegin()
       ; it != rend()
       ; ++it ) {
        const p348reco::PeriodAlias & pa = it->second;
        if( (!(rn < pa.runs.from)) && rn < pa.runs.to ) {
            auto iit = _comments.find(&(it->second));
            assert( _comments.end() != iit );
            // ^^^ must be prohibited by design, unless user added alias
            //     manually
            return EntryProxy {
                    it->first,
                    iit->second,
                    &(it->second)
                };
        }
    }
    throw errors::NoAliasDefinedForRun(rn);
}

Periods::EntryProxy
Periods::operator[](P348_RECO_NAMESPACE::Datetime_t dtm) const {
    for( auto it = rbegin()
       ; it != rend()
       ; ++it ) {
        const p348reco::PeriodAlias & pa = it->second;
        if( (!(dtm < pa.time.from)) && dtm < pa.time.to ) {
            auto iit = _comments.find(&(it->second));
            assert( _comments.end() != iit );
            return EntryProxy {
                    it->first,
                    iit->second,
                    &(it->second)
                };
        }
    }
    throw errors::NoAliasDefinedForTime(dtm);
}

}  // namespace ::na64dp::calib
}  // namespace na64dp

