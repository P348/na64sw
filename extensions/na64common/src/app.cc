#include "na64common/app.hh"
#include "na64dp/abstractHandler.hh"
#include "na64detID/TBName.hh"
#include "na64calib/loader-generic.hh"
#include "na64calib/config-yaml.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64calib/evType.hh"
#include "na64util/runtimeDirs.hh"
#include "na64calib/manager-utils.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64common/calib/periods.hh"
#include "na64event/data/event.hh"
#include "na64sw-version.h"

#include <regex>
#include <fcntl.h>
#include <dlfcn.h>

namespace na64dp {
namespace util {

#define _M_xstr(m) _M_str(m)
#define _M_str(m) # m

void
print_banner( std::ostream & os
            , const char * appDescr
            ) {
    os <<
        "\n"
        "    \033[36;2;1m    _  _____  ____ __ _ ____    __ \033[0m\n"
        "    \033[36;2;1m   / |/ / _ |/ __// / (_ -< |/|/ / \033[0m\n"
        "    \033[36;2;1m  /    / __ / _ \\/_  _/___/__,__/  \033[0m\n"
        "    \033[36;2;1m /_/|_/_/ |_\\___/ /_/ \033[0m \033[1;2mv" na64sw_VERSION_STR "\033[0m\n"
        "\n"
        "  \033[2m" << appDescr << "\033[0m.\n"
        "  Documentation: <\033[4mhttps://na64sw.docs.cern.ch/\033[0m>\n"
        "  Copyright (C) 2015-2023 \033[1mNA64 Collaboration\033[0m, CERN, SPS;\n"
        "  Licensed under GPLv3 (see <\033[4mhttps://www.gnu.org/licenses/\033[0m>).\n"
        "\n"
        ;
}
#undef _M_str
#undef _M_xstr

void
set_std_environment_variables() {
    // assure the prefix variable is set
    char * prefixVar = getenv(NA64SW_PREFIX_ENVVAR);
    if(NULL == prefixVar || '\0' == *prefixVar) {
        throw std::runtime_error("NA64sw prefix environment variable \""
                NA64SW_PREFIX_ENVVAR "\" is not defined or empty.");
    }
    // overwrite=0 means that if variable is set, its value
    // will be left intact
    setenv( NA64SW_RUN_CFG_PATH_ENVVAR
          , ".:${" NA64SW_PREFIX_ENVVAR "}/share/na64sw/run/"
          , 0 );
    setenv( NA64SW_MODULES_PATH_ENVVAR
          , ".:${" NA64SW_PREFIX_ENVVAR "}/lib/na64sw-extensions/"
          , 0 );
    // ... other environment variables here
}

void
list_registered( std::ostream & os
               , const char * type ) {
    // HANDLERS
    if( (!type) || !strcmp( "handlers", type ) ) {
        os << "\033[1mhandlers\033[0m (existing event data handler entities):" << std::endl;
        try {
            for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractHandler>() ) {
                os << " * " << p.first << " -- " << p.second.second << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    // SOURCES
    if( (!type) || !strcmp( "sources", type ) ) {
        os << "\033[1msources\033[0m (existing event data source format codecs):" << std::endl;
        try {
            for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractEventSource>() ) {
                os << " * " << p.first << " -- " << p.second.second << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
        if( type ) return;
    }
    // SADCHit
    if( (!type) || !strcmp( "SADCHit", type ) || !strcmp("sadcHits", type) ) {
        os << "Getters for \033[1msadcHits\033[0m (event field)" << std::endl;
        for( const auto & p : na64dp::event::Traits<na64dp::event::SADCHit>::getters ) {
            os << " * " << p.first << " -- " << p.second.first << std::endl;
        }
        if( type ) return;
    }
    // APVHit
    if( (!type) || !strcmp( "APVHit", type ) || !strcmp("apvHits", type) ) {
        os << "Getters for \033[1mapvHits\033[0m (event field)" << std::endl;
        for( const auto & p : na64dp::event::Traits<na64dp::event::APVHit>::getters ) {
            os << " * " << p.first << " -- " << p.second.first << std::endl;
        }
        if( type ) return;
    }
    if( (!type) || !strcmp( "StwTDCHit", type ) || !strcmp("stwtdcHits", type) ) {
        os << "Getters for \033[1mstwtdcHits\033[0m (event field)" << std::endl;
        for( const auto & p : na64dp::event::Traits<na64dp::event::StwTDCHit>::getters ) {
            os << " * " << p.first << " -- " << p.second.first << std::endl;
        }
        if( type ) return;
    }
    // CaloHit
    if( (!type) || !strcmp( "CaloHit", type ) || !strcmp("caloHits", type) ) {
        os << "Getters for \033[1mcaloHits\033[0m (event field)" << std::endl;
        for( const auto & p : na64dp::event::Traits<na64dp::event::CaloHit>::getters ) {
            os << " * " << p.first << " -- " << p.second.first << std::endl;
        }
        if( type ) return;
    }
    // ...other (track, trackScore, etc)

    if( (!type) || !strcmp( "detector-id-definitions", type ) ) {
        os << "\033[1mdetector-id-definitions\033[0m (Grammar entities for detector selection):" << std::endl;
        try {
            for( auto ge = na64dp::util::gDetIDGetters
               ; ge->name
               ; ++ge ) {
                if( '+' == ge->name[0] ) continue;  // omit external resolvers
                os << " * " << ge->name + 1 << " -- " << ge->description << std::endl;
            }
        } catch( na64dp::errors::NoCtrsOfType & e ) {
            os << " <no entries>" << std::endl;
        }
    }

    if( (!type) || !strcmp( "strPatterns", type ) ) {
        os << "\033[1mstrPatterns\033[0m (detector ID strings):" << std::endl
           << " - Defined for all DetID indexes (may not be complete,"
              " depending on DetID subtype):" << std::endl
           << "   * chip -- a human-readable detector chip name"
              " (\"SADC\", \"APV\", \"F1\", etc)" << std::endl
           << "   * chipDesc -- verbose chip description (contains spaces)" << std::endl
           << "   * chipNo -- hexadecimal chip identifier string" << std::endl
           << "   * kin -- a human-readable identifier for detector kin"
              " (MM, ECAL, ST, HCAL, etc)" << std::endl
           << "   * kinDesc -- verbose kin description (contains spaces)" << std::endl
           << "   * kinNo -- hexadecimal kin ID (no chip part)" << std::endl
           << "   * statNum -- decimal number of station (0, 1, 2, etc)" << std::endl
           << "   * statNum2 -- two-digit decimal number of station (00, 01,"
              " 02, etc)" << std::endl
           << "   * subDet -- detector ID payload part (like \"1-2\","
              " \"X-432\", etc)" << std::endl  // TODO: check
           << "   * xIdx (for cell ID) -- x-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * yIdx (for cell ID) -- y-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * zIdx (for cell ID) -- z-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * proj -- \"X\" or \"Y\" projection of MSADC hodoscope"
              " + \"U\"/\"V\" for APV/NA64TDC/F1 based dets" << std::endl
           << "   * wireNo -- number of wire for APV/F1/NA64TDC-based detectors" << std::endl
           << " - Defined for all TrackID indexes:" << std::endl
           << "   * trackID -- hexadecimal number of track (like 0xa2e01)" << std::endl
           << "   * trackZonePattern -- hexadecimal number of zone pattern"
              " (like 0x1, 0x2, etc)" << std::endl
           ;
    }
}

void
list_registered_extensions(YAML::Node & node) {
    node["handlers"] = YAML::Node(YAML::NodeType::Map);
    try {
        for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractHandler>() ) {
            node["handlers"][p.first] = p.second.second;
        }
    } catch( na64dp::errors::NoCtrsOfType & e ) {
        // pass
    }
    node["sources"] = YAML::Node(YAML::NodeType::Map);
    try {
        for( const auto & p : na64dp::VCtr::self().registry<na64dp::AbstractEventSource>() ) {
            node[p.first] = p.second.second;
        }
    } catch( na64dp::errors::NoCtrsOfType & e ) {
        // pass
    }
    node["getters"] = YAML::Node(YAML::NodeType::Map);
    node["getters"]["sadcHits"] = YAML::Node(YAML::NodeType::Map);
    for( const auto & p : na64dp::event::Traits<na64dp::event::SADCHit>::getters ) {
        node["getters"]["sadcHits"][p.first] = p.second.first;
    }
    node["getters"]["apvHits"] = YAML::Node(YAML::NodeType::Map);
    for( const auto & p : na64dp::event::Traits<na64dp::event::APVHit>::getters ) {
        node["getters"]["apvHits"][p.first] = p.second.first;
    }
    node["getters"]["stwtdcHits"] = YAML::Node(YAML::NodeType::Map);
    for( const auto & p : na64dp::event::Traits<na64dp::event::StwTDCHit>::getters ) {
        node["getters"]["stwtdcHits"][p.first] = p.second.first;
    }
    node["getters"]["caloHits"] = YAML::Node(YAML::NodeType::Map);
    for( const auto & p : na64dp::event::Traits<na64dp::event::CaloHit>::getters ) {
        node["getters"]["caloHits"][p.first] = p.second.first;
    }
    // ...other (track, trackScore, etc)
    node["detIDResolvers"] = YAML::Node(YAML::NodeType::Map);
    for( auto ge = na64dp::util::gDetIDGetters
       ; ge->name
       ; ++ge ) {
        if( '+' == ge->name[0] ) continue;  // omit external resolvers
        node["detIDResolvers"][ge->name] = ge->description;
    }
    #if 0  // TODO
    node["detIDStrFormatPatterns"] = YAML::Node(YAML::NodeType::Map);
    node["detIDStrFormatPatterns"]
        = 
    if( (!type) || !strcmp( "strPatterns", type ) ) {
        os << "\033[1mstrPatterns\033[0m (detector ID strings):" << std::endl
           << " - Defined for all DetID indexes (may not be complete,"
              " depending on DetID subtype):" << std::endl
           << "   * chip -- a human-readable detector chip name"
              " (\"SADC\", \"APV\", \"F1\", etc)" << std::endl
           << "   * chipDesc -- verbose chip description (contains spaces)" << std::endl
           << "   * chipNo -- hexadecimal chip identifier string" << std::endl
           << "   * kin -- a human-readable identifier for detector kin"
              " (MM, ECAL, ST, HCAL, etc)" << std::endl
           << "   * kinDesc -- verbose kin description (contains spaces)" << std::endl
           << "   * kinNo -- hexadecimal kin ID (no chip part)" << std::endl
           << "   * statNum -- decimal number of station (0, 1, 2, etc)" << std::endl
           << "   * statNum2 -- two-digit decimal number of station (00, 01,"
              " 02, etc)" << std::endl
           << "   * subDet -- detector ID payload part (like \"1-2\","
              " \"X-432\", etc)" << std::endl  // TODO: check
           << "   * xIdx (for cell ID) -- x-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * yIdx (for cell ID) -- y-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * zIdx (for cell ID) -- z-index of MSADC detector (0, 1, etc)" << std::endl
           << "   * proj -- \"X\" or \"Y\" projection of MSADC hodoscope"
              " + \"U\"/\"V\" for APV/NA64TDC/F1 based dets" << std::endl
           << "   * wireNo -- number of wire for APV/F1/NA64TDC-based detectors" << std::endl
           << " - Defined for all TrackID indexes:" << std::endl
           << "   * trackID -- hexadecimal number of track (like 0xa2e01)" << std::endl
           << "   * trackZonePattern -- hexadecimal number of zone pattern"
              " (like 0x1, 0x2, etc)" << std::endl
           ;
    }
    #endif
}

/// Output log stream for the `load_modules()` function, sometimes used for
/// postponed logging
static std::ostringstream * _moduleLoadingLog = nullptr;
/// Regular expression matching most common extension loading error of
/// undefined symbol
///
/// \todo compilers other than GCC?
static const std::regex _rxUndefineSymbol("[uU]ndefined\\s+symbol\\s*:?\\s*([^\\s]+)");

int
load_modules( const std::list<std::string> & modules_
            , const char * paths
            , bool quiet
            ) {
    int nFailures = 0;
    if( ! _moduleLoadingLog ) {
        _moduleLoadingLog = new std::ostringstream;
        assert(_moduleLoadingLog);
    }
    std::ostringstream & oss = *_moduleLoadingLog;
    std::vector<std::string> modules;
    std::ostringstream mlLog;
    if( ! paths ) paths = ".";

    // collect reachable paths for the modules
    na64dp::util::RuntimeDirs rd( paths );
    for( auto mdl_ : modules_ ) {
        struct stat mfStat;
        int mfStatRC = stat( mdl_.c_str(), &mfStat );
        if( -1 != mfStatRC && S_ISREG( mfStat.st_mode ) )  {
            modules.push_back(mdl_);
            continue;
        }
        std::vector<std::string> modulePaths = rd.locate_files( "{lib,}" + mdl_ + "{.so,}" );
        if( modulePaths.empty() ) {
            oss << "\033[1;31mError\033[0m: unable"
                   " to find module \"" << mdl_ << "\"." << std::endl;
            continue;
        }
        if( modulePaths.size() > 1 ) {
            oss << "\033[1;33mWarning\033[0m: few eponymous candidates found for module \""
                    << mdl_ << "\" (first takes precedence): ";
            for( auto mfe : modulePaths ) {
                oss << mfe << ", ";
            }
            oss << std::endl;
        }
        modules.push_back(*modulePaths.begin());
    }

    // load modules
    for( auto mf : modules ) {
        void * libPtr = dlopen(mf.c_str(), RTLD_NOW | RTLD_GLOBAL);
        if( !libPtr ) {
            const std::string errTxt = dlerror();
            oss << "\033[1;31mError\033[0m: "
                << "unable to load `" << mf << "' module. dlerror(): "
                << errTxt << std::endl;
            ++nFailures;
            // look up for the "undefined symbol" error in error to demangle
            // it. According to practical experience, this is most common
            // reason of failure (undefined method, vtable, etc)
            // TODO: `regex_search()` with `for()` loop is redundant here as
            // `dlerror()' typically returns only one of the undefined symbol
            // names...
            std::smatch matches;
            //auto m = std::regex_match(errTxt, _rxUndefineSymbol);
            if(std::regex_search(errTxt, matches, _rxUndefineSymbol)) {
                oss << " ... where mangled undefined symbol(s) are: ";
                for(size_t i = 1; i < matches.size(); i += 2) {
                    if( i > 1 ) oss << ", ";
                    oss << "\033[0;31m"
                        << demangle_cpp(matches[i].str().c_str())
                        << "\033[0m"
                        ;
                }
                oss << std::endl;
            }
        } else {
            if(!quiet)
                std::cout << "\033[0;34mInfo\033[0m: Module \"" << mf << "\" has been loaded." << std::endl;
        }
    }
    if( !quiet ) {
        std::cerr << oss.str();
    }
    return nFailures;
}

#if 0
void
setup_default_calibration_indexes(
                    std::unordered_map<std::string, na64dp::calib::YAMLCalibInfoCtr> & ctrs,
                    na64dp::calib::GenericLoader & genericLoader,
                    na64dp::calib::Manager & mgr ) {
    {   // Naming calibrations
        using na64dp::nameutils::DetectorNaming;
        // - manually allocate the simple YAML data index instance
        //   parameterised with `DetectorNaming` type
        auto namesIndex = new na64dp::calib::SimpleYAMLDataIndex<DetectorNaming>(
                na64dp::calib::mappings_from_yaml_API02);
        // - create the "constructor entry" structure to be called by config
        //   parser
        auto namesKey = na64dp::calib::Dispatcher::info_id<DetectorNaming>("default");
        na64dp::calib::YAMLCalibInfoCtr nmCtr { namesKey
                                              , namesIndex
                                              , nullptr
                                              };
        // - emplace "constructor entry" to "constructor entries list"
        ctrs.emplace( "names", nmCtr );
        // - emplace the data index entry into generic constructor; do it
        //   manually as externally-created objects won't be automatically
        //   bound to generic loader
        genericLoader.add_data_index( namesKey, namesIndex );
    }

    {   // Pedestals CSV parsers
        // TODO: move it to handler as this is rarely needed
        using na64dp::calib::Pedestals;
        // - manually instantiate the pedestals data index
        auto pedestalsIndex = new na64dp::calib::PedestalsCSVDataIndex( mgr );
        // - create the "constructor entry" structure
        auto pedestalsKey = na64dp::calib::Dispatcher::info_id<Pedestals>("default");
        na64dp::calib::YAMLCalibInfoCtr pdsCtr { pedestalsKey
                                               , pedestalsIndex
                                               , nullptr
                                               };
        ctrs.emplace( "SADCPedestals", pdsCtr );
        // - manually bound created data index with generic loader
        genericLoader.add_data_index( pedestalsKey, pedestalsIndex );
    }
}
#endif

void
setup_default_calibration( na64dp::calib::Manager & mgr
                         , const std::string & cfgURI
                         ) {
    auto & aliases = calib::CIDataAliases::self();
    aliases.init_logging();
    auto & L = log4cpp::Category::getInstance("calib.main");
    // Obtain and load configuration
    auto paths = util::expand_names(cfgURI);
    if( paths.size() != 1 || paths[0].empty() ) {
        NA64DP_RUNTIME_ERROR( "Bad calibration config URI: %s", cfgURI.c_str() );
    }
    YAML::Node root = YAML::LoadFile( paths[0] );

    // NOTE: for flexibility, we use aliases defined in YAML node of
    // `calibrations.yaml' instead of default ones provided by SDC. Though
    // this leads to potential divergence of periodization in pure SDC
    // and na64sw it significantly increases flexibility of na64sw.
    calib::Periods::self().add_from_YAML_node(root["periods"]);

    // Add common, framework-wide calibration data type aliases
    aliases.add_alias_of<na64dp::nameutils::DetectorNaming>("naming", "default");
    aliases.add_alias_of<na64dp::EventBitTags>("eventTags", "default");
    // Add common loaders
    // - add generic frequently-used calibration data loaders
    auto yamlDocLoader = std::make_shared<na64dp::calib::YAMLDocumentLoader>();
    mgr.add_loader( "yaml-doc", yamlDocLoader );
    // -- define a naming calibration conversion function to "yaml-doc" loader
    yamlDocLoader
        ->define_conversion( "naming"
                           , na64dp::calib::YAML2Naming );
    // -- define semantical bits conversion
    yamlDocLoader
        ->define_conversion( "eventTags"
                           , na64dp::calib::YAML2EventTags );
    // -- define a "MasterTime" conversion function to "yaml-doc" loader
    aliases.add_alias_of< std::pair<std::string, float> >("masterTimeSource", "physics");
    yamlDocLoader
        ->define_conversion( "masterTimeSource"
                           , na64dp::calib::dispatch_master_time_setting);
    aliases.add_dependency("masterTimeSource", "naming");
    // ... (other common data conversions)
    // - add CSV loader
    //mgr.add_loader( std::make_shared<na64dp::calib::CSVLoader>() );  // TODO
    // Add common indexes (must be after loaders as index sometimes refers to
    // a particular loader that must be created)
    { // - add the index provided by YAML file
        auto yamlIndex = std::make_shared<na64dp::calib::YAMLIndex>();
        try {
            // Fill the index up from YAML file (at this point, type aliases are
            // resolved into particular types, and recommended loader names are
            // addressed to the loaders-by-name map).
            yamlIndex->add_records( root["standaloneDocuments"]
                                  , aliases.type_id_by_name()
                                  , mgr.loaders_by_name()
                                  );
        } catch( std::exception & e ) {
            L.fatal( "Failed to configure calibration manager from document \"%s\"."
                      , cfgURI.c_str() );
            throw;
        }
        mgr.add_index(yamlIndex);
    }
    { // Add SDC index & loader
        //p348reco::Aliases periodAliases;
        //p348reco::append_condb_aliases(periodAliases);
        // ^^^ NOTE: see notes about periods aliasing above
        // Create SDC wrapper
        auto sdcWrapper = std::make_shared<na64dp::calib::SDCWrapper>(
                    root["SDC"], calib::Periods::self() );

        // Retrieve types provided by wrapped SDC API, assure all aliased, warn
        // user otherwise -- for run number and datetime
        const auto & dict = aliases.type_id_by_name();
        for( const auto & p : sdcWrapper->sdc::Documents<na64dp::EventID>::validityIndex.entries() ) {
            const std::string & typeName = p.first;
            if( dict.find(typeName) == dict.end() ) {
                L.debug( "Calibration data type provided by SDC is not aliased"
                         " within na64sw: \"%s\", indexed by run number."
                       , typeName.c_str() );
            }
        }
        for( const auto & p : sdcWrapper->sdc::Documents<p348reco::Datetime_t>::validityIndex.entries() ) {
            const std::string & typeName = p.first;
            if( dict.find(typeName) == dict.end() ) {
                L.debug( "Calibration data type provided by SDC is not aliased"
                         " within na64sw: \"%s\", indexed by date and time."
                       , typeName.c_str() );
            }
        }

        mgr.add_index(sdcWrapper);
        mgr.add_loader("sdc", sdcWrapper);

        #if 0
        sdcWrapper->enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVChannelEntry>();
        sdcWrapper->enable_sdc_type_conversion<p348reco::Datetime_t, na64dp::calib::APVTimingCalib>();
        na64dp::calib::CIDataAliases::self()
                .add_dependency("GEMMonitor/APVPedestals", "naming");
        na64dp::calib::CIDataAliases::self()
                .add_dependency("GEMMonitor/APVTiming",    "naming");
        #endif

        aliases.add_dependency("p348reco/CaloCellCalib",  "naming");
        aliases.add_dependency("placements", "naming");
        // ... aliases.add_dependency()
    }
}

}
}

