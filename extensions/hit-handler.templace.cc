#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"

namespace na64dp {
namespace handlers {

/**\brief ...
 *
 * \code{.yaml}
 *     - _type: MyHitHandler
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * */
class MyHitHandler : public AbstractHitHandler<event::SADCHit> {
protected:
    // ... put here properties of your handler
public:
    /// ... features of the ctr, if any
    MyHitHandler( calib::Dispatcher & cdsp
                , const std::string & selection
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class MyHitHandler


bool
MyHitHandler::process_hit(EventID, DetID, event::SADCHit & hit) {
    // ...
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MyHitHandler, cdsp, cfg
                , "..."
                ) {
    using namespace na64dp;
    return new handlers::MyHitHandler( cdsp
            , aux::retrieve_det_selection(cfg)
            // ... your additional arguments retrieved from `cfg'
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

