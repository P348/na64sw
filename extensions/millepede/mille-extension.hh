#pragma once

#ifndef NA64SW_MILLEPEDE_EXTENSION_LOG_CATEGORY
#   define NA64SW_MILLEPEDE_EXTENSION_LOG_CATEGORY "extensions.mille"
#endif

#ifndef NA64SW_MILLEPEDE_II_PTYPE
#   define NA64SW_MILLEPEDE_II_PTYPE float
#endif

#include "na64app/extension.hh"
#include "na64detID/detectorID.hh"
#include "na64util/pair-hash.hh"

#include <Mille.h>
#include <log4cpp/Category.hh>

#include <unordered_set>

namespace na64dp {
namespace aux {

///\brief Thin srapper around native `Mille` instance
///
/// Provides registry for global derivatives (unique labels per score).
class MilleCollector {
public:
    /// Parameter datum type
    typedef NA64SW_MILLEPEDE_II_PTYPE Param_t;
    /// Maps {det.ID, getterNumber} key pair into {mille.label, verb.name}
    typedef std::unordered_map< std::pair<DetID, size_t>
                              , std::pair<int, std::string>
                              , util::PairHash
                              > DataProviderEntry;
private:
    log4cpp::Category & _L;  ///< ref to logging category
    Mille * _millePtr;  ///< bound instance of native Mille
    int _lastLabel;
    /// Index of known subscribers
    std::unordered_map<const void *, DataProviderEntry> _dataProviders;
    std::string _outputSemanticsFilePath;
public:
    MilleCollector( const std::string & outputFilePath
                  , const std::string & outputSemanticsFilePath
                  , log4cpp::Category & L
                  , bool asBinary=true
                  , bool writeZero=false
                  );
    ~MilleCollector();

    int register_global_derivative(const void *, DetID_t, size_t, const std::string &);

    ///\brief Returns unique label for n-th global parameter derivative of certain
    ///       detector of certain user
    ///
    /// Idempotent function of arguments provides consistent labels for global
    /// parameters.
    const std::pair<int, std::string> &
        get_label_for(const void * userPtr, DetID detID, size_t nParameter) const;

    Mille & native_object() { assert(_millePtr); return *_millePtr; }
};

}  // namespace ::na64dp::aux

/**\brief Runtime extension maintaining set of `Mille` objects.
 *
 * `Mille` interface is used to fill the file with local and global
 * derivatives after track fitting. These data form an essential input for
 * Millepede II alignment procedure done by external routine (called `pede`,
 * should be supplied within their major package).
 *
 * This extension maintains standard lifecycle of `Mille` objects: creation,
 * parameterised with output filename and its switches and
 * destruction. Filling should be done by dedicated handler instance(s).
 * Extension may treat runtime parameters which name are provided in form
 * `<name>.<parameter>=<value>` where:
 *
 *  * `<name>` is the name of `Mille` instance (should match handler's
 *    `"instance"` parameter). Use "default" for default instance (used by
 *    handlers by default).
 *  * `<parameter>` -- one of the (few) parameter names
 *  * `<value>` -- value defined for `Mille` instance
 *
 * Permitted parameters:
 *
 *  * `data-file-path` -- path to the output file used by `Mille` to write
 *      output. By default an eponymous to the instance is used + "-mille.dat"
 *      suffix (i.e. for instance, for "default" it is "default-mille.dat").
 *  * `semantics-file-path` -- path to ASCII file containing global derivative
 *      semantics. Used to prepare steering file for `Pede`. By default an
 *      eponymous to the instance is used + "-mille-semantics.txt"
 *      suffix (i.e. for instance, for "default" it is
 *      "default-mille-semantics.txt").
 *  * `asBinary` -- if set, makes `Mille` to generate text files instead of
 *      binary
 *  * `writeZero` -- makes `Mille` to store zero values
 *
 * For instance pipeline launch with
 * `-EMilleExtension.default.filepath=alignment.dat` will write default `Mille`
 * instance's output to `alignment.dat` file.
 *
 * \note One would doubtly need more than one `Mille` instance in the workflow,
 *       however to facilitate possible future parallelism API foresees it. The
 *       files are combined easily within `pede`'s steering file.
 * \note Logic options are considered using `util::str_to_bool()`
 * \see runtime-extension-parameters
 * */
class MilleExtension
        : public iRuntimeExtension
        , protected std::unordered_map<std::string, aux::MilleCollector *>
        {
public:
    struct InstanceParameters {
        std::string dataFilePath, semanticsFilePath;
        bool asBinary, writeZero;
        InstanceParameters() : dataFilePath("")
                             , asBinary(true)
                             , writeZero(false)
                             {}
    };
protected:
    std::unordered_map<std::string, InstanceParameters> _parameters;
    void init(calib::Manager &, iEvProcInfo * epi) override;
    void finalize() override;
    void set_parameter(const std::string &, const std::string &) override;
public:
    /// Returns `Mille` instance by name
    aux::MilleCollector & get_collector(const std::string &);
};

}  // namespace ::na64dp


