# Millepede II integration extension

This dir provides integration with
[Millepede II](https://www.desy.de/~kleinwrt/MP2/doc/html/index.html) used
for automated alignment workflow.

First, a handler is implemented in file `mille.cc`. This handler is used to
create input files for `pede` application and should be configured in a
standard way within an event processing pipeline.

