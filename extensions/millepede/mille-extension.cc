#include "mille-extension.hh"

#include <cstring>

namespace na64dp {
namespace aux {

MilleCollector::MilleCollector( const std::string & outputFilePath
              , const std::string & outputSemanticsFilePath
              , log4cpp::Category & L
              , bool asBinary
              , bool writeZero
              )
    : _L(L)
    , _millePtr(new Mille(outputFilePath.c_str(), asBinary, writeZero))
    , _lastLabel(1)
    , _outputSemanticsFilePath(outputSemanticsFilePath)
    {}

MilleCollector::~MilleCollector() {
    {  // write semantic file
        std::map<int, std::string> gdpIndex;  // note: sorted
        for(const auto & [providerPtr, dpe] : _dataProviders) {
            for(const auto & [idPair, parPair] : dpe) {
                gdpIndex.emplace(parPair.first, parPair.second);
            }
        }
        std::ofstream ofs(_outputSemanticsFilePath);
        for(const auto & [index, name] : gdpIndex) {
            ofs << "\t" << index << "\t" << name << std::endl;
        }
    }
    // make Mille write the file by deleting its instance
    if(_millePtr) delete _millePtr;
    _millePtr = nullptr;
}

int
MilleCollector::register_global_derivative(
          const void * userPtr, DetID_t detID
        , size_t nPar, const std::string & verbName ) {
    // get/create data provider entry
    auto userIr = _dataProviders.emplace(userPtr, DataProviderEntry());
    auto & dataProviderEntry = userIr.first->second;
    // insert/retrieve global derivative entry
    auto itemIr = dataProviderEntry.emplace(
            std::pair<DetID, size_t>(detID, nPar)
          , std::pair<int, std::string>(_lastLabel, verbName) );
    // return mille.label
    if(itemIr.second) {
        ++_lastLabel;
        return _lastLabel - 1;
    } else {
        return itemIr.first->second.first;
    }
}

const std::pair<int, std::string> &
MilleCollector::get_label_for( const void * userPtr
                             , DetID detID
                             , size_t nParameter
                             ) const {
    auto userIt = _dataProviders.find(userPtr);
    if(_dataProviders.end() == userIt) {
        NA64DP_RUNTIME_ERROR("No known data provider %p for Mille data sink."
                , userPtr );
    }
    const auto & entries = userIt->second;
    auto it = entries.find(std::pair<DetID, size_t>(detID, nParameter));
    if(entries.end() == it) {
        NA64DP_RUNTIME_ERROR("No known global derivative for detID=%d,"
                " nParameter=%zu", (int) detID.id, nParameter );
    }
    return it->second;
}

}  // namespace ::na64dp::aux

void
MilleExtension::init(calib::Manager &, iEvProcInfo * epi) {
    // ...
}

void
MilleExtension::finalize() {
    for(auto & p: *this) {
        if(!p.second) continue;
        // calling `Mille::~Mille()` causes it to finalize file write.
        delete p.second;
    }
}

void
MilleExtension::set_parameter( const std::string & pName
                             , const std::string & pValue
                             ) {
    auto & L = log4cpp::Category::getInstance(NA64SW_MILLEPEDE_EXTENSION_LOG_CATEGORY);
    // Expected format of arguments is <name>.<parameter>=<value>
    size_t n = pName.rfind('.');
    if(std::string::npos == n) {
        NA64DP_RUNTIME_ERROR("Invalid MilleExtension's parameter name \"%s\""
                " povided (failed to find delimiter).", pName.c_str() );
    }
    const std::string instanceName = pName.substr(0, n)
                    , parameterName = pName.substr(n+1)
                    ;
    auto ir = _parameters.emplace(instanceName, InstanceParameters());
    InstanceParameters & instanceParameters = ir.first->second;
    if(parameterName == "data-file-path") {
        instanceParameters.dataFilePath = pValue;
        L << log4cpp::Priority::DEBUG
          << "Data file path for mille instance \"" << instanceName << "\" has"
          " been set to \"" << pValue << "\".";
    } else if(parameterName == "semantics-file-path") {
        instanceParameters.semanticsFilePath = pValue;
        L << log4cpp::Priority::DEBUG
          << "Semantics file path for mille instance \"" << instanceName << "\" has"
          " been set to \"" << pValue << "\".";
    } else if(parameterName == "asBinary") {
        instanceParameters.asBinary = util::str_to_bool(pValue);
        L << log4cpp::Priority::DEBUG
          << "Output for \"" << instanceName << "\" has"
          " been set to " << (instanceParameters.asBinary ? "binary" : "text") << " format.";
    } else if(parameterName == "writeZero") {
        instanceParameters.writeZero = util::str_to_bool(pValue);
        L << log4cpp::Priority::DEBUG
          << "Zero suppression for \"" << instanceName << "\" has"
          " been " << (instanceParameters.asBinary ? "dis" : "en") << "abled.";
    } else {
        NA64DP_RUNTIME_ERROR("Unknown parameter name \"%s\" for"
                " MilleExtension (\"%s\")", parameterName.c_str()
                , pName.c_str() );
    }
}

aux::MilleCollector &
MilleExtension::get_collector(const std::string & name_) {
    auto & L = log4cpp::Category::getInstance(NA64SW_MILLEPEDE_EXTENSION_LOG_CATEGORY);
    auto it = this->emplace(name_, nullptr).first;
    if(nullptr == it->second) {
        std::string name = name_;
        if(name.empty()) name = "default";
        InstanceParameters p; {
            auto it = _parameters.find(name);
            if(it != _parameters.end()) p = it->second;
        }
        L << log4cpp::Priority::INFO << "Creating Mille instance `" << name
            << "' with parameters: out.file=\""
            << p.dataFilePath << "\", asBinary="
            << (p.asBinary ? "yes" : "no") << ", writeZero="
            << (p.writeZero ? "yes" : "no") << "."
            ;
        it->second = new aux::MilleCollector(
                          ( p.dataFilePath.empty()
                          ? name + "-mille.dat"
                          : p.dataFilePath
                          )
                        , ( p.semanticsFilePath.empty()
                          ? name + "-mille-semantics.txt"
                          : p.semanticsFilePath
                          )
                        , log4cpp::Category::getInstance(NA64SW_MILLEPEDE_EXTENSION_LOG_CATEGORY "." + name)
                        , p.asBinary
                        , p.writeZero
                        );
    }
    return *(it->second);
}

REGISTER_EXTENSION(MilleExtension);

}  // namespace ::na64dp

