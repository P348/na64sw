#include "mille-extension.hh"

#include "na64dp/abstractHitHandler.hh"
#include "na64dp/extendedTrackScoreGetters.hh"

namespace na64dp {
namespace handlers {

/**\brief Interface to `Mille` part of `Millepede II` alignment package
 *
 * This handler extracts values from track score and dispatches them to `Mille`
 * instance maintained by extension object using native `Mille` file writer.
 * `Mille` need the parameters for every score (within a track) to being
 * distributed across following semantics:
 * 
 *  * gloabal derivatives (multiple per score)
 *  * local derivatives (multiple per score)
 *  * residual (one per score)
 *  * sigma (one per score)
 *
 * Global derivatives have to be labeled with certain integer globally.
 *
 * This class maintains set of getters that can be assigned for certain
 * detector or detector group by means of DSuL.
 *
 * \todo Document usage
 * \todo Invalidate cache with placements invalidation
 * */
class MilleDataProvider
        : public AbstractHitHandler<event::Track>
        , public ExtendedTrackScoreGetters
        {
public:
    /// Parameter data type; should match the one used by `Mille` classes
    typedef aux::MilleCollector::Param_t MilleValue_t;
    /// Set of getters defined for certain detector
    struct RecordGetters {
        std::vector<std::pair<std::string, Getter>> globalDerivatives, localDerivatives;
        Getter getSigma, getResidual;
    };
    /// Type used to parameterize the constructor: a selector exresion and
    /// a list of resolved getters to apply
    struct GettersPerSelection {
        const std::string selectorExpression;
        DetSelect * selection;
        RecordGetters recordGetters;
    };
private:
    /// Per-placement item cache
    std::unordered_map<DetID, RecordGetters> _recordsGetterCache;
    /// Reentrant continers for derivs (to decrease reallocations)
    std::vector<MilleValue_t> _globalDrvValues, _localDrvValues;
    /// Reentrant continer for global derivs labels (to decrease reallocations)
    std::vector<int> _cGlobDrvLabels;
    MilleValue_t _cSigma, _cResidual;
    bool _permitNaN;
    /// Constant factor for (all) sigma values
    const Float_t _sigmaFactor;

    // stats
    size_t _nTracksDispatched
         , _nScoresDispatched
         , _nScoresDeclinedBecauseOfNaN
         ;
protected:
    /// Getters per selection, str-expr
    std::vector<GettersPerSelection> _gettersPerSelections;
    /// Reference to destination object
    aux::MilleCollector & _mille;

    /// Updates placements cache
    void handle_single_placement_update(const calib::Placement &) override;
    /// Updates selector expressions
    void handle_update(const nameutils::DetectorNaming &) override;
    /// Updates/returns record getters for certain detector ID item
    ///
    /// May return null pointer if no record matches selector(s)
    RecordGetters * _consider_detector(DetID id);

    void _copy_score_data( const RecordGetters & getters
                         , event::Track::const_iterator );
public:
    /** Constructs a handler in standard way, immediately instantiates
     * `Mille` object.
     *
     * \param cdsp calibration dispatcher instance to subscribe to
     * \param trackSelection track selection expression
     * \param logCat logging category in use
     * \param gettersPerSelections getters pack for certain selection
     * \param milleInstanceName name of the `Mille` destination object
     * \param detNameClass detector naming class in use
     * \param placementsClass placements info class in use
     * */
    MilleDataProvider( calib::Dispatcher & cdsp
                     , const std::string & trackSelection
                     , log4cpp::Category & logCat
                     , const std::list< std::pair<std::string, RecordGetters> > gettersPerSelections
                     , Float_t sigmaFactor=1
                     , const std::string & milleInstanceName="mille"
                     , const std::string & detNameClass="default"
                     , const std::string & placementsClass="default"
                     ) : AbstractHitHandler<event::Track>(cdsp, trackSelection, logCat, detNameClass)
                       , ExtendedTrackScoreGetters(
                               cdsp, logCat, detNameClass, placementsClass)
                       , _permitNaN(false)  // todo: parameterize?
                       , _sigmaFactor(sigmaFactor)
                       , _nTracksDispatched(0), _nScoresDispatched(0), _nScoresDeclinedBecauseOfNaN(0)
                       , _mille(Extensions::self()["MilleExtension"].as<MilleExtension>().get_collector(milleInstanceName))
                       //, _getters(localParamsList.begin(), localParamsList.end())
                       {
        std::transform(gettersPerSelections.begin(), gettersPerSelections.end()
                , std::back_inserter(_gettersPerSelections)
                , [](const std::pair<std::string, RecordGetters> & item) {
                    return GettersPerSelection{item.first, nullptr, item.second};
                } );
    }
    
    /// Overriden, to finalize the record after iterating the track
    bool process_hit(EventID, TrackID, event::Track &) override;

    void finalize() override;
};  // class Mille

void
MilleDataProvider::handle_update(const nameutils::DetectorNaming & nm) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    for( auto & item : _gettersPerSelections ) {
        // update selectors
        if(item.selectorExpression.empty()) {
            assert(!item.selection);
            item.selection = nullptr;
        } else {
            if(item.selection) delete item.selection;
            item.selection = new DetSelect( item.selectorExpression.c_str()
                                          , util::gDetIDGetters
                                          , nm );
        }
    }
    // forward to parent
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
}

MilleDataProvider::RecordGetters *
MilleDataProvider::_consider_detector(DetID did) {
    const std::string detName = naming()[did];
    // Every pair of <detector name, global derivative name> must be
    // registered at the destination `Mille` instance to define a
    // semantic match for Mille-label (int). To do this, first find a getter
    // that finally overrides the value:
    RecordGetters recordGetters;  // final (after overriding) getters for item
    bzero(&recordGetters.getResidual.cllb, sizeof(recordGetters.getResidual.cllb));
    bzero(&recordGetters.getSigma.cllb,    sizeof(recordGetters.getSigma.cllb));
    std::unordered_map<std::string, size_t> gdPosByName, ldPosByName;
    // resolve overriding
    bool hadMatch = false;
    for( const auto & sel : _gettersPerSelections ) {
        const RecordGetters * thisGettersPtr = nullptr;
        if(!sel.selection) {
            assert(sel.selectorExpression.empty());  // sel.expr. is empty for common quantity
            thisGettersPtr = &(sel.recordGetters);
        } else if( sel.selection->matches(did) ) {
            thisGettersPtr = &(sel.recordGetters);
        }
        if(!thisGettersPtr) {
            AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                << "Placement entry \"" << detName << "\" does not "
                   " match the selector \"" << sel.selectorExpression << "\".";
            continue;  // det does not match this selection
        }
        hadMatch = true;
        // selector matches -- combine (create/override) getters
        // -- append/override global getters
        for(const auto & [gdName, gdGetter] : thisGettersPtr->globalDerivatives) {
            auto it = gdPosByName.find(gdName);
            if(it == gdPosByName.end()) {
                // add new
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Global derivative getter " << detName << "/"
                    << gdName << " is defined at position "
                    << recordGetters.globalDerivatives.size()
                    << " as matching selector \"" << sel.selectorExpression << "\".";
                gdPosByName.emplace(gdName, recordGetters.globalDerivatives.size());
                recordGetters.globalDerivatives.push_back({gdName, gdGetter});
            } else {
                // override existing
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Global derivative getter " << detName << "/"
                    << gdName << " overrides eponymous getter at position "
                    << recordGetters.globalDerivatives.size()
                    << " as (this time) matching selector \"" << sel.selectorExpression << "\".";
                recordGetters.globalDerivatives[it->second].second = gdGetter;
            }
        }
        // -- "register" global derivatives obtaining label ids for 'em
        // (build rev cache)
        std::map<size_t, std::string> gdNameByPos;
        std::transform( gdPosByName.begin(), gdPosByName.end()
                      , std::inserter(gdNameByPos, gdNameByPos.end())
                      , [](auto p) { return std::pair<size_t, std::string>(p.second, p.first); }
                      );
        #ifndef NDEBUG
        size_t order = 0;
        #endif
        for(const auto & [gdPos, gdName] : gdNameByPos) {
            assert(order == gdPos);
            std::string globalParameterName = detName + "/" + gdName;
            int label = _mille.register_global_derivative( this, did, gdPos, 
                    globalParameterName );
            AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                << "Registered global parameter \""
                << globalParameterName << "\" by label " << label;
            #ifndef NDEBUG
            ++order;
            #endif
        }
        // -- append/override local getters
        for(const auto & [ldName, ldGetter] : thisGettersPtr->localDerivatives) {
            auto it = ldPosByName.find(ldName);
            if(it == ldPosByName.end()) {
                // add new
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Local derivative getter " << detName << "/"
                    << ldName << " is defined at position "
                    << recordGetters.localDerivatives.size()
                    << " as matching selector \"" << sel.selectorExpression << "\".";
                ldPosByName.emplace(ldName, recordGetters.localDerivatives.size());
                recordGetters.localDerivatives.push_back({ldName, ldGetter});
            } else {
                // override existing
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Local derivative getter " << detName << "/"
                    << ldName << " overrides eponymous getter at position "
                    << recordGetters.localDerivatives.size()
                    << " as (this time) matching selector \"" << sel.selectorExpression << "\".";
                recordGetters.localDerivatives[it->second].second = ldGetter;
            }
        }
        // -- set/override residual
        if(thisGettersPtr->getResidual) {
            if(recordGetters.getResidual) {
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Overriding residual getter on \"" << detName
                    << "\" as (this time) matching selector \""
                    << sel.selectorExpression << "\".";
            } else {
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Setting residual getter on \"" << detName
                    << "\" as matching selector \""
                    << sel.selectorExpression << "\".";
            }
            recordGetters.getResidual = thisGettersPtr->getResidual;
        }
        // -- set/override sigma
        if(thisGettersPtr->getSigma) {
            if(recordGetters.getSigma) {
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Overriding sigma (std.dev) getter on \"" << detName
                    << "\" as (this time) matching selector \""
                    << sel.selectorExpression << "\".";
            } else {
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Setting sigma (std.dev) getter on \"" << detName
                    << "\" as matching selector \""
                    << sel.selectorExpression << "\".";
            }
            recordGetters.getSigma = thisGettersPtr->getSigma;
        }
    }
    if(!hadMatch) {
        AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Placement item \"" << detName << "\" has not match, "
                       "no mille records will be produced."
                    ;
        return nullptr;
    }
    // check consistency 
    if(recordGetters.globalDerivatives.empty()) {
        NA64DP_RUNTIME_ERROR( "No global parameters defined for item \"%s\"."
                , detName.c_str());
    }
    // strictly speaking, this apparently must not be considered as error,
    // but Millepede refuses to handle data withut local parameters
    //if(recordGetters.localDerivatives.empty()) {
    //    NA64DP_RUNTIME_ERROR( "No local parameters defined for item \"%s\"."
    //            , detName.c_str());
    //}
    if(!recordGetters.getResidual) {
        NA64DP_RUNTIME_ERROR( "No residual getter defined for item \"%s\"."
                , detName.c_str());
    }
    if(!recordGetters.getSigma) {
        NA64DP_RUNTIME_ERROR( "No sigma getter defined for item \"%s\"."
                , detName.c_str());
    }
    // memorize
    auto iir = _recordsGetterCache.emplace(did, recordGetters);
    if(!iir.second) {
        AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Replacing getters set for \"" << detName << "\".";
        iir.first->second = recordGetters;
    }
    assert(iir.first != _recordsGetterCache.end());  // absurd...
    return &(iir.first->second);
}

void
MilleDataProvider::handle_single_placement_update(const calib::Placement & pl) {
    ExtendedTrackScoreGetters::handle_single_placement_update(pl);
    if(!pl.is_detector()) return;  // omit non-detector items
    DetID did = naming()[pl.name];
    _consider_detector(did);
}

void
MilleDataProvider::_copy_score_data(
              const RecordGetters & getters 
            , event::Track::const_iterator scoreIt ) {
    // retrieve globals
    if(!getters.globalDerivatives.empty()) {
        _globalDrvValues.resize( _globalDrvValues.size()
                               + getters.globalDerivatives.size()
                               , std::nan("0"));
        _cGlobDrvLabels.resize(getters.globalDerivatives.size(), -1);
        for( size_t nGD = 0; nGD < getters.globalDerivatives.size(); ++nGD ) {
            _globalDrvValues[nGD] = getters.globalDerivatives[nGD].second(
                    scoreIt->first, *scoreIt->second, *this);
            _cGlobDrvLabels[nGD]  = _mille.get_label_for(this, scoreIt->first, nGD).first;
        }
    }
    // retrieve locals
    if(!getters.localDerivatives.empty()) {
        _localDrvValues.resize(getters.localDerivatives.size(), std::nan("0"));
        for( size_t nLD = 0; nLD < getters.localDerivatives.size(); ++nLD ) {
            _localDrvValues[nLD] = getters.localDerivatives[nLD].second(
                    scoreIt->first, *scoreIt->second, *this);
        }
    }
    if(getters.getSigma) {
        if(!std::isnan(_cSigma)) {
            NA64DP_RUNTIME_ERROR("Conflicting getter for sigma (std.dev).");
        }
        _cSigma = _sigmaFactor * getters.getSigma(scoreIt->first, *scoreIt->second, *this);
    }
    if(getters.getResidual) {
        if(!std::isnan(_cResidual)) {
            NA64DP_RUNTIME_ERROR("Conflicting getter for residual value.");
        }
        _cResidual = getters.getResidual(scoreIt->first, *scoreIt->second, *this);
    }
}

bool
MilleDataProvider::process_hit( EventID eid
                              , TrackID trackID
                              , event::Track & track
                              ) {
    try {
        bool hadScore = false;
        for( auto scoreIt = track.begin(); track.end() != scoreIt; ++scoreIt ) {
            auto cachedRecordGettersIt = _recordsGetterCache.find(scoreIt->first);
            if(cachedRecordGettersIt == _recordsGetterCache.end()) {
                // todo: we may would like to decompose scores (or something)
                // for detector items that were not directly provided by
                // placements...
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Ignoring score "
                    << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                    << " as no record getters cached for it.";
                continue;  // no cache for detector
            }
            // reset reentrant containers
            _globalDrvValues.clear();
            _localDrvValues.clear();
            _cGlobDrvLabels.clear();
            _cSigma = _cResidual = std::nan("0");

            auto & recordGetters = cachedRecordGettersIt->second;
            try {
                _copy_score_data(recordGetters, scoreIt);
            } catch( std::exception & e ) {
                AbstractHitHandler<event::Track>::_log << log4cpp::Priority::ERROR
                    << "Error appeared on score for detector "
                    << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first] << ": "
                    << e.what();
                continue;
            }
            // NaN seem to be lethal for Millepede, so we omit such
            // records here
            if(!_permitNaN) {
                if(std::isnan(_cSigma)) {
                    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                        << "Omitting score "
                        << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                        << " because of sigma=NaN";
                    ++_nScoresDeclinedBecauseOfNaN;
                    continue;
                }
                if(std::isnan(_cResidual)) {
                    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                        << "Omitting score "
                        << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                        << " because of residual=NaN";
                    ++_nScoresDeclinedBecauseOfNaN;
                    continue;
                }
                bool hasNaNs = false;
                for(MilleValue_t v : _globalDrvValues) {
                    if(std::isnan(v)) { hasNaNs = true; break; }
                }
                if(hasNaNs) {
                    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                        << "Omitting score "
                        << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                        << " because of NaN(s) in global derivative(s)";
                    ++_nScoresDeclinedBecauseOfNaN;
                    continue;
                }
                for(MilleValue_t v : _localDrvValues) {
                    if(std::isnan(v)) { hasNaNs = true; break; }
                }
                if(hasNaNs) {
                    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                        << "Omitting score "
                        << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                        << " because of NaN(s) in local derivative(s)";
                    ++_nScoresDeclinedBecauseOfNaN;
                    continue;
                }
            }
            hadScore = true;
            // dispatch collected values
            assert(_globalDrvValues.size() <= std::numeric_limits<int>::max());
            assert(_localDrvValues.size()  <= std::numeric_limits<int>::max());
            AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
                    << "Dispatching residual=" << _cResidual
                    << ", sigma=" << _cSigma
                    << " from "
                    << calib::Handle<nameutils::DetectorNaming>::get()[scoreIt->first]
                    ;
            _mille.native_object().mille(
                      static_cast<int>( _localDrvValues.size()),  _localDrvValues.data()
                    , static_cast<int>(_globalDrvValues.size()), _globalDrvValues.data()
                    , _cGlobDrvLabels.data()
                    , _cResidual, _cSigma);
        }
        // finalize track record
        if(hadScore) {
            ++_nTracksDispatched;
            _nScoresDispatched += track.size();
            _mille.native_object().end();
        }
    } catch(std::exception & e) {
        // TODO: details on exception
        _mille.native_object().kill();
        throw;
    }
    return true;
}

void
MilleDataProvider::finalize() {
    if(_nTracksDispatched) {
        AbstractHitHandler<event::Track>::_log
            << log4cpp::Priority::DEBUG << _nScoresDispatched << " scores in "
            << _nTracksDispatched << " tracks were dispatched to a"
                " Mille instance (" << ((float) _nScoresDispatched/(float) _nTracksDispatched)
            << " score/track in average, "
            << _nScoresDeclinedBecauseOfNaN << " tracks were declined because"
               " of at least one NaN values in score(s)."
            ;
    } else {
        AbstractHitHandler<event::Track>::_log
            << log4cpp::Priority::WARN << "No records were dispatched to"
                " Mille instance, "
            << _nScoresDeclinedBecauseOfNaN << " scores were declined"
            " because of NaN";
    }
}


// 
// Extended (supp.) getters list and function to retrieve them by name



// config helpers
MilleDataProvider::Getter
_resolve_getter(const std::string & nm) {
    // try to resolve in extended getters
    auto getter = ExtendedTrackScoreGetters::get_extended_getter(nm);
    if(!getter) {
        // look for the getter in common (event's standard getters)
        // this will throw exception if getter is not found.
        getter.isPlain = true;
        getter.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(nm);
        assert(getter.cllb.plainGetter);
    }
    return getter;
}

static void
_retrieve_getters_from_YAML_node( 
          const YAML::Node & node
        , std::vector<std::pair<std::string, MilleDataProvider::Getter>> & dest
        , log4cpp::Category & L
        ) {
    for(const auto & p : node) {
        dest.push_back({ p.first.as<std::string>()
                       , _resolve_getter(p.second.as<std::string>())
                       });
        L << log4cpp::Priority::DEBUG << "\"" << dest.back().first
          << "\" derivative resolved as"
          << (dest.back().second ? " plain" : " extended") << " getter \""
          << p.second.as<std::string>()
          << "\""
          ;
    }
}

static MilleDataProvider::RecordGetters
_configure_record_getters(
          const YAML::Node & node
        , log4cpp::Category & L ) {
    MilleDataProvider::RecordGetters recordGettes;
    bzero(&recordGettes.getSigma.cllb,      sizeof(recordGettes.getSigma.cllb));
    bzero(&recordGettes.getResidual.cllb,   sizeof(recordGettes.getResidual.cllb));
    if(node["global"]) {
        L << log4cpp::Priority::DEBUG << "Resolving global derivative getters";
        _retrieve_getters_from_YAML_node( node["global"]
                                        , recordGettes.globalDerivatives
                                        , L
                                        );
    }
    if(node["local"]) {
        L << log4cpp::Priority::DEBUG << "Resolving local derivative getters";
        _retrieve_getters_from_YAML_node( node["local"]
                                        , recordGettes.localDerivatives
                                        , L
                                        );
    }
    if(node["sigma"]) {
        recordGettes.getSigma
                = _resolve_getter(node["sigma"].as<std::string>());
        L << log4cpp::Priority::DEBUG << "Sigma resolved as"
          << (recordGettes.getSigma.isPlain ? " plain" : " extended") << " getter \""
          << node["sigma"].as<std::string>() << "\""
          ;
    }
    if(node["residual"]) {
        recordGettes.getResidual
                = _resolve_getter(node["residual"].as<std::string>());
        L << log4cpp::Priority::DEBUG << "Residual resolved as"
          << (recordGettes.getResidual.isPlain ? " plain" : " extended") << " getter \""
          << node["residual"].as<std::string>() << "\""
          ;
    }
    L << log4cpp::Priority::DEBUG << "Done configuring getters: "
        << recordGettes.globalDerivatives.size() << " global derivatives, "
        << recordGettes.localDerivatives.size() << " local derivatives, sigma is"
        << (recordGettes.getSigma ? "" : " not") << " provided, residual is"
        << (recordGettes.getSigma ? "" : " not") << " provided."
        ;
    return recordGettes;
}

REGISTER_HANDLER( Mille, cdsp, cfg_
        , "Interface to Millepede II data filling routine." ) {
    auto & L = aux::get_logging_cat(cfg_);
    std::list< std::pair<std::string, MilleDataProvider::RecordGetters> >
            gettersPerSelections;
    const YAML::Node & cfg = cfg_["quantities"];
    // Resolve common getters
    if(cfg["common"]) {
        L << log4cpp::Priority::DEBUG << "Configuring common handlers...";
        gettersPerSelections.push_back({"", _configure_record_getters(cfg["common"], L)});
    } else {
        L << log4cpp::Priority::DEBUG << "No common getters provided";
    }
    if(cfg["override"]) {
        const YAML::Node & overNode = cfg["override"];
        for(YAML::const_iterator it = overNode.begin(); it != overNode.end(); ++it) {
            const YAML::Node & thisNode = *it;
            std::string selection;
            if(thisNode["applyTo"]) {
                selection = thisNode["applyTo"].as<std::string>();
            }
            L << log4cpp::Priority::DEBUG << "Configuring handlers for selection\""
                << selection << "\"...";
            gettersPerSelections.push_back({selection, _configure_record_getters(thisNode, L)});
        }
    } else {
        L << log4cpp::Priority::DEBUG << "No overriding getters provided.";
    }
    return new MilleDataProvider( cdsp
                , aux::retrieve_det_selection(cfg_)
                , L
                , gettersPerSelections
                , cfg_["sigmaFactor"] ? cfg_["sigmaFactor"].as<Float_t>() : 1.
                , cfg_["instance"] ? cfg_["instance"].as<std::string>() : "default"
                , aux::get_naming_class(cfg_) 
                //, aux::get_placement_name() ?
                );
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

