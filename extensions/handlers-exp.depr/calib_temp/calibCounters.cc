#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing SADC hits of beam counters.
 *
 * Proccesses information from SADC chips of beam counters
 * and applies calibrations
 *
 * */
class CalibCount : public AbstractHitHandler<event::SADCHit> {
	
public:
    struct CountCalib {
        const char * planeName;
        double energy;
        double time;
        double timeError;
    };
    
    struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t sKinCode;
        DetKin_t vKinCode;
        DetKin_t tKinCode;
    };
	
private:
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID, CountCalib *> _calibs;
    
protected:
	
	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
public:
    CalibCount( calib::Dispatcher & dsp
              , const std::string & only )
              : AbstractHitHandler<event::SADCHit>(dsp, only) {}
                            
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit &) override;
};

static CalibCount::CountCalib gCountCalibs[] = {
	
	// Energy deposition, tmean, tsigma

    {"S2:0-0--", 1,  -37, 3.2},
    {"S3:0-0--", 1,  -37, 3.2},
    {"S4:0-0--", 0.00023 / 130. , -37.  , 4 } ,
	{"V1:0-0--", 1,  -37, 3.2},
    {"V2:0-0--", 0.0075 / 1250. , -51.8 , 4 } ,
    {"T2:0-0--", 1 , 210.  , 6.6 },
	
};

void
CalibCount::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::SADCHit>::handle_update(nm);
    
    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.sKinCode = nm.kin_id("S").second;
    _namingCache.vKinCode = nm.kin_id("V").second;
    _namingCache.tKinCode = nm.kin_id("T").second;

    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gCountCalibs)/sizeof(CountCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID did = nm.id(gCountCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gCountCalibs + i);
    }
}

bool
CalibCount::process_hit( EventID
					   , DetID did
                       , event::SADCHit & hit ) {
	
	// Process counters hits and calibrate energy deposition
	
	if ( _namingCache.sKinCode == did.kin()
	  || _namingCache.vKinCode == did.kin()
	  || _namingCache.tKinCode == did.kin() ) {
		
		const CountCalib * cCalibs = _calibs[did];
		
		double bin(12.5);
				
		if ( _namingCache.tKinCode == did.kin() ) {	
		
			double maxValue(std::numeric_limits<double>::min());
			double maxSample(std::numeric_limits<int>::min());
			
			//for ( auto & it : hit.maxima ) {
            for( const uint8_t * idx = hit.rawData->maxima
               ; std::numeric_limits<uint8_t>::max() != *idx
               ; ++idx
               ) {
						
				double energy = hit.rawData->wave[*idx] * cCalibs->energy;
				
				if ( hit.rawData->wave[*idx] > maxValue ) {
					maxSample = *idx;
					maxValue = hit.rawData->wave[*idx];
				}
			}
			// Master time in ns
			double & master = _current_event().masterTime;
			master = maxSample * bin;
			
			#if 0
			std::cout << naming()[did] << std::endl;
			std::cout << "Hit time: " << master << ", "
					  << "hit eDep: " << maxValue * cCalibs->energy << std::endl;
			std::cout << _current_event().masterTime << std::endl;
			#endif

		}
	}
	return true;
}

}

REGISTER_HANDLER( CalibCount, ch, yamlNode
                , "Handler for counter time and energy calibration" ) {
    
    return new handlers::CalibCount( ch
								   , aux::retrieve_det_selection(yamlNode) );
};
}
