#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/// Pretty stupid ad-hoc handler to plot time correlations in an event
/// Depicts time coming from certain BMS plane vs time reconstructed on one of
/// MuMegas
class TimeCorrelationPlot : public AbstractHitHandler<event::F1Hit> {
public:
    TimeCorrelationPlot();

    //ProcRes process_event( event::Event & ) override;
    bool process_hit(EventID eid, HitKey, event::F1Hit &) override {

    }
};

}
}

REGISTER_HANDLER( TimeCorr, mgr, cfg
                , "(dev) ..." ) {
    return new na64dp::handlers::TimeCorrelationPlot();
}

