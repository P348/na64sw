#include "calibration/calibCounters.hh"

namespace na64dp {
namespace handlers {

static CalibCount::CountCalib gCountCalibs[] = {
	
	// Energy deposition, tmean, tsigma

    {"S2:0-0--", 1,  -37, 3.2},
    {"S3:0-0--", 1,  -37, 3.2},
    {"S4:0-0--", 0.00023 / 130. , -37.  , 4 } ,
	{"V1:0-0--", 1,  -37, 3.2},
    {"V2:0-0--", 0.0075 / 1250. , -51.8 , 4 } ,
    {"T2:0-0--", 1 , 210.  , 6.6 },
	
};

CalibCount::CalibCount( calib::Dispatcher & ch
                      , const std::string & only
				      ) : AbstractHitHandler<SADCHit>(ch, only) {
	
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
CalibCount::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);
    
    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.sKinCode = nm.kin_id("S").second;
    _namingCache.vKinCode = nm.kin_id("V").second;
    _namingCache.tKinCode = nm.kin_id("T").second;

    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gCountCalibs)/sizeof(CountCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gCountCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gCountCalibs + i);
    }
}

bool
CalibCount::process_hit( EventID
					   , DetID_t did
                       , SADCHit & hit ) {
	
	// Process counters hits and calibrate energy deposition
	
	DetID stationID(did);

	if ( _namingCache.sKinCode == stationID.kin()
	  || _namingCache.vKinCode == stationID.kin()
	  || _namingCache.tKinCode == stationID.kin() ) {
		
		const CountCalib * cCalibs = _calibs[did];
		
		double bin(12.5);
				
		if ( _namingCache.tKinCode == stationID.kin() ) {	
		
			double maxValue(std::numeric_limits<double>::min());
			double maxSample(std::numeric_limits<int>::min());
			
			for ( auto & it : hit.maxima ) {
						
				double energy = it.second * cCalibs->energy;
				
				if ( it.second > maxValue ) {
					maxSample = it.first;
					maxValue = it.second;
				}
			}
			// Master time in ns
			double & master = _current_event().masterTime;
			master = maxSample * bin;
			
			#if 1
			std::cout << naming()[did] << std::endl;
			std::cout << "Hit time: " << master << ", "
					  << "hit eDep: " << maxValue * cCalibs->energy << std::endl;
			std::cout << _current_event().masterTime << std::endl;
			#endif

		}
	}
	return true;
}

}

REGISTER_HANDLER( CalibCount, banks, ch, yamlNode
                , "Handler for counter time and energy calibration" ) {
    
    return new handlers::CalibCount( ch
								   , aux::retrieve_det_selection(yamlNode) );
};
}
