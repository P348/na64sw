#include "calibration/calibSRD.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static CalibSRD::SRDCalib gSRDCalibs[] = {
	
	// Energy deposition, tmean, tsigma
	
	{ "SRD:0-0--", 0.042 / 1050, -78.3, 5.04 },
	{ "SRD:1-0--", 0.042 / 1070, -78.9, 4.68 },
	{ "SRD:2-0--", 0.042 / 1040, -77.2, 4.68 },
	
};

CalibSRD::CalibSRD( calib::Dispatcher & ch
                  , const std::string & only
				  ) : AbstractHitHandler<SADCHit>(ch, only) {
}

void
CalibSRD::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);
    
    try {
        _srdKinID = nm.kin_id("SRD").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find SRD kin ID in current name mappings (error: %s)."
                  , e.what() );
    }

    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gSRDCalibs)/sizeof(SRDCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gSRDCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gSRDCalibs + i);
    }
}

bool
CalibSRD::process_hit( EventID
					 , DetID_t did
                     , SADCHit & hit ) {
	
	// Process SRD hits and calibrate energy deposition
	
	double binTime(12.5);
	
	DetID stationID(did);
	
	if ( _srdKinID == stationID.kin() ) {
		
		const SRDCalib * cCalibs = _calibs[did];
		
		hit.eDep = hit.maxValue * cCalibs->energy / 1E-3;
		hit.time = hit.maxSample * binTime; // + cCalibs->time;
		hit.timeError = cCalibs->timeError;
		
		#if 0
		std::cout << naming()[did] << ", "
				  << "energy deposition: " << hit.eDep << " , "
				  << "hit time: " << hit.time << std::endl;
		
		#endif
		
	}
							   
	return true;
}

CalibSRD::ProcRes
CalibSRD::process_event(Event * evPtr) {
        
    AbstractHitHandler<SADCHit>::process_event( evPtr );
    
    return kOk;
}

}

REGISTER_HANDLER( CalibSRD, banks, ch, yamlNode
                , "Handler for SRD time and energy calibration" ) {
    
    return new handlers::CalibSRD( ch
								 , aux::retrieve_det_selection(yamlNode) );
};
}

#endif
