#include "tracking/apvPull.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include <EventDisplay.h>

// Geometry, material and magnetic field headers
#include "ConstFieldBox.h" // both have to included in somewhere in root of na64 analysis tool
#include "ConstFieldBox.cc"
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

// Tracking engine and track representatives
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <MeasuredStateOnPlane.h>
#include <MeasurementOnPlane.h>
#include <PlanarMeasurement.h>
#include <StateOnPlane.h>

// Kalman reference fitter routine and associated info
#include <AbsKalmanFitter.h>
#include <KalmanFitter.h>
#include <KalmanFitterInfo.h>
#include <KalmanFitterRefTrack.h>
#include <KalmanFitStatus.h>
#include <KalmanFittedStateOnPlane.h>

// Root dependencies 
#include <TApplication.h>
#include <TCanvas.h>
#include <TDatabasePDG.h>
#include <TEveManager.h>
#include <TGeoManager.h>
#include <TH2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TVector3.h>
#include <vector>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TString.h>

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static APVPull::DetPlacementEntry gPlacements[] = {
    /*
    // Placement for 2018_invis100
    {"MM1", -1,       0,      -1967.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ
    {"MM2",  2.1378, -0.3564, -1820.1,  0,  0,  45,   8.,  8.,  0.01, 0.02, 320}, // resolution, strip specific number of wires
    {"MM3", -31.79,  -0.2048, -364.6,   0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    {"MM4", -32.694, -1.9702, -345.2,   0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    {"MM5", -37.66,  -0.5381, -119.1,   0,  0,  45,   8.,  8.,  0.01, 0.02, 320},    
    {"MM6", -37.86,  -1.5531, -97.9,    0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    */
    
    // Placement for 2018_vis150
    {"MM1", -1,       0,      -2594.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ
    {"MM2", -1.+0.44, -0.5381, -2558.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // resolution, strip specific number of wires
    {"MM3", 1.7+0.4378, -0.3564, -2484.6,   0,  0, 45,   8.,  8.,  0.01, 0.02, 320},
    {"MM4", 1.7+0.4378, -0.3564, -2447.9,   0,  0, 45,   8.,  8.,  0.01, 0.02, 320},
    {"MM5", -21.7+4.46,  -2.048, -695,   0,  0,  -45,   8.,  8.,  0.01, 0.02, 320},    
    {"MM6", -22+3.806,  -1.07019, -675,    0,  0, -45,   8.,  8.,  0.01, 0.02, 320},   
    
};

APVPull::APVPull( calib::Dispatcher & cdsp
                              , const std::string & only
                              , const std::string & geoFilePath
                              , int minIts, int maxIts, int minHits)        
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _cFitter(nullptr)
        , _cRep(nullptr)
        , _cTrack(nullptr)
        , _cState(nullptr)
        , _cStateRefOrig(nullptr)
        , _gEvent(0)
        , _nOfApvHits(0)
        , _momentum(150)
        , _minHits(minHits)
    {

    // init geometry and mag. field
    new TGeoManager( "Geometry", "NA64 geometry");

    TGeoManager::Import( geoFilePath.c_str() );
    // ^^^ TODO: how to check whether the geometry was, indeed, imported?
    log().debug( "Geometry read from file \"%s\" into instance %p.", geoFilePath.c_str(), gGeoManager );
    assert(gGeoManager);
    
    //Init_Geometry_2018_vis
    //genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 17.2, 0.0, -100, 100, -100, 100, -1746.6, -1346.6));
    genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 17.2, 0.0, -100, 100, -100, 100, -2411.1, -2011.1));
    
    genfit::MaterialEffects::getInstance()->init( new genfit::TGeoMaterialInterface() );
    genfit::MaterialEffects::getInstance()->setNoEffects();
    
    // init fitter
    _cFitter = new genfit::KalmanFitterRefTrack();
    _cFitter->setMinIterations( minIts );
    _cFitter->setMaxIterations( maxIts );
    
    genfit::EventDisplay::getInstance();
    
    // create histograms
    gROOT->SetStyle("Plain");
    gStyle->SetPalette(1);
    gStyle->SetOptFit(1111);

    hPullMM1x = new TH1D("hPullMM1","u pull1",500, -5, 5);
    hPullMM2x = new TH1D("hPullMM2","u pull2",500, -5, 5);
    hPullMM3x = new TH1D("hPullMM3","u pull3",500, -5, 5);
    hPullMM4x = new TH1D("hPullMM4","u pull4",500, -5, 5);
    hPullMM5x = new TH1D("hPullMM5","u pull5",500, -5, 5);
    hPullMM6x = new TH1D("hPullMM6","u pull6",500, -5, 5);
    hPullMM1y = new TH1D("hPullMM1","v pull1",500, -5, 5);
    hPullMM2y = new TH1D("hPullMM2","v pull2",500, -5, 5);
    hPullMM3y = new TH1D("hPullMM3","v pull3",500, -5, 5);
    hPullMM4y = new TH1D("hPullMM4","v pull4",500, -5, 5);
    hPullMM5y = new TH1D("hPullMM5","v pull5",500, -5, 5);
    hPullMM6y = new TH1D("hPullMM6","v pull6",500, -5, 5);
}

void
APVPull::handle_update( const nameutils::DetectorNaming & nm ) {
    TDirAdapter::handle_update(nm);
    AbstractHitHandler<TrackPoint>::handle_update(nm);
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        char * postfix;
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        auto ir = _placements.emplace(did, gPlacements + i);
        if( !ir.second ) {
            //std::cerr << "Failed to insert placement for \"" << gPlacements[i].planeName << "\" (id repeated)." << std::endl;
        }
        else {
            std::cout << "Inserted placement for \"" << gPlacements[i].planeName << "\" id=" << ir.first->first << std::endl;
        }
    }
}

bool
APVPull::process_hit( EventID, DetID_t stationID, TrackPoint & cPoint) {
    
    auto it = _placements.find( stationID );
    std::string stationName = TDirAdapter::naming()[stationID];
    
    if( _placements.end() == it ) {
        //std::cout << "Unable to find station "  << stationName << std::endl;
        return true;
    }
    const DetPlacementEntry * cPlacement = _placements[stationID];
        
    int detId(_nOfApvHits); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(2);

    hitCoords(0) = ((cPlacement->sizeX * cPoint.lR[0] / cPlacement->numOfWires) - (( cPlacement->sizeX )/2) ); // Simple conversion with respect to micromega 8 cm side
    hitCoords(1) = ((cPlacement->sizeY * cPoint.lR[1] / cPlacement->numOfWires) - (( cPlacement->sizeY )/2) );

    double resolution(cPlacement->resolution);
    TMatrixDSym hitCov(2);
    hitCov(0, 0) = resolution*resolution;
    hitCov(1, 1) = resolution*resolution;     
    
    TVector3 u_plane(1, 0, 0);
    TVector3 v_plane(0, 1, 0);

    if (cPlacement->rotAngleX != 0) {
        u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
    }

    if (cPlacement->rotAngleY != 0) {
        u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
    }
    
    if (cPlacement->rotAngleZ != 0) {
        u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
    }

    genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));

    genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
    

    _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            
    _cTrack->checkConsistency();
    
    _detNames.push_back(stationName);

    _retMeasurements.push_back(measurement);
    
    _planePtrs.push_back(plane);
    
    _nOfApvHits += 1;
    
    return true;
}

AbstractHandler::ProcRes
APVPull::process_event(Event * evPtr ) {

    gRandom->SetSeed(14);

    int resolution(1);
    // start values for the fit, e.g. from pattern recognition
    //TVector3 pos(0, 0, -2007.3);
    TVector3 pos(0, 0, -2600);
    TVector3 mom(0, 0, 1);
    mom.SetMag(_momentum);
    
    // initial covariance matrix
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i)
        covM(i,i) = resolution*resolution;
    for (int i = 3; i < 6; ++i)
        covM(i,i) = pow(resolution / 5 / sqrt(3), 2);  

    // Initiat TrackRep
    genfit::AbsTrackRep* _cRep = new genfit::RKTrackRep(-11);
    _cTrack = new genfit::Track(_cRep, pos, mom);
    #if 0
    genfit::AbsTrackRep* muonRep = new genfit::RKTrackRep(13); 
    genfit::AbsTrackRep* pionRep = new genfit::RKTrackRep(211); 
    genfit::AbsTrackRep* kaonRep = new genfit::RKTrackRep(321); 
    genfit::AbsTrackRep* protonRep = new genfit::RKTrackRep(2212); 
    
    _cTrack->addTrackRep(muonRep);
    _cTrack->addTrackRep(pionRep);
    _cTrack->addTrackRep(kaonRep);
    _cTrack->addTrackRep(protonRep);
    #endif
    // Create initial state with covariance and assign it to out first rep
    genfit::MeasuredStateOnPlane state(_cRep);
    _cRep->setPosMomCov(state, pos, mom, covM);
    
    // remember original state
    const genfit::StateOnPlane stateRefOrig(state);
    
    //Show that everything is defined correctly
    TVectorD initPos;
    TMatrixDSym initCov;
    _cRep->get6DStateCov(state, initPos, initCov);
    
    // Helper vectors for current position display
    TVectorD curPos;
    TMatrixDSym curCov;
    _cRep->get6DStateCov(state, curPos, curCov);
    
    /*
    // For testing purpose we will define optimal track for given field and
    for (unsigned int i = 0; i < 40; ++i) { 
        curPos.Print();
        _cRep->extrapolateBy(state, 50);
        _cRep->get6DStateCov(state, curPos, curCov);
    }

    _cRep->extrapolateToPlane(state, stateRefOrig.getPlane());
    */
    
    // end of testing cout; ///////////////////////////////////////////////

    //Start of hits processing...
    AbstractHitHandler<TrackPoint>::process_event(evPtr);
    
    
    //==================================================================
    // Assamble virtual planes to simulate geometry ====================
    //==================================================================

    int detId(_nOfApvHits); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(2);

    hitCoords(0) = 0;
    hitCoords(1) = 0;

    double resolutionGM(0.01);
    TMatrixDSym hitCov(2);
    hitCov(0, 0) = resolutionGM*resolutionGM;
    hitCov(1, 1) = resolutionGM*resolutionGM;
    

    // S1
    genfit::SharedPlanePtr planeS1(new genfit::DetPlane( TVector3(0,0,-2580), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S1 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S1)->setPlane(planeS1, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S1, _cTrack));
    _cTrack->checkConsistency();
    
    // S2
    genfit::SharedPlanePtr planeS2(new genfit::DetPlane( TVector3(0,0,-2560), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S2 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S2)->setPlane(planeS2, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S2, _cTrack));
    _cTrack->checkConsistency();
    
    // S3
    genfit::SharedPlanePtr planeS3(new genfit::DetPlane( TVector3(0,0,-2500), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S3 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S3)->setPlane(planeS3, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S3, _cTrack));
    _cTrack->checkConsistency();
    
    // S4
    genfit::SharedPlanePtr planeS4(new genfit::DetPlane( TVector3(0,0,-2490), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S4 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S4)->setPlane(planeS4, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S4, _cTrack));
    _cTrack->checkConsistency();
      
    // S5
    genfit::SharedPlanePtr planeS5(new genfit::DetPlane( TVector3(-0.000909225,0,-2400), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S5 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S5)->setPlane(planeS5, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S5, _cTrack));
    _cTrack->checkConsistency();
    
    // S6
    genfit::SharedPlanePtr planeS6(new genfit::DetPlane( TVector3(0,0,-2400), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S6 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S6)->setPlane(planeS6, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S6, _cTrack));
    _cTrack->checkConsistency();
    
    // S7
    genfit::SharedPlanePtr planeS7(new genfit::DetPlane( TVector3(-0.43243,0,-2250), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S7 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S7)->setPlane(planeS7, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S7, _cTrack));
    _cTrack->checkConsistency();
    
    // S8
    genfit::SharedPlanePtr planeS8(new genfit::DetPlane( TVector3(-12.6404,0,-1300.08), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S8 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S8)->setPlane(planeS8, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S8, _cTrack));
    _cTrack->checkConsistency();
    
    // S9
    genfit::SharedPlanePtr planeS9(new genfit::DetPlane( TVector3(-16.8186,0,-1000.11), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S9 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S9)->setPlane(planeS9, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S9, _cTrack));
    _cTrack->checkConsistency();
    
    // S10
    genfit::SharedPlanePtr planeS10(new genfit::DetPlane( TVector3(-20.9967,0,-700.14), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S10 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S10)->setPlane(planeS10, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S10, _cTrack));
    _cTrack->checkConsistency();
    
    // GM1
    genfit::SharedPlanePtr planeGM1(new genfit::DetPlane( TVector3(-23.7822, 0, -500.159), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * GM1 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(GM1)->setPlane(planeGM1, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(GM1, _cTrack));
    _cTrack->checkConsistency();
     
    // GM2
    genfit::SharedPlanePtr planeGM2(new genfit::DetPlane( TVector3(-25.1749, 0, -400.169), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * GM2 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(GM2)->setPlane(planeGM2, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(GM2, _cTrack));
    _cTrack->checkConsistency();
    
    // GM3
    genfit::SharedPlanePtr planeGM3(new genfit::DetPlane( TVector3(-27.9603, 0, -200.188), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * GM3 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(GM3)->setPlane(planeGM3, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(GM3, _cTrack));
    _cTrack->checkConsistency();

    // GM4
    genfit::SharedPlanePtr planeGM4(new genfit::DetPlane( TVector3(-30.7458, 0, -0.20765), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * GM4 = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(GM4)->setPlane(planeGM4, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(GM4, _cTrack));
    _cTrack->checkConsistency();
        
    //==================================================================
    
    if (_gEvent % 1000 == 0) {
        std::cout << "Event num. " << _gEvent << " processed" << std::endl;
    }
    
    if (_nOfApvHits >= _minHits) {
    
        try {
            _cFitter->processTrack(_cTrack);
            
            _cTrack->checkConsistency();
            
            _cTrack->determineCardinalRep();
            
            _cRep->extrapolateToPlane(state, stateRefOrig.getPlane());
            
            for (long unsigned int i = 0; i < _retMeasurements.size(); ++i) {
                //std::cout << "Station name is " << _detNames[i] << std::endl;
                
                genfit::TrackPoint* tp = _cTrack->getPointWithFitterInfo(i, _cTrack->getCardinalRep());

                genfit::KalmanFitterInfo* kFitterInfo = tp->getKalmanFitterInfo(_cTrack->getCardinalRep());
                        
                genfit::MeasuredStateOnPlane kfState = kFitterInfo->getFittedState();
                
                const TVectorD& fstate = kfState.get6DState();
                
                //if (kfState.getPDG() != 11) {
                //    std::cout << "Particle PGD is " << kfState.getPDG() << std::endl;
                //}
                        
                //genfit::MeasurementOnPlane residual = kFitterInfo->getResidual();
                
                _cRep->extrapolateToPlane(state, _planePtrs[i]);
                _cRep->get6DStateCov(state, curPos, curCov);
                
                //residual.genfit::StateOnPlane::getState().Print();
                
                //TVectorD res = residual.genfit::StateOnPlane::getState();
                
                if (_detNames[i] == "MM1") {
                    hPullMM1x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM1y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                if (_detNames[i] == "MM2") {
                    hPullMM2x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM2y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                if (_detNames[i] == "MM3") {
                    hPullMM3x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM3y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                if (_detNames[i] == "MM4") {
                    hPullMM4x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM4y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                if (_detNames[i] == "MM5") {
                    hPullMM5x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM5y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                if (_detNames[i] == "MM6") {
                    hPullMM6x->Fill( ( fstate[0] - curPos[0] ) / sqrt(curCov[0][0]) );
                    hPullMM6y->Fill( ( fstate[1] - curPos[1] ) / sqrt(curCov[1][1]) );
                }
                
                _cRep->extrapolateToPlane(state, stateRefOrig.getPlane());
                
            if (_gEvent < 1000) {
                genfit::EventDisplay::getInstance()->addEvent(_cTrack);
            }
                
            }
            //genfit::EventDisplay::getInstance()->addEvent(_cTrack);
            ++_gEvent;
            _detNames.clear();
            _retMeasurements.clear();
            _nOfApvHits = 0;
            return kOk;
            
        }
        catch(genfit::Exception& e) {
            std::cerr << e.what();
            std::cerr << "Exception, next track" << std::endl;
            
            ++_gEvent;
            _nOfApvHits = 0;
            _retMeasurements.clear();
            _detNames.clear();
            return kOk;
        }
    }
    
    else {
        ++_gEvent;
        
        _detNames.clear();
        _retMeasurements.clear();
        _nOfApvHits = 0;
        
        return kOk;        
    }
}

void
APVPull::finalize(){
    
    
    // fit and draw histograms for MM1,2,3
    TCanvas* c3 = new TCanvas();
    c3->Divide(2,3);
    
    c3->cd(1);
    hPullMM1x->Fit("gaus");
    hPullMM1x->Draw();
    
    c3->cd(2);
    hPullMM1y->Fit("gaus");
    hPullMM1y->Draw();
    
    c3->cd(3);
    hPullMM2x->Fit("gaus");
    hPullMM2x->Draw();
    
    c3->cd(4);
    hPullMM2y->Fit("gaus");
    hPullMM2y->Draw();
    
    c3->cd(5);
    hPullMM3x->Fit("gaus");
    hPullMM3x->Draw();
    
    c3->cd(6);
    hPullMM3y->Fit("gaus");
    hPullMM3y->Draw();
    
    c3->Write();
    
    // fit and draw histograms for MM4,5,6    
    TCanvas* c4 = new TCanvas();
    c4->Divide(2,3);
    
    c4->cd(1);
    hPullMM4x->Fit("gaus");
    hPullMM4x->Draw();
    
    c4->cd(2);
    hPullMM4y->Fit("gaus");
    hPullMM4y->Draw();
    
    c4->cd(3);
    hPullMM5x->Fit("gaus");
    hPullMM5x->Draw();
    
    c4->cd(4);
    hPullMM5y->Fit("gaus");
    hPullMM5y->Draw();
    
    c4->cd(5);
    hPullMM6x->Fit("gaus");
    hPullMM6x->Draw();
    
    c4->cd(6);
    hPullMM6y->Fit("gaus");
    hPullMM6y->Draw();
    
    c4->Write();    

    genfit::EventDisplay::getInstance()->open();
}

REGISTER_HANDLER( APVPull, banks, ch, cfg
                , "Residuals determination" ) {
    
    return new APVPull( ch
                             , aux::retrieve_det_selection(cfg)
                             , cfg["geometry"].as<std::string>()
                             , cfg["iterationsRange"][0].as<int>()
                             , cfg["iterationsRange"][1].as<int>()
                             , cfg["minHitNumber"].as<int>()
                             );
}
}
}
#endif  // defined(GenFit_FOUND)

