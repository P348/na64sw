#include "new_data/tpBestChargeAPV.hh"

namespace na64dp {
namespace handlers {

TPBestChargeAPV::TPBestChargeAPV( calib::Dispatcher & ch
								, const std::string & only
								, ObjPool<TrackPoint> & obTP )
				: AbstractHitHandler<TrackPoint>(ch, only) {
}

bool
TPBestChargeAPV::process_hit( EventID
							, DetID_t did
							, TrackPoint & tp ) {
	
    // Retrieve the index by detector ID
    auto idxIt = _idx.find( did );
    if( _idx.end() == idxIt ) {
        auto ir = _idx.emplace( did, TPByCharge() );
        assert( ir.second );
        idxIt = ir.first;
    }
    TPByCharge & tpByCharge = idxIt->second;
 
    tpByCharge.emplace( tp.charge
                      , _current_event().trackPoints.pool().get_ref_of(tp) );
                      
    return true;
}

TPBestChargeAPV::ProcRes
TPBestChargeAPV::process_event(Event * evPtr) {
    
    AbstractHitHandler<TrackPoint>::process_event( evPtr );
    
    for ( auto it : _idx ) {
		_bestTPs.insert( it.second.rbegin()->second.offset_id() );
	}

    std::map<size_t, TrackPointIndex::Iterator> cIts;
    for( auto it = evPtr->trackPoints.begin()
       ; it != evPtr->trackPoints.end()
       ; ++it ) {
        cIts.emplace( (*it).second.offset_id(), it );
    }
    
    // Remove all except for "best" track points
    for( auto cPair : cIts ) {
		// curret cluster pool referece
		const size_t cOffset = cPair.first;
		//const PoolRef<APVCluster> & cPoolRef = (*cPair.second).second;
		// do not remove if in "best" clusters
        if( _bestTPs.end() != _bestTPs.find(cOffset)) continue;
        // resolve reverse and remove
        
		EvFieldTraits<TrackPoint>::remove_from_event( *evPtr
													, cPair.second
													, naming() );
    }
    
    _bestTPs.clear();
    
    return kOk;
}

}

REGISTER_HANDLER( TPBestChargeAPV, banks, ch, cfg
                , "Handler for search of conjugated track point"
                  " with highest charge" ) {
    
    return new handlers::TPBestChargeAPV( ch
										, aux::retrieve_det_selection(cfg)
										, banks.of<TrackPoint>() );
}
}
