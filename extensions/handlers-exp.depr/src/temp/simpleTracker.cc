#include "tracking/simpleTracker.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include <EventDisplay.h>

// Geometry, material and magnetic field headers
#include "ConstFieldBox.h" // both have to included in somewhere in root of na64 analysis tool
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

// Tracking engine and track representatives
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <MeasuredStateOnPlane.h>
#include <MeasurementOnPlane.h>
#include <PlanarMeasurement.h>
#include <StateOnPlane.h>

// Kalman reference fitter routine and associated info
#include <AbsKalmanFitter.h>
#include <KalmanFitter.h>
#include <KalmanFitterInfo.h>
#include <KalmanFitterRefTrack.h>
#include <KalmanFitStatus.h>
#include <KalmanFittedStateOnPlane.h>

// Root dependencies 
#include <TApplication.h>
#include <TCanvas.h>
#include <TDatabasePDG.h>
#include <TEveManager.h>
#include <TGeoManager.h>
#include <TH2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TVector3.h>
#include <vector>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TString.h>

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static SimpleTracker::DetPlacementEntry gPlacements[] = {
    
    // Placement for 2018_invis100
    {"MM1X", -1,       0,      -1967.1,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ, resolution, strip specific number of wires
    {"MM1Y", -1,       0,      -1967.11,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM2X",  2.1378, -0.3564, -1820.1,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
    {"MM2Y",  2.1378, -0.3564, -1820.11,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320}, 
    
    {"MM3X", -31.79,  -0.2048,  -364.6,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM3Y", -31.79,  -0.2048,  -364.61,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM4X", -32.694, -1.9702,  -345.2,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM4Y", -32.694, -1.9702,  -345.21,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM5X", -37.66,  -0.5381,  -119.1,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
    {"MM5Y", -37.66,  -0.5381,  -119.11,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
      
    {"MM7X", -37.86,  -1.5531,   -97.9,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM7Y", -37.86,  -1.5531,   -97.91,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM1X", -25.9,         0,   -86.3,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM1Y", -25.9,         0,   -86.31,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM2X", -23.3,         0,  -160.6,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM2Y", -23.3,         0,  -160.61,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM3X", -25.1,         0,  -105.2,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM3Y", -25.1,         0,  -105.21,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM4X", -24.2,         0,  -143.7,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM4Y", -24.2,         0,  -143.71,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"HOD0X0-0-29", -28.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-1-29", -28.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-2-29", -29.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-3-29", -29.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-4-29", -29.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-5-29", -29.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-6-29", -29.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-7-29", -30.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-8-29", -30.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-9-29", -30.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-10-29",-30.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-11-29",-30.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-12-29",-31.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-13-29",-31.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-14-29",-31.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD0Y0-0-30", -30,    1.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-1-30", -30,    1.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-2-30", -30,    1.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-3-30", -30,    0.8,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-4-30", -30,    0.6,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-5-30", -30,    0.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-6-30", -30,    0.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-7-30", -30,    0.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-8-30", -30,   -0.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-9-30", -30,   -0.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-10-30", -30,  -0.6,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-11-30", -30,  -0.8,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-12-30", -30,  -1.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-13-30", -30,  -1.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-14-30", -30,  -1.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD1X0-0-29", -31.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-1-29", -31.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-2-29", -31.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-3-29", -31.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-4-29", -32.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-5-29", -32.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-6-29", -32.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-7-29", -32.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-8-29", -32.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-9-29", -33.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-10-29", -33.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-11-29", -33.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-12-29", -33.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-13-29", -33.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-14-29", -34.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD1Y0-0-30", -32.75, -1.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-1-30", -32.75, -1.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-2-30", -32.75, -1.0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-3-30", -32.75, -0.8,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-4-30", -32.75, -0.6,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-5-30", -32.75, -0.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-6-30", -32.75, -0.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-7-30", -32.75,    0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-8-30", -32.75,  0.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-9-30", -32.75,  0.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-10-30", -32.75, 0.6,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-11-30", -32.75, 0.8,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-12-30", -32.75, 1.0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-13-30", -32.75, 1.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-14-30", -32.75, 1.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"S0:0-0--", 0, 0, -1981.7,  0,  0,   0,  2.5, 2.5, 0.01,  2.5, 2.5},
    {"S1:0-0--", 0,   0,-1883.35,  0,  0,   0,   2.5, 2.5, 0.01,  2.5, 2.5},
    {"S2:0-0--", -29.5,0,-387.5,  0,  0,   0,   2.5, 2.5, 0.01,  2.5, 2.5},
    {"S3:0-0--", -34.5,0,-190.3,  0,  0,   0,   2.5, 2.5, 0.01,  2.5, 2.5},
    {"S4:0-0--", -37.3,0, -53,  0,  0,   0,   2.5, 2.5, 0.01,  2.5, 2.5},
    
    {"HCAL0:0-0--", -20.5, -20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:1-0--", -40.5, -20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:2-0--", -60.5, -20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:0-1--", -20.5, 0, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:1-1--", -40.5, 0, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:2-1--", -60.5, 0, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:0-2--", -20.5, 20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:1-2--", -40.5, 20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL0:2-2--", -60.5, 20, 82, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"HCAL1:0-0--", -24.5, -20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:1-0--", -44.5, -20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:2-0--", -64.5, -20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:0-1--", -24.5, 0, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:1-1--", -44.5, 0, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:2-1--", -64.5, 0, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:0-2--", -24.5, 20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:1-2--", -44.5, 20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL1:2-2--", -64.5, 20, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"HCAL2:0-0--", -40, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:1-0--", -60, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:2-0--", -80, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:0-1--", -40,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:1-1--", -60,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:2-1--", -80,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:0-2--", -40,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:1-2--", -60,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL2:2-2--", -80,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"HCAL3:0-0--", -20, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:1-0--",   0, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:2-0--",  20, -20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:0-1--", -20,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:1-1--",   0,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:2-1--",  20,   0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:0-2--", -20,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:1-2--",   0,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"HCAL3:2-2--",  20,  20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"SRD:0-0--", -20, 20, 412, 0, 0, 0, 10, 20, 20, 20, 0}, // position is unimportant
    {"SRD:1-0--", -20, 20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"SRD:2-0--", -20, 20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"VETO:0-0--",   0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:1-0--",   0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:2-0--",  -0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:3-0--",  -0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:4-0--",   0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:5-0--",   0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"V1:0-0--", -80, 0, -53, 0, 0, 0, 22, 22, 1, 20, 0},
    {"V2:0-0--", 0, 0, 65, 0, 0, 0, 10, 20, 20, 20, 0},

    {"T2:0-0--", -20, 20, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    
    {"MUON0:0-0--", -40.5, 0, 80, 0, 0, 0, 10, 20, 20, 20, 0},
    {"MUON1:0-0--", -44.5, 0, 255, 0, 0, 0, 10, 20, 20, 20, 0},
    {"MUON2:0-0--", -60, 0, 412, 0, 0, 0, 10, 20, 20, 20, 0},
    {"MUON3:0-0--", -60, 0, 577, 0, 0, 0, 10, 20, 20, 20, 0},
     
    
};

SimpleTracker::SimpleTracker( calib::Dispatcher & cdsp
                              , const std::string & only
                              , bool eventDisplay
                              , const std::string & geoFilePath
                              , int minIts, int maxIts)        
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _eventDisplay(eventDisplay)
        , _cFitter(nullptr)
        , _cRep(nullptr)
        , _cTrack(nullptr)
        , _cState(nullptr)
        , _cStateRefOrig(nullptr)
        , _gEvent(0)
        , _momentum(100)
        , _nHadron(false)
        , _cHadron(false)
        , _tElectron(false)
        , _vetoV1(false)
        , _vetoV2(false)
        , _veto(false)
        , _muon(false)
        , _e(0)
        , _mu(0)
        , _pi(0)
        , _ka(0)
        , _p(0)
    {
        
    // init geometry and mag. field
    new TGeoManager( "Geometry", "NA64 geometry");

    TGeoManager::Import( geoFilePath.c_str() );
    // ^^^ TODO: how to check whether the geometry was, indeed, imported?
    log().debug( "Geometry read from file \"%s\" into instance %p.", geoFilePath.c_str(), gGeoManager );
    assert(gGeoManager);
    
    //Init_Geometry_2017
    genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 18.0, 0.0, -1000, 1000, -1000, 1000, -1796.0, -1366.0));
    
    genfit::MaterialEffects::getInstance()->init( new genfit::TGeoMaterialInterface() );
    genfit::MaterialEffects::getInstance()->setNoEffects();
    
    // init fitter
    _cFitter = new genfit::KalmanFitterRefTrack();
    _cFitter->setMinIterations( minIts );
    _cFitter->setMaxIterations( maxIts );
    
    if (_eventDisplay) {
        genfit::EventDisplay::getInstance();
    }
    
    // create histograms
    gROOT->SetStyle("Plain");
    gStyle->SetPalette(1);
    gStyle->SetOptFit(1111);
    
    hMom = new TH1D("hResMom" ,"momRes",600, 0, 300);
}

void
SimpleTracker::handle_update( const nameutils::DetectorNaming & nm ) {
    TDirAdapter::handle_update(nm);
    AbstractHitHandler<TrackPoint>::handle_update(nm);
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        auto ir = _placements.emplace(did, gPlacements + i);
        if( !ir.second ) {
            //std::cerr << "Failed to insert placement for \"" << gPlacements[i].planeName << "\" (id repeated)." << std::endl;
        }
        else {
            //std::cout << "Inserted placement for \"" << gPlacements[i].planeName << "\" id=" << ir.first->first << std::endl;
        }
    }
}

bool
SimpleTracker::process_sadc_hit( DetID detID
                                , const SADCHit & hit ) {
    // retrieve placement for SADC detector if need
    #if 1
    auto it = _placements.find( detID );
    std::string stationName = TDirAdapter::naming()[detID];
    
    //std::cout << stationName << std::endl;
    //std::cout << hit.sum << std::endl;
    // ^^^ Note, this will produce the station name like ECAL0, HCAL3 or S1,
    // without subdivision information. For subdiv, use this:
    // std::string subDetName = naming()(detID);

    if( _placements.end() == it ) {
        if (stationName.find("ECAL") != std::string::npos) {
        }
        else {
            std::cerr << "Unable to find placement for "  << stationName << std::endl;
        }
    }
    #endif
    
    // TODO: Non elegant solution with naming
    /*
    if (stationName.find("HCAL") != std::string::npos) {
        if (hit.sum > 7000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            
            if (stationName.find("HCAL3:") != std::string::npos) {
                _nHadron = true;
            }
            
            int detId(0); int planeId(0); int hitId(0);
            
            TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
            double resolution(cPlacement->resolution);
            
            TMatrixDSym hitCov(2);
            hitCov(0, 0) = resolution*resolution;
            hitCov(1, 1) = resolution*resolution;
        
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
            
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
    }
    
    if (stationName.find("SRD:") != std::string::npos) {
        if (hit.sum > 8000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            _cHadron = true;
        }
        else {
            _tElectron = true;
        }
    }   
    
    if (stationName.find("VETO:") != std::string::npos) {
        if (hit.sum > 15000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            
            int detId(0); int planeId(0); int hitId(0);
            
            TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
            double resolution(cPlacement->resolution);
            
            TMatrixDSym hitCov(2);
            hitCov(0, 0) = resolution*resolution;
            hitCov(1, 1) = resolution*resolution;
        
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
            
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
    }
    
    if (stationName.find("V1:") != std::string::npos) {
        if (hit.sum > 10000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            
            _vetoV1 = true;

            int detId(0); int planeId(0); int hitId(0);
            
            TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
            double resolution(cPlacement->resolution);
            
            TMatrixDSym hitCov(2);
            hitCov(0, 0) = resolution*resolution;
            hitCov(1, 1) = resolution*resolution;
        
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
            
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            
        }
    }
    
    if (stationName.find("V2:") != std::string::npos) {
        if (hit.sum > 10000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            
            _vetoV2 = true;
            
            int detId(0); int planeId(0); int hitId(0);
            
            TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
            double resolution(cPlacement->resolution);
            
            TMatrixDSym hitCov(2);
            hitCov(0, 0) = resolution*resolution;
            hitCov(1, 1) = resolution*resolution;
        
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
            
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
    }
    
    if (stationName.find("MUON") != std::string::npos) {
        if (hit.sum > 7000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
            
            _muon = true;
            
            int detId(0); int planeId(0); int hitId(0);
            
            TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
            double resolution(cPlacement->resolution);
            
            TMatrixDSym hitCov(2);
            hitCov(0, 0) = resolution*resolution;
            hitCov(1, 1) = resolution*resolution;
        
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
            
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
    }
    
    
    if (stationName == "S0:0-0--" || stationName == "S1:0-0--" || stationName == "S2:0-0--" ||
        stationName == "S3:0-0--" || stationName == "S4:0-0--") { 
        
        if (hit.sum > 7000) {
            //std::cout << stationName << std::endl;   
            //std::cout << hit.sum << std::endl;
        }
        
        int detId(0); int planeId(0); int hitId(0);
        
        TVectorD hitCoords(2); hitCoords(0) = 0; hitCoords(1) = 0;

        const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
        
        double resolution(cPlacement->resolution);
        
        TMatrixDSym hitCov(2);
        hitCov(0, 0) = resolution*resolution;
        hitCov(1, 1) = resolution*resolution;
    
        genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), TVector3(1, 0, 0), TVector3(0, 1, 0)));

        genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, hitId, nullptr );
        static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, ++planeId);
        
        _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
    
    }
    */
    /*
    if (stationName.find("HOD0X") != std::string::npos) {
        
        // Always MAXED value, seems like invalid channel
        if (stationName.find("HOD0X0-2-29") != std::string::npos) {
            return true;
        }
        _HOD0X.emplace(stationName, hit.sum);
        return true;
    }
    
    if (stationName.find("HOD0Y") != std::string::npos) {
        _HOD0Y.emplace(stationName, hit.sum);
        return true;
    }

    if (stationName.find("HOD1X") != std::string::npos) {
        _HOD1X.emplace(stationName, hit.sum);
        return true;
    }
        
    if (stationName.find("HOD1Y") != std::string::npos) {
        _HOD1Y.emplace(stationName, hit.sum);
        return true;
    }

    else {
        return true;
    }
    */
    if (stationName.find("HOD") != std::string::npos && hit.sum > 1000) {
		if (stationName.find("HOD0X0-2-29") != std::string::npos) {
            return true;
        }
        
		int detIdH(0); // detector ID 
		int planeIdH(0); // detector plane ID
		int hitIdH(0); // hit ID

		TVectorD hitCoords(1);
        
        const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
        hitCoords(0) = 0;
                    
		double resolution(cPlacement->resolution);
        TMatrixDSym hitCov(1);
        hitCov(0, 0) = resolution*resolution;
                    
        TVector3 u_plane(1, 0, 0);
        TVector3 v_plane(0, 1, 0);
                    
        genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
        genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
        static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
        
        if (stationName.find("X") != std::string::npos) {
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else if (stationName.find("Y") != std::string::npos){
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }        
        
        _cTrack->checkConsistency();
	}
	
	return true;
		
}

bool
SimpleTracker::process_hit( EventID, DetID_t stationID, TrackPoint & cPoint) {
    return true;
}

AbstractHandler::ProcRes
SimpleTracker::process_event(Event * evPtr ) {
    
    if (_gEvent % 1000 == 0) {
        std::cout << _gEvent << std::endl;
    }
    
    _nHadron = false; _cHadron = false; _tElectron = false;
    _vetoV1 = false;  _vetoV2 = false;  _veto = false;
    _muon = false;
    
    // Clear up
    _HOD0X.clear(); _HOD0Y.clear(); _HOD1X.clear(); _HOD1Y.clear();
    
    int res(1);
    // start values for the fit, e.g. from pattern recognition
    TVector3 pos(0, 0, 0);
    //TVector3 pos(0, 0, -2600);
    TVector3 mom(0, 0, 1);
    mom.SetMag(_momentum);
    
    // initial covariance matrix
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i)
        covM(i,i) = res*res;
    for (int i = 3; i < 6; ++i)
        covM(i,i) = pow(res / 5 / sqrt(3), 2);  

    // Initiat TrackRep
    genfit::AbsTrackRep* _cRep = new genfit::RKTrackRep(-11);
    _cTrack = new genfit::Track(_cRep, pos, mom);
    
    genfit::AbsTrackRep* muonRep = new genfit::RKTrackRep(13); 
    genfit::AbsTrackRep* pionRep = new genfit::RKTrackRep(211); 
    genfit::AbsTrackRep* kaonRep = new genfit::RKTrackRep(321); 
    genfit::AbsTrackRep* protonRep = new genfit::RKTrackRep(2212); 
    
    _cTrack->addTrackRep(muonRep); _cTrack->addTrackRep(pionRep);
    _cTrack->addTrackRep(kaonRep); _cTrack->addTrackRep(protonRep);
        
    // Create initial state with covariance and assign it to out first rep
    genfit::MeasuredStateOnPlane state(_cRep);
    _cRep->setPosMomCov(state, pos, mom, covM);
    
    // remember original state
    const genfit::StateOnPlane stateRefOrig(state);
    
    // Start of track assamble routine
    
    int detId(0); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(1);
    
    for (auto p : evPtr->apvClusters) {
        
        const DetPlacementEntry * cPlacement = _placements[DetID(p.first)];

        std::string stationName = TDirAdapter::naming()[DetID(p.first)];
        
        //std::cout << stationName << std::endl;
        
        if (stationName.find('X') != std::string::npos) {
        
            hitCoords(0) = ((cPlacement->sizeX * p.second->position / cPlacement->numOfWires) - (( cPlacement->sizeX )/2) );
            
            //std::cout << hitCoords(0) << std::endl;
            //std::cout << hitCoords(0) + cPlacement->x << std::endl;
        }
        
        if (stationName.find('Y') != std::string::npos) {
        
            hitCoords(0) = ((cPlacement->sizeY * p.second->position / cPlacement->numOfWires) - (( cPlacement->sizeY )/2) );
            
            if (stationName.find("GEM") != std::string::npos) {
                hitCoords(0) = hitCoords(0)*-1.;
            }
            
            //std::cout << hitCoords(0) << std::endl;
            //std::cout << hitCoords(0) + cPlacement->y << std::endl;
        }        
        
        
        double resolution(cPlacement->resolution);
        TMatrixDSym hitCov(1);
        hitCov(0, 0) = resolution*resolution;
        
        TVector3 u_plane(cPlacement->sizeX, 0, 0);
        TVector3 v_plane(0, cPlacement->sizeY, 0);

        if (cPlacement->rotAngleX != 0) {
            u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
            v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        }

        if (cPlacement->rotAngleY != 0) {
            u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
            v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        }
        
        if (cPlacement->rotAngleZ != 0) {
            u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
            v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        }

        genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
        genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, ++hitId, nullptr );
        
        std::string s = TDirAdapter::naming()[p.first];
        if (s.find('X') != std::string::npos) {
            
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else if (s.find('Y') != std::string::npos){
        
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else {
            std::cout << "Something went wrong, no X or Y projection" << std::endl;
        }
    }
    
    
    // Iterate over SADC detectors to extract aux info for tracking
    
    for( auto sadcHitPair : evPtr->sadcHits ) {
        process_sadc_hit( sadcHitPair.first
                        , *(sadcHitPair.second) );
    }
    
    // Template to find max value in each hodoscope and to return 
    // ===========================================================
    // =============== HOD0X =====================================
    /*
    int detIdH(0); // detector ID 
    int planeIdH(0); // detector plane ID
    int hitIdH(0); // hit ID
    
    if (_HOD0X.size() > 0) {
    
        using pair_type = decltype(_HOD0X)::value_type;

        auto pr = std::max_element
        (
            std::begin(_HOD0X), std::end(_HOD0X),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << "" << pr->first << "" << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr->first);
        
        if (hodName.find("HOD0X") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    // =============== HOD0Y =====================================
    
    if (_HOD0Y.size() > 0) {
    
        using pair_type = decltype(_HOD0Y)::value_type;

        auto pr1 = std::max_element
        (
            std::begin(_HOD0Y), std::end(_HOD0Y),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << pr1->first << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr1->first);
        
        if (hodName.find("HOD0Y") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
        
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    // =============== HOD1X =====================================
    
    if (_HOD1X.size() > 0) {
    
        using pair_type = decltype(_HOD1X)::value_type;

        auto pr2 = std::max_element
        (
            std::begin(_HOD1X), std::end(_HOD1X),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << pr2->first << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr2->first);
        
        if (hodName.find("HOD1X") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    
    // =============== HOD1Y =====================================
    
    if (_HOD1Y.size() > 0) {
    
        using pair_type = decltype(_HOD1Y)::value_type;

        auto pr3 = std::max_element
        (
            std::begin(_HOD1Y), std::end(_HOD1Y),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        std::string hodName = static_cast<std::string>(pr3->first);
        
        if (hodName.find("HOD1Y") != std::string::npos) {
        
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }      
    }
        
    // ============================================================================================================================
    */
    
    // Veto conditions
    
    if (_nHadron == true) {
        _gEvent++;
        return kOk;
    }

    if (_cHadron == true) {
        _gEvent++;
        return kOk;
    }    

    if (_vetoV1 == true) {
        _gEvent++;
        return kOk;
    }  

    if (_vetoV2 == true) {
        _gEvent++;
        return kOk;
    }  

    if (_veto == true) {
        _gEvent++;
        return kOk;
    }
    
    if (_muon == true) {
        _gEvent++;
        return kOk;
    }  

    if (hitId >= 6) {
        
        try {
            _cFitter->processTrack(_cTrack);
            
            _cTrack->checkConsistency();
            
            _cTrack->determineCardinalRep();
            
            if (hitId >= 8) {
                if (_eventDisplay) {
                    //genfit::EventDisplay::getInstance()->addEvent(_cTrack);
                }
            }
            
            genfit::MeasuredStateOnPlane stLast = _cTrack->getFittedState(-1);
            
            const genfit::FitStatus* fit = _cTrack->getFitStatus();
            const double chi2ndf = fit->getChi2() / fit->getNdf();
            
            const int particle = _cTrack->getCardinalRep()->getPDG();
            
            if (particle == -11) {
                _e++;
            }
            else if (particle == 13) {
                _mu++;
            }
            else if (particle == 211) {
                _pi++;
            }
            else if (particle == 321) {
                _ka++;
            }
            else if (particle == 2211) {
                _p++;
            }
            
            
            hMom->Fill ( stLast.getMomMag() );
            
            if ( chi2ndf < 5 ) {
                std::cout << "GenFit track"
                    << " particle PDG = " << particle
                    << " momentum = " << stLast.getMomMag()
                    << " fit converged = " << fit->isFitConverged()
                    << " fitted charge = " << fit->getCharge()
                    << " fit Chi^2 = " << fit->getChi2()
                    << " fit Ndf = " << fit->getNdf()
                    << " fit Chi^2/Ndf = " << chi2ndf
                    << std::endl;
            }
        }
        
        catch(genfit::Exception& e) {
            //std::cerr << e.what();
            std::cerr << "Exception in event "<<  _gEvent << ", next track" << std::endl;
            
            _gEvent++;
            return kOk;
        }
    }
    
    _gEvent++;
    return kOk;
}

void
SimpleTracker::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
    c1->cd(1);
    hMom->Draw();   
    
    std::cout << "Number of e = " << _e << std::endl;
    std::cout << "Number of mu = " << _mu << std::endl; 
    std::cout << "Number of pi = " << _pi << std::endl; 
    std::cout << "Number of ka = " << _ka << std::endl; 
    std::cout << "Number of p = " << _p << std::endl; 
    
    if (_eventDisplay) {
        genfit::EventDisplay::getInstance()->open();
    }
}

REGISTER_HANDLER( SimpleTracker, banks, ch, cfg
                , "Simple tracker for testing purposes" ) {
    
    return new SimpleTracker( ch
                             , aux::retrieve_det_selection(cfg)
                             , cfg["display"] ? cfg["display"].as<bool>() : false
                             , cfg["geometry"].as<std::string>()
                             , cfg["iterationsRange"][0].as<int>()
                             , cfg["iterationsRange"][1].as<int>()
                             );
}
}
}
#endif  // defined(GenFit_FOUND)
