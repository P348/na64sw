#include "tracking/alignmentGBL.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include <EventDisplay.h>

// Geometry, material and magnetic field headers
#include "ConstFieldBox.h" // both have to included in somewhere in root of na64 analysis tool
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

// Tracking engine and track representatives
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <MeasuredStateOnPlane.h>
#include <MeasurementOnPlane.h>
#include <PlanarMeasurement.h>
#include <StateOnPlane.h>

// Kalman reference fitter routine and associated info
#include <AbsKalmanFitter.h>
#include <KalmanFitter.h>
#include <KalmanFitterInfo.h>
#include <KalmanFitterRefTrack.h>
#include <KalmanFitStatus.h>
#include <KalmanFittedStateOnPlane.h>
#include <DAF.h>

// Root dependencies 
#include <TApplication.h>
#include <TCanvas.h>
#include <TDatabasePDG.h>
#include <TEveManager.h>
#include <TGeoManager.h>
#include <TH2D.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TVector3.h>
#include <vector>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TString.h>

#include "na64detID/cellID.hh"

#include <GFGbl.h>
#include <GblFitter.h>

namespace na64dp {
namespace handlers {

static AlignmentGBL::DetPlacementEntry gPlacements[] = {
    
    // Placement for 2018_invis100
    {"MM1", -1,       0,      -1967.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ
    {"MM2",  2.1378, -0.3564, -1820.1,  0,  0,  45,   8.,  8.,  0.01, 0.02, 320}, // resolution, strip specific number of wires
    {"MM3", -31.79,  -0.2048, -364.6,   0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    {"MM4", -32.694, -1.9702, -345.2,   0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    {"MM5", -37.66,  -0.5381, -119.1,   0,  0,  45,   8.,  8.,  0.01, 0.02, 320},    
    {"MM6", -37.86,  -1.5531, -97.9,    0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    
    
    {"GM01", -25.9, 0, -86.3, 0, 0, 0, 8., 8., 0.01, 0.04, 256},      
    {"GM02", -23.3, 0, -160.6, 0, 0, 0, 8., 8., 0.01, 0.04, 256}, 
    {"GM03", -25.1, 0, -105.2, 0, 0, 0, 8., 8., 0.01, 0.04, 256}, 
    {"GM04", -24.2, 0, -143.7, 0, 0, 0, 8., 8., 0.01, 0.04, 256},
    
    /*
    // Placement for 2018_vis150
    {"MM1", -1+2.222,                   0,      -2594.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ
    {"MM2", -1.44+0.6197,       -0.5381, -2558.1,  0,  0, -45,   8.,  8.,  0.01, 0.02, 320}, // resolution, strip specific number of wires
    {"MM3", 2.1378-3.302,     -0.3564, -2484.6,   0,  0, 45,   8.,  8.,  0.01, 0.02, 320},
    {"MM4", 2.1378-3.167,     -0.3564, -2447.9,   0,  0, 45,   8.,  8.,  0.01, 0.02, 320},
    {"MM5", -17.24-4.422,      -2.048, -695,   0,  0,  -45,   8.,  8.,  0.01, 0.02, 320},    
    {"MM6", -18.194,     -1.07019, -675,    0,  0, -45,   8.,  8.,  0.01, 0.02, 320},
    
    
    {"GM01", -30.053,-1.021,-58.1, 0, 0, 0, 8., 8., 0.01, 0.04, 256},      
    {"GM02", -30.547,-1.855,-159.1, 0, 0, 0, 8., 8., 0.01, 0.04, 256}, 
    {"GM03", -29.678,-0.268,-104.0, 0, 0, 0, 8., 8., 0.01, 0.04, 256}, 
    {"GM04", -30.548,-0.631,-142.4, 0, 0, 0, 8., 8., 0.01, 0.04, 256}, 
    */
    
};

AlignmentGBL::AlignmentGBL( calib::Dispatcher & cdsp
                              , const std::string & only
                              , const std::string & geoFilePath
                              , int minIts, int maxIts, int minHits)        
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _cFitter(nullptr)
        , _cGBL(nullptr)
        , _cFitterGbl(nullptr)
        , _cRep(nullptr)
        , _cTrack(nullptr)
        , _cState(nullptr)
        , _cStateRefOrig(nullptr)
        , _gEvent(0)
        , _nOfApvHits(0)
        , _momentum(100)
        , _minHits(minHits)
    {

    // init geometry and mag. field
    new TGeoManager( "Geometry", "NA64 geometry");

    TGeoManager::Import( geoFilePath.c_str() );
    // ^^^ TODO: how to check whether the geometry was, indeed, imported?
    log().debug( "Geometry read from file \"%s\" into instance %p.", geoFilePath.c_str(), gGeoManager );
    assert(gGeoManager);
    
    //Init_Geometry_2018_invis
    genfit::FieldManager::getInstance()->init(new genfit::ConstFieldBox( 0., 17.2, 0.0, -100, 100, -100, 100, -1746.6, -1346.6));
    
    genfit::MaterialEffects::getInstance()->init( new genfit::TGeoMaterialInterface() );
    genfit::MaterialEffects::getInstance()->setNoEffects();
    
    // init fitter
    _cFitter = new genfit::KalmanFitterRefTrack();
    _cFitter->setMinIterations( minIts );
    _cFitter->setMaxIterations( maxIts );
    
    _cFitterGbl = new genfit::GblFitter();
    
    _cGBL = new genfit::GFGbl();
    
    _cGBL->beginRun();


}

void
AlignmentGBL::handle_update( const nameutils::DetectorNaming & nm ) {
    TDirAdapter::handle_update(nm);
    AbstractHitHandler<TrackPoint>::handle_update(nm);
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        char * postfix;
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id( gPlacements[i].planeName );
        // impose placement entry into `_placements' map
        auto ir = _placements.emplace(did, gPlacements + i);
        if( !ir.second ) {
            //std::cerr << "Failed to insert placement for \"" << gPlacements[i].planeName << "\" (id repeated)." << std::endl;
        }
        else {
            std::cout << "Inserted placement for \"" << gPlacements[i].planeName << "\" id=" << ir.first->first << std::endl;
        }
    }
}

bool
AlignmentGBL::process_hit( EventID, DetID_t stationID, TrackPoint & cPoint) {
    
    auto it = _placements.find( stationID );
    std::string stationName = TDirAdapter::naming()[stationID];
    
    if( _placements.end() == it ) {
        //std::cout << "Unable to find station "  << stationName << std::endl;
        return true;
    }
    const DetPlacementEntry * cPlacement = _placements[stationID];
        
    int detId(_nOfApvHits); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(2);

    hitCoords(0) = ((cPlacement->sizeX * cPoint.lR[0] / cPlacement->numOfWires) - (( cPlacement->sizeX )/2) ); // Simple conversion with respect to micromega 8 cm side
    hitCoords(1) = ((cPlacement->sizeY * cPoint.lR[1] / cPlacement->numOfWires) - (( cPlacement->sizeY )/2) );

    double resolution(cPlacement->resolution);
    TMatrixDSym hitCov(2);
    hitCov(0, 0) = resolution*resolution;
    hitCov(1, 1) = resolution*resolution;     
    
    TVector3 u_plane(1, 0, 0);
    TVector3 v_plane(0, 1, 0);

    if (cPlacement->rotAngleX != 0) {
        u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
    }

    if (cPlacement->rotAngleY != 0) {
        u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
    }
    
    if (cPlacement->rotAngleZ != 0) {
        u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
    }

    genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));

    genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
    

    _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            
    _cTrack->checkConsistency();
    
    _detNames.push_back(stationName);

    _retMeasurements.push_back(measurement);
    
    _planePtrs.push_back(plane);
    
    _nOfApvHits += 1;
    
    return true;
}

AbstractHandler::ProcRes
AlignmentGBL::process_event(Event * evPtr ) {

    
    // Clear up
    _planePtrs.clear();
    _detNames.clear();
    _retMeasurements.clear();
    _nOfApvHits = 0;

    gRandom->SetSeed(14);

    int resolution(1);
    // start values for the fit, e.g. from pattern recognition
    TVector3 pos(0, 0, -2007.3);
    //TVector3 pos(0, 0, -2600);
    TVector3 mom(0, 0, 1);
    mom.SetMag(_momentum);
    
    // initial covariance matrix
    TMatrixDSym covM(6);
    for (int i = 0; i < 3; ++i)
        covM(i,i) = resolution*resolution;
    for (int i = 3; i < 6; ++i)
        covM(i,i) = pow(resolution / 5 / sqrt(3), 2);  

    // Initiat TrackRep
    genfit::AbsTrackRep* _cRep = new genfit::RKTrackRep(-11);
    _cTrack = new genfit::Track(_cRep, pos, mom);
    #if 1
    genfit::AbsTrackRep* muonRep = new genfit::RKTrackRep(13); 
    genfit::AbsTrackRep* pionRep = new genfit::RKTrackRep(211); 
    genfit::AbsTrackRep* kaonRep = new genfit::RKTrackRep(321); 
    genfit::AbsTrackRep* protonRep = new genfit::RKTrackRep(2212); 
    
    _cTrack->addTrackRep(muonRep);
    _cTrack->addTrackRep(pionRep);
    _cTrack->addTrackRep(kaonRep);
    _cTrack->addTrackRep(protonRep);
    #endif
    // Create initial state with covariance and assign it to out first rep
    genfit::MeasuredStateOnPlane state(_cRep);
    _cRep->setPosMomCov(state, pos, mom, covM);
    
    // remember original state
    const genfit::StateOnPlane stateRefOrig(state);
    
    //Show that everything is defined correctly
    TVectorD initPos;
    TMatrixDSym initCov;
    _cRep->get6DStateCov(state, initPos, initCov);
    
    // Helper vectors for current position display
    TVectorD curPos;
    TMatrixDSym curCov;
    _cRep->get6DStateCov(state, curPos, curCov);
    
    //==================================================================
    // Assamble virtual planes to simulate geometry ====================
    //==================================================================

    int detId(_nOfApvHits); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(2);

    hitCoords(0) = 0;
    hitCoords(1) = 0;


    double resolutionS0(2.5);
    TMatrixDSym hitCovS0(2);
    hitCovS0(0, 0) = resolutionS0*resolutionS0;
    hitCovS0(1, 1) = resolutionS0*resolutionS0;

    double resolutionS(3.2);
    TMatrixDSym hitCovS(2);
    hitCovS(0, 0) = resolutionS*resolutionS;
    hitCovS(1, 1) = resolutionS*resolutionS;
    
    double resolutionST(0.02);
    TMatrixDSym hitCovST(2);
    hitCovST(0, 0) = resolutionST*resolutionST;
    hitCovST(1, 1) = resolutionST*resolutionST;
    
    double resolutionH(0.02);
    TMatrixDSym hitCovH(2);
    hitCovH(0, 0) = resolutionH*resolutionH;
    hitCovH(1, 1) = resolutionH*resolutionH;

    // S0
    genfit::SharedPlanePtr planeS0(new genfit::DetPlane( TVector3(0,0,-1981.7), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S0 = new genfit::PlanarMeasurement( hitCoords, hitCovS0, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S0)->setPlane(planeS0, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S0, _cTrack));
    _cTrack->checkConsistency();    
    
    // S1
    genfit::SharedPlanePtr planeS1(new genfit::DetPlane( TVector3(0,0,-1883.35), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S1 = new genfit::PlanarMeasurement( hitCoords, hitCovS0, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S1)->setPlane(planeS1, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S1, _cTrack));
    _cTrack->checkConsistency();
    
    // S2
    genfit::SharedPlanePtr planeS2(new genfit::DetPlane( TVector3(-25.3131,0,-387.5), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S2 = new genfit::PlanarMeasurement( hitCoords, hitCovS, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S2)->setPlane(planeS2, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S2, _cTrack));
    _cTrack->checkConsistency();
    
    // S3
    genfit::SharedPlanePtr planeS3(new genfit::DetPlane( TVector3(-29.6846,0,-190.3), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S3 = new genfit::PlanarMeasurement( hitCoords, hitCovS, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S3)->setPlane(planeS3, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S3, _cTrack));
    _cTrack->checkConsistency();
    
    // S4
    genfit::SharedPlanePtr planeS4(new genfit::DetPlane( TVector3(-32.7447,0,-53), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * S4 = new genfit::PlanarMeasurement( hitCoords, hitCovS, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(S4)->setPlane(planeS4, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(S4, _cTrack));
    _cTrack->checkConsistency();
    
    // ST1
    genfit::SharedPlanePtr planeST1(new genfit::DetPlane( TVector3(-27.7,0,-277), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * ST1 = new genfit::PlanarMeasurement( hitCoords, hitCovST, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(ST1)->setPlane(planeST1, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(ST1, _cTrack));
    _cTrack->checkConsistency();    
    
    // H0
    genfit::SharedPlanePtr planeH0(new genfit::DetPlane( TVector3(-30.12,0,-165.9), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * H0 = new genfit::PlanarMeasurement( hitCoords, hitCovH, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(H0)->setPlane(planeH0, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(H0, _cTrack));
    _cTrack->checkConsistency();
    
    // ST2
    genfit::SharedPlanePtr planeST2(new genfit::DetPlane( TVector3(-27.7,0,-126.9), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * ST2 = new genfit::PlanarMeasurement( hitCoords, hitCovST, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(ST2)->setPlane(planeST2, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(ST2, _cTrack));
    _cTrack->checkConsistency();   
    
    // H1
    genfit::SharedPlanePtr planeH1(new genfit::DetPlane( TVector3(-32.3076,0,-68), TVector3(1, 0, 0), TVector3(0, 1, 0)));

    genfit::AbsMeasurement * H1 = new genfit::PlanarMeasurement( hitCoords, hitCovH, ++detId, hitId, nullptr );
    static_cast<genfit::PlanarMeasurement*>(H1)->setPlane(planeH1, planeId);
    
    _cTrack->insertPoint(new genfit::TrackPoint(H1, _cTrack));
    _cTrack->checkConsistency();
    
    //==================================================================

    //Start of hits processing...
    AbstractHitHandler<TrackPoint>::process_event(evPtr);
    
    if (_gEvent % 1000 == 0) {
        std::cout << "Event num. " << _gEvent << " processed" << std::endl;
    }
    
    if (_nOfApvHits == _minHits) {
    
        try {
            _cFitter->processTrack(_cTrack);
            
            _cTrack->checkConsistency();
            
            _cTrack->determineCardinalRep();
            
            _cFitterGbl->processTrackWithRep(_cTrack, _cRep);
            
            ++_gEvent;
            return kOk;
                
            }
        catch(genfit::Exception& e) {
            //std::cerr << e.what();
            std::cerr << "Exception in event "<<  _gEvent << ", next track" << std::endl;
            
            ++_gEvent;
            return kOk;
        }
    }
    
    else {
        ++_gEvent;
        return kOk;        
    }
}

void
AlignmentGBL::finalize(){
    
    _cGBL->endRun();

}

REGISTER_HANDLER( AlignmentGBL, banks, ch, cfg
                , "Residuals determination" ) {
    
    return new AlignmentGBL( ch
                             , aux::retrieve_det_selection(cfg)
                             , cfg["geometry"].as<std::string>()
                             , cfg["iterationsRange"][0].as<int>()
                             , cfg["iterationsRange"][1].as<int>()
                             , cfg["minHitNumber"].as<int>()
                             );
}
}
}
#endif  // defined(GenFit_FOUND)

