#include "tracking/trackAPV.hh"

#include "na64detID/wireID.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

namespace na64dp {
namespace handlers {

static TrackAPV::DetPlacementEntry gPlacements[] = {
    
    // Placement for 2017
    // StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // sizeX, sizeY, 
    // resolution, number of wires
    
    {"MM1X", -1.81, -0.616, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    {"MM1Y", -1.81, -0.616, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    
    {"MM2X", -0.909, -0.582, -1958.00,  0,  0, 135, 8., 8., 0.2, 320},
    {"MM2Y", -0.909, -0.582, -1958.00,  0,  0, 135, 8., 8., 0.2, 320}, 
    
    {"MM3X", -1.83, -0.13, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM3Y", -1.83, -0.13, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    
    {"MM4X", -0.38, -0.8, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    {"MM4Y", -0.38, -0.8, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    
    {"MM5X", -32.51, -0.85, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y", -32.51, -0.85, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
      
    {"MM7X", -36.41, -0.025, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM7Y", -36.41, -0.025, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    
    {"GM1X", -29.4, 0, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -29.4, 0, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"GM2X", -34.9, 0, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -34.9, 0, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"GM3X", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"GM4X", -34.3, 0, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -34.3, 0, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    
    
    /*
    // Placement for 2018
    {"MM1X",  -1, 0, -1967.10, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM1Y",  -1, 0, -1967.11, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM2X",  2.1378, -0.3564, -1820.10,  0,  0, 45, 8., 8., 0.2, 320},
    {"MM2Y",  2.1378, -0.3564, -1820.11,  0,  0, 45, 8., 8., 0.2, 320}, 
    {"MM3X",  -31.79, -2.048, -364.60, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM3Y",  -31.79, -2.048, -364.61, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4X",  -32.694, -1.97, -345.20, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4Y",  -32.694, -1.97, -345.21, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM5X",  -37.66, -0.5381, -119.10, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y",  -37.66, -0.5381, -119.11, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM6X", -37.86, -1.5531, -97.90, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM6Y", -37.86, -1.5531, -97.91, 0, 0, -45, 8., 8., 0.2, 320},
    
    {"GM1X", -25.9, 0, -86.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -25.9, 0, -86.31, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2X", -23.3, 0, -160.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -23.3, 0, -160.61, 0, 0, 0, 10., 10., 0.2, 256},   
    {"GM3X", -25.1, 0, -105.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -25.1, 0, -105.21, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4X", -24.2, 0, -143.70, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -24.2, 0, -143.71, 0, 0, 0, 10., 10., 0.2, 256},
    */
    
};

TrackAPV::TrackAPV( calib::Dispatcher & ch
                  , const std::string & only
                  , ObjPool<TrackPoint> & obTP )
        : AbstractHitHandler<APVCluster>(ch, only)
        , _tpInserter(obTP) {
}

void
TrackAPV::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<APVCluster>::handle_update(nm);
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
TrackAPV::process_hit( EventID
                     , DetID_t did
                     , APVCluster & clus ) {
	
	// Create multimap with unique detector (plane) identifier
	// and fill it with APV clusters and corresponding number
	// of clusters in detector plane
	
	auto idxIt = _detHits.find( did );
    if( _detHits.end() == idxIt ) {
        auto ir = _detHits.emplace( did, Clusters() );
        assert( ir.second );
        idxIt = ir.first;
    }
	
	Clusters & clusterByHit = idxIt->second;
	
	clusterByHit.emplace( clusterByHit.size()
					    , _current_event().apvClusters.pool().get_ref_of(clus));
							   
	return true;
}

TrackAPV::ProcRes
TrackAPV::process_event(Event * evPtr) {
    
    // Get reference track
    //Track & track = *(evPtr->tracks.begin()->second);
    
    auto rc = AbstractHitHandler<APVCluster>::process_event( evPtr );
    
    for( auto detEntry : _detHits ) {
		_insert_clusters( *evPtr, detEntry.first, detEntry.second );
    }
    
    _detHits.clear();
    
    return rc;
}
    
void
TrackAPV::_insert_clusters( Event & e 
						  , DetID_t did
						  , Clusters & cluster ) {
	
	for ( auto clus : cluster ) {
		
		TrackPoint tp;
	
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		/*
		//	In general case we might have detectors with a lot of planes,
		//  but here we have unique DetID_t for everyone, so it is not
		//  necessary to use it here, however we specify it to satisfy
		//  generic genfit::PlanarMeasurement constructor
		*/
		int planeId(0);				  

		TVectorD hitCoords(1);
		// ^^^ specifies measurement dimension, for better accuracy we
		// using native for strip detector "one dimension" measurements,
		// but it is disirable to figure out optimal scenario
		TMatrixDSym hitCov(1);	
		
		const DetPlacementEntry * cPlacement = _placements[did];
		
		// TODO: ASK RENAT how to get station projection out of DetID_t

		//std::string stationName = naming()[did];
		DetID stationID(did);

		if ( WireID::proj_label( WireID(stationID.payload()).proj() ) == 'X' ) {
			hitCoords(0) = ((cPlacement->sizeX * (*clus.second).position / cPlacement->numOfWires) 
						 - (( cPlacement->sizeX )/2) );
			tp.lR[0] = hitCoords(0);
			//std::cout << tp.lR[0] << std::endl;
			tp.gR[0] = ( hitCoords(0) * cos( cPlacement->rotAngleZ ) ) + cPlacement->x;
			//std::cout << tp.gR[0] << std::endl;
			
		} else if ( WireID::proj_label( WireID(stationID.payload()).proj() ) == 'Y' ) {
			hitCoords(0) = ((cPlacement->sizeY * (*clus.second).position / cPlacement->numOfWires) 
						 - (( cPlacement->sizeY )/2) );
			tp.lR[1] = hitCoords(0);
			//std::cout << tp.lR[1] << std::endl;
			tp.gR[1] = ( hitCoords(0) * sin( cPlacement->rotAngleZ ) ) + cPlacement->y;
			//std::cout << tp.gR[1] << std::endl;
		}
		
		tp.lR[2] = 0;
		tp.gR[2] = cPlacement->z;
		//std::cout << tp.gR[2] << std::endl;
		
		// Get detector resolution
		double resolution(cPlacement->resolution);
		hitCov(0, 0) = resolution*resolution;

		// Set strip detector planes
		TVector3 o_plane( cPlacement->x
						, cPlacement->y
						, cPlacement->z );
						
		TVector3 u_plane( cPlacement->sizeX
						, 0
						, 0 );
						
		TVector3 v_plane( 0
						, cPlacement->sizeY
						, 0 );

		// Now we have options that we likely have to discuss.
		// Here we see slightly bulky solution that allows to rotate
		// detector plane in all direction based on info from geometry.dat.
		// However, in most cases we have simple rotation around normal,
		// so we might prefer to use genfit::DetPlane::rotate(double angle)?

		if (cPlacement->rotAngleX != 0) {
			u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
			v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
		}

		if (cPlacement->rotAngleY != 0) {
			u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
			v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
		}
			
		if (cPlacement->rotAngleZ != 0) {
			u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
			v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
		}
		

		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
														 , v_plane));
															 
		genfit::AbsMeasurement * measurement 
							   = new genfit::PlanarMeasurement( hitCoords
								   							  , hitCov
															  , did
															  , clus.first
															  , nullptr );
			 
		static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																	  , planeId);

		if ( WireID::proj_label( WireID(stationID.payload()).proj() ) == 'Y' ) {
			static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
		}
		
		
		
		genfitTP->addRawMeasurement( measurement );
		
		tp.charge = (*clus.second).charge;
		
		tp.station = did;
			
		tp.genfitTrackPoint.reset( genfitTP );
	
		*_tpInserter( e, did, naming() ) = tp;

	}
}

}

REGISTER_HANDLER( TrackAPV, banks, ch, cfg
                , "Handler for APV tracking detectors insert in genfit::Track" ) {
    
    return new handlers::TrackAPV( ch
                                 , aux::retrieve_det_selection(cfg)
                                 , banks.of<TrackPoint>() );
}
}

#endif  // defined(GenFit_FOUND)
