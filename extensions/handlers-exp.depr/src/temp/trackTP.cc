#include "tracking/trackTP.hh"

#include "na64detID/wireID.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <KalmanFitterRefTrack.h>
#include <KalmanFitter.h>
#include <DAF.h>

#include <EventDisplay.h>

#include <fstream>

namespace na64dp {
namespace handlers {

TrackTP::TrackTP( calib::Dispatcher & ch
                    , const std::string & only
                    , ObjPool<TrackPoint> & obTP
                    , ObjPool<Track> & bank )
		: AbstractHitHandler<TrackPoint>(ch, only) {
	
	_outputup.open("output.dat");
	
	}

bool
TrackTP::process_hit( EventID
					  , DetID_t did
                      , TrackPoint & tp ) {
	
	genfit::TrackPoint * genfitTP = tp.genfitTrackPoint.get();
	
	genfitTP->Print();
	
	_uniqueIDs.insert( did );
	
	_trackPoints.push_back( genfitTP );
	
	_trackPointsTP.push_back( tp );
		   
	return true;
}

TrackTP::ProcRes
TrackTP::process_event(Event * evPtr) {
	
	AbstractHitHandler<TrackPoint>::process_event( evPtr );
	
	if ( evPtr->srdEdep < 1 || evPtr->srdEdep > 70 ) {
		return kDiscriminateEvent;
	}
	
	if ( _uniqueIDs.size() < 2 ) {
		return kDiscriminateEvent;
	}
	
	Track & track = *(evPtr->tracks.begin()->second);
    
    for ( auto it : _trackPoints ) {
		track.genfitTrackRepr->insertPoint(it);
	}
	
	_uniqueIDs.clear();
	_trackPoints.clear();
	_trackPointsTP.clear();
    
    return kOk;
}

void
TrackTP::finalize() {
	_outputup.close();
}

}

REGISTER_HANDLER( TrackTP, banks, ch, cfg
                , "Handler for hit processing and simple itterative seeding" ) {
    
    return new handlers::TrackTP( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<TrackPoint>()
                             , banks.of<Track>() );
}
}

#endif  // defined(GenFit_FOUND)
