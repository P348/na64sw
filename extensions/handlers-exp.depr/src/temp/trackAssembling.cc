#include "new_tracking/trackAssembling.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <MeasuredStateOnPlane.h>
#include <MeasurementOnPlane.h>
#include <PlanarMeasurement.h>
#include <StateOnPlane.h>

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static TrackAssembling::DetPlacementEntry gPlacements[] = {
    
    // Placement for 2018_invis100
    {"MM1X", -1,    0,  -1967.1,    0,  0,  -45,    8., 8., 0.01,   0.2,    320}, // StationName, X, Y, Z, rotAngleX, rotAngleY, rotAngleZ, sizeX, sizeY, sizeZ, resolution, strip specific number of wires
    {"MM1Y", -1,       0,      -1967.11,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM2X",  2.1378, -0.3564, -1820.1,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
    {"MM2Y",  2.1378, -0.3564, -1820.11,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320}, 
    
    {"MM3X", -31.79,  -0.2048,  -364.6,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM3Y", -31.79,  -0.2048,  -364.61,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM4X", -32.694, -1.9702,  -345.2,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM4Y", -32.694, -1.9702,  -345.21,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"MM5X", -37.66,  -0.5381,  -119.1,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
    {"MM5Y", -37.66,  -0.5381,  -119.11,  0,  0,  45,   8.,  8.,  0.01,   0.2, 320},
      
    {"MM7X", -37.86,  -1.5531,   -97.9,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    {"MM7Y", -37.86,  -1.5531,   -97.91,  0,  0, -45,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM1X", -25.9,         0,   -86.3,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM1Y", -25.9,         0,   -86.31,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM2X", -23.3,         0,  -160.6,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM2Y", -23.3,         0,  -160.61,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM3X", -25.1,         0,  -105.2,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM3Y", -25.1,         0,  -105.21,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"GM4X", -24.2,         0,  -143.7,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    {"GM4Y", -24.2,         0,  -143.71,  0,  0,   0,   8.,  8.,  0.01,   0.2, 320},
    
    {"HOD0X0-0-29", -28.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-1-29", -28.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-2-29", -29.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-3-29", -29.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-4-29", -29.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-5-29", -29.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-6-29", -29.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-7-29", -30.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-8-29", -30.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-9-29", -30.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-10-29",-30.6,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-11-29",-30.8,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-12-29",-31.0,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-13-29",-31.2,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0X0-14-29",-31.4,   0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD0Y0-0-30", -30,    1.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-1-30", -30,    1.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-2-30", -30,    1.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-3-30", -30,    0.8,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-4-30", -30,    0.6,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-5-30", -30,    0.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-6-30", -30,    0.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-7-30", -30,    0.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-8-30", -30,   -0.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-9-30", -30,   -0.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-10-30", -30,  -0.6,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-11-30", -30,  -0.8,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-12-30", -30,  -1.0,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-13-30", -30,  -1.2,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD0Y0-14-30", -30,  -1.4,  -189.8,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD1X0-0-29", -31.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-1-29", -31.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-2-29", -31.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-3-29", -31.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-4-29", -32.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-5-29", -32.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-6-29", -32.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-7-29", -32.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-8-29", -32.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-9-29", -33.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-10-29", -33.35, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-11-29", -33.55, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-12-29", -33.75, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-13-29", -33.95, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1X0-14-29", -34.15, 0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    
    {"HOD1Y0-0-30", -32.75, -1.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-1-30", -32.75, -1.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-2-30", -32.75, -1.0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-3-30", -32.75, -0.8,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-4-30", -32.75, -0.6,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-5-30", -32.75, -0.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-6-30", -32.75, -0.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-7-30", -32.75,    0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-8-30", -32.75,  0.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-9-30", -32.75,  0.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-10-30", -32.75, 0.6,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-11-30", -32.75, 0.8,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-12-30", -32.75, 1.0,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-13-30", -32.75, 1.2,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},
    {"HOD1Y0-14-30", -32.75, 1.4,   -67.5,  0,  0,   0,   3.,  3.,  0.01,   0.3, 15},

};

TrackAssembling::TrackAssembling( calib::Dispatcher & cdsp
                              , const std::string & only
                              , ObjPool<Track> & bank )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _tracksBank(bank) {
}

void
TrackAssembling::handle_update( const nameutils::DetectorNaming & nm ) {
    TDirAdapter::handle_update(nm);
    AbstractHitHandler<TrackPoint>::handle_update(nm);
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        auto ir = _placements.emplace(did, gPlacements + i);
        if( !ir.second ) {
            //std::cerr << "Failed to insert placement for \"" << gPlacements[i].planeName << "\" (id repeated)." << std::endl;
        }
        else {
            //std::cout << "Inserted placement for \"" << gPlacements[i].planeName << "\" id=" << ir.first->first << std::endl;
        }
    }
}

bool
TrackAssembling::process_sadc_hit( DetID detID
                                , const SADCHit & hit ) {
    auto it = _placements.find( detID );
    std::string stationName = TDirAdapter::naming()[detID];

    if (stationName.find("HOD") != std::string::npos) {
		
        // Inelegant solution. Bad wire in 3199, always maxed out.
        // FIXIT!
        if (stationName.find("HOD0X0-2-29") != std::string::npos) {
            return true;
        }
        
		int detIdH(0); // detector ID 
		int planeIdH(0); // detector plane ID
		int hitIdH(0); // hit ID

		TVectorD hitCoords(1);
        
        const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(stationName)];
            
        hitCoords(0) = 0;
                    
		double resolution(cPlacement->resolution);
        TMatrixDSym hitCov(1);
        hitCov(0, 0) = resolution*resolution;
                    
        TVector3 u_plane(1, 0, 0);
        TVector3 v_plane(0, 1, 0);
                    
        genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
        genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
        static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
        
        if (stationName.find("X") != std::string::npos) {
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else if (stationName.find("Y") != std::string::npos){
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }        
        
        _cTrack->checkConsistency();
	}
    

	
	return true;
		
}

TrackAssembling::ProcRes
TrackAssembling::process_event(Event * evPtr) {
    
    _cTrack = new genfit::Track();  // TODO: allocate on bank! Causes leak.
    
    // Start of track assamble routine
    
    int detId(0); // detector ID 
    int planeId(0); // detector plane ID
    int hitId(0); // hit ID
    
    TVectorD hitCoords(1);
    
    for (auto p : evPtr->apvClusters) {
        
        const DetPlacementEntry * cPlacement = _placements[DetID(p.first)];

        std::string stationName = TDirAdapter::naming()[DetID(p.first)];
        
        //std::cout << stationName << std::endl;
        
        if (stationName.find('X') != std::string::npos) {
        
            hitCoords(0) = ((cPlacement->sizeX * p.second->position / cPlacement->numOfWires) - (( cPlacement->sizeX )/2) );
            
            //std::cout << hitCoords(0) << std::endl;
            //std::cout << hitCoords(0) + cPlacement->x << std::endl;
        }
        
        if (stationName.find('Y') != std::string::npos) {
        
            hitCoords(0) = ((cPlacement->sizeY * p.second->position / cPlacement->numOfWires) - (( cPlacement->sizeY )/2) );
            
            if (stationName.find("GEM") != std::string::npos) {
                hitCoords(0) = hitCoords(0)*-1.;
            }
            
            //std::cout << hitCoords(0) << std::endl;
            //std::cout << hitCoords(0) + cPlacement->y << std::endl;
        }        
        
        
        double resolution(cPlacement->resolution);
        TMatrixDSym hitCov(1);
        hitCov(0, 0) = resolution*resolution;
        
        TVector3 u_plane(cPlacement->sizeX, 0, 0);
        TVector3 v_plane(0, cPlacement->sizeY, 0);

        if (cPlacement->rotAngleX != 0) {
            u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
            v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        }

        if (cPlacement->rotAngleY != 0) {
            u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
            v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        }
        
        if (cPlacement->rotAngleZ != 0) {
            u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
            v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        }

        genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
        genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detId, ++hitId, nullptr );
        
        std::string s = TDirAdapter::naming()[p.first];
        if (s.find('X') != std::string::npos) {
            
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else if (s.find('Y') != std::string::npos){
        
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeId);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
        }
        
        else {
            std::cout << "Something went wrong, no X or Y projection" << std::endl;
        }
    }
    
    
    // Iterate over SADC detectors to extract aux info for tracking
    
    for( auto sadcHitPair : evPtr->sadcHits ) {
        process_sadc_hit( sadcHitPair.first
                        , *(sadcHitPair.second) );
    }
    
    // Template to find max value in each hodoscope and to return 
    // ===========================================================
    // =============== HOD0X =====================================
    /*
    int detIdH(0); // detector ID 
    int planeIdH(0); // detector plane ID
    int hitIdH(0); // hit ID
    
    if (_HOD0X.size() > 0) {
    
        using pair_type = decltype(_HOD0X)::value_type;

        auto pr = std::max_element
        (
            std::begin(_HOD0X), std::end(_HOD0X),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << "" << pr->first << "" << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr->first);
        
        if (hodName.find("HOD0X") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    // =============== HOD0Y =====================================
    
    if (_HOD0Y.size() > 0) {
    
        using pair_type = decltype(_HOD0Y)::value_type;

        auto pr1 = std::max_element
        (
            std::begin(_HOD0Y), std::end(_HOD0Y),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << pr1->first << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr1->first);
        
        if (hodName.find("HOD0Y") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
        
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    // =============== HOD1X =====================================
    
    if (_HOD1X.size() > 0) {
    
        using pair_type = decltype(_HOD1X)::value_type;

        auto pr2 = std::max_element
        (
            std::begin(_HOD1X), std::end(_HOD1X),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        if (_gEvent == 3412) {
            std::cout << pr2->first << std::endl;
        }
        
        std::string hodName = static_cast<std::string>(pr2->first);
        
        if (hodName.find("HOD1X") != std::string::npos) {
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }
    }
    
    // =============== HOD1Y =====================================
    
    if (_HOD1Y.size() > 0) {
    
        using pair_type = decltype(_HOD1Y)::value_type;

        auto pr3 = std::max_element
        (
            std::begin(_HOD1Y), std::end(_HOD1Y),
            [] (const pair_type & p1, const pair_type & p2) {
                return p1.second < p2.second;
            }
        );
        
        std::string hodName = static_cast<std::string>(pr3->first);
        
        if (hodName.find("HOD1Y") != std::string::npos) {
        
            const DetPlacementEntry * cPlacement = _placements[TDirAdapter::naming().id(hodName)];
            
            hitCoords(0) = 0;
                    
            double resolution(cPlacement->resolution);
            TMatrixDSym hitCov(1);
            hitCov(0, 0) = resolution*resolution;
                    
            TVector3 u_plane(1, 0, 0);
            TVector3 v_plane(0, 1, 0);
                    
            genfit::SharedPlanePtr plane(new genfit::DetPlane( TVector3( cPlacement->x, cPlacement->y, cPlacement->z ), u_plane, v_plane));
            genfit::AbsMeasurement * measurement = new genfit::PlanarMeasurement( hitCoords, hitCov, ++detIdH, hitIdH, nullptr );
            static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane(plane, planeIdH);
            static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
            _cTrack->insertPoint(new genfit::TrackPoint(measurement, _cTrack));
            _cTrack->checkConsistency();
        }      
    }
        
    // ============================================================================================================================

                std::cout << "GenFit track"
                    << " particle PDG = " << particle
                    << " momentum = " << stLast.getMomMag()
                    << " fit converged = " << fit->isFitConverged()
                    << " fitted charge = " << fit->getCharge()
                    << " fit Chi^2 = " << fit->getChi2()
                    << " fit Ndf = " << fit->getNdf()
                    << " fit Chi^2/Ndf = " << chi2ndf
                    << std::endl;
        }
    }
    */
    // if new track has to be created
    {
        PoolRef<Track> trackRef = _tracksBank.create();
        trackRef->genfitTrackRepr = _cTrack;
        EvFieldTraits<Track>::map(*evPtr).emplace(EvFieldTraits<Track>::map(*evPtr).size(), trackRef);
        // TODO: emplace to other maps, probably returned by
        // EvFieldTraits<Track>::maps_for() or something...
        // TODO: the genfit track representation must further become an
        // instance allocated on shared allocator
    }
    return kOk;
}

REGISTER_HANDLER( TrackAssembling, banks, ch, cfg
                , "Handler for APV and SADC tracking detectors assembling" ) {
    
    return new TrackAssembling( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<Track>() );
}
}
}
#endif  // defined(GenFit_FOUND)
