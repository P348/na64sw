#include "na64dp/abstractHitHandler.hh"
#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace handlers {

/**\brief Obtains clusters centers information
 *
 * Copies information of 1D cluster position to local coordinates vector.
 *
 * \note The role of this handler is not to compute the cluster centers as one
 * would probably expect, but rather to address 1D cluster center offset to a
 * particular local coordinate taking into account information of X/Y or U/V
 * coordinates precedence (e.g. 'X' comes befor 'Y' in spatial coordinates set).
 *
 * \todo Currently only support X/Y and U/V pairwise-adjoint clusters.
 * */
class GetTrackPointsCenters : public AbstractHitHandler<event::TrackPoint> {
public:
    GetTrackPointsCenters(calib::Dispatcher & dsp)
            : AbstractHitHandler<event::TrackPoint>(dsp)
            {}
    /// Fills the 2D track points histograms
    virtual bool process_hit( EventID eid
                            , StationKey did
                            , event::TrackPoint & ) override;
};

//                          * * *   * * *   * * *

using event::TrackPoint;
using event::APVCluster;
// using event::StwTDCHit

bool
GetTrackPointsCenters::process_hit( EventID, StationKey, TrackPoint & tp ) {
    if( 2 == tp.apvClusters.size() ) {
        // Note: we rely on count of clusters rather then on some internal
        // knwoledge of particular detector assembly. This assumption works
        // only for NA64 and only until the times when stations with >2 planes
        // will appear.

        // Write third local coordinate to be NaN
        tp.lR[2] = std::nan("0");
        // Obtain cluster references
        auto it = tp.apvClusters.begin();
        const std::pair<PlaneKey, mem::Ref<APVCluster> > & pA = *it
                                                       , & pB = *(++it);
        assert( &pA != &pB );
        // Retain the wire ID signatures
        DetID didA(pA.first)
            , didB(pB.first);
        WireID wA(didA.payload())
             , wB(didB.payload());
        msg_debug( _log, "Retrieved A %#x, pos: %f, size: %zu"
                  , didA.id, pA.second->position, pA.second->size() );
        msg_debug( _log, "Retrieved B %#x, pos: %f, size: %zu"
                  , didB.id, pB.second->position, pB.second->size() );
        assert( !std::isnan(pA.second->position) );
        assert( !std::isnan(pB.second->position) );
        // Use wire ID signature to re-order the positions wrt the
        // rules: "X prcedes Y" and "U precedes V", copy the `position` value
        // from cluster reference to local coordinates (`lR`)
        if( WireID::kX == wA.proj() || WireID::kU == wA.proj() ) {
            tp.lR[0] = pA.second->position;
            tp.lR[1] = pB.second->position;
        } else {  // pA is kY or kV
            assert( ( WireID::kY == wA.proj() || WireID::kV == wA.proj() ) );
            tp.lR[0] = pB.second->position;
            tp.lR[1] = pA.second->position;
        }
    }
    // else ... TODO: straw hits, apv clusters of # >2, etc
    else {
        // This might be a point for future elaboration in case NA64
        // will introduce redundancy within a single station
        NA64DP_RUNTIME_ERROR( "Can not derive local coordinates from"
                " track point constisting of number of clusters != 2." );
    }
    return true;  // proceed
}

}

REGISTER_HANDLER( GetTrackPointsCenters
                , ch, cfg
                , "Obtains clusters ceters information" ) {
    return new handlers::GetTrackPointsCenters(ch);
}

}

