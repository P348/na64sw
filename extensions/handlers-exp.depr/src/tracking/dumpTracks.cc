#include "na64detID/wireID.hh"
#include "na64detID/TBName.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64calib/dispatcher.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Generates CSV dump of clusters collected for APV detector stations
 *
 * Generated dump is useful for external analysis. Accepts single parameter
 * defining, whether to dump hits defining a cluster.
 *
 * Output format's common part:
 *  - event ID - numerical event identifier
 *  - track ID - numerical track identifier
 *  - cluster number - unique number of cluster within a track
 *  - station name - name of station (e.g. `MM4`, `GM1`)
 *  - projection letter - letter defining projection
 *  - mean wire number - $\sum\limits_i^{N_c} N_{w,i}/N_c$
 *  - mean weighted by charge wire number $(\sum\limits_i^{N_c} q_i N_{w,i})/N_c$
 *  - cluster sparseness value
 * If `writeHits=true`, output of each 1D cluster will be appended with:
 *  - nuber of hits - number of hits composing a cluster
 *  - (repeated) phys wire number
 *  - (repeated) mapped wire number
 *  - (repeated) charge measurement 1st sample
 *  - (repeated) charge measurement 2nd sample
 *  - (repeated) charge measurement 3rd sample
 *
 * \todo "applyTo" ctr argument is currently being ignored
 * */
class APVClusterDump : public AbstractHitHandler<event::Track>
                     , public calib::Handle<nameutils::DetectorNaming>
                     {
private:
    std::ofstream _os;
    bool _writeHits;
public:
    APVClusterDump( calib::Dispatcher & dsp
                  , const std::string & oFilePath
                  , const std::string & only
                  , bool writeHits=false);
    virtual bool process_hit( EventID, size_t, event::Track & ) override;
    virtual void finalize() override;
};

//                      * * *   * * *   * * *

APVClusterDump::APVClusterDump( calib::Dispatcher & cdsp
                              , const std::string & oFilePath
                              , const std::string & only
                              , bool writeHits ) : AbstractHitHandler(cdsp)
                                                 , calib::Handle<nameutils::DetectorNaming>("default", cdsp)
                                                 , _os(oFilePath)
                                                 , _writeHits(writeHits)
                                                 {}

bool
APVClusterDump::process_hit( EventID eid
                           , size_t trackID
                           , event::Track & track ) {
    auto & names = static_cast<const calib::Handle<nameutils::DetectorNaming>*>(this)->operator*();
    for( const std::pair<StationKey, mem::Ref<event::TrackPoint> > & tp
       : track ) {  // for each track point within a track
        for( const auto & clusterPair : tp.second->apvClusters ) {  // for each cluster within a track point
            WireID wid( DetID(clusterPair.first).payload() );
            const event::APVCluster & c = *clusterPair.second;
            _os << eid << ","  // event ID
                << trackID << ","  // track ID
                //<< nCluster << "," // cluster number
                << tp.first.id << "," // track point number
                << names[tp.first] << ","  // detector name
                << WireID::proj_label( wid.proj() )  << "," // projection letter (X, U, etc)
                << c.position << ","
                //<< c.wPosition << ","
                << c.charge << ","
                //<< c.sparseness
                ;
            if( _writeHits ) {
                _os << "," << c.size();
                for( auto hitPair : c ) {
                    _os << ","
                        << hitPair.first << ","  // phys wire ID
                        << hitPair.second->rawData->wireNo << ","  // DAQ wire ID
                        << hitPair.second->rawData->samples[0] << ","  // charge 1
                        << hitPair.second->rawData->samples[1] << ","  // charge 1
                        << hitPair.second->rawData->samples[2]        // charge 1
                        ;
                }
            }
            _os << std::endl;
        }
    }
    return kOk;
}

void
APVClusterDump::finalize() {
    _os.close();
}

}

REGISTER_HANDLER( APVClusterDump
                , ch, cfg
                , "Writes CSV dump of cluster composing track points" ) {
    return new handlers::APVClusterDump( ch
                                       , cfg["outputFile"].as<std::string>()
                                       , aux::retrieve_det_selection(cfg)
                                       , cfg["writeHits"] ? cfg["writeHits"].as<bool>() : false
                                       );
}

}

