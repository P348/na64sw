#include "plotting/dimuon.hh"

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>

namespace na64dp {
namespace handlers {

using event::CaloHit;
using event::Event;

DiMuon::DiMuon( calib::Dispatcher & cdsp
              , double hcalMin
              , double hcalMax
              , double ecalMin
              , double ecalMax )
        : AbstractHitHandler<CaloHit>(cdsp)
        , _eDep(nullptr)
        , _ecalERange{ecalMin, ecalMax}
        , _hcalERange{hcalMin, hcalMax}
        {}

void
DiMuon::handle_update( const nameutils::DetectorNaming & nm ) {
    kECAL = nm["ECAL"];
    kHCAL = nm["HCAL"];
    kVETO = nm["VETO"];  // TODO: do we use this detector?
}

AbstractHandler::ProcRes
DiMuon::process_event(Event & ev) {
    // Get energy deposition for HCAL/ECAL
    double hcalE = ev.caloHits[kHCAL]->eDep
         , ecalE = ev.caloHits[kECAL]->eDep
         ;
    // Check the edep criteria for calos
    if( hcalE < _hcalERange[0] || hcalE > _hcalERange[1]
     || ecalE < _ecalERange[0] || ecalE > _ecalERange[1] ) {
        return kOk;
    }
    // Create histogram if not exist
    if( !_eDep ) {
        _eDep = new TH1F( "eDep"
                        , "VETO energy deposition"
                        , 100, 0, 600  // TODO: configurable
                        );
    }
    // Sum the energy depositions in VETO (it is not available as CaloHit)
    _eDep->Fill( ev.caloHits[kVETO]->eDep );  // todo: use error info if available?
    return kOk;
}

void
DiMuon::finalize(){
    if( _eDep ) _eDep->Write();
}

REGISTER_HANDLER( DiMuon, ch, cfg
                , "Handler to plot VETO for dimuon production"
                " energy deposition" ) {
    return new DiMuon( ch
                     , cfg["EnergyRangeHCAL"][0].as<double>()
                     , cfg["EnergyRangeHCAL"][1].as<double>()
                     , cfg["EnergyRangeECAL"][0].as<double>()
                     , cfg["EnergyRangeECAL"][1].as<double>()
                     );
}
}
}
