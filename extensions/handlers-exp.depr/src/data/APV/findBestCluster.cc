#include "data/APV/findBestCluster.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace handlers {

APVFindBestCluster::APVFindBestCluster( calib::Dispatcher & ch
                                , ObjPool<APVCluster> & bank
                                , const std::string & detSelection
                                , float clusterMinWidth
                                , float hitMinCharge
                                ) : AbstractHitHandler(ch, detSelection)
                                  , TDirAdapter(ch)
                                  , _minClusterWidth(clusterMinWidth)
                                  , _minHitCharge(hitMinCharge) 
                                  , _clusterInserter(bank) {
                                      
    if(_minClusterWidth < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"minWidth\" value provided to"
                " APVClusterAssemble handler." );
    }
    
    if(_minHitCharge < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"minCharge\" value provided to"
                " APVClusterAssemble handler." );
    }

    // fill reverse wire mapping for MMs
    for( int daqWNo = 0; daqWNo < 64; ++daqWNo ) {
        for( int i = 0; i < 5; ++i ) {
            NA64DP_APVStripNo_t realWNo = na64dp_MM_JointWires[daqWNo][i];
            _mmRealToDaqWNo.emplace( realWNo, daqWNo );
        }
    }
}

void
APVFindBestCluster::handle_update(const nameutils::DetectorNaming & nm) {
    AbstractHitHandler<APVHit>::handle_update(nm);
    TDirAdapter::handle_update(nm);
    try {
        _gmKinID = nm.kin_id("GM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find GEM kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
    try {
        _mmKinID = nm.kin_id("MM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find Micromega kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
}


AbstractHandler::ProcRes
APVFindBestCluster::process_event( Event * e ) {
    AbstractHandler::ProcRes pr = AbstractHitHandler<APVHit>::process_event( e );
    for( auto planePair : _hitsCache ) {
        _find_clusters( *e, planePair.first, planePair.second );
    }
    _hitsCache.clear();
    return pr;
}

bool
APVFindBestCluster::process_hit( EventID
                            , DetID_t did
                            , APVHit & cHit ) {
    // iDid -- incomplete detector ID; for APV detectors this will be 
    // defined with chip, kin, station number and projection ID. No wire number
    // is written within iDid
    DetID iDid(EvFieldTraits<APVHit>::uniq_detector_id(did));
    auto cIt = _hitsCache.find(iDid);
    if( _hitsCache.end() == cIt ) {
        // No cache entry for this detector plane found -- create one
        cIt = _hitsCache.emplace( iDid, SortedHits() ).first;
    }
    //auto wIt = cIt->second.find( cHit.rawData.wireNo );
    auto wIt = cIt->second.lower_bound( cHit.rawData.wireNo );
    if( cIt->second.end() != wIt && wIt->first == cHit.rawData.wireNo ) {
        // Hit on this plane with the same wire number already exists at the
        // current event. This should be interpreted as an error since it is
        // technically impossible. Did you forget to clear the cache between
        // events?
        // Note that this "wire number" is not yet a phyiscal one, so we are
        // safe for MuMega detectors that have multiple physical wire connected
        // to single APV chip channel.
        NA64DP_RUNTIME_ERROR( "Multiple APV hits on the same wire." );
    }
    cIt->second.emplace_hint( wIt, cHit.rawData.wireNo, &cHit );
    //cIt->second.emplace( cHit.rawData.wireNo, &cHit );
    return true;
}

size_t
APVFindBestCluster::_find_clusters( Event & e
                               , DetID did
                               , const SortedHits & hitsMap ) {
    
    // Temporary argument to contor cout
    bool debug = false;
    
    NA64DP_DemultiplexingMapping unmap_wire;
    // ^^^ channel-to-phys wire unmapping callback ptr
    std::multimap<NA64DP_APVWireNo_t, APVClustersIndex::Iterator> clustersByChannel;
    // ^^^ Temporary index of existing clusters by their DAQ (channel) numbers
    
    // Obtain detector layout (demultiplexing mapping)
    if( _gmKinID && _gmKinID == did.kin() ) {
        unmap_wire = na64_APV_strip_mapper__identity;
    } else if( _mmKinID && _mmKinID == did.kin() ) {
        unmap_wire = na64_APV_strip_mapper__micromegas_joints;
    } else {
        // APV detector apparently is not a subject for clustering, or some
        // kind of naming clash appeared. Notify user and abrupt the function.
        _log.warn( "No clustering for detector type %#x that is not GEM,"
                " nor Micromega.", did.id );
        return 0;
    }
    assert(unmap_wire);

    std::multimap<NA64DP_APVStripNo_t, const APVHit *> dh;
    // ^^^ map of (possibly duplicating) hits

    // Fill hits map (possibly duplicating)
    for( auto hitPair : hitsMap ) {
        const uint16_t rawWireNo = hitPair.first;  // "virtual" (DAQ) wire no
        const APVHit * hitPtr = hitPair.second;
        
        NA64DP_APVNWires_t nWires;
        // get phys. number of wires (few) that correspond to current virtual
        // wire no; how much is then be written into `nWires'
        const NA64DP_APVStripNo_t * physWireNos = unmap_wire(rawWireNo, &nWires);
        assert(physWireNos);
        // insert each hit into hits-by-phys-wire map
        for( NA64DP_APVNWires_t i = 0; i < nWires; ++i ) {
            dh.emplace( physWireNos[i], hitPtr );
        }
    }

    // Find clusters, relying on `_possibly_belongs_to_cluster()` result.
    // Note that within `dh', the hits are sorted by their real wire number
    // TODO: on the loop end, does it insert the last candidate (?)
    APVCluster c;
    
    std::vector<APVCluster> clusters;
    
    std::string stationName = TDirAdapter::naming()[did];
    
    EvFieldTraits<APVCluster>::reset_hit( c );
        
    size_t nClustersFound = 0;
    for( auto dHitPair : dh ) {
        NA64DP_APVStripNo_t physWireNo = dHitPair.first;
        const APVHit & hit = *(dHitPair.second);
        
        if (debug) {
            std::cout << "Current wNo: " << physWireNo << std::endl;  // XXX
        }
        
        if ( _possibly_belongs_to_cluster(c, physWireNo, hit) ) {
                        
            if ( hit.time == 0 || hit.timeError == 0 ) continue;
            
            c.charge += hit.maxCharge;
            c.wPosition += physWireNo * hit.maxCharge;
            
            c.emplace( physWireNo
                     , e.apvHits.pool().get_ref_of( &hit ) );
                     
            if (debug) std::cout << "  wire added" << std::endl;
            
            continue;
        } else {
            if( c.size() >= _minClusterWidth ) {
                if (debug) {
                    std::cout << "  sequence abrupt, cluster of size "
                              << c.size() << " gone" << std::endl; 
                }
                c.position = _get_cluster_wCenter( c );

                APVCluster cc;
                cc = c;
                clusters.push_back( cc );
            } else {
                if (debug) {
                    std::cout << "  sequence abrupt, cluster of size "
                          << c.size() << " ignored" << std::endl;
                }
            }
            EvFieldTraits<APVCluster>::reset_hit( c );
        }
    }
    if(!c.empty()) {
        if (debug) std::cout << "  end of hits: adding cluster of size " << c.size() << std::endl;
        
        if (c.size() >= _minClusterWidth ) {
            c.position = _get_cluster_wCenter( c );
            
            APVCluster cc;
            cc = c;
            clusters.push_back( cc );
        }
    }
    
    if (debug) {
        std::cout << stationName << std::endl;
        std::cout << "collected clusters: " << clusters.size() << std::endl;
    }
    
    nClustersFound = clusters.size();
    
    // Itterate over all found clusters and get one with highest amplitude
    
    if (!clusters.empty()) {
        
        APVCluster bestCluster = clusters[0];
        
        if ( bestCluster.charge >= _minHitCharge ) {
            *_clusterInserter( e, did, TDirAdapter::naming()) = bestCluster;
        }
    }
    
    //std::cout << "New detector plane" << std::endl;  

    return nClustersFound;
    
}

// Check if the next fired wire is closest

bool
APVFindBestCluster::_possibly_belongs_to_cluster( const APVCluster & clus
                                    , APVStripNo_t wNo
                                    , const APVHit & cHit ) const {
    
    bool debug = false;
    
    if( clus.empty() ) {
        // empty cluster -- let the first hit to be affiliated to it
        return true;
    }
    assert( !clus.empty() );
    const std::pair<APVStripNo_t, PoolRef<APVHit> > & le = *(clus.rbegin());
    
    if (debug) {
        std::cout << "  wires in current cluster: ";
        for( auto & cpair : clus ) {
            std::cout << cpair.first << ", ";
        }
        std::cout << "vs " << wNo;
    }

    bool r = 1 == wNo - le.first;

    if (debug) std::cout << (r ? " (belongs)" : " (does not belong)") << std::endl;

    return r;
}

double
APVFindBestCluster::_get_cluster_center( const APVCluster & cluster ) const {
    assert( !cluster.empty() );
    double clusterCenter = 0;
    
    for( const auto & cHitPair : cluster ) {
        clusterCenter += cHitPair.first;
    }
    clusterCenter /= cluster.size();
    
    return clusterCenter;
}

double
APVFindBestCluster::_get_cluster_wCenter( const APVCluster & cluster ) const {
    assert( !cluster.empty() );
    double clusterCenter = 0;
    double charge = 0;
    
    for( const auto & cHitPair : cluster ) {
        clusterCenter += ( cHitPair.first * cHitPair.second->maxCharge );
        charge += cHitPair.second->maxCharge;
        
    }
    clusterCenter /= charge;
    
    return clusterCenter;
}

}

REGISTER_HANDLER( APVFindBestCluster, banks, ch, yamlNode
                , "Code for APV hit processing and clusters assembling" ) {
    return new handlers::APVFindBestCluster( ch, banks.of<APVCluster>()
                              , aux::retrieve_det_selection(yamlNode)
                              , yamlNode["minWidth"].as<float>()
                              , yamlNode["minCharge"].as<float>()
                              );
}

}

