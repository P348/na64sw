#include "data/TrackPoint/genfitCalo.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

namespace na64dp {
namespace handlers {

static GenfitTrackPointCalo::DetPlacementEntry gPlacements[] = {
	
	// Placement for 2017 beamcounters
	// StationName, X, Y, Z, resolution
    {"S0:0-0--",     0, 0, -1981.7,  2.5},
    {"S1:0-0--",     0, 0, -1883.35, 2.5},
    {"S2:0-0--", -27.6, 0,   -411.5, 3.2},
    {"S3:0-0--", -32.6, 0,   -198.7, 3.2},
    {"S4:0-0--",   -35, 0,      -77, 3.2},
	{"V1:0-0--",   -80, 0,    -52.5, 22 },
    {"V2:0-0--",   -39, 0,       65, 16 },
    {"T2:0-0--",   -35, 0,      -77, 3.2},   
            
    {"HCAL0:0-0--", -20.5, -20, 82, 20},
    {"HCAL0:1-0--", -40.5, -20, 82, 20},
    {"HCAL0:2-0--", -60.5, -20, 82, 20},
    {"HCAL0:0-1--", -20.5,   0, 82, 20},
    {"HCAL0:1-1--", -40.5,   0, 82, 20},
    {"HCAL0:2-1--", -60.5,   0, 82, 20},
    {"HCAL0:0-2--", -20.5,  20, 82, 20},
    {"HCAL0:1-2--", -40.5,  20, 82, 20},
    {"HCAL0:2-2--", -60.5,  20, 82, 20},
    
    {"HCAL1:0-0--", -24.5, -20, 255, 20},
    {"HCAL1:1-0--", -44.5, -20, 255, 20},
    {"HCAL1:2-0--", -64.5, -20, 255, 20},
    {"HCAL1:0-1--", -24.5,   0, 255, 20},
    {"HCAL1:1-1--", -44.5,   0, 255, 20},
    {"HCAL1:2-1--", -64.5,   0, 255, 20},
    {"HCAL1:0-2--", -24.5,  20, 255, 20},
    {"HCAL1:1-2--", -44.5,  20, 255, 20},
    {"HCAL1:2-2--", -64.5,  20, 255, 20},
    
    {"HCAL2:0-0--", -40, -20, 412, 20},
    {"HCAL2:1-0--", -60, -20, 412, 20},
    {"HCAL2:2-0--", -80, -20, 412, 20},
    {"HCAL2:0-1--", -40,   0, 412, 20},
    {"HCAL2:1-1--", -60,   0, 412, 20},
    {"HCAL2:2-1--", -80,   0, 412, 20},
    {"HCAL2:0-2--", -40,  20, 412, 20},
    {"HCAL2:1-2--", -60,  20, 412, 20},
    {"HCAL2:2-2--", -80,  20, 412, 20},
    
    {"HCAL3:0-0--", -20, -20, 412, 20},
    {"HCAL3:1-0--",   0, -20, 412, 20},
    {"HCAL3:2-0--",  20, -20, 412, 20},
    {"HCAL3:0-1--", -20,   0, 412, 20},
    {"HCAL3:1-1--",   0,   0, 412, 20},
    {"HCAL3:2-1--",  20,   0, 412, 20},
    {"HCAL3:0-2--", -20,  20, 412, 20},
    {"HCAL3:1-2--",   0,  20, 412, 20},
    {"HCAL3:2-2--",  20,  20, 412, 20},   
    
    // TODO: Implement VETOs
    #if 0
    {"VETO:0-0--",   0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:1-0--",   0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:2-0--",  -0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:3-0--",  -0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:4-0--",   0, 1, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    {"VETO:5-0--",   0, 0, 81, 0, 0, 0, 10, 20, 20, 20, 0},
    #endif
    
    {"MUON0:0-0--", -40.5, 0,  80, 20},
    {"MUON1:0-0--", -44.5, 0, 255, 20},
    {"MUON2:0-0--", -60.0, 0, 412, 20},
    {"MUON3:0-0--", -60.0, 0, 577, 20},

};

GenfitTrackPointCalo::GenfitTrackPointCalo( calib::Dispatcher & ch
                      , const std::string & only
                      , ObjPool<TrackPoint> & obTP
                      , int threshold )
        : AbstractHitHandler<SADCHit>(ch, only)
        , _tpInserter(obTP)
        , _threshold(threshold) {

	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
    #if 1
    if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
    #endif			

}

void
GenfitTrackPointCalo::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);
 
    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.hodKinCode = nm.kin_id("HOD").second;
    _namingCache.srdKinCode = nm.kin_id("SRD").second;
    _namingCache.vetoKinCode = nm.kin_id("VETO").second;
    _namingCache.ecalKinCode = nm.kin_id("ECAL").second;
    _namingCache.ecalsumKinCode = nm.kin_id("ECALSUM").second;
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gPlacements)/sizeof(DetPlacementEntry)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
GenfitTrackPointCalo::process_hit( EventID
					             , DetID_t did
                                 , SADCHit & hit ) {
	
	// Process calorimeters hits
	
	DetID station(did);
		
	if ( _namingCache.sadcChipCode != station.chip() ) {
		NA64DP_RUNTIME_ERROR( "Non sadc detector identified with SADC chip" );
	}

	if ( _namingCache.hodKinCode == station.kin() ) {
		return true;
	}
	
	// TEMPORARY!
	if ( _namingCache.srdKinCode == station.kin() ) {
		return true;
	}
	
	if ( _namingCache.vetoKinCode == station.kin() ) {
		return true;
	}
	
	if ( _namingCache.ecalKinCode == station.kin() ) {
		return true;
	}

	if ( _namingCache.ecalsumKinCode == station.kin() ) {
		return true;
	}
	
	#if 1
	std::cout << naming()[did] << ": "
	          << "hit position: " << hit.maxSample << ", "
	          << "hit value: " << hit.maxValue << std::endl;
	#endif
	
	TrackPoint tp;
	
	genfit::TrackPoint genfitTP;
				
	int planeId(0);
	int hitId(0);  
		
	TVectorD hitCoords(2);
	TMatrixDSym hitCov(2);	
		
	const DetPlacementEntry * cPlacement = _placements[did];
		
	double resolution(cPlacement->resolution);
		
	// Set strip detector planes
	TVector3 o_plane( cPlacement->x
					, cPlacement->y
					, cPlacement->z );
	TVector3 u_plane( 1, 0, 0 );
	TVector3 v_plane( 0, 1, 0 );
		
			
	hitCoords(0) = 0;
	hitCoords(1) = 0;
	// Set local coordinates
	tp.lR[0] = hitCoords(0);
	tp.lR[1] = hitCoords(1);
	tp.lR[2] = 0;
	
	hitCov(0, 0) = resolution*resolution;
	hitCov(1, 1) = resolution*resolution;
			
	genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
													 , u_plane
												     , v_plane));
																 
	genfit::AbsMeasurement * measurement 
			= new genfit::PlanarMeasurement( hitCoords
										   , hitCov
										   , did
										   , hitId
										   , nullptr );
																  
	static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																  , planeId );
																		   
	genfitTP.addRawMeasurement( measurement );
		
	tp.genfitTrackPoint = genfitTP;
		
	// TrackPoint coordinates
	tp.gR[0] = cPlacement->x;
	tp.gR[1] = cPlacement->y;
	tp.gR[2] = cPlacement->z;

	tp.station = did;
	
	_hits.emplace( did, tp );
		   
	return true;
}

GenfitTrackPointCalo::ProcRes
GenfitTrackPointCalo::process_event(Event * evPtr) {

	_hits.clear();

	AbstractHitHandler<SADCHit>::process_event( evPtr );

	for ( auto & it : _hits ) {
		*_tpInserter( *evPtr, it.first, naming() ) = it.second;	
	}

	return kOk;
}
}

REGISTER_HANDLER( GenfitTrackPointCalo, banks, ch, cfg
                , "Handler for calorimeter hits processing" 
                " and positioning") {
    
    return new handlers::GenfitTrackPointCalo( ch
								   , aux::retrieve_det_selection(cfg)
								   , banks.of<TrackPoint>()
								   , cfg["threshold"].as<int>() );
}
}
#endif  // defined(GenFit_FOUND)
