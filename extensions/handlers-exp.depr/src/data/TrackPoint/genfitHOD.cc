#include "data/TrackPoint/genfitHOD.hh"

#include "na64detID/wireID.hh"

#include "na64util/str-fmt.hh"


#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

namespace na64dp {
namespace handlers {

static GenfitTrackPointHOD::DetPlacementEntry gPlacements[] = {
    
    // Placements for hodoscopes
    {"HOD0X0-0-0", -34.3, 0, -189.8, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    {"HOD1X0-0-0", -37.0, 0,  -67.5, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    
};

GenfitTrackPointHOD::GenfitTrackPointHOD( calib::Dispatcher & ch
										, const std::string & only )
		: AbstractHitHandler<TrackPoint>(ch, only) {
    ch.subscribe<nameutils::DetectorNaming>(*this, "default");
    #if 1
    if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
    #endif
}

void
GenfitTrackPointHOD::handle_update( const nameutils::DetectorNaming & nm ) {
	AbstractHitHandler<TrackPoint>::handle_update(nm);
	
    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.hodKinCode = nm.kin_id("HOD").second;
    assert( _namingCache.sadcChipCode == nm.kin_id("HOD").first );
    
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
GenfitTrackPointHOD::process_hit( EventID
							    , DetID_t did
								, TrackPoint & tp ) {
	
	DetID station(did);
	
	if ( _namingCache.sadcChipCode == station.chip() ) {
	
		if ( _namingCache.hodKinCode == station.kin() ) {
		
			genfit::TrackPoint genfitTP;
		
			const DetPlacementEntry * cPlacement = _placements[did];  
			
			TVectorD hitCoordsX(1), hitCoordsY(1);
			TMatrixDSym hitCov(1);
			
			hitCoordsX(0) = tp.lR[0];
			hitCoordsY(0) = tp.lR[1];
			
			double resolution = cPlacement->resolution;
			hitCov(0, 0) = resolution*resolution;
			
			// Rotate detector planes according data from detector
			// placement
			TVector3 o_plane, u_plane, v_plane;
			_rotate_detector_plane( cPlacement
								  , o_plane
								  , u_plane
								  , v_plane )
								  ;
								  
			int planeId(0);
						
			genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
															 , u_plane
															 , v_plane));
			// for x plane
			_create_measurement( genfitTP, plane, hitCoordsX, hitCov
			                   , did, planeId, false );	
			// for y plane                   
			_create_measurement( genfitTP, plane, hitCoordsY, hitCov
			                   , did, ++planeId, true );																 

			tp.genfitTrackPoint = genfitTP;
		}
	}

	return true;
									
}

void
GenfitTrackPointHOD::_create_measurement( genfit::TrackPoint & tp
                                        , genfit::SharedPlanePtr & plane
										, TVectorD & hitCoord
										, TMatrixDSym & hitCov
										, DetID_t did
										, int & planeId
										, bool vStrip ) {

	genfit::AbsMeasurement * measurement
					= new genfit::PlanarMeasurement( hitCoord
												   , hitCov
												   , did
												   , planeId
												   , nullptr );
	
	static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																  , planeId);
	
	if ( vStrip ) {
		static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
	}
	
	tp.addRawMeasurement( measurement );

}
										

void
GenfitTrackPointHOD::_rotate_detector_plane( const DetPlacementEntry * cPlacement
										   , TVector3 & o_plane
										   , TVector3 & u_plane
										   , TVector3 & v_plane ) {
		
		o_plane.SetXYZ( cPlacement->x, cPlacement->y, cPlacement->z );	
		u_plane.SetXYZ( cPlacement->sizeX, 0, 0 );
		v_plane.SetXYZ( 0, cPlacement->sizeY, 0 );
		
		if (cPlacement->rotAngleX != 0) {
			u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
			v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
		}

		if (cPlacement->rotAngleY != 0) {
			u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
			v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
		}
			
		if (cPlacement->rotAngleZ != 0) {
			u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
			v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
		}		
									   
}

}

REGISTER_HANDLER( GenfitTrackPointHOD, banks, ch, cfg
                , "Handler for Hodoscope genfit::TrackPoint creation" ) {
    
    return new handlers::GenfitTrackPointHOD( ch
                                 , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
