#include "data/TrackPoint/genfitAPV.hh"

#include "na64detID/wireID.hh"

#include "na64util/str-fmt.hh"

#if defined(GenFit_FOUND) && GenFit_FOUND

// NOTE: wrong tdecl order in GenFit -- requires following headers to be
// included prior to genfit ones
#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>

namespace na64dp {
namespace handlers {

static GenfitTrackPointAPV::DetPlacementEntry gPlacements[] = {

    // Hypothetical placement
    {"MM1X", -1.81+3.5793, -0.616+1.62146, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    {"MM1Y", -1.81+3.5793, -0.616+1.62146, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
        
    {"MM2X", -0.909+1.53395, -0.582+1.03184, -1958.00,  0,  0, 135, 8., 8., 0.2, 320},
    {"MM2Y", -0.909+1.53395, -0.582+1.03184, -1958.00,  0,  0, 135, 8., 8., 0.2, 320}, 
    
    {"MM3X", -1.83+3.65796, -0.13+0.589623, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM3Y", -1.83+3.65796, -0.13+0.589623, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
        
    {"MM4X", -0.38+0.511328, -0.8+1.76887, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    {"MM4Y", -0.38+0.511328, -0.8+1.76887, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
        
    {"MM5X", -32.51+3.741, -0.85+1.62146, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y", -32.51+3.741, -0.85+1.62146, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
         
    {"MM7X", -36.41+1.142, -0.025+0.294811, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM7Y", -36.41+1.142, -0.025+0.294811, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
        
    {"GM1X", -29.4, 0+1.47406, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -29.4, 0+1.47406, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM2X", -34.9+1.6149, 0+0.589623, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -34.9+1.6149, 0+0.589623, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM3X", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM4X", -34.3+0.4352, 0+1.76887, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -34.3+0.4352, 0+1.76887, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"MM1", -1.81+3.5793, -0.616+1.62146, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    {"MM2", -0.909+1.53395, -0.582+1.03184, -1958.00,  0,  0, 135, 8., 8., 0.2, 320}, 
    {"MM3", -1.83+3.65796, -0.13+0.589623, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM4", -0.38+0.511328, -0.8+1.76887, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    {"MM5", -32.51+3.741, -0.85+1.62146, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM7", -36.41+1.142, -0.025+0.294811, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"GM1", -29.4, 0+1.47406, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2", -34.9+1.6149, 0+0.589623, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4", -34.3+0.4352, 0+1.76887, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    
    #if 0
    // Placement for 2017
    // StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // sizeX, sizeY, 
    // resolution, number of wires
    
    {"MM1X", -1.81, -0.616, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    {"MM1Y", -1.81, -0.616, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
        
    {"MM2X", -0.909, -0.582, -1958.00,  0,  0, 135, 8., 8., 0.2, 320},
    {"MM2Y", -0.909, -0.582, -1958.00,  0,  0, 135, 8., 8., 0.2, 320}, 
    
    {"MM3X", -1.83, -0.13, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM3Y", -1.83, -0.13, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
        
    {"MM4X", -0.38, -0.8, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    {"MM4Y", -0.38, -0.8, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
        
    {"MM5X", -32.51, -0.85, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y", -32.51, -0.85, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
         
    {"MM7X", -36.41, -0.025, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM7Y", -36.41, -0.025, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
        
    {"GM1X", -29.4, 0, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -29.4, 0, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM2X", -34.9, 0, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -34.9, 0, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM3X", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
        
    {"GM4X", -34.3, 0, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -34.3, 0, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"MM1", -1.81, -0.616, -1967.00, 0,  0,  45, 8., 8., 0.2, 320},
    {"MM2", -0.909, -0.582, -1958.00,  0,  0, 135, 8., 8., 0.2, 320}, 
    {"MM3", -1.83, -0.13, -1822.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM4", -0.38, -0.8, -1813.00, 0, 0, 135, 8., 8., 0.2, 320},
    {"MM5", -32.51, -0.85, -317.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM7", -36.41, -0.025, -103.00, 0, 0, 45, 8., 8., 0.2, 320},
    {"GM1", -29.4, 0, -300.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2", -34.9, 0, -112.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3", -34.2, 0, -238.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4", -34.3, 0, -136.00, 0, 0, 0, 10., 10., 0.2, 256},
    
    
    // 2018 Placements
    {"MM1",  -1, 0, -1967.10, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM2",  2.1378, -0.3564, -1820.10,  0,  0, 45, 8., 8., 0.2, 320},
    {"MM3",  -31.79, -2.048, -364.60, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4",  -32.694, -1.97, -345.20, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM5",  -37.66, -0.5381, -119.10, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM6", -37.86, -1.5531, -97.90, 0, 0, -45, 8., 8., 0.2, 320},
    
    {"GM1", -25.9, 0, -86.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2", -23.3, 0, -160.60, 0, 0, 0, 10., 10., 0.2, 256},  
    {"GM3", -25.1, 0, -105.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4", -24.2, 0, -143.70, 0, 0, 0, 10., 10., 0.2, 256},
    
    {"MM1X",  -1, 0, -1967.10, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM1Y",  -1, 0, -1967.11, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM2X",  2.1378, -0.3564, -1820.10,  0,  0, 45, 8., 8., 0.2, 320},
    {"MM2Y",  2.1378, -0.3564, -1820.11,  0,  0, 45, 8., 8., 0.2, 320}, 
    {"MM3X",  -31.79, -2.048, -364.60, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM3Y",  -31.79, -2.048, -364.61, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4X",  -32.694, -1.97, -345.20, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4Y",  -32.694, -1.97, -345.21, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM5X",  -37.66, -0.5381, -119.10, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y",  -37.66, -0.5381, -119.11, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM6X", -37.86, -1.5531, -97.90, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM6Y", -37.86, -1.5531, -97.91, 0, 0, -45, 8., 8., 0.2, 320},
    
    {"GM1X", -25.9, 0, -86.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -25.9, 0, -86.31, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2X", -23.3, 0, -160.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -23.3, 0, -160.61, 0, 0, 0, 10., 10., 0.2, 256},   
    {"GM3X", -25.1, 0, -105.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -25.1, 0, -105.21, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4X", -24.2, 0, -143.70, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -24.2, 0, -143.71, 0, 0, 0, 10., 10., 0.2, 256},
    #endif
    
};

GenfitTrackPointAPV::GenfitTrackPointAPV( calib::Dispatcher & ch
                                        , const std::string & only )
        : AbstractHitHandler<TrackPoint>(ch, only) {
    ch.subscribe<nameutils::DetectorNaming>(*this, "default");
    #if 1
    if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
    #endif
}

void
GenfitTrackPointAPV::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<TrackPoint>::handle_update(nm);
    
    _names = &nm;
    _namingCache.apvChipCode = nm.chip_id("APV");
    _namingCache.gmKinCode = nm.kin_id("GM").second;
    _namingCache.mmKinCode = nm.kin_id("MM").second;
    assert( _namingCache.apvChipCode == nm.kin_id("GM").first );
    assert( _namingCache.apvChipCode == nm.kin_id("MM").first );
    
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
GenfitTrackPointAPV::process_hit( EventID
                                , DetID_t did
                                , TrackPoint & tp ) {
    
    DetID station(did);
        
    if ( _namingCache.apvChipCode == station.chip() ) { 
        if (  _namingCache.gmKinCode == station.kin()
          ||  _namingCache.mmKinCode == station.kin() ) {
            genfit::TrackPoint genfitTP;
            for ( auto & hit : tp.clusterRefs ) {
                DetID didStrip(hit.first);
                const DetPlacementEntry * cPlacement = _placements[didStrip];
                TVectorD hitCoords(1);
                TMatrixDSym hitCov(1);
                if (WireID::proj_label( WireID(didStrip.payload()).proj() ) == 'X' ) {   
                    tp.lR[0] = ((cPlacement->sizeX * (*hit.second).position / cPlacement->numOfWires) 
                             - (( cPlacement->sizeX )/2) );
                    tp.charge += (*hit.second).charge;
                    hitCoords(0) = tp.lR[0];
                }
                if (WireID::proj_label( WireID(didStrip.payload()).proj() ) == 'Y' ) {
                    tp.lR[1] = ((cPlacement->sizeY * (*hit.second).position / cPlacement->numOfWires) 
                             - (( cPlacement->sizeY )/2) );
                    tp.charge += (*hit.second).charge;
                    hitCoords(0) = tp.lR[1];
                }
                tp.lR[2] = 0;
                double resolution = cPlacement->resolution;
                hitCov(0, 0) = resolution*resolution;
                // Rotate detector planes according data from detector
                // placement
                TVector3 o_plane(0,0,0), u_plane(0,0,0), v_plane(0,0,0);
                _rotate_detector_plane( cPlacement
                                      , o_plane
                                      , u_plane
                                      , v_plane )
                                      ;
                int planeId(0);
                if (WireID::proj_label( WireID(didStrip.payload()).proj() ) == 'Y' ) {
                    ++planeId;
                }
                genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
                                                                 , u_plane
                                                                 , v_plane));
                                                                 
                // for x plane
                _create_measurement( genfitTP, plane, hitCoords, hitCov
                                   , didStrip, planeId );
            }
            const DetPlacementEntry * fPlacement = _placements[did];
            const double angle = fPlacement->rotAngleZ;
            tp.gR[0] = ( tp.lR[0] * cos( angle * M_PI / 180.0 ) - tp.lR[1] * sin ( angle * M_PI / 180.0 ) ) 
                     + fPlacement->x;
            tp.gR[1] = ( tp.lR[1] * cos( angle * M_PI / 180.0 ) + tp.lR[0] * sin ( angle * M_PI / 180.0 ) ) 
                     + fPlacement->y;
            tp.gR[2] = fPlacement->z;
            tp.genfitTrackPoint = genfitTP;
        }
    }
    return true;
                                    
}

void
GenfitTrackPointAPV::_create_measurement( genfit::TrackPoint & tp
                                        , genfit::SharedPlanePtr & plane
                                        , TVectorD & hitCoord
                                        , TMatrixDSym & hitCov
                                        , DetID did
                                        , int & planeId ) {

    genfit::AbsMeasurement * measurement
                    = new genfit::PlanarMeasurement( hitCoord
                                                   , hitCov
                                                   , did
                                                   , planeId
                                                   , nullptr );
    
    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
                                                                  , planeId);
                                                                  
    if (WireID::proj_label( WireID(did.payload()).proj() ) == 'Y' ) {                                                   
        static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
    }
    
    tp.addRawMeasurement( measurement );
}

void
GenfitTrackPointAPV::_rotate_detector_plane( const DetPlacementEntry * cPlacement
                                           , TVector3 & o_plane
                                           , TVector3 & u_plane
                                           , TVector3 & v_plane ) {
        
        o_plane.SetXYZ( cPlacement->x, cPlacement->y, cPlacement->z );  
        u_plane.SetXYZ( cPlacement->sizeX, 0, 0 );
        v_plane.SetXYZ( 0, cPlacement->sizeY, 0 );
        
        if (cPlacement->rotAngleX != 0) {
            u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
            v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        }

        if (cPlacement->rotAngleY != 0) {
            u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
            v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        }
            
        if (cPlacement->rotAngleZ != 0) {
            u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
            v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        }       
                                       
}

}

REGISTER_HANDLER( GenfitTrackPointAPV, banks, ch, cfg
                , "Handler for APV tracking detectors genfit::TrackPoint creation" ) {
    
    return new handlers::GenfitTrackPointAPV( ch
                                , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
