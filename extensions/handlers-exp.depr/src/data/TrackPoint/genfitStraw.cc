#include "data/TrackPoint/genfitStraw.hh"

#include "na64detID/wireID.hh"

#include "na64util/str-fmt.hh"


#if defined(GenFit_FOUND) && GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>
#include <WireMeasurementNew.h>
#include <WireMeasurement.h>

namespace na64dp {
namespace handlers {

static GenfitTrackPointStraw::DetPlacementEntry gPlacements[] = {
    
    // TODO: add coodrinates for straw!
    {"ST04U", -5.945, -9.540, 1684.13,  0,  0,  18,   20,   20,   0.6,   64, 0.6, 0.1 },
    {"ST05V",  5.660, -9.975, 1701.20,  0,  0, -18,   20,   20,   0.6,   64, 0.6, 0.1 },

};

GenfitTrackPointStraw::GenfitTrackPointStraw( calib::Dispatcher & ch
										    , const std::string & only )
		: AbstractHitHandler<TrackPoint>(ch, only) {
    ch.subscribe<nameutils::DetectorNaming>(*this, "default");
    #if 1
    if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
    #endif
}

void
GenfitTrackPointStraw::handle_update( const nameutils::DetectorNaming & nm ) {
	AbstractHitHandler<TrackPoint>::handle_update(nm);
	
    _names = &nm;
    _namingCache.stwtdcChipCode = nm.chip_id("NA64TDC");
    _namingCache.stwKinCode = nm.kin_id("ST").second;
    assert( _namingCache.stwtdcChipCode == nm.kin_id("ST").first );
    
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
GenfitTrackPointStraw::process_hit( EventID
							    , DetID_t did
								, TrackPoint & tp ) {
	
	DetID station(did);
	
	if ( _namingCache.stwtdcChipCode == station.chip() ) {
	
		if ( _namingCache.stwKinCode == station.kin() ) {
			
			
			// TODO: Handler have to be redone for STRAW measurement
			// seems like a good job for Tatyana
			genfit::TrackPoint genfitTP;
		
			const DetPlacementEntry * cPlacement = _placements[did];  
			
			// X plane
			TVectorD hitCoordsX(7);
			hitCoordsX(0) = (-cPlacement->sizeX / 2);
			hitCoordsX(1) = (tp.lR[1]);
			hitCoordsX(2) = (tp.lR[2]);

			hitCoordsX(3) = (cPlacement->sizeX / 2);
			hitCoordsX(4) = (tp.lR[1]);
			hitCoordsX(5) = (tp.lR[2]);

			hitCoordsX(6) = (cPlacement->driftDistance);
			
			// Y plane
			TVectorD hitCoordsY(7);
			hitCoordsY(0) = (tp.lR[0]);
			hitCoordsY(1) = (-cPlacement->sizeY / 2);
			hitCoordsY(2) = (tp.lR[2] + 10); // TODO:have to check position!

			hitCoordsY(3) = (tp.lR[0]);
			hitCoordsY(4) = (cPlacement->sizeY / 2);
			hitCoordsY(5) = (tp.lR[2] + 10);

			hitCoordsY(6) = (cPlacement->driftDistance);			
			
			TMatrixDSym hitCov(7);
			double resolution = cPlacement->resolution;
			hitCov(6,6) = resolution*resolution;
			
			#if 0
			TVector3 endPointX1(-cPlacement->sizeX / 2, tp.lR[1], tp.lR[2]);
			TVector3 endPointX2(cPlacement->sizeX / 2, tp.lR[1], tp.lR[2]);
			TVector3 endPointY1(tp.lR[0], cPlacement->sizeY / 2, tp.lR[2]);
			TVector3 endPointY2(tp.lR[0], -cPlacement->sizeY / 2, tp.lR[2]);
			#endif
			double driftDistance(cPlacement->driftDistance);
			double driftDistanceError(cPlacement->driftDistanceError);			
			
			
			
			// Rotate detector planes according data from detector
			// placement
			TVector3 o_plane, u_plane, v_plane;
			_rotate_detector_plane( cPlacement
								  , o_plane
								  , u_plane
								  , v_plane )
								  ;
								  
			//int planeId(0);
			int hitId(0);
						
			genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
															 , u_plane
															 , v_plane));
									 
			// for x plane
			_create_measurement( genfitTP, hitCoordsX, hitCov
							   , did, hitId);
			// for y plane                   
			_create_measurement( genfitTP, hitCoordsY, hitCov
							   , did, ++hitId);																 

			tp.genfitTrackPoint = genfitTP;
		}
	}

	return true;
									
}

void
GenfitTrackPointStraw::_create_measurement( genfit::TrackPoint & tp
										  , TVectorD & rawHitCoords
										  , TMatrixDSym & rawHitCov
										  , DetID_t did 
										  , int hitId ) {
	
	genfit::AbsMeasurement * measurement
					= new genfit::WireMeasurement( rawHitCoords
												 , rawHitCov
												 , did
												 , hitId
												 , nullptr );
	
	static_cast<genfit::WireMeasurement*>(measurement)->setLeftRightResolution(0);
		 
	tp.addRawMeasurement( measurement );

}


void
GenfitTrackPointStraw::_create_measurement_new( genfit::TrackPoint & tp
										      , const double& driftDistance
										      , const double& driftDistanceError
										      , const TVector3& endPoint1
										      , const TVector3& endPoint2
										      , DetID_t did 
										      , int hitId) {
										
	genfit::AbsMeasurement * measurement
					= new genfit::WireMeasurementNew( driftDistance //?
													, driftDistanceError//?
													, endPoint1
											    	, endPoint2
													, did
													, hitId
													, nullptr );
	
	tp.addRawMeasurement( measurement );

}
										

void
GenfitTrackPointStraw::_rotate_detector_plane( const DetPlacementEntry * cPlacement
										   , TVector3 & o_plane
										   , TVector3 & u_plane
										   , TVector3 & v_plane ) {
		
		o_plane.SetXYZ( cPlacement->x, cPlacement->y, cPlacement->z );	
		u_plane.SetXYZ( cPlacement->sizeX, 0, 0 );
		v_plane.SetXYZ( 0, cPlacement->sizeY, 0 );
		
		if (cPlacement->rotAngleX != 0) {
			u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
			v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
		}

		if (cPlacement->rotAngleY != 0) {
			u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
			v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
		}
			
		if (cPlacement->rotAngleZ != 0) {
			u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
			v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
		}		
									   
}

}

REGISTER_HANDLER( GenfitTrackPointStraw, banks, ch, cfg
                , "Handler for Straw genfit::TrackPoint creation" ) {
    
    return new handlers::GenfitTrackPointStraw( ch
                                 , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
