#include "data/CaloEDep/collectHits.hh"

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

CaloEDepCollect::CaloEDepCollect( calib::Dispatcher & ch
                                , const std::string & only
                                , ObjPool<CaloEDep> & bank
		) : AbstractHitHandler<SADCHit>(ch, only)
		  , _caloInserter(bank) {
			  
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
	if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
}

void
CaloEDepCollect::handle_update( const nameutils::DetectorNaming & nm ) {
	AbstractHitHandler<SADCHit>::handle_update(nm);
	
    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.hodKinCode = nm.kin_id("HOD").second;
    _namingCache.ecalKinCode = nm.kin_id("ECAL").second;
}

bool
CaloEDepCollect::process_hit( EventID
			     		    , DetID_t did
                            , SADCHit & hit ) {
	
	DetID stationID(did);
	
	// check if station uses mSADC chip, otherwise fatal error
	if ( _namingCache.sadcChipCode != stationID.chip() ) {
		NA64DP_RUNTIME_ERROR( "Calorimeter has no sadc type hit" );
	}
	
	// if hit belongs to hodoscope - descriminate
	if ( _namingCache.hodKinCode == stationID.kin() ) {
		return false;
	}
	
	// separate ECAL and preshower
	if ( _namingCache.ecalKinCode == stationID.kin() ) {
		stationID = _create_unique_det_ecal(stationID);
	} else {
		// reset payload to account hits in one calorimeter station
		stationID = _create_unique_det(stationID);
	}
	
	auto idxIt = _sadcHits.find( stationID );
	if( _sadcHits.end() == idxIt ) {
		auto ir = _sadcHits.emplace( stationID, Hits() );
		assert( ir.second );
		idxIt = ir.first;
	}
	
	Hits &hitMap = idxIt->second;
	
	hitMap.emplace( did
			, _current_event().sadcHits.pool().get_ref_of(hit) );
	
	return true;
}

DetID
CaloEDepCollect::_create_unique_det( DetID did ) {
	
	CellID cid(did.payload());
	cid.set_x(0);
	cid.set_y(0);
	cid.set_z(0);
	did.payload(cid.cellID);
	
	return did;
}
// Exception for ECAL to account preshower
DetID
CaloEDepCollect::_create_unique_det_ecal( DetID did ) {
	
	CellID cid(did.payload());
	cid.set_x(0);
	cid.set_y(0);
	did.payload(cid.cellID);
	
	return did;
}	

CaloEDepCollect::ProcRes
CaloEDepCollect::process_event(Event * evPtr) {
    
    _sadcHits.clear();
    
    AbstractHitHandler<SADCHit>::process_event( evPtr );
    
    for ( auto & it : _sadcHits ) {
		
		CaloEDep calo;
		
		calo.station = it.first;
		calo.stationHits = it.second;
		
		*_caloInserter( *evPtr, it.first, *_names) = calo;
	}
    
    return kOk;
}

}

REGISTER_HANDLER( CaloEDepCollect, banks, ch, yamlNode
                , "Handler to collect mSADC hits in calorimeters" ) {
    
    return new handlers::CaloEDepCollect( ch
								        , aux::retrieve_det_selection(yamlNode)
								        , banks.of<CaloEDep>() );
};
}
