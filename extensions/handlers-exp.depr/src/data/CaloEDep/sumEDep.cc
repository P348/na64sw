#include "data/CaloEDep/sumEDep.hh"

namespace na64dp {
namespace handlers {

CaloEDepSum::CaloEDepSum( calib::Dispatcher & ch
                        , const std::string & only
		) : AbstractHitHandler<CaloEDep>(ch, only) {}

bool
CaloEDepSum::process_hit( EventID
			     		, DetID_t did
                        , CaloEDep & hit ) {
	
	for ( auto & it : hit.stationHits ) {
		
		if ( isnan((*it.second).eDep) ) {
			continue;
		}

		hit.eDepMeV += (*it.second).eDep;
	}
	
	return true;
}

}

REGISTER_HANDLER( CaloEDepSum, banks, ch, yamlNode
                , "Handler to sum energy deposition in calorimeters" ) {
    
    return new handlers::CaloEDepSum( ch
								    , aux::retrieve_det_selection(yamlNode) );
};
}
