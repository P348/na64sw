#include "data/SADC/GetMaxPeaks.hh"

namespace na64dp {
namespace handlers {

bool
GetMaxPeaks::process_hit( EventID
                        , DetID_t
                        , SADCHit & cHit ){
      
    // Current channel "weight"
    int k = 2;
    // Array for signal smoothing 
    double SmWave[32];
    // Temp array for signal smoothing 
    double SmWaveTmp[32];

    // Zeroing of edge channels to search for max at the edge using derivative 
    SmWave[0] = 0;
    SmWave[31] = 0;

    // First start smoothing
    if ( _smLoop > 0 ){
      for( int i = 1; i < 31; ++i ){
        SmWave[i] = cHit.wave[i - 1] + k * cHit.wave[i] + cHit.wave[i + 1];
        SmWave[i] /= k + 2;
    }
    } else {
      for( int i = 1; i < 31; ++i ){ SmWave[i] = cHit.wave[i]; }
    }

    // Restarting smothing
    if ( _smLoop > 1 ){
      for(int j = 1; j < _smLoop; j++){
        for( int i = 1; i < 31; ++i ){
          SmWaveTmp[i] = SmWave[i - 1] + k * SmWave[i] + SmWave[i + 1];
          SmWaveTmp[i] /= k + 2;
        }
        for( int i = 1; i < 31; ++i ){ SmWave[i] = SmWaveTmp[i]; }
      }
    }
    
    // Array for the first derivative
    double derivWave[32];
    // Minimum peak value from which the event is considered
    double noice = 5;
    // Pileup counter
    int count = 0;

    // Creating a file for recording a waveform in the case of a pileup
    std::ofstream fout("PileUp.dat", std::ios_base::app);
    // Creating a file for recording a max waveform in the case of a pileup
    std::ofstream foutMax("MaxWavePileUp.dat", std::ios_base::app);

    // Numerical calculation of the first derivative
    for( int i = 1; i < 30; ++i ) {
      derivWave[i] = ( SmWave[i + 1] - SmWave[i - 1] ) / 2;
    }

    // temporaty varible for condition of pileUp
    bool condPileUp = false;
    // loop for peak search
    for( int i = 0; i < 30; ++i ) {
      // Сondition that the second peak is more than 10% of the maximum value
      condPileUp =    cHit.wave[i] > cHit.maxValue / 10
      // Сondition that the this is max, using first derivative              
                   && derivWave[i] > 0 && derivWave[i + 1] < 0
      // Сondition that the this is max, using second derivative 
      //            && ( deriv2Wave[i + 1] + deriv2Wave[i] + deriv2Wave[i - 1] ) / 3  < 0
      // Сondition that signal is more than noice
                   && cHit.wave[i] > noice;
      // write max value waveform if it is pileUp 
      if ( condPileUp ){ 
        if (count > 1 && i  == cHit.maxSample ){ 
          foutMax << i << " " << cHit.wave[i] << "\n ";
        }
        count++;
      }
    }

    // Write number of pileup
    cHit.countPipeUp = count;

    // Write waveform with pileup in file
    if (count > 1) {
      fout << "{";
      for( int i = 0; i < 31; ++i){
        fout << "{" << i << ", " << cHit.wave[i] << "}, " << " ";
      }
      fout << "}";
      fout << "\n ";
    }
    
    bool cutHit = false;
    if ( _cutPileUp ){
      // If there is only one peak and it is in the first channels. 
      // A tail from a past event
      cutHit =    ( ( count == 1 ) && cHit.maxSample < _SampLeft )
      // If there is only one peak and it is in the last channels. 
      // What is this physics?
               || ( ( count == 1 ) && cHit.maxSample > _SampRight )
      // Cat hit if it is pileup
               || ( cHit.countPipeUp != _countPeak );
      // Cut hit
      if( cutHit ){
        // If there is a detector specialization, then the whole event discrimination
        is_selective() ? this->_set_event_processing_result(kDiscriminateEvent)
        // otherwise individual hits are discriminated
                       : this->_set_hit_erase_flag();
      }
    }
    
    //Close file
    fout.close();
    foutMax.close();  
    return true;
}

}

REGISTER_HANDLER( GetMaxPeaks, banks, ch, yamlNode
                , "Get positions of max waveform " ){
    return new   handlers::GetMaxPeaks( ch
               , aux::retrieve_det_selection(yamlNode)
               , yamlNode["cutPileUp"] ? yamlNode["cutPileUp"].as<bool>() : true
               , yamlNode["countPeak"] ? yamlNode["countPeak"].as<int>() : 1
               , yamlNode["SampLeft"] ? yamlNode["SampLeft"].as<int>() : 0
               , yamlNode["SampRight"] ? yamlNode["SampRight"].as<int>() : 31
               , yamlNode["smLoop"] ? yamlNode["smLoop"].as<int>() : 0
               );

}
}

