#include "data/SADC/getMaxPeaks.hh"

namespace na64dp {
namespace handlers {

SADCGetMaxPeaks::SADCGetMaxPeaks( calib::Dispatcher & cdsp
								, const std::string & only
								, double threshold )
		: AbstractHitHandler<event::SADCHit>(cdsp, only)
		, _threshold(threshold) {}

bool
SADCGetMaxPeaks::process_hit( EventID
                            , DetID
                            , event::SADCHit & cHit ){
    NA64DP_ASSERT_EVENT_DATA( hit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
	// Find all local maxima (above certain threshold)
	// iterate over all samples (except first and last)
	for( unsigned int i = 1; i < 31; ++i ) {
		// check if the "sign" is changes for point
		if ( _check_peak( cHit.rawData->wave, i ) ) {
			cHit.maxima.emplace( i, cHit.wave[i] );
		}
	}
	return true;
}

bool
SADCGetMaxPeaks::_check_peak( double wave[32]
							, int position ) {
	// Check if the current peak is local maximum
	if ( ( wave[position - 1] < wave[position] )
	  && ( wave[position + 1] < wave[position] ) ) {
		// If peak amplitude is larger than specified threshold
		if ( wave[position] > _threshold ) {
			return true;
		}
	}
	return false;
}

}

REGISTER_HANDLER( SADCGetMaxPeaks, cdsp, yamlNode
                , "Get local maxima of mSADC waveform " ) {
    return new handlers::SADCGetMaxPeaks( cdsp
									    , aux::retrieve_det_selection(yamlNode)
								        , yamlNode["threshold"].as<int>()
									    );

}
}

