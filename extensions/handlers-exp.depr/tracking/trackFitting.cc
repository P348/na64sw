#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include <Track.h>
#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <EventDisplay.h>

// Kalman reference fitter routine and associated info
#include <KalmanFitterRefTrack.h>
#include <KalmanFitter.h>
#include <DAF.h>
#include <KalmanFitStatus.h>

#include <KalmanFitterInfo.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>
#include <TrackPoint.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */
class TrackFitting : public AbstractHandler
                   , public calib::Handle<nameutils::DetectorNaming> {

private:
	int _minIter, _maxIter;
	double _magPos;

public:
    TrackFitting( calib::Dispatcher & cdsp
				, const std::string & only
                , int minIter
                , int maxIter
                , double magPos )
        : AbstractHandler()
        , calib::Handle<nameutils::DetectorNaming>("default", cdsp)
        , _minIter(minIter)
        , _maxIter(maxIter)
        , _magPos(magPos) {}
                  
    virtual ProcRes process_event(event::Event & ) override;

    const nameutils::DetectorNaming & naming() const {
        return static_cast<const calib::Handle<nameutils::DetectorNaming>*>(this)->operator*();
    }

};

TrackFitting::ProcRes
TrackFitting::process_event(event::Event & event) {
    
    // Temp code just to fit all possible tp    
    std::multimap<StationKey, mem::Ref<event::TrackPoint>> trackCandDown;
    std::multimap<StationKey, mem::Ref<event::TrackPoint>> trackCandUp;
    
    std::vector<std::multimap<StationKey, mem::Ref<event::TrackPoint>>> trackSegments;

	trackCandDown.clear();
    trackCandUp.clear();

	for (auto & it : event.trackPoints) {
		// downstream part
		if ( it.second->gR[2] < _magPos ) {
			trackCandDown.emplace(it.first, it.second);
		} else {
			trackCandUp.emplace(it.first, it.second);
		}
	}
		
	if ( trackCandDown.empty() || trackCandUp.empty() ) {
	//	return kOk;
	}
	
	trackSegments.push_back(trackCandDown);
	trackSegments.push_back(trackCandUp);
	
	    
    if ( trackSegments.empty() ) {
		return kAbortProcessing;
	}
	
	if ( event.trackPoints.empty() ) {
		return kAbortProcessing;
	}
    
	genfit::AbsKalmanFitter * fitter
	//		= new genfit::KalmanFitter();
			= new genfit::DAF();
	//		= new genfit::KalmanFitterRefTrack();
	
	const genfit::eMultipleMeasurementHandling mmHandling = genfit::unweightedClosestToPredictionWire;
	
	fitter->setMultipleMeasurementHandling(mmHandling);
	
	fitter->setMinIterations( _minIter ); 
	fitter->setMaxIterations( _maxIter );
    
    std::vector<genfit::Track*> trackContainer;
    
    TVector3 seed(0,0,0);
    TVector3 mom(0,0,160);
    
    for ( auto & trackCand : trackSegments ) {
				
		/* 
		 * Check track size. If there is no points in upstream
		 * or downstream part -> terminate track processing and
		 * return empty track. All of the track handlers should
		 * check if track has fit status hasFitStatus()
		 *
		 */
		
		//genfit::AbsTrackRep * eRep = new genfit::RKTrackRep(11);
		genfit::AbsTrackRep * mRep = new genfit::RKTrackRep(13);
		
		//genfit::AbsTrackRep * pRep = new genfit::RKTrackRep(-211);
		
		genfit::Track * gTrack = new genfit::Track( mRep, seed, mom );
		//gTrack->addTrackRep(mRep);
		//gTrack->addTrackRep(pRep);
				
		// Set high initial covariance to decrease an influence of initial momentum hypothesis
		double initCov(1000);
		
		TMatrixDSym covM(6);
		for (int i = 0; i < 6; ++i)
			covM(i,i) = initCov;
		
		gTrack->setCovSeed(covM);
		
		// Sort track points in order ==================================
		std::multimap<double, mem::Ref<event::TrackPoint>> genfitPoints;
		
		for ( auto tp : trackCand ) {
			genfitPoints.emplace((*tp.second).gR[2], tp.second);
		}
		
		for ( auto & tp : genfitPoints ) {
			genfit::TrackPoint * gTP 
				= new genfit::TrackPoint(*(*tp.second).genfitTrackPoint);
			gTrack->insertPoint( gTP );
		}
		
		genfitPoints.clear();
		// =============================================================
		
		// Resort points and process fitting
		try {
			fitter->processTrack( gTrack );
							
			gTrack->checkConsistency();
		}
		catch(genfit::Exception& e) {
			std::cerr << e.what();
			continue;
		}
								
		// Fit status control
		if ( !gTrack->hasFitStatus() ) {
			std::cout << "Track has no fit status, event is not reconstructable" << std::endl;
			delete gTrack;
			gTrack = nullptr;
			break;
		}
		
		trackContainer.push_back(new genfit::Track(*gTrack));
		
		delete gTrack;
		gTrack = nullptr;
	}
	
	// start to merge tracks
	genfit::Track * firstTrack = trackContainer[0];
	genfit::Track * secondTrack = trackContainer[1];

	firstTrack->mergeTrack(secondTrack);
	
	firstTrack->checkConsistency();
	
	firstTrack->udpateSeed();
	
	firstTrack->checkConsistency();
	
	try {
		fitter->processTrack( firstTrack );
							
		firstTrack->checkConsistency();
	}
	catch(genfit::Exception& e) {
		std::cerr << e.what();
	}		
			
	try {
		
		firstTrack->determineCardinalRep();
		
		const genfit::MeasuredStateOnPlane stLast = firstTrack->getFittedState(-1);
		
		genfit::FitStatus * newfit = firstTrack->getFitStatus();
		
		#if 0		
		std::cout << "Fitted PDG: " << stLast.getPDG() << ", "
		          << "with momentum: " << stLast.getMomMag() << std::endl;
		stLast.Print();
		genfit::EventDisplay::getInstance()->addEvent(firstTrack);		          
		#endif
		
		
		// TEMP: Track for down part
		mem::Ref<event::Track> trackRef = lmem().create<event::Track>(lmem());
		trackRef->seed[0] = seed.X();
		trackRef->seed[1] = seed.Y();
		trackRef->seed[2] = seed.Z();
		trackRef->mom[0] = mom.X();
		trackRef->mom[1] = mom.Y();
		trackRef->mom[2] = mom.Z();
		trackRef->pdg = 11;
		trackRef->trackLen = firstTrack->getTrackLen();
		trackRef->momentum = stLast.getMomMag();
		trackRef->chi2ndf = newfit->getChi2() / newfit->getNdf();
		trackRef->insert(event.trackPoints.begin(), event.trackPoints.end());
		trackRef->genfitTrackRepr = new genfit::Track(*firstTrack);
		event.tracks.emplace(event.tracks.size()
							, trackRef);

	} catch (genfit::Exception& e) {
		std::cerr << e.what();
	}
	
	delete firstTrack;
	firstTrack = nullptr;
    
    delete fitter;
	fitter = nullptr;
		
    return kOk;
}

REGISTER_HANDLER( TrackFitting, ch, cfg
                , "Handler to perform fitting of track candidates" ) {
    
    return new handlers::TrackFitting( ch
                           , aux::retrieve_det_selection(cfg)
                           , cfg["iterations"][0].as<int>()
                           , cfg["iterations"][1].as<int>()
                           , cfg["magnetPos"].as<double>()
                           );
}
}
}
#endif  // defined(GenFit_FOUND)
