#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include <EventDisplay.h>
#include <Track.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */

class TrackDestruct : public AbstractHandler {
    
public:

		TrackDestruct( calib::Dispatcher & cdsp
                     , const std::string & only
                     )
					 : AbstractHandler() {}
        
    virtual ProcRes process_event(event::Event & event) override;

};

TrackDestruct::ProcRes
TrackDestruct::process_event(event::Event & event) {

    if ( event.tracks.empty() ) {
		return kDiscriminateEvent;
	}
	
	for ( auto & trackCand : event.tracks ) {
    
		event::Track & track = (*trackCand.second);

		delete track.genfitTrackRepr;
		track.genfitTrackRepr = nullptr;
	}
    
    return kOk;
}

REGISTER_HANDLER( TrackDestruct, ch, cfg
                , "Handler to destruct genfit::Track" ) {
    
    return new handlers::TrackDestruct( ch
                        , aux::retrieve_det_selection(cfg)
                        );
}
}
}
#endif  // defined(GenFit_FOUND)
