#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH2D.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */
class TrackPlotMom : public AbstractHandler {

private:

    TH1D *hMom;
    TH2D *hMomVsCal;
    TH2D *hMomVsChi;
    TH1D *hMomVsECal;
    TH1D *hChi;
    
public:
    TrackPlotMom( calib::Dispatcher & cdsp
                , const std::string & only            
                )
        : AbstractHandler() {

    hMom = new TH1D("hResMom" ,"momRes",50, 0, 200);
    
    hMomVsCal = new TH2D("Momentum" ,"momVsEcalHcalCal", 200, 0, 200, 200, 0, 200);
    
    hMomVsECal = new TH1D("MomentumEcal" ,"momVsECal", 50, 0, 200);
    
    hChi = new TH1D("Chi/Ndf" ,"chiSq2", 100, 0, 20);
    
    hMomVsChi = new TH2D("MomVsChi" ,"momChiSq2", 50, 0, 200, 100, 0, 100);
	}
                  
    virtual ProcRes process_event(event::Event & event ) override;
                            
    virtual void finalize() override;
};

TrackPlotMom::ProcRes
TrackPlotMom::process_event( event::Event & event ) {
    
    if ( event.tracks.empty() ) {
		return kDiscriminateEvent;
	}
    
    // TODO: Just for the testing purpose
	double edep(0);
    
    for ( auto & it : event.caloHits ) {
		edep += (*it.second).eDep;
	}
        
    for ( auto & track : event.tracks ) {
				
		hMomVsCal->Fill ( (*track.second).momentum , edep );
					
		hChi->Fill ( (*track.second).chi2ndf);
		
		hMom->Fill ((*track.second).momentum);
		
		hMomVsECal->Fill ( edep );

		hMomVsChi->Fill ( (*track.second).momentum, (*track.second).chi2ndf );

    }
    
    return kOk;
}

void
TrackPlotMom::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
	c1->cd(1);
    hMom->Draw();
    TCanvas* c2 = new TCanvas(); 
    c2->cd(1);
    hMomVsCal->Draw();
    TCanvas* c3 = new TCanvas(); 
    c3->cd(1);
    hMomVsECal->Draw();
    TCanvas* c4 = new TCanvas(); 
    c4->cd(1);
    hChi->Draw();
    TCanvas* c5 = new TCanvas(); 
    c5->cd(1);
    hMomVsChi->Draw();
    
}

REGISTER_HANDLER( TrackPlotMom, ch, cfg
                , "Handler to plot reconstructed momentum" ) {
    
    return new TrackPlotMom( ch
                           , aux::retrieve_det_selection(cfg)
                           );
}
}
}
#endif  // defined(GenFit_FOUND)
