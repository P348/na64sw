#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include <EventDisplay.h>

// Geometry, material and magnetic field headers
#include "ConstFieldBox.h" // TODO: include ConstFieldBox in main-pipe
#include "ConstFieldBox.cc"
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TGeoMaterialInterface.h>
#include <TGeoManager.h>

namespace na64dp {
namespace handlers {

/**\brief Entry for track proccessing routine
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */

class TrackInit : public AbstractHandler {

private:
    /// Controls GenFit event display window
    bool _eventDisplay;
    /// 
    bool _matInteraction;
    double _magFieldUp  ///<
         , _magFieldDown  ///<
         ;
    double _magValueX  ///<
         , _magValueY  ///<
         , _magValueZ  ///<
         ;
    /// Ptr to TGeoManger instance used to maintain ROOT geometry
    TGeoManager * _geoMgr;
public:
	TrackInit( calib::Dispatcher & cdsp
			 , bool eventDisplay
			 , bool matInteraction
			 , const std::string & geoFilePath
			 , double magFieldUp
			 , double magFieldDown
			 , double magValueX
			 , double magValueY
			 , double magValueZ)
        : AbstractHandler()
        , _eventDisplay(eventDisplay)
        , _matInteraction(matInteraction)
        , _magFieldUp(magFieldUp)
        , _magFieldDown(magFieldDown)
        , _magValueX(magValueX)
        , _magValueY(magValueY)
        , _magValueZ(magValueZ)
        , _geoMgr(new TGeoManager( "Geometry", "NA64 geometry"))
        {

		TGeoManager::Import( geoFilePath.c_str() );
		log().debug( "Geometry read from file \"%s\" into instance %p."
				   , geoFilePath.c_str()
				   , gGeoManager );
		assert(gGeoManager);
		
		// Create simple magnetic field. TODO: use inhomogenous field

		genfit::ConstFieldBox * magBox = 
				new genfit::ConstFieldBox( magValueX, magValueY, magValueZ 
										 , -1120, 1120
										 , -100, 100
										 , magFieldUp, magFieldDown
										 );
		
		genfit::FieldManager::getInstance()->init(magBox);		
		
		// Set if there is interaction with materials defined in root geo
		genfit::TGeoMaterialInterface * matInter = 
				new genfit::TGeoMaterialInterface();
		
		genfit::MaterialEffects::getInstance()->init(matInter);
		
		if ( !_matInteraction ) {
			genfit::MaterialEffects::getInstance()->setNoEffects();
		}
		
		if ( _eventDisplay ) {
			genfit::EventDisplay::getInstance();
		}
	}
	
	virtual ProcRes process_event(event::Event & event) override;
	
	virtual void finalize() override;
};

TrackInit::ProcRes
TrackInit::process_event(event::Event & event) {
	return kOk;
}

void
TrackInit::finalize() {	
	if ( _eventDisplay ) {
		genfit::EventDisplay::getInstance()->open();
	}
}

REGISTER_HANDLER( TrackInit, ch, cfg
                , "Handler to set magnetic field, material interaction and"
                  " initiate tracking routine" ) {
    
    return new handlers::TrackInit( ch
                        , cfg["display"] ? cfg["display"].as<bool>() : false
                        , cfg["matInter"] ? cfg["matInter"].as<bool>() : false
                        , cfg["geometry"].as<std::string>()
                        , cfg["magnetPos"][0].as<double>()
						, cfg["magnetPos"][1].as<double>()
						, cfg["fieldValue"][0].as<double>()
						, cfg["fieldValue"][1].as<double>()
						, cfg["fieldValue"][2].as<double>()
                        );
}
}
}
#endif  // defined(GenFit_FOUND)
