#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>

#include <TRandom.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers f1-handlers
 * */
class TrackFinder : public AbstractHitHandler<event::F1Hit> {

public:                
	TrackFinder( calib::Dispatcher & ch
			   , const std::string & only )
			: AbstractHitHandler<event::F1Hit>(ch, only) {}
                                 
    virtual bool process_hit( EventID
                            , DetID
                            , event::F1Hit & ) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;

};
    
bool
TrackFinder::process_hit( EventID
                        , DetID planekey
                        , event::F1Hit & hit ) { return true; }

TrackFinder::ProcRes
TrackFinder::process_event(event::Event & event) {
	
	std::unordered_multimap<StationKey, mem::Ref<event::TrackPoint>> hits;
	
	for ( auto & hit : event.trackPoints ) {
		hits.emplace(hit.first, hit.second);
	}
	
	typedef std::unordered_multimap<StationKey, mem::Ref<event::TrackPoint>>::iterator Iter;
	
	
	for ( Iter it = hits.begin(); it != hits.end(); ++it ) {
		std::cout << naming()[it->first] << std::endl;
	}

	hits.clear();
	
	return kOk;
}

}

REGISTER_HANDLER( TrackFinder, ch, cfg
                , "Handler to generate toy Monte Carlo hits for tracking test" ) {
    
    return new handlers::TrackFinder( ch
									, aux::retrieve_det_selection(cfg)
									);
}
}

#endif  // defined(GenFit_FOUND)
