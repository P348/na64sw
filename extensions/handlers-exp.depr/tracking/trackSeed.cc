#include "na64detID/wireID.hh"

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

using event::TrackPoint;

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */
class TrackSeed : public AbstractHitHandler<event::TrackPoint> {
public:

    struct Cell {
        mem::Ref<event::TrackPoint> trackPoint;
        StationKey planeName;
        std::string name;
        double x, y, z;
        int state;
        int layer;
        double charge;
        bool update;
        bool accounted;
    };
    
    typedef std::multimap<StationKey, Cell> CellMap;
    
    typedef std::multimap<int, Cell>::iterator sIt;
    typedef std::multimap<int, Cell>::iterator lIt;
    
private:
    
    /// Map of cells sorted by station
    std::map<double, CellMap> _cells;

protected:

    double _magPosition;
    
    int _missingHit;
    
    void _cellular_automaton( std::vector<std::vector<Cell>> & cell  );
    
    void _create_space_points( CellMap & map 
                             , std::vector<std::vector<Cell>> & spDown
                             , std::vector<std::vector<Cell>> & spUp);
                             
    void _calculate_cell_state( std::vector<Cell> & curCell
                              , std::vector<Cell> & prevCell );

    bool _update_cell_state( std::vector<std::vector<Cell>> & cell );
    

    bool _track_seeding( std::multimap<int, Cell> & states
                       , std::multimap<int, Cell> & layers
                       , std::vector<std::vector<Cell>> & trackCands );
    
    bool _backward_check( Cell & curCell
                        , Cell & nextCell
                        , std::vector<Cell> & trackCand);

    void _join_vectors( std::vector<std::map<StationKey, mem::Ref<event::TrackPoint>>> & joint
                      , std::vector<Cell> down
                      , std::vector<Cell> up);


public:
    TrackSeed( calib::Dispatcher & ch
             , const std::string & only
             , double magPosition
             , int missingHit = 0.)
        : AbstractHitHandler<TrackPoint>(ch, only)
        , _magPosition(magPosition)
        , _missingHit(missingHit) {}
                          
    virtual AbstractHandler::ProcRes process_event(event::Event & ) override;
                            
    virtual bool process_hit( EventID
                            , StationKey
                            , TrackPoint & ) override;
             
};

bool
TrackSeed::process_hit( EventID
                      , StationKey stationID
                      , event::TrackPoint & ) {

    // allows to ignore missing {proj}
    //DetID station(did);
    #if 1
    std::map<std::string, std::string> substDict;
    AbstractHitHandler<event::TrackPoint>::naming().append_subst_dict_for( DetID(stationID)
                                                                         , substDict );
    std::string station = util::str_subst( "{kin}{statNum}", substDict, true ); 
    #endif
    
    //std::string station = naming()[stationID];

    // Create cells for a cellular automaton in up and downstreams
    // For test only for x planes. Sorting is based on z position
    
    //std::cout << station << std::endl;
    
    Cell cell;
    cell.trackPoint = _current_event().trackPoints.find(stationID)->second;
    auto & tp = *cell.trackPoint;
    cell.planeName = stationID;
    cell.name = station;
    cell.x = tp.lR[0];
    cell.y = tp.lR[1];
    cell.z = tp.gR[2];
    cell.state = 1;
    cell.update = false;
    cell.accounted = false;
    cell.charge = tp.charge;
        
    auto idxIt = _cells.find( cell.z );
    if( _cells.end() == idxIt ) {
        auto ir = _cells.emplace( cell.z, CellMap() );
        assert( ir.second );
        idxIt = ir.first;
    }
            
    CellMap & cellByDet = idxIt->second;
    
    cellByDet.emplace( stationID, cell );
           
    return true;
}

TrackSeed::ProcRes
TrackSeed::process_event(event::Event & event) {
    
    AbstractHitHandler<TrackPoint>::process_event( event );
        
    std::vector<std::vector<Cell>> downCells;
    std::vector<std::vector<Cell>> upCells;
    
    // We are using multimap to sort our trackpoints in correct order
    // there is possible much more elegante solution
    
    for ( auto it : _cells ) {
        _create_space_points( it.second, downCells, upCells);
    }
            
    // main routine
    _cellular_automaton(downCells);
    //_cellular_automaton(upCells);
    
    std::vector<std::map<StationKey, mem::Ref<event::TrackPoint>>> trackCands;
    
    for ( auto it : downCells ) {
        for ( auto ir : upCells ) {
            _join_vectors(trackCands, it, ir);
        }
    }
    
    TVector3 seed(0,0,0);
    TVector3 mom(0,0,100);
    
    //std::cout<< "joint size: " << joint.size() << std::endl;
    for ( auto & trackCand : trackCands ) {
    
        mem::Ref<event::Track> trackRef = lmem().create<event::Track>(lmem());
        trackRef->seed[0] = seed.X();
        trackRef->seed[1] = seed.Y();
        trackRef->seed[2] = seed.Z();
        trackRef->mom[0] = mom.X();
        trackRef->mom[1] = mom.Y();
        trackRef->mom[2] = mom.Z();
        trackRef->pdg = 11;
        trackRef->momentum = 100;
        trackRef->edep = 0;
        //trackRef->trackPoints = trackCand;
        trackRef->insert(trackCand.begin(), trackCand.end());
        trackRef->genfitTrackRepr = nullptr;
        event.tracks.emplace(event.tracks.size()
                            , trackRef);
    }
    
    trackCands.clear();

    _cells.clear();
    
    downCells.clear();
    upCells.clear();
        
    return kOk;

}


void
TrackSeed::_join_vectors( std::vector<std::map<StationKey, mem::Ref<event::TrackPoint>>> & joint
                        , std::vector<Cell> down
                        , std::vector<Cell> up ) {
    
    std::map<StationKey, mem::Ref<event::TrackPoint>> temp;
    
    for (auto it : down ) {
        temp.emplace(it.planeName, it.trackPoint);
    }
    
    for (auto it : up ) {
        temp.emplace(it.planeName, it.trackPoint);
    }

    joint.push_back(temp);
    
    temp.clear();
}


void
TrackSeed::_create_space_points( CellMap & map 
                               , std::vector<std::vector<Cell>> & spDown
                               , std::vector<std::vector<Cell>> & spUp  ) {
    
    std::vector<Cell> pointsInDownLayer;
    std::vector<Cell> pointsInUpLayer;
    
    // Collect every point in detector plane
    for ( auto it : map ) {
        //std::cout << naming()[it.first] << std::endl;
        if ( it.second.z > _magPosition ) {
            it.second.layer = spUp.size();
            pointsInUpLayer.push_back( it.second );
        } else {
            it.second.layer = spDown.size();
            pointsInDownLayer.push_back( it.second );
        }       
    }
    
    if ( !pointsInUpLayer.empty() ) {
        spUp.push_back( pointsInUpLayer );
    }
    
    if ( !pointsInDownLayer.empty() ) {
        spDown.push_back( pointsInDownLayer );
    }
    
    pointsInDownLayer.clear();
    pointsInUpLayer.clear();
}

void
TrackSeed::_calculate_cell_state( std::vector<Cell> & curCell
                                , std::vector<Cell> & prevCell ) {
    
    // Cut for maximal distance between layers.
    // Seems to be correct but might be optimazed
    double maxDistance(1000);
    double maxAngle(0.5); // approximately 2.5 Degrees
    
    double distance, brAngleX, brAngleY, x, y, z;
            
    // Function to update cell states in neighbour layers.
    // As we have possibility to have missing hits in detector
    // layers, we might want tolerate up to several missing hits
    
    for ( auto & curPoint : curCell ) {
        for ( auto & prevPoint : prevCell ) {
            
            distance = abs( curPoint.z - prevPoint.z );
            
            x = ( curPoint.x - prevPoint.x );
            y = ( curPoint.y - prevPoint.y );
            z = ( curPoint.z - prevPoint.z );
                
            brAngleX = atan2(x, z);
            brAngleY = atan2(y, z);         
                    
            if ( distance < maxDistance && abs(brAngleX) < maxAngle && abs(brAngleY) < maxAngle) {
                // If at lease one cell updated it state we continue
                if ( curPoint.state == prevPoint.state ) 
                    curPoint.update = true; 
            }
        }
    }
}


bool
TrackSeed::_update_cell_state( std::vector<std::vector<Cell>> & cell ) {

    bool updateInStep(false);
    // Itterate over all of detector planes (aka superlayers)
    // and update status of cells. Otherwise: return false if
    // system in stable state.
    

    for ( unsigned int layer = 0; layer < cell.size(); ++layer ) {

        for ( auto & hit : cell[layer] ) {
            // assign all hits to certain layer
                        
            if ( hit.update ) {
                updateInStep = true;
                hit.state++;
                hit.update = false;
            }
        }
    }
    return updateInStep;
}

void
TrackSeed::_cellular_automaton( std::vector<std::vector<Cell>> & cell ) {

    // 100 is a randomly choosed value to ensure that the system will
    // stabilzed in computationally acceptable time
    int numOfSteps(100);
    
    unsigned int numSuperLayers(cell.size());
    
    bool updateInStep(false);

    // Cycle for time evolution of our dynamic system
    
    for ( int k = 0; k < numOfSteps; ++k ) {
        
        for ( unsigned int sLayer = 1; sLayer < numSuperLayers; ++sLayer ) {
                                
            /* for each space-point the automaton finds its neighbour
             * in the previous layer based on certain criteria.
             * For now we will use angle and distance to consider "real" hit/
             * https://www.star.bnl.gov/~gorbunov/main/node24.html
             */
        
            _calculate_cell_state( cell[sLayer], cell[sLayer-1] );
            
            // option to tolerate one missing hit
            if ( sLayer != 1 ) {
                _calculate_cell_state( cell[sLayer], cell[sLayer-2] );
            }
    
        }
        // After end of the loop - update status of all cells
        updateInStep = false;
        
        updateInStep = _update_cell_state( cell );
    
        if ( !updateInStep ) {
            //std::cout<< "Cellular automaton reached final state in " << k+1
            //       << " steps" << std::endl;
            break;
        }
    }
    
    
    #if 1

    for ( auto & superLayer : cell ) {
        for ( auto & hit : superLayer ) {
                std::cout << "Detector: " << hit.name << ","
                          << "hit state: " << hit.state << "," 
                          << "hit layer: " << hit.layer << ","
                          << "x: " << hit.x << ","
                          << "y: " << hit.y << std::endl;
        }
    }
    #endif
    
    // Create to multimaps
    
    std::multimap<int, Cell> states;
    std::multimap<int, Cell> layers;
    
    for (auto it : cell ) {
        for (auto ir : it ) {
            states.emplace(ir.state, ir);
            layers.emplace(ir.layer, ir);
        }
    }
        
    std::vector<std::vector<Cell>> trackCands;
    
    if (!states.empty()) {
        _track_seeding(states, layers, trackCands);
    }
    
    // Assign track cand
    cell = trackCands;

}

bool
TrackSeed::_track_seeding( std::multimap<int, Cell> & states
                         , std::multimap<int, Cell> & layers
                         , std::vector<std::vector<Cell>> & trackCands ) {
        
    std::pair <sIt, sIt> sRet;
    
    std::multimap<int, Cell>::reverse_iterator rev;
    
    rev = states.rbegin();
    
    sRet = states.equal_range(rev->first);
    
    for (sIt it = sRet.first; it != sRet.second; ++it) {
        
        Cell & curCell = it->second;
        
        int initState = curCell.state;
        
        std::vector<Cell> trackCand;
                
        std::pair <lIt, lIt> ret;
        
        trackCand.push_back(curCell);
        
        for ( int step = initState - 1; step > 0 ; --step) {
            
            bool found(false);
                        
            ret = layers.equal_range(step);
            
            for (lIt it = ret.first; it != ret.second; ++it) {
                                
                Cell & nextCell = it->second;
                
                found = _backward_check(curCell, nextCell, trackCand);
                
                if (found) {
                    curCell = it->second;
                    break;
                } else if ( !found ) {
                    continue;
                }
            }       
        }
        
        trackCands.push_back(trackCand);
        
        
        trackCand.clear();
        
    }
    
    return true;
        
        
}

bool
TrackSeed::_backward_check( Cell & curCell
                          , Cell & nextCell
                          , std::vector<Cell> & trackCand ) {

    double maxAngleX(1); // approximately 2.5 Degrees
    double maxAngleY(1); // approximately 2.5 Degrees
    
    double brAngleX, brAngleY, x, y, z;
    
    int diff = curCell.state - nextCell.state;
    
    if ( diff == 1 ) {
        x = ( curCell.x - nextCell.x );
        y = ( curCell.y - nextCell.y );
        z = ( curCell.z - nextCell.z );
                
        brAngleX = atan2(x, z);
        brAngleY = atan2(y, z);
        
        if ( abs(brAngleX) < maxAngleX && abs(brAngleY) < maxAngleY ) {
            trackCand.push_back(nextCell);
            return true;
        }
    }

    return false; 

}

}

REGISTER_HANDLER( TrackSeed, ch, cfg
                , "Handler for hit processing and simple itterative seeding" ) {
    
    return new handlers::TrackSeed( ch
                             , aux::retrieve_det_selection(cfg)
                             , cfg["magPosition"].as<double>()
                             , cfg["missingHits"].as<int>(0) );
}
}

#endif  // defined(GenFit_FOUND)
