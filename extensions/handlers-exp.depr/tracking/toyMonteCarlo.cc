#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

#include <AbsTrackRep.h>
#include <RKTrackRep.h>

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>

#include <TRandom.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers f1-handlers
 * */
class ToyMonteCarlo : public AbstractHitHandler<event::F1Hit> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
        
private:

	double _energy, _dev, _resolution;

    /// Placements dictionary; TODO: subst with calib handle
    std::vector<DetPlacementEntry *> _placements;    

public:                
	ToyMonteCarlo( calib::Dispatcher & ch
			     , const std::string & only
			     , double energy
			     , double dev
			     , double res )
			: AbstractHitHandler<event::F1Hit>(ch, only)
			, _energy(energy)
			, _dev (dev)
			, _resolution(res) {}
                                 
    virtual bool process_hit( EventID
                            , DetID
                            , event::F1Hit & ) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;

};

static ToyMonteCarlo::DetPlacementEntry gPlacements[] = {
    
    // Placement for 2021 mu run
    {"BM4", 0,   0,  -593.3800, 0, 0, 90, 4, 23, 0.5,  64}, 
    
    {"BM6", 0,   0, -1538.4467, 0, 0, 90, 4, 16, 0.125, 128},
        
    {"BM3", 0,   0, -1825.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM7", 0,   0, -2000.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM8", 0,   0, -2500.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM9", 0,   0, -3000.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM10", 0,  0, -3500.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM11", 0,  -1.56, -4000.8500, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM12", 0,  -7.977, -4500.8500, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM13", 0,  -19.3594, -5000.8500, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM14", 0,  -43.4246, -5800.8500, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM2", 0,  -74.65, -6832.8500, 0, 0, 90, 4, 9, 0.5,  64},

	{"BM5", 0,  -94.0235, -7472.8900, 0, 0, 90, 4, 16, 0.25,  64},
    
    {"BM1", 0, -115.242, -8173.8700, 0, 0, 90, 4, 18, 0.5,  64},

    #if 0        
    
	// Placement for 2017

    {"MM1X", -0.21484, -1.309757, 35.5000, 0,  0,  45+180, 8.1, 8.1, 0.025, 320},
        
    {"MM2X", 0.772714, -1.189551, 44.5,  0,  0, 45-90, 8.1, 8.1, 0.025, 320},
    
    {"MM3X", -0.202403, -0.831729, 180.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
        
    {"MM4X", 1.332376, -1.415417, 189.5, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
    
    {"GM1X", -29.510777, 0.309302, 1662, 0, 0, 0, 10.6, 10.6, 0.04, 256},
      
    {"MM5X", -32.036361, -1.820219, 1688.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},

    {"GM3X", -34.841116, -0.002498, 1725.2, 0, 0, 0, 10.6, 10.6, 0.04, 256},

    {"GM2X", -34.381352, -0.236811, 1860.5, 0, 0, 0, 10.6, 10.6, 0.04, 256},
        
    {"GM4X", -34.94229, 0.741795, 1837.1, 0, 0, 0, 10.6, 10.6, 0.04, 256},

    {"MM7X", -36.409401, -0.9791366, 1902, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    #endif
    
};

bool
ToyMonteCarlo::process_hit( EventID
                          , DetID planekey
                          , event::F1Hit & hit ) { return true; }

ToyMonteCarlo::ProcRes
ToyMonteCarlo::process_event(event::Event & event) {
	
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // impose placement entry into `_placements' map
        _placements.push_back(gPlacements + i);
    }
	
	double current_energy = gRandom->Gaus(_energy, _dev);
	
	unsigned int repNum = gRandom->Uniform(0, 3);
	//unsigned int repNum = 2;
	
	genfit::AbsTrackRep* trep;
	
	if ( repNum == 0 ) trep = new genfit::RKTrackRep(11);
	
	if ( repNum == 1 ) trep = new genfit::RKTrackRep(13);
	
	if ( repNum == 2 ) trep = new genfit::RKTrackRep(-211);
	
	genfit::StateOnPlane state(trep);
	
	TVector3 npos(0,0,0);
	TVector3 nmom(0,0,1);
	
	nmom.SetMag(160);
	
	trep->setPosMom(state, npos, nmom);
	
	genfit::SharedPlanePtr plane(new genfit::DetPlane(TVector3(0,0,1701.20), TVector3(0,0,1)));
	
	TVectorD curState = trep->get6DState(state);
	curState.Print();
	
	trep->extrapolateToPlane(state, plane);
	
	std::cout << "Particle: " << trep->getPDG() << ", "
	          << "with PDG charge: " << trep->getPDGCharge() << ", "
	          << "mass to momentum: " << state.getQop() << ", "
	          << "with mass: " << trep->getMass(state) << ", "
	          << "momentum magnitude: " << state.getMomMag() << std::endl;
	
	//trep->Print();
	state.Print();
		
	delete trep;
	
	#if 0
	double firstZ(0), distance(0);
	
	for (auto it : _placements) {
		
		auto tp = lmem().create<event::TrackPoint>(lmem());
		util::reset(*tp);
    
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		TVectorD hitCoord(2);
		TMatrixDSym hitCov(2);
			
		const DetPlacementEntry * cPlacement = it;
		
		const auto & nm = naming();
		DetID did(naming()[cPlacement->planeName]);
		
		double self_res = cPlacement->sizeX;
		hitCov(0, 0) = self_res*self_res;
		hitCov(1, 1) = self_res*self_res;
		
		int hitId(0), planeId(0);
		
		distance = cPlacement->z - firstZ;
		
		state.extrapolateBy(distance);
		state.getPosMom(npos, nmom);
		
		firstZ = cPlacement->z;
		
		//state.Print();
		
		// Rotate detector planes according data from detector placement
		TVector3 o_plane(npos), u_plane(10,0,0), v_plane(0,10,0);
		
		
		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
                                                         , v_plane));
        
		hitCoord(0) = 0;//gRandom->Gaus(0, _resolution + self_res);
		hitCoord(1) = 0;//gRandom->Gaus(0, _resolution + self_res);
			
		genfit::AbsMeasurement * measurement
							   = new genfit::PlanarMeasurement( hitCoord
															  , hitCov
                                                              , did
                                                              , hitId
                                                              , nullptr );
	
		static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																	  , planeId);
			
			
		genfitTP->addRawMeasurement( measurement );

		
		tp->gR[2] = cPlacement->z;
		
		tp->genfitTrackPoint = genfitTP;
    
		event.trackPoints.emplace(did, tp);
	}
	#endif
	
	
	return kOk;
}

}

REGISTER_HANDLER( ToyMonteCarlo, ch, cfg
                , "Handler to generate toy Monte Carlo hits for tracking test" ) {
    
    return new handlers::ToyMonteCarlo( ch
									  , aux::retrieve_det_selection(cfg)
									  , cfg["energy"].as<double>()
									  , cfg["edev"].as<double>()
									  , cfg["resolution"].as<double>() );
}
}

#endif  // defined(GenFit_FOUND)
