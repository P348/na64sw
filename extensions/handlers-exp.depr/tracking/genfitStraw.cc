#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include "na64util/str-fmt.hh"

#include "na64detID/wireID.hh"

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <WireMeasurement.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers stw-handlers
 * */
class GenfitTrackPointStraw : public AbstractHitHandler<event::StwTDCHit> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float numOfWires;
        double driftDistance;
		double driftDistanceError;
    };
        
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<StationKey, DetPlacementEntry *> _placements;
        
protected:

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    

public:
                       
	GenfitTrackPointStraw( calib::Dispatcher & ch
					     , const std::string & only )
			: AbstractHitHandler<event::StwTDCHit>(ch, only) {
	}
	                                 
    virtual bool process_hit( EventID
                            , DetID
                            , event::StwTDCHit & ) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;

};

static GenfitTrackPointStraw::DetPlacementEntry gPlacements[] = {
	
	{"ST01U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
	{"ST02U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
	{"ST03U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
	{"ST04U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST05U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST06U", -1.8925, -1.6325, 1684.13,  0,  0,  8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST01V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST02V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST03V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST04V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST05V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },
    {"ST06V", -1.3699, -1.6899, 1701.20,  0,  0, -8,  118.8108,   60,   64, 0.6156, 0.2 },

};

void
GenfitTrackPointStraw::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::StwTDCHit>::handle_update(nm);
    
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        StationKey plane(nm.id(gPlacements[i].planeName));
        // impose placement entry into `_placements' map
        _placements.emplace(plane, gPlacements + i);
    }
}

bool
GenfitTrackPointStraw::process_hit( EventID
                                  , DetID did
                                  , event::StwTDCHit & hit ) { 

	if (WireID::proj_label( WireID(did.payload()).proj() ) == 'U' ) {   

		//std::cout << "Hit in: " << naming()[did] << std::endl;
		
		hit.rawData->wireNo = (64 - hit.rawData->wireNo);
		
		
	}
	
	return true; 
}
								

GenfitTrackPointStraw::ProcRes
GenfitTrackPointStraw::process_event(event::Event & event) {
	
	AbstractHitHandler<event::StwTDCHit>::process_event(event);
	
	
	std::multimap<StationKey, mem::Ref<event::StwTDCHit>> _hits;
	
	std::set<StationKey> _uniquePlanes;
	
	// Create list of unique planes and fill multimap
	for (auto & it : event.stwtdcHits) {
		StationKey pl(it.first);
		_hits.emplace(pl, it.second);
		_uniquePlanes.insert(pl);
	}
	
	typedef std::multimap<StationKey, mem::Ref<event::StwTDCHit>>::iterator clusIt;
	
	std::pair <clusIt, clusIt> ret;
	
	// Search through all of the unique planes and treat them separately
	for ( auto & it : _uniquePlanes ) {
				
		ret = _hits.equal_range(it);
//	    std::cout << "new hit in layer: " << naming()[it] << std::endl;
		
		auto tp = lmem().create<event::TrackPoint>(lmem());
		util::reset(*tp);
    
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		// Get geometry for particular plane
		const DetPlacementEntry * cPlacement = _placements[it];
		
		// Create wire plane
		TVectorD hitCoords(7);
		
		TMatrixDSym hitCov(7);
		double resolution = cPlacement->driftDistance;
		hitCov(6,6) = resolution*resolution;
		
		int hitId(0);
		
		#if 0
		// Rotate detector planes according data from detector placement
		TVector3 o_plane(cPlacement->x, cPlacement->y, cPlacement->z), u_plane(1,0,0), v_plane(0,1,0);

		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
                                                         , v_plane));
		#endif
		
		/* Iterate over all hits in one plane and create TrackPoint
		 * containing genfit-type AbsMeasurements
		 */
		
		for ( clusIt ir = ret.first; ir != ret.second; ++ir ) {
			
			//std::cout << "Hit in: " << naming()[ir->first] << ", "
			//          << "Cluster position: " << ir->second->rawData->channel << std::endl;
						
			// TODO: Fix it!
			// As for muon run we have one real Straw station and several chips
			// refered to wires (and chips named as different stations)
			// we have to do it. However, much more elegant solution
			// should be foreseen
			const uint16_t & wireNo = ir->second->rawData->wireNo;
			const double step = cPlacement->driftDistance/2;
			
			// Wires in Straw station are in chess order.
			// For even station we shold include z smearing
			const double stepZ = 0.7044;
			double x(0), z(0);
						
			if (naming()[ir->first] == "ST1") {
				x = step/2 + (wireNo * step) - cPlacement->sizeX/2;
			}
			
			if (naming()[ir->first] == "ST2") {
				x = step/2 + ( (wireNo+64)  * step) - cPlacement->sizeX/2;
			}
			
			if (naming()[ir->first] == "ST3") {
				x = step/2 + ( (wireNo+128)  * step) - cPlacement->sizeX/2;
			}
			
			if (naming()[ir->first] == "ST4") {
				x = step/2 + ( (wireNo+194)  * step) - cPlacement->sizeX/2;
			}
			
			if (naming()[ir->first] == "ST5") {
				x = step/2 + ( (wireNo+256)  * step) - cPlacement->sizeX/2;
			}
			
			if (naming()[ir->first] == "ST6") {
				x = step/2 + ( (wireNo+320)  * step) - cPlacement->sizeX/2;
			}
						
			// Position of first wire point (top)
			hitCoords(0) = -x;
			hitCoords(1) = (cPlacement->y + cPlacement->sizeY/2);
			
			// Position of second wire point (bottom)
			hitCoords(3) = -x;
			hitCoords(4) = (cPlacement->y - cPlacement->sizeY/2);
			
			( wireNo % 2 == 0 ) ? z = cPlacement->z + stepZ : z = cPlacement->z;
			
			hitCoords(2) = z;
			hitCoords(5) = z;
			
			// Should rotate vector before measurement creation!
			
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			// Detector resolution
			hitCoords(6) = (cPlacement->driftDistance);
			
			// For a testing purpose - reconstructed straw positon
			ir->second->position = hitCoords(0);
			
			genfit::AbsMeasurement * measurement
							= new genfit::WireMeasurement( hitCoords
														 , hitCov
												         , it.id
												         , hitId
														 , nullptr );
	
			static_cast<genfit::WireMeasurement*>(measurement)->setLeftRightResolution(0);
			
			genfitTP->addRawMeasurement( measurement );
			
			++hitId;
			
		}
		
		tp->gR[2] = cPlacement->z;
		
		//std::cout << "end of layer: " << it.id << std::endl;
		
		tp->genfitTrackPoint = genfitTP;
		    
		event.trackPoints.emplace(it, tp);
		
	}
	
	_hits.clear();
	_uniquePlanes.clear();
	
	return kOk;
}

}

REGISTER_HANDLER( GenfitTrackPointStraw, ch, cfg
                , "Handler for Straw hit integration into Genfit processing" ) {
    
    return new handlers::GenfitTrackPointStraw( ch
                                   , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
