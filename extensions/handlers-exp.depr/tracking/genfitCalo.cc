#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based handler for proccessing SADC hits of calorimeters.
 *
 * \ingroup handlers tracking-handlers sadc-handlers
 */
class GenfitTrackPointCounter : public AbstractHitHandler<event::SADCHit> {
private:
	
	int _triggerType;
		    	    
public:
    
    GenfitTrackPointCounter( calib::Dispatcher & ch
                           , const std::string & only
                           , int triggerType )
        : AbstractHitHandler<event::SADCHit>(ch, only)
        , _triggerType(triggerType) {}
                            
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit &) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;	

};

bool
GenfitTrackPointCounter::process_hit( EventID
					             , DetID did
                                 , event::SADCHit & hit ) {	return true; }


GenfitTrackPointCounter::ProcRes
GenfitTrackPointCounter::process_event(event::Event & event) {
	
	#if 0
	// For trigger type 2 Muon run we need to install 4 beam counters

	TVectorD hitCoord(2);
	TMatrixDSym hitCov(2);
	
	int hitId(0), planeId(0);

	double resolution = 5;
	hitCov(0, 0) = resolution*resolution;
	hitCov(1, 1) = resolution*resolution;
	
	// FOR S4 Counter
	DetID did1(naming()["S4"]);
		
	auto tp1 = lmem().create<event::TrackPoint>(lmem());
	util::reset(*tp1);
    
	genfit::TrackPoint * genfitTP1 = new genfit::TrackPoint();
		
	TVector3 o_plane1(0,0,0), u_plane1(1,0,0), v_plane1(0,1,0);


		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
                                                         , v_plane));
                                                         
		/* Iterate over all hits in one plane and create TrackPoint
		 * containing genfit-type AbsMeasurements
		 */
		
		for ( clusIt ir = ret.first; ir != ret.second; ++ir ) {
			//std::cout << "Hit in: " << naming()[ir->first] << ", "
			//          << "Cluster position: " << (*ir->second).position << std::endl;
    
			hitCoord(0) = (cPlacement->sizeX * (*ir->second).position / cPlacement->numOfWires) - cPlacement->sizeX / 2;
			
			genfit::AbsMeasurement * measurement
								   = new genfit::PlanarMeasurement( hitCoord
																  , hitCov
                                                                  , did
                                                                  , hitId
                                                                  , nullptr );
	
		    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																		  , planeId);
			
			// Might be already accounted in detecor placement
			if (WireID::proj_label( WireID(did.payload()).proj() ) == 'Y' ) {                                                   
				//static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
			}
			
			genfitTP->addRawMeasurement( measurement );
			
			++hitId;
			
		}
		
		tp->gR[2] = cPlacement->z;
		
		//std::cout << "end of layer: " << naming()[it] << std::endl;
		
		tp->genfitTrackPoint = genfitTP;
    
		event.trackPoints.emplace(did, tp);
	#endif
	
	return kOk;
}

}

REGISTER_HANDLER( GenfitTrackPointCounter, ch, cfg
                , "Handler for calorimeter hits processing" 
                " and positioning") {
    
    return new handlers::GenfitTrackPointCounter( ch
								   , aux::retrieve_det_selection(cfg)
								   , cfg["triggerType"].as<int>() );
}
}
#endif  // defined(GenFit_FOUND)
