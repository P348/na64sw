#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "PlanarMeasurement.h"

#include "na64dp/abstractHitHandler.hh"
#include "na64calib/manager.hh"
//#include "na64calib/sdc.hh"
#include "na64calib/sdc-integration.hh"

namespace na64dp {

#if 0
/**\brief Utility adapter class linking APV clusters and genfit measurements
 *
 * This class implements GenFit's `AbsMeasurementProducer` interface to provide
 * conversion from APV clusters provided within an event into GenFit's 1D
 * measurements (`genfit::WireMeasurement` or `genfit::WireMeasurementNew`).
 * */
class APVMeasurementProducer
        : public genfit::AbsMeasurementProducer<genfit::WireMeasurementNew> {
protected:
    // ... position index -> detID cache?
public:
   /** \brief Create a Measurement from the cluster at position index
    *         in na64sw::event */
    genfit::WireMeasurementNew * produce( int index
                                        , const TrackCandHit* hit) override;
};
#endif

namespace calib {

struct Placement {
    std::string name;  ///< name of the detector spatial item (plane)
    float center[3]  ///< global coordinates of the item, cm
        , size[3]  ///< size of the item, cm
        , rot[3]  ///< Euler angles defining orientation of an item
        ;
    unsigned int nWires;  ///< number of wires
    float resolution;  ///< detector resolution, if specified

    void vectors( TVector3 & o
                , TVector3 & u
                , TVector3 & v ) const {
            o.SetXYZ( center[0], center[1], center[1] );  
            u.SetXYZ( size[0], 0, 0 );
            v.SetXYZ( 0, size[1], 0 );
            u.RotateX( M_PI * rot[0] / 180 );
            v.RotateX( M_PI * rot[0] / 180 );
            u.RotateY( M_PI * rot[1] / 180 );
            v.RotateY( M_PI * rot[1] / 180 );
            u.RotateZ( M_PI * rot[2] / 180 );
            v.RotateZ( M_PI * rot[2] / 180 );
                                           
    }
};

typedef std::list<Placement> Placements;

}  // namespace ::na64dp::calib
}  // namespace na64dp

namespace sdc {
template<>
struct CalibDataTraits<na64dp::calib::Placement> {
    /// Type name alias
    static constexpr auto typeName = "placements";
    /// A collection type of the parsed entries
    template<typename T=na64dp::calib::Placement>
        using Collection=std::list<T>;
    //typedef std::list<P348_RECO_NAMESPACE::CaloCellCalib> Collection;
    /// An action performed to put newly parsed data into collection
    template<typename T=na64dp::calib::Placement>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t
                              ) { col.push_back(e); }

    /// This way one could define the data type name right in the structure
    /// defining a parser for data line. However, p348reco forbids
    /// doing this because of "header-only" politics that doesn't allow
    /// to define static data members for reentrant usage.
    //static constexpr auto typeName = "CaloCell";

    /// Parses the CSV line into `CaloCellCalib` data object
    ///
    /// Parsing can be affected by the parser state to handle changes within
    /// a file. Namely, metadata "columns=name1,name2..." may denote number of
    /// token with certain semantics. By default, "name,x,y,peakpos" is assumed
    ///
    /// The `lineNo` argument can be used for diagnostic purposes (e.g.
    /// to report a format error).
    static na64dp::calib::Placement
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & filename
                      ) {
        // create object and set everything to zero
        na64dp::calib::Placement obj;
        // Create and validate columns order
        auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
                //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
                .interpret(aux::tokenize(line));
        // ^^^ note: every CSV block must be prefixed by columns order,
        // according to Anton's specification

        // Get columns
        obj.name = csv("name");

        obj.center[0] = csv("x", std::nan("0"));
        obj.center[1] = csv("y", std::nan("0"));
        obj.center[2] = csv("z", std::nan("0"));

        obj.size[0] = csv("sx", std::nan("0"));
        obj.size[1] = csv("sy", std::nan("0"));
        obj.size[2] = csv("sz", std::nan("0"));

        obj.rot[0] = csv("ax", std::nan("0"));
        obj.rot[1] = csv("ay", std::nan("0"));
        obj.rot[2] = csv("az", std::nan("0"));

        obj.nWires = csv("nWires", 0);
        obj.resolution = csv("resolut", std::nan("0"));
        return obj;
    }
};  // struct CalibDataTraits<na64dp::calib::Placement>
}  // namespace sdc

namespace na64dp {
namespace aux {

template<typename HitT> struct MsrmntTraits;

/// Specialization of hit conversion APVCluster -> genfit::PlanarMeasurement
template<>
struct MsrmntTraits<event::APVCluster> {
    /// Placement cache subject: a plane + standard resolution
    struct PlaneEntry {
        genfit::SharedPlanePtr planePtr;
        float resolution;
        float firstWire, stride;
    };

    /// A placement cache type
    typedef std::unordered_map<PlaneKey, PlaneEntry> Cache;

    /// Converts placement into cache entry and adds an entry to a cache
    static void recache_placement( Cache & cache
                                 , PlaneKey pk
                                 , const calib::Placement & placement
                                 ) {
        TVector3 o, u, v;
        placement.vectors(o, u, v);
        // try to find a plane to update it instead of (re)creation
        auto it = cache.find(pk);
        if( cache.end() == it ) {
            // create new
            it = cache.emplace( pk
                         , PlaneEntry{ genfit::SharedPlanePtr(
                                            new genfit::DetPlane(o, u, v))
                                     , placement.resolution
                                     }
                         ).first;
        } else {
            // update
            it->second.planePtr->set(o, u, v);
            it->second.resolution = placement.resolution;
        }
        // a distance from first wire to the center of detector
        it->second.firstWire = - placement.size[0]/2;
        assert(!std::isnan(it->second.firstWire));
        // a stride parameter defining inter-wire distance (pitch)
        it->second.stride = placement.size[0]/placement.nWires;
        assert(!std::isnan(it->second.stride));
        // if resolution is not provided, assume it to be stride/sqrt(12)
        if(std::isnan(it->second.resolution))
            it->second.resolution = it->second.stride/sqrt(12.);
        assert(!std::isnan(it->second.resolution));
    }

    /// Translates APV cluster into GenFit's 1D "planar measurement"
    static genfit::AbsMeasurement *
        new_genfit_measurement( Cache & cache
                              , PlaneKey pk
                              , event::APVCluster & c
                              ) {
        auto it = cache.find(pk);
        if(cache.end() == it) return nullptr;  // no placement for this detector
        const PlaneEntry & pe = it->second;
        // hit coordinate
        TVectorD lr(1);
        lr(0) = c.position * pe.stride + pe.firstWire;
        // covariance
		TMatrixDSym hitCov(1);
        hitCov(0, 0) = pe.resolution * pe.resolution;
        // instantiate 1D planar measurement
        return new genfit::PlanarMeasurement( lr  // (local) hit coordinate(s)
											, hitCov  // hit covariance
                                            , pk  // plane key ID as detector ID
                                            , -1 // set later
                                            , nullptr  // genfit trackpoint instance
                                            );
    }
};

}  // namespace ::na64dp::aux

namespace handlers {

/**\brief Populates GenFit input with measurements
 *
 * Relies on traits to convert hits of certain type into GenFit's "measurement"
 * instances.
 *
 * Allocates `TrackPoints` and inserts them into an event.
 *
 * Relies on `"placements"` calibration data type.
 *
 * \ingroup handlers tracking-handlers calib-handlers
 * */
template<typename HitT>
class CreateGenFitTrackPoints : public AbstractHitHandler<HitT>
                              , public calib::Handle<calib::Placements> {
public:
    typedef AbstractHitHandler<HitT> HitHandler;
    typedef calib::Handle<calib::Placements> PlacementsHandle;
protected:
    /// Caches placements information needed by traits
    typename aux::MsrmntTraits<HitT>::Cache _cache;
    /// Collection of created track points
    std::unordered_map< PlaneKey, mem::Ref<event::TrackPoint> > _tps;
    /// Collection of failed keys with missed placements, etc
    std::unordered_set<PlaneKey> _faultyIDs;
protected:
    // recaches placements
    void handle_update( const calib::Placements & placements ) override {
        calib::Handle<calib::Placements>::handle_update( placements );
        for( const auto & placement : placements ) {
            aux::MsrmntTraits<HitT>::recache_placement( _cache
                            , PlaneKey(HitHandler::naming()[placement.name])
                            , placement
                            );
        }
    }
public:
    CreateGenFitTrackPoints( calib::Dispatcher & dsp
                           , const std::string & selection
                           ) : HitHandler(dsp, selection)
                             , PlacementsHandle("default", dsp)
                             {}

    /// Adds hits matching selection criteria (applyTo) to a new track point
    ///
    /// Forwards execution to corresponding traits to instantiate the
    /// measurement using cached placements and adds new measurement to
    /// new GenFit's track point.
    virtual bool process_hit( EventID eventID
                            , typename AbstractHitHandler<HitT>::HitKey hk
                            , HitT & hit
                            ) override {
        genfit::AbsMeasurement * msrmt
            = aux::MsrmntTraits<HitT>::new_genfit_measurement( _cache, hk, hit );
        if( !msrmt ) {
            // failed to create a track point -- missing placements or other,
            // traits-specific, reason
            _faultyIDs.insert(hk);
            return true;  // continue iteration over hits
        }
        auto it = _tps.find(hk);
        if( _tps.end() == it ) {
            mem::Ref<event::TrackPoint> tp
                = this->lmem().template create<event::TrackPoint>(this->lmem());
            util::reset(*tp);
            tp->genfitTrackPoint = new genfit::TrackPoint();
            it = _tps.emplace(hk, tp).first;
        }
        it->second->apvClusters.emplace(hk, this->_current_hit_ref());
        it->second->genfitTrackPoint->addRawMeasurement(msrmt);
        // TODO: msrmt->setHitId(int ...) ?
        return true;
    }

    /// Iterates over selected detectors, adding track points to an event
    ///
    /// Forwards execution to parent's (selective) `process_event()`. Once
    /// done, looks for genfit track points within local cache and adds
    /// them to an event.
    AbstractHandler::ProcRes process_event( event::Event & eve ) {
        //_current_event(). ... -> genfitTrackRepr->insertPoint();
        return AbstractHandler::kOk;
    }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( CreateGenFitTrackPoints
                , mgr, cfg
                , "Populates GenFit track candidate with track points/measurements"
                ) {
    // Register the calibration data type, instantiate SDC conversion (relies
    // on traits defined above), establish deps
    na64dp::calib::CIDataAliases::self()
                .add_alias_of<na64dp::calib::Placements>("placements", "default");
    mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<p348reco::RunNo_t, na64dp::calib::Placement>();
    na64dp::calib::CIDataAliases::self().add_dependency("placements", "naming");
    // Get detector selection
    const auto sel = na64dp::aux::retrieve_det_selection(cfg);
    // Get the subject
    const std::string subj = cfg["subject"].as<std::string>();
    // Instantiate the handler
    if("APVCluster" == subj) {
        return new na64dp::handlers::CreateGenFitTrackPoints<
                                    na64dp::event::APVCluster>(mgr, sel);
    }  // else if() ...
    NA64DP_RUNTIME_ERROR( "Unknown \"subject\" provided to"
                          " CreateGenFitTrackPoints handler: \"%s\""
                        , subj.c_str() );
}


#endif  // defined(GenFit_FOUND) && GenFit_FOUND

