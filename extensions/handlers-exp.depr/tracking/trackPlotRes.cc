#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include "na64detID/wireID.hh"
#include "na64detID/cellID.hh"

#include <Track.h>
#include <MeasuredStateOnPlane.h>

#include <KalmanFitterInfo.h>

#include <TH1F.h>

#if 0
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#endif

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers
 * */
class TrackPlotRes : public AbstractHitHandler<event::TrackPoint>
                   , public util::TDirAdapter {

private:
    
    int _nBins, _min, _max;
    
    std::string _overridenPath;
    
    std::map<DetID, TH1F *> _histograms;
    
public:
	TrackPlotRes( calib::Dispatcher & cdsp
                , const std::string & only
                , std::string overridenPath
                )
        : AbstractHitHandler<event::TrackPoint>(cdsp, only)
        , util::TDirAdapter(cdsp)
        , _overridenPath(overridenPath) {}
        
    ~TrackPlotRes() {
		for(auto p : _histograms) {
			delete p.second;
		}
	}
                      
    virtual ProcRes process_event(event::Event & ) override;
                            
    virtual bool process_hit( EventID eventID
                            , StationKey did
                            , event::TrackPoint & hit ) override {assert(false);}
                            
    virtual void finalize() override;
};



TrackPlotRes::ProcRes
TrackPlotRes::process_event( event::Event & event ) {
    
    // Plot residuals for each track

    if ( event.tracks.empty() ) {
		return kDiscriminateEvent;
	}

    for ( auto & trackCand : event.tracks ) {
		
		event::Track & track = (*trackCand.second);
    		
		genfit::Track * cTrack = new genfit::Track(*track.genfitTrackRepr);
		
		// Itterate over all track points with fitter info
		for ( unsigned int tpNum = 0
		    ; tpNum < cTrack->getNumPointsWithMeasurement()
		    ; ++tpNum
		    ) {
		
			// start of residual calculating and plotting routine		
			genfit::TrackPoint* tp 
				= cTrack->getPointWithFitterInfo( tpNum , cTrack->getCardinalRep());
			
			// If point has no fitter info, skip
			if ( !tp->hasFitterInfo(cTrack->getCardinalRep()) ) {
				continue;
			}
				
			genfit::AbsFitterInfo * fitterInfo 
				= tp->getFitterInfo(cTrack->getCardinalRep());
					
			genfit::KalmanFitterInfo * kFitterInfo 
				= static_cast<genfit::KalmanFitterInfo*>(fitterInfo);
		
			std::vector<double> weights(kFitterInfo->getWeights());

			
			for (unsigned int measNum = 0
				; measNum <  kFitterInfo->getNumMeasurements()
				; ++measNum ) {
				
				// Get weights of measurements.
				//std::cout << "measurement nr. " << measNum 
				//          << ", weight: " << weights[measNum] << std::endl;
				
				if ( weights[measNum] != 1 ) {
					//continue;
				}
				
				const genfit::MeasuredStateOnPlane residual
							= kFitterInfo->getResidual(measNum);

				genfit::AbsMeasurement * measurement 
									   = tp->getRawMeasurement(measNum);

				DetID did(measurement->getDetId());
					
				TVectorD res = residual.getState();
				TMatrixDSym covM = residual.getCov();
							
				auto it = _histograms.find(did);
					if ( _histograms.end() == it ) {
					std::string hstName = "residuals";
					std::string hstDescription = "residuals of reconstruction";
								
					auto substCtx = subst_dict_for(did, hstName);
					std::string description = util::str_subst(hstDescription, substCtx);
					auto p = dir_for( did, substCtx, _overridenPath);
					p.second->cd();
					TH1F * newHst = new TH1F( p.first.c_str()
											, description.c_str()
											, 200, 1, 1 );
					it = _histograms.emplace(did, newHst).first;
				}
						
				it->second->Fill( res[0] );
			}
			
			weights.clear();
		
		}
		
	}
    
    return kOk;
}

void
TrackPlotRes::finalize(){
	
	for( auto idHstPair : _histograms ) {
		tdirectories()[idHstPair.first]->cd();
		idHstPair.second->Write();
	}
}

REGISTER_HANDLER( TrackPlotRes, ch, cfg
                , "Handler to plot residuals" ) {
    
    return new TrackPlotRes( ch
                           , aux::retrieve_det_selection(cfg)
                           , cfg["path"].as<std::string>()
                           );
}
}
}
#endif  // defined(GenFit_FOUND)
