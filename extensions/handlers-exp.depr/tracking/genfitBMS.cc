#include "na64sw-config.h"
#include <bits/stdc++.h>

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers f1-handlers
 * */
class GenfitTrackPointBMS : public AbstractHitHandler<event::F1Hit> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
    
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<StationKey, DetPlacementEntry *> _placements;
        
protected:
    
    const nameutils::DetectorNaming * _names;
 
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    

public:
                       
	GenfitTrackPointBMS( calib::Dispatcher & ch
					   , const std::string & only )
			: AbstractHitHandler<event::F1Hit>(ch, only) {
				
		std::freopen("output.csv", "w", stdout);
	}
                                 
    virtual bool process_hit( EventID
                            , DetID
                            , event::F1Hit & ) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;

};

static GenfitTrackPointBMS::DetPlacementEntry gPlacements[] = {
    
    // 2021 Muon run
    {"BM1", 0, -116.5250, -8173.8700, 0, 0, 90, 4, 19, 0.5,  64},

    {"BM5", 0,  -93.3200, -7472.8900, 0, 0, 90, 4, 16, 0.25,  64},
    
    {"BM2", 0,  -73.4000, -6832.8500, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM3", 0,   0, -1825.6700, 0, 0, 90, 4, 9, 0.5,  64},
    
    {"BM6", 0,   0, -1538.4467, 0, 0, 90, 4, 16, 0.125, 128},
    
    {"BM4", 0,   0,  -593.3800, 0, 0, 90, 4, 23, 0.5,  64},
        
};

void
GenfitTrackPointBMS::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::F1Hit>::handle_update(nm);
    
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        StationKey plane(nm.id(gPlacements[i].planeName));
        // impose placement entry into `_placements' map
        _placements.emplace(plane, gPlacements + i);
    }
}

bool
GenfitTrackPointBMS::process_hit( EventID
                                , DetID did
                                , event::F1Hit & hit ) {
	
	return true;
}
								

GenfitTrackPointBMS::ProcRes
GenfitTrackPointBMS::process_event(event::Event & event) {
	
	//AbstractHitHandler<event::F1Hit>::process_event( event );
	
	// =================================================================
	std::multimap<StationKey, mem::Ref<event::F1Hit>> _hits;
	
	std::set<StationKey> _uniquePlanes;
	
	for (auto it : event.f1Hits) {
		StationKey pl(it.first);
		_hits.emplace(pl, it.second);
		_uniquePlanes.insert(pl);
	}
	
	typedef std::multimap<StationKey, mem::Ref<event::F1Hit>>::iterator clusIt;
	
	std::pair <clusIt, clusIt> ret;
	
	// Search through all of the unique planes and treat them separately
	for ( auto & it : _uniquePlanes ) {
				
		ret = _hits.equal_range(it);
		
		auto tp = lmem().create<event::TrackPoint>(lmem());
		util::reset(*tp);
    
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		TVectorD hitCoord(1);
		TMatrixDSym hitCov(1);
			
		const DetPlacementEntry * cPlacement = _placements[it];
		
		double resolution = cPlacement->resolution;
		hitCov(0, 0) = resolution*resolution;
		
		int hitId(0), planeId(0);
		
		
		// Rotate detector planes according data from detector placement
		TVector3 o_plane(cPlacement->x, cPlacement->y, cPlacement->z), u_plane(1,0,0), v_plane(0,1,0);
		
		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
                                                         , v_plane));
                                                         
		/* Iterate over all hits in one plane and create TrackPoint
		 * containing genfit-type AbsMeasurements
		 */		 
		
		for ( clusIt ir = ret.first; ir != ret.second; ++ir ) {
					
			//hitCoord(0) = (cPlacement->sizeX * ir->second->rawData->channel / cPlacement->numOfWires) - cPlacement->sizeX / 2;
			// Terrible part which consider different layout of BSM modules
			// TODO: Use smarter geometry of some constant
			
			DetID bms(it);
			
			double fibersize(0);
			
			const int32_t wirePos = ir->second->rawData->channel;
			
			const int32_t timeDecoded = ir->second->rawData->timeDecoded;
						
			#if 0
			std::cout << "New hit in station: " << naming()[it] << ", "
	          << "DAQ channel source: " << ir->second->rawData->sourceID << ", "
	          << "DAQ port ID: " <<  ir->second->rawData->portID << ", "
	          << "DAQ detector channel: " <<  ir->second->rawData->channel << ", "
	          << "channel position: " <<  ir->second->rawData->channelPosition << ", "
	          << "at time: " <<  ir->second->rawData->time << ", "
	          << "time unit: " <<  ir->second->rawData->timeUnit << ", "
	          << "time in ns wrt trigger time: " <<  ir->second->rawData->timeDecoded << ", "
	          << "time reference: " <<  ir->second->rawData->timeReference << std::endl;
	        #endif


			if ( naming()[it] == "BM1" ) {
				
				if ( timeDecoded < -970 || timeDecoded > -965 ) continue;
				
				fibersize = 0.5;
				
				if ( wirePos <= 4 ) {
					hitCoord(0) = fibersize * ( wirePos - 1) - cPlacement->sizeY / 2;
				} else if ( wirePos >4 && wirePos <= 60 ) {
					hitCoord(0) = 2.0 + fibersize * ( (wirePos -5 ) / 2) - cPlacement->sizeY / 2;
				} else {
					hitCoord(0) = 16.0 + fibersize * (wirePos -61 ) - cPlacement->sizeY / 2;
				}
							
				ir->second->hitPosition = hitCoord(0);
			
			}

			
			if ( naming()[it] == "BM2" ) {
				
				if ( timeDecoded < -925 || timeDecoded > -915 ) continue;
				
				fibersize = 0.5; // Thickness of scintillator
				
				if ( wirePos <= 8 ) {
					hitCoord(0) = fibersize * ( wirePos - 1 )/2 - cPlacement->sizeY / 2;
				} else if ( wirePos >8 && wirePos <= 20 ) {
					hitCoord(0) = 2.0 + fibersize * ( (wirePos -9 ) / 4) - cPlacement->sizeY / 2;
				} else if ( wirePos >20 && wirePos <= 44 ) {
					hitCoord(0) = 3.5 + fibersize * ( (wirePos -21 ) / 6) - cPlacement->sizeY / 2;
				} else if ( wirePos >44 && wirePos <= 56 ) {
					hitCoord(0) = 5.5 + fibersize * ( (wirePos -45 ) / 4) - cPlacement->sizeY / 2;
				} else {
					hitCoord(0) = 6.0 + fibersize * ( (wirePos -57 ) / 2) - cPlacement->sizeY / 2;
				}
							
				ir->second->hitPosition = hitCoord(0);
			
			}
			
			if ( naming()[it] == "BM3" ) {
				
				if ( timeDecoded < -410 || timeDecoded > -400 ) continue;
				
				fibersize = 0.5; // Thickness of scintillator
				
				if ( wirePos <= 12 ) {
					hitCoord(0) = fibersize * (wirePos -1 )/2 - cPlacement->sizeY / 2;
				} else if ( wirePos >12 && wirePos <= 20 ) {
					hitCoord(0) = 3.0 + fibersize * ( (wirePos -13 ) / 4) - cPlacement->sizeY / 2;
				} else if ( wirePos >20 && wirePos <= 44 ) {
					hitCoord(0) = 4.0 + fibersize * ( (wirePos -21 ) / 6) - cPlacement->sizeY / 2;
				} else if ( wirePos >44 && wirePos <= 54 ) {
					hitCoord(0) = 6.0 + fibersize * ( (wirePos -45 ) / 4) - cPlacement->sizeY / 2;
				} else {
					hitCoord(0) = 7.5 + fibersize * ( (wirePos -55 ) / 2) - cPlacement->sizeY / 2;
				}
							
				ir->second->hitPosition = hitCoord(0);
			
			}

			if ( naming()[it] == "BM4" ) {
				
				if ( timeDecoded < -370 || timeDecoded > -360 ) continue;
				
				fibersize = 0.5; // Thickness of scintillator
				
				if ( wirePos <= 14 ) {
					hitCoord(0) = fibersize * (wirePos -1) - cPlacement->sizeY / 2;
				} else if ( wirePos >14 && wirePos <= 50 ) {
					hitCoord(0) = 7.0 + fibersize * ( (wirePos -15 ) / 2) - cPlacement->sizeY / 2;
				} else {
					hitCoord(0) = 16.0 + fibersize * (wirePos -51 ) - cPlacement->sizeY / 2;
				}
							
				ir->second->hitPosition = hitCoord(0);
			
			}
			
			if ( naming()[it] == "BM5" ) {
				
				if ( timeDecoded < -975 || timeDecoded > -965 ) continue;
				
				fibersize = 0.25; // Thickness of scintillator
				
				hitCoord(0) = fibersize * wirePos - cPlacement->sizeY / 2;

				ir->second->hitPosition = hitCoord(0);
			
			}
			
			if ( naming()[it] == "BM6" ) {
				
				if ( timeDecoded < -400 || timeDecoded > -390 ) continue;
				
				fibersize = 0.125; // Thickness of scintillator
				
				hitCoord(0) = fibersize * wirePos - cPlacement->sizeY / 2;

				ir->second->hitPosition = hitCoord(0);
			
			}
			
			std::cout << "REAL_HIT" << " "
					  << event.id.run_no() << " " 
	                  << event.id.event_no() << " "
	                  << event.id.spill_no() << " "
					  << naming()[it] << " "
				      << ir->second->rawData->timeDecoded << " "
	                  << ir->second->hitPosition << std::endl;
	                  

			
			genfit::AbsMeasurement * measurement
								   = new genfit::PlanarMeasurement( hitCoord
																  , hitCov
                                                                  , it.id
                                                                  , hitId
                                                                  , nullptr );
	
		    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																		  , planeId);
																		  
			
			genfitTP->addRawMeasurement( measurement );
			
			++hitId;
			
		}
		
		tp->gR[2] = cPlacement->z;
		
		tp->genfitTrackPoint = genfitTP;
		    
		event.trackPoints.emplace(it, tp);
	}
	
	_hits.clear();
	_uniquePlanes.clear();
	
	return kOk;
}

}

REGISTER_HANDLER( GenfitTrackPointBMS, ch, cfg
                , "Handler for BMS tracking detectors genfit::TrackPoint creation" ) {
    
    return new handlers::GenfitTrackPointBMS( ch
                                   , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
