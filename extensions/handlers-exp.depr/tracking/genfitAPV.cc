#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \ingroup handlers tracking-handlers apv-cluster-handlers
 * */
class GenfitTrackPointAPV : public AbstractHitHandler<event::APVCluster> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
        
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID, DetPlacementEntry *> _placements;
        
protected:

    
    void _rotate_detector_plane( const DetPlacementEntry * cPlacement
                               , TVector3 & o_plane
                               , TVector3 & u_plane
                               , TVector3 & v_plane );

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    

public:
                       
	GenfitTrackPointAPV( calib::Dispatcher & ch
					   , const std::string & only )
			: AbstractHitHandler<event::APVCluster>(ch, only) {

	//std::freopen("output.csv", "w", stdout);
				
}
                                 
    virtual bool process_hit( EventID
                            , PlaneKey
                            , event::APVCluster & ) override;
                            
    virtual ProcRes process_event(event::Event & event ) override;

};

static GenfitTrackPointAPV::DetPlacementEntry gPlacements[] = {
    
    #if 1
    // 2021 Muon run
    {"MM1X", -4.4300, -81.2800, -7082.9300, 0, 0, 0, 8.1, 8.1, 0.25, 320},
    {"MM1Y", -4.4300, -81.2800, -7082.9250, 0, 0, 0, 8.1, 8.1, 0.25, 320},
        
    {"MM2X", -3.4300, -79.7200, -6999.8000, 0, 0, 0, 8.1, 8.1, 0.25, 320},
    {"MM2Y", -3.4300, -79.7200, -6999.7950, 0, 0, 0, 8.1, 8.1, 0.25, 320}, 
    
    {"MM3X",  0.3800,   0.3800, -1768.6400, 0, 0, 0, 8.1, 8.1, 0.25, 320},
    {"MM3Y",  0.3800,   0.3800, -1768.6350, 0, 0, 0, 8.1, 8.1, 0.25, 320},
        
    {"MM4X",  0.9000,   0.8800, -1687.1100, 0, 0, 0, 8.1, 8.1, 0.25, 320},
    {"MM4Y",  0.9000,   0.8800, -1687.1050, 0, 0, 0, 8.1, 8.1, 0.25, 320},

    {"GM1X", 0.7117, 0.2333, 432.4900, 0, 0,  0, 10.6, 10.6, 0.4, 256},
    {"GM1Y", 0.7117, 0.2333, 432.4950, 0, 0, 90, 10.6, 10.6, 0.4, 256},

    {"GM2X", 0.35, -2.3083, 600.8400, 0, 0,  0, 10.6, 10.6, 0.4, 256},
    {"GM2Y", 0.35, -2.3083, 600.8450, 0, 0, 90, 10.6, 10.6, 0.4, 256},

    {"GM3X", 0.7583, -1.7766, 759.2800, 0, 0,  0, 10.6, 10.6, 0.4, 256},
    {"GM3Y", 0.7583, -1.7766, 759.2850, 0, 0, 90, 10.6, 10.6, 0.4, 256},

    {"GM4X", 1.65, 0, 813.9800, 0, 0,  0, 10.6, 10.6, 0.4, 256},
    {"GM4Y", 1.65, 0, 813.9850, 0, 0, 90, 10.6, 10.6, 0.4, 256},  
    
    {"MM5X", -5.51, 0.1320, 1126.0000, 0, 0, 0, 24, 8.1, 0.25, 960},
    {"MM5Y", -5.51, 0.1320, 1126.0050, 0, 0, 90, 8.1, 24, 0.25, 320},
    
    {"MM6X", -5.6383, 1.9417, 1161.1500, 0, 0, 0, 24, 8.1, 0.25, 960},
    {"MM6Y", -5.6383, 1.9417, 1161.1550, 0, 0, 90, 8.1, 24, 0.25, 320},
    
    {"MM7X", -4.8120, 2.1000, 1199.4000, 0, 0, 0, 24, 8.1, 0.25, 960},
    {"MM7Y", -4.8120, 2.1000, 1199.4000, 0, 0, 90, 8.1, 24, 0.25, 320},
    #endif
    // Placement for 2017
    // StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // sizeX, sizeY, 
    // resolution, number of wires
	
	#if 0    
    {"MM1X", -0.21484, -1.309757, 35.5000, 0,  0,  45+180, 8.1, 8.1, 0.025, 320},
    {"MM1Y", -0.21484, -1.309757, 35.5050, 0,  0,  45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM2X", 0.772714, -1.189551, 44.5,  0,  0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM2Y", 0.772714, -1.189551, 44.5050,  0,  0, 45, 8.1, 8.1, 0.025, 320}, 
    
    {"MM3X", -0.202403, -0.831729, 180.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM3Y", -0.202403, -0.831729, 180.5050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM4X", 1.332376, -1.415417, 189.5, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM4Y", 1.332376, -1.415417, 189.5050, 0, 0, 45, 8.1, 8.1, 0.025, 320},
        
    {"MM5X", -32.036361, -1.820219, 1688.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM5Y", -32.036361, -1.820219, 1688.5050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
         
    {"MM7X", -36.409401, -0.9791366, 1902, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM7Y", -36.409401, -0.9791366, 1902.0050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"GM1X", -29.510777, 0.309302, 1662, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM1Y", -29.510777, 0.309302, 1662.0050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM2X", -34.381352, -0.236811, 1860.5, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM2Y", -34.381352, -0.236811, 1860.5050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM3X", -34.841116, -0.002498, 1725.2, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM3Y", -34.946117, -0.002498, 1725.2050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM4X", -34.94229, 0.741795, 1837.1, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM4Y", -34.94229, 0.741795, 1837.1050, 0, 0, 90, 10.6, 10.6, 0.04, 256},    
    #endif
   

	// Original NA64 Placements
    #if 0  
    {"MM1X", -1.81, -0.616, -1967, 0,  0,  45+180, 8.1, 8.1, 0.025, 320},
    {"MM1Y", -1.81, -0.616, -1967.0050, 0,  0,  45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM2X", -0.909, -0.582, -1958,  0,  0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM2Y", -0.909, -0.582, -1958.0050,  0,  0, 45, 8.1, 8.1, 0.025, 320}, 
    
    {"MM3X", -1.83, -0.13, -1822, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM3Y", -1.83, -0.13, -1822.0050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM4X", -0.38, -0.8, -1813, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM4Y", -0.38, -0.8, -1813.0050, 0, 0, 45, 8.1, 8.1, 0.025, 320},
        
    {"MM5X", -32.51, -0.85, -317, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM5Y", -32.51, -0.85, -317.5050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
         
    {"MM7X", -36.41, -0.025, -103, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM7Y", -36.41, -0.025, -103.0050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"GM1X", -29.4, 0, -300.3, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM1Y", -29.4, 0, -300.3050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM2X", -34.9, 0, -112.6, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM2Y", -34.9, 0, -112.6050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM3X", -34.2, 0, -238.2, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM3Y", -34.2, 0, -238.2050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM4X", -34.3, 0, -136.1, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM4Y", -34.3, 0, -136.1050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
	#endif
	
	// Without angle corrections
    #if 0
    {"MM1X", -1.81, -0.616, 35.5000, 0,  0,  45, 8.1, 8.1, 0.025, 320},
    {"MM1Y", -1.81, -0.616, 35.5000, 0,  0,  45+90, 8.1, 8.1, 0.025, 320},
        
    {"MM2X", -0.909, -0.582, 44.5,  0,  0, 135, 8.1, 8.1, 0.025, 320},
    {"MM2Y", -0.909, -0.582, 44.5050,  0, 135+90, 45, 8.1, 8.1, 0.025, 320}, 
    
    {"MM3X", -1.83, -0.13, 180.5, 0, 0, 45, 8.1, 8.1, 0.025, 320},
    {"MM3Y", -1.83, -0.13, 180.5050, 0, 0, 45+90, 8.1, 8.1, 0.025, 320},
        
    {"MM4X", -0.38, -0.8, 189.5, 0, 0, 135, 8.1, 8.1, 0.025, 320},
    {"MM4Y", -0.38, -0.8, 189.5050, 0, 0, 135+90, 8.1, 8.1, 0.025, 320},
        
    {"MM5X", -32.51, -0.85, 1688.5, 0, 0, 45, 8.1, 8.1, 0.025, 320},
    {"MM5Y", -32.51, -0.85, 1688.5050, 0, 0, 45+90, 8.1, 8.1, 0.025, 320},
         
    {"MM7X", -36.41, -0.025, 1902, 0, 0, 45, 8.1, 8.1, 0.025, 320},
    {"MM7Y", -36.41, -0.025, 1902.0050, 0, 0, 45+90, 8.1, 8.1, 0.025, 320},
        
    {"GM1X", -29.4, 0, 1662, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM1Y", -29.4, 0, 1662.0050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM2X", -34.9, 0, 1860.5, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM2Y", -34.9, 0, 1860.5050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM3X", -34.2, 0, 1725.2, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM3Y", -34.2, 0, 1725.2050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM4X", -34.3, 0, 1837.1, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM4Y", -34.3, 0, 1837.1050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
	#endif
	
	// Original in CORAL geo
    #if 0
    {"MM1X", -1.81, -0.616, 35.5000, 0,  0,  45+180, 8.1, 8.1, 0.025, 320},
    {"MM1Y", -1.81, -0.616, 35.5000, 0,  0,  45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM2X", -0.909, -0.582, 44.5,  0,  0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM2Y", -0.909, -0.582, 44.5050,  0,  0, 45, 8.1, 8.1, 0.025, 320}, 
    
    {"MM3X", -1.83, -0.13, 180.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM3Y", -1.83, -0.13, 180.5050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"MM4X", -0.38, -0.8, 189.5, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
    {"MM4Y", -0.38, -0.8, 189.5050, 0, 0, 45, 8.1, 8.1, 0.025, 320},
        
    {"MM5X", -32.51, -0.85, 1688.5, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM5Y", -32.51, -0.85, 1688.5050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
         
    {"MM7X", -36.41, -0.025, 1902, 0, 0, 45+180, 8.1, 8.1, 0.025, 320},
    {"MM7Y", -36.41, -0.025, 1902.0050, 0, 0, 45-90, 8.1, 8.1, 0.025, 320},
        
    {"GM1X", -29.4, 0, 1662, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM1Y", -29.4, 0, 1662.0050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM2X", -34.9, 0, 1860.5, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM2Y", -34.9, 0, 1860.5050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM3X", -34.2, 0, 1725.2, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM3Y", -34.2, 0, 1725.2050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
        
    {"GM4X", -34.3, 0, 1837.1, 0, 0, 0, 10.6, 10.6, 0.04, 256},
    {"GM4Y", -34.3, 0, 1837.1050, 0, 0, 90, 10.6, 10.6, 0.04, 256},
	#endif

	#if 0
    // 2018 Placements   
    {"MM1X",  -1, 0, -1967.10, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM1Y",  -1, 0, -1967.11, 0,  0,  -45, 8., 8., 0.2, 320},
    {"MM2X",  -1.7, 0, -1820.10,  0,  0, 45, 8., 8., 0.2, 320},
    {"MM2Y",  -1.7, 0, -1820.11,  0,  0, 45, 8., 8., 0.2, 320}, 
    {"MM3X",  -31.9, 0, -364.60, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM3Y",  -31.9, 0, -364.61, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4X",  -32.9, 0, -345.20, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM4Y",  -32.9, 0, -345.21, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM5X",  -38.1, 0, -119.10, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM5Y",  -38.1, 0, -119.11, 0, 0, 45, 8., 8., 0.2, 320},
    {"MM6X", -39.7, 0, -97.90, 0, 0, -45, 8., 8., 0.2, 320},
    {"MM6Y", -39.7, 0, -97.91, 0, 0, -45, 8., 8., 0.2, 320},
    
    {"GM1X", -25.9, 0, -86.30, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM1Y", -25.9, 0, -86.31, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2X", -23.3, 0, -160.60, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM2Y", -23.3, 0, -160.61, 0, 0, 0, 10., 10., 0.2, 256},   
    {"GM3X", -25.1, 0, -105.20, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM3Y", -25.1, 0, -105.21, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4X", -24.2, 0, -143.70, 0, 0, 0, 10., 10., 0.2, 256},
    {"GM4Y", -24.2, 0, -143.71, 0, 0, 0, 10., 10., 0.2, 256},
    #endif
    
    
};

void
GenfitTrackPointAPV::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::APVCluster>::handle_update(nm);
     
    for( long unsigned int i = 0; i < sizeof(gPlacements)/sizeof(DetPlacementEntry); ++i ) {
        
        // retrieve numerical detector id by its string name
        PlaneKey plane(nm.id(gPlacements[i].planeName));
        // impose placement entry into `_placements' map
        _placements.emplace(plane, gPlacements + i);
    }
}

bool
GenfitTrackPointAPV::process_hit( EventID
                                , PlaneKey planekey
                                , event::APVCluster & hit ) { 
	
	#if 0
	std::cout << "Station: " << naming()[planekey] << ", "
	          << "at position: " << hit.position << ", "
	          << "with charge: " << hit.charge << ", "
	          << "at time: " << hit.time << ", " 
	          << "with error: " << hit.timeError << std::endl;	        
	#endif
	
	return true; 
	
}

GenfitTrackPointAPV::ProcRes
GenfitTrackPointAPV::process_event(event::Event & event) {
								
	AbstractHitHandler<event::APVCluster>::process_event( event );

	std::multimap<PlaneKey, mem::Ref<event::APVCluster>> _hits;
	
	std::set<PlaneKey> _uniquePlanes;
	
	for (auto & it : event.apvClusters) {
		_hits.emplace(it.first, it.second);
		_uniquePlanes.insert(it.first);
	}
	
	typedef std::multimap<PlaneKey, mem::Ref<event::APVCluster>>::iterator clusIt;
	
	std::pair <clusIt, clusIt> ret;
	
	// Search through all of the unique planes and treat them separately
	for ( auto & it : _uniquePlanes ) {
		
		DetID did(it);
		
		ret = _hits.equal_range(it);
		//std::cout << "new hit in layer: " << naming()[did] << std::endl;
		
		auto tp = lmem().create<event::TrackPoint>(lmem());
		util::reset(*tp);
    
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		TVectorD hitCoord(1);
		TMatrixDSym hitCov(1);
			
		const DetPlacementEntry * cPlacement = _placements[did];
		
		double resolution = cPlacement->resolution;
		hitCov(0, 0) = resolution*resolution;
		
		int hitId(0), planeId(0);
		
		
		// Rotate detector planes according data from detector placement
		TVector3 o_plane(0,0,0), u_plane(0,0,0), v_plane(0,0,0);
		_rotate_detector_plane( cPlacement
							  , o_plane
                              , u_plane
                              , v_plane )
                              ;

		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
                                                         , v_plane));
                                                         
		/* Iterate over all hits in one plane and create TrackPoint
		 * containing genfit-type AbsMeasurements
		 */
		
		for ( clusIt ir = ret.first; ir != ret.second; ++ir ) {
			//std::cout << "Hit in: " << naming()[ir->first] << ", "
			//          << "Cluster position: " << (*ir->second).position << std::endl;
			   
			hitCoord(0) = (cPlacement->sizeX * (*ir->second).position / cPlacement->numOfWires) - cPlacement->sizeX / 2;
			
			//Dump data in file
                                              
			std::cout << "REAL_HIT" << " "
					  << event.id.run_no() << " " 
					  << event.id.event_no() << " "
                      << event.id.spill_no() << " "
				      << naming()[it] << " "
				      << 123 << " "
			          //<< ir->second->time << " "
                      << hitCoord(0) << std::endl;

			
			
			genfit::AbsMeasurement * measurement
								   = new genfit::PlanarMeasurement( hitCoord
																  , hitCov
                                                                  , did
                                                                  , hitId
                                                                  , nullptr );
	
		    static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																		  , planeId);
			
			
			genfitTP->addRawMeasurement( measurement );
			
			++hitId;
			
		}
		
		tp->gR[2] = cPlacement->z;
		
		//std::cout << "end of layer: " << naming()[it] << std::endl;
		
		tp->genfitTrackPoint = genfitTP;
    
		event.trackPoints.emplace(did, tp);
		
	}
	
	std::cout << std::endl;
	
	_hits.clear();
	_uniquePlanes.clear();
	
	return kOk;
}

void
GenfitTrackPointAPV::_rotate_detector_plane( const DetPlacementEntry * cPlacement
                                           , TVector3 & o_plane
                                           , TVector3 & u_plane
                                           , TVector3 & v_plane ) {
        
        o_plane.SetXYZ( cPlacement->x, cPlacement->y, cPlacement->z );  
        u_plane.SetXYZ( cPlacement->sizeX, 0, 0 );
        v_plane.SetXYZ( 0, cPlacement->sizeY, 0 );
        
        if (cPlacement->rotAngleX != 0) {
            u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
            v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
        }

        if (cPlacement->rotAngleY != 0) {
            u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
            v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
        }
            
        if (cPlacement->rotAngleZ != 0) {
            u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
            v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
        }       
                                       
}

}

REGISTER_HANDLER( GenfitTrackPointAPV, ch, cfg
                , "Handler for APV tracking detectors genfit::TrackPoint creation" ) {
    
    return new handlers::GenfitTrackPointAPV( ch
                                , aux::retrieve_det_selection(cfg) );
}
}

#endif  // defined(GenFit_FOUND)
