#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"
#include "na64detID/TBName.hh"
#include "na64util/json.hh"
#include "na64event/stream.hh"
#include "na64event/data/event.hh"
#include "na64event/id.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Template handler for event serialization to JSON file
 *
 * A pretty basic handler converting the entire event to JSON dictionary.
 * Because it currently does not takes into account `mem::Ref` destination,
 * generates a huge output with duplicated data (each hit can be referenced
 * multiple times -- _per se_ and in the various collections).
 *
 * According to JSON specification:
 *
 *  - insertion order should not matter (so, we add "$id" field for ordered
 *    collections
 *  - all keys must be converted to strings (even numerical ones)
 *  - trailing comma is not allowed
 *
 * \todo Correctly take into account `mem::Ref`s to decrease output
 * \todo Make it template around stream implementation
 * */
class JSONSerializeEvent : public AbstractHandler
                         , public calib::Handle<nameutils::DetectorNaming> {
protected:
    /// Output file
    std::ofstream _outFile;
    /// JSON stream wrapper
    util::json::OutStream _jsonSerializer;
    event::Stream<util::json::OutStream> _stream;
    size_t _eventsCount;
    /// When set, detector IDs will be converted to string; otherwise,
    /// bumerical identifiers will be used (as hex string, though).
    bool _detIDsToStr;
protected:
    void handle_update( const nameutils::DetectorNaming & nm ) override {
        calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
        if(_detIDsToStr)
            _jsonSerializer.jsParams.nmPtr = &nm;
    }
public:
    JSONSerializeEvent( calib::Dispatcher & dsp
                      , const std::string & outFile
                      , const std::string & namingSubClass="default"
                      , bool detIDsToStr=true
                      )
            : calib::Handle<nameutils::DetectorNaming>(namingSubClass, dsp)
            , _outFile(outFile)
            , _jsonSerializer(_outFile)
            , _stream(_jsonSerializer)
            , _eventsCount(0)
            , _detIDsToStr(detIDsToStr)
            {
        _outFile << "[";
    }

    ProcRes process_event(event::Event & event) override {
        //assert(_jsonSerializer._stack.empty());
        if( _eventsCount ) _outFile << ",";
        size_t dummy = 0;
        _stream.op(dummy, event);
        ++_eventsCount;
        return kOk;
    }

    void finalize() override {
        _outFile << "]" << std::endl;  // todo: remove endl here
    }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( JSONDump
                , cdsp, cfg
                , "Dumps events to JSON file." ) {
    return new na64dp::handlers::JSONSerializeEvent(cdsp
                , cfg["file"].as<std::string>()
                , cfg["namingSubclass"]
                        ? cfg["namingSubclass"].as<std::string>()
                        : "default"
                , cfg["stringDetIDs"]
                        ? cfg["stringDetIDs"].as<bool>()
                        : true
                );
}

