#pragma once

#include "na64sw-config.h"
#include "na64dp/abstractHitHandler.hh"

#include <TH1F.h>

namespace na64dp {
namespace handlers {

/**\brief A simple handler to plot energy deposition
 *
 * Handler to plot energy deposition in VETO with a certain
 * threshold in HCAL. For dimuon production analysis.
 *
 * If energy depositions in the calorimeters are within certain thresholds,
 * puts energy deposition in vero detector and fill the histogram; give
 * up otherwise.
 *
 * Requires that ECAL/HCAL/VETO energy deposition are described within CaloHits.
 * */
class DiMuon : public AbstractHitHandler<event::CaloHit> {
private:
    /// Energy deposition in VETO
    TH1F * _eDep;
    const double _ecalERange[2]  ///< thresholds in ECAL
               , _hcalERange[2]  ///< thresholds in HVAL
               ;
protected:
    /// Name caches
    DetID kECAL, kHCAL, kVETO;
    /// Caches also the veto detector kin ID
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    /// Creates a plotting handler with certain thresholds for calos
    DiMuon( calib::Dispatcher & dsp
          , double minEnergyHCAL = 0., double maxEnergyHCAL = 200.
          , double minEnergyECAL = 0., double maxEnergyECAL = 200.
          );
    /// Fills VETO energy deposition histogram, if matches
    virtual ProcRes process_event(event::Event &) override;
    /// not used
    virtual bool process_hit( EventID eventID
                            , HitKey detID
                            , event::CaloHit & hit ) override {assert(false);}
    /// Writes histogram if exists
    virtual void finalize() override;
};

}
}
