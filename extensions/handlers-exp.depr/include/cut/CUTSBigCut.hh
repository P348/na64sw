#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief 

 * 
 * */
class CUTSBigCut : public AbstractHitHandler<SADCHit> {
private:
    // Using cat on max sample
    bool _catChannelFlag;
    int _catSampLeft,
        _catSampRight;
    // Using cat on rising edge 
    bool _cattimeBeginFlag;
    int _cattimeBeginLeft,
        _cattimeBeginRight;
    // Using cat on angel
    bool _catTanFlag;
    double _angelLow,
           _angelUp; 
public:
    CUTSBigCut  ( calib::Dispatcher & ch
                  , const std::string & only
                  , bool catChannelFlag=false
                  , int catSampLeft=8
                  , int catSampRight=26
                  , bool cattimeBeginFlag=false
                  , int cattimeBeginLeft=8
                  , int cattimeBeginRight=26
                  , bool catTanFlag=false
                  , double angelLow=70
                  , double angelUp=90
                  ) : AbstractHitHandler<SADCHit>(ch, only)   
                    , _catChannelFlag(catChannelFlag)
                    , _catSampLeft(catSampLeft)
                    , _catSampRight(catSampRight)
                    , _cattimeBeginFlag(cattimeBeginFlag)
                    , _cattimeBeginLeft(cattimeBeginLeft)
                    , _cattimeBeginRight(cattimeBeginRight)  
                    , _catTanFlag(catTanFlag) 
                    , _angelLow(angelLow)
                    , _angelUp(angelUp)
                    {}
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentHit);
};

}
}
