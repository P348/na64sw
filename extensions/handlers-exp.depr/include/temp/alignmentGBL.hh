#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"


#include <SharedPlanePtr.h>
#include <TH1D.h>
#include <TH2D.h>

namespace genfit {  // fwd decls
class Track;
class EventDisplay;
class AbsTrackRep;
class StateOnPlane;
class AbsKalmanFitter;
class AbsMeasurement;
class GFGbl;
class GblFitter;
}

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \todo running without event display
 * \todo `gGeoManager` is a global instance provided by ROOT -- must be instantiated by app
 * */
class AlignmentGBL : public AbstractHitHandler<TrackPoint>
                     , public TDirAdapter {
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY, sizeZ;
        float resolution;
        float numOfWires;
    };

private:
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
    //// GenFit  track fitter instance (e.g. `KalmanFitterRefTrack`)
    genfit::AbsKalmanFitter * _cFitter;
    
    genfit::GFGbl * _cGBL;
    
    /// General Broken Lines filter for the mille file production
    genfit::GblFitter * _cFitterGbl; 
    /// Generic GenFit track representation ptr (e.g. Runge-Kutta)
    genfit::AbsTrackRep* _cRep;
    /// GenFit track object pointer
    genfit::Track* _cTrack;
    /// ???
    genfit::StateOnPlane* _cState;
    /// ???
    genfit::StateOnPlane* _cStateRefOrig;
    /// Event number counted by tracker. TODO: Ask R.R. to implement here global event_counter
    unsigned int _gEvent;
    /// Number of track points being accounted by `process_hit()`
    int _nOfApvHits;
    /// ??? Particle momentum hypothesis
    double _momentum;
    /// Measurements 
    std::vector<genfit::AbsMeasurement*> _retMeasurements;
    /// Vectors with station names. TODO: redone!!!
    std::vector<std::string> _detNames;
    /// Planes
    std::vector<genfit::SharedPlanePtr> _planePtrs;    
    /// Minimal hit number defined by user
    int _minHits;

     
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    AlignmentGBL( calib::Dispatcher & dsp
                  , const std::string & only
                  , const std::string & geoFilePath
                  , int minIts, int maxIts, int minHits
                  );
    virtual ProcRes process_event(Event * ) override;
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;
    virtual void finalize() override;
};

}
}

#endif  //  defined(GenFit_FOUND)



