#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackTP : public AbstractHitHandler<TrackPoint> {

private:
        
    std::vector<genfit::TrackPoint*> _trackPoints;
    std::set < DetID_t > _uniqueIDs;
    std::vector<TrackPoint> _trackPointsTP;

public:

	std::ofstream _outputup;
    std::ofstream _outputdown;
	

public:
    TrackTP( calib::Dispatcher & ch
             , const std::string & only
             , ObjPool<TrackPoint> & obTP
             , ObjPool<Track> & bank );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;
    
    virtual void finalize() override;
             
};

}
}

#endif  //  defined(GenFit_FOUND)


