#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

class APVCorrHitTime : public AbstractHitHandler<APVCluster> {

public:

    /// Default ctr
    APVCorrHitTime( calib::Dispatcher & dsp
                  , const std::string & select
                  , ObjPool<APVCluster> & );
    
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVCluster & ) override;
};

}
}

