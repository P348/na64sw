#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TPCandAPV : public AbstractHitHandler<TrackPoint> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
	
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
    
    std::multimap<double, int> _ros;

protected:

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
    //void _insert_clusters( Event & e, DetID_t did, Clusters & cluster, Track & track );

public:
    TPCandAPV( calib::Dispatcher & ch
                  , const std::string & only
                  , ObjPool<TrackPoint> & obTP );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;

};

}
}
