#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

namespace na64dp {
namespace handlers {

class APVCalcHitTimes : public AbstractHitHandler<APVHit>
                      , public TDirAdapter {

public:

    struct MMCalibData {
		const char * planeName;
		double a02timeParam[4];
		double a02timeError[6];
		double a12timeParam[4]; 
		double a12timeError[6];
    };
    
    struct MMWireCalibData {
		const char * planeName;
		double chCalib[64];
    };
    
    struct GEMCalibData {
		const char * planeName;
		double a02timeParam[5];
		double a12timeParam[5]; 
    };

private:

    DetKin_t _gmKinID  ///< GEMs kin ID cache
           , _mmKinID  ///< MuMegas kin ID cache
           ;
           
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, MMCalibData *> _mmCalibs;
    
    std::map<DetID_t, MMWireCalibData *> _mmWireCalibs;
    
    std::map<DetID_t, GEMCalibData *> _gemCalibs;

protected:

    virtual void handle_update( const nameutils::DetectorNaming & ) override;

	virtual double _get_hit_timeMM( DetID_t did , const APVHit & cHit, double & ratio, int mode) const;
	
	virtual double _get_hit_timeRise( DetID_t did , const APVHit & cHit, double & ratio, double time) const;

    virtual double _get_hit_sigmaMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const;
    
public:


    /// Default ctr
    APVCalcHitTimes( calib::Dispatcher & dsp, const std::string & select );
    
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVHit & ) override;
};

}
}

