#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based handler for proccessing SADC hits of Hodoscopes.
 *
 * Proccesses information from SADC chips of hodoscopes, applies
 * calibrations (as there is simply routine no need in special handler
 * arrises yet, but might be point for discussion) and attaches hits to
 * genfit::Track (maybe it will be smarter to add hits to trackpoint
 * field).
 *
 * */
class TrackHOD : public AbstractHitHandler<SADCHit> {
	
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
	
	/// Define hodoscope strip number after DetID_t decoding
	typedef int HODStripNo;
	
	/// Define numerical value for hodoscope projection (29 or 30)
	typedef int HODProj;
	
	/// Hodoscope hits ordered by "wire number" 
	/// (in fact by station subnumber)
	typedef std::map<DetID_t, PoolRef<SADCHit>> Hits;
	
	/// Define NA64HOD local cluster
	typedef std::map<HODStripNo, PoolRef<SADCHit>> HODCluster;
	
	
private:

	/// Routine to insert track point
	HitsInserter<TrackPoint> _tpInserter;

	///< HODs kin ID cache
	DetKin_t _hodKinID;
	
	int _threshold;

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;

    /// Map of SADC hits for hodoscope
    std::map<DetID_t, Hits> _hodoHits;
    
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
    DetID _create_unique_det( DetID did );
    
    void _insert_hod_hits( Event & e, DetID did, Hits & rawHits );
    
    bool _possibly_belongs_to_cluster( const HODCluster & clus
								     , HODStripNo wNo
								     , const SADCHit & cHit ) const;
								
	double _get_cluster_center( const HODCluster & cluster ) const;

public:
    TrackHOD( calib::Dispatcher & dsp
            , const std::string & only
            , ObjPool<TrackPoint> & bank
            , int threshold );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

#endif  //  defined(GenFit_FOUND)


