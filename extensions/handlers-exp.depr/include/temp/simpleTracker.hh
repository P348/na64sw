#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"


#include <SharedPlanePtr.h>
#include <TH1D.h>
#include <TH2D.h>

namespace genfit {  // fwd decls
class Track;
class EventDisplay;
class AbsTrackRep;
class StateOnPlane;
class AbsKalmanFitter;
class AbsMeasurement;
}

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class SimpleTracker : public AbstractHitHandler<TrackPoint>
                     , public TDirAdapter {
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY, sizeZ;
        float resolution;
        float numOfWires;
    };

private:
    /// Event display on
    bool _eventDisplay;
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
    //// GenFit  track fitter instance (e.g. `KalmanFitterRefTrack`)
    genfit::AbsKalmanFitter * _cFitter;
    /// Generic GenFit track representation ptr (e.g. Runge-Kutta)
    genfit::AbsTrackRep* _cRep;
    /// GenFit track object pointer
    genfit::Track* _cTrack;
    /// ???
    genfit::StateOnPlane* _cState;
    /// ???
    genfit::StateOnPlane* _cStateRefOrig;
    /// Event number counted by tracker. TODO: Ask R.R. to implement here global event_counter
    unsigned int _gEvent;
    /// ??? Particle momentum hypothesis
    double _momentum;
    /// Measurements 
    std::vector<genfit::AbsMeasurement*> _retMeasurements;
    /// PLane pointers for propagation to corresponding measurement
    std::vector<genfit::SharedPlanePtr> _planePtrs;
    
    /// Hodoscope hit maps to define the best one. TODO: need in separate handler
    std::map<std::string, int> _HOD0X;
    std::map<std::string, int> _HOD0Y;
    std::map<std::string, int> _HOD1X;
    std::map<std::string, int> _HOD1Y;
    
    /// Flags for reps
    bool _nHadron;
    bool _cHadron;
    bool _tElectron; // True electron hypothesis based on SRD signal
    bool _vetoV1;
    bool _vetoV2;
    bool _veto;
    bool _muon;

    
    /// Particle rep counters
    int _e;
    int _mu;
    int _pi;
    int _ka;
    int _p;
    
    // Momentum resolution
    TH1D *hMom;

protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    SimpleTracker( calib::Dispatcher & dsp
                  , const std::string & only
                  , bool eventDisplay
                  , const std::string & geoFilePath
                  , int minIts, int maxIts
                  );
                  
    virtual ProcRes process_event(Event * ) override;
    virtual bool process_sadc_hit( DetID did
                            , const SADCHit & );
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;
    virtual void finalize() override;
};

}
}

#endif  //  defined(GenFit_FOUND)


