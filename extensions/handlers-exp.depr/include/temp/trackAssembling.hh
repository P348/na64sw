#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"
#include "na64event/hitInserter.hh"

namespace genfit {  // fwd decls
class Track;
}

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackAssembling : public AbstractHitHandler<TrackPoint>
                      , public TDirAdapter {
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY, sizeZ;
        float resolution;
        float numOfWires;
    };

private:
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
    /// GenFit track object pointer
    genfit::Track* _cTrack;
    /// Track allocator reference
    ObjPool<Track> & _tracksBank;

protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    TrackAssembling( calib::Dispatcher & dsp
                  , const std::string & only
                  , ObjPool<Track> & bank );
                  
    virtual ProcRes process_event(Event * ) override;
    virtual bool process_sadc_hit( DetID did
                            , const SADCHit & );
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override {assert(false);}
};

}
}

#endif  //  defined(GenFit_FOUND)


