#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackAPV : public AbstractHitHandler<APVCluster> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
    
    /// Clusters with their counters for each detector plane
    typedef std::multimap<int, PoolRef<APVCluster>> Clusters;

private:

	/// Routine to insert track point
	HitsInserter<TrackPoint> _tpInserter;

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
  
    /// Map of clustered hits and corresponding detector planes
    std::map<DetID_t, Clusters> _detHits;

protected:

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
    void _insert_clusters( Event & e, DetID_t did, Clusters & cluster );

public:
    TrackAPV( calib::Dispatcher & ch
                  , const std::string & only
                  , ObjPool<TrackPoint> & obTP );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVCluster & ) override;
};

}
}

#endif  //  defined(GenFit_FOUND)


