#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based handler for proccessing SADC hits of SRD.
 *
 * Proccesses information from SADC chips of srd and applies calibrations
 *
 * */
class CalibSRD : public AbstractHitHandler<SADCHit> {
	
public:
    struct SRDCalib {
        const char * planeName;
        double energy;
        double time;
        double timeError;
    };
	
private:

	DetKin_t _srdKinID;  ///< SRDs kin ID cache

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, SRDCalib *> _calibs;
    
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    CalibSRD( calib::Dispatcher & dsp
            , const std::string & only );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

