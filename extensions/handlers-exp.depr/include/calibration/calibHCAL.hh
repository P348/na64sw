#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing SADC hits of HCAL.
 *
 * Proccesses information from SADC chips of hcal and applies calibrations
 *
 * */
class CalibHCAL : public AbstractHitHandler<SADCHit> {
	
public:
    struct HCALCalib {
        const char * planeName;
        double factor;
        double mkfact;
        double energy;
        double time;
        double timeError;
    };
    
    struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t hcalKinCode;
    };
	
private:

	double _timeCut;

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, HCALCalib *> _calibs;
    
protected:

	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;

    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    CalibHCAL( calib::Dispatcher & dsp
             , const std::string & only
             , double timeCut );
                                              
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

