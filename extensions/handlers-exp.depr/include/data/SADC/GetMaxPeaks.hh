#pragma once

#include "na64dp/abstractHitHandler.hh"
#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief 

 * 
 * */
class GetMaxPeaks : public AbstractHitHandler<SADCHit> {
private:
  // Discarding events with a certain number of peaks
  bool _cutPileUp;
  // Number of peaks with which events pass the condition
  int _countPeak,
  // The range of channels in which the selection of an event with one peak
      _SampLeft,
      _SampRight, 
  // Number of smoothing cycles
      _smLoop;
public:
    GetMaxPeaks( calib::Dispatcher & ch
               , const std::string & only
               , bool cutPileUp=true
               , int countPeak=1
               , int SampLeft=0
               , int SampRight=31
               , int smLoop=0
               ) : AbstractHitHandler<SADCHit>(ch, only)
                 , _cutPileUp(cutPileUp)
                 , _countPeak(countPeak)
                 , _SampLeft(SampLeft)
                 , _SampRight(SampRight)
                 , _smLoop(smLoop)  
               {}
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentHit);
};

}
}
