#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based handler for proccessing SADC hits of Hodoscopes.
 *
 * Proccesses information from SADC chips of hodoscopes, applies
 * calibrations (as there is simply routine no need in special handler
 * arrises yet, but might be point for discussion) and attaches hits to
 * genfit::Track (maybe it will be smarter to add hits to trackpoint
 * field).
 *
 * */
class HODHitProcess : public AbstractHitHandler<SADCHit> {
	
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
    
	/// Define hodoscope strip number after DetID_t decoding
	typedef int HODStripNo;
	
	/// Define numerical value for hodoscope projection (29 or 30)
	typedef int HODProj;
    
    /// Structure describing HODoscope hit
    struct HODHit {
		DetID_t did;
		HODStripNo wireNo;
		PoolRef<SADCHit> ref;
		bool xproj, yproj;
		double time;
	};
    	
	/// Hodoscope hits ordered by "wire number" 
	/// (in fact by station subnumber)
	typedef std::map<HODStripNo, HODHit> Hits;
	
	typedef std::pair<HODHit, HODHit> hitPair;
	
private:

	/// Routine to insert track point
	HitsInserter<TrackPoint> _tpInserter;

	///< HODs kin ID cache
	DetKin_t _hodKinID;
	
	/// SADC amplitude threshold
	int _threshold;

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
   
    /// Map of SADC hits for hodoscope
    std::map<DetID_t, Hits> _hodoHits;
    
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
    DetID _create_unique_det( DetID did );
    
    void _create_clusters( DetID did
						 , Hits & rawHits
						 , std::vector<hitPair> & conHits );
	void _create_track_points( Event & e
                             , DetID_t did
				             , std::vector<hitPair> & conHits);

public:
    HODHitProcess( calib::Dispatcher & dsp
           , const std::string & only
           , ObjPool<TrackPoint> & bank
           , int threshold );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

#endif  //  defined(GenFit_FOUND)


