#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for assembling SADC hits into CaloEDep.
 *
 * Proccesses information from SADC chips of calorimeters
 *
 * */
class CaloEDepCollect : public AbstractHitHandler<SADCHit> {

public:
	/// map for collection of SADC hits with their unique detector IDs
	typedef std::map<DetID_t, PoolRef<SADCHit>> Hits;

	struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t hodKinCode;
        DetKin_t ecalKinCode;
    };

private:
	
	std::map<DetID_t, Hits> _sadcHits;

protected:

	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;
    
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

	/// CaloEDep allocator reference
    HitsInserter<CaloEDep> _caloInserter;
    
	DetID _create_unique_det( DetID );
	DetID _create_unique_det_ecal( DetID did );
	
public:
    CaloEDepCollect( calib::Dispatcher & dsp
                   , const std::string & only
                   , ObjPool<CaloEDep> & bank );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}
