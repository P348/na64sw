#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for sum of energy deposition in CaloEDep.
 *
 * Proccesses information from SADC chips of calorimeters
 *
 * */
class CaloEDepMaxCell : public AbstractHitHandler<CaloEDep> {
public:
    struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t ecalKinCode;
        DetKin_t hcalKinCode;
    };

protected:

	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;

    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    CaloEDepMaxCell( calib::Dispatcher & dsp
                   , const std::string & only );
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , CaloEDep &) override;
};

}
}
