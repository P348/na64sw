#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for sum of energy deposition in CaloEDep.
 *
 * Proccesses information from SADC chips of calorimeters
 *
 * */
class CaloEDepSum : public AbstractHitHandler<CaloEDep> {

public:
    CaloEDepSum( calib::Dispatcher & dsp
                , const std::string & only );
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , CaloEDep &) override;
};

}
}
