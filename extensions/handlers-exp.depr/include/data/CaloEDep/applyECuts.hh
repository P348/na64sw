#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for sum of energy deposition in CaloEDep.
 *
 * Proccesses information from SADC chips of calorimeters
 *
 * */
class CaloEDepCuts : public AbstractHitHandler<CaloEDep> {

private:
	double _minEnergy, _maxEnergy;

public:
    CaloEDepCuts( calib::Dispatcher & dsp
                , const std::string & only
                , double minEnergy
                , double maxEnergy );
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , CaloEDep &) override;
};

}
}
