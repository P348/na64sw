#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based handler for proccessing SADC hits of calorimeters.
 */
class GenfitTrackPointCalo : public AbstractHitHandler<SADCHit> {
	
public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float resolution;
    };
    
    struct NamingCache {
		DetChip_t sadcChipCode;
        DetKin_t hodKinCode;
        DetKin_t vetoKinCode;
        DetKin_t ecalKinCode;
        DetKin_t srdKinCode;
        DetKin_t ecalsumKinCode;
    };
	
private:

	/// Routine to insert track point
	HitsInserter<TrackPoint> _tpInserter;
	
	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;
	
	int _threshold;

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
    
    std::map<DetID_t, TrackPoint> _hits;
    
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    GenfitTrackPointCalo( calib::Dispatcher & dsp
						, const std::string & only
						, ObjPool<TrackPoint> & bank
						, int threshold );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

#endif  //  defined(GenFit_FOUND)


