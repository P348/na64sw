#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class GenfitTrackPointStraw : public AbstractHitHandler<TrackPoint> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
        double driftDistance;
		double driftDistanceError;
    };
    
	struct NamingCache {
        DetChip_t stwtdcChipCode;
        DetKin_t stwKinCode
               ;
    };
	
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
        
protected:

    NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;
    
    void _rotate_detector_plane( const DetPlacementEntry * cPlacement
							   , TVector3 & o_plane
							   , TVector3 & u_plane
							   , TVector3 & v_plane );
	
	void _create_measurement_new( genfit::TrackPoint & tp
							, const double& driftDistance
							, const double& driftDistanceError
							, const TVector3& endPoint1
							, const TVector3& endPoint2
							, DetID_t did 
							, int hitId);
							
	void _create_measurement( genfit::TrackPoint & tp
		   				    , TVectorD & rawHitCoords
						    , TMatrixDSym & rawHitCov
						    , DetID_t did 
						    , int hitId );

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    

public:
    GenfitTrackPointStraw( calib::Dispatcher & ch
                         , const std::string & only );
                                      
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;

};

}
}

#endif

