#pragma once

#include "na64sw-config.h"

#if defined(GenFit_FOUND) && GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

#include <TVector3.h>
#include <SharedPlanePtr.h>
#include <TMatrixDSym.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class GenfitTrackPointAPV : public AbstractHitHandler<TrackPoint> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
    
    struct NamingCache {
        DetChip_t apvChipCode;
        DetKin_t mmKinCode
               , gmKinCode
               ;
    };
    
private:

    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, DetPlacementEntry *> _placements;
        
protected:

    NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;
    
    void _rotate_detector_plane( const DetPlacementEntry * cPlacement
                               , TVector3 & o_plane
                               , TVector3 & u_plane
                               , TVector3 & v_plane );
                               
    void _create_measurement( genfit::TrackPoint & tp
                            , genfit::SharedPlanePtr & plane
                            , TVectorD & hitCoord
                            , TMatrixDSym & hitCov
                            , DetID did
                            , int & planeId );

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    

public:
    GenfitTrackPointAPV( calib::Dispatcher & ch
                       , const std::string & only );
                                      
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;

};

}
}

#endif
