# An MSDC event viewer extension

This is the runtime extension to NA64sw designed for inspection of MSADC
detectors at the individual events basis. Provides a plotting GUI
depicting individual hits waveform with additional calibration markup and
reconstruction internals.

## Disclaimer

This extension is under development and not yet stable enough to be recommended
for general use.

## Usage

Capable to run as an extension for the pipeline application. To run, load
module `msadc-viewer-window` to the pipeline with `-m` option. In the pipeline
config use the following handler (example restricts ECAL view for few cells):

    - _type: ShowMSADCHits
      applyTo: "kin == ECAL && zIdx == 1 && xIdx != 0 && xIdx != 4 && yIdx != 0 && yIdx != 5"
      destination: default

