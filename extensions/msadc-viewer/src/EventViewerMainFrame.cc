#include "EventViewerMainFrame.hh"

#include <Rtypes.h>
#include <TAttLine.h>
#include <TGButton.h>
#include <TGraph.h>
#include <TH2Poly.h>
#include <TMultiGraph.h>
#include <GuiTypes.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGLabel.h>
#include <TGStatusBar.h>
#include <TGTab.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TMarker.h>
#include <TLine.h>
#include <TBox.h>
#include <TPaveText.h>
#include <TCanvasImp.h>
#include <TVirtualPad.h>
#include <cmath>

#include <gsl/gsl_randist.h>

#include "msadcItem.hh"
#include "na64/moyal/function.h"

static Double_t
gaus_prefit_f(Double_t * x_, Double_t * par_) {
    const double x = *x_
               , mu = par_[0]      // 1 center
               , sigma = par_[1]   // 2 width
               , S = par_[2]       // 3 square
               ;
    return S*gsl_ran_gaussian_pdf(x - mu, sigma);
}

static Double_t
moyal_fit_f(Double_t * x_, Double_t * par_) {
    #if 0  // this is function for ROOT fit
    const Double_t x = *x_
                 , mu     = par_[0]  // 1 center
                 , sigma  = par_[1]  // 2 width
                 , S      = par_[2]  // 3 square
                 , lambda = (x - mu)/sigma
                 , exp1   = exp(-lambda)
                 , exp2   = exp(-(lambda + exp1)/2)
                 ;
    return exp2*S;
    #else
    return na64util_moyal_f(*x_ - 12.5/2., par_);  // TODO: why 1/2 sample difference?
    #endif
}

/// Defines single canvas layout
struct PadsLayout {
    const char name[32];
    const char items[16][16][32];
};

static const PadsLayout _defaultLayout[] = {
    {
        "ECAL-prs",
        {
            {"ECAL0:0-0-0", "ECAL0:1-0-0", "ECAL0:2-0-0", "ECAL0:3-0-0", "ECAL0:4-0-0", ""},
            {"ECAL0:0-1-0", "ECAL0:1-1-0", "ECAL0:2-1-0", "ECAL0:3-1-0", "ECAL0:4-1-0", ""},
            {"ECAL0:0-2-0", "ECAL0:1-2-0", "ECAL0:2-2-0", "ECAL0:3-2-0", "ECAL0:4-2-0", ""},
            {"ECAL0:0-3-0", "ECAL0:1-3-0", "ECAL0:2-3-0", "ECAL0:3-3-0", "ECAL0:4-3-0", ""},
            {"ECAL0:0-4-0", "ECAL0:1-4-0", "ECAL0:2-4-0", "ECAL0:3-4-0", "ECAL0:4-4-0", ""},
            {"ECAL0:0-5-0", "ECAL0:1-5-0", "ECAL0:2-5-0", "ECAL0:3-5-0", "ECAL0:4-5-0", ""},
            {""}
        }
    }, {
        "ECAL-main",
        {
            {"ECAL0:0-0-1", "ECAL0:1-0-1", "ECAL0:2-0-1", "ECAL0:3-0-1", "ECAL0:4-0-1", ""},
            {"ECAL0:0-1-1", "ECAL0:1-1-1", "ECAL0:2-1-1", "ECAL0:3-1-1", "ECAL0:4-1-1", ""},
            {"ECAL0:0-2-1", "ECAL0:1-2-1", "ECAL0:2-2-1", "ECAL0:3-2-1", "ECAL0:4-2-1", ""},
            {"ECAL0:0-3-1", "ECAL0:1-3-1", "ECAL0:2-3-1", "ECAL0:3-3-1", "ECAL0:4-3-1", ""},
            {"ECAL0:0-4-1", "ECAL0:1-4-1", "ECAL0:2-4-1", "ECAL0:3-4-1", "ECAL0:4-4-1", ""},
            {"ECAL0:0-5-1", "ECAL0:1-5-1", "ECAL0:2-5-1", "ECAL0:3-5-1", "ECAL0:4-5-1", ""},
            {""}
        }
    },
    { "" }
};

int gTimeClusterColors[] = {
    kGray + 1,  // "0 cluster" for peaks not in time corr cluster
    kBlue - 3,  kGreen - 3, kCyan - 3, kMagenta - 3,
    kBlue - 2,  kGreen - 2, kCyan - 2, kMagenta - 2,
    kBlue - 1,  kGreen - 1, kCyan - 1, kMagenta - 1,
    0  // terminate
};

struct Markers {
    float x[64], y[64];
    Int_t n;
    TGraph * tg;
};

void EventViewerMainFrame::DoDraw() {
    std::string itemsGroupName = "default";  // TODO get from UI
    auto & items = na64dp::msadcDispl::Destinations::self().get_destination(itemsGroupName);
    assert(!items.empty());  // XXX
    {  // update event ID label
        char bf[64];
        snprintf(bf, sizeof(bf), "Event #%s", items.eventID.c_str());
        _statusBar->SetText(bf, 0);
    }
    for(TabAssets & tabAssets : _tabs) {
        // grid
        //unsigned short nCols = ceil(sqrt(items.size()))
        //             , nRows = ceil(float(items.size())/nCols)
        //             ;
        unsigned short nCols = 0, nRows = 0;
        std::unordered_map<std::string, std::pair<unsigned short, unsigned short>> layout;
        for(auto * cr = tabAssets.padLayoutDesc->items; '\0' != **cr[0]; ++cr, ++nRows) {
            unsigned nCol = 0;
            for(auto * cl = *cr; '\0' != **cl; ++cl, ++nCol) {
                layout.emplace(*cl, std::pair<unsigned short, unsigned short>(nRows, nCol));
            }
            if(nCols < nCol) nCols = nCol;
        }
        std::unordered_map<TPaveText *, Int_t> labels;  // to be (re-)drawn at the end
        // create pads
        TCanvas * cnv = tabAssets.canvas->GetCanvas();
        //cnv->();
        cnv->cd();
        cnv->Clear();
        cnv->Divide(nCols, nRows, 0.00015, 0.00015);
        //size_t nItem = 0;
        std::unordered_multimap<int, TF1*> overimposedFunctions;
        for(auto & itemPair : items) {
            //++nItem;
            auto & item = itemPair.second;
            std::list<decltype(layout)::iterator> matches;
            {  // figure out where to cd()
                // look for the matche(s) of item's name in the layout
                for(auto it = layout.begin(); it != layout.end(); ++it) {
                    // we only check initial n symbols
                    if(0 != strncmp(it->first.c_str(), itemPair.first.c_str(), it->first.size())) {
                        continue;
                    }
                    matches.push_back(it);
                }
                if(matches.empty()) {
                    //std::cerr << "Dev.warning: no layout placement for \""
                    //        << itemPair.first << "\"" << std::endl;  // XXX
                    // ^^^ generates misleading warnings as lookup is done only
                    //     within current tab's layout
                    continue;  // item is not for this pad
                }
            }
            for(auto layoutIt : matches) {
                unsigned short nPad = layoutIt->second.first*nCols + layoutIt->second.second + 1;
                cnv->cd(nPad);
                TMultiGraph * itemMG; {  // create the TMultiGraph to draw the item
                    char bf[64];
                    snprintf(bf, sizeof(bf), "%s combined view", itemPair.first.c_str());
                    itemMG = new TMultiGraph( itemPair.first.c_str(), bf );
                }
                TGraph * tg = nullptr;
                {  // plot the waveform
                    if(!std::isnan(item.samplesSize)) {
                        float tValues[item.nSamples];
                        for(size_t nSample = 0; nSample < item.nSamples; ++nSample) {
                            tValues[nSample] = (nSample + .5)*item.samplesSize;
                        }
                        if(fChkDoSubtractPedestals->IsOn()) {
                            float aValues[item.nSamples];
                            for(size_t nSample = 0; nSample < item.nSamples; ++nSample) {
                                if(std::isnan(item.pedestals[0]) || std::isnan(item.pedestals[1])) continue;
                                aValues[nSample] = item.waveform[nSample]
                                                 - (nSample%2 ? item.pedestals[1] : item.pedestals[0]) ;
                            }
                            tg = new TGraph(item.nSamples, tValues, aValues);
                        } else {
                            tg = new TGraph(item.nSamples, tValues, item.waveform);
                        }
                    } else {
                        assert(0);  // TODO?
                        //tg = new TGraph(item.nSamples, item.waveform);
                    }
                    if(tg) {
                        char bf[64];
                        snprintf(bf, sizeof(bf), "waveform-%s", itemPair.first.c_str());
                        tg->SetName(bf);
                        tg->SetTitle(itemPair.first.c_str());
                        tg->SetLineColor(kGray);
                        tg->SetLineWidth(2);
                        tg->SetMarkerColor(kBlack);
                        tg->SetMarkerSize(1);
                        tg->SetMarkerStyle(33);
                        itemMG->Add(tg, "alp");
                        //tg->Draw("ALP*");
                    }
                }  // plot waveforms
                // plot discovered maxima, if any
                //std::cout << "(Y)"
                //    << itemPair.first << " - "
                //    << std::isnan(item.maxima[0][0]) << ", " << std::isnan(item.maxima[0][1])
                //    << std::endl;  // XXX
                if(!(std::isnan(item.maxima[0][0]) && std::isnan(item.maxima[0][1]))) {
                    _draw_maxima(itemMG, tg, itemPair, nPad, overimposedFunctions, labels);
                }  // draw maxima
                // plot favored maximum, if found
                if(!(std::isnan(item.maxAmp) || std::isnan(item.maxTime))) {
                    float x = item.maxTime
                        , y = item.maxAmp;
                    //std::cout << " xxx " << itemPair.first << "'s best time = " << x << std::endl;  // XXX
                    // Mark time and amplitude for winning hit
                    TGraph * tg = new TGraph(1, &x, &y);
                    tg->SetMarkerStyle(2);  // +-cross
                    tg->SetMarkerSize(3);
                    tg->SetMarkerColor(kBlue);
                    itemMG->Add(tg, "p");
                }  // if maxTime

                TVirtualPad * cPad = cnv->cd(nPad);
                itemMG->Draw();
                cPad->Modified();

                // draw timing markers (mean, deviations)
                #if 0
                std::cout << itemPair.first << ": "
                          << wfMin << " / " << itemMG->GetYaxis()->GetXmin()
                          << " / " << gPad->GetUymin()
                          << " // "
                          << wfMax << " / " << itemMG->GetYaxis()->GetXmax()
                          << " / " << gPad->GetUymax()
                          << std::endl
                          ;
                #else
                assert(cPad == gPad);
                itemMG->GetYaxis()->GetXmin();
                itemMG->GetYaxis()->GetXmax();
                Float_t yAxMin = cPad->GetUymin()
                      , yAxMax = cPad->GetUymax()
                      , xAxMin = cPad->GetUxmin()
                      , xAxMax = cPad->GetUxmax()
                      ;
                //std::cout << itemPair.first << ": "
                //          << yAxMin
                //          << " // "
                //          << yAxMax
                //          << std::endl
                //          ;
                #endif
                if( !std::isnan(item.meanTime) ) {
                    TLine * l = new TLine( item.meanTime
                            , yAxMin //itemMG->GetYaxis()->GetXmin() //wfMin //gPad->GetUymin()
                            , item.meanTime
                            , yAxMax //itemMG->GetYaxis()->GetXmax() //wfMax //gPad->GetUxmax()
                            );
                    //Set line attributes 
                    l->SetLineColor(kRed);
                    l->SetLineWidth(1);
                    l->SetLineStyle(kDotted);
                    //draw the line
                    l->Draw("same");
                }  // mean time line
                if( !std::isnan(item.timeStdDev) ) {
                    TBox * one = new TBox( item.meanTime - 3*item.timeStdDev
                                         , gPad->GetUymin()
                                         , item.meanTime + 3*item.timeStdDev
                                         , gPad->GetUymax()
                            );
                    //Set line attributes 
                    one->SetLineColor(kRed);
                    one->SetLineWidth(1);
                    one->SetFillColorAlpha(0, 0);
                    one->SetFillStyle(0);
                    one->SetLineStyle(kDashed);
                    //draw the line
                    one->Draw("same");
                }  // mean time line
                if( !std::isnan(item.minAmpThreshold) ) {
                    TLine * l = new TLine( xAxMin
                            , item.minAmpThreshold //itemMG->GetYaxis()->GetXmin() //wfMin //gPad->GetUymin()
                            , xAxMax
                            , item.minAmpThreshold //itemMG->GetYaxis()->GetXmax() //wfMax //gPad->GetUxmax()
                            );
                    //Set line attributes 
                    l->SetLineColor(kRed);
                    l->SetLineWidth(1);
                    l->SetLineStyle(kDotted);
                    //draw the line
                    l->Draw("same");
                }  // mean time line

                TPaveText * pTxt = new TPaveText(0.65, 0.99, 0.99, 0.75, "NBNDC");
                // ^ opts meaning: NB - no border, NDC - pad's local frame coords
                pTxt->SetFillStyle(3001);
                pTxt->SetTextFont(102);
                pTxt->AddText(itemPair.first.c_str());  // XXX?
                char strbf[64];
                {
                    snprintf(strbf, sizeof(strbf), "zProb:%.2e", item.canBeZero);
                    pTxt->AddText(strbf);  // TODO: can be zero, overall info, etc
                } {
                    snprintf(strbf, sizeof(strbf), "abs.err:%.2e", item.absError);
                    pTxt->AddText(strbf);  // TODO: can be zero, overall info, etc
                }
                //pTxt->SetLabel("some");  // xxx?
                pTxt->Draw();

            }  // loop over pad matches
        }  // loop by item pairs
        for(auto txt: labels) {
            cnv->cd(txt.second);
            txt.first->Draw();
        }
        for(auto & p : overimposedFunctions) {
            cnv->cd(p.first);
            p.second->Draw("SAME");
        }
        cnv->Update();
    }  // loop over tabs
}

EventViewerMainFrame::~EventViewerMainFrame() {
    // Clean up used widgets: frames, buttons, layout hints
    fMain->Cleanup();
    delete fMain;
}

void
EventViewerMainFrame::_draw_maxima(
          TMultiGraph * itemMG
        , TGraph * itemTGraph
        , const std::pair<std::string, na64dp::msadcDispl::MSADCDisplayItem> & itemPair
        , unsigned short nPad
        , std::unordered_multimap<int, TF1*> & overimposedFunctions
        , std::unordered_map<TPaveText *, Int_t> & labels
        ) {
    const auto & item = itemPair.second;

    std::map<na64dp::msadcDispl::PeakMarkerStyle, Markers> peaks;
    //float x[64], y[64];
    Int_t nMax;
    for(nMax = 0; nMax < (Int_t) item.nSamples; ++nMax) {
        if(std::isnan(item.maxima[nMax][0]) && std::isnan(item.maxima[nMax][1]))
            break;
        //std::cout << " maximum #" << nMax << " on " << itemPair.first << ": "
        //          << item.maxima[nMax][0] << ", " << item.maxima[nMax][1] << std::endl;
        //x[nMax] = item.maxima[nMax][0];
        //y[nMax] = item.maxima[nMax][1];
        //tg->SetPoint(nMax, item.maxima[nMax][0], item.maxima[nMax][1]);
        {
            auto ir = peaks.emplace(item.markerStyles[nMax], Markers{{0}, {0}, 0, nullptr});
            Markers & m = ir.first->second;
            m.x[m.n] = item.maxima[nMax][0];
            m.y[m.n] = item.maxima[nMax][1];
            ++m.n;
        }

        // XXX, tmp:
        if(!std::isnan(item.gaussianMu[nMax])) {
            char bf[64];
            snprintf(bf, sizeof(bf), "%s-gaus-%d", itemPair.first.c_str(), nMax);
            TF1 * f = new TF1(bf, gaus_prefit_f
                    , item.gaussianRange[nMax][0]
                    , item.gaussianRange[nMax][1]
                    , 3 );
            f->SetParameters( item.gaussianMu[nMax]
                    , item.gaussianSigma[nMax]
                    , item.gaussianS[nMax]
                    );
            f->SetLineColor(kGreen+2);
            f->SetLineWidth(3);
            f->SetLineStyle(kDotted);
            //std::cout << "Max (Gaussian) #" << nMax << " of "
            //        << itemPair.first << ": mu=" << item.gaussianMu[nMax]
            //        << ", sigma=" << item.gaussianSigma[nMax]
            //        << ", S=" << item.gaussianS[nMax]
            //        << std::endl;
            overimposedFunctions.emplace(nPad, f);
            //f->Draw("SAME");
            //itemMG->Add(f);
            //std::cout << "xxx " << item.maxima[nMax][0] - 3*12.5
            //    << ", " << item.maxima[nMax][0] + 3*12.5 << std::endl;  // XXX
        }
        // smol pave text to describe peak
        TPaveText * maxPTxt = nullptr;
        {
            TAxis * xAxis = itemTGraph->GetXaxis()
                , * yAxis = itemTGraph->GetYaxis()
                ;
            float mpX = (item.maxima[nMax][0] - xAxis->GetXmin())/(xAxis->GetXmax() - itemTGraph->GetXaxis()->GetXmin())
                , mpY = (item.maxima[nMax][1] - yAxis->GetXmin())/(yAxis->GetXmax() - itemTGraph->GetYaxis()->GetXmin())
                ;
            
            double gcs[4] = {
                    mpX - .07, mpY - .45,
                    mpX + .07, mpY - .15
                };
            maxPTxt = new TPaveText(gcs[0], gcs[1], gcs[2], gcs[3], "NBNDC");
            maxPTxt->SetFillStyle(3001);
            maxPTxt->SetTextFont(102);
            char bf[64];
            snprintf(bf, sizeof(bf), "%.2f", item.sqRatio[nMax]);
            maxPTxt->AddText(bf);  
            snprintf(bf, sizeof(bf), "%.2f", item.maxima[nMax][0]);  // TODO: time
            maxPTxt->AddText(bf);
            // ... other info on this peak
            //maxPTxt->Draw();
            labels.emplace(maxPTxt, nPad);
        }
        // if Moyal fit available
        if(!std::isnan(item.moyalMu[nMax])) {
            char bf[64];
            snprintf(bf, sizeof(bf), "%s-moyal-%d", itemPair.first.c_str(), nMax);
            TF1 * f = new TF1(bf, moyal_fit_f
                    , item.moyalRange[nMax][0]
                    , item.moyalRange[nMax][1]
                    , 3 );
            //std::cout << "xxx " << itemPair.first << " // "  // XXX
            //        << item.moyalRange[nMax][0] << ", "      // XXX
            //        << item.moyalRange[nMax][1]              // XXX
            //        << std::endl;                            // XXX
            f->SetLineWidth(1);
            auto peakColor = kRed + 2;  // kBlue-4;
            if( itemPair.second.timeClusterLabel[nMax] >= 0
             && itemPair.second.timeClusterLabel[nMax] < (Int_t) (sizeof(gTimeClusterColors)/sizeof(*gTimeClusterColors)) - 1 ) {
                peakColor = gTimeClusterColors[itemPair.second.timeClusterLabel[nMax]];
            }
            //if(std::isfinite(item.canBeZero)) {
            //    if(item.canBeZero > .95) {
            //        peakColor = kGray + 1;
            //    } // ...
            //}
            f->SetFillColor(peakColor);
            f->SetLineColor(peakColor);
            // sequence of the filling stroke
            // stencils: 3315, 3351, 3325, 3352, ... 3385, 3358
            // -> 1, 2, 3, ... 8
            // NOTE: in 3x?? `x' defines sparseness of strokes
            int stencilNo = 3700 + ((nMax%2) ? 1 : 10)*((nMax%8) + 1) + ((nMax%2) ? 10 : 1)*5;
            f->SetFillStyle(stencilNo);
            f->SetParameters( item.moyalMu[nMax]
                    , item.moyalSigma[nMax]
                    , item.moyalS[nMax]
                    );
            //std::cout << "Max (Moyal) #" << nMax << " of "
            //        << itemPair.first << ": mu=" << item.gaussianMu[nMax]
            //        << ", sigma=" << item.gaussianSigma[nMax]
            //        << ", S=" << item.gaussianS[nMax]
            //        << std::endl;
            overimposedFunctions.emplace(nPad, f);
            //f->Draw("SAME");
            //itemMG->Add(f);
            //std::cout << "xxx " << item.maxima[nMax][0] - 3*12.5
            //    << ", " << item.maxima[nMax][0] + 3*12.5 << std::endl;  // XXX
        }  // if Moyal mu
    }  // nMax
    assert(nMax > 0);
    for(auto & p : peaks) {
        // Mark time and amplitude for found peak
        p.second.tg = new TGraph(p.second.n, p.second.x, p.second.y);
        // use different markers depending on style
        if(p.first == na64dp::msadcDispl::kPeakMarker_notFitted) {
            // not fitted peak -- green hollow cross
            p.second.tg->SetMarkerStyle(28);
            p.second.tg->SetMarkerColor(kGreen+2);
            p.second.tg->SetMarkerSize(2);
        } else if(p.first == na64dp::msadcDispl::kPeakMarker_standard) {
            // standard (converged) fit -- blue hollow rhombus
            p.second.tg->SetMarkerStyle(27);
            p.second.tg->SetMarkerColor(kBlue+2);
            p.second.tg->SetMarkerSize(2);
        } else if(p.first == na64dp::msadcDispl::kPeakMarker_troublesome) {
            // troublesome/dubious fit -- red rhombus
            p.second.tg->SetMarkerStyle(27);
            p.second.tg->SetMarkerColor(kRed);
            p.second.tg->SetMarkerSize(2);
        } else if(p.first == na64dp::msadcDispl::kPeakMarker_erroneousFit) {
            // erroineous/failed fit -- red x-cross
            p.second.tg->SetMarkerStyle(5);
            p.second.tg->SetMarkerColor(kRed+2);
            p.second.tg->SetMarkerSize(2);
        } else {  // unknown style (?) -- red hollow cross
            p.second.tg->SetMarkerStyle(28);
            p.second.tg->SetMarkerColor(kRed+2);
            p.second.tg->SetMarkerSize(2);
        }
        itemMG->Add(p.second.tg, "p");
    }
}

//  +--------+
//  | settin |
//  +--+-----+
//  |b | cnv |
//  +--+-----+
//  | status |
//  +--------+

EventViewerMainFrame::EventViewerMainFrame(
          const TGWindow *p
        , UInt_t w
        , UInt_t h
        ) {
    // Create a main frame
    fMain = new TGMainFrame(p, w, h, kVerticalFrame);

    {  // top frame w settings
        TGHorizontalFrame * fDrawOpts = new TGHorizontalFrame(fMain, w, h/5);
        // checkbox steering pedestal subtraction at view
        fChkDoSubtractPedestals = new TGCheckButton(fDrawOpts, "subtract pedestals");
        fChkDoSubtractPedestals->Connect("Toggled(Bool_t)", "EventViewerMainFrame", this, "DoDraw()");
        fDrawOpts->AddFrame(fChkDoSubtractPedestals, new TGLayoutHints(kLHintsLeft, 15, 5, 5, 5));

        fMain->AddFrame(fDrawOpts, new TGLayoutHints(kLHintsExpandX | kLHintsTop, 5, 5, 5, 15));
    }

    {  // create horizontal frame containing buttons and pads
        TGHorizontalFrame * fBtnsAndCnv
            = new TGHorizontalFrame(fMain, w, 4*h/5);

        // Create a vertical frame widget with buttons
        TGVerticalFrame *vframe = new TGVerticalFrame(fBtnsAndCnv,w/3,h); {
            //
            TGTextButton *draw = new TGTextButton(vframe,"&Redraw");
            draw->Connect("Clicked()","EventViewerMainFrame",this,"DoDraw()");
            vframe->AddFrame(draw, new TGLayoutHints( kLHintsLeft
                                                    , 5,5,3,4
                                                    ));

            TGTextButton *next = new TGTextButton(vframe,"&Next", "gApplication->Terminate()");
            vframe->AddFrame(next, new TGLayoutHints( kLHintsLeft
                                                    , 5,5,3,4
                                                    ));

            // TODO: exit button does not work so far
            TGTextButton *exit = new TGTextButton(vframe,"&Exit",
                                                    "gApplication->Terminate(0)");
            vframe->AddFrame(exit, new TGLayoutHints( kLHintsLeft
                                                                  , 5,5,15,4
                                                                  ));
        }
        fBtnsAndCnv->AddFrame( vframe, new TGLayoutHints( kLHintsLeft | kLHintsExpandY
                                                                , 5, 5, 5, 5
                                                                ));

        // Create tabs
        _tabsPtr = new TGTab(fBtnsAndCnv, 2*w/3, h);
        for( auto * cTabLayoutDesc = _defaultLayout
           ; '\0' != cTabLayoutDesc->name[0]
           ; ++cTabLayoutDesc) {
            TabAssets cTabAssets = {NULL, NULL, cTabLayoutDesc};
            // Single tab
            cTabAssets.frameInTab = _tabsPtr->AddTab(cTabLayoutDesc->name);
            // Create canvas widget
            cTabAssets.canvas = new TRootEmbeddedCanvas("Ecanvas", cTabAssets.frameInTab, 2*w/3., 3.9*h/5);
            
            //fEcanvas->GetCanvas()->GetCanvasImp()->ShowStatusBar(kTRUE);  // doesn't work
            cTabAssets.canvas->GetCanvas()->Connect( "ProcessedEvent(Int_t,Int_t,Int_t,TObject*)"
                                          , "EventViewerMainFrame"
                                          , this
                                          , "ExecEvent(Int_t,Int_t,Int_t,TObject*)"
                                          );
            cTabAssets.frameInTab->AddFrame(cTabAssets.canvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
            _tabs.push_back(cTabAssets);
        }
        fMain->AddFrame(fBtnsAndCnv
                , new TGLayoutHints( kLHintsRight | kLHintsExpandX | kLHintsExpandY
                                              , 5, 5, 5, 5
                                              )
                );
        fBtnsAndCnv->AddFrame( _tabsPtr
                           , new TGLayoutHints( kLHintsRight | kLHintsExpandX | kLHintsExpandY
                                              , 5, 5, 5, 5
                                              )
                           );
    }  // buttons + canvas
    {  // status bar
        //TGHorizontalFrame * fStatus = new TGHorizontalFrame(fMain, w, h/5);
        // label showing general event information (id, whatever...)
        //lblPointerCoords = new TGLabel(fStatus, "....................................");  // TODO
        //fStatus->AddFrame(lblPointerCoords, new TGLayoutHints(kLHintsLeft, 5, 5, 1, 1));
        //fMain->AddFrame(fStatus, new TGLayoutHints(kLHintsExpandX | kLHintsTop, 5, 5, 2, 5));

        // Create status bar
        int parts[] = { 33, 10, 10, 47 };
        _statusBar = new TGStatusBar(fMain, 10, 10);
        _statusBar->SetParts(parts, 4);
        auto fStatusBarLayout = new TGLayoutHints(kLHintsBottom | kLHintsLeft | kLHintsExpandX, 2, 2, 1, 1);
        fMain->AddFrame(_statusBar, fStatusBarLayout);
    }
    // Set a name to the main frame
    fMain->SetWindowName("MSADC Event Viewer");
    // Map all subwindows of main frame
    fMain->MapSubwindows();
    // Initialize the layout algorithm
    fMain->Resize(fMain->GetDefaultSize());
    // Map main frame
    fMain->MapWindow();
}

void
EventViewerMainFrame::ExecEvent( Int_t event
                               , Int_t x
                               , Int_t y
                               , TObject* /*unused?*/) {
    #if 1
    // Note: this is useful snippet derived from following sources:
    //  - initial thread with some sort of wrong answer:
    //      https://roottalk.root.cern.narkive.com/BTWXe7Jq/root-creating-a-status-bar
    //    The problem is "middle mosue button" which is inconvinient and
    //    tedious.
    //  - alternatively, native TCanvas implementation (TCanvas.cxx:1357) has
    //    a nice example of retrieving (and switching) a TPad being currently
    //    selected:
    //      https://root.cern.ch/doc/master/TCanvas_8cxx_source.html
    //TPad * pad = fEcanvas->GetCanvas()->Pick(x, y, gROOT->GetSelectedPad());  // TODO: uncomment
    //pad->cd();
    {   // loop over all canvases to make sure that only one pad is highlighted
        TIter next(gROOT->GetListOfCanvases());
        TCanvas *tc;
        while ((tc = (TCanvas *)next()))
            tc->Update();
    }
    _statusBar->SetText(gROOT->GetSelectedPad()->GetObjectInfo(x,y), 3);
    #endif
}

