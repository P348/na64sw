#include "msadcItem.hh"
#include "na64event/data/sadc.hh"
#include "na64util/str-fmt.hh"

#include <cmath>
#include <cstring>
#include <limits>

namespace na64dp {
namespace msadcDispl {

MSADCDisplayItem::MSADCDisplayItem(size_t nSamples_)
    : nSamples(nSamples_)
    , samplesSize(std::nan("0"))
    , waveform(nullptr)
    , maxima(nullptr)
    , markerStyles(nullptr)
    // ...
    {
    pedestals[0] = pedestals[1] = std::nan("0");
    waveform = (decltype(waveform)) malloc(sizeof(*waveform)*nSamples);
    maxima = (decltype(maxima)) malloc(sizeof(*maxima)*(nSamples+1)*2);
    markerStyles = (decltype(markerStyles)) malloc(sizeof(*markerStyles)*(nSamples+1)*2);
}

MSADCDisplayItem::MSADCDisplayItem(const MSADCDisplayItem & orig)
        : MSADCDisplayItem(orig.nSamples)
        {
    samplesSize = orig.samplesSize;
    assert(waveform);
    assert(maxima);
    memcpy(waveform,     orig.waveform,     sizeof(*waveform)*nSamples);
    memcpy(maxima,       orig.maxima,       sizeof(*maxima)*nSamples);
    memcpy(markerStyles, orig.markerStyles, sizeof(*markerStyles)*nSamples);
    pedestals[0] = orig.pedestals[0];
    pedestals[1] = orig.pedestals[1];
    maxAmp = orig.maxAmp;
    maxTime = orig.maxTime;
    // ... TODO
    canBeZero = orig.canBeZero;
    absError = orig.absError;

    memcpy(gaussianMu,    orig.gaussianMu,      sizeof(gaussianMu));
    memcpy(gaussianSigma, orig.gaussianSigma,   sizeof(gaussianSigma));
    memcpy(gaussianS,     orig.gaussianS,       sizeof(gaussianS));
    memcpy(gaussianRange, orig.gaussianRange,   sizeof(gaussianRange));

    memcpy(moyalMu,    orig.moyalMu,      sizeof(moyalMu));
    memcpy(moyalSigma, orig.moyalSigma,   sizeof(moyalSigma));
    memcpy(moyalS,     orig.moyalS,       sizeof(moyalS));
    memcpy(moyalRange, orig.moyalRange,   sizeof(moyalRange));

    memcpy(sqRatio,   orig.sqRatio, sizeof(sqRatio));

    memcpy(timeClusterLabel, orig.timeClusterLabel, sizeof(timeClusterLabel));
    // ...
}

MSADCDisplayItem::~MSADCDisplayItem() {
    if(waveform)        free(waveform);
    if(maxima)          free(maxima);
    if(markerStyles)    free(markerStyles);
}

MSADCDisplayItem::MSADCDisplayItem(event::SADCHit & hit)
        : MSADCDisplayItem(sizeof(hit.rawData->wave)/sizeof(*hit.rawData->wave))
    {
    samplesSize = 12.5;  // set standard time interval for 2015-2023, TODO: configurable
    // copy raw waveform
    for(size_t i = 0; i < nSamples; ++i) {
        waveform[i] = hit.rawData->wave[i];
    }
    size_t nPeak = 0;
    for(auto & peakInfoPair : hit.rawData->maxima) {
        maxima[nPeak][0] = peakInfoPair.second->time;
        maxima[nPeak][1] = peakInfoPair.second->amp;
        sqRatio[nPeak]    = peakInfoPair.second->sumCoarse/peakInfoPair.second->sumFine;
        timeClusterLabel[nPeak] = peakInfoPair.second->timeClusterLabel;
        // XXX, deprecated:
        //if(peakInfoPair.second->gaussianFit) {
        //    gaussianMu[nPeak]    = peakInfoPair.second->gaussianFit->mu;
        //    gaussianSigma[nPeak] = peakInfoPair.second->gaussianFit->sigma;
        //    gaussianS[nPeak]     = peakInfoPair.second->gaussianFit->S;
        //    gaussianRange[nPeak][0] = peakInfoPair.second->gaussianFit->interval[0];
        //    gaussianRange[nPeak][1] = peakInfoPair.second->gaussianFit->interval[1];
        //} else {
            gaussianMu[nPeak] = gaussianSigma[nPeak] = gaussianS[nPeak]
                = gaussianRange[nPeak][0] = gaussianRange[nPeak][1]
                = std::numeric_limits<float>::quiet_NaN();
        //}
        if(peakInfoPair.second->moyalFit) {
            //assert(!std::isnan(moyalMu[nPeak]));
            moyalMu[nPeak]    = peakInfoPair.second->moyalFit->mu;
            moyalSigma[nPeak] = peakInfoPair.second->moyalFit->sigma;
            moyalS[nPeak]     = peakInfoPair.second->moyalFit->S;
            moyalRange[nPeak][0] = peakInfoPair.second->moyalFit->interval[0];
            moyalRange[nPeak][1] = peakInfoPair.second->moyalFit->interval[1];
            //std::cout << "XXX peak id" << peakInfoPair.first << " range = {"  // XXX
            //                << moyalRange[nPeak][0] << ", "      // XXX
            //                << moyalRange[nPeak][1]              // XXX
            //                << "}, time = "
            //                << maxima[nPeak][0] << ", amp = " << maxima[nPeak][1]  // XXX
            //                << std::endl;                            // XXX
        } else {
            moyalMu[nPeak] = moyalSigma[nPeak] = moyalS[nPeak] 
                = moyalRange[nPeak][0] = moyalRange[nPeak][1]
                = std::numeric_limits<float>::quiet_NaN();
        }

        // marker style
        if(0 == peakInfoPair.second->lastFitStatusCode) {
            if(peakInfoPair.second->moyalFit) {
                // 0 fit status code and Moyal fit results exists -- ok
                markerStyles[nPeak] = kPeakMarker_standard;
                //std::cout << "" << peakInfoPair.second->
            } else {
                // 0 fit status code (default) and no Moyal fit results -- fit
                // was not performed
                markerStyles[nPeak] = kPeakMarker_notFitted;
            }
        } else if(peakInfoPair.second->lastFitStatusCode > 0) {
            // troublesome fit
            markerStyles[nPeak] = kPeakMarker_troublesome;
        } else {
            assert(peakInfoPair.second->lastFitStatusCode < 0);
            // problematic fit or no fit at all
            markerStyles[nPeak] = kPeakMarker_erroneousFit;
        }

        ++nPeak;
        if(nPeak == NA64SW_MSADC_BROWSER_NMAX_PER_HIT) {
            // TODO: print warning
            break;
        }
    }
    maxima[nPeak][0] = maxima[nPeak][1] = std::nan("0");

    // copy favored maximum
    maxAmp  = hit.rawData->maxAmp;
    maxTime = hit.time;
    // copy pedestals
    pedestals[0] = hit.rawData->pedestals[0];
    pedestals[1] = hit.rawData->pedestals[1];

    // ... TODO: forgot meanTime etc
    //meanTime = hit.rawData-> ... NOTE: set in ShowMSADCHits::process_hit()
    //timeStdDev = hit.rawData-> ...  NOTE: set in ShowMSADCHits::process_hit()
    canBeZero = hit.rawData->canBeZero;
    absError = hit.rawData->maxAmpError;
}

//
// Destinations index

Destinations * Destinations::_self = nullptr;

Destinations::Destinations() {
}

Destinations &
Destinations::self() {
    if(!_self) _self = new Destinations;
    return *_self;
}

Items &
Destinations::get_destination(const std::string & nm) {
    auto ir = emplace(nm, Items());
    return ir.first->second;
}

}  // namespace ::na64dp::msadcDispl
}  // namespace na64dp

