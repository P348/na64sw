#include "na64detID/detectorID.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "msadcItem.hh"
#include "na64util/na64/event-id.hh"
#include "msadcViewerExtension.hh"
#include "na64common/calib/sdc-integration.hh"

#include <cmath>
#include <log4cpp/Priority.hh>
#include <string>

/*\file
 *\brief Handler implementation, collects waveforms and dispatches to viewer
 */

namespace na64dp {
namespace handlers {

/**\brief Collects (M)SADC to be shown in the GUI-based viewer window.
 *
 * ...
 *
 * \code{.yaml}
 *     - _type: MyHtiHandler
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * */
class ShowMSADCHits
        : public AbstractHitHandler<event::SADCHit>
        , public calib::Handle< calib::Collection<calib::SADCCalib> >
        // ...
        {
protected:
    msadcDispl::Items & _items;
    std::unordered_map<DetID, calib::SADCCalib> _sadcCalibs;
public:
    /// ... features of the ctr, if any
    ShowMSADCHits( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::string & destination
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , calib::Handle< calib::Collection<calib::SADCCalib> >("default", cdsp)
        , _items(msadcDispl::Destinations::self().get_destination(destination))
        {}
    void handle_update(const calib::Collection<calib::SADCCalib> &) override;
    /// Creates new `MSADCDisplayItem` and puts it in a destination map to be
    /// shown
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
    /// Clears destination map before processing hits
    ProcRes process_event(event::Event &) override;
};  // class ShowMSADCHits

void
ShowMSADCHits::handle_update(const calib::Collection<calib::SADCCalib> & lst) {
    calib::Handle< calib::Collection<calib::SADCCalib> >::handle_update(lst);
    // concatenate calibration data
    for(auto c : lst) {
        //_usedSources.emplace(c.srcDocID);
        // convert name to detector ID
        DetID did = nameutils::sdc_id( naming(), c.data.name
                                     , c.data.x, c.data.y, c.data.z
                                     , c.srcDocID, c.lineNo //< optional
                                     );
        //std::cout << "updating calib for " << naming()[did]
        //    << ", time=" << c.data.time
        //    << "; from " << c.srcDocID << ":" << c.lineNo
        //    << std::endl;  // XXX
        // copy the calibration data to the cache
        auto ir = _sadcCalibs.emplace(did, c);
        if(!ir.second) {
            // merge
            auto & dest = ir.first->second.data
               , & src  = c.data;
            assert(dest.name == c.data.name);
            if(std::isfinite(src.peakpos))   dest.peakpos    = src.peakpos;
            if(std::isfinite(src.time))      dest.time       = src.time;
            if(std::isfinite(src.timesigma)) dest.timesigma  = src.timesigma;
            if(std::isfinite(src.factor))    dest.factor     = src.factor;
            if(std::isfinite(src.refLED))    dest.refLED     = src.refLED;
        }
    }
}

bool
ShowMSADCHits::process_hit( EventID eid
                          , DetID did
                          , event::SADCHit & hit) {
    if(!hit.rawData) return true;  // omit hits without raw data
    std::string detName = naming()[did];

    auto ir = _items.emplace(detName, msadcDispl::MSADCDisplayItem(hit));
    assert(ir.second);
    {
        auto it = _sadcCalibs.find(did);
        if(_sadcCalibs.end() != it) {
            auto & item  = ir.first->second;
            auto & calib = it->second.data;
            //item.meanTime   = calib.time;
            //if(calib.is_relative) {
            //    item.meanTime += _current_event().masterTime;
            //}
            item.meanTime = _current_event().masterTime;  // XXX
            item.timeStdDev = calib.timesigma;
            item.minAmpThreshold = hit.rawData->ampThreshold;
            //std::cout << " xxx, time=" << item.meanTime
            //          << ", timesigma=" << item.timeStdDev
            //          << ", amp.th.=" << item.minAmpThreshold
            //          << std::endl;  // XXX
        }
    }
    return true;
}

AbstractHandler::ProcRes
ShowMSADCHits::process_event(event::Event & event) {
    _items.clear();

    char buf[64];
    na64sw_eid2str(event.id, buf, sizeof(buf));
    _items.eventID = buf;
    // ... other event-relevant info to annotate the display

    ProcRes pr = AbstractHitHandler<event::SADCHit>::process_event(event);
    
    // TODO: consider pr and items.empty()?
    this->log() << log4cpp::Priority::DEBUG
        << "Forwarding execution to (M)SADC viewer's show() method.";
    MSADCEventViewer::self().show();
    this->log() << log4cpp::Priority::INFO
        << "Got back to handler.";

    return pr;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( ShowMSADCHits, cdsp, cfg
                , "Collects (M)SADC to be shown in the GUI-based viewer window."
                ) {
    using namespace na64dp;
    return new handlers::ShowMSADCHits( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["destination"] ? cfg["destination"].as<std::string>() : "default"
            // ... your additional arguments retrieved from `cfg'
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

