#include "msadcViewerExtension.hh"
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <stdexcept>

namespace na64dp {

MSADCEventViewer * MSADCEventViewer::_self = nullptr;

MSADCEventViewer &
MSADCEventViewer::self() {
    if(!_self) throw std::runtime_error("MSADC viewer is not initialized.");
    return *_self;
}

MSADCEventViewer::MSADCEventViewer()
    : _enableWindow(true)
    , _mainFrame(nullptr)
    , _logCatName("msadc-viewer")
    {
    assert(!_self);  // guaranteed by API
    _self = this;
}

void
MSADCEventViewer::init( calib::Manager & cmgr, iEvProcInfo *) {
    if(_enableWindow) {  // turn on event display
        _TApp = new TApplication("msadcViewer", 0, 0);
        // disable ROOT sighandlers right after app instantiated
        //for( int sig = 0; sig < kMAXSIGNALS; sig++) {
        //    gSystem->ResetSignal((ESignals)sig);
        //}
    }
}

void
MSADCEventViewer::set_parameter( const std::string & pn
                      , const std::string & pv
                      ) {
    if(pn == "enableWindow") {
        _enableWindow = util::str_to_bool(pv);
    } else {
        NA64DP_RUNTIME_ERROR("Unsupported parameter (option) name for"
                " extension `GenFit2': \"%s\"", pn.c_str() );
    }
}

void
MSADCEventViewer::finalize() {
    if(_enableWindow) {
        log4cpp::Category::getInstance(_logCatName) << log4cpp::Priority::INFO
            << "Exit TApplication event processing...";
        //EventDisplay::getInstance()->open();
    }
}

void
MSADCEventViewer::show() {
    log4cpp::Category::getInstance(_logCatName)
        << log4cpp::Priority::DEBUG << "Viewer show() method called.";
    if(!_enableWindow) {
        log4cpp::Category::getInstance(_logCatName)
        << log4cpp::Priority::DEBUG << "Viewer has no active window, ignoring.";
        return;
    }
    if(!_mainFrame) {
        _mainFrame = new EventViewerMainFrame(gClient->GetRoot(), 400, 200);
    }
    if(_mainFrame) {
        _mainFrame->DoDraw();
    }

    log4cpp::Category::getInstance(_logCatName)
        << log4cpp::Priority::DEBUG << "Forwarding execution to TApplication.";
    _TApp->SetReturnFromRun(kTRUE);  // possibly duplicating w below:
    _TApp->Run(kTRUE);
    log4cpp::Category::getInstance(_logCatName)
        << log4cpp::Priority::DEBUG << "TApplication returned from run()";
}

REGISTER_EXTENSION( MSADCEventViewer );

}  // namespace na4dp

