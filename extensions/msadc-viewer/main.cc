#include "EventViewerMainFrame.hh"

#include <TCanvas.h>
#include <TF1.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TApplication.h>

int
main(int argc, char * argv[]) {
    TApplication * rootApp = new TApplication("app", 0, 0);

    // disable silly ROOT sighandlers
    for( int sig = 0; sig < kMAXSIGNALS; sig++) {
        gSystem->ResetSignal((ESignals)sig);
    }

    // Popup the GUI...
    new EventViewerMainFrame(gClient->GetRoot(),400,200);
    rootApp->Run();
}

