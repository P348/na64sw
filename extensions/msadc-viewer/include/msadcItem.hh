#pragma once

#include <cstdlib>
#include <cstring>
#include <string>
#include <unordered_map>
#include <map>

#ifndef NA64SW_MSADC_BROWSER_NMAX_PER_HIT
#   define NA64SW_MSADC_BROWSER_NMAX_PER_HIT 16
#endif

namespace na64dp {
namespace event {
struct SADCHit;  // fwd
}  // namespace ::na64dp::event
namespace msadcDispl {

enum PeakMarkerStyle {
    kPeakMarker_notFitted,
    kPeakMarker_standard,
    kPeakMarker_troublesome,
    kPeakMarker_erroneousFit
};

/// Corresponds to single MSADC entity to be shown in graphics
struct MSADCDisplayItem {
    /// Number of samples in the waveform
    const size_t nSamples;
    /// Time interval between samples, ns; can be NaN (in this case samples #
    /// must be used)
    float samplesSize;
    /// Waveform depiction, mem must be managed with malloc()/delete, owned by
    /// item struct, number of elements matches `nSamples`
    float * waveform;
    /// Array of maximum samples, must be depicted as markers
    /// managed by item instance, with malloc()/delete, number of allocated
    /// elements matches `nSamples`, used terminated with NaN
    float (* maxima)[2];
    PeakMarkerStyle * markerStyles;
    /// Pedestal values (even/odd), can be NaN
    float pedestals[2];
    /// Maximum amplitude commonly used in many reconstruction schemes
    float maxAmp;
    /// Time of the maximum time commonly used in many reconstruction schemes,
    /// ns
    float maxTime;
    /// Channel mean time, used by some algorithms to find the closest maximum
    float meanTime;
    /// Channel mean time std.dev, used by some algorithms to discriminate
    /// side peaks
    float timeStdDev;
    /// Min amplitude threshold
    float minAmpThreshold;
    /// Can-be-zero probability
    float canBeZero;
    /// Abs. error estimate
    float absError;
    // ...

    // (pre-)fit results with Gaussian PDF, assumed that up to 8 peaks can be
    // found in a hit
    float gaussianMu[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , gaussianSigma[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , gaussianS[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , gaussianRange[NA64SW_MSADC_BROWSER_NMAX_PER_HIT][2]
        ;
    // fit results with Moyal function
    float moyalMu[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , moyalSigma[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , moyalS[NA64SW_MSADC_BROWSER_NMAX_PER_HIT]
        , moyalRange[NA64SW_MSADC_BROWSER_NMAX_PER_HIT][2];
    /// Peak square ratio
    float sqRatio[NA64SW_MSADC_BROWSER_NMAX_PER_HIT];
    /// Colored label for peak's time cluster
    int timeClusterLabel[NA64SW_MSADC_BROWSER_NMAX_PER_HIT];

    MSADCDisplayItem(size_t nSamples_);
    MSADCDisplayItem(event::SADCHit &);
    MSADCDisplayItem(const MSADCDisplayItem &);
    ~MSADCDisplayItem();
};

/// List of items indexed by string key
struct Items : public std::map<std::string, MSADCDisplayItem> {
    std::string eventID;
    // ... other event-wide info to annotate
};

/// Global index of destinations to see (singleton)
struct Destinations : protected std::unordered_map<std::string, Items> {
private:
    static Destinations * _self;
    Destinations();
public:
    static Destinations & self();

    Items & get_destination(const std::string &);
};  // struct Destinations

}  // namespace ::na64dp::msadcDispl
}  // namespace na64dp

