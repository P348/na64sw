#pragma once

#include "EventViewerMainFrame.hh"
#include "TGClient.h"
#include "TROOT.h"
#include "na64app/extension.hh"
#include "msadcItem.hh"

#include <log4cpp/Category.hh>

#include <TSystem.h>
#include <TApplication.h>

namespace na64dp {

/**\brief Runtime extension to display MSADC event viewer window
 *
 * ...
 * */
class MSADCEventViewer : public iRuntimeExtension {
private:
    bool _enableWindow  ///< turns on event display
       ;
    /// Ptr to main frame, if enabled
    EventViewerMainFrame * _mainFrame;
    TApplication * _TApp;

    /// Name of logging category, used only if `_handleStreams`
    std::string _logCatName;

    static MSADCEventViewer * _self;
public:
    MSADCEventViewer();
    void init( calib::Manager & cmgr
             , iEvProcInfo *) final;
    void set_parameter( const std::string & pn
                      , const std::string & pv
                      ) final;
    void finalize() final;

    /// Forwards execution to TApplication::Run() waiting for next
    void show();

    static MSADCEventViewer & self();
};  // class MSADCEventViewer

}  // namespace na64dp

