#pragma once

#include <list>
#include <unordered_map>

#include <TGClient.h>
#include <RQ_OBJECT.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>

class TGCheckButton;  // fwd
class TGLabel;  // fwd
class TGStatusBar;  // fwd
class TGTab;  // fwd
class TGraph;  // fwd
class TMultiGraph;  // fwd
class TF1;  // fwd
class TPaveText;  // fwd
struct PadsLayout;  // fwd

namespace na64dp {
namespace msadcDispl {
struct MSADCDisplayItem;  // fwd
}
}

struct TabAssets {
    TGCompositeFrame * frameInTab;
    TRootEmbeddedCanvas * canvas;
    const PadsLayout * padLayoutDesc;
};

class EventViewerMainFrame {
    RQ_OBJECT("EventViewerMainFrame")
private:
    /// main widget where all the nasty stuff resides
    TGMainFrame         *fMain;
    /// important widget with "embedded canvas"
    //TRootEmbeddedCanvas *fEcanvas;
    /// Status bar
    TGStatusBar     * _statusBar;
    /// turns on/off forced pedestal subtraction for shown graphs
    TGCheckButton   * fChkDoSubtractPedestals;
    // ...
    TGTab * _tabsPtr;
    /// List of tab assets
    std::list<TabAssets> _tabs;

    void _draw_maxima( TMultiGraph * itemMultiGraph
                     , TGraph * itemTGraph
                     , const std::pair<std::string, na64dp::msadcDispl::MSADCDisplayItem> &
                     , unsigned short nPad
                     , std::unordered_multimap<int, TF1*> & overimposedFunctions
                     , std::unordered_map<TPaveText *, Int_t> & labels
                     );
public:
    EventViewerMainFrame(const TGWindow *p,UInt_t w,UInt_t h);
    virtual ~EventViewerMainFrame();
    void DoDraw();
    /// Gets called at `ProcessedEvent()`
    void ExecEvent(Int_t,Int_t,Int_t,TObject*);
};  // class EventViewerMainFrame

