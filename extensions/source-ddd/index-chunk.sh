#!/bin/bash
#
# WARNING: since spills accounting procedure on backend is INCREMENTAL,
# repeatative running of this script on same chunk will produce DB with WRONG
# RESULTS (trigger counts, scalers, event type counts, etc will be WRONG).
#
# Invokation syntax:
#
#   $ index-chunk.sh <chunk-path> <sparseness> <uri>
#
# For given chunk, sparseness (duty) and remote server address, indexes chunk:
# calculates chunk's size and hashsum, gets information about spills, sparsed
# event offsets, etc. Extracted info is placed then into to .json files that
# are dispatched using curl to the remote endpoint.
#
# This script supposed to be ran for newly created data files to account them.
#
# Generated log is intended to be used in some kind of automated procedure.
# All lines except those for stdout starting with [??] will be ignored. Lines
# Starting with [??] describe indexing stages:
#
#   [?0] (preindex) provides log aggregation tool with identity of the job
#   [?1] (md5sum) MD5 hashsum extraction
#   [?2] (size) Obtaining file size
#   [?3] (index) Obtaining file index
#   [?4] (index-dispatch) Adding index to DB
#   [?5] (stats-dispatch) Adding stats to DB
#

function retry {
    command="$*"
    retval=1
    attempt=1
    until [[ $retval -eq 0 ]] || [[ $attempt -gt 10 ]]; do
        # Execute inside of a subshell in case parent
        # script is running with "set -e"
        (
            set +e
            $command
        )
        retval=$?
        attempt=$(( $attempt + 1 ))
        if [[ $retval -ne 0 ]]; then
            # If there was an error wait 10 seconds
            sleep 30
        fi
    done
    if [[ $retval -ne 0 ]] && [[ $attempt -gt 10 ]]; then
        # Something is foul, go ahead and exit
        exit $retval
    fi
}

#THIS_SCRIPT_LOC="$( cd "$( dirname "`readlink -f ${BASH_SOURCE[0]}`" )" && pwd )"
# Set this to path of `na64sw-index-ddd-file` util
#INDEX_UTIL=./extensions/source-ddd/na64sw-index-ddd-file
INDEX_UTIL=/afs/cern.ch/work/r/rdusaev/public/na64/sw/LCG_101/x86_64-centos7-gcc11-opt/var/src/na64sw.build/extensions/source-ddd/na64sw-index-ddd-file

# Get command line arguments
CHUNK_PATH=$1
SPARSENESS=$2
INDEX_URI=$3

# Get base name of the chunk:
CHUNK_FILENAME=$(basename -- ${CHUNK_PATH})
CHUNK_NAME=${CHUNK_FILENAME%.*}
# All NA64 chunks are of form like "cdr01001-006005.dat" where first number is
# chunk + 1000, second is a run number.
read -r CHUNK_NO RUN_NO <<<$(echo ${CHUNK_NAME} | sed -r 's/^cdr(.+)-(.+)$/\1 \2/')
CHUNK_NO=$((10#${CHUNK_NO} - 1000))
RUN_NO=$((10#${RUN_NO}))
# ^^^ NOTE: `10#' here forces BASH to apply dec base (otherwise it interprets
#     numbers in octal due to leading zeros)

#                                                                    __________
# _________________________________________________________________/ 0 Preindex

# Before anything is attempted, create an empty entry for the chunk in DB
echo "{\"md5Hashsum\":null,\"size\":null}" > chunk-${CHUNK_NAME}-stats.json
curl -X PUT \
    -D response-${CHUNK_NAME}-stats-0.txt \
    -H 'Content-Type: application/json' \
    --data-binary "@./chunk-${CHUNK_NAME}-stats.json" \
    ${INDEX_URI}/rawdata/index/run/${RUN_NO}/chunk/${CHUNK_NO}
# ^^^ This PUT request will erase/create with empty fields the chunk entry.
# Useful to detect broken/unindexed chunks.
if [[ $? -ne 0 ]] ; then
    echo "[E0] preindex"
    exit 1
else
    echo "[I0] preindex ; file=${CHUNK_NAME} run=${RUN_NO} chunk=${CHUNK_NO})"
fi

# NOTE: /eos/ is known to be temporary unavailable for HTConder nodes. This
# retry block forces to assure file can be read
#retry stat --print="%s" ${CHUNK_PATH}

#                                                                    __________
# _________________________________________________________________/ 1 MD5 sum

# Calculate file's md5 sum and get its size
MD5SUM=$(retry md5sum ${CHUNK_PATH} | awk '{print $1}')
if [[ $? -ne 0 ]] ; then
    echo "[E1] md5sum ; ${MD5SUM}"
    exit 1
else
    echo "[I1] md5sum"
fi

#                                                                       _______
# ____________________________________________________________________/ 2 Size

SIZE=$(retry stat --printf="%s" ${CHUNK_PATH})
if [[ $? -ne 0 ]] ; then
    echo "[E2] size ; ${SIZE}"
    exit 1
else
    echo "[I2] size"
fi

echo "{ \"md5Hashsum\":\"${MD5SUM}\", \"size\":${SIZE} }" > chunk-${CHUNK_NAME}-stats.json

#                                                                      ________
# ___________________________________________________________________/ 3 Index

# Indexing
#export LD_PRELOAD="/snap/root-framework/current/usr/local/lib/libEve.so:/snap/root-framework/current/usr/local/lib/libRGL.so"  # XXX
retry ${INDEX_UTIL} -s ${SPARSENESS} ${CHUNK_PATH} > chunk-${CHUNK_NAME}-index.json

if [[ $? -ne 0 ]] ; then
    echo "[E3] index"
    exit 1
else
    echo "[I3] index"
fi

# Get run ID / chunk ID:
# - run number(s) based on index content:
#RUN_NO=$(jq -r '.runs[].runNo' chunk-cdr01001-006005-index.json | sort | uniq)
# ^^^ no need, actually, for "official" data dir

#                                                             _________________
# __________________________________________________________/ 4 Index-dispatch

# Send the index using PATCH request to recursively create run/spill/events
# index (chunk without md5/size info is created automatically)
curl -X PATCH \
    -D response-${CHUNK_NAME}-index.txt \
    -H 'Content-Type: application/json' \
    --data-binary "@./chunk-${CHUNK_NAME}-index.json" \
    ${INDEX_URI}/rawdata/index/runs

if [[ $? -ne 0 ]] ; then
    echo "[E4] index-dispatch"
    exit 1
else
    echo "[I4] index-dispatch"
fi


#                                                             _________________
# __________________________________________________________/ 5 Stats-dispatch

# Modify chunk file's stats with calculated MD5 sum and size info
curl -X PATCH \
    -D response-${CHUNK_NAME}-stats.txt \
    -H 'Content-Type: application/json' \
    --data-binary "@./chunk-${CHUNK_NAME}-stats.json" \
    ${INDEX_URI}/rawdata/index/run/${RUN_NO}/chunk/${CHUNK_NO}

if [[ $? -ne 0 ]] ; then
    echo "[E5] stats-dispatch"
    exit 1
else
    echo "[I5] stats-dispatch"
fi

