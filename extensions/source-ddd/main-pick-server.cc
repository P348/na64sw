#include "fileMap.hh"

#include <unistd.h>
#include <fstream>
#include <cassert>
#include <cstring>

#include <sys/socket.h>
#include <netinet/in.h>

// Base directory for chunks lookup; expected to contain chunks, like
// <gDataDirPath>/cdr01001-007852.dat
static std::string gDataDirPath = "";

static void
_usage_info(const char * appName, std::ostream & os) {
    os << "NA64 reentrant event picking process." << std::endl
       << "Usage:" << std::endl
       << "    $ " << appName << " <base-dir> <port-no>" << std::endl
       << "Runs as server process listening to certain port. Awaits"
          " input info on chunk number, initial offset and desired event ID"
          " to read the events. Intended usage implies daemonization on the"
          " host with environment providing DaqDataDecoding library and stuff"
          " (like sft.cvmfs.cern.ch, etc). Usually coped with Python services"
          " delegating event reading task to this process running as system"
          " daemon (logging and daemonization utilities controlled by"
          " external means)."
       << std::endl;
}

// Defines binary format of expected incoming message, packed bytes, in order
#define for_all_incoming_msg_entries(m) \
    m(uint32_t, runNo   ) \
    m(uint16_t, spillNo ) \
    m(uint32_t, eventNo ) \
    m(uint64_t, offset  ) \
    m(uint16_t, chunkNo )
// Response format is given in two forms:
//  1. <errLength:uint32_t><errMessage:str>
//  2. <eventSize:uint32_t><eventData:bytearray>
static int
send_error(int connfd, char * errMsg, size_t errMsgLen) {
    assert(errMsgLen < (UINT32_MAX >> 1));  // last bit is reserved
    uint32_t msgLen = ((uint32_t) errMsgLen) << 1;
    msgLen |= 1;
    if(-1 == send(connfd, &msgLen, sizeof(msgLen), MSG_MORE)) {
        fprintf( stderr
               , "(response) failed to send error msg length: %s"
               , strerror(errno) );
        return -1;
    }
    if(-1 == send(connfd, errMsg, errMsgLen, 0x0) ) {
        fprintf( stderr
               , "(response) failed to send error msg: %s"
               , strerror(errno) );
        return -1;
    }
    return 0;
}


int
fetch_event(int connfd) {
    // We expect incoming connection to bring following byte data:
    const size_t bufLenght = 0
        # define increment_buf_len(tp, nm)  + sizeof(tp)
        for_all_incoming_msg_entries(increment_buf_len)
        # undef increment_buf_len
        ;
    std::vector<uint8_t> readBytes;
    readBytes.resize(bufLenght);
    size_t nRead = 0;
    do {
        ssize_t recvd = recv(connfd, readBytes.data() + nRead, bufLenght, 0x0);
        if(-1 == recvd) {
            fprintf(stderr, "recv() error: %s, giving up.\n", strerror(errno));
            return -1;
        }
        if(nRead + recvd > bufLenght) {
            fprintf( stderr, "Incoming msg is too large (at least %zu"
                             " bytes vs expected %zu).\n"
                   , nRead + recvd, bufLenght );
            return -1;
        }
        if(recvd == 0) {
            fprintf( stderr, "Incoming msg is too short (got only %zu"
                             " bytes instead of %zu).\n"
                   , nRead + recvd, bufLenght );
            return -1;
        }
        assert(recvd > 0);
        nRead += recvd;
    } while(nRead < bufLenght);
    assert(nRead == bufLenght);
    // Extract values
    uint8_t * c = readBytes.data();
    # define define_variable(tp, nm)                \
        tp nm = *reinterpret_cast<const tp*>(c);    \
        c += sizeof(tp);
    for_all_incoming_msg_entries(define_variable)
    # undef define_variable

    na64dp::EventID eid(runNo, spillNo, eventNo);

    size_t errMsgLen;
    char errbf[1024];
    char path[512];
    snprintf( path, sizeof(path)
            , "%s/cdr%05d-%06d.dat"
            , gDataDirPath.c_str()
            , 1000 + chunkNo
            , runNo
            );
    std::cout << "Requested event " << eid << " assumed to be in file "
        << path << " (chunk#=" << chunkNo << ") after " << offset << "-th byte."
        << std::endl;

    // check chunk file exists and we can read it
    if(-1 == access(path, F_OK | R_OK)) {
        int errNo = errno;
        errMsgLen = snprintf( errbf, sizeof(errbf)
                            , "File \"%s\" access error: %s"
                            , path, strerror(errNo) );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    }

    // Open file
    std::ifstream is(path, std::ios::binary);
    is.seekg(offset);

    std::vector<uint8_t> data;
    try {
        data = na64dp::ddd::pick_event(is, eid);
    } catch(na64dp::ddd::error::MagicCheckFailed & e) {
        errMsgLen = snprintf( errbf, sizeof(errbf)
                , "bad magic number for an event header"
                  " (wrong initial offset or corrupted file \"%s\"): %s"
                , path, e.what() );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    } catch(na64dp::ddd::error::SourceDepleted & e) {
        errMsgLen = snprintf( errbf, sizeof(errbf)
                , "unable to [further?] read the source file \"%s\""
                  " (no such event in file or file is corrupted): %s"
                , path, e.what() );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    } catch(na64dp::ddd::error::FileMapError & e) {
        errMsgLen = snprintf( errbf, sizeof(errbf)
                , "generic DDD source error \"%s\": %s"
                , path, e.what() );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    } catch(std::exception & e) {
        errMsgLen = snprintf( errbf, sizeof(errbf)
                , "generic std error on file \"%s\": %s"
                , path, e.what() );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    }
    is.close();
    // if we reach here, event has been successfully read. Put log entry and
    // dispatch it back now
    std::cout << "Event " << eid << " of size " << data.size() << "b read."
        << std::endl;

    if(data.size() >= (UINT32_MAX >> 1)) {  // higly imporbable
        errMsgLen = snprintf( errbf, sizeof(errbf)
                , "event seem too large: %zu bytes vs max allowed %u"
                , data.size(), UINT32_MAX >> 1 );
        fputs(errbf, stderr); fputc('\n', stderr);
        send_error(connfd, errbf, errMsgLen);
        return -1;
    }
    uint32_t msgLen = ((uint32_t) data.size()) << 1;
    if(-1 == send(connfd, &msgLen, sizeof(msgLen), MSG_MORE)) {
        fprintf( stderr
               , "(response) failed to send event msg length: %s"
               , strerror(errno) );
        return -1;
    }
    if(-1 == send(connfd, data.data(), data.size(), 0x0) ) {
        fprintf( stderr
               , "(response) failed to send event data: %s"
               , strerror(errno) );
        return -1;
    }
    return 0;
}

int
main(int argc, char * argv[]) {
    if( argc == 1 || (argc == 2 && (std::string(argv[1]) == "-h")) ) {
        _usage_info(argv[0], std::cout);
        return 0;
    }
    if(argc != 3) {
        std::cerr << argv[0] << " error: bad command line arguments"
            " (expected 3, got " << argc - 1 << ")." << std::endl;
    }
    
    //
    gDataDirPath = argv[1];  // TODO: check dir exists
    int portNo = atoi(argv[2]);
    if(portNo <= 0) {
        fprintf(stderr, "Bad port number specified: %d.\n", portNo);
        exit(EXIT_FAILURE);
    }
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    {
        int sockOpt = 1;
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockOpt, sizeof(sockOpt));
    }
    if (sockfd == -1) {
        fprintf(stderr, "Failed to create socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    bzero(&servaddr, sizeof(servaddr));
    // assign
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(portNo);
    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) == -1) {
        fprintf(stderr, "Failed to bind socket: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    // Mark socket for listening
    if ((listen(sockfd, 8/*(backlog)*/)) == -1) {
        printf("listen() failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    // Listening loop
    socklen_t cliSockLen = sizeof(cli);
    for(;;) {
        puts("listening...");
        connfd = accept(sockfd, (struct sockaddr*)&cli, &cliSockLen);
        if (connfd < 0) {
            fprintf( stderr
                   , "accept() error: %s, ignoring\n"
                   , strerror(errno));
            continue;
        }
        fetch_event(connfd);
    }
    close(sockfd);

    return 0;
}

