import socket, struct

def get_event_data(eid, chunkNo, offset, portNo=4948, host=''):
    sock = socket.socket()
    sock.connect((host, portNo))
    # runNo:uint32_t, spillNo:uint16_t, eventNo:uint32_t, offset:uint64_t, chunkNo:uint16_t
    msg = struct.pack('<IHIQH', eid[0], eid[1], eid[2], offset, chunkNo )
    sock.send(msg)
    evReadResponse = sock.recv(struct.calcsize('@I'))
    retCode = struct.unpack('@I', evReadResponse)[0]
    if retCode & 1:
        # error
        errMsgLen = retCode >> 1
        errMsg = sock.recv(errMsgLen).decode()
        raise RuntimeError(errMsg)
    evDataLen = retCode >> 1
    return sock.recv(evDataLen)

evData = get_event_data((8452, 199, 12002), 47, 585401708)
print(f'Event data of {len(evData)}b read.')

#                           * * *   * * *   * * *

@contextlib.contextmanager
def mkfifo_tmp(baseFilename='fifo'):
    """
    Context Manager for creating named pipes with temporary names.
    """
    tmpdir = tempfile.mkdtemp()
    filename = os.path.join(tmpdir, baseFilename)
    fifo = os.mkfifo(filename)
    try:
        yield filename
    finally:
        os.unlink(filename)
        os.rmdir(tmpdir)

#                           * * *   * * *   * * *

        # Use config parameters to render chunk path
        chunkPath = evPickCfg['chunkPathFormat'].format( chunkNo=int(chunkNo),
                chunkNo1k=1000+int(chunkNo), runNo=int(runNo) )
        # Create the named pipe to communicate binary output of the event
        # reading app, keep result, check exit code, etc
        eventData = bytearray()
        with mkfifo_tmp('event') as fifoPath:
            with open(fifoPath, 'rb') as fifo:
                # NOTE: cmd-line interface for event picking app:
                #       <input-file> <initial-offset> <event-id> <output-file>
                readP = os.Popen( [ evPickCfg['appExec']  # event picking app
                                  , chunkPath  # input file
                                  , closestOffset  # starting offset
                                  , f'{runNo}/{spillNo}-{eventNo}'  # event ID to look for
                                  , fifoPath  # output file
                                  ]
                                  , stdout=subprocess.PIPE, stderr=subprocess.PIPE
                                )
                while True:
                    data = fifo.read()  # shall return bytearray
                    if 0 == len(data):
                        break  # writer done
                    eventData.append(data)
