# Server app

Provides REST API for run index database.

* `browse-spa/` -- JavaScript frontend app to browse the DB by means of REST
  API implemented by server app
* `config.*.json` -- configuration file for the REST server corresponding to
  different usage scenarios
* `docker-compose.yaml.example` -- a boilerplate config for deployment of
  production bundle with DB and REST server
* `Dockerfile` -- REST server container build instructions
* `requirements.txt` -- Python dependencies for REST server app
* `server.py` -- REST server app

Note, that REST server app may also provide event picking, if host runs
corresponding event reader process.

## Intended usage

A Docker container with server app is running on `na64-dev.cern.ch`
node exposing the server to CERN intranet. Batch jobs, logbook site and
other apps looking for run/spill/raw data info query the instance for such
info. Copies of database dump are performed on regular basis with `acrontab` or
similar service as a simple measure of protection against data loss or damage.
Only snapshots with different hashsums are kept.

Tables are intended to keep only the raw data-related info like chunk size and
layout, or spills start/stop time and counts. Things like shift crew comments,
notes and other info shall be decoupled and shall become the subject for
another DB(s) supervised by other service(s).

## Snippets

Production server instance exposes API and also offers simple single-page
app (SPA) for browsing the tables. It is possible to run instance with API and
SPA independently for development purposes (note: one has to enable CORS for
latter case).

### Production deployment (picking daemon + middleware container + DB container)

1. Clone the repo on *prod host* (like `na64-dev`)
2. Make the client SPA build in `srv/browse-spa/`. You probably won't have node
   on the target host, so do it on your *local env* to copy results:
    
    $ yarn build

3. Copy the `srv/browse-spa/dist/` from *local env* to *prod host* (e.g. by SSH).
   For instance:

    (local env) $ tar cvjf dist.tar.bz2 dist/
    (local env) $ scp dist.tar.bz2 rdusaev@lxplus.cern.ch:/afs/cern.ch/user/r/rdusaev/Projects/na64/na64sw/extensions/source-ddd/srv/browse-spa/
    (prod host) $ tar xvvf dist.tar.bz2

4. Choose password for DB and secret key and create modified files
   `config.production.json` and `docker-compose.yaml` in cwd (you may
   refer to `*.example` for boilerplate); In `.json` config also set 
   `NA64SW_EVENT_PICKING_PORT` and `NA64SW_EVENT_PICKING_HOST` variables
   to refer to event-picking daemon
5. Run `docker compose build` (or `docker-compose build`) to make the
   images as described in `docker-compose.yaml`
6. Run `docker compose up` to run the service and DB

### REST server in a standalone container

Debug deployment may rely on sqlite3 backend. Useful, when DB is needed for
small local developments/distributed calculus without centralized DB.

1. Build the container image

    $ docker build . -t ddd-srv --build-arg CFG=./config.debug.json

2. Run container as daemon (TODO: mount `/cvmfs`, `/eos` for event picking)

    $ docker run -d --name ddd-srv --restart always -p 8080:8080 -v /home/uswgi/ddd ddd-srv

3. (opt) Check service is available with:

    $ curl 127.0.0.1:8080/rawdata/index/runs

   A valid JSON data has to be returned.

#### Common snippets

Following environment variables are of use for the running Flask app instance:

 * `DDDSRV_INSTANCE_PATH` shall refer to the instance dir, where log and
   database instance shall be stored.
 * `DDDSRV_STATIC_FILES_DIR` shall refer to the directory containing client
   SPA (see `browse-spa/`). Requests from root path will be redirected there,
   to static files, if they does not match API patterns.

Note: both variables are hidden (used internally) in the container.

Consider following snippet to build the container:

    srv/ $ docker build . -t ddd-srv --build-arg CFG=./config.debug.json

Note: to change default exposed port, use `--build-arg PORT=<your-port>`.

Consider following snippets to run container locally (by default on port `8080`
with whatever IP will be assigned by Docker service:

    $ docker run -it --name ddd-srv -v /home/uwsgi/ddd -p 8080:8080 ddd-srv

It is possible to customize the `waitress` settings from command line:

    $ docker run --rm -it --name ddd-srv -p 8182:8182 ddd-srv --port 8182 server:app

Note that removal of container also wipes its volumes with all the stored data
if volume is not used.

It is possible to run server with `python3` in virtualenv with flask, sqlite3
(and other stuff from `requirements.txt` installed).

### Copying data

Copy the local DB dump from running container with:

    $ docker cp ddd-srv:/home/uswgi/ddd/index.db .

Similarly, to restore DB from back-up:

    $ docker cp ./index.db.backup ddd-srv:/home/uswgi/ddd/index.db

There is also a `log.txt` file in the container providing internal log of the
app.

### Note on production deployment and client app

The Docker image does not provide JS stuff, nor built SPA is kept by the repo.
One has to build the SPA by its own and then copy resulting `index.html`
and `assets/` dir to `browser-spa/dist` dir before building the image.

# TODO

1. No authentication currently anticipated for server. That means basically anyone
   can PATCH/POST modifications to DB. Potentially dangerous.
3. HATEOAS as HAL is not currently supported for the API. I doubt we'll come to
   the point when it is really needed, but it's good to have it for general
   clarity eventually anyway.

