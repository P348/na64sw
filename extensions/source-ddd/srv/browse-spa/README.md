# DaqDataDecoding Files Index Browser

This dir provides source code for a small single-page app (SPA) to browse
index tables. This app uses running RESTful backend to retrieve information
about run, spills and chunks.

## Project structure

Project structure has following major components:

* Entry point configuring a [Vue](https://vuejs.org/) app instance and
  [Vue router](https://router.vuejs.org/) for stateful browsing: `src/main.js`.
* A (rather generic) component implementing paginated table browsing with
  asynchroneous data fetching. To the moment, this component is used only
  to show the list of runs: `src/components/IndexEntriesTable.vue` in thin
  wrapper specialized component `src/components/Runs.vue`;
* A (highly specialized) component to show info of single
  run: `src/components/Run.vue`;
*  A (highly specialized) component to show info of single
  spill: `src/components/Spill.vue`.

The project is steered (packed) by `yarn` and `vite`. For development purposes
it is expected that you run a server instance accessible by some URL (given in
dotenv file) -- it may be a development Flask server or full-fledged production
server.

For production purposes, the SPA might be hosted on the same node as the
server.

## Snippets

Run dev server on vite for develpment and debug:

    $ yarn dev

Prepare bundle for production ("build"):

    $ yarn build

Note on config parameters: dotenv files provide configuration for dev/prod
builds. Edit `VITE_DEFAULT_BACKEND_URL` variable to specify particular backend.
For instance, to specify the Flask dev server running on host on port 5000,
edit `.env.development`:

    VITE_DEFAULT_BACKEND_URL=127.0.0.1:5000

(do not forget to enable CORS in server's config!) For production the config
file is called `.env.production`.

