const gDatetimeFmt = new Intl.DateTimeFormat('en-US', {
        timeZone: 'Europe/Zurich',
        year: '2-digit', month: 'short', day: 'numeric',
        hour: 'numeric', minute: 'numeric', second: 'numeric',
        hour12: false,
    });

export function datetime_fmt(ts) {
    if(!ts) return '(no data)'
    return gDatetimeFmt.format(new Date(ts*1e3));
}

