import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import './style.css'
import App from './App.vue'
import Runs from './components/Runs.vue'
import Run from './components/Run.vue'
import Spill from './components/Spill.vue'

let defaultBackendHost = import.meta.env.VITE_DEFAULT_BACKEND_URL;
if(!defaultBackendHost)
    defaultBackendHost = 'http://' + location.host;

const routes = [
    {path: '/', redirect: '/runs/' + encodeURIComponent(defaultBackendHost)},
    {path: '/runs/:backendProvider', component: Runs, props: true, name: 'runs'},
    {path: '/run/:backendProvider/run/:runNo', component: Run, props: true, name: 'run' },
    {path: '/run/:backendProvider/run/:runNo/spill/:spillNo', component: Spill, props: true, name: 'spill' },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

const app = createApp(App);
app.use(router);
app.mount('#app');


