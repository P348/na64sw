#!/bin/sh

OUTFILE=/afs/cern.ch/work/r/rdusaev/na64/index-db-backup-$(date "+%y-%m-%d_%H-%M-%S").gz

sudo docker exec -t srv_ddd-db_1 pg_dumpall -c -U postgres | gzip > $OUTFILE

echo "$OUTFILE written."
