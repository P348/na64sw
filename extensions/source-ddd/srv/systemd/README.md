# Services/target scripts for systemd

Put it in `/usr/local/lib/systemd/system/` to manage ddd server bundle as a target
with systemd (`systemctl start ddd-srv.target`). Bump the paths wrt your
deployment. Put the `env | sort` dump to `this-env.txt` for pick event service.

Usual snippets (mostly, tandard systemd stuff which I hate and constantly
forget):
    
    # mkdir /usr/local/lib/systemd/system -p
    ... (copy and modify the configs)
    # systemctl daemon-reload
    # systemd-analyze verify ddd-pick-event.service
    # systemd-analyze verify ddd-index-storage.service
    # systemctl start ddd-pick-event.service
    # systemctl start ddd-index-storage
    # systemctl status ddd-index-storage
    # journalctl -u ddd-index-storage
    # /bin/bash -c "pv /afs/cern.ch/work/r/rdusaev/na64/index-db-backup-23-01-05_10-46-29.sql | docker exec -i na64ddd_ddd-db_1 psql -U postgres"


