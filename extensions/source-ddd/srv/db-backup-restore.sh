#!/bin/sh

if [[ ! -f $1 ]] ; then
    echo 'File "$1" does not exist.'
    exit 1
fi

zcat $1 | pv -cN zcat | docker exec -i srv_ddd-db_1 psql -U postgres

