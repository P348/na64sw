import json, copy, functools, operator, logging, os, contextlib, socket, struct, traceback
import logging.config
import logging.handlers

from sqlalchemy.orm import object_session
from sqlalchemy.sql.expression import ClauseElement
from sqlalchemy.ext.hybrid import hybrid_property #, hybrid_method
from sqlalchemy.sql import select
from sqlalchemy import func, UniqueConstraint, Table, ForeignKeyConstraint
import sqlalchemy.exc

from flask import Flask, Blueprint, send_from_directory, jsonify, make_response
from werkzeug.exceptions import HTTPException
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, request, url_for
from flask_restful import Resource, Api

#
# Utils

#
# Initial setup

# create the extension
db = SQLAlchemy()
# create the app
app = Flask( __name__
           , instance_path=os.getenv('DDDSRV_INSTANCE_PATH')
           )

api_bp = Blueprint('rawdata', __name__)
api = Api(api_bp)
# configure the SQLite database, relative to the app instance folder
app.config.from_file(os.getenv('DDDSRV_CONFIG', "config.json"), load=json.load)
# init logging
logging.config.dictConfig(app.config['LOGGING'])
# initialize the app with the extension
if app.config.get('CORS', False):
    CORS(app)

#
# Set error handler
#@app.errorhandler(Exception)  # HTTPException
def handle_http_exception(error):
    error_dict = {
        'code': error.code if hasattr(error, 'code') else None,
        'description': error.description if hasattr(error, 'code') else str(error),
        'stack_trace': traceback.format_exc()
    }
    log_msg = f"HTTPException {error_dict['code']}, Description: {error_dict['description']}, Stack trace: {error_dict['stack_trace']}"
    logging.getLogger().error(log_msg)
    response = jsonify(error_dict)
    return response \
         , error.code if hasattr(error, 'code') else 500

app.register_error_handler(Exception, handle_http_exception)

logging.getLogger().info('DDD index server starting.')

#
# Resources

class RunsAPI(Resource):
    def get(self):
        """
        Returns list or run records.
        """
        currentPage = int(request.args.get('page', 1))
        q = Run.query.order_by(Run.runNo.desc()) \
                        .paginate( page=currentPage
                                 , per_page=int(request.args.get('itemsPerPage', 10))
                                 )
        #q = db.select(Run).order_by(Run.runNo)
        R = {
                'total': q.total,  #db.session.query(Run).count(),
                'first': q.first,
                'last': q.last,
                'pages': [ page for page in q.iter_pages() ],
                'currentPage': currentPage,
                #'count': ... # TODO: on this page
                '_links': {
                    'self': {'href': url_for('rawdata.runs')},
                    'next': {'href': url_for('rawdata.runs', page=2)},
                    'find': {'href': '/rawdata/index/run/{runNo}', 'templated': True}  # NOTE: templated HAL URL
                },
                '_embedded': {
                    'items': []
                }
            }
        for run in q.items:
            R['_embedded']['items'].append({
                'runNo': run.runNo,
                'nSpills': len(run.spills),
                'nChunks': len(run.chunks),
                'nOffsets': None,  #run.nOffsets,
                'startTime': run.startTime,
                'endTime': run.endTime,
                'nEvents': run.nEvents,
                # size
                '_links': {
                    'self': {'href': url_for('rawdata.run', runNo=run.runNo)}
                }
            })
        return R

    def patch(self, runNo=None):
        """
        A shortcut for recursive creation of the run+spill+chunk record used
        by `na64sw-index-ddd-file` application.
        """
        if request.content_type.startswith('application/json'):
            rq = request.get_json()
        else:
            return {'errors': ['No JSON payload (wrong content type).']}, 400
        if runNo is not None:
            if type(rq) is dict and 'runs' in rq.keys():
                return {'errors': ['Both "runs" and runID in address provided']}, 400
            if type(rq) is not dict:
                return {'errors': ['Wrong payload data type']}, 400
            rq = {'runs': copy.copy(rq)}
        if 'runs' not in rq.keys() or type(rq['runs']) is not list:
            return {'errors': ['No "runs" list in JSON payload or it is not a list.']}, 400
        # this chunk extraction here is purely for logging
        providedChunks = set()
        for runPl in rq['runs']:
            for spillPl in runPl['spills']:
                if spillPl.get('chunkNo', None) is None: continue
                providedChunks.add((int(runPl['runNo']), int(spillPl['chunkNo'])))
        strChunkIDs = ', '.join(map(lambda e: f'#{e[0]}/{e[1]}', sorted(providedChunks)))
        logging.getLogger().info(f'Got PATCH for runs update from {request.remote_addr} for {strChunkIDs}')
        for runPl in rq['runs']:
            runNo = int(runPl['runNo'])
            run = db.session.query(Run).filter_by(runNo=runNo) \
                    .with_for_update() \
                    .one_or_none()
            if run is None:
                run = Run(runNo=runNo)
                db.session.add(run)
                db.session.flush()
            for spillPl in runPl['spills']:
                assert run is not None
                # Create/retrieve chunk object
                chunk = None
                if spillPl.get('chunkNo', None):
                    chunkNo = int(spillPl['chunkNo'])
                    chunk = db.session.query(Chunk).filter_by(chunkNo=chunkNo, runNo=runNo).one_or_none()
                    if chunk is None:
                        chunk = Chunk(chunkNo=chunkNo, runNo=runNo)
                        db.session.add(chunk)
                        db.session.flush()
                # Modify/create spill object
                spillNo = int(spillPl['spillNo'])
                assert spillNo
                # Spills often added concurrently, so we reconsider result
                retry = 2
                spill = None
                while not spill:
                    try:
                        spill = db.session.query(Spill) \
                                .filter_by(spillNo=spillNo, runNo=runNo) \
                                .with_for_update() \
                                .one_or_none()
                        if spill is None:
                            incrementalUpdate = False
                            spill = Spill(run=run, spillNo=spillNo)
                            db.session.add(spill)
                            db.session.flush()
                        else:
                            # `override' querystring bool opt cause rewriting (just 
                            # reserved so far -- unused, untested)
                            incrementalUpdate = not request.args.get('override', False)
                    except sqlalchemy.exc.IntegrityError as e:
                        if retry < 1:
                            raise e
                        retry -= 1
                        spill = None
                        continue
                # Add/assure chunk for this spill
                if chunk not in spill.chunks:
                    spill.chunks.append(chunk)
                    incrementalUpdate = False  # has no sense
                # Modify scalar properties
                # NOTE/TODO: increment values for multichunk entries !!!gh
                if spillPl.get('nEvents', None):
                    if incrementalUpdate:
                        spill.nEvents += int(spillPl['nEvents'])
                    else:
                        spill.nEvents  = int(spillPl['nEvents'])
                #if spillPl.get('chunkNo', None):
                #   if incrementalUpdate:
                #        spill.chunkNo = int(spillPl['chunkNo'])
                if spillPl.get('start', None):
                    if (not incrementalUpdate) \
                    or spill.startTimestamp > int(spillPl['start'][0]):  # TODO: consider subsec?
                        spill.startTimestamp = int(spillPl['start'][0])
                        spill.startSubSec = int(spillPl['start'][1])
                if spillPl.get('end', None):
                    if (not incrementalUpdate) \
                    or spill.endTimestamp < int(spillPl['end'][0]):  # TODO: consider subsec?
                        spill.endTimestamp = int(spillPl['end'][0])
                        spill.endSubSec = int(spillPl['end'][1])
                # vector property: a chunk and a byte offsets within this chunk
                if spillPl.get('offsets', None):
                    for offsetPl in spillPl['offsets']:
                        assert run
                        assert spill
                        assert chunk
                        eventNo = int(offsetPl[0])
                        offsetEntry = db.session.query(EventOffset) \
                                .filter_by( run=run, spill=spill, chunk=chunk, eventNo=eventNo ) \
                                .one_or_none()
                        if not offsetEntry:
                            offsetEntry = EventOffset(run=run, spill=spill, chunk=chunk, eventNo=eventNo)
                        offsetEntry.eventNo = eventNo
                        offsetEntry.byteOffset = int(offsetPl[1])
                        db.session.add(offsetEntry)
                        db.session.flush()
                # vector property: event type counts per spill
                if spillPl.get('typeCounts'):
                    for typeCountName, typeCountVal in spillPl['typeCounts'].items():
                        tc = db.session.query(TypeCount) \
                                .filter_by(runNo=run.runNo, spill=spill, name=typeCountName) \
                                .one_or_none()
                        if tc is None:
                            tc = TypeCount(runNo=run.runNo, spill=spill, name=typeCountName)
                            tc.value  = typeCountVal
                        elif incrementalUpdate:
                            tc.value += typeCountVal
                        else:
                            tc.value  = typeCountVal
                        db.session.add(tc)
                        db.session.flush()
                # vector property: trigger counts per spill
                if spillPl.get('triggerCounts'):
                    for triggerCountBits, val in spillPl['triggerCounts'].items():
                        bits = int(triggerCountBits, 16)
                        tc = db.session.query(TriggerCount) \
                                .filter_by(runNo=run.runNo, spill=spill, bits=bits) \
                                .one_or_none()
                        if tc is None:
                            tc = TriggerCount(runNo=run.runNo, spill=spill, bits=bits)
                            tc.value  = val
                        elif incrementalUpdate:
                            tc.value += val
                        else:
                            tc.value  = val
                        # TODO: incrementalUpdate
                        tc.value = val
                        db.session.add(tc)
                        db.session.flush()
                # vector property: scaler counts (TODO)
                # ...
                db.session.add(spill)
        db.session.commit()
        return {}, 200  # TODO: meaningful details, location(s), etc

class RunAPI(Resource):
    def get(self, runNo):
        try:
            run = db.session.query(Run).filter(Run.runNo==runNo).one()
        except sqlalchemy.exc.NoResultFound as e:
            return {'errors':['Run not found.']}, 404
        # find out page for run (convinient for some user apps)
        R = {
            'runNo': run.runNo,
            'nSpills': len(run.spills),
            'nChunks': len(run.chunks),
            #'nOffsets': len(run.offsets),
            'startTime': run.startTime,
            'endTime': run.endTime,
            'nEvents': run.nEvents,
            # TODO: size
            '_links': {
                'collection': {'href': url_for('rawdata.runs')},
                'self': {'href': url_for('rawdata.run', runNo=runNo)},
                #'next': {'href': url_for('rawdata.runs', page=2)},
            },
            '_embedded': {
                'spills': [],
                'chunks': []
            }
        }
        pagination = db.session.query(Run.runNo).order_by(Run.runNo.desc()) \
                .filter(runNo >= runNo) \
                .paginate( per_page=10)
        while True:
            if runNo in set(map(lambda tpl: tpl[0], pagination.items)):
                R['runsPageNum'] = pagination.page
                break
            if not pagination.has_next:
                runsPageNum = None
                break
            pagination = pagination.next()
        #spills = db.session.query(Spill).join(run)
        for spill in run.spills:
            R['_embedded']['spills'].append({
                'runNo': spill.runNo,
                'spillNo': spill.spillNo,
                'chunks': list(chunk.chunkNo for chunk in spill.chunks),
                'startTimestamp': spill.startTimestamp,
                'startSubSec': spill.startSubSec,
                'endTimestamp': spill.endTimestamp,
                'endSubSec': spill.endSubSec,

                'nEvents': spill.nEvents,
                'nOffsets': spill.nOffsets,  #len(spill.offsets),  # TODO: hybrid property!
                'nEventTypes': len(spill.eventTypeCounts),
                'nTriggerCountTypes': len(spill.triggerCounts),
                'nScalerCounts': len(spill.scalerCounts),

                '_links': {
                    'self': {'href': url_for('rawdata.spill', runNo=runNo, spillNo=spill.spillNo)}
                }
            })
        for chunk in run.chunks:
            R['_embedded']['chunks'].append({
                    'chunkNo': chunk.chunkNo,
                    'size': chunk.size,
                    'md5': chunk.md5Hashsum,
                    'nOffsets': chunk.nOffsets
                })
        return R

class SpillAPI(Resource):
    def get(self, runNo, spillNo):
        """
        Singular spill API.
        """
        spill = db.session.query(Spill).filter(Spill.runNo==runNo, Spill.spillNo==spillNo).one()
        R = {
            'runNo': spill.runNo,
            'spillNo': spill.spillNo,
            'startTimestamp': spill.startTimestamp,
            'startSubSec': spill.startSubSec,
            'endTimestamp': spill.endTimestamp,
            'endSubSec': spill.endSubSec,
            'nEvents': spill.nEvents,
            'offsets': [],
            'eventTypeCounts': [],
            'triggerCounts': [],
            'scalerCounts': [],
            '_links': {
                'self': {'href': url_for('rawdata.spill', runNo=runNo, spillNo=spill.spillNo)}
            }
        }
        for offsetPair in spill.offsets:
            R['offsets'].append({
                    'eventNo': offsetPair.eventNo,
                    'byteOffset': offsetPair.byteOffset,
                    'chunkNo': offsetPair.chunkNo
                })
        for eventType in spill.eventTypeCounts:
            R['eventTypeCounts'].append((eventType.name, eventType.value))
        for triggerCount in spill.triggerCounts:
            R['triggerCounts'].append((triggerCount.bits, triggerCount.value))
        for scalerCounts in spill.scalerCounts:
            R['scalerCounts'].append((scalerCount.name, ScalerCount.value))
        return R

class ChunkAPI(Resource):
    """Singular chunk API"""
    def get(self, runNo, chunkNo):
        try:
            chunk = db.session.query(Chunk).filter(Chunk.runNo==runNo, Chunk.chunkNo==chunkNo).one()
        except sqlalchemy.exc.NoResultFound as e:
            return {'errors':['Run such chunk.']}, 404
        R = {
                'runNo': chunk.runNo,
                'chunkNo': chunk.chunkNo,
                'size': chunk.size,
                'md5': chunk.md5Hashsum,
                'offsets': []
            }
        acqOffsets = request.args.get('offsets', True)
        if type(acqOffsets) is str:
            if acqOffsets.lower() in ('no', 'false', 'disable', '0'):
                acqOffsets = False
            else:
                acqOffsets = True
        if acqOffsets:
            R['offsets'] = []
            for offsetPair in chunk.offsets:
                R['offsets'].append({
                        'eventNo': offsetPair.eventNo,
                        'byteOffset': offsetPair.byteOffset,
                        'spillNo': offsetPair.spillNo
                    })
        #for offsetPair in chunk.offsets:
        #    R['offsets'].append((offsetPair.spillNo, offsetPair.eventNo, offsetPair.byteOffset))
        return R

    def put(self, runNo, chunkNo):
        """Updates or creates new chunk _and_run_"""
        runCreated, chunkCreated = False, False
        if request.content_type.startswith('application/json'):
            rq = request.get_json()
        else:
            return {'errors': ['No JSON payload (wrong content type).']}, 400
        # Check the run
        runNo = int(runNo)
        run = db.session.query(Run).filter_by(runNo=runNo).one_or_none()
        if not run:
            run = Run(runNo=runNo)
            db.session.add(run)
            runCreated = True
        chunk = db.session.query(Chunk).filter(Chunk.run==run, Chunk.chunkNo==chunkNo).one_or_none()
        if chunk is None:
            chunk = Chunk(chunkNo=chunkNo, runNo=runNo)
            chunkCreated = True
        #else:
        #    return {'errors': ['Resource exists.']}, 409
        chunk.size = rq.get('size', None)
        chunk.md5Hashsum = rq.get('md5Hashsum', None)
        db.session.add(chunk)
        db.session.commit()
        # TODO: meaningful response payload, location, etc:
        return {'runCreated': runCreated}, 201 if chunkCreated else 200

    def patch(self, runNo, chunkNo):
        """Modifies existing chunk's info"""
        if request.content_type.startswith('application/json'):
            rq = request.get_json()
        else:
            return {'errors': ['No JSON payload (wrong content type).']}, 400
        try:
            chunk = db.session.query(Chunk).filter(Chunk.runNo==runNo, Chunk.chunkNo==chunkNo).one()
        except sqlalchemy.exc.NoResultFound:
            return {'errors':['No such chunk']}, 404
        chunk.size = rq.get('size', None)
        chunk.md5Hashsum = rq.get('md5Hashsum', None)
        db.session.add(chunk)
        db.session.commit()
        return {}, 200  # TODO: meaningful response payload, location?


def get_event_data(eid, chunkNo, offset, portNo=4948, host=''):
    """
    Utility function retrieving event data from event picking server.
    Expects eid to be tuple of run, spill, event-in-spill integer numbers.
    Provided offset, chunk number and event ID will be submitted by TCP socket
    to the given host:port destination as binary data. Response is considered
    to be marked with fail bit (least) in size prefix. Depending on that bit,
    tesponse shall be interpreted as either an error message (string) or as
    binary event content.
    Normally function returns byte array with event content, while for failures
    it raises exceptions.
    """
    sock = socket.socket()
    sock.connect((host, portNo))
    # runNo:uint32_t, spillNo:uint16_t, eventNo:uint32_t, offset:uint64_t, chunkNo:uint16_t
    msg = struct.pack('<IHIQH', eid[0], eid[1], eid[2], offset, chunkNo )
    sock.send(msg)
    evReadResponse = sock.recv(struct.calcsize('@I'))
    retCode = struct.unpack('@I', evReadResponse)[0]
    if retCode & 1:
        # error
        errMsgLen = retCode >> 1
        errMsg = sock.recv(errMsgLen).decode()
        raise RuntimeError(errMsg)
    evDataLen = retCode >> 1
    return sock.recv(evDataLen)

class EventAPI(Resource):
    """Raw event retrieve API"""
    def get(self, runNo, spillNo, eventNo, dataFormat='dat'):
        """
        Exploits app written on C to retrieve particular event using initial
        offset.
        """
        evPickCfg = app.config.get('NA64SW_EVENT_PICKING_PORT', False)
        if not evPickCfg:
            return {'errors': 'Event picking is not supported.'}, 510
        if dataFormat != 'dat':
            # TODO: implement other formats?
            return {'errors': 'Format not implemented.'}
        # Obtain chunk number and closest event's offset, making sure the
        # event ID at least makes any sense.
        # todo: need something similar to equal_range() here; any way to do it
        # more efficiently? See, e.g.:
        #   - https://stackoverflow.com/q/48424970
        #   - https://stackoverflow.com/q/71068535
        lb = db.session.query(EventOffset) \
                .filter( EventOffset.runNo==runNo
                       , EventOffset.spillNo==spillNo
                       , EventOffset.eventNo<=eventNo
                       ) \
                .order_by(EventOffset.eventNo.desc()) \
                .first()
        ub = db.session.query(EventOffset) \
                .filter( EventOffset.runNo==runNo
                       , EventOffset.spillNo==spillNo
                       , EventOffset.eventNo>=eventNo
                       ) \
                .order_by(EventOffset.eventNo.asc()) \
                .first()
        chunkNo, closestOffset = None, None
        # Following cases are possible depending on chunk number:
        # 1. lb < eid < ub (most frequent) both numbers are from the same
        #    chunk. Take the chunk number.
        # 2. Chunk numbers differ, but lb == eid || rb == eid, take the
        #    matching chunk number
        # Other cases should be considered as DB integrity violation since
        # indexed events are always matching the boundaries of the file (first
        # and last event in chunk are always on index)
        if lb is None:
            return {'errors': [f'No preceding event for {runNo}-{spillNo}-{eventNo} on index.'
                ]}, 404
        elif ub is None:
            chunkNo, closestOffset = lb.chunkNo, lb.byteOffset
        elif ub.chunkNo == lb.chunkNo:
            chunkNo, closestOffset = lb.chunkNo, lb.byteOffset
        elif lb.eventNo == eventNo:
            chunkNo, closestOffset = lb.chunkNo, lb.byteOffset
        elif ub.eventNo == eventNo:
            chunkNo, closestOffset = ub.chunkNo, ub.byteOffset
        else:
            return {'errors': [f'Event {runNo}-{spillNo}-{eventNo} is in between' +
                    f' of events {lb.eventNo} and {ub.eventNo} in chunks' +
                    f' #{lb.chunkNo} and #{ub.chunkNo}, but there is no index' +
                    ' data in between.'
                ]}, 404
        #print(f'Found {lb.chunkNo}.{lb.spillNo}-{lb.eventNo}:{lb.byteOffset} //' +
        #      f' {ub.chunkNo}.{lb.spillNo}-{ub.eventNo}:{ub.byteOffset}, selected {chunkNo}')  # XXX
        try:
            evData = get_event_data( (int(runNo), int(spillNo), int(eventNo))
                    , chunkNo, closestOffset
                    , host=app.config.get('NA64SW_EVENT_PICKING_HOST', '')
                    , portNo=app.config.get('NA64SW_EVENT_PICKING_PORT') )
        except Exception as e:
            return {'errors': [str(e)]}, 500
        resp = make_response(evData)
        resp.headers['Content-Type'] = 'application/octet-stream'
        return resp

#
# API Routes

api.add_resource( RunsAPI
                , '/rawdata/index/runs'
                , endpoint='runs'
                )
api.add_resource( RunAPI
                , '/rawdata/index/run/<int:runNo>'
                , endpoint='run'
                )
api.add_resource( SpillAPI
                , '/rawdata/index/run/<int:runNo>/spill/<int:spillNo>'
                , endpoint='spill'
                )
api.add_resource( ChunkAPI
                , '/rawdata/index/run/<int:runNo>/chunk/<int:chunkNo>'
                , endpoint='chunk'
                )
api.add_resource( EventAPI
                , '/rawdata/event/<int:runNo>-<int:spillNo>-<int:eventNo>.<string:dataFormat>'
                , endpoint='event'
                )
app.register_blueprint(api_bp)


#
# Front-end routes
frontend_bp = Blueprint('browse-ddd', __name__)

@frontend_bp.route('/<path:namepath>')
def client_app(namepath):
    """
    Provides client app.
    """
    basePath = os.getenv('DDDSRV_STATIC_FILES_DIR', app.instance_path)
    return send_from_directory(basePath, namepath)

app.register_blueprint(frontend_bp)

#
# ORM
db.init_app(app)

class Run(db.Model):
    """
    A Run is defined as tagged collection of spills.
    Tags are used to mark good/bad runs, certain detector problems and stuff.
    """
    __tablename__ = "runs"
    # key
    runNo   = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=False)
    # inferred
    spills  = db.relationship('Spill',       back_populates='run', order_by='Spill.spillNo')
    chunks  = db.relationship('Chunk',       viewonly=True )
    offsets = db.relationship('EventOffset', viewonly=True ) #back_populates='run')

    @hybrid_property
    def startTime(self):
        """Start timestamp of the earliest spill in run"""
        r = None
        for spill in self.spills:
            if r is None or r > spill.startTimestamp:
                r = spill.startTimestamp
        return r

    @startTime.expression
    def startTime(cls):
        return (select([func.min(Spill.startTimestamp)]).
                    where(Spill.runNo == cls.runNo).
                    label("startTime")
                    )

    @hybrid_property
    def endTime(self):
        """End timestamp of the latest spill in run"""
        r = None
        for spill in self.spills:
            if r is None or r < spill.endTimestamp:
                r = spill.endTimestamp
        return r

    @endTime.expression
    def endTime(cls):
        return (select([func.max(Spill.endTimestamp)]).
                    where(Spill.runNo == cls.runNo).
                    label("endTime")
                    )

    @hybrid_property
    def nEvents(self):
        if not self.spills: return None
        return functools.reduce( operator.add
                               , map(lambda s: s.nEvents if s.nEvents is not None else 0, self.spills)
                               )

    @nEvents.expression
    def nEvents(cls):
        return (select([func.sum(Spill.nEvents)]).
                    where(Spill.runNo == cls.runNo).
                    label("nEvents")
                    )

    @hybrid_property
    def nSpills(self):
        #return len(self.spills)
        return object_session(self).query(Spill).filter(
                  Spill.runNo==self.runNo ).count()

    @nSpills.expression
    def nSpills(self):
        return db.query(Spill).filter(Spill.runNo==self.runNo).count()

    @hybrid_property
    def nOffsets(self):
        #return len(self.offsets)  #< NOTE: slooow
        return object_session(self).query(EventOffset).filter(
                  EventOffset.runNo==self.runNo ).count()

    @nOffsets.expression
    def nOffsets(cls):
        # TODO: check
        return select([func.count(EventOffset.byteOffset)]).where(
                EventOffset.runNo == cls.runNo
                ).label('spillsCount')

    #@hybrid_property
    #def size(self):
    #    """Returns size on record in bytes"""
    #    # ... TODO: have no chunk size info yet

# NOTE: spills with respect to chunks form a many-to-many relation. Chunks
#       usually contain few spills and one spill might be shared across more
#       than one chunk. Code below introduces association table to cope
#       this kind of relation.
chunkSpillAssocTable = Table(
    "chunk_spill_assoc_table",
    db.Model.metadata,
    db.Column("runNo",   db.Integer, primary_key=True),
    db.Column("chunkNo", db.Integer, primary_key=True),
    db.Column("spillNo", db.Integer, primary_key=True),
    #db.Column("runNo",   db.Integer, primary_key=True),
    #db.Column("chunkNo", db.Integer, primary_key=True),
    #db.Column("spillNo", db.Integer, primary_key=True),
    ForeignKeyConstraint(
            ('runNo', 'chunkNo'),
            ('chunks.runNo', 'chunks.chunkNo')
        ),  # ... or? ,
    ForeignKeyConstraint(
            ('runNo', 'spillNo'),
            ('spills.runNo', 'spills.spillNo')
        )
)

class Chunk(db.Model):
    """
    Representation of raw data file (a "chunk" of the run).
    """
    __tablename__ = "chunks"
    # key
    #chunkNo = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=False)
    runNo   = db.Column(db.Integer, db.ForeignKey('runs.runNo'), primary_key=True, nullable=False)
    chunkNo = db.Column(db.Integer, primary_key=True)
    spills = db.relationship('Spill', secondary=chunkSpillAssocTable
                , back_populates='chunks' )
    # props
    size = db.Column(db.Integer)
    md5Hashsum = db.Column(db.String)
    # inferred
    run     = db.relationship('Run',         back_populates='chunks')
    #
    offsets = db.relationship('EventOffset', viewonly=True) #back_populates='chunk')

    @hybrid_property
    def nOffsets(self):
        #return len(self.offsets)  #< NOTE: slooow
        return object_session(self).query(EventOffset).filter(
                  EventOffset.runNo==self.runNo,
                  EventOffset.chunkNo==self.chunkNo ).count()

    @nOffsets.expression
    def nOffsets(cls):
        # TODO: check
        return select([func.count(EventOffset.byteOffset)]).where(
                EventOffset.runNo == cls.runNo,
                EventOffset.chunkNo==self.chunkNo
                ).label('chunksCount')

class Spill(db.Model):
    """
    Spill is defined as tagget collection of event records in chunk with
    start/end timestamps and event stats.
    """
    __tablename__ = "spills"
    # key
    #spillNo = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=False)
    spillNo = db.Column(db.Integer, primary_key=True)
    runNo = db.Column(db.Integer, db.ForeignKey('runs.runNo'), primary_key=True, nullable=False)
    chunks = db.relationship('Chunk'
                , secondary=chunkSpillAssocTable
                #, primaryjoin='and_(Chunk.chunkNo == chunk_spill_assoc_table.c.chunkNo,' \
                #    ' Chunk.runNo == chunk_spill_assoc_table.c.runNo)'
                , primaryjoin='and_(Spill.spillNo == chunk_spill_assoc_table.c.spillNo'
                    ', Spill.runNo == chunk_spill_assoc_table.c.runNo)'
                #, secondaryjoin='and_(Chunk.runNo == chunk_spill_assoc_table.c.runNo'
                #    ', Chunk.chunkNo == chunk_spill_assoc_table.c.chunkNo)'
                #, secondaryjoin=''
                , back_populates='spills'  #< important wrt current runs.patch()
                )
    #chunk   = db.relationship('Chunk',       back_populates='spills')
    # props
    #chunkNo = db.Column(db.Integer, db.ForeignKey('chunks.chunkNo'))
    startTimestamp = db.Column(db.Integer)
    startSubSec = db.Column(db.Integer)
    endTimestamp = db.Column(db.Integer)
    endSubSec = db.Column(db.Integer)
    nEvents = db.Column(db.Integer)
    # inferred
    run             = db.relationship('Run',            back_populates='spills')
    #
    offsets         = db.relationship('EventOffset',    viewonly=True)

    eventTypeCounts = db.relationship('TypeCount',      back_populates='spill')
    triggerCounts   = db.relationship('TriggerCount',   back_populates='spill')
    scalerCounts    = db.relationship('ScalerCount',    back_populates='spill')
    # TODO: scaler counts
    # UniqueConstraint
    __table_args__ = ( UniqueConstraint('runNo', 'spillNo', name='_spill_uc')
                     , )

    @hybrid_property
    def nOffsets(self):
        #return len(self.offsets)  #< NOTE: slooow
        return object_session(self).query(EventOffset).filter(
                  EventOffset.runNo==self.runNo
                , EventOffset.spillNo==self.spillNo ).count()

    @nOffsets.expression
    def nOffsets(cls):
        # TODO: check
        return select([func.count(EventOffset.byteOffset)]).where(
                EventOffset.runNo == cls.runNo, EventOffset.spillNo == cls.spillNo
                ).label('spillsCount')

class EventOffset(db.Model):
    """
    Certain spill's event offset within a certain chunk file.
    """
    __tablename__ = "offsets"
    # key
    runNo   = db.Column(db.Integer, db.ForeignKey('runs.runNo'), primary_key=True, nullable=False)
    spillNo = db.Column(db.Integer, primary_key=True, nullable=False)
    chunkNo = db.Column(db.Integer, primary_key=True, nullable=False)
    eventNo = db.Column(db.Integer, primary_key=True, nullable=False)
    # props
    byteOffset = db.Column(db.Integer, nullable=True)
    # inferred
    run   = db.relationship('Run',   back_populates='offsets')
    spill = db.relationship('Spill', back_populates='offsets', overlaps='run')
    chunk = db.relationship('Chunk', back_populates='offsets', overlaps='run,spill')

    # UniqueConstraint
    __table_args__ = ( UniqueConstraint('runNo', 'chunkNo', 'spillNo', 'eventNo', name='_offset_uc')
                     , ForeignKeyConstraint(
                            ('runNo', 'spillNo'),
                            ('spills.runNo', 'spills.spillNo')
                        )
                     , ForeignKeyConstraint(
                            ('runNo', 'chunkNo'),
                            ('chunks.runNo', 'chunks.chunkNo')
                        )
                     )

class TypeCount(db.Model):
    """
    Event type counter type and value for certain spill.
    """
    __tablename__ = "eventTypePerSpillCounts"
    # key
    runNo   = db.Column(db.Integer, primary_key=True, nullable=False)
    spillNo = db.Column(db.Integer, primary_key=True, nullable=False)
    name    = db.Column(db.String, primary_key=True)
    # props
    value   = db.Column(db.Integer)
    # inferred
    spill = db.relationship('Spill', back_populates='eventTypeCounts')
    __table_args__ = (ForeignKeyConstraint(
            ('runNo', 'spillNo'),
            ('spills.runNo', 'spills.spillNo')
        ), )

class TriggerCount(db.Model):
    """
    A trigger count is unique per spill, for every trigger bit combination met
    in spill.
    """
    __tablename__ = "triggerCountsPerSpill"
    # key
    runNo   = db.Column(db.Integer, primary_key=True, nullable=False)
    spillNo = db.Column(db.Integer, primary_key=True, nullable=False)
    bits    = db.Column(db.Integer, primary_key=True)
    # props
    value   = db.Column(db.Integer)
    # inferred
    spill = db.relationship( 'Spill'
                           , back_populates='triggerCounts'
                           #, foreign_keys=[runNo, spillNo]
                           )
    __table_args__ = (ForeignKeyConstraint(
            ('runNo', 'spillNo'),
            ('spills.runNo', 'spills.spillNo')
        ), )

class ScalerCount(db.Model):
    """
    Scaler counts are produced by dedicated online tool during the data taking
    since 2022. There is no way to get it from raw data file.
    """
    __tablename__ = "scalerCountsPerSpill"
    # key
    runNo   = db.Column(db.Integer, primary_key=True, nullable=False)
    spillNo = db.Column(db.Integer, primary_key=True, nullable=False)
    name    = db.Column(db.String, primary_key=True)
    # props
    value   = db.Column(db.Integer)
    # inferred
    spill = db.relationship('Spill', back_populates='scalerCounts')
    __table_args__ = (ForeignKeyConstraint(
            ('runNo', 'spillNo'),
            ('spills.runNo', 'spills.spillNo')
        ), )

#
# Run app

with app.app_context():
    db.create_all()

if "__main__" == __name__:
    app.run(debug=app.config.get('DEBUG'))

