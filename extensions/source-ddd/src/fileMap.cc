/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <fileMap.hh>

// A dirty kludge to get event header declaration. Who on Earth does the
// private declaration of public-available structs?
// To make the `DaqEvent::Header` declaration available, we define `private`
// keyword to be `public` in `DaqEvent.h` header file. To prevent other headers
// to be affected with this definition, we include them before.
#include <DaqError.h>
#include <Chip.h>
#include <DaqOption.h>
#include <OnlineFilter.h>
#include <TriggerTime.h>
#include <DaqEventsManager.h>
# define private public
# include <DaqEvent.h>
# undef private

# include <algorithm>
# include <fstream>

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace ddd {

# define FULLHDRLENGTH \
    (((sizeof(CS::DaqEvent::Header::HeaderDATE_old)) > (sizeof(CS::DaqEvent::Header::HeaderDATE_36))) ? \
     ((sizeof(CS::DaqEvent::Header::HeaderDATE_old))) : ((sizeof(CS::DaqEvent::Header::HeaderDATE_36))))

const size_t gDDDEventHeaderSize = FULLHDRLENGTH;

// implemented in source.cc, not declared in header because of CS types
namespace aux {

static struct EvTypeCodeEntry {
    CS::DaqEvent::EventType code;
    const char * name;
} _evTypeCodeEntries[] = {
    { CS::DaqEvent::START_OF_RUN        , "START_OF_RUN" },
    { CS::DaqEvent::END_OF_RUN          , "END_OF_RUN" },
    { CS::DaqEvent::START_OF_RUN_FILES  , "START_OF_RUN_FILES" },
    { CS::DaqEvent::END_OF_RUN_FILES    , "END_OF_RUN_FILES" },
    { CS::DaqEvent::START_OF_BURST      , "START_OF_BURST" },
    { CS::DaqEvent::END_OF_BURST        , "END_OF_BURST" },
    { CS::DaqEvent::PHYSICS_EVENT       , "PHYSICS_EVENT" },
    { CS::DaqEvent::CALIBRATION_EVENT   , "CALIBRATION_EVENT" },
    { CS::DaqEvent::END_OF_LINK         , "END_OF_LINK" },
    { CS::DaqEvent::EVENT_FORMAT_ERROR  , "EVENT_FORMAT_ERROR" },
};

static std::unordered_map<std::string, CS::DaqEvent::EventType> _CSEventTypeMap;

///\brief Complementary function for `CS::DaqEvent::stringEventType()`
///
/// Somehow missed in the original lib. Converts string description to numeric
/// code.
int  // CS::DaqEvent::EventType
cs_daq_event_type_code_by_string(const char * nm) {
    if(_CSEventTypeMap.empty()) {
        // Retain permit event types
        for( size_t i = 0
           ; i < sizeof(_evTypeCodeEntries)/sizeof(EvTypeCodeEntry)
           ; ++i ) {
            const EvTypeCodeEntry & ee = _evTypeCodeEntries[i];
            _CSEventTypeMap[ee.name] = ee.code;
        }
    }
    auto it = _CSEventTypeMap.find(nm);
    if( _CSEventTypeMap.end() == it ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unknown event type specification for permission: \"%s\""
                , nm );
        throw std::runtime_error(errbf);
    }
    return it->second;
}
}  // namespace aux

bool
direct_look_up_event( std::istream & is
                    , const na64dp::EventID & eid ) {
    uint8_t buf[FULLHDRLENGTH];
    memset(buf, 0x0, sizeof(buf));
    CS::DaqEvent::Header h(buf);
    for( is.seekg( 0, is.beg );
         is;
         is.seekg( h.GetLength() - sizeof(buf), is.cur ) ) {
        // Read data into buffer associated to header:
        is.read((char *) buf, sizeof(buf));
        if( eid.run_no() == h.GetRunNumber()           \
         && eid.spill_no() == h.GetBurstNumber()       \
         && eid.event_no() == h.GetEventNumberInBurst() ) {
            // Found requested event:
            is.seekg( - sizeof(buf), is.cur );
            return true;
        }
    }
    return false;
}

//
// Index class and its subclasses implementation

size_t
DDDIndex::SpillDataLayout::merge( const SpillDataLayout_t & src ) {
    if( src.chunkNo != chunkNo ) {
        #if 0
        na64ee_raise( mergeMismatch,
                      "Original chunk number: %u, merged: %u.",
                      src.chunkNo, chunkNo );
        #else
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Original chunk number: %u, merged: %u."
                , src.chunkNo, chunkNo);
        throw std::runtime_error(errbf);
        #endif
    }
    if( src.eventsInSpill != eventsInSpill ) {
        #if 0
        na64ee_raise( mergeMismatch,
                      "Original events in spill: %u, merged: %u.",
                      src.eventsInSpill, eventsInSpill );
        #else
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Original events in spill: %u, merged: %u."
                , src.eventsInSpill, eventsInSpill );
        throw std::runtime_error(errbf);
        #endif
    }
    return merge( src.eventPositions );
}

size_t
DDDIndex::SpillDataLayout::merge(
                            const std::map<EventNo_t, FilePostion_t> & src ) {
    size_t nUniq = 0;
    for( std::map<EventNo_t, FilePostion_t>::const_iterator it = src.begin();
                                                    src.end() != it; ++it ) {
        const std::pair<EventNo_t, FilePostion_t> & evlPairIt = *it;
        //auto insertionResult = eventPositions.emplace( evlPairIt.first,
        //                                               evlPairIt.second );
        auto insertionResult = eventPositions.insert( evlPairIt );
        if( insertionResult.second ) {
            ++nUniq;
        }
    }
    return nUniq;
}

std::pair<DDDIndex::EventNo_t, DDDIndex::FilePostion_t>
DDDIndex::SpillDataLayout::get_preceding_event_offset(
                                                    EventNo_t needNo ) const {
    auto uIt = eventPositions.upper_bound( needNo );
    --uIt;
    return *uIt;
}


DDDIndex::DDDIndex() {
}

DDDIndex::DDDIndex( RunNo_t rn, const RunDataLayout & rl ) {
    //_runs.emplace( rn, rl );
    _runs.insert( std::pair<RunNo_t, RunDataLayout>(rn, rl) );
}

DDDIndex::DDDIndex( const DDDIndex & orig ) : _runs( orig._runs.begin(),
                                                     orig._runs.end() ) {
}

size_t
DDDIndex::merge( const DDDIndex & src ) {
    size_t nUniq = 0;
    for( std::map<RunNo_t, RunDataLayout>::const_iterator it = src._runs.begin();
                                            src._runs.end() != it; ++it ) {
        nUniq += merge( it->first, it->second );
    }
    return nUniq;
}

size_t
DDDIndex::merge( RunNo_t rn, const RunDataLayout & lstRef ) {
    size_t nUniq = 0;
    auto targetRunIt = _runs.find( rn );
    if( _runs.end() == targetRunIt) {
        // Entire run not indexed --- just insert entire copy.
        //targetRunIt = _runs.emplace(rn, lstRef).first;
        targetRunIt = _runs.insert( std::pair<RunNo_t, RunDataLayout>(rn, lstRef) ).first;
        // Count number of entries:
        # if __cplusplus > 199711L
        std::for_each( lstRef.begin(), lstRef.end(),
                [ & nUniq ] ( std::remove_reference<decltype(lstRef)>::type::value_type v ) {
                    nUniq += v.second.eventPositions.size();
                } );
        # else
        for( auto it = lstRef.begin(); lstRef.end() != it; ++it ) {
            nUniq += it->second.eventPositions.size();
        }
        # endif
        return nUniq;
    }
    //for( const auto & spillPairIt : lstRef )
    for( auto it = lstRef.begin(); it != lstRef.end(); ++it ) {
        auto & spillPairIt = *it;
        const auto & spillNo = spillPairIt.first;
        auto targetSpillIt = targetRunIt->second.find( spillNo );
        if( targetRunIt->second.end() == targetSpillIt ) {
            // New spill introduced --- just insert a copy.
            //targetRunIt->second.emplace( spillNo, spillPairIt.second );
            targetRunIt->second.insert( std::pair<SpillNo_t, SpillDataLayout>(spillNo, spillPairIt.second) );
            nUniq += spillPairIt.second.eventPositions.size();
        } else {
            // Additional info submitted for existing spill. Merge it.
            nUniq += targetSpillIt->second.merge( spillPairIt.second );
        }
    }
    return nUniq;
}

void
DDDIndex::write( std::ostream & ) const {
}

// aux
class IndexingState {
private:
    class EventReadStruct {
    private:
        // Reading buffer for header struct
        uint8_t _buf[FULLHDRLENGTH];
        // Header struct
        CS::DaqEvent::Header _hdr;
        // Position in a file
        SpillDataLayout_t::FilePostion_t _position;
        // Read?
        bool _read;
    public:
        EventReadStruct() : _buf{0x0}
			  , _hdr(_buf)
                          , _position(0x0)
                          , _read(false)
                          {}
        bool read(std::istream & is) {
            _position = is.tellg();
            is.read((char *) _buf, sizeof(_buf));
            if((_read = (bool) static_cast<std::ifstream&>(is))) {
                // Check for magic number provides non-ideal, yet most of the
                // time efficient control for reading validity
                if( _hdr.GetMagic() != CS::DaqEvent::Header::EVENT_MAGIC_NUMBER ) {
                    _read = false;
                    char errbf[256];
                    snprintf( errbf, sizeof(errbf)
                            , "Magic number for CS::DaqEvent failed at"
                              " position %lu -- wrong event offset assumed or"
                              " file integrity problem."
                            , (unsigned long) _position
                            );
                    throw error::MagicCheckFailed(errbf);
                }
                is.seekg( _hdr.GetLength() - sizeof(_buf), is.cur );
                return true;
            }
            return false;
        }
        bool was_read() const { return _read; }
        SpillDataLayout_t::FilePostion_t position() const {
            assert(_read);
            return _position;
        }
        const CS::DaqEvent::Header header() const {
            assert(_read);
            return _hdr;
        }
    } _evs[2];

    // Debug log (may be null)
    FILE * _dbgf;
    // Ptrs to reading structs
    EventReadStruct * _currentEv
                  , * _prevEv;
    // Layout currently filled
    DDDIndex::RunDataLayout & _lst;
    // Reentrant stat accum struct
    SpillDataLayout_t _csl;

    void _index_event( const EventReadStruct * );
    void _new_spill(DDDIndex::SpillDataLayout::SpillNo_t, size_t nEventsInSpill);
    void _swap_events() { std::swap(_currentEv, _prevEv); }
    void _consider_event(const EventReadStruct *);
public:
    IndexingState( DDDIndex::SpillDataLayout::ChunkNo_t chunkNo
                 , DDDIndex::RunDataLayout & lst
                 , FILE * dbgStream )
            : _dbgf(dbgStream)
            , _currentEv(_evs + 1)
            , _prevEv(_evs)
            , _lst(lst)
            {
        _csl.chunkNo = chunkNo;
    }

    void do_index(size_t duty, std::istream & is);
};

void
IndexingState::_consider_event(const EventReadStruct * evs) {
    const auto & h = evs->header();
    {  // event type stats
        auto it = _csl.eventTypeCounts.emplace(h.GetEventType(), 0).first;
        ++(it->second);
    }
    {  // trigger stats
        auto it = _csl.triggerCounts.emplace(h.GetTrigger(), 0).first;
        ++(it->second);
    }
    if(h.GetTime().first) {
        // update logical timestamps
        _csl.lastEventTime = h.GetTime();
        if(!_csl.firstEventTime.first) {
            _csl.firstEventTime = h.GetTime();
        }
        // update physical timestamps
        if(h.GetEventType() == CS::DaqEvent::PHYSICS_EVENT) {
            _csl.lastPhysEventTime = h.GetTime();
            if(!_csl.firstPhysEventTime.first) {
                _csl.firstPhysEventTime = h.GetTime();
            }
        }
    }
}

void
IndexingState::_new_spill( DDDIndex::SpillDataLayout::SpillNo_t spillNo
                         , size_t nEventsInSpill ) {
    if(!_csl.eventPositions.empty()) {
        _csl.eventsInSpill = nEventsInSpill;
        auto ir = _lst.insert(DDDIndex::RunDataLayout::value_type(spillNo, _csl));
        assert(ir.second);
        if(_dbgf) {
            fprintf( _dbgf
                   , "  Spill index #%d:%d memorized with %zu events (%zu offsets).\n"
                   , (int) spillNo
                   , (int) _csl.chunkNo
                   , nEventsInSpill
                   , _csl.eventPositions.size()
                   );
        }
        assert(ir.first->second.eventPositions.size() == _csl.eventPositions.size());
    }

    _csl.eventPositions.clear();
    _csl.eventTypeCounts.clear();
    _csl.triggerCounts.clear();
    _csl.firstEventTime = _csl.lastEventTime = {0, 0};
    _csl.firstPhysEventTime = _csl.lastPhysEventTime = {0, 0};
    if(_dbgf) fprintf(_dbgf, "  Spill data layout reset.\n");
}

void
IndexingState::_index_event( const EventReadStruct * ers ) {
    assert(ers->was_read());
    _csl.eventPositions.insert(
            std::pair<DDDIndex::RunNo_t, DDDIndex::FilePostion_t>(
                        ers->header().GetEventNumberInBurst(),
                        ers->position()
                    )
        );
    if(_dbgf)
        fprintf( _dbgf, "    event #%d-%d-%d added to index at offset %lu.\n"
               , (int) ers->header().GetRunNumber()
               , (int) ers->header().GetBurstNumber()
               , (int) ers->header().GetEventNumberInBurst()
               , (long unsigned) ers->position()
               );
}

void
IndexingState::do_index( size_t duty
                       , std::istream & is
                       ) {
    size_t eventOrderlyCount = 0;
    // Event indexing will be triggered in following cases:
    //  1. no previous event => index current, swap, proceed
    //  2. cur/prev have different run/spill IDs => index current and prev, swap, proceed
    //  3. duty condition reached => index current, swap, proceed
    //  4. no current event read => index previous, if exists, exit
    if(_dbgf) fprintf(_dbgf, "Entering loop...\n");
    for(;;) {
        if(!_currentEv->read(is)) {  // Read new event, check Condition #4 (read failed)
            if(_dbgf) fprintf(_dbgf, "Failed to read data (EOF?); loop exit condition triggered.\n");
            if(_prevEv->was_read()) {
                _index_event(_prevEv);
                _new_spill(_prevEv->header().GetBurstNumber(), eventOrderlyCount);
            }
            break;
        }
        if( _currentEv->header().GetEventType() != CS::DaqEvent::PHYSICS_EVENT
         && _dbgf ) {
            char buf[32];
            time_t timeCur  = _currentEv->header().GetTime().first
                 ;
            strftime( buf, sizeof(buf)
                    , "%Y-%m-%d %H:%M:%S", localtime(&timeCur)
                    );
            fprintf(_dbgf, "  Non-physical event #%d-%d-%d at %lu, timestamp %s, type %s.\n"
                , (int) _currentEv->header().GetRunNumber()
                , (int) _currentEv->header().GetBurstNumber()
                , (int) _currentEv->header().GetEventNumberInBurst()
                , (long unsigned) _currentEv->position()
                , buf
                , CS::DaqEvent::stringEventType(_currentEv->header().GetEventType()).c_str()
                );
        }
        ++eventOrderlyCount;
        // here was a BUG, affects DB index before 2023: this is a wrong place
        // for _consider_event() as it affects the CURRENT spill even if this
        // event belongs to next.
        //_consider_event(_currentEv);  // XXX
        if(!_prevEv->was_read()) {  // Condition #1 (first event in a file)
            if(_dbgf) fprintf(_dbgf, "First event read condition triggered.\n");
            //assert(0 == _currentEv->position());  // we're at file's beginning
            assert(1 == eventOrderlyCount);
            _new_spill(_currentEv->header().GetBurstNumber(), 0);
            _index_event(_currentEv);
            _consider_event(_currentEv);
            _swap_events();
            continue;
        }
        // Condition #2 (spill or/and run's boundary)
        if( _prevEv->header().GetBurstNumber() != _currentEv->header().GetBurstNumber()
         || _prevEv->header().GetRunNumber() != _currentEv->header().GetRunNumber() ) {
            if(_dbgf) {
                char bufCur[32], bufPrev[32];
                time_t timeCur  = _currentEv->header().GetTime().first
                     , timePrev =    _prevEv->header().GetTime().first
                     ;
                strftime( bufCur, sizeof(bufCur)
                        , "%Y-%m-%d %H:%M:%S", localtime(&timeCur)
                        );
                strftime( bufPrev, sizeof(bufPrev)
                        , "%Y-%m-%d %H:%M:%S", localtime(&timePrev)
                        );
                fprintf(_dbgf, "  Spill/run boundary condition triggered"
                    " with event at #%d-%d-%d at %lu, timestamp %s, type %s;"
                    " previous event was #%d-%d-%d at %lu, timestamp %s, type %s.\n"
                    , (int) _currentEv->header().GetRunNumber()
                    , (int) _currentEv->header().GetBurstNumber()
                    , (int) _currentEv->header().GetEventNumberInBurst()
                    , (long unsigned) _currentEv->position()
                    , bufCur
                    , CS::DaqEvent::stringEventType(_currentEv->header().GetEventType()).c_str()
                    , (int) _prevEv->header().GetRunNumber()
                    , (int) _prevEv->header().GetBurstNumber()
                    , (int) _prevEv->header().GetEventNumberInBurst()
                    , (long unsigned) _prevEv->position()
                    , bufPrev
                    , CS::DaqEvent::stringEventType(_prevEv->header().GetEventType()).c_str()
               );
            }
            _index_event(_prevEv);
            _consider_event(_prevEv);
            --eventOrderlyCount;
            _new_spill(_prevEv->header().GetBurstNumber(), eventOrderlyCount);
            eventOrderlyCount = 1;
            _index_event(_currentEv);
            _consider_event(_currentEv);
            _swap_events();
            continue;
        }
        if( !(eventOrderlyCount%duty) ) {  // Condition #3 (duty)
            if(_dbgf) fprintf(_dbgf, "  Duty condition triggered (#%zu).\n", eventOrderlyCount);
            _index_event(_currentEv);
        }
        _consider_event(_currentEv);
        _swap_events();
    }
}

/** Performs sequential reading of events in given stream. Will fill the list
 * instance with spill (called burst in DDD) information.
 *
 * Accepts a "duty" parameter meaning that only each N event will be cached.
 * One can safely set it to 0, however it will lead to large indexing objects.
 *
 * Default chunk number value affects nothing except the initial value of all
 * the chunkNo fields in SpillDataLayout instances. There is no way to validate
 * this number from inside this function.
 *
 * Does not initialize sentry to stream begin --- reading will be continued
 * from current sentry position.
 * */
void
DDDIndex::index_chunk( std::istream & is,
                       RunDataLayout & lst,
                       size_t nDuty,
                       RunNo_t expectedRunNo,
                       ChunkNo_t defaultChunkNo,
                       FILE * dbgStream ) {
    #if 1
    IndexingState state( defaultChunkNo
                       , lst
                       , dbgStream
                       );
    state.do_index(nDuty, is);
    #else
    uint8_t buf[FULLHDRLENGTH];
    CS::DaqEvent::Header h(buf);
    IndexingState state(lst);
    size_t nEventsRead = 0,
           cPosition = is.tellg();
    SpillNo_t lastSpill = 0;
    SpillDataLayout csl;  // current spill layout
    csl.chunkNo = defaultChunkNo;
    EventNo_t eventsInSpill = 0;
    bool spillNoInitialized = false;
    for( ; is;
           eventsInSpill++,
           nEventsRead++,
           is.seekg( h.GetLength() - sizeof(buf), is.cur ) ) {
        cPosition = is.tellg();
        // Read data into buffer associated to header:
        is.read((char *) buf, sizeof(buf));
        if( !spillNoInitialized ) {
            lastSpill = h.GetBurstNumber();
            spillNoInitialized = true;
        }
        //std::cout << " xxx " << h.GetBurstNumber() << std::endl;  // XXX
        // New event will be cached in cases:
        //  - 0 == nEventsRead%nDuty (duty reached)
        //  - lastSpill != <current spill #> (new spill starts)
        //  - target list is empty
        if( lastSpill != h.GetBurstNumber() ) {
            if( !csl.eventPositions.empty() ) {
                csl.eventsInSpill = eventsInSpill - 1;
                //lst.emplace( lastSpill, csl );
                lst.insert( RunDataLayout::value_type(lastSpill, csl) );
                if( dbgStream ) {
                    fprintf( dbgStream, "New spill %d (%d memorized with %zu "
                            "entries indexing %u events).\n",
                            h.GetBurstNumber(),
                            lastSpill,
                            csl.eventPositions.size(),
                            csl.eventsInSpill );
                }
                lastSpill = h.GetBurstNumber();
                eventsInSpill = 0;
            } else if( dbgStream ) {
                fprintf( dbgStream, "Empty spill ignored.\n" );
            }
            csl.eventPositions.clear();
            csl.eventTypeCounts.clear();
            csl.triggerCounts.clear();
            csl.firstEventTime = csl.lastEventTime = {0, 0};
        }
        if( 0 == nEventsRead
         || 0 == nDuty
         || !(nEventsRead%nDuty) ) {
            if( expectedRunNo != h.GetRunNumber() ) {
                #if 0
                na64ee_raise( badRunNumber, "Expected run number %u while "
                    "%zu-th event belongs to run %d.",
                    expectedRunNo, nEventsRead, h.GetRunNumber() );
                #else
                char errbf[128];
                snprintf(errbf, sizeof(errbf)
                        , "Expected run number %u while "
                          "%zu-th event belongs to run %d."
                        , expectedRunNo, nEventsRead, h.GetRunNumber() );
                throw std::runtime_error(errbf);
                #endif
            }
            if( dbgStream ) {
                fprintf( dbgStream, "Cached event %zu (%u in burst)"
                         " by offset %#010lx.\n",
                         nEventsRead, h.GetEventNumberInBurst(), cPosition );
            }
            //csl.eventPositions.emplace( h.GetEventNumberInBurst(), cPosition );
            csl.eventPositions.insert( std::pair<RunNo_t, FilePostion_t>(
                                               h.GetEventNumberInBurst(), cPosition ) );
        }
        // done for every event
        {  // event type stats
            CS::DaqEvent::EventType type = h.GetEventType();
            auto it = csl.eventTypeCounts.emplace(type, 0).first;
            ++it->second;
        } {  // trigger stats
            auto it = csl.triggerCounts.emplace(h.GetTrigger(), 0).first;
            ++it->second;
        }
        if(h.GetTime().first) {
            csl.lastEventTime = h.GetTime();
            if(!csl.firstEventTime.first) {
                csl.firstEventTime = h.GetTime();
            }
        }
    }
    #endif
}

void
DDDIndex::print_to_ostream( std::ostream & os ) const {
    //for( const auto & runIt : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        const auto & runIt = *it;
        os << "Run #" << runIt.first << ":" << std::endl;
        //for( const auto & spillIt : runIt.second )
        for( auto iit = runIt.second.begin(); runIt.second.end() != iit; ++iit ) {
            const auto & spillIt = *iit;
            const SpillDataLayout & csl = spillIt.second;
            os << "  spill #" << spillIt.first
               << " stored in chunk " << (unsigned short) csl.chunkNo
               << " has " << csl.eventPositions.size()
               << " entries, indexing " << csl.eventsInSpill
               << " events, starting from " << csl.eventPositions.begin()->first
               << std::endl;
               // note that this information does not reflect the fact that
               // spills are eventaully written not from first event, but
               // starting from some number after. However, the number of
               // events in spill is correct and refers to actual number of
               // events written in spill.
        }
    }
}

///TODO: pretty print option should strip the
/// spaces.
void
DDDIndex::to_JSON_str( std::ostream & os/*, bool prettyPrint*/ ) const {
    size_t ncol;  // aux
    char dlm[3] = {' ', ' ', ' '};  // aux
    os << "{ \"runs\" : [";
    //for( const auto & runIt : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        const auto & runIt = *it;
        os << dlm[0] << std::endl
           << std::string(4, ' ') << "{"
           << std::endl;
        os << std::string(8, ' ') << "\"runNo\" : " << runIt.first << "," << std::endl
           << std::string(8, ' ') << "\"spills\" : [";
        //for( const auto & spillIt : runIt.second )
        for( auto iit = runIt.second.begin(); runIt.second.end() != iit; ++iit ) {
            const auto & spillIt = *iit;
            const SpillDataLayout & csl = spillIt.second;
            os << dlm[1] << std::endl
               << std::string(12, ' ') << "{" << std::endl
               << std::string(16, ' ') << "\"spillNo\" : " << spillIt.first
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"nEvents\" : " << csl.eventsInSpill
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"chunkNo\" : " << (int) csl.chunkNo
                                                           << "," << std::endl
               << std::string(16, ' ') << "\"typeCounts\" : {";
            bool isFirst = true;
            for(auto typeCount: csl.eventTypeCounts) {
                os  << (isFirst ? "\n" : ",\n")
                    << std::string(24, ' ') << "\""
                    << CS::DaqEvent::stringEventType(CS::DaqEvent::EventType(typeCount.first))
                    << "\" : " << typeCount.second;
                isFirst = false;
            }
            os << std::endl << std::string(16, ' ') << "}," << std::endl
               << std::string(16, ' ') << "\"triggerCounts\" : {";
            isFirst = true;
            for(auto triggerCount: csl.triggerCounts) {
                char bf[64];
                snprintf(bf, sizeof(bf), "%x", triggerCount.first);
                os  << (isFirst ? "\n" : ",\n")
                    << std::string(24, ' ') << "\"" << bf
                    << "\" : " << triggerCount.second;
                isFirst = false;
            }

            os << std::endl << std::string(16, ' ') << "}," << std::endl
               << std::string(16, ' ') << "\"start\" : ["
                    << csl.firstEventTime.first << ", "
                    << csl.firstEventTime.second << "]," << std::endl
               << std::string(16, ' ') << "\"end\" : ["
                    << csl.lastEventTime.first << ", "
                    << csl.lastEventTime.second << "]," << std::endl
               << std::string(16, ' ') << "\"physStart\" : ["
                    << csl.firstPhysEventTime.first << ", "
                    << csl.firstPhysEventTime.second << "]," << std::endl
               << std::string(16, ' ') << "\"physEnd\" : ["
                    << csl.lastPhysEventTime.first << ", "
                    << csl.lastPhysEventTime.second << "]," << std::endl;
            os << std::string(16, ' ') << "\"offsets\" : ["
               ;
            ncol = 0;
            //for( const auto & evpIt : csl.eventPositions )
            for( auto iiit = csl.eventPositions.begin(); csl.eventPositions.end() != iiit; ++iiit ) {
                const auto & evpIt = *iiit;
                if( ncol ) {
                    os << dlm[2] << " ";
                }
                if( !(ncol%4) ) {
                    os << std::endl
                       << std::string(20, ' ');
                       ;
                }
                os << "[" << evpIt.first << ", "
                   //<< std::hex << "\""
                   << evpIt.second
                   //<< "\"" << std::dec
                   << "]"
                   ;
                ++ncol;
                dlm[2] = ',';
            }
            os << std::endl
               << std::string(16, ' ') << "]" << std::endl
               << std::string(12, ' ') << "}"
               ;
            dlm[1] = ',';
        }
        os << std::endl
           << std::string(8, ' ') << "]" << std::endl
           << std::string(4, ' ') << "}"
           ;
        dlm[0] = ',';
    }
    os << std::endl << "] }"
       << std::endl;
}

DDDIndex
DDDIndex::from_JSON_str( const std::string & is ) {
    #if 1
    YAML::Node root(is);
    // ...jsut check few things as it anyways generated automatically usually
    if(!root.IsMap())
        throw std::runtime_error("Root node is not a map.");
    if((!root["runs"]) || (!root["runs"].IsSequence()) )
        throw std::runtime_error("\"runs\" node is not a sequence.");
    DDDIndex index;
    for(auto runNode : root["runs"]) {
        RunNo_t runNo = runNode["runNo"].as<int>();
        DDDIndex::RunDataLayout runLayout;
        for(auto spillNode : runNode["spills"]) {
            SpillDataLayout_t sl;
            SpillNo_t spillNo = spillNode["spillNo"].as<int>();
            sl.eventsInSpill = spillNode["nEvents"].as<size_t>();
            sl.chunkNo = spillNode["chunkNo"].as<int>();
            // type counts
            for(auto typeCountItem : spillNode["typeCounts"] ) {
                const std::string typeCountString
                    = typeCountItem.first.as<std::string>();
                size_t typeCount = typeCountItem.second.as<size_t>();
                CS::DaqEvent::EventType evType
                    = (CS::DaqEvent::EventType) aux::cs_daq_event_type_code_by_string(typeCountString.c_str());
                sl.eventTypeCounts.emplace(evType, typeCount);
            }
            // trigger counts
            for(auto triggerCountItem : spillNode["triggerCounts"]) {
                sl.triggerCounts.emplace( triggerCountItem.first.as<uint32_t>()
                        , triggerCountItem.second.as<size_t>() );
            }
            // spill time span
            sl.firstEventTime = std::pair<time_t, uint32_t>(
                    spillNode["start"][0].as<time_t>(), spillNode["start"][1].as<uint32_t>());
            sl.lastEventTime = std::pair<time_t, uint32_t>(
                    spillNode["end"][0].as<time_t>(), spillNode["end"][1].as<uint32_t>());
            // offsets
            for(auto offsetItem : spillNode["offsets"]) {
                sl.eventPositions.emplace( offsetItem[0].as<EventNo_t>()
                                         , offsetItem[1].as<long long unsigned int>()  // FilePostion_t
                                         );
            }
            runLayout.emplace(spillNo, sl);
        }
        index._runs.emplace(runNo, runLayout);
    }
    return index;
    #else
    std::istringstream iss(is);
    YAML::Parser parser(iss);
    YAML::Node root;
    while(parser.GetNextDocument(root)) {
       // ...
    }
    #endif
}

DDDIndex::FilePostion_t
DDDIndex::get_event_stream_pos( std::istream & is
                              , na64sw_EventID_t eid_
                              , FILE * dbgStream ) const {
    EventID eid(eid_);
    // Reset stream first:
    is.clear();  // clear EOF flag
    //is.seekg( 0, is.beg );
    // Get nearest event id:
    auto runIdxContainer = _runs.find( eid.run_no() );
    if( _runs.end() == runIdxContainer ) {
        #if 0
        na64ee_raise( eventNotFound, "Event ID suggests run number %u which "
            "is not found in index %p.", eid.layout.runNo, this );
        #else
        char errbf[128];
        snprintf(errbf, sizeof(errbf)
                , "Event ID suggests run number %u which "
                  "is not found in index %p.", eid.run_no(), this );
        throw std::runtime_error(errbf);
        #endif
    }
    const RunDataLayout & spillMap = runIdxContainer->second;
    RunDataLayout::const_iterator spillIt;
    SpillNo_t sno = eid.spill_no();
    {
        spillIt = spillMap.find( sno );
        if( spillMap.end() == spillIt ) {
            #if 0
            na64ee_raise( eventNotFound, "Event ID suggests spill number %u "
                "which is not found in index %p indexing run %u.",
                sno, this, eid.layout.runNo );
            #else
            char errbf[128];
            snprintf(errbf, sizeof(errbf)
                    , "Event ID suggests spill number %u "
                      "which is not found in index %p indexing run %u.",
                      sno, this, eid.run_no() );
            throw std::runtime_error(errbf);
            #endif
        }
    }
    EventNo_t evno = eid.event_no();
    auto naeraestPrecPosPair = spillIt
                               ->second.get_preceding_event_offset( evno );
    if( dbgStream ) {
        fprintf( dbgStream,
                 "Got nearest ev #%u\n",
                  naeraestPrecPosPair.first );
    }
    // If event number matches perfectly, just return cached position:
    if( evno == naeraestPrecPosPair.first ) {
        return naeraestPrecPosPair.second;
    }
    // Set sentry to nearest preceding event and read headers until requested
    // event number will not be reached.
    is.seekg( naeraestPrecPosPair.second, is.beg );
    {
        uint8_t buf[FULLHDRLENGTH];
        CS::DaqEvent::Header h(buf);
        do {
            is.read((char *) buf, sizeof(buf));
            if( evno == h.GetEventNumberInBurst() ) {
                is.seekg( - sizeof(buf), is.cur );
                return is.tellg();
            } else if(dbgStream) {
                fprintf( dbgStream, "Omitting ev #%u\n",
                    (EventNo_t) h.GetEventNumberInBurst() );
            }
            is.seekg( h.GetLength() - sizeof(buf), is.cur );
        } while( is && h.GetEventNumberInBurst() < evno
                    && h.GetBurstNumber() == sno
                    && h.GetRunNumber() == eid.run_no() );
        if( dbgStream ) {
            fprintf( stdout, "Traversal loop exit conditions: "
                             "%d < %d && %d == %d && %d == %d "
                             "violated.\n",
                        (int) h.GetEventNumberInBurst(), (int) evno,
                        (int) h.GetBurstNumber(), (int) sno,
                        (int) h.GetRunNumber(), (int) eid.run_no() );
        }
    }
    #if 0
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic push
    # pragma GCC diagnostic ignored "-Wformat"
    # pragma GCC diagnostic ignored "-Wformat-extra-args"
    # endif
    na64ee_raise( eventNotFound, "Event ID %" fmt_EVENTID
                                 " not found in stream.", eid.numeric );
    # if __STDC_VERSION__ >= 201112L
    # pragma GCC diagnostic pop
    # endif
    #else
    char errbf[128];
    snprintf( errbf, sizeof(errbf), "Event ID %s not found in the stream."
            , eid.to_str().c_str() );
    throw std::runtime_error(errbf);
    #endif
}


/* NOTE:
 * I was concerned about `legitimate' conversion for pos_type and asked people
 * at stackoverlow for details:
 *  http://stackoverflow.com/questions/41257958/how-one-can-safely-serialize-stdbasic-istreamcharpos-type
 * Apparently there is nothing useful in this struct besides just an offset
 * value that can be safely (de)serialized from within std::streamoff type.
 */

size_t
DDDIndex::serialized_size() const {
    const size_t nBytesPerPosT = sizeof( std::streamoff );
    size_t nBytes = 2*sizeof(uint32_t);
    //for( auto & rp : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        auto & rp = *it;
        nBytes += sizeof( RunNo_t );
        nBytes += sizeof( uint32_t );  // for number of spills in run
        //for( auto & sl : rp.second )
        for( auto iit = rp.second.begin(); iit != rp.second.end(); ++iit ) {
            auto & sl = *iit;
            nBytes += sizeof( SpillNo_t )
                    + sizeof( ChunkNo_t )
                    + sizeof( EventNo_t )
                    + sizeof( uint32_t )  // for number of entries
                    ;
            nBytes += (nBytesPerPosT +
                            sizeof(EventNo_t))*sl.second.eventPositions.size();
        }
    }
    return nBytes;
}


size_t
DDDIndex::serialize( uint8_t * bytes, size_t bytesLength ) const {
    uint32_t & nBytesUsed = *reinterpret_cast<uint32_t *>(bytes);
    nBytesUsed = sizeof( uint32_t );
    uint8_t * c = bytes + sizeof(uint32_t);
    # define wrt_ssq( type, value )                                     \
    nBytesUsed += sizeof(type);                                         \
    if( nBytesUsed > bytesLength ) {                                    \
        throw std::runtime_error("Serialization buffer has insufficient length.");  \
    }                                                                   \
    *reinterpret_cast<type *>(c) = value;                               \
    c += sizeof(type);
    wrt_ssq( uint32_t, (uint32_t) _runs.size() );
    //for( auto & rp : _runs )
    for( auto it = _runs.begin(); _runs.end() != it; ++it ) {
        auto & rp = *it;
        wrt_ssq( RunNo_t, rp.first );
        wrt_ssq( uint32_t, rp.second.size() );
        //for( auto & sl : rp.second )
        for( auto iit = rp.second.begin(); rp.second.end() != iit; ++iit ) {
            auto & sl = *iit;
            wrt_ssq( SpillNo_t, sl.first );
            wrt_ssq( ChunkNo_t, sl.second.chunkNo );
            wrt_ssq( EventNo_t, sl.second.eventsInSpill );
            wrt_ssq( uint32_t,  ((uint32_t) sl.second.eventPositions.size()) );
            //for( auto & entry : sl.second.eventPositions )
            for( auto iiit = sl.second.eventPositions.begin();
                               sl.second.eventPositions.end() != iiit; ++iiit ) {
                const auto & entry = *iiit;
                wrt_ssq( EventNo_t, entry.first );
                wrt_ssq( std::streamoff, ((std::streamoff) entry.second) );
            }
        }
    }
    return nBytesUsed;
    # undef wrt_ssq
}

DDDIndex *
DDDIndex::deserialize( const uint8_t * bytes ) {
    DDDIndex * instancePtr = new DDDIndex();
    DDDIndex & idx = *instancePtr;

    const uint32_t & bytesLength = *reinterpret_cast<const uint32_t *>(bytes);
    const uint8_t * c = bytes + sizeof(uint32_t);
    size_t nBytesUsed = sizeof(uint32_t);

    # define _rdv_ssq( type, value )                                        \
    nBytesUsed += sizeof(type);                                             \
    if( nBytesUsed > bytesLength ) {                                        \
        throw std::runtime_error("Serialization buffer has insufficient length.");  \
    }

    # define rdv_ssq( type, value ) _rdv_ssq(type, value)                   \
    const type & value = *reinterpret_cast<const type *>( c );              \
    c += sizeof(type);

    # define rev_ssq( type, value ) _rdv_ssq(type, value)                   \
    value = *reinterpret_cast<const type *>( c );                           \
    c += sizeof(type);

    rdv_ssq( uint32_t, nRuns );
    for( uint32_t nRun = 0; nRun < nRuns; ++nRun ) {
        rdv_ssq( RunNo_t, runNo );
        rdv_ssq( uint32_t, nSpills );
        RunDataLayout runLt;
        for( uint32_t nSpill = 0; nSpill < nSpills; ++nSpill ) {
            rdv_ssq( SpillNo_t, spillNo );
            SpillDataLayout spl;
            rev_ssq( ChunkNo_t, spl.chunkNo );
            rev_ssq( EventNo_t, spl.eventsInSpill );
            rdv_ssq( uint32_t,  nEntries );
            for( uint32_t nEvent = 0; nEvent < nEntries; ++nEvent ) {
                rdv_ssq( EventNo_t,         evInSpNo );
                rdv_ssq( std::streamoff,    offsetVal );
                //spl.eventPositions.emplace( evInSpNo, offsetVal );
                spl.eventPositions.insert( std::pair<SpillNo_t, FilePostion_t>(evInSpNo, offsetVal) );
            }
            //runLt.emplace( spillNo, spl );
            runLt.insert( RunDataLayout::value_type(spillNo, spl) );
        }
        //idx._runs.emplace( runNo, runLt );
        idx._runs.insert( std::pair<RunNo_t, RunDataLayout>(runNo, runLt) );
    }

    return instancePtr;
}

# if 0
/** This function performs direct reading of certain event pointed out by ID.
 * It is not recommended for practical usage since it will traverse the entire
 * file reading headers of each event until required one will be reached.
 *
 * The serialized DDD-events has headers describing event size, so there is no
 * need to read out entire event to figure out its size. Even so, the reading
 * all the headers of events preceeding to interesting one can be quite
 * time-consuming operation so one has to utilize advanced techniques.
 * */
void read_event(
        std::istream &,
        const EventID &,
        CS::DaqEvent & ) {
    // ... TODO
}
# endif

std::ostream &
operator<<(std::ostream & os, const ChunkID & sid) {
    os << "{" << "run#" << (int) sid.runNo << ", chunk#"
                        << (int) sid.chunkNo << "}";
    return os;
}

#if 0
std::vector<uint8_t>
read_one_new_raw_event( const std::string & filePath,
                        const DDDIndex & idx,
                        EventID eid ) {
    std::ifstream is( filePath );
    DDDIndex::FilePostion_t evPos
                          = idx.get_event_stream_pos( is, eid );
    if( !is ) {
        char errbf[128];
        snprintf(errbf, sizeof(errbf), "Unable to open file \"%s\".", filePath.c_str());
    }
    is.seekg( evPos, is.beg );
    CS::DaqEvent * evPtr;
    try {
        evPtr = new CS::DaqEvent( is );
    } catch( CS::DaqEvent::ExceptionEndOfStream & e ) {
        #if 0
        # if __STDC_VERSION__ >= 201112L
        # pragma GCC diagnostic push
        # pragma GCC diagnostic ignored "-Wformat"
        # pragma GCC diagnostic ignored "-Wformat-extra-args"
        # endif
        na64ee_raise( fileNotFound, "Unable to find event " fmt_EVENTID " in \"%s\": %s.",
                eid.numeric,
                filePath.c_str(),
                e.what() );
        # if __STDC_VERSION__ >= 201112L
        # pragma GCC diagnostic pop
        # endif
        #else
        char errbf[128];
        snprintf( errbf, sizeof(errbf), "Can't read event %s in file \"%s\": %s"
                , eid.to_str().c_str(), filePath.c_str(), e.what() );
        #endif
    }
    return std::vector<uint8_t>(  (unsigned char *) evPtr->GetBuffer(),
                                 ((unsigned char *) evPtr->GetBuffer()) + evPtr->GetLength());
}
#endif

std::vector<uint8_t>
pick_event( std::istream & is
          , EventID eid
          ) {
    std::vector<uint8_t> res;
    //if(!is) {
    //    char errbf[128];
    //    snprintf( errbf, sizeof(errbf), "Failed to read file at position %lu."
    //            , (unsigned long) offset );
    //    throw std::runtime_error(errbf);
    //}
    uint8_t buf[FULLHDRLENGTH];
    CS::DaqEvent::Header h(buf);
    for( ; ; is.seekg( h.GetLength() - sizeof(buf), is.cur ) ) {
        auto cPos = is.tellg();
        is.read((char*) buf, sizeof(buf));
        if(!is) throw error::SourceDepleted();  // file seem depleted
        if( h.GetMagic() != CS::DaqEvent::Header::EVENT_MAGIC_NUMBER ) {
            char errbf[256];
            snprintf( errbf, sizeof(errbf)
                    , "Magic number for CS::DaqEvent failed at"
                      " position %lu -- wrong event offset assumed or"
                      " file integrity problem."
                    , (unsigned long) cPos
                    );
            throw error::MagicCheckFailed(errbf);
        }
        EventID thisEID(h.GetRunNumber(), h.GetBurstNumber(), h.GetEventNumberInBurst());
        if(thisEID != eid) continue;
        // that's it -- put header to vector and concat rest of the event data
        res.resize(h.GetLength(), 0x0);
        std::copy(std::begin(buf), std::end(buf), res.begin());
        is.read( reinterpret_cast<char*>(res.data() + sizeof(buf))
               , h.GetLength() - sizeof(buf) );
        if(!is) {
            // failed to read rest of the event
            char errbf[128];
            snprintf( errbf, sizeof(errbf), "Failed to read rest of the event"
                    " after header of the event was read at position %lu."
                    , (unsigned long) cPos );
            throw error::SourceDepleted(errbf);
        }
        return res;
    }
}

}  // namespace ::na64dp::ddd
}  // namespace na64dp


