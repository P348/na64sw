#include "source.hh"

#include "chipTraits.hh"
#include "na64calib/evType.hh"
#include "na64detID/TBName.hh"
#include "na64detID/TBNameErrors.hh"
#include "na64util/str-fmt.hh"
#include "fileMap.hh"

#include <limits>
#include <log4cpp/Priority.hh>
#include <stdexcept>
#include <fstream>  // to read external files like with event IDs
#include <unistd.h>

namespace na64dp {

namespace errors {

HitCreationError::HitCreationError( const char * details
                                  , const CS::Chip::Digit & dgt
                                  , DetID_t did_
                                  ) throw()
        : GenericRuntimeError(details)
        , did(did_)
        {
    try {
        daqName = dgt.GetDetID().GetName();
        {
            std::ostringstream oss;
            oss << dgt.GetDetID();
            daqVerbose = oss.str();
        }
    } catch(...) {
        std::cerr << "A second exception arose while extracting details for"
            " CS::Chip::Digit. Error details will be incomplete."
            << std::endl;
    }
}

const char *
HitCreationError::what() const throw() {
    snprintf( errBf, sizeof(errBf)
            , "%s Details: [in]complete na64sw::DetID=%#x,"
              " daqName=\"%s\", CS::DetID::Print(): %s."
            , GenericRuntimeError::what()
            , did
            , daqName.c_str()
            , daqVerbose.c_str()
            );
    return errBf;
}

}  // namespace ::na64dp::errors

namespace ddd {

DDDEventSource::DDDEventSource( calib::Manager & mgr
                              //, const std::string & coralMapsDir
                              //, const std::vector<std::string> & inputs
                              , std::vector<std::string> permittedEvTypes
                              , const std::set<std::string> & tbnames2ignore
                              , log4cpp::Category & logCat
                              , const std::string & detNamingClass
                              , iEvProcInfo * epi
                              , std::function<bool(EventID)> filterEventsByID
                              ) : AbstractNameCachedEventSource(mgr, logCat, detNamingClass, epi)
                                , calib::Handle<EventBitTags>("default", mgr)
                                , _decodingFailures(0)
                                , _tbnames2ignore(tbnames2ignore)
                                , _filterEventsByID(filterEventsByID)
                                , _serviceTypeCode(0x0)
                                , _physicsTypeCode(0x0)
                                {
    // Retain permit event types
    for( auto strType : permittedEvTypes ) {
        _permitTypes.insert(
                (CS::DaqEvent::EventType) aux::cs_daq_event_type_code_by_string(strType.c_str()) );
    }
}

void
DDDEventSource::_event_fill( event::Event & e, event::LocalMemory & lmem, CS::Chip::Digits & digits ) {
    assert( e.id.run_no() );  // event ID has to be set prior to get_mappings() call.
    const nameutils::DetectorNaming & nm = calib::Handle<nameutils::DetectorNaming>::get();
    #if 0  // consider this snippet for in advance checks of detectors existing in run
    // Acquire mappings of the DAQ IDs to 
    const CS::Chip::Maps & daqMaps = _dddMgr->GetMaps();
    {
        std::ostringstream oss;
        oss << "Detectors registered "
        for( auto p : daqMaps.GetDetSrcIDes() ) {
            std::cout << " * DetID=" << p.first.GetName() << " ("
                      << p.first.GetNumber() << ") with SrcIDs: ";
            for( auto srcID : p.second ) {
                std::cout << srcID << ", ";
            }
            std::cout << "wires=" << daqMaps.GetWires(p.first);
            std::cout << std::endl;
        }
    }
    #endif
    for( auto it = digits.begin(); digits.end() != it; ++it  ) {
        DetID did;
        const std::string dddPlaneName = it->second->GetDetID().GetName();
        if( _tbnames2ignore.end() != _tbnames2ignore.find(dddPlaneName) ) {
            // ^^^ TODO: perform the partial/wildcard comparison here
            continue;  // omit decoding
        }
        if( _log.getPriority() >= log4cpp::Priority::INFO ) {
            auto ir = _detectorsMet.emplace(dddPlaneName, 0);
            ++(ir.first->second);
        }
        // Attempt first conversion stage
        try {
            did.id = nm[dddPlaneName];
        } catch( errors::TBNameParsingFailure & err ) {
            auto unknownDetIt = _unknownDets.find( err.culprit() );
            if( _unknownDets.end() == unknownDetIt ) {
                _unknownDets.emplace( err.culprit(), 1 );
                _log.warn( "Failed to translate TBName \"%s\" to numerical"
                        " detector ID -- no hit data for this detector will be"
                        " available in an event. Other occurrences will be"
                        " quietly ignored."
                     , it->second->GetDetID().GetName().c_str() );
                it->second->Print();  // useful snippet to dump unknown digits
            } else {
                ++(unknownDetIt->second);
            }
            continue;
        } catch( std::exception & e ) {
            auto unknownDetIt = _unknownDets.find( dddPlaneName );
            if( _unknownDets.end() == unknownDetIt ) {
                _unknownDets.emplace( dddPlaneName, 1 );
                _log.warn( "Failed to translate TBName \"%s\" to numerical"
                        " detector ID -- no hit data for this detector will be"
                        " available in an event. Other occurrences will be"
                        " quietly ignored (generic error occured during"
                        " TBname conversion: %s)."
                     , it->second->GetDetID().GetName().c_str()
                     , e.what()
                     );
                it->second->Print();  // useful snippet to dump unknown digits
            } else {
                ++(unknownDetIt->second);
            }
            continue;
        }
        // ^^^ Value of `did' is not yet complete detector ID actually -- the
        // name itself does not not always fully identify a particular
        // detector part for certain hit. We have to fill ID's part called
        // `payload' w.r.t. the hit content.
        DetChip_t chipID = did.chip();
        char suppBf[256] = "";
        try {
            if( naming().kSADC == chipID) {
                auto ir = _fill_hit<CS::ChipSADC::Digit>( e, lmem, did, it->second );
                if( !ir.second ) {
                    // insertion failed -- gain some common information on the
                    // SADC digit for user's consideration.
                    const CS::ChipSADC::Digit & sadc
                        = dynamic_cast<const CS::ChipSADC::Digit &>(*(it->second));
                    snprintf( suppBf, sizeof(suppBf)
                            , "overflow=%s, suppression=%s, sourceID=%#x,"
                              " port=%#x, chip=%#x, channel=%#x, x=%d, y=%d"
                            , sadc.GetChannel() ? "yes" : "no"
                            , sadc.GetSuppression() ? "yes" : "no"
                            , sadc.GetSourceID()
                            , sadc.GetPort()
                            , sadc.GetChip()
                            , sadc.GetChannel()
                            , sadc.GetX()
                            , sadc.GetY()
                            );
                    _log.error( "Hit insertion failed; %s", suppBf );
                }
            } else if( naming().kAPV == chipID ) {
                auto ir = _fill_hit<CS::ChipAPV::Digit>( e, lmem, did, it->second );
                if( !ir.second ) {
                    _log.error( "APV hit insertion failed." );
                }
            } else if( naming().kStwTDC == chipID ) {
                auto ir = _fill_hit<CS::ChipNA64TDC::Digit>( e, lmem, did, it->second );
                if( !ir.second ) {
                    _log.error( "NA64TDC hit insertion failed." );
                }
            } else if( naming().kF1 == chipID ) {
                auto ir = _fill_hit<CS::ChipF1::Digit>( e, lmem, did, it->second );
                if( !ir.second ) {
                    _log.error( "F1 hit insertion failed." );
                }
            /* } else if( ... other chips ... ) { */       
            } else {
                NA64DP_RUNTIME_ERROR(
                        , "Translation of data from chip \"%s\" (code %#x) is"
                          " not yet implemented (detector \"%s\")."
                        , nm.chip_features(chipID).name.c_str()
                        , (int) chipID
                        , it->second->GetDetID().GetName().c_str() );
            }
        }
        #if 0
        catch( const errors::HitKeyIsNotUniqe & err ) {
            _log.error( "Non-unique detector mapping revealed for"
                    " digit on \"%s\" (chip %#x \"%s\", kin %#x \"%s\")."
                    , it->second->GetDetID().GetName().c_str()
                    , chipID, nm.chip_features(chipID).name.c_str()
                    , did.kin()
                    , nm.kin_features(std::make_pair(did.chip(), did.kin())).name.c_str()
                    );
            throw;
        }
        #else
        catch( const std::exception & e ) {
            _log.error( "Error for"
                    " digit on \"%s\" (chip %#x \"%s\", kin %#x \"%s\"): %s"
                    , it->second->GetDetID().GetName().c_str()
                    , chipID, nm.chip_features(chipID).name.c_str()
                    , did.kin()
                    , nm.kin_features(std::make_pair(did.chip(), did.kin())).name.c_str()
                    , e.what()
                    );
            throw;
        }
        #endif
        #if 0
        if( !ir ) {
            // TODO: Dirty place to kill concerning STT0. FIXIT
            if ( did.kin() == naming().kStt ) {
            } else {
                _log.error( "Failed to impose hit of TBName \"%s\" (chip=%#x \"%s\","
                        " kin=%#x \"%s\" due to incomplete traits or bad digit data."
                        " DDD info: {%s}."
                        , it->second->GetDetID().GetName().c_str()
                        , did.chip(), nm.chip_features(did.chip()).name.c_str()
                        , did.kin(), nm.kin_features(std::make_pair(did.chip(), did.kin())).name.c_str()
                        , suppBf
                        );
            }
        }
        #endif
    }
}

void
DDDEventSource::handle_update(const EventBitTags & ebt) {
    calib::Handle<EventBitTags>::handle_update(ebt);
    auto it = ebt.types.find("service");
    if(it == ebt.types.end()) {
        _log.warn("DDD source requires \"service\" type code to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _serviceTypeCode = 0x0;
    } else {
        _serviceTypeCode = it->second;
    }
    it = ebt.types.find("phys");
    if(it == ebt.types.end()) {
        _log.warn("DDD source requires \"phys\" type code to be"
                " defined, but no entry found in bit tags semantics"
                " -- bitflag annotation disabled");
        _physicsTypeCode = 0x0;
    } else {
        _physicsTypeCode = it->second;
    }
}

bool
DDDEventSource::read( event::Event & evRef, event::LocalMemory & lmem ) {
    bool accepted = false;
    do {
        #if 0
        if( ! _dddMgr->ReadEvent() ) {
            return false;  // end of raw input
        }
        if( ! (accepted = _dddMgr->DecodeEvent()) ) {
            // NOTE: internal counter of the manager is used here to identify
            // event's number
            _log.warn( "Event source %p: failed to decode an event #%zu."
                     , this, _dddMgr->GetEventsCounter() );
            ++_decodingFailures;
            continue;  // skip events with decoding problems
        }
        auto & dddEvRef = _dddMgr->GetEvent();
        #else
        if( ! (accepted = _get_next_event()) ) return false;
        auto & dddEvRef = _current_event();
        #endif

        // skip unpermitted event types, if at least one type is set (otherwise,
        // pass all)
        if( !_permitTypes.empty() ) {
            auto it = _permitTypes.find(dddEvRef.GetType());
            if( it == _permitTypes.end() ) {
                // omit event as it is not among the permitted types
                //
                // note: although discrimination by event type can be made
                //       within processing pipe, skipping it here may save some
                //       performance as subsequent decoding is not needed
                accepted = false;
                continue;
            }
        }
        if(_log.getPriority() >= log4cpp::Priority::INFO) {  // append event types
           auto ir = _nativeEventTypesMet.emplace(dddEvRef.GetType(), 0);
           ++(ir.first->second);
        }
        if(_log.getPriority() >= log4cpp::Priority::INFO) {  // append triggers
           auto ir = _nativeTriggersMet.emplace(dddEvRef.GetTrigger(), 0);
           ++(ir.first->second);
        }

        if( dddEvRef.GetRunNumber() > na64sw_g_runNo_max )
            NA64DP_RUNTIME_ERROR( "Run number exceeds expected maximum value"
                    " (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetRunNumber()
                    , (unsigned long) na64sw_g_runNo_max );
        if( dddEvRef.GetBurstNumber() > na64sw_g_spillNo_max )
            NA64DP_RUNTIME_ERROR( "Spill number exceeds expected maximum value"
                    " (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetBurstNumber()
                    , (unsigned long) na64sw_g_spillNo_max );
        if( dddEvRef.GetEventNumberInBurst() > na64sw_g_eventNo_max )
            NA64DP_RUNTIME_ERROR( "Event-in-spill number exceeds expected"
                    " maximum value (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetEventNumberInBurst()
                    , (unsigned long) na64sw_g_eventNo_max );
        evRef.id = EventID( dddEvRef.GetRunNumber()
                          , dddEvRef.GetBurstNumber()
                          , dddEvRef.GetEventNumberInBurst()
                          );

        if(_filterEventsByID && !_filterEventsByID(evRef.id)) {
            accepted = false;
            continue;
        }

        evRef.time = dddEvRef.GetTime();
        // This may cause updates on all calibration subscribers including
        // the current data source object:
        calib_manager().event_id( evRef.id, dddEvRef.GetTime() );
        // ^^^ note that, albeit DDD's event returns pair (seconds +
        // microseconds), microsecond are not used practically in NA64
        
        {   // match DDD event type with NA64sw semantical bits
            decltype(event::Event::trigger) nativeEventType = 0x0
                                          , systemEventType = 0x0
                                          ;
            // event type shall be converted
            #if 0
            switch(dddEvRef.GetType()) {
                case CS::DaqEvent::START_OF_RUN      : {
                    nativeEventType = NA64SW_EVENT_TYPE_START_OF_RUN;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::END_OF_RUN        : {
                    nativeEventType = NA64SW_EVENT_TYPE_END_OF_RUN;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::START_OF_RUN_FILES: {
                    nativeEventType = NA64SW_EVENT_TYPE_START_OF_RUN_FILES;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::END_OF_RUN_FILES  : {
                    nativeEventType = NA64SW_EVENT_TYPE_END_OF_RUN_FILES;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::START_OF_BURST    : {
                    nativeEventType = NA64SW_EVENT_TYPE_START_OF_BURST;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::END_OF_BURST      : {
                    nativeEventType = NA64SW_EVENT_TYPE_END_OF_BURST;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::PHYSICS_EVENT     : {
                    nativeEventType = NA64SW_EVENT_TYPE_PHYSICS_EVENT;
                    systemEventType = _physicsTypeCode;
                } break;
                case CS::DaqEvent::CALIBRATION_EVENT : {
                    nativeEventType = NA64SW_EVENT_TYPE_CALIBRATION_EVENT;
                    systemEventType = _physicsTypeCode;
                } break;
                case CS::DaqEvent::END_OF_LINK       : {
                    nativeEventType = NA64SW_EVENT_TYPE_END_OF_LINK;
                    systemEventType = _serviceTypeCode;
                } break;
                case CS::DaqEvent::EVENT_FORMAT_ERROR: {
                    nativeEventType = NA64SW_EVENT_TYPE_EVENT_FORMAT_ERROR;
                    systemEventType = _serviceTypeCode;
                } break;
                default:
                    NA64DP_RUNTIME_ERROR("CS::DaqEvent has unknown event type code: %d"
                            , (int) dddEvRef.GetType() );
            };
            #else
            nativeEventType = static_cast<uint16_t>(dddEvRef.GetType());
            systemEventType = ( dddEvRef.GetType() == CS::DaqEvent::PHYSICS_EVENT
                             || dddEvRef.GetType() == CS::DaqEvent::CALIBRATION_EVENT)
                            ? _physicsTypeCode
                            : _serviceTypeCode
                            ;
            #endif
            if(nativeEventType & systemEventType) {
                NA64DP_RUNTIME_ERROR("Native event type code (%#x) and"
                        " system event type code (%#x) has overlapping semantical"
                        " bits (%#x)", nativeEventType, systemEventType
                        , nativeEventType & systemEventType );
            }
            evRef.evType = nativeEventType | systemEventType;
        }
        {   // match DDD trigger code with NA64sw semantical bits
            auto nativeTriggerCode = dddEvRef.GetTrigger();
            if( nativeTriggerCode < std::numeric_limits<decltype(event::Event::trigger)>::min()
             || nativeTriggerCode > std::numeric_limits<decltype(event::Event::trigger)>::max()
             ) {
                _log.warn("Native trigger code %#x is outside of permitted"
                        " range for Event::trigger field data type (will be truncated)."
                        , (size_t) nativeTriggerCode);
            }
            evRef.trigger = static_cast<decltype(event::Event::trigger)>(nativeTriggerCode);
        }

        _event_fill( evRef, lmem, _get_current_event_digits() );
    } while( (!accepted)
           || !evRef.id  //< this was added to a weird DAQ errors
                         //  filling chunk with malformed events with 0
                         //  ID.
           );
    return accepted;
}

void
DDDEventSource::finalize() {
    if( 0 == _decodingFailures && _unknownDets.empty() )
        return; // Source done with no decoding errors and no unknown detectors.
    if( _decodingFailures )
        this->log().warn( "There were %zu decoding failures during DDD source lifetime."
                 , _decodingFailures);
    if( !_unknownDets.empty() ) {
        std::stringstream ss;
        bool isFirst = true;
        for( const auto & p : _unknownDets ) {
            if(isFirst) isFirst = false; else ss << ", ";
            ss << "\"" << p.first << "\" (" << p.second << ")";
        }
        this->log().warn( "There were %zu unknown detector entity names during DDD"
                   " data source lifetime: %s."
                 , _unknownDets.size(), ss.str().c_str() );
    }
    // In debug mode, print all the detectors met during source object lifetime
    if( _log.getPriority() >= log4cpp::Priority::INFO ) {
        // detectors
        std::map<std::string, size_t>
            namesAlphabetic(_detectorsMet.begin(), _detectorsMet.end());
        std::ostringstream oss;
        bool isFirst = true;
        for(const auto & p : namesAlphabetic) {
            if( !isFirst ) oss << ", ";
            oss << p.first << "(" << p.second << ")";
            isFirst = false;
        }
        _log << log4cpp::Priority::INFO << "Detector digits"
            " met during lifetime: " << oss.str() << ".";
        // event types
        oss.str("");
        isFirst = true;
        for(const auto & p : _nativeEventTypesMet) {
            if( !isFirst ) oss << ", "; else isFirst = false;
            oss << CS::DaqEvent::stringEventType(p.first) << " ("
                << p.second << " times)";
        }
        _log << log4cpp::Priority::INFO << "Event types met"
            << " during lifetime: " << oss.str() << ".";
        // trigger counts
        oss.str("");
        isFirst = true;
        for(const auto & p : _nativeTriggersMet) {
            if( !isFirst ) oss << ", "; else isFirst = false;
            oss << std::hex << std::showbase << p.first << " (" << std::dec
                << p.second << " times)";
        }
        _log << log4cpp::Priority::INFO << "Event trigger codes met"
            << " during lifetime: " << oss.str() << ".";
    }
}

//
// Manager-based event reading

DDDEventSource_Mgr::DDDEventSource_Mgr( calib::Manager & mgr
                                      , const std::string & coralMapsDir
                                      , const std::vector<std::string> & inputs
                                      , std::vector<std::string> permittedEvTypes
                                      , const std::set<std::string> & tbnames2ignore
                                      , log4cpp::Category & logCat
                                      , const std::string & detNamingClass
                                      , iEvProcInfo * epi
                                      , std::function<bool(EventID)> filterEventsByID
                              ) : DDDEventSource( mgr, permittedEvTypes,
                                        tbnames2ignore, logCat, detNamingClass,
                                        epi, filterEventsByID )
                                {
    // Instantiate DDD manager object
    _dddMgr = new CS::DaqEventsManager();
    // Set the CORAL maps directory providing vital decoding info
    _dddMgr->SetMapsDir(coralMapsDir);
    // Initialize the manager object with all the data files
    for( const std::string & ifName : inputs ){
        _dddMgr->AddDataSource(ifName);
    }
}

bool
DDDEventSource_Mgr::_get_next_event() {
    for(;;) {
        if( ! _dddMgr->ReadEvent() ) {
            return false;  // end of raw input
        }
        if( ! _dddMgr->DecodeEvent() ) {
            // NOTE: internal counter of the manager is used here to identify
            // event's number
            _log.warn( "Event source %p: failed to decode an event #%zu."
                     , this, _dddMgr->GetEventsCounter() );
            ++_decodingFailures;
            continue;  // skip events with decoding problems
        }
        break;
    }
    return true;
}

CS::DaqEvent &
DDDEventSource_Mgr::_current_event() {
    return _dddMgr->GetEvent();
}

CS::Chip::Digits &
DDDEventSource_Mgr::_get_current_event_digits() {
    return _dddMgr->GetEventDigits();
}

//
// Indexed source

///\brief Looks for .json index file, named similarly to the raw data provided
class LocalFileIndexResolver : public DDDEventSource_Indexed::iIndexResolver {
protected:
    // Regular expression the original file has match to, to be parsed
    //
    // First group must be chunk number, second must be run number
    //std::regex _rxRawDataFilename;
    log4cpp::Category & _logCat;
public:
    LocalFileIndexResolver( log4cpp::Category & logCat ) : _logCat(logCat) {}

    std::shared_ptr<DDDEventSource_Indexed::SourceMetadata>
    resolve_index(const std::string & filename) override {
        auto sm = std::make_shared<DDDEventSource_Indexed::SourceMetadata>(filename);
        // Form an expected filename and check the file exists
        // TODO: it might be better to build the filename according to regex,
        // customize lookup path, etc. For the time being we just look for
        // postfixed .json file
        std::string indexFilePath = filename + ".json";
        // if index file does not exist, return a nullptr
        if( access(indexFilePath.c_str(), F_OK) != 0 ) return nullptr;
        // otherwise, parse it
        std::fstream f(indexFilePath);
        std::stringstream ss;
        ss << f.rdbuf();
        try {
            sm->index( DDDIndex::from_JSON_str(ss.str()) );
        } catch( std::exception & e ) {
            _logCat.error( "Failed to use file index \"%s\": %s"
                         , indexFilePath.c_str()
                         , e.what() );
            return nullptr;
        } catch( ... ) {
            _logCat.error( "Failed to use file index \"%s\": unknown error."
                         , indexFilePath.c_str()
                         );
            return nullptr;
        }
        return sm;
    }
};  // LocalFileIndexResolver

///\brief A stub to use sources with unresolved indeces
///
/// Does not actually resolves indexes; may be used to safely terminate index
/// resolving chain in cases when it is needed
class StubIndexResolver : public DDDEventSource_Indexed::iIndexResolver {
protected:
    log4cpp::Category & _logCat;
    const bool _noWarn;
public:
    StubIndexResolver( log4cpp::Category & logCat
                     , bool noWarn=false
                     ) : _logCat(logCat)
                       , _noWarn(noWarn)
                       {}

    std::shared_ptr<DDDEventSource_Indexed::SourceMetadata>
    resolve_index(const std::string & filename) override {
        auto sm = std::make_shared<DDDEventSource_Indexed::SourceMetadata>(filename);
        if(!_noWarn)
            _logCat.warn("Couldn't resolve index for file \"%s\"."
                    , filename.c_str() );
        return sm;
    }
};

static DDDEventSource_Indexed::iIndexResolver *
_instantiate_index_resolver(const YAML::Node & cfg, log4cpp::Category & logCat) {
    if( !cfg["_type"] )
        NA64DP_RUNTIME_ERROR("No \"type\" field for index resolver config.");
    const std::string resolverTypeStr = cfg["_type"].as<std::string>();
    if(resolverTypeStr == "localFile") {
        return new LocalFileIndexResolver(logCat);
    }
    if(resolverTypeStr == "stub") {
        return new StubIndexResolver(logCat);
    }
    // ... other resolvers here
    NA64DP_RUNTIME_ERROR("Unnknown DDD metadata resolver type: \"%s\"",
            resolverTypeStr.c_str());
}

// internal func to take first usable index from resolvers chain
static std::shared_ptr<DDDEventSource_Indexed::SourceMetadata>
_make_metadata_for_source( const std::string & id
                         , std::list<DDDEventSource_Indexed::iIndexResolver*> & resolvers
                         ) {
    for(auto resolver : resolvers) {
        auto sm = resolver->resolve_index(id);
        if(sm) return sm;
    }
    return nullptr;
}

DDDEventSource_Indexed::Index::Index(
            const std::vector<std::string> & inputs
          , std::list<iIndexResolver*> & resolvers
          ) {
    _indexPtr = new DDDIndex();
    // iterate over provided source URLs/IDs and construct the metadata items
    // for every instance
    for(const auto & id: inputs) {
        push_back(_make_metadata_for_source(id, resolvers));
    }
}

DDDEventSource_Indexed::DDDEventSource_Indexed( calib::Manager & mgr
                                              , const std::string & coralMapsDir
                                              , const std::vector<std::string> & inputs
                                              , std::list<iIndexResolver*> & resolvers
                                              , std::vector<std::string> permittedEvTypes
                                              , const std::set<std::string> & tbnames2ignore
                                              , log4cpp::Category & logCat
                                              , const std::string & detNamingClass
                                              , iEvProcInfo * epi
                                              , std::function<bool(EventID)> filterEventsByID
                                              )
        : DDDEventSource(mgr, permittedEvTypes, tbnames2ignore
                , logCat, detNamingClass, epi, filterEventsByID)
        , _index(inputs, resolvers)
        {
    _cSourceIDRit = _index.begin();
}

bool
DDDEventSource_Indexed::_get_next_event() {
    if(_cSourceIDRit == _index.end()) return false;  // chain depleted
    auto & srcMData = **_cSourceIDRit;
    std::lock_guard<std::mutex> lock(srcMData._mutex);  // wait unitl metadata is retrieved, if need
    // TODO: support other types of ID?
    throw std::runtime_error("todo: _get_next_event()");  // TODO
}

CS::DaqEvent &
DDDEventSource_Indexed::_current_event() {
    throw std::runtime_error("todo: _current_event()");  // TODO
}

CS::Chip::Digits &
DDDEventSource_Indexed::_get_current_event_digits() {
    throw std::runtime_error("todo: _get_current_event_digits()");  // TODO
}

DDDEventSource_Indexed::SourceMetadata::~SourceMetadata() {
    // lock mutex in case there are some pending operations on the index object
    _mutex.lock();
    if(_subIndex) delete _subIndex;
    _mutex.unlock();
}

void
DDDEventSource_Indexed::SourceMetadata::index(const DDDIndex & index) {
    //assert(_mutex.is_locked());  // todo: is there any equivalent?
    assert(!_subIndex);
    _subIndex = new DDDIndex(index);
}

const DDDIndex &
DDDEventSource_Indexed::SourceMetadata::index() const {
    if(!has_index()) {
        NA64DP_RUNTIME_ERROR("No index for source \"%s\".", _id.c_str() );
    }
    return *_subIndex;
}

namespace aux {
/// An event ID filtering class getting input from stream; useful for
/// cherry-picking certain events
///
/// \todo `unordered_set` would be faster, but hash for EvID tp is not yet implemd
class FilterIDs : public set<EventID> {
public:
    FilterIDs() {}
    virtual bool has_event_id(EventID eid) {
        return find(eid) != end();
    }
};

}  // namespace ::na64dp::ddd::aux

}  // namespace ::na64dp::ddd
}  // namespace na64dp

using na64dp::ddd::DDDEventSource;
REGISTER_SOURCE( DDDEventSource
               , calibManager
               , cfg
               , ids
               , "A COMPASS DaqDataDecoding lib data format used by NA64 for"
                 " storing raw events data."
               ) {
    if( ! cfg["CORALMapsDir"] ) {
        NA64DP_RUNTIME_ERROR("Mandatory \"CORALMapsDir\" parameter is not"
                " provided for DaqDataDecoding data source object");
    }
    #ifdef CORAL_MAPS_DIR
    setenv("CORAL_MAPS_DIR", CORAL_MAPS_DIR, 0);
    #endif
    auto mapsDirs = na64dp::util::expand_names(cfg["CORALMapsDir"].as<std::string>());
    if( mapsDirs.size() != 1 ) {
        NA64DP_RUNTIME_ERROR( "CORAL maps dir \"%s\" expanded to %zu path strings; has to be"
                              " exactly one."
                , cfg["CORALMapsDir"].as<std::string>().c_str()
                , mapsDirs.size() );
    }
    std::string coralMapsDir = mapsDirs[0];
    // Event type filtering
    std::vector<std::string> permitEvTypes;
    if( cfg["permitEvTypes"] ) {
        permitEvTypes = cfg["permitEvTypes"].as<std::vector<std::string>>();
    }
    // TBName filtering
    std::set<std::string> tbnames2ignore;
    if( cfg["tbnames2ignore"] ) {
        auto v = cfg["tbnames2ignore"].as<std::vector<std::string>>();
        tbnames2ignore = std::set<std::string>(v.begin(), v.end());
    }

    char bf[1024], permdEvTypesStr[1024];
    size_t cPos = 0;
    for(auto entityID : ids) {
        cPos += snprintf( bf + cPos, sizeof(bf) - cPos
                        , "\"%s\", "
                        , entityID.c_str() );
    }
    cPos = 0;
    for( auto petId : permitEvTypes ) {
        cPos += snprintf( permdEvTypesStr + cPos, sizeof(permdEvTypesStr) - cPos
                        , "\"%s\", "
                        , petId.c_str() );
    }
    DDDEventSource * src;
    const std::string detNamingClass = cfg["namingScheme"]
                                     ? cfg["namingScheme"].as<std::string>()
                                     : "default"
                                     ;
    // Certain events filtering
    std::function<bool(na64dp::EventID)> evIDFilter = nullptr;
    if( cfg["pickIDs"] ) {
        // todo: check if sequence, type, etc; if string => filename
        const std::string & fname = cfg["pickIDs"].as<std::string>();
        auto filterPtr = std::make_shared<na64dp::ddd::aux::FilterIDs>();
        std::ifstream ifs(fname);
        int runNo, spillNo, eventIDNo;
        while( ifs >> runNo >> spillNo >> eventIDNo ) {
            filterPtr->insert(na64dp::EventID(runNo, spillNo, eventIDNo));
        }
        evIDFilter = [filterPtr](na64dp::EventID eid){return filterPtr->has_event_id(eid);};
    }

    log4cpp::Category & logCat = log4cpp::Category::getInstance(
                cfg["_log"] ? cfg["_log"].as<std::string>() : "source.ddd"
            );

    na64dp::iEvProcInfo * epi = nullptr;  // TODO: provide in ctr or set it aposteriori with some method...
    try {
        if( (!cfg["subtype"]) || (cfg["subtype"].as<std::string>() == "manager") ) {
            src = new na64dp::ddd::DDDEventSource_Mgr( calibManager
                                    , coralMapsDir
                                    , ids
                                    , permitEvTypes
                                    , tbnames2ignore
                                    , logCat
                                    , detNamingClass
                                    , epi
                                    , evIDFilter
                                    );
        } else if( cfg["subtype"].as<std::string>() == "indexed" ) {
            std::list<na64dp::ddd::DDDEventSource_Indexed::iIndexResolver *> metadataResolvers;
            for( auto resolverCfg : cfg["metadataResolvers"] ) {
                metadataResolvers.push_back(na64dp::ddd::_instantiate_index_resolver(resolverCfg, logCat));
            }
            src = new na64dp::ddd::DDDEventSource_Indexed( calibManager
                                    , coralMapsDir
                                    , ids
                                    , metadataResolvers
                                    , permitEvTypes
                                    , tbnames2ignore
                                    , logCat
                                    , detNamingClass
                                    , epi
                                    , evIDFilter
                                    );
        } else {
            NA64DP_RUNTIME_ERROR("DDD source subtype \"%s\" not known.",
                    cfg["subtype"].as<std::string>().c_str() );
        }
    } catch( std::exception & e ) {
        log4cpp::Category::getInstance("sources").error( "Unable to instantiate"
                " DaqDataDecoding data source object with arguments:"
                " {calibManagerPtr=%p, CORALMapsDir=\"%s\", inputFiles=[%s],"
                " permittedEventTypes=[%s]}"
                , &calibManager
                , coralMapsDir.c_str()
                , bf
                , permdEvTypesStr
                );
        throw;
    }
    log4cpp::Category::getInstance("sources").debug( "Instantiated"
                " DaqDataDecoding data source object with arguments:"
                " {calibManagerPtr=%p, CORALMapsDir=\"%s\", inputFiles=[%s],"
                " permittedEventTypes=[%s]}; object ptr is %p"
                , &calibManager
                , coralMapsDir.c_str()
                , bf
                , permdEvTypesStr
                , src
                );
    return src;
}

