#include "selectiveRead.hh"

namespace na64dp {
namespace ddd {

void
SelectedEventsInSpill::consider(DDDIndex::EventNo_t newEvNo) {
    insert(newEvNo);
}

void
SelectedEvents::consider( DDDIndex::RunNo_t runNo
                        , DDDIndex::SpillNo_t spillNo
                        , DDDIndex::EventNo_t evNoInSpill
                        ) {
    auto runIR = emplace( runNo
                     , std::map<DDDIndex::SpillNo_t, SelectedEventsInSpill>()
                     );
    auto spillIR = runIR.first->second.emplace(spillNo, SelectedEventsInSpill());
    spillIR.first->second.consider(evNoInSpill);
}

size_t
SelectedEvents::n_events() const {
    size_t n = 0;
    for(auto & e: *this) {
        n += e.second.size();
    }
    return n;
}


void
collect_event_ids_from_ROOT_scan( std::istream & is
                                , SelectedEvents & dest
                                ) {
    // Expected grammar is the table separated with `*` symbol, with
    // various decorations, whitespace tabulation and header.
    const std::regex rgx("[\\s\\*]+");
    std::string line;
    size_t nLine = 0;
    while(std::getline(is, line)) {
        ++nLine;
        std::sregex_token_iterator iter(line.begin(), line.end(), rgx, -1);
        std::sregex_token_iterator end;
        std::vector<std::string> tokens(++iter, end);  // 0th iter is empty (?)
        if( tokens.empty() ) continue;
        if( tokens.size() != 4 ) {
            // differs from Andrea's fmt?
            char errbf[128];
            snprintf(errbf, sizeof(errbf)
                    , "Line %zu: number of tokens %zu != 4."
                    , nLine, tokens.size()
                    );
            throw std::runtime_error(errbf);
        }
        dest.consider( std::stoi(tokens[1])
                     , std::stoi(tokens[2])
                     , std::stoi(tokens[3])
                     );
    }
}

}  // namespace ::na64dp::ddd
}  // namespace na64dp

int
main(int argc, char * argv[]) {
    std::ifstream ifs(argv[1]);
    na64dp::ddd::SelectedEvents se;
    na64dp::ddd::collect_event_ids_from_ROOT_scan(ifs, se);
    return 0;
}

