#include "chipTraits.hh"

#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

namespace na64dp {
namespace ddd {

// SADC
//////

bool
CSDigitTraits<CS::ChipSADC::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                        , DetID & did
                                                        , const CS::ChipSADC::Digit & dgt ) {
    assert( c.kSADC == did.chip() );
    CellID cID;
    // these lines below makes x/y indexes be always set. Unclear, whether it
    // is a good strategy though (but how then distinguish it from case when
    // x=0, y=0 without additional knowledge of detector struct?)
    cID.set_x(dgt.GetX());
    cID.set_y(dgt.GetY());
    if( did.is_payload_set() && CellID(did.payload()).is_z_set() ) {
        cID.set_z(CellID(did).get_z());
    }
    // no z set except for ECAL for now (TODO: hodoscopes?)
    if( c.kECAL == did.kin() ) {
        assert( (!did.is_payload_set()) || !CellID(did.payload()).is_z_set() );
        assert( did.number() < 2 );  // shall be either main part, either preshower
        cID.set_z( 0 == did.number() ? 0 : 1 );
        did.number(0);  // both parts of the ECAL are now single station
    }
    did.payload( cID.cellID );
    return true;
}

std::pair<typename decltype(event::Event::sadcHits)::iterator, bool>
CSDigitTraits<CS::ChipSADC::Digit>::emplace_digit( event::Event & e
                                                 , event::LocalMemory & lmem
                                                 , DetID did
                                                 , const CS::ChipSADC::Digit & sadcDigit
                                                 ) {
    auto ir = e.sadcHits.emplace( did, lmem.create<event::SADCHit>(lmem) );
    HitType & sadcHit = *(ir.first->second);
    util::reset(sadcHit);

    assert(sadcDigit.GetSamples().data());
    sadcHit.rawData = lmem.create<event::RawDataSADC>(lmem);
    util::reset(*(sadcHit.rawData));
    std::copy( sadcDigit.GetSamples().data()
             , sadcDigit.GetSamples().data() + 32  // todo: hardcoded value -- # of samples
             , sadcHit.rawData->wave );
    return ir;
}

// APV
/////

bool
CSDigitTraits<CS::ChipAPV::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                       , DetID & did
                                                       , const CS::ChipAPV::Digit & apv ) {
    assert( c.kAPV == did.chip() );
    if( c.kMM == did.kin() ) {
        // MM02X, MM03Y, etc.
        WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( apv.GetChannel() );
        did.payload( wID.id );
        return true;
    } else if( c.kGEM == did.kin() ) {
        // GM01Y1__, GM02X1__, etc.
        // The last 3 chars are always '1__'
        WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( apv.GetChannel() );
        did.payload( wID.id );
        return true;
    }
    return false;  // shall be considered as not being implemented behaviour
}

std::pair<typename decltype(event::Event::apvHits)::iterator, bool>
CSDigitTraits<CS::ChipAPV::Digit>::emplace_digit( event::Event & e
                                                , event::LocalMemory & lmem
                                                , DetID did
                                                , const CS::ChipAPV::Digit & apvDigit
                                                ) {
    auto ir = e.apvHits.emplace( did, lmem.create<event::APVHit>(lmem) );
    HitType & apvHit = *(ir.first->second);
    util::reset(apvHit);

    apvHit.rawData = lmem.create<event::RawDataAPV>();
    util::reset(*(apvHit.rawData)); {  // fill raw data
        apvHit.rawData->wireNo = apvDigit.GetChannel();
        apvHit.rawData->chip = apvDigit.GetChip();  // uint16_t
        apvHit.rawData->srcID = apvDigit.GetSourceID();  // uint16_t
        apvHit.rawData->adcID = apvDigit.GetAdcID(); // uint16_t
        apvHit.rawData->chipChannel = apvDigit.GetChipChannel();  // uint16_t
        apvHit.rawData->timeTag = apvDigit.GetTimeTag();
        std::copy( apvDigit.GetAmplitude()
                 , apvDigit.GetAmplitude() + 3  // sizeof(APVRawData::samples)
                 , apvHit.rawData->samples );
        // ...
    }
    return ir;
}

// Straw TDC
///////////

bool
CSDigitTraits<CS::ChipNA64TDC::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                           , DetID & did
                                                           , const CS::ChipNA64TDC::Digit & stwtdc
                                                           ) {
    assert( c.kStwTDC == did.chip() );
    if( c.kSt == did.kin() ) {
        // For straw tubes we use supplementary projection codes (*2 -- X2,
        // U2, etc), choosing it depending on the parity.
		WireID wID(did.payload());
        assert(wID.proj_is_set());
        if( stwtdc.GetChannel()%2 ) {
            // we assume odd wires to be of reciprocal plane
            wID.proj(WireID::adjoint_proj_code(wID.proj()));
        }
        wID.wire_no( stwtdc.GetChannel()/2 );
        did.payload( wID.id );
        return true;
    }

	if( c.kStt == did.kin() ) {
        // unset projection is legal for this type of chip (it's a TDC)
		WireID wID = did.is_payload_set() ? WireID(did.payload())
                                          : WireID()
                                          ;
        wID.wire_no( stwtdc.GetChannel() );
        did.payload( wID.id );
        return true;
    }
    
    return false;  // shall be considered as not being implemented behaviour
}

std::pair<typename decltype(event::Event::stwtdcHits)::iterator, bool>
CSDigitTraits<CS::ChipNA64TDC::Digit>::emplace_digit( event::Event & e
                                                    , event::LocalMemory & lmem
                                                    , DetID did
                                                    , const CS::ChipNA64TDC::Digit & stwtdcDigit
                                                    ) {
    auto ir = e.stwtdcHits.emplace( did, lmem.create<event::StwTDCHit>(lmem) );
    HitType & stwtdcHit = *(ir.first->second);
    util::reset(stwtdcHit);

    stwtdcHit.rawData = lmem.create<event::StwTDCRawData>();
    util::reset(*(stwtdcHit.rawData));
    stwtdcHit.rawData->wireNo        = stwtdcDigit.GetChannel();
    stwtdcHit.rawData->time          = (float)(CS::int16) stwtdcDigit.GetTime();
    stwtdcHit.rawData->timeDecoded   = stwtdcDigit.GetTimeDecoded();

    #if 0
    if( 32 == stwtdcDigit.GetChannel() ) {
    std::cout << "NA64TDC digit:" << std::endl;
        std::cout << "  time ........... : " << stwtdcDigit.GetTime()
                  << " (uint16: " << (uint16_t) stwtdcDigit.GetTime()
                  << "), (uint32: " << (uint32_t) stwtdcDigit.GetTime()
                  << "), (int16: " << (int16_t) stwtdcDigit.GetTime()
                  << ")" << std::endl
                  << "  time decoded ... : " << stwtdcDigit.GetTimeDecoded() << std::endl
                  << "  src ID ......... : " << stwtdcDigit.GetSourceID() << std::endl
                  << "  port ........... : " << stwtdcDigit.GetPort() << std::endl
                  << "  channel ........ : " << stwtdcDigit.GetChannel() << std::endl
                  << "  wire ........... : " << stwtdcDigit.GetWire() << std::endl
                  << "  x .............. : " << stwtdcDigit.GetX() << std::endl
                  << "  y .............. : " << stwtdcDigit.GetY() << std::endl
                  << "  channel pos .... : " << stwtdcDigit.GetChannelPos() << std::endl
                  << "  amplitude ...... : " << stwtdcDigit.GetAmplitude() << std::endl
                  << "  time unit ...... : " << stwtdcDigit.GetTimeUnit() << std::endl
                  ;
        stwtdcDigit.Print();
        std::cout << "--- end of NA64TDC digit ---" << std::endl;
    }
    #endif

    return std::pair<typename decltype(event::Event::stwtdcHits)::iterator, bool>(ir.first, true);
}

// F1
////

bool
CSDigitTraits<CS::ChipF1::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                      , DetID & did
                                                      , const CS::ChipF1::Digit & f1Digit ) {
    assert( c.kF1 == did.chip() );
    WireID wID(did.payload());
    wID.wire_no(f1Digit.GetChannel());
    did.payload( wID.id );
    return true;
}

std::pair<typename decltype(event::Event::f1Hits)::iterator, bool>
CSDigitTraits<CS::ChipF1::Digit>::emplace_digit( event::Event & e
                                               , event::LocalMemory & lmem
                                               , DetID did
                                               , const CS::ChipF1::Digit & f1Digit
                                               ) {
    auto it = e.f1Hits.emplace( did, lmem.create<event::F1Hit>(lmem) );
    HitType & f1Hit = *(it->second);
    util::reset(f1Hit);

    f1Hit.rawData = lmem.create<event::RawDataF1>();
    util::reset(*(f1Hit.rawData));

    f1Hit.rawData->sourceID = f1Digit.GetSourceID();
    f1Hit.rawData->portID   = f1Digit.GetPort();

    f1Hit.rawData->channel  = f1Digit.GetChannel();
    f1Hit.rawData->channelPosition = f1Digit.GetChannelPos();

    f1Hit.rawData->time        = f1Digit.GetTime();
    f1Hit.rawData->timeUnit    = f1Digit.GetTimeUnit();
    f1Hit.rawData->timeDecoded = f1Digit.GetTimeDecoded();
    f1Hit.rawData->timeReference = f1Digit.GetTimeReference();
    return {it, true};
}

}  // namespace na64::ddd
}  // namespace na64

