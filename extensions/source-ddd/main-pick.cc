#include "fileMap.hh"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <regex>

static void
_usage_info(const char * appName, std::ostream & os) {
    os << "NA64 simplistic event picking app." << std::endl
       << "Usage:" << std::endl
       << "    $ " << appName << " <input-file> <initial-offset> <event-id> <output-file>" << std::endl
       << "Will open <input file>, set the byte reading event by <initial-offset>"
          " and will keep to silently read events until event with <event-id> is"
          " met. Once event with <event-id> is met, its binary data will be"
          " written into <output-file> and app quits normally." << std::endl;
}

int
main(int argc, char * argv[]) {
    if( argc == 1 || (argc == 2 && (std::string(argv[1]) == "-h")) ) {
        _usage_info(argv[0], std::cout);
        return 0;
    }
    if(argc != 5) {
        std::cerr << argv[0] << " error: bad command line arguments"
            " (expected 4, got " << argc - 1 << ")." << std::endl;
    }
    // Parse event ID, expecting it to match format of three numbers:
    // <run>-<spill>-<event-in-spill>
    // Delimeters between three numbers may be any of (-._:/)
    na64dp::EventID eid; {
        std::cmatch m;
        if(!std::regex_match( argv[3], m, std::regex("^(\\d+)[-/:@](\\d+)[-/:@](\\d+)$"))) {
            std::cerr << "Error: \"" << argv[3] << "\" does not match expected"
                " event ID format (expected <run#>/<spill#>/<event#>)."
                << std::endl;
            return 2;
        }
        eid = na64dp::EventID( stoul(m[1]), stoul(m[2]), stoul(m[3]) );
    }
    // Get numerical offset
    na64dp::ddd::DDDIndex::SpillDataLayout::FilePostion_t offset
            = std::stoul(argv[2]);
    // Open file
    std::ifstream is(argv[1], std::ios::binary);
    is.seekg(offset);

    std::vector<uint8_t> data;
    try {
        data = na64dp::ddd::pick_event(is, eid);
    } catch(na64dp::ddd::error::MagicCheckFailed & e) {
        std::cerr << "Error: bad magic number for an event header"
                     " (wrong initial offset or corrupted file): "
                  << e.what()
                  << std::endl;
        return 102;
    } catch(na64dp::ddd::error::SourceDepleted & e) {
        std::cerr << "Error: unable to read the source"
                     " (no such event in file or file is corrupted): "
                  << e.what()
                  << std::endl;
        return 103;
    } catch(na64dp::ddd::error::FileMapError & e) {
        std::cerr << "Error: generic DDD source error: "
                  << e.what()
                  << std::endl;
        return 101;
    } catch(std::exception & e) {
        std::cerr << "Error: generic std error: "
                  << e.what()
                  << std::endl;
        return 104;
    }
    is.close();
    
    std::cout << "Event " << eid << " of size " << data.size() << "b read."
        << std::endl;

    {
        std::ofstream os(argv[4]);
        os.write(reinterpret_cast<char*>(data.data()), data.size());
    }

    return 0;
}

