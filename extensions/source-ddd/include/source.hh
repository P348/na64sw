#pragma once

#include "na64calib/dispatcher.hh"
#include "na64calib/evType.hh"
#include "na64event/data/event.hh"
#include "na64sw-config.h"

#include <DaqEventsManager.h>

#include "na64dp/abstractEventSource.hh"
#include "na64util/mem/pools.hh"
#include "fileMap.hh"

#include <mutex>
#include <unordered_map>

namespace na64dp {

namespace errors {

///\brief Thrown if hit is not unique while it is expected
///
/// Depending on event relation logic, this exception might appear when event
/// should provide a unique hit per sensitive element, but according to the
/// data read this condition is violated (say, multiple digits stored in a raw
/// event for same calorimeter cell).
class HitCreationError : public GenericRuntimeError {
    mutable char errBf[1024];
public:
    DetID_t did;
    std::string daqName
              , daqVerbose
              ;

    HitCreationError( const char * details
                    , const CS::Chip::Digit & dgt
                    , DetID_t did_=0x0
                    ) throw();
    const char * what() const throw() override;
};

}  // namespace ::na64dp::errors

namespace ddd {

template<typename CSDigitT> struct CSDigitTraits;  // fwd

/**\brief DaqDataDecoding library CS::EventsManager adapter.
 *
 * Represents a DDD events data source.
 *
 * \todo Get rid of DaqEventsManager class in favor of custom file/xrootd/date interface.
 * \todo Generate offsets index (na64ee) by option.
 * \todo Resolve issue with wrong kin "STT"
 */
class DDDEventSource : public AbstractNameCachedEventSource
                     , public calib::Handle<EventBitTags>
                     {
protected:
    /// Number of decoding failures occured
    size_t _decodingFailures;
    /// Names of unknown detectors and times they were met duing the iteration
    std::map<std::string, size_t> _unknownDets;
    /// Set of permitted event types
    std::set<CS::DaqEvent::EventType> _permitTypes;
    /// Set of tbnames to be ignored
    ///\todo make here a more complex object that will perform partial/wildcard match
    std::set<std::string> _tbnames2ignore;
    /// A callable to filter out events by some criterion
    std::function<bool(EventID)> _filterEventsByID;

    /// Filled for informative log: detectors met during object lifetime
    std::unordered_map<std::string, size_t> _detectorsMet;
    /// Filled for informative log: event types met during object lifetime
    std::unordered_map<CS::DaqEvent::EventType, size_t> _nativeEventTypesMet;
    /// Filled for informative log: trigger codes met during object lifetime
    std::unordered_map<CS::uint32, size_t> _nativeTriggersMet;

    decltype(event::Event::evType) _serviceTypeCode, _physicsTypeCode;
protected:
    //virtual void handle_update( const nameutils::DetectorNaming & ) override;

    /// On successfull insertion returns complete detector ID, on failure - 0x0
    template<typename DigitT>
    std::pair<DetID_t, bool> _fill_hit( event::Event & e
                     , event::LocalMemory & lmem
                     , DetID incompleteDID
                     , const CS::Chip::Digit * abstractDigitPtr
                     ) {
        const DigitT & dgt = dynamic_cast<const DigitT &>(*abstractDigitPtr);
        typedef CSDigitTraits<DigitT> Traits;
        if( Traits::complete_detector_id( naming(), incompleteDID, dgt ) ) {
            // ^^^ note: complete_detector_id() modifies incompleteDID, so
            // it is complete afterwards:
            auto ir = Traits::emplace_digit(e, lmem, incompleteDID, dgt);
            if(!ir.second) {
                throw errors::HitCreationError( "Complete detector ID does"
                        " not uniquely identify a hit."
                        , dgt
                        , incompleteDID
                        );
            }
            return std::pair<DetID_t, bool>(ir.first->first, true);
        }
        return std::pair<DetID_t, bool>(incompleteDID, false);
    }

    void _event_fill( event::Event &, event::LocalMemory &, CS::Chip::Digits & );

    /// Updates _serviceTypeCode and _physicsTypeCode;
    void handle_update(const EventBitTags &) override;
protected:
    ///\brief Shall modify instance's state by reading next event in queue
    ///
    ///\returns `false` if failed to read new event and reading shall be abrupt
    virtual bool _get_next_event() = 0;
    /// Returns reference to current DDD event instance
    virtual CS::DaqEvent & _current_event() = 0;
    /// Returns (decoded) digits array for current event
    virtual CS::Chip::Digits & _get_current_event_digits() = 0;
public:
    /// Common ctr
    DDDEventSource( calib::Manager & mgr
                  , std::vector<std::string> permittedEvTypes
                  , const std::set<std::string> & tbnames2ignore
                  , log4cpp::Category & logCat
                  , const std::string & detNamingClass="default"
                  , iEvProcInfo * epi=nullptr
                  , std::function<bool(EventID)> filterEventsByID=nullptr
                  );
    virtual ~DDDEventSource() {}


    /// Interface implementation: reads an event with DDD manager.
    bool read( event::Event &, event::LocalMemory & lmem ) override;

    /// Prints reports
    void finalize() override;
    /// Returns number of event decoding failures
    size_t decoding_failures() const { return _decodingFailures; }
    /// Returns map of TBName vs occurance counter
    const std::map<std::string, size_t> & unknown_detectors() const { return _unknownDets; }
};

///\brief Manager-based implementation of DDD source
///
/// DaqDecoding library provides an abstraction for reading events based on
/// so-called "manager". This manager is capable to read events using DATE or
/// RFIO libraries supporting UDP networking etc. It does not imply a
/// lookahead functionality, however.
///
/// Use this implementation for generic-purpose DDD event reading, when file
/// indexing/event lookahead functionality is not needed.
class DDDEventSource_Mgr : public DDDEventSource {
protected:
    /// Ptr to DaqDataDecoding events manager instance.
    CS::DaqEventsManager * _dddMgr;
    ///\brief Uses DaqEventsManager to retrieve next event.
    ///
    /// Forwards execution to `DaqEventsManager::ReadEvent()` and, if succeed,
    /// to `DaqEventsManager::DecodeEvent()` returning its result.
    ///
    ///\returns `false` if failed to read new event and reading shall be abrupt
    bool _get_next_event() final;
    ///\brief Returns reference to current DDD event instance
    ///
    /// Forwards execution to `DaqEventsManager::GetEvent()`
    CS::DaqEvent & _current_event() final;
    /// Forwards execution to `DaqEventsManager::GetEventDigits()`
    CS::Chip::Digits & _get_current_event_digits() final;
public:
    /// Default ctr: immediately instantiates DDD manager with all the
    /// neccessary assets: CORAL maps directory and list of inputs (files or
    /// network sockets)
    DDDEventSource_Mgr( calib::Manager & mgr
                      , const std::string & coralMapsDir
                      , const std::vector<std::string> & inputs
                      , std::vector<std::string> permittedEvTypes
                      , const std::set<std::string> & tbnames2ignore
                      , log4cpp::Category & logCat
                      , const std::string & detNamingClass="default"
                      , iEvProcInfo * epi=nullptr
                      , std::function<bool(EventID)> filterEventsByID=nullptr
                      );

    /// Getter for DDD manager
    CS::DaqEventsManager & cs_manager_ref() { return *_dddMgr; }
    /// Getter for DDD manager (const version)
    const CS::DaqEventsManager & cs_manager_ref() const { return *_dddMgr; }
};

class DDDIndex;
///\brief implementation of DDD event source with indexes
///
/// This implementation provides some additional functionality on the DDD events
/// reading by leveraging events indexing technics like arbitrary events
/// retrieval, metadata fetch, etc. It is particularly useful for event
/// display or othe single event picking applications.
///
/// Depending on input provided, the source item may contribute in the global
/// events index that is further retrievable by the external apps (like event
/// display) and can be used to arbitrarily retrieve events (random access).
/// In case of files named in a standard form of raw data, this class
/// may attempt to retrieve chunk layout from DDD server.
class DDDEventSource_Indexed : public DDDEventSource {
public:
    struct iIndexResolver;  // fwd

    /// Describes metadata info retrieved for particular source item in the list
    class SourceMetadata {
    private:
        /// Original resource qualifier (filename, uri, etc)
        const std::string _id;
    protected:
        /// Obtained index (can be null)
        DDDIndex * _subIndex;
        /// Guards access to this metadata instance
        std::mutex _mutex;
    public:
        std::mutex & mutex() { return _mutex; }

        SourceMetadata(const std::string & id_) 
                : _id(id_), _subIndex(nullptr), _mutex()
                {}
        ~SourceMetadata();

        /// Index setter
        void index(const DDDIndex &);
        /// Index getter (check with `has_index()`)
        const DDDIndex & index() const;
        /// Returns `false` if no index acquired for the source
        bool has_index() const { return _subIndex; }

        // ...

        friend class DDDEventSource_Indexed;
    };

    /// Abstract base to resolve indexes for the source instance
    struct iIndexResolver {
        /// May return nullptr on failure
        virtual std::shared_ptr<SourceMetadata> resolve_index(const std::string &) = 0;
    };

    /// Index storage for designated inputs
    class Index : public std::vector< std::shared_ptr<SourceMetadata> > {
    protected:
        /// Events index obtained for given source data
        DDDIndex * _indexPtr;
        /// Mutex guarding `_indexPtr`
        std::mutex _indexPtrMutex;

        Index(const std::vector<std::string> & inputs, std::list<iIndexResolver*> & );

        friend class DDDEventSource_Indexed;
    };
private:
    /// Index instance
    Index _index;
    /// Iterator to source name/id in collection
    std::vector< std::shared_ptr<SourceMetadata> >::iterator _cSourceIDRit;
    /// Ptr to current stream
protected:
    ///\brief Retrieves next event; may switch source if one is depleted.
    ///
    ///\returns `false` if failed to read new event
    bool _get_next_event() final;
    /// Returns reference to current DDD event instance
    CS::DaqEvent & _current_event() final;
    /// Returns current event's digits
    CS::Chip::Digits & _get_current_event_digits() final;
public:
    ///\brief Setups the event source instance to navigate through provided files
    ///
    /// Besides of usual ctr args, may accept DDD srv endpoint URL to retrieve
    /// indexes on particular run and other semantical information on the
    /// events in use.
    DDDEventSource_Indexed( calib::Manager & mgr
                          , const std::string & coralMapsDir
                          , const std::vector<std::string> & inputs
                          , std::list<iIndexResolver*> & resolvers
                          , std::vector<std::string> permittedEvTypes
                          , const std::set<std::string> & tbnames2ignore
                          , log4cpp::Category & logCat
                          , const std::string & detNamingClass="default"
                          , iEvProcInfo * epi=nullptr
                          , std::function<bool(EventID)> filterEventsByID=nullptr
                          );

    #if 0
    Features_t features() const override;  // TODO
    bool read_prev( event::Event &, event::LocalMemory & lmem ) override;  // TODO
    bool read_by_id( EventID, event::Event &, event::LocalMemory & lmem ) override;  // TODO
    size_t list_events( std::vector<EventID> &, size_t nPage, size_t nPerPage ) override;  // TODO
    void append_state_info(YAML::Node &,
            const std::unordered_map<std::string, std::string> &) override;  // TODO
    #endif
};

}  // namespace ddd
}  // namespace na64dp

