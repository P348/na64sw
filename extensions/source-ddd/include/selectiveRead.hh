#pragma once

#include "fileMap.hh"

#include <set>
#include <map>
#include <regex>
#include <fstream>

namespace na64dp {
namespace ddd {

/**\brief Helper class to keep choosen event IDs
 *
 * Since event IDs are pretty often has contiguous sequences, keeping them
 * in such a form might be useful for memory efficiency. Also, sorting event
 * IDs should greatly enhance event reading procedure.
 *
 * This class assumes that events in spill are given in arbitrary order to
 * `consider()` method, sorts them and unite contiguous ranges.
 *
 * \todo Implement efficient storage? Not implemented in the way it's
 *       described, actually.
 * */
class SelectedEventsInSpill
        : public std::set<DDDIndex::EventNo_t> {
public:
    void consider(DDDIndex::EventNo_t newEvNo);
};

class SelectedEvents : public std::map< DDDIndex::RunNo_t
                                      , std::map< DDDIndex::SpillNo_t
                                                , SelectedEventsInSpill
                                                >
                                      > {
public:
    /// Adds event ID to be considered
    void consider( DDDIndex::RunNo_t runNo
                 , DDDIndex::SpillNo_t spillNo
                 , DDDIndex::EventNo_t evNoInSpill
                 );
    /// Returns total number of events
    size_t n_events() const;
};

/**\file
 * \brief Parser for event list file produced by ROOT
 *
 * See https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/329#note_6372566
 * for details.
 * */
void collect_event_ids_from_ROOT_scan( std::istream & is
                                     , SelectedEvents & dest
                                     );

}  // namespace ::na64dp::ddd
}  // namespace na64dp

