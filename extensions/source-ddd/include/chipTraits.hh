#pragma once

#include "na64sw-config.h"

#include <ChipSADC.h>
#include <ChipAPV.h>
#include <ChipNA64TDC.h>

#include "na64detID/detectorID.hh"
#include "na64event/data/event.hh"
#include "source.hh"

namespace na64dp {
namespace ddd {

/// Specification defines how to translate the TBName postfix and some
/// additional hit payload to complete detector ID.
template<typename CSDigitT> struct CSDigitTraits;

template<>
struct CSDigitTraits<CS::ChipSADC::Digit> {
    typedef event::SADCHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipSADC::Digit & );

    /// Copies SADC data (SADC DDD chip type)
    static std::pair<typename decltype(event::Event::sadcHits)::iterator, bool>
    emplace_digit( event::Event &
                 , event::LocalMemory &
                 , DetID completeDID
                 , const CS::ChipSADC::Digit &
                 );
};

template<>
struct CSDigitTraits<CS::ChipAPV::Digit> {
    typedef event::APVHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipAPV::Digit & );

    /// Copies APV data (APV DDD chip type)
    static std::pair<typename decltype(event::Event::apvHits)::iterator, bool>
    emplace_digit( event::Event & e
                 , event::LocalMemory & lmem
                 , DetID completeDID
                 , const CS::ChipAPV::Digit &
                 );
};

template<>
struct CSDigitTraits<CS::ChipNA64TDC::Digit> {
    typedef event::StwTDCHit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipNA64TDC::Digit & );

    /// Copies raw STWTDC data (NA64TDC type of DDD)
    static std::pair<typename decltype(event::Event::stwtdcHits)::iterator, bool>
    emplace_digit( event::Event & e
                 , event::LocalMemory & lmem
                 , DetID completeDID
                 , const CS::ChipNA64TDC::Digit &
                 );
};

template<>
struct CSDigitTraits<CS::ChipF1::Digit> {
    typedef event::F1Hit HitType;

    static bool complete_detector_id( const DDDEventSource::NameCache &
                                    , DetID &
                                    , const CS::ChipF1::Digit & );

    /// Copies raw STWTDC data (NA64TDC type of DDD)
    static std::pair<typename decltype(event::Event::f1Hits)::iterator, bool>
    emplace_digit( event::Event & e
                 , event::LocalMemory & lmem
                 , DetID completeDID
                 , const CS::ChipF1::Digit &
                 );
};

}  // namespace na64::ddd
}  // namespace na64

