#pragma once

/**\file
 * \brief Declares DDD file (chunk) indexing API
 *
 * This header summirizes API initially implemented in `na64ee` library
 * (https://gitlab.cern.ch/P348/na64-event-exchange). This code was migrated
 * merged with NA64sw project.
 * */

#include "na64util/na64/event-id.hh"

#include <map>
#include <unordered_map>
#include <vector>
#include <stdexcept>

namespace CS {
class DaqEvent;  // FWD
}  // namespace CS

namespace na64dp {
namespace ddd {

namespace aux {
///\brief Complementary function for `CS::DaqEvent::stringEventType()`
///
/// Somehow missed in the original lib. Converts string description to numeric
/// code.
int cs_daq_event_type_code_by_string(const char *);
}

/// Global constant providing size of the CS::DaqEvent::Header
extern const size_t gDDDEventHeaderSize;

namespace error {
class FileMapError : public std::runtime_error {
public:
    FileMapError(const char * msg) : std::runtime_error(msg) {}
};

///\brief Event header has magic number mismatch
///
/// This error indicates that the data piece considered as
/// CS::DaqEvent's header is not really a header. Most of the time it means
/// wrong offset or file corruption.
class MagicCheckFailed : public FileMapError {
public:
    MagicCheckFailed( const char * msg ) : FileMapError(msg) {}
};

///\brief File ended prematurely
class SourceDepleted : public FileMapError {
public:
    SourceDepleted() : FileMapError("data source depleted") {}
    SourceDepleted( const char * msg )
        : FileMapError(msg) {}
};
}  // namespace ::na64dp::ddd::error

/// This function causes setting the position of input stream to be set at
/// particular event ID number.
bool
direct_look_up_event( std::istream &, const EventID & );

/**@brief container structure representing data layout inside one spill.
 *
 * This auxilliary struct keeps interim data describing the events layout
 * inside spill written in a file (typically, chunk).
 * */
struct SpillDataLayout_t {
    typedef uint8_t  ChunkNo_t;
    typedef uint16_t SpillNo_t;
    typedef uint32_t EventNo_t;
    typedef std::basic_istream<char>::pos_type FilePostion_t;
    /// Field referring to particular chunk number.
    ChunkNo_t chunkNo;
    /// Ordered map of event positions info in form (event ID, bytesOffset).
    /// Can be sparsed.
    std::map<EventNo_t, FilePostion_t> eventPositions;
    /// Event type counts in spill. Key is the value of `CS::DaqEvent::EventType`
    /// enum
    std::unordered_map<int, size_t> eventTypeCounts;
    /// Counters for unique trigger combinations
    std::unordered_map<uint32_t, size_t> triggerCounts;
    /// Range of events no in chunk; this is number of events that can be
    /// less than last event's number ("event in spill").
    EventNo_t eventsInSpill;
    /// Spill logical start time
    std::pair<time_t, uint32_t> firstEventTime;
    /// Spill logical end time
    std::pair<time_t, uint32_t> lastEventTime;
    /// Spill physicsl start time
    std::pair<time_t, uint32_t> firstPhysEventTime;
    /// Spill physical end time
    std::pair<time_t, uint32_t> lastPhysEventTime;

    /// Merges with another instance. Returns number of unique entries.
    size_t merge( const SpillDataLayout_t & src );
    /// Merges with another instance. Returns number of unique entries.
    size_t merge( const std::map<EventNo_t, FilePostion_t> & src );
    /// Returns pair of event number and its offset for the nearest
    /// preceding event. Can raise NotInSpill exception.
    std::pair<EventNo_t, FilePostion_t> get_preceding_event_offset(
                                                        EventNo_t ) const;
};

/**@brief An index class describing DDD file's metadata.
 *
 * This class imposed at basic abstraction layer and probably has never be
 * used by user code unless the cases when structure other than physical
 * chunks has to be utilized. The chunks with their aux info are considered
 * in descendant class.
 *
 * Instances of this class usually corresponds to physical artifacts (files).
 * It caches positional info for events. This information is structured in
 * sorted associative arrays. Despite the unsorted containers will be faster
 * the sorted ones is more readable.
 * */
class DDDIndex {
public:
    typedef SpillDataLayout_t SpillDataLayout;
    typedef SpillDataLayout::ChunkNo_t ChunkNo_t;
    typedef SpillDataLayout::SpillNo_t SpillNo_t;
    typedef SpillDataLayout::EventNo_t EventNo_t;
    typedef SpillDataLayout::FilePostion_t FilePostion_t;

    typedef uint16_t RunNo_t;
    /// Parent structure for run.
    typedef std::map<SpillNo_t, SpillDataLayout> RunDataLayout;
private:
    /// Data layouts and stats
    std::map<RunNo_t, RunDataLayout> _runs;
public:
    DDDIndex();
    DDDIndex( RunNo_t rn, const RunDataLayout & );
    DDDIndex( const DDDIndex & );
    virtual ~DDDIndex() {}

    /// Constructs index from JSON string (uses yamlcpp)
    static DDDIndex from_JSON_str(const std::string &);

    /// Merges info from given index instance into current.
    size_t merge( const DDDIndex & );
    /// Merges obtained list of spill layout information into given dictionary.
    size_t merge( RunNo_t rn, const RunDataLayout & );
    /// Writes itself to bytestream.
    void write( std::ostream & ) const;
    /// Prints content in human-readable form. Can not be parsed.
    virtual void print_to_ostream( std::ostream & ) const;
    /// Prints content to JSON data.
    virtual void to_JSON_str( std::ostream &/*, bool prettyPrint=true*/ ) const;
    // Adds information from JSON string. (TODO)
    //virtual void from_JSON_str( std::istream & );
    /// Returns position in the stream corresponding to event specified by ID.
    FilePostion_t get_event_stream_pos( std::istream &
                                      , na64sw_EventID_t
                                      , FILE * dbgStream=nullptr ) const;
    /// Calculates the size of the buffer needed for serialization.
    size_t serialized_size() const;
    /// Serializes the object returning actual used bytes length.
    size_t serialize( uint8_t * bytes, size_t bytesLength ) const;
    /// Forms spill layout information for given stream.
    static void index_chunk( std::istream & is,
                             RunDataLayout & destLst,
                             size_t nDuty,
                             RunNo_t expectedRunNo,
                             ChunkNo_t defaultChunkNo,
                             FILE * dbgStream=nullptr );
    /// Deserialization class method returning pointer to dynamically allocated
    /// instance.
    static DDDIndex * deserialize( const uint8_t * bytes );
};  // class DDDIndex


/**@struct ChunkID
 * @brief Struct uniquely identifying raw data file.
 *
 * This union is used to identify data files (called "chunks") in NA64
 * experimental data.
 * */
struct ChunkID {
    DDDIndex::RunNo_t   runNo;
    DDDIndex::ChunkNo_t chunkNo;
    ChunkID( ) : runNo(0), chunkNo(0) {}
    ChunkID( DDDIndex::RunNo_t rn, DDDIndex::ChunkNo_t cn ) :
                                                    runNo(rn), chunkNo(cn) {}
};

std::ostream & operator<<(std::ostream & os, const ChunkID &);

#if 0
/**@brief Utility function reading one raw event.
 *
 * Upon successfull reading, the newly-allocated event instance will be
 * returned.
 *
 * @exception fileNotFound when unable to reach the file
 * @exception eventNotFound when failed to look-up an event at the given
 *            position.
 * */
std::vector<uint8_t>
read_one_new_raw_event( const std::string & filePath,
                        const DDDIndex & idx,
                        na64dp::EventID );
#endif

std::vector<uint8_t> pick_event( std::istream & is
                               , EventID eid
                               );

}  // namespace ::na64dp::ddd
}  // namespace na64dp

