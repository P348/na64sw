#include "fileMap.hh"

#include <unistd.h>
#include <iostream>
#include <strings.h>
#include <fstream>

struct AppConfig {
    std::string inputFile;
    size_t sparseness;
    std::string outputFile;
    bool dbgOut;
};

static void
_print_usage(std::ostream & os, const char * appName, const AppConfig & appCfg) {
    os << "Produces index file for file of DaqDataDecoding format." << std::endl
       << "Usage:" << std::endl
       << "    $ " << appName << " [-v] [-s <nEvts=" << appCfg.sparseness
       << ">] [-o <outFile=" << appCfg.outputFile << ">] <inputFile>"
       << std::endl
       << "Reads events from given <inputFile> collecting byte offsets for every N-th"
          " event (N is defined by `-s' parameter). Collected data"
          " will be written as JSON object into <outfile> if `-o` provided"
          ". Optionally, indexing state evaluation report will be printed to"
          " stdout by `-v` option."
       << std::endl;
}

static int
_configure_app(int argc, char * argv[], AppConfig & appCfg) {
    int opt;
    while((opt = getopt(argc, argv, "hvs:o:")) != -1) {
        switch(opt) {
        case 'h': {
            _print_usage(std::cout, argv[0], appCfg);
            return 1;
        } break;
        case 's': {
            long int sparseness = atol(optarg);
            if(sparseness < 1) {
                fputs("Bad value for sparseness specified by command line arg (<1).\n", stderr);
                return -1;
            }
            appCfg.sparseness = sparseness;
        } break;
        case 'o': {
            appCfg.outputFile = optarg;
        } break;
        case 'v': {
            appCfg.dbgOut = true;
        } break;
        default:
            std::cerr << "Bad command line option." << std::endl;
            _print_usage(std::cerr, argv[0], appCfg);
            return -1;
        };
    }
    if(argc - optind != 1) {
        std::cerr << "Command line error: expected input file argument after options."
            << std::endl;
        return -1;
    }
    appCfg.inputFile = argv[optind];
    return 0;
}

int
main(int argc, char * argv[]) {
    // configure application
    AppConfig appCfg = {"", 1024, "index.json", false};
    int rc;
    if( (rc = _configure_app(argc, argv, appCfg)) < 0 ) {
        std::cerr << "Abort due to configuration error." << std::endl;
        return 1;
    }
    if(rc > 0) {
        return 0;
    }
    //
    const char * fname = rindex( appCfg.inputFile.c_str(), '/' );
    if(*fname == '/') ++fname;
    int runNo = -1,
        chunkNo = -1;
    sscanf( fname, "cdr%d-%d.dat", &chunkNo, &runNo );
    if( -1 == runNo || -1 == chunkNo ) {
        fprintf( stderr,
                 "Unable to interpret \"%s\" filename as chunk descriptor.\n"
                 "Expected: \"cdr-<chunkNo>-<runNo>.dat\".\n", fname);
        return -1;
    }
    chunkNo -= 1000;  // todo: configurable?

    std::ifstream is( appCfg.inputFile.c_str() );
    if( !is ) {
        fprintf(stderr, "Unable to open file %s.\n", appCfg.inputFile.c_str());
        return EXIT_FAILURE;
    }

    //
    // Extract index

    // Declare indexing object:
    na64dp::ddd::DDDIndex::RunDataLayout lst;
    // Fill one indexing objectect with duty number 275 making the
    // indexing procedure remember offset of each 275-th event:
    na64dp::ddd::DDDIndex::index_chunk(
                /* std::istream instance with data ... */ is,
                /* where to fill indexes ............. */ lst,
                /* duty parameter (can be 0) ......... */ appCfg.sparseness,
                /* guessed run number ................ */ runNo,
                /* guessed chunk number .............. */ chunkNo,
                /* logging stream C-handle ........... */ appCfg.dbgOut ? stdout : NULL );
    is.close();

    //
    // Save obtained index

    na64dp::ddd::DDDIndex idx(runNo, lst);
    std::ofstream * filePtr = nullptr;
    if( !appCfg.outputFile.empty() ) {
        filePtr = new std::ofstream(appCfg.outputFile);
    }
    idx.to_JSON_str(appCfg.outputFile.empty() ? std::cout : *filePtr);
    if( !appCfg.outputFile.empty() ) {
        delete filePtr;
    }
    #if 0
    if( !appCfg.outputFile.empty() ) {
        na64dp::ddd::DDDIndex idx(runNo, lst);
        //idx.print_to_ostream(std::cout);
        idx.to_JSON_str(std::cout);  // XXX
        // Serialize obtained data to file:
        size_t bufLength = idx.serialized_size();
        uint8_t * bytes = new uint8_t [bufLength];
        idx.serialize( bytes, bufLength );
        
        std::ofstream os( appCfg.outputFile, std::ofstream::out | std::ofstream::trunc );
        if( !os ) {
            fprintf( stderr, "Unable to open file %s for writing.\n", appCfg.outputFile.c_str() );
            return EXIT_FAILURE;
        }
        os.write( (const char*) bytes, bufLength );
        os.close();
        delete [] bytes;
    }
    #endif
    return 0;
}

