cmake_minimum_required( VERSION 3.19 )

include(FindDDD)

if( NOT DaqDataDecoding_FOUND )
    message(WARNING "DaqDataDecoding library is not found, related source extension will not be built.")
    return()
endif( NOT DaqDataDecoding_FOUND )

#
# Data source target

# Find CORAL maps dir
find_path( CORAL_MAPS_DIR
    NAMES 2015.xml/ECAL.xml 2015.xml/HCAL.xml
    HINTS ${DaqDataDecoding_INCLUDE_DIR}/../../maps/
          ${DaqDataDecoding_INCLUDE_DIR}/../../../../maps/
    NO_DEFAULT_PATH
    NO_CACHE
    )
if(CORAL_MAPS_DIR)
    message(STATUS "DDD source extension: Found CORAL maps for NA64 at ${CORAL_MAPS_DIR}" )
    add_compile_definitions( CORAL_MAPS_DIR="${CORAL_MAPS_DIR}" )
else(CORAL_MAPS_DIR)
    message(WARNING "DDD source extension: CORAL DAQ maps dir for NA64 is not found.")
endif(CORAL_MAPS_DIR)

#set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# Lib
set( na64ddd_LIB dddMap )
add_library(${na64ddd_LIB} src/fileMap.cc)
set_target_properties(${na64ddd_LIB} PROPERTIES
    PUBLIC_HEADER "include/fileMap.hh;include/selectiveRead.hh")
# Since this lib will be included into loadable module, always force -fPIC
set_property(TARGET ${na64ddd_LIB} PROPERTY POSITION_INDEPENDENT_CODE 1)
target_link_libraries(${na64ddd_LIB} PUBLIC
    ${DaqDataDecoding_LIB}
    ${date_monitor_LIB}
    ${EXPAT_LIBRARIES}
    )
target_include_directories (${na64ddd_LIB}
    PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    PUBLIC $<INSTALL_INTERFACE:include/${CMAKE_PROJECT_NAME}/extensions/ddd> )
# Get path to the linked dir to push it into RPATH
get_filename_component(DaqDataDecoding_DIR ${DaqDataDecoding_LIB} DIRECTORY)
message(STATUS "DDD module has rpath appended to ${DaqDataDecoding_DIR}")
set_target_properties(${na64ddd_LIB} PROPERTIES VERSION ${na64sw_VERSION}
                                              SOVERSION ${na64sw_SOVERSION}
                        # push rpath to lib
                        INSTALL_RPATH ${DaqDataDecoding_DIR}
                        BUILD_RPATH ${DaqDataDecoding_DIR}
                        )
target_link_libraries(${na64ddd_LIB}
    PUBLIC ${DaqDataDecoding_LIB}
           ${date_monitor_LIB}
           ${EXPAT_LIBRARIES}
           ${event_LIB}
           ${calib_LIB}
           )
# TODO: DaqDataDecoding_LIB must bring up this include
target_include_directories(${na64ddd_LIB} SYSTEM PUBLIC ${DaqDataDecoding_INCLUDE_DIR} )

# Plugin
set( na64ddd_MODULE ddd )
add_library(${na64ddd_MODULE} MODULE src/chipTraits.cc src/source.cc)
target_link_libraries(${na64ddd_MODULE} ${na64ddd_LIB})
target_include_directories (${na64ddd_MODULE}
    PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> )

#target_include_directories(${na64ddd_LIB} SYSTEM PUBLIC ${DaqDataDecoding_INCLUDE_DIR} )
#target_include_directories(${na64ddd_LIB} PRIVATE $<TARGET_PROPERTY:${dp_LIB},INTERFACE_INCLUDE_DIRECTORIES>)


message(STATUS "Enabled DDD event source extension." )
# install commmon lib
install(TARGETS ${na64ddd_LIB} EXPORT ${na64ddd_LIB} LIBRARY
    PUBLIC_HEADER DESTINATION include/${CMAKE_PROJECT_NAME}/extensions/ddd/
)
# install modules
install(TARGETS ${na64ddd_MODULE} EXPORT ${na64ddd_MODULE} LIBRARY
    DESTINATION ${NA64SW_STDEXT_DIR}
    PUBLIC_HEADER DESTINATION include/${CMAKE_PROJECT_NAME}/extensions/ddd/
)

install( FILES source-ddd.yaml DESTINATION share/na64sw )
# This is to provide working configuration in the build directory
add_custom_target( ddd-ext-bld-symlink ALL
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_BINARY_DIR}/lib${na64ddd_MODULE}.so ../${na64ddd_MODULE} )
add_custom_target( ddd-ext-bld-symlink-cfg ALL
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_SOURCE_DIR}/source-ddd.yaml ../../share/na64sw/source-ddd.yaml
    DEPENDS mkdir-share-noinstall
    )

set(na64sw_ddd_index_EXEC na64sw-ddd-file-index)
add_executable(${na64sw_ddd_index_EXEC} main-index.cc)
target_link_libraries(${na64sw_ddd_index_EXEC} PUBLIC ${na64app_LIB} ${na64ddd_LIB})
install(TARGETS ${na64sw_ddd_index_EXEC} EXPORT ${na64sw_ddd_index_EXEC} RUNTIME)

set(na64sw_ddd_pick_EXEC na64sw-ddd-pick-event)
add_executable(${na64sw_ddd_pick_EXEC} main-pick.cc)
target_link_libraries(${na64sw_ddd_pick_EXEC} PUBLIC ${na64app_LIB} ${na64ddd_LIB})
install(TARGETS ${na64sw_ddd_pick_EXEC} EXPORT ${na64sw_ddd_pick_EXEC} RUNTIME)

set(na64sw_ddd_pickd_EXEC na64sw-ddd-pick-event-server)
add_executable(${na64sw_ddd_pickd_EXEC} main-pick-server.cc)
#target_link_libraries(${na64sw_ddd_pickd_EXEC} PUBLIC ${na64app_LIB} ${na64ddd_LIB})
target_link_libraries(${na64sw_ddd_pickd_EXEC} PUBLIC ${na64ddd_LIB})
install(TARGETS ${na64sw_ddd_pickd_EXEC} EXPORT ${na64sw_ddd_index_EXEC} RUNTIME)

set_target_properties(
    ${na64sw_ddd_pickd_EXEC} ${na64sw_ddd_pick_EXEC} ${na64sw_ddd_index_EXEC}
    PROPERTIES INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/${NA64SW_STDEXT_DIR}
    )
