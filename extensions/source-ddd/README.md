# DaqDataDecoding (DDD) data source extension for NA64sw

This directory contains source files of runtime extension implementing
data source interface for NA64sw.

Unique feature of this extension and related tools is
event lookup over the entire NA64 data array as well as useful metadata
extraction from raw data.

## Utilities

This dir also keeps some related things: a server implementing RESTful
resources to *index* DDD data on disk into DB and retrieve individual events
by their IDs (`srv/`). *Indexing* is needed to leverage individual events
lookup by keeping event offsets within chunk file with some duty parameter.

## Structure

Executable apps provided as utilities related to the extension:

* `main-index.cc` -- application that produces *index* of some DDD data chunk
* `main-pick-server.cc` -- application designed to communicate individual
  events by tcp socket
* `main-pick.cc` -- simplistic app retrieving individual event from particular
  chunk file, that benefits from initial byte offset knowledge

Note: event-picking daemon demands same execution environment as rest NA64sw
app do.

