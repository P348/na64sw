#include "na64detID/TBName.hh"
#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"

#include "msadc-fft.h"
#include "na64util/str-fmt.hh"

#include <TMath.h>
#include <cmath>
#include <fstream>
#include <sdc-base.hh>
#include <unordered_map>

namespace na64dp {
namespace handlers {

/**\brief Marks SADC hit with 
 *
 * draft
 * */
class SADCLinSumQCanBeZero : public AbstractHitHandler<event::SADCHit> {
protected:
    /// Number of choosen column in overriding CSV file
    size_t _nCol;
    /// If set, a file will be used instead of calibration data
    const std::string _fileSourceOverride;
    /// Mean freqs in use
    std::unordered_map<DetID, float> _thresholds;
public:
    void handle_update(const nameutils::DetectorNaming & nm) override {
        if(!_fileSourceOverride.empty()) {
            // open file and read freqs mean and stddev, line by line
            std::ifstream ifs(_fileSourceOverride);
            std::string line;
            DetID did;
            while(std::getline(ifs, line)) {
                if(line.empty()) continue;
                auto toks = sdc::aux::tokenize(line, ',');
                if(_nCol >= toks.size()) {
                    NA64DP_RUNTIME_ERROR("No column #%d in file %s"
                            , _nCol, _fileSourceOverride.c_str());
                }
                size_t nTok = 0;
                float th = std::nan("0");
                for(const auto & tok : toks) {
                    if(nTok < 4) {
                        if(0 == nTok) {
                            did = nm[tok];
                            ++nTok;
                            continue;
                        } else if(_nCol == nTok) {
                            th = sdc::aux::lexical_cast<float>(tok);
                            ++nTok;
                            continue;
                        } else {
                            ++nTok;
                            continue;
                        }
                    }
                }
                if(!std::isnan(th)) {
                    log().debug("Setting linear sum threshold for %s to %e."
                            , nm[did].c_str(), th );
                    _thresholds.emplace(did, th);
                }
            }
        }
        AbstractHitHandler<event::SADCHit>::handle_update(nm);
    }

    SADCLinSumQCanBeZero( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::string & filePathOverride
                , const int nQ
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _nCol(nQ)
        , _fileSourceOverride(filePathOverride)
        {
        assert(nQ > 0);
    }
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class SADCFFTCanBeZero

bool
SADCLinSumQCanBeZero::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if((!hit.rawData) || (!hit.rawData->fft)) return true;
    auto it = _thresholds.find(did);
    if(_thresholds.end() == it) {
        // TODO: VHCAL is not converted properly to/from string!
        //log().warn("No item \"%s\", %x"
        //        , naming()[did].c_str()
        //        , did.id
        //        );  // TODO: collect warnings
        return true;
    }
    if(!std::isnan(it->second)) {
        hit.rawData->canBeZero = hit.rawData->sum < it->second ? 1 : 0;
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCLinSumQCanBeZero, cdsp, cfg
                , "..."
                ) {
    using namespace na64dp;
    return new handlers::SADCLinSumQCanBeZero( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["fileOverride"].as<std::string>()
            , cfg["q"].as<int>()
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

