#include "na64calib/dispatcher.hh"
#include "na64detID/TBName.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"
#include <log4cpp/Category.hh>
#include <string>

namespace na64dp {
namespace handlers {

/*\brief Sets master time from certain source based on trigger values.
 *
 * Ad-hoc handler setting master time from NA64TDC sources with respect to
 * trigger configuration as on 2023e.
 *
 * \note In there are "prescaled" and "unprescaled" channels, different in
 *       time offset. As for 2023, the following correspondance defined
 *       for them:
 *          2023B (9718 <= e.run && e.run <= 10315):
 *                  unpresc.    presc.
 *          phys    33          53
 *          beam    41          57
 *          rand    37          61
 *
 *          2023A (> 10316)
 *                  unpresc.    presc.
 *          phys    ?           31
 *          beam    ?           30
 *          rand    ?           29
 */
class ExpSetMasterTimeByTrigger2023e
            : public AbstractHandler
            , public calib::Handle<nameutils::DetectorNaming>
            {
public:
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::get(); }

    ExpSetMasterTimeByTrigger2023e( calib::Dispatcher & cdsp
            , log4cpp::Category & L
            , const std::string namingSubclass="default"
            ) : AbstractHandler(L)
              , calib::Handle<nameutils::DetectorNaming>(namingSubclass, cdsp)
              {}

    ProcRes process_event(event::Event & e) {

        std::cout << "XXX "
            << e.id.run_no() << "-" << e.id.spill_no() << "-"
            << e.id.event_no() << ": " << e.masterTime << std::endl;  // XXX
        return kOk;  // XXX !!!

        const decltype(event::Event::trigger)
            phTrigBit   = 0x100,
            beamTrigBit = 0x200,
            rndTrigBit  = 0x400
            ;

        event::StdFloat_t mTime;
        if(beamTrigBit & e.trigger) {
            // if beam trigger bit is set, use unprescaled beam trigger time
            // as master time
            mTime = e.stwtdcHits[naming()["STT0:XY-41"]]->correctedTime;
        } else if(phTrigBit & e.trigger) {
            // otherwise, if phys trigger bit is set, use unprescaled phys
            // trigger time as master time
            mTime = e.stwtdcHits[naming()["STT0:XY-33"]]->correctedTime;
        } else if(rndTrigBit & e.trigger) {
            // otherwise, if random trigger time is set, use unprescaled
            // random trigger time as master time
            mTime = e.stwtdcHits[naming()["STT0:XY-37"]]->correctedTime;
        } else {
            // otherwise event seem to not have valid time, abort event
            // propagation
            return kDiscriminateEvent;
        }

        e.masterTime = 130 - mTime;  //130;  // 123.4

        // XXX, drop non-physics (when not both beam and phys triggers are set)
        //if( ((phTrigBit | beamTrigBit) & e.trigger) != (phTrigBit | beamTrigBit) )
        //    return kDiscriminateEvent;

        return kOk;
    }
};  // class ExpSetMasterTimeByTrigger2023e

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( ExpSetMasterTimeByTrigger2023e, cdsp, cfg
        , "Ad-hoc master time settings for 2023e") {
    auto & L = na64dp::aux::get_logging_cat(cfg);
    return new na64dp::handlers::ExpSetMasterTimeByTrigger2023e( cdsp,
                L, na64dp::aux::get_naming_class(cfg) );
}

