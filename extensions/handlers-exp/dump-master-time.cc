#include "na64dp/abstractHandler.hh"
#include "na64event/data/event.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Saves master time for every event
 *
 * This simple handler is used for direct comparison of master time obtained
 * by different methods, by comparing ASCII dump files. Can be superseded by
 * some generic handler, though.
 * */
class DumpMasterTime : public AbstractHandler {
private:
    std::ofstream _ofs;
public:
    DumpMasterTime(const std::string & filePath) : _ofs(filePath) {}

    ProcRes process_event(event::Event & event) {
        _ofs << event.id.to_str() << "\t" << event.masterTime << std::endl;
        return kOk;
    }
};

}   // namespace ::na64dp::handlers
}   // namespace na64dp

REGISTER_HANDLER(DumpMasterTime, calibDsp, cfg
        , "Dump event ID and master time to ASCII file") {
    return new na64dp::handlers::DumpMasterTime(cfg["file"].as<std::string>());
}

