#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64util/str-fmt.hh"
#include <log4cpp/Category.hh>
#include <cmath>
#include <log4cpp/Priority.hh>

#include <gsl/gsl_fft_halfcomplex.h>

namespace na64dp {
namespace handlers {

static constexpr size_t gWaveArrLength = sizeof(event::RawDataSADC::wave)/sizeof(event::RawDataSADC::wave[0]);

/**\brief Frequence-based threshold setter
 *
 * Calculates mean value and standard deviation for specified frequency ranges
 * and sets the (M)SADC amp. threshold to the defined value.
 *
 * \code{.yaml}
 *     - _type: MSADCFFTSetThrMeanAmp
 *       # 2-component tuple of ints, required
 *       range: ...
 *       # Number of sigma above mean to use; int, opt
 *       nSigma: 3
 * \endcode
 *
 * */
class MSADCFFTSetThrMeanAmp : public AbstractHitHandler<event::SADCHit> {
protected:
    /// Freqs range to calculate the mean
    const int _range[2];
    /// Number of sigmas above the mean to set the threshold (can be zero or
    /// negative for loose cuts)
    const float _nSigma;
public:
    /// Parameterised with frequency ragne and number of sigma
    MSADCFFTSetThrMeanAmp( calib::Dispatcher & cdsp
                , const std::string & selection
                , int freqMin, int freqMax
                , float nSigma
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _range{freqMin, freqMax}
        , _nSigma(nSigma)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class MSADCFFTSetThrMeanAmp


bool
MSADCFFTSetThrMeanAmp::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if((!hit.rawData) || (!hit.rawData->fft)) return true;
    const int nFreqs = _range[1] - _range[0];
    assert(nFreqs > 0);
    double //mean = 0.
         //, stdDev = 0.
           meanAmpNoise = 0.
         , stdDevNoise  = 0.;

    #if 0
    std::cout << "xxx, waveform: ";
    for(int i = 0; i < 32; ++i) {
        std::cout << hit.rawData->wave[i] << ", ";
    }
    std::cout << "FFT: ";
    for(int i = 0; i < 16; ++i) {
        std::cout << hit.fft->real[i] << " + "
            << hit.fft->imag[i] << "j, "
            ;
    }
    std::cout << std::endl;
    #endif

    const int freqArrLength = (gWaveArrLength >> 1) + 1;
    for(int i = 0; i < freqArrLength; ++i) {
        const double re = ( i != freqArrLength-1 ? hit.rawData->fft->real[i] : hit.rawData->fft->imag[0] )
                   , im = ( (0 != i && i != freqArrLength-1) ? hit.rawData->fft->imag[i] : 0. )
                   , abs2 = re*re + im*im
                   ;
        assert(abs2 >= 0);
        //mean   += sqrt(abs2);
        //stdDev += abs2;
        if(i >= _range[0] && i < _range[1]) {
            meanAmpNoise += sqrt(abs2);
            stdDevNoise  += abs2;
        }
        //std::cout << "xxx sqrt(" << re << "^2 + j*" << im << "^2) = " << sqrt(abs2) << std::endl;
    }
    //mean   /= gWaveArrLength;
    //stdDev /= gWaveArrLength;
    if(nFreqs > 1) {
        meanAmpNoise /= 16;  //nFreqs;
        stdDevNoise  /= 16;  //nFreqs;
    }

    stdDevNoise -= meanAmpNoise*meanAmpNoise;
    //stdDev      -= mean*mean;
    assert(stdDevNoise >= 0);  // E(x^2) - E(x)^2
    //assert(stdDev >= 0);
    stdDevNoise  = sqrt(stdDevNoise);
    //stdDev       = sqrt(stdDev);
    //std::cout << "xxx, mean=" << meanAmpNoise << ", sigma=" << stdDevNoise << std::endl;  // XXX

    hit.rawData->ampThreshold = meanAmpNoise + _nSigma * stdDevNoise;

    #if 0
    {
        double radixCs[32];
        bzero(radixCs, sizeof(radixCs));
        //for(int i = _range[0]; i < _range[1]; ++i) {
        std::cout << "... coeffs: ";  // XXX
        for(int i = 0; i < 16; ++i) {
            radixCs[i   ] = hit.fft->real[i];
            std::cout << hit.fft->real[i] << " + j" << hit.fft->imag[i] << ", ";  // XXX
            if(i > 0) {
                radixCs[32-i] = hit.fft->imag[i];
            }
        }
        std::cout << std::endl;  // XXX
        gsl_fft_halfcomplex_radix2_inverse(radixCs, 1, 32);
        double mean2 = 0;
        std::cout << "... wave:";
        for(int i = 0; i < 32; ++i) {
            mean2 += radixCs[i];
            std::cout << hit.rawData->wave[i] << ", ";  // XXX
            //std::cout << hit.rawData->wave[i] << " - " << radixCs[i] << " = "
            //    << hit.rawData->wave[i] - radixCs[i] << std::endl;
        }
        std::cout << std::endl;  // XXX
        mean2 /= 32;
        //std::cout << "xXx " << mean2 << " ~~ " << meanAmpNoise << std::endl;
    }
    #endif

    if(log().getPriority() >= log4cpp::Priority::INFO) {  // TODO: debug
        log().info("%s mean amp threshold (%e, %e) results in %e threshold"
                , naming()[did].c_str()
                , meanAmpNoise, stdDevNoise, hit.rawData->ampThreshold );
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCFFTSetThrMeanAmp, cdsp, cfg
                , "Sets (M)SADC peak-finding threshold to the mean amp. of N freqs"
                ) {
    using namespace na64dp;
    auto & L = aux::get_logging_cat(cfg);
    int range[2] = {
            cfg["range"][0].as<int>(), cfg["range"][1].as<int>()
        };
    for(int i = 0; i < 2; ++i) {
        if(range[i] < 0) range[i] += na64dp::handlers::gWaveArrLength/2 + 1;
    }
    if(range[0] > range[1]) {
        L.warn("Freq. range specification [%d:%d) has been inverted"
                , range[0], range[1]);
        std::swap(range[0], range[1]);
    }
    if(range[0] == range[1]) {
        NA64DP_RUNTIME_ERROR("Bad frequency range: [%d:%d)"
                , range[0], range[1]);
    }

    return new handlers::MSADCFFTSetThrMeanAmp( cdsp
            , aux::retrieve_det_selection(cfg)
            , range[0], range[1]
            , cfg["nSigma"] ? cfg["nSigma"].as<float>() : 3
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

