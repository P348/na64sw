#include "na64detID/TBName.hh"
#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"

#include "msadc-fft.h"
#include "na64util/str-fmt.hh"

#include <TMath.h>
#include <cmath>
#include <fstream>
#include <sdc-base.hh>
#include <unordered_map>

namespace na64dp {
namespace calib {
/// (M)SADC Mean frequencies and std deviations per channels
struct SADCFreqDistParameters {
    int nFreqsLow, nFreqsHigh;
    float mean[17], stddev[17];
    char method;
};
}  // namespace ::na64dp::calib
namespace handlers {

/**\brief Calculates chi2 p-value of deviations for MSADC DFT
 *
 * Examines (M)SADC waveform with information about mean value on DFT
 * amplitudes with chi-squared p-value distribution.
 *
 * By default all the frequencies are examined. If `freqs` or `freqsRange` is
 * specified, only selected frequencies (or frequencies within given range)
 * are examined. Parameters `freqs` and `freqsRange` can not be specified
 * together. `invertFreqsSelection` boolean parameter inverts meaning
 * of `freqs` or `freqsRange` parameter.
 *
 * `debugCopyDeviations` parameter writes deviation values to real parts of
 * `fft` attribute of SADC hits, for further examination (thus, invalidating
 * FFT results) -- should be used only for debug.
 *
 * \code{.yaml}
 *     - _type: SADCFFTCanBeZero
 *       # Detector selection parameter
 *       applyTo: ...
 *       # Override calibrations with file; str, opt
 *       fileOverride: ...
 *       # List of selected frequencies to compute deviations; list of
 *       # ints (0-16), opt
 *       freqs: []
 *       # Range of selected frequencies to compute deviations; two ints, opt
 *       freqsRange: [0, 17]
 *       # Inverts meaning of "freqs" or "freqsRange" if one of them was
 *       # specified; bool, opt
 *       invertFreqsSelection: false
 *       # (dev) copies deviation values to sadcHits.rawData.fft.real; bool, opt
 *       debugCopyDeviations: false
 * \endcode
 *
 * \todo If this method of zero suppression will suceed, this handler must be
 *       modified to consider mean/stddev values from some kind of calib data
 *       (now the only way to make it work is to provide `fileOverride`
 *       parameter).
 * */
class SADCFFTCanBeZero : public AbstractHitHandler<event::SADCHit> {
protected:
    /// If set, a file will be used instead of calibration data
    const std::string _fileSourceOverride;
    /// If set, deviations will be copied to real part of (D)FT part of hit's
    /// raw data
    bool _dbgCopyDeviations;
    /// If set, only the selected frequencies will be compared with respect to
    /// mean values
    std::set<int> _choosenFreqs;
    /// If true, inverts meaning of `_choosenFreqs`
    bool _invertFreqsSelection;

    /// Mean freqs in use
    std::unordered_map<DetID, calib::SADCFreqDistParameters> _meanFreqs;
    // ... put here properties of your handler

    /// Returns p-value assuming mean and stddevs are for dBFS or dBFS scaling
    double _dBFS_deviations_chi2( event::MSADCDFFTCoefficients &
                                , const calib::SADCFreqDistParameters & );
    /// Returns p-value assuming mean and stddevs are without scaling
    double _abs_amp_deviations_chi2( event::MSADCDFFTCoefficients &
                                   , const calib::SADCFreqDistParameters & );
public:
    void handle_update(const nameutils::DetectorNaming & nm) override {
        if(!_fileSourceOverride.empty()) {
            // open file and read freqs mean and stddev, line by line
            std::ifstream ifs(_fileSourceOverride);
            std::string line;
            DetID did;
            while(std::getline(ifs, line)) {
                if(line.empty()) continue;
                calib::SADCFreqDistParameters ps;
                auto toks = sdc::aux::tokenize(line, ',');
                size_t nTok = 0;
                for(const auto & tok : toks) {
                    if(nTok < 4) {
                        if(0 == nTok) {
                            did = nm[tok];
                            ++nTok;
                            continue;
                        } else if(1 == nTok) {
                            ps.nFreqsLow = sdc::aux::lexical_cast<int>(tok);
                            ++nTok;
                            continue;
                        } else if(2 == nTok) {
                            ps.nFreqsHigh = sdc::aux::lexical_cast<int>(tok);
                            ++nTok;
                            continue;
                        } else if(3 == nTok) {
                            assert(tok.size() == 1);
                            ps.method = tok[0];
                            ++nTok;
                            continue;
                        }
                    }
                    int nComponent = (nTok - 4)/2;
                    bool isMean = !((nTok - 4)%2);
                    assert(nComponent >= 0 && nComponent < 17);
                    if(isMean) {
                        ps.mean[nComponent] = sdc::aux::lexical_cast<float>(tok);
                    } else {
                        ps.stddev[nComponent] = sdc::aux::lexical_cast<float>(tok);  // TODO: remove factor 3!
                    }
                    ++nTok;
                }
                assert(nTok == (4 + 17*2));
                _meanFreqs.emplace(did, ps);
            }
        }
        AbstractHitHandler<event::SADCHit>::handle_update(nm);
    }

    /// ... features of the ctr, if any
    SADCFFTCanBeZero( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::string & filePathOverride
                , const std::vector<int> selectedFreqs
                , bool invertFreqsSelection
                // ...
                , log4cpp::Category & logCat
                , bool dbgCopy=false
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _fileSourceOverride(filePathOverride)
        , _dbgCopyDeviations(dbgCopy)
        , _choosenFreqs(selectedFreqs.begin(), selectedFreqs.end())
        , _invertFreqsSelection(invertFreqsSelection)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class SADCFFTCanBeZero


double
SADCFFTCanBeZero::_dBFS_deviations_chi2(
          event::MSADCDFFTCoefficients & dft
        , const calib::SADCFreqDistParameters & cf ) {
    double stdRe[17], stdIm[17], scaledAmps[17];
    // unpack
    na64sw_msadc_fft_compact2std(16, dft.real, dft.imag
            , stdRe, stdIm);
    // scale transform
    na64sw_msadc_fft_dBFS(16, stdRe, stdIm, 1., 32, scaledAmps);
    // get reference value
    double nrm = 0.;  // note: if low==high, the result is dBFS, otherwise its dB
    for(int i = cf.nFreqsLow; i < cf.nFreqsHigh; ++i) {
        nrm += scaledAmps[i];
    }
    nrm /= cf.nFreqsHigh - cf.nFreqsLow;
    // calc dev and chi2
    double chi2 = 0., deviation;
    int ndf = 0;
    if(!_choosenFreqs.empty()) {
        for(const int i : _choosenFreqs) {
            assert(i >= 0 && i < 17);
            scaledAmps[i] -= nrm;
            deviation = (scaledAmps[i] - cf.mean[i])/cf.stddev[i];
            if(std::isnan(deviation) || !std::isfinite(deviation)) continue;
            if(_dbgCopyDeviations) {   // XXX, this is just to check
                if(i == 16) {
                    dft.imag[0] = deviation;
                } else {
                    dft.real[i] = deviation;
                    if(0 != i) {
                        dft.imag[i] = 0.;
                    }
                }
            }
            chi2 += deviation*deviation;
            ++ndf;
        }
    } else {
        for(int i = 0; i < 17; ++i) {
            scaledAmps[i] -= nrm;
            deviation = (scaledAmps[i] - cf.mean[i])/cf.stddev[i];
            if(std::isnan(deviation) || !std::isfinite(deviation)) continue;
            if(_dbgCopyDeviations) {   // XXX, this is just to check
                if(i == 16) {
                    dft.imag[0] = deviation;
                } else {
                    dft.real[i] = deviation;
                    if(0 != i) {
                        dft.imag[i] = 0.;
                    }
                }
            }
            chi2 += deviation*deviation;
            ++ndf;
        }
    }
    // return likelihood measure
    if(0 != ndf) return TMath::Prob(chi2, ndf);
    return std::nan("0");
}

double
SADCFFTCanBeZero::_abs_amp_deviations_chi2(
            event::MSADCDFFTCoefficients & dft
          , const calib::SADCFreqDistParameters & cf ) {
    double stdRe[17], stdIm[17], scaledAmps[17];
    // unpack
    na64sw_msadc_fft_compact2std(16, dft.real, dft.imag
            , stdRe, stdIm);
    // scale transform
    na64sw_msadc_fft_abs_amps(16, stdRe, stdIm, scaledAmps, 1.);
    // calc dev and chi2
    double chi2 = 0., deviation;
    int ndf = 0;
    if(!_choosenFreqs.empty()) {
        for(const int i : _choosenFreqs) {
            assert(i >= 0 && i < 17);
            deviation = (scaledAmps[i] - cf.mean[i])/cf.stddev[i];
            if(std::isnan(deviation) || !std::isfinite(deviation)) continue;
            if(_dbgCopyDeviations) {   // XXX, this is just to check
                if(i == 16) {
                    dft.imag[0] = deviation;
                } else {
                    dft.real[i] = deviation;
                    if(0 != i) {
                        dft.imag[i] = 0.;
                    }
                }
            }
            chi2 += deviation*deviation;
            ++ndf;
        }
    } else {
        for(int i = 0; i < 17; ++i) {
            deviation = (scaledAmps[i] - cf.mean[i])/cf.stddev[i];
            if(std::isnan(deviation) || !std::isfinite(deviation)) continue;
            if(_dbgCopyDeviations) {   // XXX, this is just to check
                if(i == 16) {
                    dft.imag[0] = deviation;
                } else {
                    dft.real[i] = deviation;
                    if(0 != i) {
                        dft.imag[i] = 0.;
                    }
                }
            }
            chi2 += deviation*deviation;
            ++ndf;
        }
    }
    // return likelihood measure
    if(0 != ndf) return TMath::Prob(chi2, ndf);
    return std::nan("0");
}

bool
SADCFFTCanBeZero::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if((!hit.rawData) || (!hit.rawData->fft)) return true;
    auto it = _meanFreqs.find(did);
    if(_meanFreqs.end() == it) {
        // TODO: VHCAL is not converted properly to/from string!
        //log().warn("No item \"%s\", %x"
        //        , naming()[did].c_str()
        //        , did.id
        //        );  // TODO: collect warnings
        return true;
    }
    if(it->second.method == 'a') {
        hit.rawData->canBeZero = _abs_amp_deviations_chi2(*hit.rawData->fft, it->second);
    } else if(it->second.method == 'r') {
        hit.rawData->canBeZero = _dBFS_deviations_chi2(*hit.rawData->fft, it->second);
    } else {
        NA64DP_RUNTIME_ERROR("Unknown amp. normalization method for %s: `%c'"
                , naming()[did].c_str(), it->second.method );
    }

    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCFFTCanBeZero, cdsp, cfg
                , "Sets can-be-zero probability of SADC hit to chi2 p-value of"
                  " deviations wrt mean values"
                ) {
    std::vector<int> selectedFreqs;
    if(cfg["freqs"]) {
        selectedFreqs = cfg["freqs"].as<std::vector<int>>();
    }
    if(cfg["freqsRange"]) {
        if(!selectedFreqs.empty()) {
            NA64DP_RUNTIME_ERROR("Both \"freqs\" and \"freqsRange\" specified.");
        }
        int freqFrom = cfg["freqsRange"][0].as<int>()
          , freqTo   = cfg["freqsRange"][1].as<int>();
        if(freqFrom >= freqTo) {
            NA64DP_RUNTIME_ERROR("\"freqsRange\" defines empty or negative range.");
        }
        for(int i = freqFrom; i < freqTo; ++i) {
            selectedFreqs.push_back(i);
        }
    }

    using namespace na64dp;
    return new handlers::SADCFFTCanBeZero( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["fileOverride"].as<std::string>()
            , selectedFreqs, cfg["invertFreqsSelection"] ? cfg["invertFreqsSelection"].as<bool>() : false
            // ... your additional arguments retrieved from `cfg'
            , aux::get_logging_cat(cfg)
            , cfg["debugCopyDeviations"] ? cfg["debugCopyDeviations"].as<bool>() : false
            , aux::get_naming_class(cfg)
            );
}

