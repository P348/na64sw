#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64/user-api/adjust-pedestals.h"

#include <cmath>
#include <fstream>
#include <ios>
#include <stdexcept>

namespace na64dp {
namespace handlers {

/**\brief Advanced baseline-finding algorithm based on Lagrange interp of triplets
 *
 * Can be used in addition to some pedestal-finding algorithm. Depending on
 * whether the current pedestals values are finite works in two ways:
 * - if pedestals are set, they are assumed to be already subtracted from
 *   waveform; in that case, new values are summed up with existing ones
 *   and waveform is corrected taking into account new ones
 * - otherwise, just sets pedestal values.
 * This way one can rely on some fallback values provided previously.
 *
 * \code{.yaml}
 *     - _type: MSADCFindPedestalsLagrangeTriplets
 *       cutOff: 10  # float, cut-off value to consider minima
 * \endcode
 * */
class MSADCFindPedestalsLagrangeTriplets : public AbstractHitHandler<event::SADCHit> {
protected:
    float _cutOff;
    std::ofstream * _badWfDump;
    bool _inlineSubtract;
public:
    MSADCFindPedestalsLagrangeTriplets( calib::Dispatcher & cdsp
                , const std::string & selection
                , float cutOff
                , log4cpp::Category & logCat
                , bool inlineSubtract=false
                , const std::string & badWfDump=""
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _cutOff(cutOff)
        , _badWfDump(nullptr)
        , _inlineSubtract(inlineSubtract)
        {
        if(!badWfDump.empty()) {
            _badWfDump = new std::ofstream(badWfDump);
        }
    }
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
    void finalize() override {
        if(_badWfDump) delete _badWfDump;
        _badWfDump = nullptr;
    }
};  // class MyHitHandler


bool
MSADCFindPedestalsLagrangeTriplets::process_hit(EventID eid, DetID did, event::SADCHit & hit) {
    NA64DP_ASSERT_EVENT_DATA( hit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    const size_t nSamples = 32;  // TODO
    assert(sizeof(event::RawDataSADC::wave)/sizeof(event::RawDataSADC::wave[0]) == nSamples);  // TODO
    double wfCopy[nSamples];
    for(unsigned int i = 0; i < nSamples; ++i) {
        wfCopy[i] = hit.rawData->wave[i];
        if(_inlineSubtract) {
            if(std::isnan(hit.rawData->pedestals[i%2])) {
                throw std::runtime_error("Can't subtract"
                        " pedestal value (not set or NaN).");
            }
            wfCopy[i] -= hit.rawData->pedestals[i%2];
        }
    }
    double p[2] = {hit.rawData->pedestals[0], hit.rawData->pedestals[1]};
    int rc = na64util_adjust_pedestals(wfCopy, p, nSamples, _cutOff);
    if(rc) {
        if(_badWfDump) {
            *_badWfDump << eid << " " << naming()[did]
                << " " << hit.rawData->pedestals[0]
                << " " << hit.rawData->pedestals[1]
                ;
            for(unsigned int i = 0; i < nSamples; ++i) {
                *_badWfDump << " " << std::scientific << wfCopy[i];
            }
            *_badWfDump << std::endl;
        }
        log().warn("na64util_adjust_pedestals() returned %d on %s"
                " detector at event %s", rc
                , naming()[did].c_str()
                , eid.to_str().c_str()
                );
        return true;
    }
    
    if(!std::isfinite(hit.rawData->pedestals[0])) {
        hit.rawData->pedestals[0]  = p[0];
    } else {
        for(unsigned int i = 0; i < nSamples; i += 2) {
            hit.rawData->wave[i] -= p[0];
        }
        hit.rawData->pedestals[0] += p[0];
    }

    if(!std::isfinite(hit.rawData->pedestals[1])) {
        hit.rawData->pedestals[1]  = p[1];
    } else {
        for(unsigned int i = 1; i < nSamples; i += 2) {
            hit.rawData->wave[i] -= p[1];
        }
        hit.rawData->pedestals[1] += p[1];
    }

    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCFindPedestalsLagrangeTriplets, cdsp, cfg
                , "Pedestals finding algorithm based on Lagrange interpolation of triplets"
                ) {
    using namespace na64dp;
    return new handlers::MSADCFindPedestalsLagrangeTriplets( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["cutOff"].as<float>(10)
            , aux::get_logging_cat(cfg)
            , cfg["inlineSubtract"].as<bool>(false)
            , cfg["dumpBadWaveformsTo"].as<std::string>("")
            , aux::get_naming_class(cfg)
            );
}

