
#include "msadc-remove-peaks.hh"

#include "na64util/pair-hash.hh"
#include "na64util/str-fmt.hh"
#include <algorithm>
#include <cmath>
#include <limits>
#include <log4cpp/Priority.hh>
#include <string>

namespace na64dp {
namespace handlers {

/**\brief Performs rough pile-up suppression based on relative amplitudes,
 *        favouring leading cluster to become a hit's principal parameters
 *
 * Discriminates events by condition on ratio b/w maximum amplitude found
 * within a cluster.
 *
 * - within time clusters \f$i\f$ present in an event, look for item (typically
 *   cell) highest amp. value \f$A_{i, max}\f$
 * - require that two clusters with highest amps \f$i, j\f$ for which
 *   \f$A_{i,max} > A_{j, max}\f$ the ratio computed as
 *   \f$r_{max} = A_{j, max}/A_{i, max} < r_{th}\f$ where threshold
 *   \f$r_{th}\f$ is an external tunable parameter. If event passes the
 *   criteria, all the clusters except for one with highest amplitude are
 *   removed. While leading cluster's peaks become hit's main data: amp,
 *   timing and error.
 * - Otherwise (if ratio of max amplitudes in competitive clusters), the whole
 *   event gets discriminated (as being probably piled up).
 *
 * Albeit, not being good in general (might be too
 * conservative and drop events that are not, in fact, piled up -- e.g. for
 * cases when primary particle hits at the edge of the cells), does not
 * rely on calibration information and thus can be used to clean the input
 * stats for calibration.
 *
 * \code{.yaml}
 *     - _type: BaseMSADCTimeClusterDiscriminate
 *       # optional, det item (hit) selection expression
 *       applyTo: ...
 *       # Sets how the "stray peaks" (ones not related to any particular
 *       # cluster) have to be handled; str. option, opt
 *       strayPeaksErrorEstimate: noError  # disable, noError, byAmplitude, byUncertainty, byAmpAndUncertainty
 *       # Sets how others than winning cluster's peaks have to be treated;
 *       # str. option, opt
 *       sidePeaksErrorEstimate: noError
 *       # Sets how peaks "in general case" have to be treated; str. option, opt
 *       commonPeaksErrorEstimate: byAmpAndUncertainty
 *       # Permitted ratio threshold for amplitudes of maximal peaks b/w
 *       # competing clusters; float, required
 *       ampRatioThreshold: ...
 *       # Whether to invert meaning (used to study pile-up events); bool, opt
 *       invert: false
 *       # Multiplier for amplitude quantity to be accounted as hit's abs.
 *       # error. Float, opt.
 *       ampErrorMultiplier: 1.
 *       # Multiplier for amplitude abs. error quantity to be accounted as
 *       # hit's abs.error. Float, opt.
 *       uncErrorMultiplier: 1.
 *       # Whether to set winning peak's values (time, amp, square) as hit's
 *       # estimates: raw (uncalibrated) energy deposition estimate and time.
 *       # bool, opt
 *       setHitValues: true
 * \endcode
 *
 * \note Handler will remove stray peaks (not marked as ones included into any
 *       time cluster) optionally increasing item's absolute error or 
 * */
class MSADCTimeClusterConservativeBestAmp : public BaseMSADCTimeClusterDiscriminate {
public:
    const double ampRatioThreshold;
    const bool invert;
    const ErrorEstimationMethod sidePeaksRemoveMode;
    /// If set, single winning cluster's peaks will be used to set `maxAmp`,
    /// `maxSample` and `
    const bool setHitValues;
protected:
    bool _analyze_clusters() override;
public:
    MSADCTimeClusterConservativeBestAmp(calib::Dispatcher & cdsp
                , const std::string & selection
                , ErrorEstimationMethod removeStrayPeaks
                , ErrorEstimationMethod sidePeaksRemoveMode_
                , ErrorEstimationMethod commonPeakRemovalMethod
                , double ampRatioThreshold_
                , bool invert_
                , log4cpp::Category & logCat
                , float multErrAmp=1., float multErrErr=1.
                , bool setHitValues_=true
                , const std::string & namingSubclass="default")
        : BaseMSADCTimeClusterDiscriminate(cdsp, selection, removeStrayPeaks
                , commonPeakRemovalMethod, logCat, multErrAmp, multErrErr, namingSubclass)
        , ampRatioThreshold(ampRatioThreshold_)
        , invert(invert_)
        , sidePeaksRemoveMode(sidePeaksRemoveMode_)
        , setHitValues(setHitValues_)
        {
        }
};  // class MSADCTimeClusterConservativeBestAmp


bool
MSADCTimeClusterConservativeBestAmp::_analyze_clusters() {
    if(_clusters.size() < 2)
        return true;  // ignore events without clusters of with just a single one
    if(log().getPriority() >= log4cpp::Priority::DEBUG) {
        for(auto & clusterEntry: _clusters) {
            log() << log4cpp::Priority::DEBUG
                << "Cluster #" << clusterEntry.first << " has "
                << clusterEntry.second.size() << " items with max amp. "
                << clusterEntry.second.maxAmp << ".";
        }
    }
    // build map of clusters sorted by max amp. This is a multimap, albeit
    // collision b/w float values is not very probable we still should
    // correctly handle these rare cases
    std::multimap<double, TimeCluster *> byAmpAscending;
    std::transform( _clusters.begin(), _clusters.end()
            , std::inserter(byAmpAscending, byAmpAscending.end())
            , [](auto & clusRef) {
                    return std::pair<double, TimeCluster *>(clusRef.second.maxAmp, &clusRef.second);
                }
            );
    assert(_clusters.size() == byAmpAscending.size());
    // look at the ratio b/w two last clusters (ones with the highest amp)
    // and set the parent handler class' flag for current event to be
    // discriminated if max amp ratio exceeds the threshold
    assert(byAmpAscending.size() > 1);
    auto it1 = byAmpAscending.end();
    --it1;
    assert(it1 != byAmpAscending.end());
    assert(it1 != byAmpAscending.begin());
    auto it2 = it1;
    // floating point exact comparison below is delibirate. In case of
    // matching amplitude we should find the one next in descending order
    while( it2->first == it1->first
        && it2 != byAmpAscending.begin()
        ) --it2;
    // check the thresholds. Note, that in case of NaN values the check
    // will return false
    bool discriminateEvent = false;
    if(it2->first / it1->first > ampRatioThreshold) {
        discriminateEvent = true;
    }
    if(invert && !discriminateEvent) discriminateEvent = true;
    if(discriminateEvent) {
        _set_event_processing_result(kDiscriminateEvent);
        return false;  // do not consider other hits from the event
    }

    // Otherwise, simply drop (mark for removal) all the peaks
    // not belonging to the primary (winning) cluster.
    for(;;) {
        assert(it2 != byAmpAscending.end());
        for( auto peakIt = it2->second->begin()
           ; it2->second->end() != peakIt
           ; ++peakIt ) {
            //assert(peakIt->second->second->timeClusterLabel /*== it2->second);
            mark_peak_for_removal(peakIt->first, peakIt->second);
        }
        // This is rather redundant if clusters are not used after this method.
        // Removes all except one leading records in `_clusters` map
        //if(set_hit_values()) {
        //    for(auto clusterIt = _clusters.begin(); clusterIt != _clusters.end();) {
        //        if(it2->second == &clusterIt->second) {
        //            clusterIt = _clusters.erase(clusterIt);
        //        } else {
        //            ++clusterIt;
        //        }
        //    }
        //}
        if(it2 == byAmpAscending.begin()) break;
        --it2;
    }
    // Peaks from the winning time cluster are (optionally) used to set
    // max.amp. and timing quantities of corresponding hits
    if(setHitValues) {
        for( auto peakIt = it1->second->begin()
           ; peakIt != it1->second->end()
           ; ++peakIt ) {
            event::RawDataSADC & hitRawData = *peakIt->first->rawData;
            const event::MSADCPeak & peakRef = *peakIt->second->second;

            hitRawData.maxAmp = peakRef.amp;  // TODO: peakRef.sumFine ?
            assert(peakIt->second->first < std::numeric_limits<decltype(event::RawDataSADC::maxSample)>::max());
            hitRawData.maxSample = static_cast<decltype(event::RawDataSADC::maxSample)>(peakIt->second->first);
            peakIt->first->time = peakRef.time;
            peakIt->first->timeError = peakRef.timeErr;
            //peakIt->first->rawData->maxAmp 
        }
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCTimeClusterConservativeBestAmp, cdsp, cfg
                , "Discriminates events with similar max amps within MSADC"
                  " cluster or purges the side clusters"
                ) {
    using namespace na64dp;
    handlers::RemoveMSADCMax::ErrorEstimationMethod strayPeaksRemoval
            = handlers::RemoveMSADCMax::peak_rm_mode_from_str(
                    cfg["strayPeaksErrorEstimate"] ? cfg["strayPeaksErrorEstimate"].as<std::string>() : "noError"
                    )
        , sidePeaksRemoval
            = handlers::RemoveMSADCMax::peak_rm_mode_from_str(
                    cfg["sidePeaksErrorEstimate"] ? cfg["sidePeaksErrorEstimate"].as<std::string>() : "noError"
                    )
        , commonPeakRemoval
            = handlers::RemoveMSADCMax::peak_rm_mode_from_str(
                    cfg["commonPeaksErrorEstimate"] ? cfg["commonPeaksErrorEstimate"].as<std::string>() : "byAmpAndUncertainty"
                    )
        ;
    return new handlers::MSADCTimeClusterConservativeBestAmp( cdsp
            , aux::retrieve_det_selection(cfg)
            , strayPeaksRemoval
            , sidePeaksRemoval
            , commonPeakRemoval
            , cfg["ampRatioThreshold"].as<float>()
            , cfg["invert"] ? cfg["invert"].as<bool>() : false
            , aux::get_logging_cat(cfg)
            , cfg["ampErrorMultiplier"] ? cfg["ampErrorMultiplier"].as<float>() : 1.
            , cfg["uncErrorMultiplier"] ? cfg["uncErrorMultiplier"].as<float>() : 1.
            , cfg["setHitValues"] ? cfg["setHitValues"].as<bool>() : true
            , aux::get_naming_class(cfg)
            );
}

#if 0  // (draft)
namespace na64dp {
namespace handlers {

//
// Modular cluster analyizng handler

class MSADCTimeClusterDiscriminate : public BaseMSADCTimeClusterDiscriminate {
public:
    struct CheckMSADCTimeCluster {
        // ...
    };
protected:
    bool _analyze_clusters() override;
public:
    MSADCTimeClusterDiscriminate(calib::Dispatcher & cdsp
                , const std::string & selection
                , PeakRemoveMode removeStrayPeaks
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default")
        : BaseMSADCTimeClusterDiscriminate(cdsp, selection, removeStrayPeaks, logCat, namingSubclass)
        {
        }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCTimeClusterDiscriminate, cdsp, cfg
                , "..."
                ) {
    using namespace na64dp;
    handlers::MSADCTimeClusterDiscriminate::PeakRemoveMode strayPeaksRemoval
        = handlers::MSADCTimeClusterDiscriminate::peak_rm_mode_from_str(
                cfg["strayPeaksRemoval"] ? cfg["strayPeaksRemoval"].as<std::string>() : "common"
                );  // TODO
    return new handlers::MSADCTimeClusterDiscriminate( cdsp
            , aux::retrieve_det_selection(cfg)
            , strayPeaksRemoval
            // ... your additional arguments retrieved from `cfg'
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}
#endif

