#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"
#include "na64util/str-fmt.hh"
#include "msadc-fft.h"

#include <TProfile.h>
#include <TH2F.h>
#include <TH2.h>
#include <TColor.h>
#include <TStyle.h>

#include <cmath>
#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Combined plot of the (D)FFT amplitudes from SADC detectors
 *
 * This handler depicts amplitude spectrum obtained for different frequencies
 * of (M)SADC signal, relative to averaged part of the signal.
 *
 * \code{.yaml}
 *     - _type: SADCPlotDFTSpectra
 *       # ...
 *       baseName: "..."
 *       # ...
 *       histDescr: "..."
 *       # A subject value to plot. Choices are: amp, re, im; str, opt
 *       subject: "amp"
 *       # Sets the frequencies for relative plots. When set to non-zero
 *       # spanning window, a mean value of n-th freq will be used to
 *       # renormalize plotted quantity
 *       baseFreqs: [0, 0]
 *       # Makes sense only when amplitude is plotted. Enables plotting in dB
 *       # units (or dBFS if `baseFreqs` set to zero window)
 *       dBUnits: true
 * \endcode
 *
 * \note This handler may provide additional substitution variables to
 *      formatting string: `baseFreqLow` and `baseFreqUp` when ...
 *
 * \note Handler can be used to extract zero-like amplitudes spectra for
 *      zero-suppression. See docs on `fft-zeroes` routine in `na64sw-get-fts`
 *      util (in this case output name and description must match certain
 *      format).
 *
 * \see TDirAdapter
 * \ingroup handlers sadc-handlers
 * */
class SADCPlotDFTSpectra : public util::TDirAdapter
                       , public AbstractHitHandler<event::SADCHit> {
public:
    enum FTSubject {
        kAmplitude,
        kRealPart,
        kImagPart,
    };
private:
    /// Number of bins by vertical axis (not used if profile)
    size_t _ampNBins;
    /// Amplitude range bounds (not used if profile)
    double _ampRange[2];
    /// Histogram base name suffix
    const std::string _hstBaseName
                    , _hstDescr
                    , _overridenPath
                    ;
    /// Histogram index by detector ID
    std::map<DetID_t, TNamed *> _hists;
    /// When set, a TProfile is used instead of TH2F
    const bool _profile;
    /// baselevel averaging range for plotting in dB or as fractions
    int _baseWindow[2];
    /// When set the plotted amplitudes are either dB (when `_baseWindow` is
    /// set) or dBFS. When this flag is not set eith absolute (D)FFT freq's
    /// value is plotted (when `_baseWindow` is not set or of 0 length), or
    /// fraction wrt mean value from `_baseWindow` is plotted.
    bool _dBUnits;
    /// Plotting subject: amp, re%, im, etc
    FTSubject _subj;
public:
    /// Main ctr of the handler
    SADCPlotDFTSpectra( calib::Dispatcher & ch
                    , const std::string & selection
                    , FTSubject subj
                    , int baseFreqLow, int baseFreqUp
                    , size_t nAmpBins
                    , double ampMin, double ampMax
                    , bool profile=false
                    , bool dBUnits=true
                    , const std::string & hstBaseName="fftAmp"
                    , const std::string & descr="SADC Wf DFFT Spectra for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp}; Freq.no; Amplitude, dB"
                    , const std::string & path=""
                    );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADCPlotDFTSpectra();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & currentEcalHit) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

/** \param ch Calibration dispatcher instance
 *  \param selection Detector selection expression
 *  \param nAmpBins Number of bins by vertical axis
 *  \param ampMin Lower bound of amplitude histogram range (vertical axis)
 *  \param ampMax Upper bound of amplitude histogram range (vertical axis)
 *  \param baseName Histogram name suffix to name the corresponding TH2F
 *  */
SADCPlotDFTSpectra::SADCPlotDFTSpectra( calib::Dispatcher & ch
                                  , const std::string & selection
                                  , FTSubject subj
                                  , int baseFreqLow, int baseFreqUp
                                  , size_t nAmpBins
                                  , double ampMin, double ampMax
                                  , bool profile
                                  , bool dBUnits
                                  , const std::string & baseName
                                  , const std::string & descr
                                  , const std::string & path
                                  ) : util::TDirAdapter(ch)
                                    , AbstractHitHandler(ch, selection)
                                    , _ampNBins(nAmpBins)
                                    , _ampRange{ampMin, ampMax}
                                    , _hstBaseName(baseName)
                                    , _hstDescr(descr)
                                    , _overridenPath(path)
                                    , _profile(profile)
                                    , _baseWindow{baseFreqLow, baseFreqUp}
                                    , _dBUnits(dBUnits)
                                    , _subj(subj)
                                    {
    if(dBUnits && subj != kAmplitude) {
        log().warn("\"dBUnits\" switch ignored as plotted quantity is not"
                " an amplitude.");
    }
}

bool
SADCPlotDFTSpectra::process_hit( EventID
                             , DetID did
                             , event::SADCHit & currentHit) {
    if( !currentHit.rawData ) return true;  // no raw data associated with hit
    if( !currentHit.rawData->fft ) return true;
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;

        std::string hstName = _hstBaseName;
        auto substCtx = util::TDirAdapter::subst_dict_for(did, _hstBaseName);
        // push additional variables in string formatting context and
        // substitute the description string
        substCtx["baseFreqLow"] = util::format("%d", _baseWindow[0]);
        substCtx["baseFreqUp"]  = util::format("%d", _baseWindow[1]);
        std::string description = util::str_subst( _hstDescr, substCtx );
        auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
        p.second->cd();

        TNamed * newHst;
        TAxis * xAx;
        if(!_profile) {
            TH2F * newHst_ = new TH2F( p.first.c_str()
                             , description.c_str()
                             , 17, 0, 17
                             , _ampNBins, _ampRange[0], _ampRange[1] );
            newHst = newHst_;
            xAx = newHst_->GetXaxis();
        } else {
            TProfile * newHst_ = new TProfile( p.first.c_str()
                                 , description.c_str()
                                 , 17, 0, 17 );
            newHst = newHst_;
            xAx = newHst_->GetXaxis();
        }
        auto ir = _hists.emplace( did, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
        // set freq. labels
        double freqs[17];
        na64sw_msadc_fft_freqs_MHz(16, freqs, 12.5);  // TODO: parameterize ns per sample
        char lblBf[32];
        for(UInt_t i = 0; i < 17; ++i) {
            snprintf(lblBf, sizeof(lblBf), "#%u, %.1f", i, freqs[i]);
            xAx->SetBinLabel(i+1, lblBf);
        }
    }

    #if 1
    // unpack (D)FFT coefficients
    double stdRe[17], stdIm[17], scaledAmps[17];
    na64sw_msadc_fft_compact2std( 16
                                , currentHit.rawData->fft->real, currentHit.rawData->fft->imag
                                , stdRe, stdIm );

    // if reference freq(s) are provided, we should plot relative amplitude
    const bool isRelative = (_baseWindow[1] - _baseWindow[0] > 0);
    if(_subj == kAmplitude) {
        if(!isRelative) {  // absolute values
            if(_dBUnits) {
                // dB units with absolute scale -- "dB Full Scale" units (dBFS)
                // NOTE on reference value here: (M)SADC max has been already used prior
                // to (D)FFT, so for floating point number [0..1] dBFS are calculated with
                // ref value =1.0 (full scale).
                na64sw_msadc_fft_dBFS(16, stdRe, stdIm, 1., 32, scaledAmps);
            } else {
                // absolute, not dB units -- just plot the abs values per freq
                na64sw_msadc_fft_abs_amps(16, stdRe, stdIm, scaledAmps, 1.);
            }
        } else {  // relative values
            if(_dBUnits) {
                // dB units relatively to some freq(s) range's mean
                // 1. calc the dBFS
                na64sw_msadc_fft_dBFS(16, stdRe, stdIm, 1., 32, scaledAmps);
                // 2. Get mean value in a subrange
                double nrm = 0.;
                for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                    nrm += scaledAmps[i];
                }
                nrm /= _baseWindow[1] - _baseWindow[0];
                // 3. Subtract
                for(int i = 0; i < 17; ++i) {
                    scaledAmps[i] -= nrm;
                }
            } else {
                // relative, abs. amp units -- plot abs amp ratio wrt certain freqs
                // 1. get abs. values
                na64sw_msadc_fft_abs_amps(16, stdRe, stdIm, scaledAmps, 1);
                // 2. Get mean value in a subrange
                double nrm = 0.;
                for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                    nrm += scaledAmps[i];
                }
                nrm /= _baseWindow[1] - _baseWindow[0];
                // 3. Divide
                for(int i = 0; i < 17; ++i) {
                    scaledAmps[i] /= nrm;
                }
            }
        }
    } else if(kRealPart == _subj) {
        double factor;
        if(isRelative) {
            factor = 0;
            for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                factor += stdRe[i];
            }
            factor /= (_baseWindow[1] - _baseWindow[0]);
            factor = 1./factor;
        } else {
            factor = 1.;
        }
        for(int i = 0; i < 17; ++i) {
            scaledAmps[i] = stdRe[i]*factor;
        }
    } else if(kImagPart == _subj) {
        double factor;
        if(isRelative) {
            factor = 0;
            for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                factor += stdIm[i];
            }
            factor /= (_baseWindow[1] - _baseWindow[0]);
            factor = 1./factor;
        } else {
            factor = 1.;
        }
        for(int i = 0; i < 17; ++i) {
            scaledAmps[i] = stdIm[i]*factor;
        }
    }
    #ifndef NDEBUG
    else {assert(0);}  // else?
    #endif

    if(_profile) {
        for(int i = 0; i < 17; ++i) {
            if(std::isnan(scaledAmps[i]) || !std::isfinite(scaledAmps[i])) continue;
            static_cast<TProfile*>(it->second)->Fill( i, scaledAmps[i] );
        }
    } else {
         for(int i = 0; i < 17; ++i) {
            if(std::isnan(scaledAmps[i]) || !std::isfinite(scaledAmps[i])) continue;
            static_cast<TH2F*>(it->second)->Fill( i, scaledAmps[i] );
        }
    }
    
    #else
    // --- CUT --- CUT --- CUT ---
    double re, im, s = 0., ref = 0;
    //currentHit.rawData->fft->real
    if(_dBUnits) {
        // calc sum of high frequencies to calculate normalized amps in dB
        for(int i = 0; i < 17; ++i){
            re = currentHit.rawData->fft->real[i];
            if(i != 0 && i != 16) {
                im = currentHit.rawData->fft->imag[i];
            } else {
                im = 0;
                if(re == 16) {
                    re = currentHit.rawData->fft->imag[0];
                }
            }
            re = sqrt(re*re + im*im);;
            s += re;
            if(i >= _baseWindow[0] && i < _baseWindow[1])
                ref += re;
        }
        s /= 17;
        ref /= (_baseWindow[1] - _baseWindow[0]);
    } else {
        ref = 1.;
    }
    // --- CUT --- CUT --- CUT ---

    // fill the histogram
    for(int i = 0; i < 17; ++i){
        re = currentHit.rawData->fft->real[i];
        if(i != 0 && i != 16) {
            im = currentHit.rawData->fft->imag[i];
        } else {
            im = 0;
            if(re == 16) {
                re = currentHit.rawData->fft->imag[0];
            }
        }
        if(_dBUnits)  //< XXX
            re = sqrt(re*re + im*im);
        if(_dBUnits) {
            // NOTE: a bit weird that here we have to divide by square of ref value...
            //re = ::log(re/ref);
            //re = ::log(re/(ref*ref))/::log(ref);
            //re = ::log(re)/::log(ref);  // - log(ref*ref)
            //re = ::log(re/(ref*ref));
            //re -= ref;
            //re = re - ref;
            //re = (::log(re)/::log(ref)) + (::log(s)/::log(ref));  // high zero-like prob for signal
            //re = (::log(re)/::log(s));
            //re = (::log(s/ref) + ::log(re/ref))*::log(ref); //?
            re = ::log(re/ref);
        }
        // At least TProfile seems to not be resistant vs NaN/infinite values...
        if(std::isnan(re) || !std::isfinite(re))
            continue;
        // ---
        if(_profile) {
            static_cast<TProfile*>(it->second)->Fill( i, re );
        } else {
            static_cast<TH2*>(it->second)->Fill( i, re );
        }
        //static_cast<TProfile*>(it->second)->Fill( i, currentHit.rawData->wave[i] );
        // ---
    }
    #endif
    return true;
}

void
SADCPlotDFTSpectra::finalize() {
    for( auto idHstPair : _hists ) {
        if(!_profile) {
            static_cast<TH2*>(idHstPair.second)->SetOption("colz");
        }
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

SADCPlotDFTSpectra::~SADCPlotDFTSpectra() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( SADCPlotDFTSpectra, ch, cfg
                , "Builds 2D histograms of SADC FFT amplitude spectra" ) {
    bool profile = cfg["profile"] && cfg["profile"].as<bool>();
    const std::string subjStr = cfg["subject"] ? cfg["subject"].as<std::string>() : "amp";
    na64dp::handlers::SADCPlotDFTSpectra::FTSubject subj;
    if(subjStr == "amp") subj = na64dp::handlers::SADCPlotDFTSpectra::kAmplitude;
    else if(subjStr == "re") subj = na64dp::handlers::SADCPlotDFTSpectra::kRealPart;
    else if(subjStr == "im") subj = na64dp::handlers::SADCPlotDFTSpectra::kImagPart;
    else {
        NA64DP_RUNTIME_ERROR("Bad subject: \"%s\", permitted are: amp, re, im"
                , subjStr.c_str());
    }
    int baseFreqs[2] = {
          cfg["baseFreqs"] ? cfg["baseFreqs"][0].as<int>() : 0
        , cfg["baseFreqs"] ? cfg["baseFreqs"][1].as<int>() : 0
        };
    if(baseFreqs[1] < baseFreqs[0] || (baseFreqs[0] == baseFreqs[1] && 0 != baseFreqs[0])) {
        NA64DP_RUNTIME_ERROR("Bad amp.baseline frequency range [%d:%d)"
                , baseFreqs[0], baseFreqs[1] );
    }
    const bool isSubrangeRelative = (baseFreqs[1] - baseFreqs[0] != 0)
            , dBUnits = cfg["dBUnits"]
                      ? cfg["dBUnits"].as<bool>()
                      : (subj == na64dp::handlers::SADCPlotDFTSpectra::kAmplitude ? true : false);
    std::string descr;
    if(!cfg["histDescr"]) {
        descr = "(M)SADC Wf DFT ";
        if(subj == na64dp::handlers::SADCPlotDFTSpectra::kRealPart) descr += "real part ";
        else if(subj == na64dp::handlers::SADCPlotDFTSpectra::kImagPart) descr += "imag.part ";
        else if(subj == na64dp::handlers::SADCPlotDFTSpectra::kAmplitude) descr += "Spectrum ";
        if(isSubrangeRelative) {
            descr += dBUnits
                  ? "for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp};"
                        " Freq., MHz; Amp., dB"
                  : "for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp};"
                        " Freq., MHz; Amp. ratio"
                  ;
        } else {
            descr += dBUnits
                  ? "for {TBName}; Freq., MHz; Amplitude, dBFS"
                  : "for {TBName}; Freq., MHz; Amp., abs.units"
                  ;
                  ;
        }
    } else {
        descr = cfg["histDescr"].as<std::string>();
    }
    return new handlers::SADCPlotDFTSpectra( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , subj
            , baseFreqs[0], baseFreqs[1]
        , profile ? 10 : cfg["ampNBins"].as<int>()
        , profile ? -1 : cfg["ampRange"][0].as<float>()
            , profile ?  1 : cfg["ampRange"][1].as<float>()
            , profile
            , dBUnits
            , cfg["baseName"].as<std::string>()
            , descr
            , cfg["path"] ? cfg["path"].as<std::string>() : ""
            );
}

}
