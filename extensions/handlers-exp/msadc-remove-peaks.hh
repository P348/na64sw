#pragma once

#include "na64dp/abstractHitHandler.hh"

#include "na64event/data/sadc.hh"

#include <limits>

namespace na64dp {
namespace handlers {

/**\brief Helper class for MSADC max remove
 *
 * Removing element by iterator shall not invalidate other iterators (quoting
 * C++11 standard):
 *      > (§23.2.5/13) ... The erase members shall invalidate
 *      > only iterators and references to the erased elements.
 * */
class RemoveMSADCMax {
public:
    /// Iterator referring to peak to be removed
    typedef decltype(event::RawDataSADC::maxima)::iterator PeakIterator;
    /// Pointer to hit owning the peak to (optionally) increase abs. error
    typedef event::SADCHit * HitRef;
    /// Item defining max remove: hit to increase abs.error, reference to
    /// the peak being removed, abs.error estimate
    typedef std::pair<HitRef, PeakIterator> Item;
    /// Callback type for error estimation
    typedef double (RemoveMSADCMax::*ErrorEstimationMethod)(HitRef, PeakIterator);
private:
    struct PeakIDHash {
        size_t
        operator()(const Item & p) const {
            auto hash1 = std::hash<HitRef>{}(p.first);
            auto hash2 = std::hash<void*>{}(p.second->second.get());
            return hash1 ^ hash2;
        }
    };

    const ErrorEstimationMethod _estimate_error_for_peak_in_hit;
    std::unordered_map<Item, double, PeakIDHash> _itemsToRemove;
    /// Multiplier for peak amp. when this quantity is used to increase amp.
    /// abs. error
    float _ampErrMultiplier;
    /// Multiplier for peak amp. error when this quantity is used to increase
    /// amp. abs. error
    float _ampErrUncertaintyMultiplier;
public:
    RemoveMSADCMax(ErrorEstimationMethod errMtd, float multAmp=1., float multErr=1.);
    /// Set `absErr` to NaN to use internal method for error estimation
    void mark_peak_for_removal(HitRef hitRef, PeakIterator peakIt
            , double absErr=std::numeric_limits<double>::quiet_NaN());
    /// Actually removes the max
    void remove_items();

    // Error estimation methods to be choosen

    /// Does not add any error to the hit upon peak removal
    double no_error(HitRef, PeakIterator) { return std::numeric_limits<double>::quiet_NaN(); }
    /// Estimates error by peak amp. only
    double peak_amp_error(HitRef hr, PeakIterator peakIt);
    /// Estimates error by peak amp. only
    double peak_amp_unc_error(HitRef hr, PeakIterator peakIt);
    /// Estimates hit's error by peak amp. and peak amp error (additively)
    double peak_amp_and_unc_error(HitRef hr, PeakIterator peakIt);

    static ErrorEstimationMethod peak_rm_mode_from_str(const std::string &);
};  // class RemoveMSADCMax

/**\brief Abstract base class for handlers dealing with (M)SADC time clusters
 *
 * Bolierplate for MSADC time cluster analyzis handlers. Collects time
 * clusters into a map calculating various side properties of time clusters.
 * What is done further with this information is defined by subclassing
 * handlers (usually, time clusters and/or events get discriminated).
 *
 * \code{.yaml}
 *     - _type: BaseMSADCTimeClusterDiscriminate
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * \note Handler may remove stray peaks (not marked as ones included into any
 *       time cluster).
 * */
class BaseMSADCTimeClusterDiscriminate
            : public AbstractHitHandler<event::SADCHit>
            , protected RemoveMSADCMax
            {
protected:
    /// What to do with "stray" peaks (standalone, not belonging to any
    /// particular cluster)
    ErrorEstimationMethod _removeStrayPeaks;

    ///\brief Temporary time cluster representation (utility structure)
    struct TimeCluster : public std::unordered_map<HitRef, PeakIterator> {
        double maxAmp;
        TimeCluster()
            : maxAmp(-std::numeric_limits<decltype(maxAmp)>::infinity())
            {}
        void add_peak(HitRef, PeakIterator);
    };
    /// Time clusters built within an event based on previously assigned labels
    std::unordered_map<int, TimeCluster> _clusters;

    virtual bool _analyze_clusters() = 0;
public:
    /// ... features of the ctr, if any
    BaseMSADCTimeClusterDiscriminate( calib::Dispatcher & cdsp
                , const std::string & selection
                , ErrorEstimationMethod removeStrayPeaks  // may be set to null to disable
                , ErrorEstimationMethod commonPeakRemovalMethod
                , log4cpp::Category & logCat
                , float multErrAmp=1., float multErrErr=1
                , const std::string & namingSubclass="default"
                );
    virtual ProcRes process_event(event::Event &) override;
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class BaseMSADCTimeClusterDiscriminate

}  // namespace ::na64dp::handlers
}  // namespace na64dp


