#include "na64calib/dispatcher.hh"
#include "na64common/calib/msadc-PedLED.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64common/config.h"
#include "na64/donskov-waveform-reconstruction.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64util/na64/event-id.hh"
#include "na64detID/cellID.hh"
#include "na64event/reset-values.hh"
#include "na64util/str-fmt.hh"
#include <cmath>
#include <log4cpp/Category.hh>
#include <na64/libna64utils-config.h>

namespace na64dp {
namespace handlers {

//
// Pedestals reconstruction

/**\brief Performs pedestals finding somewhat similar to S.V. Donskov's way
 *
 * This handler forwards actual pedestal finding to `svd_wf_reco__defped()`
 * routine which is literal copy of original Donskov's function from `na64.cc`
 * app. Besides of some settings and waveform, this function requires some
 * predefined data about pedestals mean per burst and pedestals in previous
 * event which can be or can not be used, depending on the waveform.
 *
 * This handler assumes that burst mean pedestals are written in hit's
 * pedestals information (use `AssignPedestals` header for instance) and
 * maintains pedestals being successfully derived from previous event.
 * */
class MSADCWfPedestalsSVD
        : public AbstractHitHandler<event::SADCHit>
        {
protected:
    /// Parameters for original SVD `defped()` procedure
    na64util::SVDWfPedestalFindingParameters _svdParameters;
    /// "Backup pedestals" found for previous spill, per detector
    std::unordered_map<DetID, std::pair<float, float>> _backupsByID;
public:
    MSADCWfPedestalsSVD( calib::Dispatcher & cdsp
                , log4cpp::Category & logCat
                , const std::string & sel
                , const std::string & namingSubclass="default" )
        : na64dp::AbstractHitHandler<event::SADCHit>(cdsp, sel, logCat, namingSubclass)
        {}

    bool process_hit( EventID eid
                    , DetID did
                    , event::SADCHit & hit
                    ) {
        if(!hit.rawData) return true;
        // copy samples array
        assert(_svdParameters.n_samples == 32);
        int sample[32];
        for(int i = 0; i < 32; ++i) {
            sample[i] = hit.rawData->wave[i];
            assert(fabs(sample[i] - hit.rawData->wave[i]) < 1e-3);
        }
        // pedestals found (output of `na64common::svd_wf_reco__defped()`)
        na64util_Float_t pedestals[2];
        // backup pedestals; should be set to previously found ones (from
        // previous event) if there are no pedestals available, ones from
        // DB must be used (mean per spill).
        // IMPORTANT: we assume here that latter ones are provided by hit's
        // `.pedestals[2]` attribute
        na64util_Float_t backupPedestals[2];
        auto prevIt = _backupsByID.find(did);
        if(_backupsByID.end() != prevIt) {
            backupPedestals[0] = prevIt->second.first;
            backupPedestals[1] = prevIt->second.second;
        } else {
            backupPedestals[0] = hit.rawData->pedestals[0];
            backupPedestals[1] = hit.rawData->pedestals[1];
        }
        // call Donskov's original pedestal-finding procedure
        int rc = na64util::svd_wf_reco__defped(
                  sample
                , pedestals
                , backupPedestals
                , _svdParameters
                );
        // rc -> 0 means that new pedestals were calculated, we should memorize
        // it as preferable backup
        if(0 == rc) {
            _backupsByID[did] = std::pair<float, float>(pedestals[0], pedestals[1]);
        } else if(1 != rc) {
            NA64DP_RUNTIME_ERROR("na64common::svd_wf_reco__defped()"
                    " returned %d", rc);
        }
        #if 1
        _log.debug("Backup pedestals used for %s in %s (%.2f, %.2f)"
                , naming()[did].c_str(), eid.to_str().c_str()
                , backupPedestals[0], backupPedestals[1] );
        #endif
        hit.rawData->pedestals[0] = pedestals[0];
        hit.rawData->pedestals[1] = pedestals[1];
        return true;
    }
};

//
// Waveform reconstruction

/**\brief ad-hoc handler with Donskov's reconstruction
 *
 * this is (almost) literal copy of the original `htime()' function from Sergey
 * Donskov's original application performing waveform reconstruction. It relies
 * on (M)SADC calibrations given in standard Type#1 form.
 */
class MSADCWfRecoSVD
        : public AbstractHitHandler<event::SADCHit>
        , public calib::Handle< calib::Collection<calib::SADCCalib> >
        {
private:
    /// Major parameters for S.Donskov reconstruction function
    na64util::SVDWfRecoParams _p;
protected:
    struct TimeCalibStruct {
        float time, timesigma, ledtime;
        bool timeIsRelativeToMT;
    };

    // ... TODO: seems that this is rather a flag set by caller code for LED
    // events, isn't it?
    bool _ledTrigger;
    /// Timing calibrations dict
    std::unordered_map<DetID, TimeCalibStruct> _timeCalibs;

    ///\brief (Re-)caches calibration data index
    ///
    /// Converts SDC calibration entry for `p348reco` into `SADCCalib` object
    /// and places it into the `_calibs` index by corresponding name.
    void handle_update( const calib::Collection<calib::SADCCalib> & cc ) override {
        calib::Handle< calib::Collection<calib::SADCCalib> >::handle_update(cc);
        for(const auto & c : cc) {
            if(std::isnan(c.data.peakpos) )
                continue;  // omit no-calibration cases
            // convert name to detector ID
            DetID did = nameutils::sdc_id( naming(), c.data.name
                                         , c.data.x, c.data.y, c.data.z
                                         , c.srcDocID, c.lineNo //< optional
                                         );
            // copy the calibration data to the cache
            _timeCalibs[did] = TimeCalibStruct{ c.data.time
                , c.data.timesigma
                , /*c.data.ledtime*/ (float) std::nan("0")
                , c.data.is_relative /*timeIsRelativeToMT*/
                };  // TODO: LED time calib
        }
    }
public:
    MSADCWfRecoSVD( calib::Dispatcher & cdsp
                , log4cpp::Category & logCat
                , const std::string & sel
                , bool ledTrigger=false
                , const std::string & namingSubclass="default"
                )
        : na64dp::AbstractHitHandler<event::SADCHit>(cdsp, sel, logCat, namingSubclass)
        , calib::Handle< calib::Collection<calib::SADCCalib> >(namingSubclass, cdsp)
        , _ledTrigger(ledTrigger)
        {}
    bool process_hit(EventID, DetID, event::SADCHit &) override;
};


bool
MSADCWfRecoSVD::process_hit( EventID eid
            , DetID detID
            , event::SADCHit & hit
            ) {
    na64util_Float_t waveCpy[32];

    auto it = _timeCalibs.find(detID);
    if(it == _timeCalibs.end()) return true;  // no calibs
    na64util_Float_t r[3]  // time, max.sample no, max.amp 
                      , maximaTimes[_p.n_maxima]
                      , maximaAmps[_p.n_maxima]
                      ;
    uint8_t maximaNSamples[_p.n_maxima];
    for(size_t i = 0; i < 32; ++i) { waveCpy[i] = hit.rawData->wave[i];}  // type conversion
    int rc =
        na64util::svd_wf_reco__htime( _p
                     , waveCpy
                     , sizeof(event::RawDataSADC::wave)/sizeof(event::RawDataSADC::wave[0])
                     , hit.rawData->pedestals[0], hit.rawData->pedestals[1]
                     , _ledTrigger
                     , it->second.timeIsRelativeToMT ? _current_event().masterTime : 0.
                     , it->second.time, it->second.ledtime
                     , r
                     , maximaNSamples, maximaTimes, maximaAmps
                     );
    if(rc < 0) {
        NA64DP_RUNTIME_ERROR("na64common::svd_wf_reco__htime() returned %d", rc);
    }
    //std::cout << " XXX best time is " << r[0] << std::endl;  // XXX
    if(r[0] > -300) {  // magic number denoting failure in time estimation
        hit.time = r[0];
    }
    if(0 != (int) round(r[1])) {
        hit.rawData->maxSample = (int) round(r[1]);
    }
    if(!std::isnan(r[2])) {
        hit.rawData->maxAmp = r[2];
    }

    //
    // Copy found peaks
    int i;
    for(i = 0; i < _p.n_maxima; ++i) {
        auto peakPtr = lmem().create<event::MSADCPeak>(lmem());
        util::reset(*peakPtr);
        peakPtr->time = maximaTimes[i];
        peakPtr->amp  = maximaAmps[i];
        hit.rawData->maxima.emplace(maximaNSamples[i], peakPtr);
        if(peakPtr->time) break;

        //hit.rawData->maximaTimes[i] = maximaTimes[i];
        //hit.rawData->maximaAmps[i]  = maximaAmps[i];
        //if(std::isnan(hit.rawData->maximaTimes[i])) break;
        //hit.rawData->maxima[i] = maximaNSamples[i];
        //std::cout << " #" << i << ": " << (int) maxima[i] << ", " << maximaTimes[i] << std::endl;  // XXX
    }
    // write terminating elements
    //hit.rawData->maximaTimes[i] = hit.rawData->maximaAmps[i] = std::nan("0");
    //hit.rawData->maxima[i] = std::numeric_limits<uint8_t>::max();
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCWfPedestalsSVD, mgr, cfg
        , "Donskov-like pedestals finding procedure") {
    return new na64dp::handlers::MSADCWfPedestalsSVD( mgr
                , na64dp::aux::get_logging_cat(cfg)
                , na64dp::aux::retrieve_det_selection(cfg)
                , na64dp::aux::get_naming_class(cfg)
                );
}

REGISTER_HANDLER( MSADCWfRecoSVD, mgr, cfg
        , "Ad-hoc master time settings for 2023e") {
    mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<na64dp::EventID, P348_RECO_NAMESPACE::MSADCCalibPedLedType1>();
    na64dp::calib::CIDataAliases::self()
            .add_dependency("donskov/PedLED", "naming");
    return new na64dp::handlers::MSADCWfRecoSVD( mgr
                , na64dp::aux::get_logging_cat(cfg)
                , na64dp::aux::retrieve_det_selection(cfg)
                , cfg["ledTrigger"] ? cfg["ledTrigger"].as<bool>() : false
                , na64dp::aux::get_naming_class(cfg)
                );
}


