#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64/ddd-ext/event-id.hh"

#include <log4cpp/Priority.hh>

#include <sstream>
#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief ...
 *
 * \code{.yaml}
 *     - _type: MyHitHandler
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * */
class DumpMSADCSelection : public AbstractHitHandler<event::SADCHit> {
protected:
    std::map<EventID, std::set<std::string>> _selectionStr;
    std::ofstream _ofs;
public:
    /// ... features of the ctr, if any
    DumpMSADCSelection( calib::Dispatcher & cdsp
                , const std::string & selection
                , const std::map<EventID, std::set<std::string>> & sel
                , const std::string & filename
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _selectionStr(sel)
        , _ofs(filename)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class MyHitHandler


bool
DumpMSADCSelection::process_hit(EventID eid, DetID did, event::SADCHit & hit) {
    auto evIt = _selectionStr.find(eid);
    if(_selectionStr.end() == evIt) return true;
    auto it = evIt->second.find(naming()[did]);
    if(it == evIt->second.end()) return true;
    log() << log4cpp::Priority::DEBUG
        << "Dump, event " << eid << "), detector ID " << *it;
    _ofs << eid << " " << *it;
    for(int i = 0; i < 32; ++i) {  // TODO: nSamples
        _ofs << " " << hit.rawData->wave[i];
    }
    _ofs << std::endl;
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( DumpMSADCSelection, cdsp, cfg
                , "..."
                ) {
    using namespace na64dp;
    auto & L = aux::get_logging_cat(cfg);

    std::map<EventID, std::set<std::string>> selection;

    const YAML::Node & selNode = cfg["selection"];
    for(auto nodeIt = selNode.begin(); nodeIt != selNode.end(); ++nodeIt) {
        // parse event ID
        auto eventIDStr = nodeIt->first.as<std::string>();
        EventID eid;
        auto eid_ = ddd_ext::EventID::parse(eventIDStr.c_str());
        eid.run_no(eid_.runNo);
        eid.spill_no(eid_.spillNo);
        eid.event_no(eid_.eventNo);
        auto ir = selection.emplace(eid, std::set<std::string>());
        // collected detector IDs
        const auto & detIDsStr = nodeIt->second.as<std::vector<std::string>>();
        ir.first->second.insert(detIDsStr.begin(), detIDsStr.end());
        L << log4cpp::Priority::INFO << "Read " << ir.first->second.size() << " items for event "
            << eid;
    }

    return new handlers::DumpMSADCSelection( cdsp
            , aux::retrieve_det_selection(cfg)
            , selection
            , cfg["file"].as<std::string>()
            // ... your additional arguments retrieved from `cfg'
            , L
            , aux::get_naming_class(cfg)
            );
}

