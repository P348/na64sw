#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "msadc-fft.h"

#include <gsl/gsl_fft_real.h>
#include <limits>

namespace na64dp {
namespace handlers {

/**\brief Radix2-based FFT transformation for MSADC
 *
 * Applies Radix-2 FFT to compute in-place FFT. This algorithm is only valid
 * for number of samples being the power of 2 (which is the case for all
 * (M)SADC detectors as in 2023).
 *
 * \code{.yaml}
 *     - _type: MSADCFFTRadix2
 *       # Stride parameter for Radix2 DFFT; int, opt
 *       stride: 1
 *       # Windowing function. Possible choices: "rect" (no window, default),
 *       # "hann", "hamming", "blackman"; str, opt
 *       window: rect
 * \endcode
 *
 * \note The `stride` parameter is barely needed in practice.
 * */
class MSADCFFTRadix2 : public AbstractHitHandler<event::SADCHit> {
private:
    int _stride;
    float _ADCMax;
    std::function<void(int, double *)> _windowFunction;
    double * _window;
public:
    /// Parameterised mainly by the stride parameter
    MSADCFFTRadix2( calib::Dispatcher & cdsp
                , const std::string & selection
                , int stride_
                , float ADCMax
                , std::function<void(int, double *)> windowFunction
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _stride(stride_)
        , _ADCMax(ADCMax)
        , _windowFunction(windowFunction)
        , _window(NULL)
        {
        // TODO: this is true only for 32-sampled MSADC. To support other
        // samples one would have to create a cache of `_window` arrays or
        // something...
        if(windowFunction) {
            _window = new double [32];
            _windowFunction(32, _window);
        }
    }
    ~MSADCFFTRadix2() {
        if(_window) delete [] _window;
    }
    /// Applies Radix-2 DFFT to waveform, creates `fft` member of (M)SADC hit
    /// if needed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class MSADCFFTRadix2


bool
MSADCFFTRadix2::process_hit(EventID, DetID did, event::SADCHit & hit) {
    constexpr size_t waveArrLength = sizeof(event::RawDataSADC::wave)/sizeof(event::RawDataSADC::wave[0]);
    double wave[waveArrLength];  // double-typed copy of waveform
    if(!_window) {
        for(unsigned int i = 0; i < waveArrLength; ++i) {
            wave[i] = hit.rawData->wave[i]/_ADCMax;
        }
    } else {
        assert(32 == waveArrLength);
        // ^^^ TODO: otherwise `_window` has to become a map to support non-32
        //     sampled MSADC
        for(unsigned int i = 0; i < waveArrLength; ++i) {
            wave[i] = _window[i]*hit.rawData->wave[i]/_ADCMax;
        }
    }
    // Apply in-place transform:
    int rc = gsl_fft_real_radix2_transform(wave, _stride, waveArrLength);
    if(0 != rc) {
        log().warn("gsl_fft_real_radix2_transform(..., %d, %d) returned %d for %s"
                " waveform, DFFT cancelled for the hit."
                , rc, _stride, waveArrLength
                , naming()[did].c_str() );  // TODO: gsl_strerror()?
        return true;
    }
    // NOTE: Radix-2 output layout is a bit tricky in the imaginary part. Here
    // we (re)create `fft` item and fill it with "unpacked" values taking into
    // account the layout
    if(!hit.rawData->fft) {
        hit.rawData->fft = this->lmem().create<event::MSADCDFFTCoefficients>(lmem());
        util::reset(*hit.rawData->fft);
    }

    // Copy coefficients (note the 1st element, it's a tricky one!)
    // into "compact form"
    hit.rawData->fft->real[0] = wave[0];
    int i;
    assert(waveArrLength < std::numeric_limits<int>::max());
    for(i = 1; i < static_cast<int>(waveArrLength) - i; ++i) {
        hit.rawData->fft->real[i] = wave[i*_stride];
        hit.rawData->fft->imag[i] = wave[(waveArrLength - i)*_stride];
        //std::cout << "xxx #" << i << ": "
        //    << hit.rawData->fft->real[i]
        //    << " + " << hit.rawData->fft->imag[i] << "j"
        //    << std::endl;
    }
    // for even sampling rates the n//2 + 1 contains Nyquist frequency (for odd
    // there will be no such term)
    if(i == static_cast<int>(waveArrLength) - i) {
        // this is 16-th freq real part
        hit.rawData->fft->imag[0] = wave[(waveArrLength - 1)*_stride];
        //std::cout << "xxx #" << i << " " << (waveArrLength - 1)*_stride << ": "
        //    << hit.rawData->fft->imag[0]
        //    << std::endl;
    }

    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MSADCFFTRadix2, cdsp, cfg
                , "Calculates real-valued DFFT of (M)SADC waveform"
                ) {
    using namespace na64dp;
    std::function<void(int, double *)> windowFunction;
    std::string windowStr = cfg["window"] ? cfg["window"].as<std::string>() : "rect";
    if(windowStr == "hann") {
        windowFunction = [](int n, double * vs) {
                na64sw_msadc_fft_window_cosSum(n, 0.5, 0.5, 0., vs); };
    } else if(windowStr == "hamming") {
        windowFunction = [](int n, double * vs) {
                na64sw_msadc_fft_window_cosSum(n, 25/46., (1-25/46.), 0., vs); };
    } else if(windowStr == "blackman") {
        windowFunction = [](int n, double * vs) {
                na64sw_msadc_fft_window_cosSum(n, 7938./18608, 9240./18608, 1430./18608, vs); };
    } else if(windowStr != "rect") {
        NA64DP_RUNTIME_ERROR("Unknown window function type: \"%s\"", windowStr.c_str());
    }
    return new handlers::MSADCFFTRadix2( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["stride"] ? cfg["stride"].as<int>() : 1
            , cfg["ADCMax"] ? cfg["ADCMax"].as<float>() : 4096
            , windowFunction
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

