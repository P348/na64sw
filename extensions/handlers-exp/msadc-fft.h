#ifndef H_NA64SW_MSADC_FFT_H
#define H_NA64SW_MSADC_FFT_H 1

#ifdef __cplusplus
extern "C" {
#endif

/**\brief Converts "compact" FFT layout to "standard" one for real-only signal
 *
 * A "standard" layout for real data is half-spectrum that is a bit more sparse
 * than one used in the event ("compact"). Complex part of (D)FFT has no
 * imag. part, also a Nyquist frequency exists for even sampling rates. This
 * function turns layout in a proper form for calculus.
 *
 * \note Size of `compactRe`, `compactIm` must be of `nFreqs` elements while
 *       `stdRe` and `stdIm` must be allocated for to hold `nFreqs+1`.
 * */
void
na64sw_msadc_fft_compact2std( unsigned int nFreqs
                            , const double * compactRe, const double * compactIm
                            , double * stdRe, double * stdIm
                            );
/**\brief Converts "standard" FFT layout to "compact" one for real-only signal
 *
 * A "standard" layout for real data is half-spectrum that is a bit more sparse
 * than one used in the event ("compact"). Complex part of (D)FFT has no
 * imag. part, also a Nyquist frequency exists for even sampling rates. This
 * function turns layout of a proper form for calculus into compact form.
 *
 * \note Size of `compactRe`, `compactIm` must be of `nFreqs` elements while
 *       `stdRe` and `stdIm` must be allocated for to hold `nFreqs+1`.
 * */
void
na64sw_msadc_fft_std2compact( unsigned int nFreqs
                            , const double * stdRe, const double * stdIm
                            , double * compactRe, double * compactIm
                            );

/**\brief Calculates absolute amplitudes without scaling */
void
na64sw_msadc_fft_abs_amps( unsigned int nFreqs
                         , const double * stdRe, const double * stdIm
                         , double * amps
                         , double fact
                         );

/**\brief Calculates amplitudes in 20*log10() scale
 *
 * This definition is very close to one used to define `dB` or `dBFS` units
 * (the main difference is the `baselevel` value). */
void
na64sw_msadc_fft_20log10_amps( unsigned int nFreqs
                         , const double * stdRe, const double * stdIm
                         , double * amps
                         , double fact, double baselevel
                         );

/**\brief Converts (D)FFT result for real-only data into Decibell Full-Scale
 *        amplitudes
 *
 * dBFS units are a bit more convenient for applications. `winSum` must be set
 * to `2*nFreqs` if no windowing were applied to a signal, otherwise it must
 * be sum of weights within a window.
 *
 * \note "standard" layout is expected for freqs, `dBFSAmps` must be
 *       of `nFreqs+1` length
 * */
void
na64sw_msadc_fft_dBFS( unsigned int nFreqs
                     , const double * stdRe, const double * stdIm
                     , const double ref, const double winSum
                     , double * dBFSAmps
                     );

/**\brief Generates list of frequencies (in MHz) corresponding to (D)FFT of
 *        given sampling rate
 *
 * \note `freqValues` must be of `nFreqs+1` elements length.
 * */
void
na64sw_msadc_fft_freqs_MHz( unsigned int nFreqs
                          , double * freqValues
                          , double nsSampleTime
                          );

/*
 * Window functions
 */

/**\brief Fills `f` with windowing function values of "cosine-sum" family
 *
 * * Use `a0=0.5`, `a1=1-a0` to generate von Hann window
 * * Use `a0=25/46`, `a1=1-a0` to generate Hamming window
 * * Use `a0=7938/18608`, `a1=9240/18608`, `a2=1430/18608` for Blackman window
 *
 * \todo Function uses some kind of poor-man normalizations, perhaps can be
 *       optimized with analytical integral.
 * */
void
na64sw_msadc_fft_window_cosSum( const int n
                              , const double a0, const double a1, const double a2
                              , double * fValues
                              );

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  /* H_NA64SW_MSADC_FFT_H */
