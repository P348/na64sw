#include "msadc-remove-peaks.hh"

namespace na64dp {
namespace handlers {

RemoveMSADCMax::RemoveMSADCMax(ErrorEstimationMethod errMtd, float multAmp, float multErr)
    : _estimate_error_for_peak_in_hit(errMtd)
    , _ampErrMultiplier(multAmp)
    , _ampErrUncertaintyMultiplier(multErr)
    {}

void
RemoveMSADCMax::mark_peak_for_removal(HitRef hitRef, PeakIterator peakIt, double absErr) {
    _itemsToRemove.emplace(Item(hitRef, peakIt), absErr);
}

void RemoveMSADCMax::remove_items() {
    for(auto & item: _itemsToRemove) {
        auto & hitRef  = std::get<0>(item.first);
        auto & peakRef = std::get<1>(item.first);
        double absErr = item.second;
        if((!std::isfinite(absErr)) && _estimate_error_for_peak_in_hit) {  // i.e. error was not set explicitly
            absErr = (this->*_estimate_error_for_peak_in_hit)(hitRef, peakRef);
        }
        // increase or initialize hit's error, if set
        if(std::isfinite(absErr)) {  // note non-finite error estimates are simply dropped
            if(std::isfinite(hitRef->rawData->maxAmpError))
                hitRef->rawData->maxAmpError += absErr;
            else
                hitRef->rawData->maxAmpError  = absErr;
        }
        hitRef->rawData->maxima.erase(peakRef);
    }
    _itemsToRemove.clear();
}

/// Estimates error by peak amp. only
double RemoveMSADCMax::peak_amp_error(HitRef hr, PeakIterator peakIt) {
    assert(peakIt->second);
    return _ampErrMultiplier*peakIt->second->amp;
}
/// Estimates error by peak amp. only
double RemoveMSADCMax::peak_amp_unc_error(HitRef hr, PeakIterator peakIt) {
    assert(peakIt->second);
    return _ampErrMultiplier*peakIt->second->ampErr;
}
/// Estimates hit's error by peak amp. and peak amp error (additively)
double RemoveMSADCMax::peak_amp_and_unc_error(HitRef hr, PeakIterator peakIt) {
    assert(peakIt->second);
    return (std::isfinite(peakIt->second->amp) ?    peakIt->second->amp*_ampErrMultiplier : 0.)
         + (std::isfinite(peakIt->second->ampErr) ? peakIt->second->ampErr*_ampErrUncertaintyMultiplier : 0);
}

RemoveMSADCMax::ErrorEstimationMethod
RemoveMSADCMax::peak_rm_mode_from_str(const std::string & name) {
    if(name == "disable")               return nullptr;
    if(name == "noError")               return &RemoveMSADCMax::no_error;
    if(name == "byAmplitude")           return &RemoveMSADCMax::peak_amp_error;
    if(name == "byUncertainty")         return &RemoveMSADCMax::peak_amp_unc_error;
    if(name == "byAmpAndUncertainty")   return &RemoveMSADCMax::peak_amp_and_unc_error;
    NA64DP_RUNTIME_ERROR("Invalid peak error estimation method name: \"%s\""
            , name.c_str());
}

BaseMSADCTimeClusterDiscriminate::BaseMSADCTimeClusterDiscriminate( calib::Dispatcher & cdsp
            , const std::string & selection
            , ErrorEstimationMethod removeStrayPeaks  // may be set to null to disable
            , ErrorEstimationMethod commonPeakRemovalMethod
            , log4cpp::Category & logCat
            , float multErrAmp, float multErrErr
            , const std::string & namingSubclass
            )
    : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
    , RemoveMSADCMax(commonPeakRemovalMethod, multErrAmp, multErrErr)
    , _removeStrayPeaks(removeStrayPeaks)
    {}

void
BaseMSADCTimeClusterDiscriminate::TimeCluster::add_peak(HitRef hr, PeakIterator peakIt) {
    assert(peakIt->second);
    if(peakIt->second->amp > maxAmp) maxAmp = peakIt->second->amp;
    emplace(hr, peakIt);
    assert(size());  // XXX
}

bool
BaseMSADCTimeClusterDiscriminate::process_hit( EventID, DetID, event::SADCHit & hit) {
    // quietly omit hits without max info
    if(!hit.rawData) return true;
    if(hit.rawData->maxima.empty()) return true;
    // collect clusters
    for( auto maxIt = hit.rawData->maxima.begin()
       ; hit.rawData->maxima.end() != maxIt
       ; ++maxIt ) {  // iterate by max items
        //const int maxID = maxIt->first;
        auto & maxItem = maxIt->second;
        int tClusterLabel = maxItem->timeClusterLabel;
        if( -1 == tClusterLabel ) {
            // peak not considering by clustering algorithm met in the data.
            // todo: warn user?
            continue;
        }
        if( 0 == tClusterLabel ) {  // stray peak
            if( nullptr != _removeStrayPeaks )
                RemoveMSADCMax::mark_peak_for_removal(&hit, maxIt);
            continue;  // omit stray peaks
        }
        if(tClusterLabel < 0) {  // XXX
            log().warn("Bad time cluster label: %d, skipping event", tClusterLabel);
            _set_event_processing_result(kDiscriminateEvent);
            return false;
        }
        assert(tClusterLabel > 0);  // otherwise the event data is probably corrupted
        // add hit in the clusters map for further consideration
        auto it = _clusters.find(tClusterLabel);
        if(_clusters.end() == it) {
            it = _clusters.emplace(tClusterLabel, TimeCluster()).first;
        }
        it->second.add_peak(&hit, maxIt);
    }

    return true;
}

AbstractHandler::ProcRes
BaseMSADCTimeClusterDiscriminate::process_event(event::Event & ev) {
    auto rc = AbstractHitHandler<event::SADCHit>::process_event(ev);
    log().debug("Found %zu time clusters in the event.", _clusters.size());
    if(_clusters.size() > 1) {
        _analyze_clusters();
    }
    remove_items();  // actually remove peaks marked for removal
    #if 0  // temporary check: assign survived cluster to hit's values
    {
        for(auto & c : _clusters) {
            for(auto peakIt : c.second) {
                peakIt.first->rawData->maxAmp = peakIt.second->second->amp;
                peakIt.first->time = peakIt.second->second->time;
            }
        }
    }
    #endif
    _clusters.clear();
    return rc;
} 

}  // namespace ::na64dp::handlers
}  // namespace na64dp

