#include "na64dp/abstractHitHandler.hh"
#include "na64util/pair-hash.hh"

#include <cmath>
#include <na64/time-based-pulse-iterator.hh>
#include <unordered_set>

namespace na64dp {
namespace handlers {

/**\brief Creates a `CaloHit` item in an event, based on per-cells hits os
 *        same cluster ID computing overall energy, sum, etc
 *
 * Builds up entire calorimeter hit as a whole, based on enrgy deposition in
 * particular cells defined by hit's `maxima` item(s). Depending on
 * configuration, may create ECAL, HCAL, BGO, SRD etc. Adds hits (instances of
 * `CaloHit` structure) to an event.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: MakeCaloHitByTimeClusterID
 *       # optional, hit selection expression
 *       applyTo: null
 *       # list of detector names to apply; list of strings, required
 *       detectors: [..., ...]
 * \endcode
 *
 * Detectors that supposed to be the subject of hits creation should be
 * provided as their names, like "HCAL" (for entire HCAL) or "HCAL3" (for HCAL
 * module #3), "ECAL", "ECAL0", "ECAL1", etc.
 *
 * Resulting hits will be imposed into main event's `caloHits` map.
 *
 * \ingroup exp-handlers calo-handlers
 * */
class MakeCaloHitByTimeClusterID : public AbstractHitHandler<event::SADCHit> {
public:
    typedef std::pair<DetID, na64util::TimeClusterID_t> Key_t;
private:
    /// Collection of tags
    std::set<std::string> _tags;
    std::unordered_set<DetID> _tagsConverted;
protected:
    /// A collection of hits to be imposed once event is done.
    std::unordered_map<
          Key_t
        , std::pair<mem::Ref<event::CaloHit>, size_t>
        , util::PairHash
        > _collectedCaloHits;

    /// Re-caches the calorimeter kins
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    /// Creates new instance parameterised with calorimeter station selection
    /// to apply the sums
    MakeCaloHitByTimeClusterID( calib::Dispatcher &  ch
               , const std::string & selection
               , const std::set<std::string> & tags_
               ) : AbstractHitHandler<event::SADCHit>(ch, selection)
                 , _tags(tags_) {}

    /// Re-sets the computed sums at each the event
    virtual ProcRes process_event(event::Event & event) override;

    /// Modifies the hit accordingly
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & currentHit ) override;
};

//                          * * *   * * *   * * *

void
MakeCaloHitByTimeClusterID::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::SADCHit>::handle_update(nm);  // fwd to parent
    // re-cache the sums
    _collectedCaloHits.clear();
    for( auto tag : _tags ) {
        _tagsConverted.insert(nm[tag]);
    }
}

bool
MakeCaloHitByTimeClusterID::process_hit( EventID
                        , DetID detID
                        , event::SADCHit & hit ) {
    for( auto & detIDToCollect : _tagsConverted ) {
        if( ! detIDToCollect.matches(detID) ) continue;
        if( ! hit.rawData ) continue;
        if( hit.rawData->maxima.empty() ) continue;
        for( const auto & pulse : hit.rawData->maxima ) {
            // omit pulses without cluster label (-1 -- not clustered,
            // 0 -- not in cluster)
            if(pulse.second->timeClusterLabel < 1)
                continue;
            // construct key object unqie for detector tag and cluster ID
            auto key = Key_t(detIDToCollect, pulse.second->timeClusterLabel);
            // find "CaloHit" corresponding to this detector and this time
            // cluster
            auto it = _collectedCaloHits.find( key );

            if(it != _collectedCaloHits.end()) {
                auto hitRef = it->second.first;

                assert(hitRef->timeClusterLabel == key.second);

                // note: few pulses can be added to same calo hit from same
                // detector. Although this is most probably not a valid case
                // we anticipate it for data integrity
                hitRef->hits.emplace(detID, _current_hit_iterator()->second);
                ++(it->second.second);

                // append pulse to the CaloHit (not the SADCHit!)
                hitRef->eDep += pulse.second->amp;  // TODO: guarantee calibrated energy!
                hitRef->eDepError += pulse.second->amp;
                hitRef->time += pulse.second->time;
                hitRef->timeStddev += pulse.second->timeErr * pulse.second->timeErr;
            } else {
                // no existing entries found -- create one, initalizing sums
                auto newHitRef = lmem().create<event::CaloHit>(lmem());
                util::reset(*newHitRef);
                newHitRef->timeClusterLabel = key.second;

                newHitRef->hits.emplace(detID, _current_hit_iterator()->second);

                newHitRef->eDep = pulse.second->amp;
                newHitRef->eDepError = 0.;
                newHitRef->time = 0.;
                newHitRef->timeClusterLabel = 0.;

                _collectedCaloHits.emplace(Key_t(detID, pulse.second->timeClusterLabel)
                        , std::pair<mem::Ref<event::CaloHit>, size_t>(newHitRef, 1) );
            }
        }
    }
    return true;
}

AbstractHandler::ProcRes
MakeCaloHitByTimeClusterID::process_event(event::Event & event) {
    auto r = AbstractHitHandler<event::SADCHit>::process_event(event);
    // create calo hits and reset cached sums
    for( auto & p : _collectedCaloHits ) {
        assert( p.second.first );
        auto ir = event.caloHits.emplace(p.first.first, p.second.first);
        // finalize calo hit calculus
        auto caloHitRef = *ir->second;
        assert(caloHitRef.timeClusterLabel == p.first.second);

        caloHitRef.time /= p.second.second;
        caloHitRef.timeStddev = sqrt(caloHitRef.timeStddev/p.second.second);
        caloHitRef.eDep /= p.second.second;
        caloHitRef.eDepError = sqrt(caloHitRef.eDepError/p.second.second);
    }
    return r;
}

}

REGISTER_HANDLER( MakeCaloHitByTimeClusterID, ch, yamlNode
                , "Sums up energy deposition from individual stations to calorimeter energy." ) {
    auto tags = yamlNode["detectors"].as<std::vector<std::string> >();
    return new handlers::MakeCaloHitByTimeClusterID( ch
                , aux::retrieve_det_selection(yamlNode)
                , std::set<std::string>(tags.begin(), tags.end())
                //, yamlNode["nullifyNegative"] ? yamlNode["nullifyNegative"].as<bool>() : false
                //, yamlNode["errorOnMissed"] ? yamlNode["errorOnMissed"].as<bool>() : true
                );
}

}


