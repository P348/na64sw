#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"
#include "na64util/str-fmt.hh"
#include "msadc-fft.h"

#include <TArrayI.h>
#include <TProfile.h>
#include <TH2F.h>
#include <TH2.h>
#include <TColor.h>
#include <TStyle.h>
#include <THnSparse.h>

#include <cmath>
#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Create discretized EPDF for DFT spectra
 *
 * \see TDirAdapter
 * \ingroup handlers sadc-handlers
 * */
class SADC_DFT_EPDF : public util::TDirAdapter
                    , public AbstractHitHandler<event::SADCHit> {
public:
    enum FTSubject {
        kAmplitude,
        kRealPart,
        kImagPart,
    };
private:
    /// Number of bins for every amp
    size_t _ampNBins;
    /// Amplitude range bounds (not used if profile)
    double _ampRange[2];
    /// Histogram base name suffix
    const std::string _hstBaseName
                    , _hstDescr
                    , _overridenPath
                    ;
    /// Histogram index by detector ID
    std::map<DetID_t, std::pair<TArrayI*, THnSparseI *>> _hists;
    /// baselevel averaging range for plotting in dB or as fractions
    int _baseWindow[2];
    /// When set the plotted amplitudes are either dB (when `_baseWindow` is
    /// set) or dBFS. When this flag is not set eith absolute (D)FFT freq's
    /// value is plotted (when `_baseWindow` is not set or of 0 length), or
    /// fraction wrt mean value from `_baseWindow` is plotted.
    bool _dBUnits;
    /// Plotting subject: amp, re%, im, etc
    FTSubject _subj;
    /// List of selected frequencies
    std::vector<int> _selectedFreqs;
public:
    /// Main ctr of the handler
    SADC_DFT_EPDF( calib::Dispatcher & ch
                 , const std::string & selection
                 , FTSubject subj
                 , const std::vector<int> & selectedFreqs
                 , int baseFreqLow, int baseFreqUp
                 , size_t nAmpBins
                 , double ampMin, double ampMax
                 , bool profile=false
                 , bool dBUnits=true
                 , const std::string & hstBaseName="fftAmp"
                 , const std::string & descr="SADC Wf DFFT Spectra for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp}; Freq.no; Amplitude, dB"
                 , const std::string & path=""
                 );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADC_DFT_EPDF();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & currentEcalHit) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

/** \param ch Calibration dispatcher instance
 *  \param selection Detector selection expression
 *  \param nAmpBins Number of bins by vertical axis
 *  \param ampMin Lower bound of amplitude histogram range (vertical axis)
 *  \param ampMax Upper bound of amplitude histogram range (vertical axis)
 *  \param baseName Histogram name suffix to name the corresponding TH2F
 *  */
SADC_DFT_EPDF::SADC_DFT_EPDF( calib::Dispatcher & ch
                            , const std::string & selection
                            , FTSubject subj
                            , const std::vector<int> & selectedFreqs
                            , int baseFreqLow, int baseFreqUp
                            , size_t nAmpBins
                            , double ampMin, double ampMax
                            , bool profile
                            , bool dBUnits
                            , const std::string & baseName
                            , const std::string & descr
                            , const std::string & path
                            ) : util::TDirAdapter(ch)
                                    , AbstractHitHandler(ch, selection)
                                    , _ampNBins(nAmpBins)
                                    , _ampRange{ampMin, ampMax}
                                    , _hstBaseName(baseName)
                                    , _hstDescr(descr)
                                    , _overridenPath(path)
                                    , _baseWindow{baseFreqLow, baseFreqUp}
                                    , _dBUnits(dBUnits)
                                    , _subj(subj)
                                    , _selectedFreqs(selectedFreqs)
                                    {
    if(dBUnits && subj != kAmplitude) {
        log().warn("\"dBUnits\" switch ignored as plotted quantity is not"
                " an amplitude.");
    }
}

bool
SADC_DFT_EPDF::process_hit( EventID
                          , DetID did
                          , event::SADCHit & currentHit) {
    if( !currentHit.rawData ) return true;  // no raw data associated with hit
    if( !currentHit.rawData->fft ) return true;
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;

        std::string hstName = _hstBaseName;
        auto substCtx = util::TDirAdapter::subst_dict_for(did, _hstBaseName);
        // push additional variables in string formatting context and
        // substitute the description string
        substCtx["baseFreqLow"] = util::format("%d", _baseWindow[0]);
        substCtx["baseFreqUp"]  = util::format("%d", _baseWindow[1]);
        std::string description = util::str_subst( _hstDescr, substCtx );
        auto p = util::TDirAdapter::dir_for( did, substCtx, _overridenPath);
        p.second->cd();

        Int_t nBins[_selectedFreqs.size()];
        Double_t mins[_selectedFreqs.size()], maxs[_selectedFreqs.size()];
        for(size_t i = 0; i < _selectedFreqs.size(); ++i) {
            nBins[i] = _ampNBins;
            mins[i] = _ampRange[0];
            maxs[i] = _ampRange[1];
        }

        THnSparseI * newHst = new THnSparseI( p.first.c_str()
                         , description.c_str()
                         , _selectedFreqs.size(), nBins, mins, maxs );
        TArrayI * nFreqs = new TArrayI(_selectedFreqs.size());
        for(size_t i = 0; i < _selectedFreqs.size(); ++i) {
            (*nFreqs)[i] = _selectedFreqs[i];
        }
        auto ir = _hists.emplace( did, std::pair<TArrayI*, THnSparseI *>{nFreqs, newHst});
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }

    // unpack (D)FFT coefficients
    double stdRe[17], stdIm[17], scaledAmps[17];
    na64sw_msadc_fft_compact2std( 16
                                , currentHit.rawData->fft->real, currentHit.rawData->fft->imag
                                , stdRe, stdIm );

    // if reference freq(s) are provided, we should plot relative amplitude
    const bool isRelative = (_baseWindow[1] - _baseWindow[0] > 0);
    if(_subj == kAmplitude) {
        if(!isRelative) {  // absolute values
            if(_dBUnits) {
                // dB units with absolute scale -- "dB Full Scale" units (dBFS)
                // NOTE on reference value here: (M)SADC max has been already used prior
                // to (D)FFT, so for floating point number [0..1] dBFS are calculated with
                // ref value =1.0 (full scale).
                na64sw_msadc_fft_dBFS(16, stdRe, stdIm, 1., 32, scaledAmps);
            } else {
                // absolute, not dB units -- just plot the abs values per freq
                na64sw_msadc_fft_abs_amps(16, stdRe, stdIm, scaledAmps, 1.);
            }
        } else {  // relative values
            if(_dBUnits) {
                // dB units relatively to some freq(s) range's mean
                // 1. calc the dBFS
                na64sw_msadc_fft_dBFS(16, stdRe, stdIm, 1., 32, scaledAmps);
                // 2. Get mean value in a subrange
                double nrm = 0.;
                for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                    nrm += scaledAmps[i];
                }
                nrm /= _baseWindow[1] - _baseWindow[0];
                // 3. Subtract
                for(int i = 0; i < 17; ++i) {
                    scaledAmps[i] -= nrm;
                }
            } else {
                // relative, abs. amp units -- plot abs amp ratio wrt certain freqs
                // 1. get abs. values
                na64sw_msadc_fft_abs_amps(16, stdRe, stdIm, scaledAmps, 1);
                // 2. Get mean value in a subrange
                double nrm = 0.;
                for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                    nrm += scaledAmps[i];
                }
                nrm /= _baseWindow[1] - _baseWindow[0];
                // 3. Divide
                for(int i = 0; i < 17; ++i) {
                    scaledAmps[i] /= nrm;
                }
            }
        }
    } else if(kRealPart == _subj) {
        double factor;
        if(isRelative) {
            factor = 0;
            for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                factor += stdRe[i];
            }
            factor /= (_baseWindow[1] - _baseWindow[0]);
            factor = 1./factor;
        } else {
            factor = 1.;
        }
        for(int i = 0; i < 17; ++i) {
            scaledAmps[i] = stdRe[i]*factor;
        }
    } else if(kImagPart == _subj) {
        double factor;
        if(isRelative) {
            factor = 0;
            for(int i = _baseWindow[0]; i < _baseWindow[1]; ++i) {
                factor += stdIm[i];
            }
            factor /= (_baseWindow[1] - _baseWindow[0]);
            factor = 1./factor;
        } else {
            factor = 1.;
        }
        for(int i = 0; i < 17; ++i) {
            scaledAmps[i] = stdIm[i]*factor;
        }
    }
    #ifndef NDEBUG
    else {assert(0);}  // else?
    #endif

    Double_t sample[_selectedFreqs.size()];
    for(size_t i = 0; i < _selectedFreqs.size(); ++i) {
        sample[i] = scaledAmps[_selectedFreqs[i]];
        if(std::isnan(scaledAmps[i]) || !std::isfinite(scaledAmps[i])) continue;
    }
    it->second.second->Fill(sample);
    return true;
}

void
SADC_DFT_EPDF::finalize() {
    char arrNameBf[128];
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        snprintf(arrNameBf, sizeof(arrNameBf), "%s-freqs", idHstPair.second.second->GetName());
        dir->WriteObject(idHstPair.second.first, arrNameBf);
        idHstPair.second.second->Write();
    }
}

SADC_DFT_EPDF::~SADC_DFT_EPDF() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second.first;
        delete idHstPair.second.second;
    }
}

}

REGISTER_HANDLER( SADC_DFT_EPDF, ch, cfg
                , "Builds EPDF (17-ary histogram) of SADC DFT amplitudes" ) {
    bool profile = cfg["profile"] && cfg["profile"].as<bool>();
    const std::string subjStr = cfg["subject"] ? cfg["subject"].as<std::string>() : "amp";
    na64dp::handlers::SADC_DFT_EPDF::FTSubject subj;
    if(subjStr == "amp") subj = na64dp::handlers::SADC_DFT_EPDF::kAmplitude;
    else if(subjStr == "re") subj = na64dp::handlers::SADC_DFT_EPDF::kRealPart;
    else if(subjStr == "im") subj = na64dp::handlers::SADC_DFT_EPDF::kImagPart;
    else {
        NA64DP_RUNTIME_ERROR("Bad subject: \"%s\", permitted are: amp, re, im"
                , subjStr.c_str());
    }
    int baseFreqs[2] = {
          cfg["baseFreqs"] ? cfg["baseFreqs"][0].as<int>() : 0
        , cfg["baseFreqs"] ? cfg["baseFreqs"][1].as<int>() : 0
        };
    if(baseFreqs[1] < baseFreqs[0] || (baseFreqs[0] == baseFreqs[1] && 0 != baseFreqs[0])) {
        NA64DP_RUNTIME_ERROR("Bad amp.baseline frequency range [%d:%d)"
                , baseFreqs[0], baseFreqs[1] );
    }
    const bool isSubrangeRelative = (baseFreqs[1] - baseFreqs[0] != 0)
            , dBUnits = cfg["dBUnits"]
                      ? cfg["dBUnits"].as<bool>()
                      : (subj == na64dp::handlers::SADC_DFT_EPDF::kAmplitude ? true : false);
    std::string descr;
    if(!cfg["histDescr"]) {
        descr = "(M)SADC Wf DFT ";
        if(subj == na64dp::handlers::SADC_DFT_EPDF::kRealPart) descr += "real part ";
        else if(subj == na64dp::handlers::SADC_DFT_EPDF::kImagPart) descr += "imag.part ";
        else if(subj == na64dp::handlers::SADC_DFT_EPDF::kAmplitude) descr += "Spectrum ";
        if(isSubrangeRelative) {
            descr += dBUnits
                  ? "for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp};"
                        " Freq., MHz; Amp., dB"
                  : "for {TBName} wrt freqs {baseFreqLow}-{baseFreqUp};"
                        " Freq., MHz; Amp. ratio"
                  ;
        } else {
            descr += dBUnits
                  ? "for {TBName}; Freq., MHz; Amplitude, dBFS"
                  : "for {TBName}; Freq., MHz; Amp., abs.units"
                  ;
                  ;
        }
    } else {
        descr = cfg["histDescr"].as<std::string>();
    }

    int ampNBins = profile ? 10 : cfg["ampNBins"].as<int>();

    auto & L = aux::get_logging_cat(cfg);
    std::vector<int> selectedFreqs;
    if(cfg["freqs"]) {
        selectedFreqs = cfg["freqs"].as<std::vector<int>>();
        // TODO one has to write the selected frequencies as TArray or
        // something (yet, handler's code should support a list now)
        NA64DP_RUNTIME_ERROR("TODO: freqs list support is not yet implemented"
                " for SADC_DFT_EPDF handler.");
    }
    if(cfg["freqsRange"]) {
        if(!selectedFreqs.empty()) {
            NA64DP_RUNTIME_ERROR("Both \"freqs\" and \"freqsRange\" specified.");
        }
        int freqFrom = cfg["freqsRange"][0].as<int>()
          , freqTo   = cfg["freqsRange"][1].as<int>();
        if(freqFrom >= freqTo) {
            NA64DP_RUNTIME_ERROR("\"freqsRange\" defines empty or negative range.");
        }
        for(int i = freqFrom; i < freqTo; ++i) {
            selectedFreqs.push_back(i);
        }
    } else {
        assert(selectedFreqs.empty());
        for(int i = 0; i < 5; ++i) {
            selectedFreqs.push_back(i);
        }
    }
    double estimatedMaxSizeGb = sizeof(UInt_t)*pow(ampNBins, selectedFreqs.size())/(1024*1024*1024);
    if(estimatedMaxSizeGb > 1.0) {
        L.warn("SADC_DFT_EPDF will be set up to handle %zu frequencies with %d"
              " bins. This may result in up to %.2eGb of memory usage."
              , selectedFreqs.size()
              , estimatedMaxSizeGb
              );
    }

    // TODO: use `L'!
    return new handlers::SADC_DFT_EPDF( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , subj
            , selectedFreqs
            , baseFreqs[0], baseFreqs[1]
            , ampNBins
            , profile ? -1 : cfg["ampRange"][0].as<float>()
            , profile ?  1 : cfg["ampRange"][1].as<float>()
            , profile
            , dBUnits
            , cfg["baseName"].as<std::string>()
            , descr
            , cfg["path"] ? cfg["path"].as<std::string>() : ""
            );
}

}
