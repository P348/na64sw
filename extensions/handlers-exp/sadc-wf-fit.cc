#include "na64calib/dispatcher.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64event/reset-values.hh"

#include "na64/waveform-reconstruction.hh"
#include "na64/moyal/function.h"

#include <log4cpp/Category.hh>
#include <na64/user-api/waveform-fitting.hh>
#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace handlers {

/**\brief Finds and fits waveforms in the SADC signals
 *
 * This class interfaces libna64utils-msadc library of versions starting from
 * v2.4. This handler relies on *default* waveform fitting configuration
 * providing access to opaque waveform fitting procedures -- a user API layer
 * similar to one exposed for p348reco.
 * */
class SADCFitWaveforms
        : public AbstractHitHandler<event::SADCHit>
        {
private:
    na64util::WaveformReconstruction * _subj;

    const size_t _nSamples;
    double * _wfCache;
    std::vector<na64util::iWaveformReco::PeakInfo> _resultCache;
public:
    SADCFitWaveforms( calib::Dispatcher & cdsp
                , const std::string & selection
                , const na64util::WaveformFittingParameters & ps
                , float timeClusteringDt
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                ) 
        : na64dp::AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _subj( na64util::WaveformReconstruction::instantiate(ps) )
        , _nSamples(ps.nSamples)
        {
        _wfCache = new double [_nSamples];
    }
    ~SADCFitWaveforms() {
        delete _wfCache;
        delete _subj;
    }

    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class SADCFitWaveforms

bool
SADCFitWaveforms::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if(!hit.rawData) return true;  // quetly ignore hits w/out raw data
    _resultCache.clear();

    std::copy(hit.rawData->wave, hit.rawData->wave + _nSamples, _wfCache);

    _subj->reset();
    if(!_subj->reconstruct( naming()[did].c_str()
            , _wfCache
            , _resultCache )) {
        this->log().debug("%s: no pulses found"
                , naming()[did].c_str() );
        return true;
    }

    // set peak estimates
    int nPeak = 0;
    for( const auto & pulseEntry : _resultCache) {
        if(log().getPriority() >= log4cpp::Priority::DEBUG) {
            this->log().debug("%s pulse %d: time %f +/- %f "
                    ", amp %f +/- %f"
                    , naming()[did].c_str(), nPeak
                    , pulseEntry.time, pulseEntry.timeErr
                    , pulseEntry.rawAmp,  pulseEntry.rawAmpErr
                    );
        }
        auto newPeakPtr = lmem().create<event::MSADCPeak>(lmem());
        util::reset(*newPeakPtr);

        newPeakPtr->amp = pulseEntry.rawAmp;
        newPeakPtr->ampErr = pulseEntry.rawAmpErr;

        newPeakPtr->sumFine = pulseEntry.sumFine;
        newPeakPtr->sumCoarse = pulseEntry.sumFineErr;

        newPeakPtr->time = pulseEntry.time;
        newPeakPtr->timeErr = pulseEntry.timeErr;

        if( pulseEntry.fit_function == na64util_moyal_model_f
         && pulseEntry.fitFunctionUserdata
          ) {
            auto moyalInfo = lmem().create<event::MoyalSADCWfFitParameters>(lmem());
            util::reset(*moyalInfo);
            newPeakPtr->moyalFit = moyalInfo;
            const double * p = reinterpret_cast<const double *>(pulseEntry.fitFunctionUserdata);
            moyalInfo->mu    = p[0];
            moyalInfo->sigma = p[1];
            moyalInfo->S     = p[2];
            moyalInfo->interval[0] = pulseEntry.validityRange[0];
            moyalInfo->interval[1] = pulseEntry.validityRange[1];
        }

        newPeakPtr->lastFitStatusCode = pulseEntry.statusCode;  // ?
        newPeakPtr->timeClusterLabel = pulseEntry.timeClusterLabel;

        hit.rawData->maxima.emplace(pulseEntry.sampleNo, newPeakPtr);
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp


REGISTER_HANDLER( SADCFitWaveforms, cdsp, cfg
                , "SADC waveform fitting procedure"
                ) {
    using namespace na64dp;

    auto ps = na64util::gDefaultMoyal01Parameters;

    ps.nSamples     = cfg["nSamples"].as<int>(32);
    ps.nsPerSample  = cfg["nsPerSample"].as<float>(12.5);
    ps.debugLog     = false;  // TODO
    ps.keepInitialValues = false;  // TODO
    //ps.normalizeValues  = false;  // TODO
    ps.absNoiseThreshold = cfg["absNoiseThreshold"].as<float>(ps.absNoiseThreshold);
    // ...
    ps.tripletLookupThreshold = std::nan("0");


    float timeClusteringDt = cfg["t-clus-dt"].as<float>(std::nan("0"));

    auto & L = aux::get_logging_cat(cfg);

    return new na64dp::handlers::SADCFitWaveforms( cdsp
            , aux::retrieve_det_selection(cfg)
            , ps
            , timeClusteringDt
            , L
            , aux::get_naming_class(cfg)
            );
}

