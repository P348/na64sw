#include "na64detID/TBName.hh"
#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"

#include "na64/user-api/time-clustering.hh"

#include "na64util/str-fmt.hh"

#include <cmath>
#include <log4cpp/Priority.hh>
#include <memory>
#include <na64/libna64utils-config.h>
#include <na64/time-based-pulse-iterator.hh>
#include <sstream>
#include <type_traits>

static_assert(std::is_same<na64util::DetID_t, na64dp::DetID_t>::value
        , "libna64util configured with other detector ID type" );
// ^^^ note: in principle, this case can also be handled with short in-memory
//       transformation dictionary... However for performance reasons it is
//       better to maintain static compatibility.

namespace na64dp {
namespace handlers {

/**\brief Finds time clusters based on time correlation peak information
 *
 * Among SADC reconstructed pulses (found by other handler and given in
 * `SADCHit::maxima`) looks for pulses with closest time
 * and marks found combinations with "time cluster label", taking into account
 * setup topology given as pairs of detectors with expected delay values.
 *
 * \code{.yaml}
 *     - _type: SADCClusterByTime
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * \todo Implement delay topology as calibration information. Currently, only
 *       ECAL is considered as hardcoded topology.
 * */
class SADCClusterByTime : public AbstractHitHandler<event::SADCHit> {
protected:
    /// Per-event index of maxima, gets re-set every event
    na64util::TimingTopologyFilter<DetID_t, na64util::TimePulseNo_t>::MaximaInfo _maxima;
    /// Topology filter, maintains info about permitted delays
    na64util::TimingTopologyFilter<DetID_t, na64util::TimePulseNo_t> _topology;
    /// Ptr to clustering method in use
    std::shared_ptr<na64util::iTimeClusteringMethod<DetID_t>> _clusteringMethod;

    /// Fills topology for ECAL
    ///
    /// \todo should be substituted with more general calib-info handler
    ///       for timing correlations later on
    void handle_update(const nameutils::DetectorNaming & nm) override;
public:
    /// ... features of the ctr, if any
    SADCClusterByTime( calib::Dispatcher & cdsp
                , const std::string & selection
                , std::shared_ptr<na64util::iTimeClusteringMethod<DetID_t>> clusteringMethod
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , _clusteringMethod(clusteringMethod)
        {}
    ProcRes process_event(event::Event & ev) override;
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class SADCClusterByTime

// deltas for hardcoded ECAL topology test stub
static const int _xxxECALDeltas[][3] = {
                {0,  1, 0},
    {-1, 0, 0},             {1, 0, 0},
                {0, -1, 0},
    {0, 0,  1}, {0,  0,-1},
};

void
SADCClusterByTime::handle_update(const nameutils::DetectorNaming & nm) {
    // fwd to parent
    AbstractHitHandler<event::SADCHit>::handle_update(nm);

    // --- XXX --- XXX --- XXX ----
    // TODO: this is temporary stub inserting topology info for ECAL
    char nameBuf1[64], nameBuf2[64];
    for(int yCell = 0; yCell < 6; ++yCell) {
        for(int xCell = 0; xCell < 6; ++xCell) {
            for(int zCell = 0; zCell < 2; ++zCell) {
                snprintf( nameBuf1, sizeof(nameBuf1)
                        , "ECAL0:%d-%d-%d"
                        , xCell, yCell, zCell
                        );
                DetID didA = nm[nameBuf1];
                for(size_t i = 0; i < sizeof(_xxxECALDeltas)/sizeof(_xxxECALDeltas[0]); ++i) {
                    int nXCell = xCell + _xxxECALDeltas[i][0]
                      , nYCell = yCell + _xxxECALDeltas[i][1]
                      , nZCell = zCell + _xxxECALDeltas[i][2]
                      ;
                    if(nXCell < 0 || nXCell >= 6) continue;
                    if(nYCell < 0 || nYCell >= 6) continue;
                    if(nZCell < 0 || nZCell >= 2) continue;
                    snprintf( nameBuf2, sizeof(nameBuf1)
                            , "ECAL0:%d-%d-%d"
                            , nXCell, nYCell, nZCell
                            );
                    // impose topology link
                    DetID didB = nm[nameBuf2];
                    //std::cout << "XXX "
                    //    << nameBuf1 << " (" << didA.id << ") -> "
                    //    << nameBuf2 << " (" << didB.id << "): "
                    //    << 0 << std::endl;  // XXX
                    _topology.add_delay_info(didA, didB, 0, 5);  // TODO: configurable?
                }
            }
        }
    }
    // --- XXX --- XXX --- XXX ----
}

SADCClusterByTime::ProcRes
SADCClusterByTime::process_event(event::Event & ev) {
    _maxima.clear();
    // fill index
    AbstractHitHandler<event::SADCHit>::process_event(ev);
    // clusterize
    if(_maxima.empty()) return kOk;  // no maxima in event
    assert(_clusteringMethod);
    _clusteringMethod->clusterize(_topology, _maxima);
    // iterate resulting peaks, set max IDs
    bool dbgOutput = log().getPriority() >= log4cpp::Priority::DEBUG;
    std::map<int, std::ostringstream> ossMap;
    for(auto maxItem : _maxima) {
        #if 1
        reinterpret_cast<event::MSADCPeak *>(maxItem.second.userdata)
            ->timeClusterLabel = maxItem.second.timeClusterLabel;
        //std::cout << "xxx time cluster label: "
        //    << maxItem.second.timeClusterLabel
        //    << std::endl;  // XXX
        #else
        DetID did(maxItem.first.first);
        auto hitIt = ev.sadcHits.find(did);
        assert(hitIt != ev.sadcHits.end());
        //unsigned int nPeak = maxItem.second.peakNum;
        // (dbg) assure peak properly ...
        assert(!hitIt->second->rawData->maxima.empty());
        assert(hitIt->second->rawData->maxima.end() != hitIt->second->rawData->maxima.find(maxItem.first.second));
        assert(fabs(maxItem.second.hitTime - hitIt->second->rawData->maxima[maxItem.first.second]->time) < 1e-8);
        hitIt->second->rawData->maxima[maxItem.first.second]->timeClusterLabel
                 = maxItem.second.timeClusterLabel;
        assert(maxItem.second.timeClusterLabel != -1);  // item was ignored by clustering algo (xxx?)
        if(dbgOutput && maxItem.second.timeClusterLabel) {
            auto & oss = ossMap.emplace(maxItem.second.timeClusterLabel, std::ostringstream()).first->second;
            oss << naming()[DetID(maxItem.first.first)]
                << "; (t=" << maxItem.second.hitTime << " +/-" << maxItem.second.hitTimeError << "), "
                ;
        }
        #endif
    }
    if(dbgOutput) {
        for(auto & p : ossMap) {
            log().debug("Time cluster #%d: %s", p.first, p.second.str().c_str());
        }
    }
    return kOk;
}

bool
SADCClusterByTime::process_hit(EventID eid, DetID did, event::SADCHit & hit) {
    if(!hit.rawData) return true;
    if(hit.rawData->maxima.empty()) return true;
    for(auto & p : hit.rawData->maxima) {
        const double time    = p.second->time
                   , timeErr = p.second->timeErr
                   ;
        if(!std::isfinite(time)) continue;
        _maxima.emplace( std::pair<DetID_t, unsigned int>(did, p.first)
                       , na64util::HitTimeInfo( time
                            , timeErr
                            , p.second.get()
                           )
                       );
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCClusterByTime, cdsp, cfg
                , "Finds and labels MSADC peaks by time correlation"
                ) {
    using namespace na64dp;
    auto & L = aux::get_logging_cat(cfg);
    const std::string clusteringMethod
            = cfg["method"] && cfg["method"]["_type"]
            ? cfg["method"]["_type"].as<std::string>()
            : "simple";
    std::shared_ptr<na64util::iTimeClusteringMethod<DetID_t>> clusteringMethodPtr;
    if(clusteringMethod == "simple") {
        float threshold = 12.5;  // ns
        size_t nMaxItems = 100;
        if(cfg["method"] && cfg["method"]["timeDeltaNs"]) {
            threshold = cfg["method"]["timeDeltaNs"].as<float>();
        }
        if(cfg["method"] && cfg["method"]["nMaxItems"]) {
            nMaxItems = cfg["method"]["nMaxItems"].as<size_t>();
        }
        L.debug("MSADC clustering method is \"%s\", time delta threshold is %e (ns)."
                , clusteringMethod.c_str(), threshold );
        clusteringMethodPtr = std::make_shared<na64util::SimpleTimeClustering>(
                threshold, nMaxItems );
    } else {
        NA64DP_RUNTIME_ERROR("Unknown MSADC time clustering method name \"%s\""
                , clusteringMethod.c_str() );
    }
    return new handlers::SADCClusterByTime( cdsp
            , aux::retrieve_det_selection(cfg)
            , clusteringMethodPtr
            , L
            , aux::get_naming_class(cfg)
            );
}

