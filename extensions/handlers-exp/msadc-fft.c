#include <math.h>
#include <assert.h>

void
na64sw_msadc_fft_compact2std( unsigned int nFreqs
                            , const double * compactRe, const double * compactIm
                            , double * stdRe, double * stdIm
                            ) {
    /* zero frequency term */
    stdRe[0] = compactRe[0];
    stdIm[0] = 0.;
    for(unsigned int i = 1; i < nFreqs; ++i) {
        stdRe[i] = compactRe[i];
        stdIm[i] = compactIm[i];
    }
    /* Nyquist frequency term, if any */
    if(0 == nFreqs%2) {
        stdRe[nFreqs] = compactIm[0];
        stdIm[nFreqs] = 0.;
    }
}

void
na64sw_msadc_fft_std2compact( unsigned int nFreqs
                            , const double * stdRe, const double * stdIm
                            , double * compactRe, double * compactIm
                            ) {
    /* set 1st element to contain zero frequcny term and put Nyquist to
     * imag. number of 1st frequency, if nFreqs is even */
    compactRe[0] = stdRe[0];
    if(0 == nFreqs%2)
        compactIm[0] = stdRe[nFreqs];
    for(unsigned int i = 1; i < nFreqs; ++i) {
        compactRe[i] = stdRe[i];
    }
}

void
na64sw_msadc_fft_abs_amps( unsigned int nFreqs
                         , const double * stdRe, const double * stdIm
                         , double * amps
                         , double fact
                         ) {
    for(unsigned int i = 0; i <= nFreqs; ++i) {
        amps[i] = fact*sqrt(stdRe[i]*stdRe[i] + stdIm[i]*stdIm[i]);
    }
}

void
na64sw_msadc_fft_20log10_amps( unsigned int nFreqs
                         , const double * stdRe, const double * stdIm
                         , double * amps
                         , double fact, double baselevel
                         ) {
    for(unsigned int i = 0; i <= nFreqs; ++i) {
        amps[i] = baselevel + 20*log10(fact*sqrt(stdRe[i]*stdRe[i] + stdIm[i]*stdIm[i]));
    }
}

void
na64sw_msadc_fft_dBFS( unsigned int nFreqs
                     , const double * stdRe, const double * stdIm
                     , const double ref, const double winSum
                     , double * dBFSAmps
                     ) {
    /* Scale the magnitude of FFT by window sum (nFreqs for rect window) and
     * factor of 2 since we'r using half of FFT spectrum */
    const double fact = 2/(ref*winSum);
    #if 0
    for(unsigned int i = 0; i <= nFreqs; ++i) {
        dBFSAmps[i] = 20*log10(sqrt(stdRe[i]*stdRe[i] + stdIm[i]*stdIm[i])*fact);
    }
    #else
    na64sw_msadc_fft_20log10_amps(nFreqs, stdRe, stdIm, dBFSAmps, fact, 0);
    #endif
}

#if 0
void
na64sw_msadc_fft_dB( unsigned int nFreqs //,unsigned int nRefFreqLeft, unsigned int nRefFreqRight
                   , const double * stdRe, const double * stdIm
                   , const double ref, const double winSum
                   , const double baseLevel
                   , double * amps
                   ) {
    na64sw_msadc_fft_dBFS(nFreqs, stdRe, stdIm, ref, winSum, amps);
    #if 0
    assert(nRefFreqRight > nRefFreqLeft);
    /* Calc in-place abs freqs */
    na64sw_msadc_fft_abs_amps(nFreqs, stdRe, stdIm, amps, 1.);
    /* Get average frequency */
    double nrm = 0.;
    for(unsigned int nRefFreq = nRefFreqLeft; nRefFreq < nRefFreqRight; ++nRefFreq) {
        nrm += amps[nRefFreq];
    }
    nrm /= (nRefFreqRight - nRefFreqLeft);
    /* Renormalize freqs and get log10 */
    for(unsigned int i = 0; i <= nFreqs; ++i) {
        amps[i] /= nrm;
    }
    #endif
}
#endif

void
na64sw_msadc_fft_freqs_MHz( unsigned int nFreqs
                          , double * freqValues
                          , double nsSampleTime
                          ) {
    for(unsigned int i = 0; i <= nFreqs; ++i) {
        // TODO: check
        freqValues[i] = 1e3*i/(nsSampleTime*nFreqs);
    }
}

/*
 * Windowing functions
 */

void
na64sw_msadc_fft_window_cosSum( const int n
                              , const double a0, const double a1, const double a2
                              , double * fValues
                              ) {
    assert(n > 1);
    double sum = 0;
    const int n_ = n-1;
    for(int i = 0; i < n; ++i) {
        fValues[i] = a0 - a1*cos(M_2_PI*i/n_) + a2*cos(2*M_2_PI*i/n_);
        sum += fValues[i];
    }
    for(int i = 0; i < n; ++i) {
        fValues[i] /= sum;
    }
}

