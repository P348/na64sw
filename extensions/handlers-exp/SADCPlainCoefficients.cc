#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/sadc.hh"
#include "na64detID/cellID.hh"
#include "na64util/str-fmt.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64util/na64/event-id.hh"

#include <cmath>
#include <sdc-base.hh>
#include <unordered_map>

namespace na64dp {
namespace calib {

/// Calibration datum type
struct PlainCoefficient {
    /// Name of this sub-detector entity (ECAL, HCAL, etc)
    std::string name;
    /// X,Y indeces of this sub-detector entity
    int xIdx, yIdx, zIdx;
    /// Proportion coefficient to be used for energy reconstruction
    float c;
};

typedef sdc::SrcInfo<PlainCoefficient> PlainCoefficientItem;

/// Coefficients collection type in use
typedef std::list<PlainCoefficientItem> PlainCoefficientItems;

}  // namespace ::na64dp::calib
}  // namespace na64dp

namespace sdc {
template<>
struct CalibDataTraits<na64dp::calib::PlainCoefficient> {
    /// Type's name alias
    static constexpr auto typeName = "SADC/plainCoeffs";
    /// A collection type for parsed plain coefficients objects
    template<typename T=na64dp::calib::PlainCoefficient>
        using Collection=std::list<T>;
    /// Action to append collection of parsed objects
    template<typename T=na64dp::calib::PlainCoefficient>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t lineNo
                              ) { col.push_back(e); }
    /// Parses the CSV line into `na64dp::calib::PlainCoefficient` data object
    ///
    /// Parsing can be affected by the parser state to handle changes within
    /// a file. Namely, metadata "columns=name1,name2..." may denote number of
    /// token with certain semantics. By default, "name,x,y,peakpos" is assumed
    ///
    /// The `lineNo` argument can be used for diagnostic purposes (e.g.
    /// to report a format error).
    static na64dp::calib::PlainCoefficient
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      ) {
        #ifndef NDEBUG
        const size_t lineNo_ = m.get<size_t>("@lineNo");
        assert(lineNo_ == lineNo);
        #endif
        // create object
        na64dp::calib::PlainCoefficient obj;
        // Create and validate columns order
        auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
                //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
                .interpret(aux::tokenize(line), loadLogPtr);
        // ^^^ note: every CSV block must be prefixed by columns order,
        //     according to Anton's specification
        // get columns
        obj.name = csv("name");
        obj.xIdx = csv("xIdx");
        obj.yIdx = csv("yIdx");
        obj.zIdx = csv("zIdx");
        obj.c = csv("coeff");
        return obj;
    }
};  // struct CalibDataTraits<PlainCoefficient>
}  // namespace sdc

namespace na64dp {
namespace handlers {

/**\brief ...
 *
 * \code{.yaml}
 *     - _type: MyHtiHandler
 *       # ...; str, opt
 *       foo: bar
 *       # ...; int, required
 *       bar: 123
 * \endcode
 *
 * */
class SADCApplyPlainCoeffs
            : public AbstractHitHandler<event::SADCHit>
            , public calib::Handle<calib::PlainCoefficientItems>
            {
protected:
    std::unordered_map<DetID, float> _coeffs;
    // ... put here properties of your handler
public:
    /// Sets calibration coefficients
    void handle_update(const calib::PlainCoefficientItems & coeffs) override;

    /// ... features of the ctr, if any
    SADCApplyPlainCoeffs( calib::Dispatcher & cdsp
                , const std::string & selection
                // ... your additional arguments here
                , log4cpp::Category & logCat
                , const std::string & namingSubclass="default"
                )
        : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
        , calib::Handle<calib::PlainCoefficientItems>("default", cdsp)
        {}
    /// short summary on how the hit gets processed
    bool process_hit(EventID, DetID, event::SADCHit & hit) override;
};  // class SADCApplyPlainCoeffs

void
SADCApplyPlainCoeffs::handle_update(const calib::PlainCoefficientItems & coeffs) {
    const auto & nm = AbstractHitHandler<event::SADCHit>::naming();
    for(const auto & c : coeffs) {
        // use data provided by calibration item to build encoded detector ID
        DetID did = nm[c.data.name];
        did.number(0);
        CellID cellID;
        cellID.set_x(c.data.xIdx);
        cellID.set_y(c.data.yIdx);
        cellID.set_z(c.data.zIdx);
        did.payload(cellID.cellID);
        auto ir = _coeffs.emplace(did, c.data.c);
        if(ir.second) {
            log().debug("Using %.3e calib. coeff. for %s item, as read from %s:%zu"
                , c.data.c, nm[did].c_str(), c.srcDocID.c_str(), c.lineNo );
        } else {
            log().debug("Overriding calib. coeff. for %s item with %.e, as read from %s:%zu"
                , nm[did].c_str(), c.data.c, c.srcDocID.c_str(), c.lineNo );
            ir.second = c.data.c;
        }
    }
}

bool
SADCApplyPlainCoeffs::process_hit(EventID, DetID did, event::SADCHit & hit) {
    if(!hit.rawData) return true;  // omit as no raw data provided for item
    auto cIt = _coeffs.find(did);
    if(_coeffs.end() == cIt) {
        NA64DP_RUNTIME_ERROR("No plain calib. coeff. set for \"%s\""
                , naming()[did].c_str() );  // TODO: configurable warn/error/ignore
    }
    if(std::isnan(hit.rawData->maxAmp))  // TODO: configurable
        return true;  // TODO: configurable warn/error/ignore
    hit.eDep = hit.rawData->maxAmp * cIt->second;
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCApplyPlainCoeffs, cdsp, cfg
                , "Applies plain calibration coefficients to SADC hit based on maxAmp"
                ) {
    // assure calibration data type is up
    //na64dp::calib::CIDataAliases::self()
    //        .add_alias_of<na64dp::calib::PlainCoefficients>("SADC/plainCoeffs", "default");
    cdsp.get_loader<na64dp::calib::SDCWrapper>("sdc")
            .enable_sdc_type_conversion<na64dp::EventID, na64dp::calib::PlainCoefficientItem>();
    na64dp::calib::CIDataAliases::self()
            .add_dependency(sdc::CalibDataTraits<na64dp::calib::PlainCoefficientItem>::typeName, "naming");

    //na64dp::calib::CIDataAliases::self()
    //        .add_alias_of<Placements>("placements", "default");
    // get SDC loader from manager, downcast it to proper type (SDCWrapper)
    // and impose type conversion using previously defined traits
    //mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
    //    .enable_sdc_type_conversion<p348reco::RunNo_t, Placement>();
    // Require "naming" to be loaded before "placements"
    //na64dp::calib::CIDataAliases::self().add_dependency("placements", "naming");

    using namespace na64dp;
    return new handlers::SADCApplyPlainCoeffs( cdsp
            , aux::retrieve_det_selection(cfg)
            // ... your additional arguments retrieved from `cfg'
            , aux::get_logging_cat(cfg)
            , aux::get_naming_class(cfg)
            );
}

