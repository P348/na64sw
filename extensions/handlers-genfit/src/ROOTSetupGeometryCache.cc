#include "absFitter.hh"
#include "stdMagFields.hh"

#include <AbsFitter.h>

#include "ROOTSetupGeometryCache.hh"

#include "na64detID/TBName.hh"

#include <TGeoManager.h>
#include <TGeoMaterialInterface.h>
#include <TGeoBBox.h>
#include <FieldManager.h>
#include <MaterialEffects.h>

#include <log4cpp/Priority.hh>

namespace na64dp {
namespace util {

ROOTSetupGeometryCache::ROOTSetupGeometryCache( calib::Manager & mgr
    , log4cpp::Category & loggingCategory
    , const std::string & geomSrcURI
    , bool disableMatEffects
    ) : calib::SetupGeometryCache(mgr, loggingCategory)
      , _calibMgr(mgr)
      , _setupValid(false)
      , _plBGeomPtr(nullptr)
      , _geoMgr(nullptr)
      , _geomSrcURI(geomSrcURI)
      , _disableMaterialEffects(disableMatEffects)
      , _magField(nullptr)
      , _matIface(nullptr)
      {
    mgr.subscribe<calib::Placements>(*this, "default");
    _log << log4cpp::Priority::DEBUG
         << "ROOT/GenFit2 setup geometry cache instantiated.";
}

void
ROOTSetupGeometryCache::handle_single_placement_update(const calib::Placement &) {
    // must not invoke this (handle_update(Placements) overriden)
    assert(false);
}

void
ROOTSetupGeometryCache::handle_update( const calib::Placements & placements ) {
    _setupValid = false;
    // Fill set of available zones and detID-to-zone map
    for( const auto & placement : placements ) {
        if((((int) calib::Placement::kMagnetsGeometry)
          & ((int) placement.data.suppInfoType))) {
            // add magnets description info
            _magnetsPlacements.push_back(placement);
        } else
        if((((int) calib::Placement::kDetectorsGeometry)
          & ((int) placement.data.suppInfoType))) {
            // add detector placements info
            auto & nm = SetupGeometryCache::naming();
            DetID did(nm[placement.data.name]);
            _zoneByDets[did.id] = placement.data.zone;
            _zones.insert(placement.data.zone);
        } else {
            _log << log4cpp::Priority::WARN
                << "Setup cache ignores \""
                << placement.data.name
                << "\" placement item.";
        }
    }
    // if geometry spec is not forced, push placement entity into `_plBGeomPtr`
    // for further creation
    if( _geomSrcURI.empty() ) {
        if(!_plBGeomPtr) {
            _geoMgr = new TGeoManager( "Geometry", "NA64 geometry" );
            _plBGeomPtr = new PlacementsBasedGeometry(_geoMgr, _log);
            _log << log4cpp::Priority::INFO
                 << "TGeoManager instantiated, placements-based geometry"
                    " instantiated.";
        }
        _plBGeomPtr->add_placements( placements );
    }
}

void
ROOTSetupGeometryCache::_recache_geometry() {
    if( !_geomSrcURI.empty() ) {  // Load geometry file, if specified
        assert(!_geoMgr);  // manager was not deleted, wrong recache invokation?
        _geoMgr = new TGeoManager( "Geometry", "NA64 geometry" );
        TGeoManager::Import( _geomSrcURI.c_str() );
        _log.info( "Geometry imported from \"%s\".", _geomSrcURI.c_str() );
        return;
    }
    // otherwise, construct a geometry based on placements
    if( ! _plBGeomPtr ) {
        NA64DP_RUNTIME_ERROR( "Can not construct geometry since placements"
                " cache is empty (missing placements info?)." );
    }
    // Create a "topmost" encompassing world volume
    TGeoVolume * worldVol;
    {
        // Get estimation of world size
        auto wrldCorners = _plBGeomPtr->world_boundaries();
        // To create the world, we take the largest coordinate per each axis
        // and double it
        Double_t worldDims[3] = { std::fabs(wrldCorners.first[0])
                                , std::fabs(wrldCorners.first[1])
                                , std::fabs(wrldCorners.first[2])
                                };
        for( int d = 0; d < 3; ++d ) {
            if( worldDims[d] < std::fabs(wrldCorners.second[d]) )
                worldDims[d] = std::fabs(wrldCorners.second[d]);
            _worldDims[d] = worldDims[d] *= 2;
        }
        _log.info( "Generated world will roughly span from"
                    " {%.1f, %.1f, %1.f} to {%.1f, %.1f, %.1f} => creating"
                    " a world with dimensions %.1fx%.1fx%.1f."
                    , wrldCorners.first[0], wrldCorners.first[1], wrldCorners.first[2]
                    , wrldCorners.second[0], wrldCorners.second[1], wrldCorners.second[2]
                    , worldDims[0], worldDims[1], worldDims[2]
                    );
        worldVol = _geoMgr->MakeBox( "World"
                , _plBGeomPtr->get_medium("air")
                , worldDims[0], worldDims[1], worldDims[2]
                );
    }
    _geoMgr->SetTopVolume(worldVol);
    // Populate with geometry wrt placements previously specified
    _plBGeomPtr->instantiate_geometry(worldVol);
    _geoMgr->CloseGeometry();
    _log.info( "Tracking geometry generated (simplified)." );
}

void
ROOTSetupGeometryCache::_recache_mag_fields() {
    if(_magnetsPlacements.empty()) {
        _log << log4cpp::Priority::NOTICE
             << "No magnetic field description provided.";
        return;  // do not create magnetic field if nothing is provided
    }
    YAML::Node motherMFieldCfg = YAML::Node(YAML::NodeType::Map);
    motherMFieldCfg["label"] = "magnets";
    motherMFieldCfg["fields"] = YAML::Node(YAML::NodeType::Sequence);
    size_t nItem = 0;
    // Build magnetic field YAML config object
    for(const auto & placementItem : _magnetsPlacements) {
        YAML::Node cNode(YAML::NodeType::Map);
        cNode["_sourcInfoFile"] = placementItem.srcDocID;
        cNode["_sourcInfoLine"] = placementItem.lineNo;
        cNode["name"] = placementItem.data.name;
        cNode["position"] = YAML::Node(YAML::NodeType::Sequence);
        cNode["rotation"] = YAML::Node(YAML::NodeType::Sequence);
        for(int i = 0; i < 3; ++i) {
            cNode["position"][i] = placementItem.data.center[i];
            cNode["rotation"][i] = placementItem.data.rot[i];
        }
        if(placementItem.data.suppInfoType == calib::Placement::kFieldMap) {
            const auto & si = placementItem.data.suppInfo.fieldMap;
            cNode["_type"] = si.fieldClass;
            if('\0' != *si.mapFilePath)
                cNode["mapURI"] = si.mapFilePath;
            if('\0' != *si.lengthUnits)
                cNode["lengthUnits"] = si.lengthUnits;
            if('\0' != *si.fieldUnits)
                cNode["fieldUnits"] = si.fieldUnits;
            if('\0' != *si.techLabel)
                cNode["techLabel"] = si.techLabel;
        } else {
            _log << log4cpp::Priority::WARN
                << "Unsupported field type for \"" << placementItem.data.name
                << "\" (at " << placementItem.srcDocID << ":" << placementItem.lineNo
                << ").";
            continue;
        }
        motherMFieldCfg["fields"][nItem++] = cNode;
    }
    _magField = VCtr::self().make<iAbstractGenFitMField>(
            "Group"
          , _calibMgr
          , motherMFieldCfg
          );
    genfit::FieldManager::getInstance()->init(_magField);
    _log << log4cpp::Priority::INFO
         << "GenFit2 field manager initialized with m.field description.";
    #if 0
    double pc[6];
    if( _magFieldsCfg["dump"] ) {
        for( auto planeEntryNode : _magFieldsCfg["dump"] ) {
            std::ofstream ofs(planeEntryNode["file"].as<std::string>());
            // TODO: parameterise
            for( int xIdx = 0; xIdx < 100; ++xIdx ) {
                for( int zIdx = 0; zIdx < 1000; ++zIdx ) {
                    TVector3 v( pc[0] = _worldDims[0]*(.5 - xIdx/100.)
                              , pc[1] = -83
                              , pc[2] = _worldDims[2]*(.5 - zIdx/1000.)
                              ), B = _magField->get(v);
                    //ofs << B.Mag() << " ";
                    //ofs << v(0) << "\t" << v(2) << "\t" << B.Mag() << std::endl;
                    genfit::FieldManager::getInstance()
                        ->getFieldVal( pc[0], pc[1], pc[2]
                                     , pc[3], pc[4], pc[5]);
                    ofs << pc[0] << "\t" << pc[2] << "\t"
                        << TVector3(pc[3], pc[4], pc[5]).Mag() << std::endl;
                }
                ofs << std::endl;
            }
        }
        #if 0
        auto frames = _magField->labeled_field_frames();
        for( auto & frameItem : frames ) {
            // TODO: we now use only the first transformation; after
            // chaining of affine transform will be properly implemented,
            // this shall change
            util::Transformation t = frameItem.second[0];
        }
        #endif
    }  // dump
    #endif
}

void
ROOTSetupGeometryCache::recache_setup() {
    if(_setupValid) return;  // setup is valid
    _recache_geometry();
    _recache_mag_fields();

    //if( _matIface ) {
    //    delete _matIface;
    //}
    if(!_matIface) {
        _matIface = new genfit::TGeoMaterialInterface();
        genfit::MaterialEffects::getInstance()->init(_matIface);
    }
    if( _disableMaterialEffects ) {
        genfit::MaterialEffects::getInstance()->setNoEffects();
    }
    _setupValid = true;
}

ROOTSetupGeometry::ROOTSetupGeometry() : _ptr(nullptr) {}

void ROOTSetupGeometry::init( calib::Manager & cmgr
                            , iEvProcInfo * //epi
                            ) {
    auto & L = log4cpp::Category::getInstance("extensions.ROOTSetupGeometry");
    //YAML::Node magFieldsCfg;
    std::string geomSrcURI;
    bool useMatEff = false;
    // Override geometry source URI if has external parameter
    auto parIt = _externalParameters.find("geometry");
    if( _externalParameters.end() != parIt ) {
        L << log4cpp::Priority::NOTICE
          << "Using static geometry from file \""
          << parIt->second << "\" (instead of placements-based auto"
             " generated one).";
        geomSrcURI = parIt->second;
    }
    if( _externalParameters.end() != (parIt = _externalParameters.find("considerMaterialEffects")) ) {
        // TODO: use interpreter here that shall accepts yes,[Tt]rue,1, etc.
        if( parIt->second == "yes" ) {
            useMatEff = true;
        } else if( parIt->second == "no" ) {
            useMatEff = false;
        } else {
            NA64DP_RUNTIME_ERROR("Couldn't interpret \"%s\" as logic value"
                    " for parameter"
                    " `ROOTSetupGeometry.considerMaterialEffects",
                    parIt->second.c_str() );
        }
    }
    L << log4cpp::Priority::DEBUG
      << "ROOT/GenFit2 setup geometry instantiated.";
    // Query user flag for enabling/disabling material effects
    _ptr = new ROOTSetupGeometryCache( cmgr
            , log4cpp::Category::getInstance(NA64SW_GENFIT_LOGCAT)
            , geomSrcURI
            , !useMatEff
            );
}

void
ROOTSetupGeometry::set_parameter( const std::string & nm
                                , const std::string & val) {
    _externalParameters[nm] = val;
}

void
ROOTSetupGeometry::finalize() {
    if(cache()) {
        // TODO: field manager foresees "destructor" for this singleton, but it is
        // a proper method of the instance, so it is not clear what is the
        // intenional use of this "destructor"
        //genfit::FieldManager::getInstance()->destruct();
    }
}


REGISTER_EXTENSION( ROOTSetupGeometry );

}  // namespace ::na64dp::util
}  // namespace na64dp

