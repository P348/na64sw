#include "na64dp/abstractHitHandler.hh"

#include <Eigen/Dense>
#include <Eigen/SVD>
#include <Eigen/LU>

using namespace na64dp;

/**\brief A simple projection-based track fitter
 *
 * For every composed track with scores given in certain projection(s),
 * performs an LMS fitting with given track propagation model.
 *
 * * `applyTo` -- track selection expression (zones), won't affect detector of
 *          choice. If not given, all the tracks will be considered;
 * * `useDets` -- scores that do not pass this createria will not be
 *          considered at all;
 * * `relyOn` -- score selector to choose for LMS fit meaning that scores that
 *          pass selection will participate in LMS fit.
 *
 * Selection scheme above is done to be able to selectively include/exclude
 * detectors in track fitting procedure and indpendently compute unbiased
 * residuals for 'em. Usage examples:
 *  1. Use all MMs to fit track and obtain unbiased residuals for them.
 *  2. Use all MMs and one of the ST plane to obtain residuals for reciprocal
 *     ST plane.
 *  3. Use all MMs and one all STs plane to obtain biased residuals for all
 *     detectors under consideration.
 *  ...etc.
 *
 * \note One shall note the difference between `applyTo`, `relyOn` and
 *       `useDets` selectors here.
 *
 * \todo support for unbiased residuals
 * \todo parameterize LMS method
 * \todo support non-linear track models (propagators): arc, helix, etc
 * \todo parameterize track model (propagator)
 * */
class LMSProj_FitTracks : public AbstractHitHandler<event::Track>
                        , public calib::Handle<nameutils::DetectorNaming>
                        {
public:
    /// Type of strexpr + compiled det ID selector
    typedef std::pair<std::string, DetSelect *> Selector;
    
    LMSProj_FitTracks( calib::Dispatcher & cdsp
                     , const std::string & selection
                     , const std::string & useDets
                     , const std::string & relyOn
                     , log4cpp::Category & logCat
                     , APVPlaneID::Projection proj2consider
                     );
    ~LMSProj_FitTracks();
    /// This method gets called for every event. We use it to (re-)allocate
    /// reentrant structures
    ProcRes process_event(event::Event &) override;

    /// This method gets invoked for every "track" within the event (so, in
    /// overriden context, one should rather call it "process_track()")
    bool process_hit( EventID, TrackID, event::Track & ) override;
protected:
    void handle_update(const nameutils::DetectorNaming & ) override;
protected:
    /// Projection code to be considered
    const APVPlaneID::Projection _proj2consider;
    /// Number of the component to be taken from score's gR vector to be fitted
    /// (e.g. 1 for Y)
    int _nVecComponent;
    /// Selector to pick up scores to be considered
    Selector _toConsider;
    /// Selector to pick up scores to participate in LMS fit
    Selector _relyOn;
};

// Implem
////////

LMSProj_FitTracks::LMSProj_FitTracks( calib::Dispatcher & cdsp
                                    , const std::string & selection
                                    , const std::string & useDets
                                    , const std::string & relyOn
                                    , log4cpp::Category & logCat
                                    , APVPlaneID::Projection proj2consider
                                    ) : AbstractHitHandler<event::Track>(cdsp, selection, logCat)
                                      , calib::Handle<nameutils::DetectorNaming>("default", cdsp)
                                      , _proj2consider(proj2consider)
                                      , _toConsider{useDets, nullptr}
                                      , _relyOn{relyOn, nullptr}
                                      {
    if(WireID::kX == proj2consider) _nVecComponent = 0;
    else if(WireID::kY == proj2consider) _nVecComponent = 1;
    else {
        NA64DP_RUNTIME_ERROR("Unsupported component %c.", WireID::proj_label(proj2consider));
    }
}

LMSProj_FitTracks::~LMSProj_FitTracks() {
    if(_relyOn.second) delete _relyOn.second;
    if(_toConsider.second) delete _toConsider.second;
}

void
LMSProj_FitTracks::handle_update(const nameutils::DetectorNaming & nm ) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    // init selectors, if given
    if(!_toConsider.first.empty()) {
        _toConsider.second = new DetSelect( _toConsider.first.c_str()
                , util::gDetIDGetters, nm );
    }
    if(!_relyOn.first.empty()) {
        _relyOn.second = new DetSelect( _relyOn.first.c_str()
                , util::gDetIDGetters, nm );
    }
}


LMSProj_FitTracks::ProcRes
LMSProj_FitTracks::process_event(event::Event & ev) {
    // do not forget to call the parent -- it will iterate over event's tracks
    // matching selection criteria
    auto rc = AbstractHitHandler<event::Track>::process_event(ev);
    // ...
    return rc;
}

bool
LMSProj_FitTracks::process_hit( EventID eid
                              , TrackID trackID
                              , event::Track & track
                              ) {
    std::map<double, double> toFit;  // to be sorted
    // We compose the matrix to be (pseudo-)inverted from a single projection
    // here
    for(auto scoreRef : track) {
        // scoreRef => (first:DetID, second:TrackScore)
        // Drop scores of other projection
        if( scoreRef.first.payload_as<WireID>()->proj() != _proj2consider )
            continue;
        if( _relyOn.second && !_relyOn.second->matches(scoreRef.first) )
            continue;
        auto p = std::make_pair( scoreRef.second->score->gR[2]
                               , scoreRef.second->score->gR[_nVecComponent]
                               );
        assert(!std::isnan(p.first));
        assert(!std::isnan(p.second));
        toFit.insert(p);
        //std::cout << " xxx {" << p.first << ", " << p.second << "}" << std::endl;
    }
    if(toFit.size() < 3) return true;  // do not fit well-determined case

    // Create Eigen assets for SVP
    // with linear propagator we're going to resolve an overdetermined
    // `k*z + a = f' where `z' and `f' we know.
    double k, a;
    {
        Eigen::MatrixXf m(toFit.size(), 2);
        Eigen::VectorXf v(toFit.size());
        size_t n = 0;
        for(const auto & p : toFit) {
            m(n, 0) = p.first;
            m(n, 1) = 1.;
            v[n++]  = p.second;
        }
        // TODO: move to parameter?
        auto solver = 
            //m.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(v)
            //m.CompleteOrthogonalDecomposition()
            m.colPivHouseholderQr()
            //m.fullPivHouseholderQr()
            ;
        Eigen::MatrixXf res =
            solver.solve(v);
            //= (m.transpose() * m).ldlt().solve(m.transpose() * v)  // known to be not very precise
            ;
        k = res(0, 0);
        a = res(1, 0);
    }

    // Calc biased residuals for detectors participated in tracking and ubiased
    // residuals for detectors that of use, but were not used in fitting
    track.fitInfo = lmem().create<event::TrackFitInfo>(lmem());
    // well, it is not actually a chi2 (one has to divide it by resolutions),
    // but let's use this field to propagate the squared error sum
    track.fitInfo->chi2 = 0.;
    track.fitInfo->ndf = toFit.size();
    for(auto scoreRef : track) {
        // scoreRef => (first:DetID, second:TrackScore)
        // Drop scores of other projection
        if( scoreRef.first.payload_as<WireID>()->proj() != _proj2consider )
            continue;
        if( (!_relyOn.second) || _relyOn.second->matches(scoreRef.first) ) {
            // "fitted - measured"
            const double err = scoreRef.second->lRBErr[_nVecComponent]
                = (k*scoreRef.second->score->gR[2] + a) - scoreRef.second->score->gR[_nVecComponent]
                ;
            assert(!std::isnan(err));
            track.fitInfo->chi2 += err*err;
        } else if( (!_toConsider.second) || _toConsider.second->matches(scoreRef.first) ) {
            // "fitted - measured"
            scoreRef.second->lRUErr[_nVecComponent]
                = (k*scoreRef.second->score->gR[2] + a) - scoreRef.second->score->gR[_nVecComponent]
                ;
        }
    }
    return true;  // means "continue iterate over hits"
}

REGISTER_HANDLER( LMSProj_FitTracks  // handler name, as it has to be referren in cfg
                , cmgr  // calibration manager instance variable name
                , cfg  // this handler's config node
                , "Projection-based LMS track fit."
                ) {
    auto & logCat = log4cpp::Category::getInstance( cfg["_log"]
                                                  ? cfg["_log"].as<std::string>()
                                                  : std::string("handlers.fitTrack")
                                                  );
    if(!cfg["projection"]) {
        NA64DP_RUNTIME_ERROR("No node \"projection\"");
    }
    auto proj2considerStr = cfg["projection"].as<std::string>();
    if(proj2considerStr.size() != 1) {
        NA64DP_RUNTIME_ERROR("Bad projection string: \"%s\""
                , proj2considerStr.c_str() );
    }
    return new LMSProj_FitTracks( cmgr
                                , na64dp::aux::retrieve_det_selection(cfg)
                                , cfg["useDets"] ? cfg["useDets"].as<std::string>() : ""
                                , cfg["relyOn"] ? cfg["relyOn"].as<std::string>() : ""
                                , logCat
                                , APVPlaneID::proj_code(*proj2considerStr.c_str())
                                );
};


