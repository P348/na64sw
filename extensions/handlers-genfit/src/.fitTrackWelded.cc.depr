#include "FitTrack.hh"

// genfit
#include <AbsFitter.h>
#include <RKTrackRep.h>
#include <Track.h>
#include <AbsBField.h>
#include <FitStatus.h>
#include <KalmanFitStatus.h>

#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"
#include "na64detID/TBName.hh"
#include "na64event/data/event.hh"

#include "AbsFitter.hh"

namespace na64dp {
namespace handlers {

/**\brief Two-staged track fitting: segments + welded
 *
 * Usage:
 *
 * \code{.yaml}
 *     - _type: GenFit_FitTrackWelded
 *       # Makes handler to create a new track instance of the welded track
 *       # in the event upon successful fit. Has no effect if `doWelding' is
 *       # false; opt, bool
 *       copyTracks: true
 *       # Makes handler to create a new track per fitted linear
 *       # segment (a tracklet) before any welding. Helps to debug pre-fitting
 *       # procedure on zones of linear tracks; opt, bool
 *       keepLinearTracks: false
 *       # A particle name for PDG entry for initial tracking hypothesis. Can
 *       # be any string supported by ROOT's TDatabasePDG; str, optional
 *       RKInitParticle: "e-"
 *       # An initial momentum hypothesis (seed), GeV; 3-list of floats, required
 *       momSeed: [0, 0, 100]
 *       # If set to non-empty string, the denoted file will be used instead
 *       # of the geometry constructed automatically, based on current run
 *       # info; str, optional
 *       geometryFile: ""
 *       # Disables material effects consideration; opt, bool
 *       disableMatEffects: false
 *       # Position seed used to init the tracking, strategy used to update
 *       # seed with a trackpoint. Possible variants are: "lowest-z" (single,
 *       # for the time); opt, string
 *       positionSeed: lowest-z
 *       # Set it to the vector for constant offset of the position seed.
 *       # It is shown, that for better convergence a position seed has to be
 *       # in some distance upstream from the Kalman filter start; opt, 3-list
 *       positionSeedOffset: [0, 0, 0]
 *       # Covariance matrix seed -- initial values at diagonal; opt, float
 *       covSeed: 1.0
 *       # Magnetic field type and configuration; required, sub-node
 *       magField:
 *          _type: ...
 *       # Fitter type and its configuration; required, sub-node
 *       fitter:
 *          _type: ...
 *       # When disabled, only linear fitting will be performed; bool, opt
 *       doWelding: true
 * \endcode
 *
 * This handler mimics one initially written by Alexey Shevelev. The handler
 * performs two staged fitting: first with linear segments, then these segments
 * are welded into a single track.
 *
 * The welding is done via GenFit's `Track::mergeTrack()` that keeps
 * `FitterInfo` from track points already fitted (merged tracks has to be of
 * same type and PDG).
 *
 * \ingroup handlers tracking-handlers
 * */
class GenFit_FitTrackWelded : public AbstractGenFit_FitTrack {
protected:
    /// Pointer to a fitter instance (used for both, the segments and welded
    /// track)
    genfit::AbsFitter * _fitter;
    /// A callback pointer to update the position seed (used in some fitting
    /// scenarios)
    std::function<void(TVector3 & seed, DetID, const genfit::TrackPoint *)>
            _posSeedUpdate;
    /// Modifies position seed after update
    TVector3 _posSeedOffset;
    /// Momentum seed for fitting
    TVector3 _momSeed;
    /// Covariance matrix seed
    TMatrixDSym _covSeed;

    /// Defines whether the welding is enabled; if not, only linear fitting
    /// will be performed in zones.
    bool _doWelding;
    /// Enables copying of linear track segments into NA64SW event's track set
    bool _keepLinearTracks;
    /// Enables copying of resulting welded track into NA64SW event's tracks set
    bool _copyTrack;

    /// Det ID to order parameter to sort track points
    std::unordered_map<DetID, float> _ordering;
protected:
    ///\brief (Re-)caches ordering
    ///
    /// This method is currently used only to cache Z ordering for planes, but
    /// may be extended to configurable behaviour in the future.
    void handle_update( const calib::Placements & placements ) override {
        AbstractGenFit_FitTrack::handle_update(placements);
        // Update Z position for plane
        auto & nm = calib::Handle<nameutils::DetectorNaming>::get();
        for(const auto & placement : placements) {
            DetID did = nm[placement.data.name];
            _ordering[did] = placement.data.center[2];
        }
    }
public:
    GenFit_FitTrackWelded( calib::Manager & mgr
                         , const YAML::Node & magFieldsCfg
                         , int pdg
                         , const TVector3 & momSeed
                         , const std::string & geomSrcURI
                         , bool disableMatEffects
                         , const YAML::Node & fitterCfg
                         , bool copyLinTracks=false
                         , bool copyResultTracks=true
                         , std::function<void(TVector3 & seed, DetID, const genfit::TrackPoint *)> posSeedUpdF=nullptr
                         , const TVector3 & seedOffset=TVector3(0, 0, 0)
                         , const TMatrixDSym covSeed=TMatrixDSym(6)
                         , bool doWelding=true
                         ) : AbstractGenFit_FitTrack( mgr
                                                    , pdg
                                                    , magFieldsCfg
                                                    , geomSrcURI
                                                    , disableMatEffects
                                                    )
                            , _posSeedUpdate(posSeedUpdF)
                            , _posSeedOffset(seedOffset)
                            , _momSeed(momSeed)
                            , _covSeed(covSeed)
                            , _doWelding(doWelding)
                            , _keepLinearTracks(copyLinTracks)
                            , _copyTrack(copyResultTracks)
                            {
        _fitter = VCtr::self().make<genfit::AbsFitter>(
                fitterCfg["_type"].as<std::string>(), mgr, fitterCfg );
        log().debug("Welded track fitter instantiated.");
    }

    ~GenFit_FitTrackWelded() { delete _fitter; }

    ProcRes process_event(event::Event & e) override;
};

AbstractHandler::ProcRes
GenFit_FitTrackWelded::process_event(event::Event & e) {
    _recache_setup();
    // fill up the _gfTPs cache (genfit "track points" grouping up the
    // "raw measurements")
    _collect_gf_track_points(e);
    if( _gfTPs.empty() ) {
        log().debug( "No track points collected for the event %s."
                   , e.id.to_str().c_str() );
        return kOk;  // no track points in the event
    }

    //
    // Linear segments

    // assemble linear (or assumed to be linear) track segments
    std::unordered_map<int, genfit::Track *> linearTracks;
    for(int zoneID : _zones) {
        // A (foremost) position used to seed the tracking.
        // This shall be a point in the very upstream of the fitted track
        // (segment) for the fitting procedure to start. This may advance
        // the convergence procedure if choosen close to the real point.
        // For now we just use the center of the most upstream
        // plane, triggered within an event.
        TVector3 positionSeed( std::nan("0")
                             , std::nan("0")
                             , std::nan("0")
                             );
        // filter out track points related to this particular zone only
        // and update the position seed
        std::multimap<float, genfit::TrackPoint *> thisZoneTPs;
        for(auto tpp : _gfTPs) {
            if( zoneID != _zoneByDets[tpp.first] ) continue;
            // TODO: track unused track points here to delete 'em
            float sortValue = 0; {
                auto sortIt = _ordering.find(DetID(tpp.first));
                if( _ordering.end() != sortIt ) {
                    sortValue = sortIt->second;
                }
            }
            thisZoneTPs.emplace(sortValue, tpp.second);
            // Update position seed if need
            if( _posSeedUpdate ) {
                _posSeedUpdate(positionSeed, DetID(tpp.first), tpp.second);
            }
        }
        positionSeed += _posSeedOffset;

        // skip this zone if track points subset is empty
        if( thisZoneTPs.empty() ) {
            log().debug( "event %s -- no tracks at zone %d"
                   , e.id.to_str().c_str(), zoneID );
            continue;
        }
        
        // Create genfit track corresponding to linear segment in
        // current zone
        auto mRep = new genfit::RKTrackRep( _particlePDG /*, propDir=0*/ );
        genfit::Track * trackPtr = new genfit::Track( mRep
                , positionSeed  // position seed
                , _momSeed  // momentum seed
                );
        linearTracks[zoneID] = trackPtr;
        trackPtr->setCovSeed(_covSeed);

        // Move track points to genfit track
        // TODO: is sorting (by Z for instance) needed here or it is
        //       defined by state/momentum seed?
        for( auto tpPair : thisZoneTPs ) {
            trackPtr->insertPoint(tpPair.second);
            # if 0  // XXX
            {
                std::cout << " xxx ";
                for( const genfit::AbsMeasurement * rawMPtr : tpPair.second->getRawMeasurements() ) {
                    const TVectorD & rawCoords = rawMPtr->getRawHitCoords();
                    for( Int_t i = 0; i < rawCoords.GetNoElements(); ++i ) {
                        std::cout << rawCoords[i] << ", ";
                    }
                }
                std::cout << std::endl;
            }
            # endif
        }
    }

    // Fit linear tracks
    for( auto trackInZone : linearTracks ) {
        genfit::Track * trackPtr = trackInZone.second;
        //trackInZone.second->Print();  // XXX
        try {
            _fitter->processTrack( trackPtr );
            trackPtr->checkConsistency();
        } catch( genfit::Exception & err ) {
            log().error( "Failed to fit linear track in zone %d due to an"
                         " error (event %s): %s"
                       , trackInZone.first
                       , e.id.to_str().c_str()
                       , err.what()
                    );
            // NOTE: we do not remove track from collection, hoping that it
            // will still be possible to fit it with welded one
        }
        if( trackPtr->hasKalmanFitStatus() ) {
            genfit::KalmanFitStatus * kfs = trackPtr->getKalmanFitStatus();

            if( kfs->isFitConvergedFully() ) {
                log().debug( "event %s, zone %d: %zu track points, fit converged"
                             ", chi2/ndf=%.2e/%f (pv=%.2e), %u iterations"
                           , e.id.to_str().c_str()
                           , trackInZone.first
                           , trackInZone.second->getNumPoints()
                           , kfs->getChi2(), kfs->getNdf(), kfs->getPVal()
                           , kfs->getNumIterations()
                           );
            } else if( kfs->isFitConvergedPartially() ) {
                log().debug( "event %s, zone %d: %zu track points, fit converged"
                             ", %d points dropped, chi2/ndf=%.2e/%f (pv=%.2e), %u"
                             " iterations"
                           , e.id.to_str().c_str()
                           , trackInZone.first
                           , trackInZone.second->getNumPoints()
                           , kfs->getNFailedPoints()
                           , kfs->getChi2(), kfs->getNdf(), kfs->getPVal()
                           , kfs->getNumIterations()
                           );
            } else {
                log().debug( "event %s, zone %d: %zu track points, fit did not"
                             " converge for %u iterations"
                           , e.id.to_str().c_str()
                           , trackInZone.first
                           , trackInZone.second->getNumPoints()
                           , kfs->getNumIterations()
                           );
            }
            //trackInZone.second->Print();  // XXX
        } else {
            // AFAIU, this must not happen
            log().warn( "event %s, zone %d: %zu track points, fitting"
                        " resulted no Kalman fit status."
                      , e.id.to_str().c_str()
                      , trackInZone.first
                      , trackInZone.second->getNumPoints() );
        }
        //if( ! trackPtr->hasKalmanFitStatus() ) {
        //    continue;
        //}
        trackPtr->determineCardinalRep();  // xxx?
        if( _keepLinearTracks  // if we shall keep linear tracks
         && trackPtr->hasKalmanFitStatus()  // ...and track has fit status
         && trackPtr->getKalmanFitStatus()->isFitted()  // ... and track was fitted
          ) {
            auto track = lmem().create<event::Track>(lmem());
            util::reset(*track);
            copy_track_info(*track, *trackPtr);
            track->zonePattern = (0x1 << trackInZone.first);
            e.tracks.emplace(e.tracks.size(), track);
        }
    }

    //
    // Welding
    if(_doWelding) {
        // Merge tracks b/w zones
        genfit::Track * mergedTrack = nullptr;
        uint32_t zonePattern = 0x0;
        for( auto trackInZone : linearTracks ) {
            if( (!mergedTrack)
             && trackInZone.second->getNumPoints() ) {
                mergedTrack = new genfit::Track(*trackInZone.second);  // XXX? copy
                zonePattern |= (0x1 << trackInZone.first);
                continue;
            }
            //assert( mergedTrack->getPDG() == trackInZone.second->getPDG() );  // TODO ???
            mergedTrack->mergeTrack(new genfit::Track(*trackInZone.second));  // XXX? copy
            zonePattern |= (0x1 << trackInZone.first);
            if(!_keepLinearTracks) {
                // delete trackInZone.second;  // TODO
            }
        }
        if( mergedTrack->getNumPoints() > 1 ) {
            mergedTrack->checkConsistency();
            mergedTrack->udpateSeed();  // "udpate" -- native typo
            mergedTrack->checkConsistency();
            //mergedTrack->setCovSeed(_covSeed);  // xxx?
            _fitter->processTrack(mergedTrack);
            mergedTrack->checkConsistency();
            //mergedTrack->determineCardinalRep();

            genfit::FitStatus * mergedFS = nullptr;
            genfit::MeasuredStateOnPlane stLast;
            try {
                stLast = mergedTrack->getFittedState(-1);
                mergedFS = mergedTrack->getFitStatus();
            } catch( genfit::Exception & err ) {
                log().error( "Event %s: failed to fit welded track: %s"
                           , e.id.to_str().c_str()
                           , err.what() );
                delete mergedTrack;
                mergedTrack = nullptr;
            }

            if( mergedTrack && mergedTrack->hasKalmanFitStatus() ) {
                genfit::KalmanFitStatus * kfs = mergedTrack->getKalmanFitStatus();

                if( kfs->isFitConvergedFully() ) {
                    log().debug( "event %s, welded track: %zu track points, fit converged"
                                 ", chi2/ndf=%.2e/%f (pv=%.2e), %u iterations, cop=%.2e"
                               , e.id.to_str().c_str()
                               , mergedTrack->getNumPoints()
                               , kfs->getChi2(), kfs->getNdf(), kfs->getPVal()
                               , kfs->getNumIterations()
                               , stLast.getMomMag()
                               );
                } else if( kfs->isFitConvergedPartially() ) {
                    log().debug( "event %s, welded track: %zu track points, fit converged"
                                 ", %d points dropped, chi2/ndf=%.2e/%f (pv=%.2e), %u"
                                 " iterations, cop=%.2e"
                               , e.id.to_str().c_str()
                               , mergedTrack->getNumPoints()
                               , kfs->getNFailedPoints()
                               , kfs->getChi2(), kfs->getNdf(), kfs->getPVal()
                               , kfs->getNumIterations()
                               , stLast.getMomMag()
                               );
                } else {
                    log().debug( "event %s: %zu track points, fit did not"
                                 " converge for %u iterations"
                               , e.id.to_str().c_str()
                               , mergedTrack->getNumPoints()
                               , kfs->getNumIterations()
                               );
                }
                //trackInZone.second->Print();  // XXX
            } else if(mergedTrack) {
                // AFAIU, this must not happen
                log().warn( "event %s, welded track: %zu track points, fitting"
                            " resulted no Kalman fit status."
                          , e.id.to_str().c_str()
                          , mergedTrack->getNumPoints() );
            }

            if( mergedTrack
             && _copyTrack
             && mergedTrack->hasFitStatus()
             && mergedTrack->hasKalmanFitStatus()  // ...and track has fit status
             && mergedTrack->getKalmanFitStatus()->isFitted()  // ... and track was fitted
             ) {
                mergedTrack->determineCardinalRep();  // xxx?
                //if( mergedTrack->hasFitStatus() ) { ... }
                auto track = lmem().create<event::Track>(lmem());
                util::reset(*track);

                copy_track_info(*track, *mergedTrack);
                track->zonePattern = zonePattern;

                track->pdg      = stLast.getPDG();
                track->momentum = stLast.getMomMag();

                e.tracks.emplace(e.tracks.size(), track);
            }
        }
        if( mergedTrack && !_copyTrack ) {
            delete mergedTrack;
        }
    }

    // Cleanup
    _gfTPs.clear();
    _apvClusterPtrs.clear();
    return kOk;
}

namespace aux {

/// A functor setting tracking position seed to the most upstream center of
/// triggered plane. The Z is used to determine the "most upstream". No other
/// detector geometry info is taken into account.
class ForemostCenterZ
            : public util::Observable<calib::Placements>::iObserver
            , public calib::Handle<nameutils::DetectorNaming>
            {
public:
    /// A cached placement entry struct
    ///
    /// This entries are used for some auxiliry tasks to provide the additional
    /// spatial info like plane center to seed the fitting
    struct PlacementEntry {
        float c[3];
    };
protected:
    /// Detector placements cache to update the position seed
    std::unordered_map<DetID, PlacementEntry> _placementsCache;
public:
    ForemostCenterZ( calib::Manager & mgr )
            : calib::Handle<nameutils::DetectorNaming>("default", mgr) {
        mgr.subscribe<calib::Placements>(*this, "default");
    }
    void handle_update( const calib::Placements & placements ) override {
        auto & nm = calib::Handle<nameutils::DetectorNaming>::get();
        for( auto placement : placements ) {
            DetID did = nm[placement.data.name];
            _placementsCache[did] = PlacementEntry{
                    { placement.data.center[0]
                    , placement.data.center[1]
                    , placement.data.center[2]
                    }
                };
        }
    }
    void operator()( TVector3 & posSeed, DetID did, const genfit::TrackPoint * ) {
        auto plIt = _placementsCache.find(did);
        if( _placementsCache.end() == plIt ) {
            // This is not a fatal error in fact -- one can assume the
            // GenFit's part of information to still be valid even if
            // corresponding placement was not updated. Yet, we print a
            // warning here to make the user know about sub-optimal
            // fitting (todo: once per plane?)
            log4cpp::Category::getInstance("handlers.tracking.aux").warn(
                    "Missing placement info for %s"
                    , calib::Handle<nameutils::DetectorNaming>::get()
                                [DetID(did)].c_str() );
            return;
        }
        if( std::isnan(posSeed[2]) || posSeed[2] > plIt->second.c[2] ) {
            posSeed.SetXYZ( plIt->second.c[0]
                          , plIt->second.c[1]
                          , plIt->second.c[2]
                          );
        }
    }
};

}  // namespace ::na64dp::handlers::aux

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( GenFit_FitTrackWelded
                , mgr, cfg
                , "Assembles GenFit measurement objects into track points"
                  " and performs track fitting"
                ) {
    std::string geomSrcURI;
    if( cfg["geometryFile"] ) {
        geomSrcURI = cfg["geometryFile"].as<std::string>();
    }
    Int_t pdgCode = TDatabasePDG::Instance()->GetParticle(
                (cfg["RKInitParticle"] ? cfg["RKInitParticle"].as<std::string>()
                                       : std::string("e-")).c_str()
            )->PdgCode();
    const std::string posSeedUpdStrExpr
            = cfg["positionSeed"]
            ? cfg["positionSeed"].as<std::string>()
            : std::string("lowest-z")
            ;
    std::function<void(TVector3 & seed, na64dp::DetID, const genfit::TrackPoint *)> updF;
    if( posSeedUpdStrExpr == "lowest-z" ) {
        auto p = std::make_shared<na64dp::handlers::aux::ForemostCenterZ>(mgr);
        updF = [p]( TVector3 & seed
                  , na64dp::DetID did
                  , const genfit::TrackPoint * tp
                  ){p->na64dp::handlers::aux::ForemostCenterZ::operator()(seed, did, tp);};
    } else {
        NA64DP_RUNTIME_ERROR( "Unknown position seed update method \"%s\"." );
    }

    TVector3 seedOffset(0, 0, 0);
    if( cfg["positionSeedOffset"] ) {
        seedOffset.SetX(cfg["positionSeedOffset"][0].as<float>());
        seedOffset.SetY(cfg["positionSeedOffset"][1].as<float>());
        seedOffset.SetZ(cfg["positionSeedOffset"][2].as<float>());
    }

    TVector3 momSeed;
    if( !cfg["momSeed"] ) {
        NA64DP_RUNTIME_ERROR( "\"momSeed\" parameter not found." );
    }
    momSeed.SetX(cfg["momSeed"][0].as<float>());
    momSeed.SetY(cfg["momSeed"][1].as<float>());
    momSeed.SetZ(cfg["momSeed"][2].as<float>());

    TMatrixDSym covSeed(6);
    {
        // TODO: non-scalar case(s) to vary covariance seeds?
        const float scalar = cfg["covSeed"] ? cfg["covSeed"].as<float>() : 1.0;
        for( int i = 0; i < 6; ++i ) covSeed(i, i) = scalar;
    }

    return new ::na64dp::handlers::GenFit_FitTrackWelded( mgr
                , cfg["magField"]
                , pdgCode
                , momSeed
                , geomSrcURI
                , cfg["disableMatEffects"] ? cfg["disableMatEffects"].as<bool>() : false
                , cfg["fitter"]
                , cfg["keepLinearTracks"] ? cfg["keepLinearTracks"].as<bool>() : false
                , cfg["copyTracks"] ? cfg["copyTracks"].as<bool>() : true
                , updF
                , seedOffset
                , covSeed
                , cfg["doWelding"] ? cfg["doWelding"].as<bool>() : false
                );
}

