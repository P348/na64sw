#include "fitTrack.hh"
#include "absFitter.hh"
#include "eventDisplay.hh"

// genfit
#include <TGeoManager.h>

#include <AbsFitter.h>
#include <TGeoMaterialInterface.h>
#include <FieldManager.h>
#include <MaterialEffects.h>
#include <TrackPoint.h>
#include <Track.h>
#include <TDecompSVD.h>
#include <KalmanFitStatus.h>
#include <KalmanFitterInfo.h>
#include <PlanarMeasurement.h>
#include <WireMeasurementNew.h>
#include <WireMeasurement.h>
#include <RectangularFinitePlane.h>
#include <SharedPlanePtr.h>
//#include <ProlateSpacepointMeasurement.h>
#include <SpacepointMeasurement.h>

#include "na64detID/TBName.hh"
#include "na64event/data/event.hh"
#include "na64event/data/track.hh"
#include "eventDisplay.hh"

// (un)comment this to define which version of wire measurment to use
// It seems "old" wire hits to be more suited for drift-like detectors
// representation. "New" ones also aren't supported by display.
//#define USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS 1

// (un) comment this to enable/disable additional DEBUG log; may affect
// performance
#define GF_DEBUG_LOG_ENABLED 1

namespace na64dp {
namespace handlers {
namespace aux {

//
// Wired plane geometry cache entry -------------------------------------------

GenfitGeometryCache::WiredPlane::WiredPlane( log4cpp::Category & L_
                                           , const calib::Placement & pl
                                           , DetID did )
        : iDetGeoEntry(L_, pl.name)
        , gfPlaneID(0x0)
        , measuredSize(pl.size[0])
        , ignoreDriftFeatures(false)
        , nHit(0)
        {
    // set GenFit's ids to NA64SW
    gfDetID = ((DetID_t) StationKey(did).id) >> na64dp::aux::gDetNumOffset;
    gfPlaneID = did.id & na64dp::aux::gMaskPayload;
    // assure it is ordinary plane's placement
    if(!(calib::Placement::kRegularWiredPlane == pl.suppInfoType
      || calib::Placement::kIrregularWiredPlane == pl.suppInfoType ) ) {
        NA64DP_RUNTIME_ERROR( "Wrong type of placement information"
                " entry" );  // TODO: dedicated exception for better msging
    }
    // Create GenFit plane
    util::Transformation t = util::transformation_by(pl);
    auto boundingShape = new genfit::RectangularFinitePlane(
                    - pl.size[0]/2., pl.size[0]/2.,
                    - pl.size[1]/2., pl.size[1]/2.
        );
    // Note: GF does not say anything about length of u and v here. Seems it
    // only direction matters.
    gfPlanePtr
        = genfit::SharedPlanePtr(new genfit::DetPlane( TVector3(t.o().r)
                    , TVector3(t.gU().r)
                    , TVector3(t.gV().r)
                    , boundingShape));
    if(pl.suppInfoType == calib::Placement::kRegularWiredPlane) {
        if( std::isnan(pl.suppInfo.regularPlane.resolution) ) {
            resolution = pl.size[0]/pl.suppInfo.regularPlane.nWires;
            resolution /= sqrt(12.);
        } else {
            resolution = pl.suppInfo.regularPlane.resolution;
        }
    } else {
        resolution = std::nan("0");
    }
    L << log4cpp::Priority::DEBUG << "Created GenFit [rectangular finite] plane #"
      << (int) gfDetID << "/" << (int) gfPlaneID
      << " based on placement info \"" << pl.name << "\" at "
      << t.o() << " with dimensions " << pl.size[0] << "x" << pl.size[1]
      << " rotated by " << pl.rot[0] << ", " << pl.rot[1] << ", " << pl.rot[2]
      << ", resolution=" << resolution << ", local bases: u=" << t.gU() << ", v="
      << t.gV() << "."
      ;
}

genfit::AbsMeasurement *
GenfitGeometryCache::WiredPlane::new_measurement( const event::TrackScore & score
                                                , float & sortPar
                                                ) {
    assert(!std::isnan(measuredSize));
    assert(measuredSize > 0);
    if(ignoreDriftFeatures || !score.driftFts) {
        // Create ordinary 1D Genfit2's measurement
        TVectorD lr(1);
        lr(0) = score.lR[0]*measuredSize;
        if(!std::isnan(score.lR[1])) {
            NA64DP_RUNTIME_ERROR("Creating 1D GenFit2 measurement from"
                    " score of at least two dimensions." );
        }
        TMatrixDSym hitCov(1);
        if(std::isnan(resolution)) {
            // resolution is not overriden for plane -- use hit's
            throw std::runtime_error("TODO: scale score's uncertainty for wired plane");
            // TODO cov should be multiplied here by size^2
            hitCov(0, 0) = score.lRErr[0]*score.lRErr[0];
        } else {
            hitCov(0, 0) = resolution;
        }
        if(std::isnan(hitCov(0, 0))) {
            NA64DP_RUNTIME_ERROR("Resolution is NaN for track score of"
                    " GF plane #%d/%d.", (int) gfDetID, (int) gfPlaneID
                    );
        }
        auto msrmntPtr = new genfit::PlanarMeasurement(
                      lr  // (local) hit coordinate(s)
                    , hitCov  // hit covariance
                    , gfPlaneID  // plane key ID as detector ID
                    , ++nHit // hit ID
                    , nullptr  // genfit trackpoint instance
                    );
        msrmntPtr->setPlane( gfPlanePtr, gfPlaneID );
        #warning "TODO: elaborate sorting parameter"
        sortPar = gfPlanePtr->getO()[2];
        msrmntPtr->setDetId(gfDetID);
        L << log4cpp::Priority::DEBUG << "Created GF measurement at regular"
            " rectangular plane #" << (int) gfPlaneID
              << " (" << name << "): score u_nrm=" << score.lR[0]
              << ", u=" << lr(0) << "cm, resolution="
              << hitCov(0, 0) << ", without drift fts.";
        return msrmntPtr;
    } else {
        // Create 1D GenFit2's measurement for wired plane.
        // NOTE: though GenFit2 assumes drift measurement to have their own
        // "virtual" planes it is convenient to not split them here as
        // logically here.
        auto & df = *score.driftFts;
        // GenFit2 does not offer plane ID for `WireMeasurement[New]`, so we
        // encode plane key into hit ID...
        WireID wid;
        assert(gfPlaneID < 16);
        wid.proj((WireID::Projection) gfPlaneID);
        DetID did(gfDetID);
        did.payload(wid.id);
        // TODO: only straw hit is considered here
        //hit.hitRefs
        #if defined(USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS) && USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS
        auto m = new genfit::WireMeasurementNew( df.distance  // distance, R(T)
                , df.distanceError  // R(T) error
                , TVector3(df.wireCoordinates)
                , TVector3(df.wireCoordinates + 3)
                , did.id // plane key ID as detector ID
                , ++nHit // hit ID
                , nullptr  // genfit trackpoint instance
                );
        #else  // USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS
        TVectorD rawHitCoords(7);
        rawHitCoords[6] = df.distance;
        TMatrixDSym rawHitCov(7);
        rawHitCov(6,6) = df.distanceError * df.distanceError;
        for(int i = 0; i < 3; ++i) {
            rawHitCoords[i  ] = df.wireCoordinates[i  ];
            rawHitCoords[i+3] = df.wireCoordinates[i+3];
            //rawHitCov(  i,   i) = 10;  // cm
            //rawHitCov(i+3, i+3) = 10;  // cm
            throw std::runtime_error("TODO: set cov for drift detectors");
        } 
        auto m = new genfit::WireMeasurement( rawHitCoords
                , rawHitCov
                , did.id  // det ID
                , ++nHit  // hit ID
                , nullptr  // genfit trackpoint instance
                );
        m->setLeftRightResolution(0);
        #endif  // USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS
        #warning "TODO: elaborate sorting parameter (drift detector)"
        sortPar = gfPlanePtr->getO()[2];
        // TODO: log this
        //L << log4cpp::Priority::DEBUG << "Created GF measurement at regular"
        //    " rectangular plane #" << (int) gfPlaneID
        //      << ": score u_nrm=" << score.lR[0]
        //      << ", u=" << lr(0) << "cm, resolution="
        //      << hitCov(0, 0) << ", with drift fts accounted.";
        return m;
    }
}

void
GenfitGeometryCache::WiredPlane::set_residuals(
          const std::vector<genfit::MeasurementOnPlane *> & measurements
        , event::ScoreFitInfo & sfi
        , const genfit::MeasuredStateOnPlane & smoothedState
        , bool isBiased
        ) {
    assert(sfi.score);
    const auto & score = *sfi.score;
    if(ignoreDriftFeatures || !score.driftFts) {
        assert( 1 == measurements.size() );  // strip/simple wire detector
        auto & measurement = measurements[0];
        const genfit::AbsHMatrix* H = measurement->getHMatrix();
        TVectorD res(H->Hv(smoothedState.getState()));
        res -= measurement->getState();
        res *= -1;
        assert(1 == res.GetNrows());
        auto * residualsArray = (isBiased ? sfi.lRBErr : sfi.lRUErr);
        // yet, if we "ignore" drift features AND they're exist, use another
        // destination array
        if(ignoreDriftFeatures && score.driftFts) {
            residualsArray = score.driftFts->uError;
        }
        residualsArray[0] = res(0);
        residualsArray[1] = std::nan("0");
        sfi.weight = measurement->getWeight();
        if(!(ignoreDriftFeatures && score.driftFts)) {
            residualsArray[0] /= measuredSize;
        }
        #if 0
        TMatrixDSym cov(smoothedState.getCov());
        H->HMHt(cov);
        cov += measurement->getCov();

        return MeasurementOnPlane(res, cov, plane
                , smoothedState.getRep(), H->clone(), measurement->getWeight());
        #endif
        // copy state values (valid at least for RK track
        // respresentation): (q/p, u', v', u, v)
        //score.tangent[0] = smoothedState.getState()(0);
        //score.tangent[1] = smoothedState.getState()(1);
    } else {
        assert( 2 == measurements.size() );  // left, right -- drift detector
        float out[2][2];  // {left, right}, {residual, weight}
        for( int i = 0; i < 2; ++i ) {
            auto & measurement = measurements[i];
            const genfit::AbsHMatrix* H = measurement->getHMatrix();
            TVectorD res(H->Hv(smoothedState.getState()));
            res -= measurement->getState();
            res *= -1;
            assert(1 == res.GetNrows() && 1 == res.GetNoElements());
            out[i][0] = res(0);
            out[i][1] = measurement->getWeight();
            // ^^^ NOTE: This "weights" seems to be initialized with equal
            // values since we've just created it. There must be a way to
            // obtain weights for raw measurements using fitter info struct,
            // but GenFit seem to lose this association.
        }
        // decide wich side to consider for main/subsidiary residual
        auto * residualsArray = (isBiased ? sfi.lRBErr : sfi.lRUErr);
        int nMainHit = -1;
        if( out[0][1] > out[1][1] ) {
            nMainHit = 0;
        } else if( out[0][1] < out[1][1] ) {
            nMainHit = 1;
        } else {
            // weights is equal, just choose closest
            if(fabs(out[0][0]) < fabs(out[1][0])) {
                nMainHit = 0;
            } else {
                nMainHit = 1;
            }
        }
        assert( nMainHit > -1 );
        residualsArray[0] = out[nMainHit][0] / measuredSize;
        residualsArray[1] = std::nan("0");
        sfi.weight = out[nMainHit][1];
        (isBiased ? score.driftFts->sideHitBError
                  : score.driftFts->sideHitUError ) = out[(nMainHit+1)%2][0];
        (isBiased ? score.driftFts->sideWeight
                  : score.driftFts->sideWeight ) = out[(nMainHit+1)%2][1];
    }
    // copy local fitted state
    if(unbiasedFittedState == !isBiased) {
        // copy local fitted state
        //smoothedState.Print();  // XXX
        //sfi.qop = smoothedState.getQop();
        sfi.time = smoothedState.getTime();
        // NOTE: to get local direction tangents here we assume that current
        // track representation is standard GenFit's RK with 5D state
        // (q/p,u',v',u,v), so state can be copied literally. This assumption
        // can be wrong in general.
        auto localState = smoothedState.getState();
        assert(5 == localState.GetNoElements());
        sfi.qop = localState(0);
        sfi.lTan[0] = localState(1);
        sfi.lTan[1] = localState(2);
        sfi.lR[0] = localState(3);
        sfi.lR[1] = localState(4);
        #if 0
        //TVector3 dir = smoothedState.getDir();
        //sfi.lTan = smoothedState.getState();
        const genfit::AbsTrackRep * trackRep = smoothedState.getRep();
        const genfit::RKTrackRep * rkTrackRep = static_cast<const genfit::RKTrackRep *>(trackRep);
        assert(rkTrackRep);
        rkTrackRep->getDir(smoothedState);
        #endif
    }
}

void
GenfitGeometryCache::WiredPlane::reset() {
    nHit = 0;
    // this idle measurements aren't bound to any of the GenFit2 owners, so
    // we delete them explicitly
    for( auto p : scores2idleMsrmnts ) {
        delete p.second;
    }
    scores2idleMsrmnts.clear();  // track scores cleared by event dtr
}

//
// 2D plane geometry cache entry ----------------------------------------------

GenfitGeometryCache::DetectorStation::DetectorStation(log4cpp::Category & L_, const std::string & name_)
        : iDetGeoEntry(L_, name_)
        , gfPlaneID(0x0)  // remains zero
        , gfPlanePtr(nullptr)  // TODO
        , nHit(0)
        {
    resolution[0] = resolution[1] = std::nan("0");
}

void
GenfitGeometryCache::DetectorStation::add_plane(const calib::Placement & placement, DetID did){
    auto gfDetID_ = ((DetID_t) StationKey(did).id) >> na64dp::aux::gDetNumOffset;
    if(!gfDetID) {
        gfDetID = gfDetID_;
    } else if( (unsigned int) gfDetID != gfDetID_ ) {
        NA64DP_RUNTIME_ERROR("Unable to combine detector ID for det geo cache"
                " entry");
        // ^^^ TODO: details; this means that combined planes are not from
        // single station, apparently
    }
    // gfPlaneID remains zero
    auto ir = _planePlacements.emplace(PlaneKey(did), placement);
    if(!ir.second) {
        NA64DP_RUNTIME_ERROR("Duplicating plane key for single station");
        // ^^^ TODO: details
    }
}

void
GenfitGeometryCache::DetectorStation::_make_gf_plane() {
    // Following cases are possible for DetectorStation:
    //  1) Station measures two coordinates (X, Y) by two planes; these hits
    //     are combined then into 2D hit.
    //  2) Similar to 1), but measurement is done with doubled layers (TODO)
    //  3) Station measures both local X,Y at once (i.e. pixel detector, TODO)
    //  4) Station measures spatial point (TODO)

    if( 2 == _planePlacements.size() ) {
        // case 1) -- expect two planes
        // figure out which pair we have to consider here (may be: X/Y or U/V)
        StationKey sk(_planePlacements.begin()->first.id);
        PlaneKey pkX1(sk.chip(), sk.kin(), sk.number(), WireID::kX)
               , pkU1(sk.chip(), sk.kin(), sk.number(), WireID::kU)
               , pkY1(sk.chip(), sk.kin(), sk.number(), WireID::kY)
               , pkV1(sk.chip(), sk.kin(), sk.number(), WireID::kV)
               ;
        auto itX1 = _planePlacements.find(pkX1)
           , itU1 = _planePlacements.find(pkU1)
           ;
        if((_planePlacements.end() == itX1) == (_planePlacements.end() == itU1)) {
            NA64DP_RUNTIME_ERROR("Failed to derive projection pair"
                    " for station.");  // TODO: details
        }
        decltype(_planePlacements)::iterator it1, it2;
        if(_planePlacements.end() == itU1) {  // X/Y pair
            it1 = itX1;
            it2 = _planePlacements.find(pkY1);
        } else {
            it1 = itU1;
            it2 = _planePlacements.find(pkV1);
        }
        if(it2 == _planePlacements.end()) {
            NA64DP_RUNTIME_ERROR("No reciprocal placements cache for plane in"
                    " station.");  // TODO: details
        }
        // define plane on local measurement vectors for both projections
        const util::Transformation t1 = util::transformation_by(it1->second)
                                 , t2 = util::transformation_by(it2->second)
                                 ;
        // find "averaged" point
        util::Vec3 meanO = (t1.o() + t2.o())/2;
        #if 0
        const calib::Placement & pl1 = it1->second
                             , & pl2 = it2->second;
        util::Vec3 o[2], u[2], v[2], w[2];
        util::cardinal_vectors( pl1.center, pl1.size, pl1.rot, o[0], u[0], v[0], w[0]);
        util::cardinal_vectors( pl2.center, pl2.size, pl2.rot, o[1], u[1], v[1], w[1]);
        if( std::fabs((o[1] - o[0]).norm()) > 1e-2 ) {
            // ... TODO: perhaps, compute it inline?
            NA64DP_RUNTIME_ERROR("Centers do not match for station planes");
            // ^^^ TODO: details
        }
        #endif
        gfPlanePtr
            = genfit::SharedPlanePtr(new genfit::DetPlane(
                        TVector3(meanO.r), TVector3(t1.u().r), TVector3(t2.u().r)));
        // ^^^ TODO: bounding shape?
        if( std::isnan(it1->second.suppInfo.regularPlane.resolution) ) {
            resolution[0] = it1->second.size[0]/it1->second.suppInfo.regularPlane.nWires;
            resolution[0] /= sqrt(12.);
        } else {
            resolution[0] = it1->second.suppInfo.regularPlane.resolution;
        }
        if( std::isnan(it2->second.suppInfo.regularPlane.resolution) ) {
            resolution[1] = it2->second.size[0]/it2->second.suppInfo.regularPlane.nWires;
            resolution[1] /= sqrt(12.);
        } else {
            resolution[1] = it2->second.suppInfo.regularPlane.resolution;
        }
    }
    // ... else if()  // (TODO: other plane combining cases)
    else {
        NA64DP_RUNTIME_ERROR("Unexpected number of projection planes for"
                " station.");  // TODO: details
    }
}

genfit::AbsMeasurement *
GenfitGeometryCache::DetectorStation::new_measurement( const event::TrackScore & score
                                                     , float & sortPar
                                                     ) {
    if( ! gfPlanePtr ) {
        _make_gf_plane();
    }
    // Create 2D Genfit2's measurement
    TVectorD lr(2);
    lr(0) = score.lR[0];
    lr(1) = score.lR[1];
    if(!std::isnan(score.lR[2])) {
        NA64DP_RUNTIME_ERROR("Creating GenFit2 measurement from"
                " score of at least three dimensions is not implemented" );
    }
    TMatrixDSym hitCov(2);
    hitCov(0, 0) = resolution[0];
    hitCov(0, 1) = 0.;
    hitCov(1, 1) = resolution[1];
    hitCov(1, 0) = 0.;
    auto msrmntPtr = new genfit::PlanarMeasurement(  // TODO: 
                  lr  // (local) hit coordinate(s)
                , hitCov  // hit covariance
                , gfPlaneID  // plane key ID as detector ID
                , ++nHit // hit ID
                , nullptr  // genfit trackpoint instance
                );
    msrmntPtr->setPlane( gfPlanePtr, gfPlaneID );
    #warning "TODO: elaborate sorting parameter"
    sortPar = gfPlanePtr->getO()[2];
    msrmntPtr->setDetId(gfDetID);
    return msrmntPtr;
}

void
GenfitGeometryCache::DetectorStation::set_residuals(
        const std::vector<genfit::MeasurementOnPlane *> &,
        event::ScoreFitInfo & sfi,
        const genfit::MeasuredStateOnPlane &,
        bool isBiased) {
    throw std::runtime_error("TODO: residuals extraction for station hit");
    // ...
}

void
GenfitGeometryCache::DetectorStation::reset() {
    nHit = 0;
    // ...
}

//
// "Deep" hodoscope plane

GenfitGeometryCache::VolumetricDetector::VolumetricDetector( DetID did
                        , const calib::Placement & pl
                        , log4cpp::Category & logCat)
                : iDetGeoEntry(logCat, pl.name)
                , _name(pl.name)
                , _nHit(0)
                , _t(util::transformation_by(pl))
                , gfPlaneID(0)
                {
    gfDetID = ((DetID_t) StationKey(did).id) >> na64dp::aux::gDetNumOffset;
    gfPlaneID = did.number();
}

genfit::AbsMeasurement *
GenfitGeometryCache::VolumetricDetector::new_measurement( const event::TrackScore & score
                                                        , float & sortPar
                                                        ) {
    // TODO: it is not clear from GenFit docs, whether spacepoint measurement
    //       can be defined with respect to some detector volume. Apparently
    //       not (but why then ctr parameter is still called `rawHitCoords`?)
    #if 0
    TVectorD rawHitCoords(3);
    // NOTE: ROOT's TMatrixDSym is not actually symmetric in memory
    TMatrixDSym covs(3);
    for(int nRow = 0; nRow < 3; ++nRow) {
        rawHitCoords[nRow] = score.lR[nRow];
        covs(nRow, nRow) = score.lRErr[nRow]*score.lRErr[nRow];
    }
    auto m = new genfit::SpacepointMeasurement(
                rawHitCoords, covs, gfPlaneID, ++_nHit, nullptr
            );
    sortPar = _o.c.z;
    #else
    util::Vec3 globalPos = _t({{score.lR[0], score.lR[1], score.lR[2]}});
    TVectorD hitCoords(3);
    // NOTE: ROOT's TMatrixDSym is not actually symmetric in memory
    TMatrixDSym covs(3);
    for(int nRow = 0; nRow < 3; ++nRow) {
        hitCoords[nRow] = globalPos.r[nRow];
    }
    // NOTE: in case of volumetric measurement GenFit seem to rely on the
    //       covariance matrix in global frame, so we have to provide it with
    //       "rotated" cavoariance. It is given by:
    //          cov(R.v) = R.cov(v).R^T
    //       Our sigmas (`lRErr`) are given in local frame, so we have to
    //       build cov(v), R and apply "rotation".
    const double covNorms[3] = { _t.u().norm(), _t.v().norm(), _t.w().norm() };
    for(int i = 0; i < 3; ++i) {
        // scale cov up to measurement range, square it and put on diagonal
        double cov = score.lRErr[i];
        cov *= covNorms[i];
        covs(i, i) = cov*cov;
    }
    // obtain final cov matrix as R.cov(v).R^T
    // TODO: extenral function
    TMatrixD R(3, 3);
    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
            R(i, j) = _t.affine_matrix().cm[i][j];
        }
    }
    TMatrixD covs_(R, TMatrixD::kMult, TMatrixD(covs, TMatrixD::kMultTranspose, R));
    assert(covs_.IsSymmetric());
    // TODO: how to do it in more efficient way with ROOT?
    covs(0, 0) = covs_(0, 0);
    covs(0, 1) = covs_(0, 1);
    covs(0, 2) = covs_(0, 2);
    covs(1, 1) = covs_(1, 1);
    covs(0, 1) = covs_(0, 1);
    covs(2, 2) = covs_(2, 2);

    auto m = new genfit::SpacepointMeasurement(
                hitCoords, covs, gfPlaneID, ++_nHit, nullptr
            );
    sortPar = globalPos.c.z;
    #endif
    return m;
}

void
GenfitGeometryCache::VolumetricDetector::set_residuals(
          const std::vector<genfit::MeasurementOnPlane *> & measurements
        , event::ScoreFitInfo & sfi
        , const genfit::MeasuredStateOnPlane & smoothedState
        , bool isBiased
        ) {
    if(measurements.size() > 1) {
        NA64DP_RUNTIME_ERROR("More than one measurements (%zu) provided for"
                " a single track score in volumetric detector (for"
                " residuals calculus).", measurements.size() );
    }
    genfit::MeasurementOnPlane * mopPtr = measurements[0];
    // get measured coordinates
    //TVector3 measuredPos = mopPtr->StateOnPlane::getPos();  // prohibited
    TVectorD measuredStateOnPlane = mopPtr->getState();  // return local state ("on plane")
    // Use (virtual) plane to convert measured coordinatie in global:
    // - get "local state-to-measurement" conversion matrix (H-matrix)
    const genfit::AbsHMatrix * H = mopPtr->getHMatrix();
    // - get measured and fitted local ("on plane") states using H-matrix
    TVectorD fittedLocal(H->Hv(smoothedState.getState()))
           , measuredLocal(mopPtr->getState());
    // - use (virtual) plane to convert into global frame
    auto vPlanePtr = smoothedState.getPlane();
    TVector3 fitted   = vPlanePtr->toLab(TVector2(fittedLocal(0), fittedLocal(1)))
           , measured = vPlanePtr->toLab(TVector2(measuredLocal(0), measuredLocal(1)));
    Double_t res_[3];
    (measured - fitted).GetXYZ(res_);
    TVectorD res(3, res_);
    // xxx:
    #if 0
    res.Print();
    std::cout << "  u = " << _u << std::endl  // XXX
              << "  v = " << _v << std::endl  // XXX
              << "  w = " << _w << std::endl  // XXX
              ;
    #endif
    // decompose residuals vector in local basis
    TMatrixF basis(3, 3, _t.affine_matrix().m);
    TDecompSVD svd(basis);
    Bool_t resolved = svd.Solve(res);
    //const Float_t norms[] = {1/_t.u().norm(), 1/_t.v().norm(), 1/_t.w().norm()};
    if(resolved) {
        auto * r = (isBiased ? sfi.lRBErr : sfi.lRUErr);
        for(int i = 0; i < 3; ++i) {
            r[i] = res(i); //*norms[i];
        }
    }
    #if 0
    res.Print(); /// XXX
    TVectorD ctrl = _bases*res;
    //ctrl -= measured - fitted;
    ctrl.Print();
    #endif
    #if 0
    auto & measurement = measurements[0];
    //measurement->Print();  // XXX
    // GenFit computes residuals as difference:
    //      measured - fitted
    // where measured can be retrieved as `measurement->getState()` and
    // fitted is `H->Hv(state->getState())`.
    const genfit::AbsHMatrix * H = measurement->getHMatrix();
    // apply H matrix on vector of smoothed state to get local coords on a
    // virtual plane
    TVectorD res(H->Hv(smoothedState.getState()));
    res -= measurement->getState();
    res *= -1;  // flip
    assert(2 == res.GetNrows());  // ?
    auto * residualsArray = (isBiased ? score.lRBErr : score.lRUErr);
    residualsArray[0] = res(0);
    residualsArray[1] = res(1);
    //residualsArray[2] = res(2);
    score.weight = measurement->getWeight();
    // This is for debug ------------------------------------------------------
    std::cout << "-- biased=(" << (isBiased ? "yes" : "no") << std::endl;
    auto sharedPlanePtr = smoothedState.getPlane();
    sharedPlanePtr->Print();  // XXX
    //std::cout << "Plane (virtual?):" << std::endl
    //          << "  o: {" << sharedPlanePtr->getO(0)
    TVectorD fitted(H->Hv(smoothedState.getState()))
           , measured(measurement->getState())
           ;
    TVectorD smoothedState_ = smoothedState.getState();
    std::cout << "    smoothedState = {"
              << smoothedState_(0) << ", "
              << smoothedState_(1) << ", "
              << smoothedState_(2) << ", "
              << smoothedState_(3) << ", "
              << smoothedState_(4)
              << "}" << std::endl
              << "    R = {" << residualsArray[0] << ", " << residualsArray[1] << "}" << std::endl
              << "    Fitted = {"
              << fitted(0) << ", " << fitted(1) << ", " << fitted(2)
              << "}" << std::endl
              << "    Measured = {"
              << measured(0) << ", " << measured(1) << ", " << measured(2) << "}" << std::endl
              << "--" << std::endl;
    #endif
}

void
GenfitGeometryCache::VolumetricDetector::reset() {
    _nHit = 0;
}

//
// Geometry cache ============================================================

void
GenfitGeometryCache::handle_update( const calib::Placements & placements ) {
    if( ! placements.empty() ) {
        std::set<std::string> sources;
        for(const auto & pl : placements) {
            sources.insert(pl.srcDocID);
        }
        _geoCacheLog << log4cpp::Priority::NOTICE
                     << "Using " << placements.size()
                     << " entries from " << sources.size() << " file(s): "
                     << util::str_join(sources.begin(), sources.end());
    }
    //std::unordered_map<StationKey, DetectorStation *> stations;
    // Iterate over placements and create corresponding geo cache entry
    for(auto & pl_ : placements) {
        const calib::Placement & pl = pl_.data;
        if( ((int) pl.suppInfoType) & ((int) calib::Placement::kDetectorsGeometry ) ) {
            DetID dID = naming()[pl.name];
            auto it = _detGeoCache.find(dID);
            if(_detGeoCache.end() != it) {
                _geoCacheLog << log4cpp::Priority::DEBUG
                    << "Erasing GenFit detector entry \"" << pl.name << "\" as"
                       " it has been overriden by placements at " << pl_.srcDocID
                    << pl_.lineNo << ".";
                    ;
                delete it->second;
                _detGeoCache.erase(it);
            }
            if( pl.suppInfoType == calib::Placement::kRegularWiredPlane
             || pl.suppInfoType == calib::Placement::kIrregularWiredPlane
              ) {
                it = _detGeoCache.emplace(dID, new WiredPlane(_geoCacheLog, pl, dID)).first;
                _geoCacheLog << log4cpp::Priority::DEBUG
                             << "Added [ir]regular wired plane \"" << pl.name << "\" to GenFit"
                                " tracking detectors set (ptr=" << (void*) it->second << ")";
            } else if(pl.suppInfoType == calib::Placement::kVolumetricDetector ) {
                it = _detGeoCache.emplace(dID, new VolumetricDetector(dID, pl, _geoCacheLog)).first;
                _geoCacheLog << log4cpp::Priority::DEBUG
                             << "Added volumetric detector \"" << pl.name << "\" to GenFit"
                                " tracking detectors set (ptr=" << (void*) it->second << ")";
                // volumetric detectors obtained from placements may contain
                // projection identifier that may be absent in score IDs within a
                // track (depending on the track finding scheme). Here we make
                // alias for station key, assuring the uniquiness
                StationKey sk(dID);
                auto stationIt = _detGeoCache.find(DetID(sk));
                if(stationIt != _detGeoCache.end()) {
                    // This is supsicious. Probably indicates that there are more
                    // than one projections for "volumetric detector" item...
                    _geoCacheLog << log4cpp::Priority::WARN
                        << "While deriving station key from volumetric"
                            " detector \"" << pl.name << "\": failed to alias"
                            " fully-qualified geom.entity with station key \""
                        << naming()[sk] << "\" as it already exists.";
                } else {
                    _detGeoCache.emplace(sk, it->second);
                }
            } else {
                _geoCacheLog << log4cpp::Priority::WARN
                             << "Ignoring detector placement \"" << pl.name << "\" as it is of"
                             << " unknown geometry type.";
                continue;
            }
        } else if( ((int) pl.suppInfoType) & ((int) calib::Placement::kMagnetsGeometry) ) {
            // NOTE: we omit magnetic fields creation here as, for certain
            // types sizes of volumes is not known until they're created.
            _geoCacheLog << log4cpp::Priority::DEBUG
                             << "Magnetic field placement " << pl.name
                             << " quietly ignored for decoration creation"
                                " (that's fine).";
        }  /* else if() ... other placement types? */ else {
            _geoCacheLog << log4cpp::Priority::DEBUG
                << "Omitting placements item \"" << pl.name << "\" as of unknown"
                   " type.";
        }
    }
}

GenfitGeometryCache::~GenfitGeometryCache() {
    // set of already deleted items to handle aliased ones (we have some cache
    // items appearing more than one, by different IDs).
    std::unordered_set<void *> geoCacheDeletedItems;
    for( auto p : _detGeoCache ) {
        if( geoCacheDeletedItems.find(p.second) != geoCacheDeletedItems.end()) continue;
        geoCacheDeletedItems.insert(p.second);
        delete p.second;
    }
}

void
GenfitGeometryCache::create_GF2_tps( std::multimap<float, genfit::TrackPoint *> & gfTPs
                                   , const event::Track & track ) const {
    float gfTPSortPar = std::nan("0");
    // Populate GenFit2 tracks with track points -- one TP per unique DetID
    std::unordered_map<int, DetID_t> pseudoIDToDet;
    auto detCacheIt = _detGeoCache.end();
    DetID_t lastDetID = 0x0;
    genfit::TrackPoint * gfTP = nullptr;
    #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
    _geoCacheLog << log4cpp::Priority::DEBUG
         << "Converting track of " << track.size() << " entries";
    #endif
    for( auto & scoreP : track ) {
        DetID scoreID = scoreP.first;
        if( lastDetID != scoreID ) {  // new det ID
            #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
            _geoCacheLog << log4cpp::Priority::DEBUG
                 << "Entry of \"" << naming()[scoreID] << "\" assumed to"
                   " appear first time,";
            #endif
            detCacheIt = _detGeoCache.find(scoreID);
            if( _detGeoCache.end() == detCacheIt ) {
                // collect faulty detector name and proceed
                auto it = _faultyNames.emplace(naming()[scoreID], 0).first;
                ++(it->second);
                #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
                _geoCacheLog << log4cpp::Priority::DEBUG
                    << " ...entry \"" << naming()[scoreID] << "\" ignored"
                       " since no corresponding geom.cache entry found.";
                #endif
                continue;
            }
            if(gfTP) {
                // track point exists, but detID has changed -> add TP
                // to the track
                assert(!std::isnan(gfTPSortPar));
                gfTPs.emplace(gfTPSortPar, gfTP);
                gfTPSortPar = std::nan("0");
                #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
                _geoCacheLog << log4cpp::Priority::DEBUG
                     << " ...added GenFit2 track point " << (void *) gfTP << ",";
                #endif
            }
            gfTP = new genfit::TrackPoint();
            #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
            _geoCacheLog << log4cpp::Priority::DEBUG
                 << " ...new GenFit2 track point " << (void *) gfTP << " created.";
            #endif
        }
        assert(_detGeoCache.end() != detCacheIt);
        mem::Ref<event::ScoreFitInfo> sfiRef = scoreP.second;
        // Create GenFit2 measurement and put it into GenFit2 track
        // point (keep eye on sorting parameter)
        try {
            float gfTPSortPar_new = std::nan("0");
            genfit::AbsMeasurement * msrmntPtr
                = detCacheIt->second->new_measurement(*sfiRef->score, gfTPSortPar_new);
            _msrmnts2sfi.emplace( msrmntPtr
                                , std::pair<DetID, mem::Ref<event::ScoreFitInfo> >(
                                            scoreID, sfiRef));
            // ^^^ NOTE: to be cleared by user code
            gfTP->addRawMeasurement(msrmntPtr);
            #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
            _geoCacheLog << log4cpp::Priority::DEBUG
                         << " ...created GenFit2 measurement for score \""
                         << naming()[scoreID] << "\" and added it"
                         " to GenFit2 track point " << (void *) gfTP << ".";
            #endif
            if( (!std::isnan(gfTPSortPar))
             && fabs(gfTPSortPar - gfTPSortPar_new) >= 1e-6) {
                _geoCacheLog << log4cpp::Priority::WARN
                     << "Sorting parameter has changed for measurement"
                        " within the same detector entity.";
            }
            gfTPSortPar = gfTPSortPar_new;
        } catch( std::exception & e ) {
            _geoCacheLog << log4cpp::Priority::ERROR
                  << "While converting track score \""
                  << naming()[scoreID] << "\" into GenFit2's"
                     " measurement, an error occured: " << e.what();
            throw;
        }
        // Create "idle" measurement for drift-like detectors if need
        if( _collectRT && sfiRef->score->driftFts ) {
            // for drift detectors, there must be a supplementary idle
            // (not participating directly) raw measurement
            // specifically to collect R(T).
            assert(dynamic_cast<WiredPlane*>(detCacheIt->second));
            // ^^^ TODO: valid, unitl score having drift features
            //     always correspond to `WiredPlane` cache entry. If
            //     and when we'll get other types of drift detectors
            //     this may not be true (consider interim inheritance
            //     class then)
            auto gce = static_cast<WiredPlane*>(detCacheIt->second);
            gce->ignoreDriftFeatures = true;
            genfit::AbsMeasurement * idleMeasurement = nullptr;
            try {
                float ignored;
                idleMeasurement =
                    gce->new_measurement(*sfiRef->score, ignored);
            } catch(...) {
                gce->ignoreDriftFeatures = false;
                throw;
            }
            gce->ignoreDriftFeatures = false;
            // memorize this idle measurement for further usage
            gce->scores2idleMsrmnts.emplace(sfiRef->score.get(), idleMeasurement);
        }
    }  // iter over scores in track
    if(gfTP && gfTP->hasRawMeasurements()) {
        assert(!std::isnan(gfTPSortPar));
        gfTPs.emplace(gfTPSortPar, gfTP);
        gfTPSortPar = std::nan("0");
        #if defined(GF_DEBUG_LOG_ENABLED) && GF_DEBUG_LOG_ENABLED
        _geoCacheLog << log4cpp::Priority::DEBUG
             << " ...added GenFit2 track point " << (void *) gfTP
             << " (assumed to be the last one in track)";
        #endif
    }
}


}  // namespace ::na64dp::handlers::aux

//
// GenFit2 tracking handler implementation

GenFit2_FitTrack::GenFit2_FitTrack( calib::Manager & cmgr
                                  , const std::string & sel
                                  , size_t nHitsLimit
                                  , CopyMode copyMode
                                  , const std::string & unbiasedFor
                                  , log4cpp::Category & loggingCategory
                                  , bool collectRT
                                  , genfit::AbsFitter * fitter
                                  , bool dispatch2GFEvDisplay
                                  , const std::string & debugOutput
                                  ) : AbstractHitHandler<event::Track>(cmgr, sel, loggingCategory )
                                    , aux::GenfitGeometryCache( cmgr
                                                              , collectRT
                                                              , loggingCategory
                                                              )
                                    , _fitter(fitter)
                                    , _rootSetupExtension(Extensions::self()["ROOTSetupGeometry"].as<util::ROOTSetupGeometry>())
                                    , _nHitsLimit(nHitsLimit)
                                    , _unbiasedSelector(unbiasedFor == "none" ? "" : unbiasedFor, nullptr)
                                    , _collectUnbiasedForAll(unbiasedFor == "all" ? true : false)
                                    , _doCollectUnbiasedInt(unbiasedFor.empty() || unbiasedFor == "none" ? 0 : -1)
                                    , _copyMode(copyMode)
                                    , _dispatch2GFEvDisplay(dispatch2GFEvDisplay)
                                    , _nTracksConsidered(0)
                                    , _nTracksFitted(0)
                                    , _nTracksConvergedFully(0)
                                    , _nTrackConvergedPartially(0)
                                    , _nTracksDidNotConverged(0)
                                    {
    cmgr.subscribe<calib::Placements>(*this, "default");
    _log << log4cpp::Priority::DEBUG
         << "Handler " << this << " initialized.";
}

void
GenFit2_FitTrack::handle_update( const nameutils::DetectorNaming & nm ) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    if( _unbiasedSelector.first.empty() || _collectUnbiasedForAll ) {
        return;
    }
    assert( _unbiasedSelector.first != "none" && _unbiasedSelector.first != "all" );
    if( !_unbiasedSelector.second ) {
        _unbiasedSelector.second = new DetSelect( _unbiasedSelector.first.c_str()
                                                , util::gDetIDGetters
                                                , nm
                                                );
    } else {
        _unbiasedSelector.second->reset_context(nm);
    }
}

std::string
GenFit2_FitTrack::get_det_name_for_score(mem::Ref<event::TrackScore> score) const {
    char detNameStr[64] = "???";
    // find score pair for diagnostics
    for( auto & scorePair : _current_event().trackScores ) {
        if(scorePair.second == score) {
            snprintf( detNameStr, sizeof(detNameStr)
                    , "\"%s\" (%#x)"
                    , naming()[scorePair.first].c_str()
                    , scorePair.first.id
                    );
        }
    }
    return detNameStr;
}

genfit::Track *
GenFit2_FitTrack::to_gf_track(const event::Track & track) const {
    auto & L = const_cast<GenFit2_FitTrack*>(this)->log();
    if( track.empty() ) return nullptr;
    // Create GenFit2 track copying parameters from NA64SW track and
    // populate it with track points
    // TODO: propDir -- (-1, 0, 1) -> (backward, auto, forward)
    auto mRep = new genfit::RKTrackRep( track.pdg /*, propDir=0*/ );
    assert(track.fitInfo);

    auto gfTrackPtr = new genfit::Track(mRep
            , TVector3(track.fitInfo->positionSeed)
            , TVector3(track.fitInfo->momentumSeed)
            );

    TMatrixDSym cov(6);
    {
        for(int i = 0; i < 6; ++i) {
            cov(i, i) = track.fitInfo->covarSeed[i];
        }
        gfTrackPtr->setCovSeed(cov);
    }
    if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        oss << "  seeds:" << std::endl
               << "    PDG: " << track.pdg << std::endl
               << "    position: {" << track.fitInfo->positionSeed[0] << ", "
                                    << track.fitInfo->positionSeed[1] << ", "
                                    << track.fitInfo->positionSeed[2] << "}"
                                    << std::endl
               << "    momentum: {" << track.fitInfo->momentumSeed[0] << ", "
                                    << track.fitInfo->momentumSeed[1] << ", "
                                    << track.fitInfo->momentumSeed[2] << "}"
                                    << std::endl
               << "    covariance matrix:" << std::endl;
               ;
        for(int i = 0; i < 6; ++i) {
            oss << "      ";
            for(int j = 0; j < 6; ++j) {
                oss << cov(j, i) << "\t";
            }
            oss << std::endl;
        }
        L << log4cpp::Priority::DEBUG << oss.str();
    }

    // Populate track points
    size_t nPoint = 0;
    std::multimap<float, genfit::TrackPoint *> gfTPs;
    create_GF2_tps(gfTPs, track);
    if( gfTPs.empty() ) {
        delete gfTrackPtr;
        return nullptr;
    }
    for(auto gfTP : gfTPs) {
        gfTrackPtr->insertPoint(gfTP.second);
        if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
            std::ostringstream oss;
            oss << "  track point #" << nPoint << ", hits:" << std::endl;
            const auto & rm = gfTP.second->getRawMeasurements();
            for(genfit::AbsMeasurement * m : rm) {
                DetID_t did_ = m->getDetId();
                did_ <<= na64dp::aux::gDetNumOffset;
                
                genfit::PlanarMeasurement * plM = dynamic_cast<genfit::PlanarMeasurement *>(m);
                if(plM) {
                    did_ |= ((DetID_t) plM->getPlaneId());
                }
                #if defined(USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS) && USE_NEW_GENFIT_WIRE_MEASUREMENT_CLASS
                genfit::WireMeasurementNew * wM
                    = dynamic_cast<genfit::WireMeasurementNew *>(m);
                #else
                genfit::WireMeasurement * wM
                    = dynamic_cast<genfit::WireMeasurement *>(m);
                #endif
                if(wM) {
                    // GenFit2 does not offer plane ID for `WireMeasurementNew`,
                    // but we wrote the full NA64sw's plane ID to GF's plane ID
                    did_ = ((DetID_t) wM->getDetId());
                }
                // ^^^ TODO: add here other measurement types (perhaps, based
                //     on station key to avoid dynamic_cast<>()?)
                DetID did(did_);

                oss << "    " << (void*) m << ", " << m->getDim() << "D msrmnt GF's hit ID=" << m->getHitId()
                       << " of GF det ID=" << m->getDetId() /*<< "(" << naming()[did] << ")"*/
                       << " at {"
                       << m->getRawHitCoords()(0) << " (cov[0,0]=" << m->getRawHitCov()(0, 0) << ")"
                       ;
                if( m->getDim() > 1 )
                    oss << ", " << m->getRawHitCoords()(1) << " (cov[1,1]=" << m->getRawHitCov()(1, 1) << ")";
                if( m->getDim() > 2 )
                    oss << ", " << m->getRawHitCoords()(2) << " (cov[2,2]=" << m->getRawHitCov()(2, 2) << ")";
                oss << "}" << std::endl;
            }
            ++nPoint;
            L << log4cpp::Priority::DEBUG << oss.str();
        }
    }

    return gfTrackPtr;
}

void
GenFit2_FitTrack::copy_from_fitted_track( const genfit::Track & gfTrack
                                        , event::Track & track) const {
    auto & L = const_cast<GenFit2_FitTrack*>(this)->log();
    // Following table reflects cases of `_copyMode` and fit status. The idea is
    // that user may restrict the track fit info copying strategy depending on
    // fit convergence.
    //
    // FS = 0x1 | ( isFitConverged(false) ? 0x2 ) | ( isFitConverged(true) ? 0x4 )
    // doCopy = CM & FS
    //
    //      copy mode    |  always      | partially         | fully     | never
    //  fit converged?   | 0x1          | 0x2               | 0x4       | 0x0
    // ------------------+--------------+-------------------+-----------+------
    //     no at all 0x1 | yes 0x1      | no  0x0           | no  0x0   | no
    //     partially 0x3 | yes 0x1      | yes 0x2           | no  0x0   | no
    //     fully     0x7 | yes 0x1      | yes 0x2           | yes 0x4   | no

    assert(gfTrack.hasFitStatus());
    if(!gfTrack.getFitStatus()->isFitted()) {
        return;  // TODO: report on error?
    }
    ++_nTracksFitted;
    const genfit::FitStatus & fs = *gfTrack.getFitStatus();

    {
        genfit::MeasuredStateOnPlane lastState;
        bool gotState = false;
        assert(gfTrack.getNumPointsWithMeasurement());  // what did we fit otherwise?
        for( unsigned int nGFPt = gfTrack.getNumPointsWithMeasurement() - 1
           ; nGFPt
           ; --nGFPt ) {
            try {
                lastState = gfTrack.getFittedState(nGFPt);
            } catch(genfit::Exception & e) {
                L << log4cpp::Priority::ERROR << "While getting fitted state at"
                    " point #" << nGFPt << ": "
                    << e.what();  // XXX?
                continue;
            }
            gotState = true;
            break;
        }
        if(!gotState) {
            L << log4cpp::Priority::ERROR
              << "At event " << _current_event().id
                          << ", fitted track #" << _nTracksFitted << " ("
                          << (fs.isFitConverged(true) ? "fully converged"
                                  : (fs.isFitConverged(false) ? "partially converged"
                                      : "not converged" ))
                          << ") GenFit unable to get any fitted state, no track"
                          " momentum estimation available.";
            util::reset(track.momentum);
        } else {
            track.momentum = lastState.getMomMag();
        }
        #if 1
        #else
        genfit::TrackPoint* tp = fitTrack->getPointWithMeasurementAndFitterInfo(0, rep);
        genfit::KalmanFittedStateOnPlane kfsop(*(static_cast<genfit::KalmanFitterInfo*>(
                        tp->getFitterInfo(rep))->getBackwardUpdate()));
        const double charge = TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.);
        track.momentum = charge/kfsop.GetState()[0];
        #endif
    }
    if(fs.isFitConverged(true)) {
        ++_nTracksConvergedFully;
    } else if(fs.isFitConverged(false)) {
        ++_nTrackConvergedPartially;
    } else {
        ++_nTracksDidNotConverged;
    }

    int FS = 0x1
           | (fs.isFitConverged(false) ? 0x2       : 0x0)
           | (fs.isFitConverged(true)  ? 0x2 | 0x4 : 0x0)
           ;
    assert(track.fitInfo);  // todo: shall we create it otherwise?
    if( ! (FS&_copyMode) ) {
        if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
            L << log4cpp::Priority::DEBUG
                   << "Won't copy the track (isConvergedFully="
                   << (fs.isFitConverged(true) ? "yes" : "no" )
                   << ", isConvergedPartially="
                   << (fs.isFitConverged(false) ? "yes" : "no" )
                   << ", copyPolicy="
                   << (int) _copyMode << ")";
        }
        util::reset( *track.fitInfo );
        return;
    } else {
        if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
            L << log4cpp::Priority::DEBUG
                   << "Copying the track (isConvergedFully="
                   << (fs.isFitConverged(true) ? "yes" : "no" )
                   << ", isConvergedPartially="
                   << (fs.isFitConverged(false) ? "yes" : "no" )
                   << ", copyPolicy="
                   << (int) _copyMode << ")";
        }
    }
    track.fitInfo->chi2 = fs.getChi2();
    track.fitInfo->ndf  = fs.getNdf();
    track.fitInfo->pval = fs.getPVal();

    genfit::AbsTrackRep * trackRepPtr = gfTrack.getCardinalRep();
    L << log4cpp::Priority::DEBUG
      << "In track: " << gfTrack.getNumPoints() << " pts overall, "
      << gfTrack.getNumPointsWithMeasurement() << " pts w msrmnts."
      ;
    for( unsigned int nGFPt = 0
       ; nGFPt < gfTrack.getNumPointsWithMeasurement()
       ; ++nGFPt ) {
        genfit::TrackPoint * gfTP
				= gfTrack.getPointWithFitterInfo(nGFPt, trackRepPtr);
        if((!gfTP) || !gfTP->hasFitterInfo(trackRepPtr) ) continue;
        // ^^^ point does not participate in this rep
        // Retrieve fitter information for this GF2 track point
        genfit::KalmanFitterInfo * tpFitInfo = gfTP->getKalmanFitterInfo(trackRepPtr);
        assert(tpFitInfo);
        // getting a smoothed state may be expensive, so
        // it is lazy here (we might not need it below, if detector is not
        // booked for [un]biased residuals)
        const genfit::MeasuredStateOnPlane * smoothedStatePtr = nullptr;
        // Weights vector (for DAF only)
        //const std::vector<double> ws = tpFitInfo->getWeights();
        // Compute residuals for every individual track's score.
        // This procedure is pretty similar to one provided by default GenFit2's
        // KalmanFitterInfo::getResidual() except for *measurement* under
        // consideration. Here the raw measurements are considered while
        // native (GenFit2's) operates with `MeasurementOnPlane` which is
        // track's state on plane taken into consideration by the fitting.
        for( int isBiased = 1; isBiased > _doCollectUnbiasedInt; --isBiased  ) {
            if( (!isBiased) && (!_doCollectUnbiasedInt) ) continue;
            // for every raw measurement:
            const auto rawMeasurements = gfTP->getRawMeasurements();
            for( const genfit::AbsMeasurement * absRawMeasurement : rawMeasurements ) {
                auto scoreIt = _msrmnts2sfi.find(absRawMeasurement);
                if(_msrmnts2sfi.end() == scoreIt) {
                    NA64DP_RUNTIME_ERROR("Integrity error: no score found for"
                            " measurement." );
                    // ^^^ todo: details
                }
                event::TrackScore & score = *(scoreIt->second.second->score);
                auto scoreID = scoreIt->second.first;
                auto geoCacheEntry = _detGeoCache.find(scoreID);
                assert(geoCacheEntry != _detGeoCache.end());
                if( (!_collectUnbiasedForAll) && (!isBiased) && _unbiasedSelector.second ) {
                    if(! _unbiasedSelector.second->matches( scoreIt->second.first )) {
                        util::reset(scoreIt->second.second->lRUErr);
                        continue;  // do not collect unbiased for this detector
                    }
                }
                if( !smoothedStatePtr ) {
                    // - retrieve "smoothed" track point (according to track's fit)
                    try {
                        smoothedStatePtr = &tpFitInfo->getFittedState((bool) isBiased);
                    } catch( genfit::Exception & e ) {
                        L << log4cpp::Priority::ERROR
                          << "At event " << _current_event().id
                          << ", fitted track #" << _nTracksFitted << " ("
                          << (fs.isFitConverged(true) ? "fully converged"
                                  : (fs.isFitConverged(false) ? "partially converged"
                                      : "not converged" ))
                          << ") GenFit unable to get "
                          << (isBiased ? "biased" : "unbiased")
                          << " fitted state on plane "
                          " \"" << geoCacheEntry->second->name
                          << "\" due to an error: " << e.what()
                          // TODO: raw measurement
                          ;
                        continue;
                    }
                }
                // Construct "measurements on plane" for raw measurements wrt
                // fitted state's plane. Note that multiple
                // measurements-on-plane can be returned here for a single raw
                // measurement as it can be specified by raw measurement
                // nature: for instance, for drift detectors it will be two
                // measurements-on-plane because of L/R ambiguity. This must
                // be properly treated by corresponding `iDetGeoEntry` subclass
                std::vector<genfit::MeasurementOnPlane*> msops;
                msops = absRawMeasurement->constructMeasurementsOnPlane(*smoothedStatePtr);
                geoCacheEntry->second->set_residuals( msops
                        , *scoreIt->second.second
                        , *smoothedStatePtr
                        , (bool) isBiased
                        );
                // R(T), related to retrieved residuals
                if( _collectRT && score.driftFts && isBiased ) {
                    // for drift detectors, there must be a supplementary idle
                    // (not participating directly) raw measurement
                    // specifically to collect R(T) created previously.
                    // NOTE: `isBiased` check in this clause done to avoid
                    // repition (we do R(T) on biased=true as it always enabled).
                    //
                    // TODO: downcasting, see comment note in idle measurement
                    // creation code
                    assert(dynamic_cast<WiredPlane*>(geoCacheEntry->second));
                    auto gce = static_cast<WiredPlane*>(geoCacheEntry->second);
                    auto it = gce->scores2idleMsrmnts.find(&score);
                    if( it != gce->scores2idleMsrmnts.end() ) {
                        msops = it->second->constructMeasurementsOnPlane(*smoothedStatePtr);
                        gce->ignoreDriftFeatures = true;
                        try {
                            geoCacheEntry->second->set_residuals( msops
                                    , *scoreIt->second.second
                                    , *smoothedStatePtr
                                    , (bool) false  // always "unbiased" (idle msmnt)
                                );
                        } catch(...) {
                            gce->ignoreDriftFeatures = false;
                            throw;
                        }
                        gce->ignoreDriftFeatures = false;
                    }
                }  // collect R(T)
            }  // for every raw measurement
        }  // biased/unbiased
    }  // iterate over (GF) track points in a track
}

bool
GenFit2_FitTrack::process_hit( EventID eventID
                             , TrackID trackID
                             , event::Track & track ) {
    auto & L = const_cast<GenFit2_FitTrack*>(this)->log();
    if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
        L << log4cpp::Priority::DEBUG
          << "Processing track " << trackID.code << " with "
          << track.size() << " hits:";
    }
    if( track.size() < _nHitsLimit )
        return true;

    genfit::Track * gfTrackPtr = to_gf_track(track);
    if(!gfTrackPtr) {
        // empty track, nothing to do
        // This case is a bit redundant since we've already discriminated
        // empty tracks above, however subclasses may override
        // `to_gf_track()` method in some way and take their own decision.
        return true;
    }
    // If fitter is present, fit the track and copy results
    if(_fitter) {
        try {
            ++_nTracksConsidered;
            _fitter->processTrack(gfTrackPtr);
            gfTrackPtr->checkConsistency();
        } catch( genfit::Exception & err ) {
            log().error( "Failed to fit track %#x due to an"
                         " error (event %s): %s"
                       , trackID.code
                       , eventID.to_str().c_str()
                       , err.what()
                    );
            // NOTE: we do not remove track from collection, hoping that it
            // will still be possible to fit it with welded one
        }
        gfTrackPtr->determineCardinalRep();  // xxx?
        copy_from_fitted_track(*gfTrackPtr, track);
    }
    if(EventDisplay::exists() && _dispatch2GFEvDisplay
    #if 0
     && (!std::isnan(track.momentum) && track.momentum > 150)
    #endif
      ) {

        EventDisplay::getInstance()->addEvent(gfTrackPtr);
    }
    else
        delete gfTrackPtr;
    return true;
}

AbstractHandler::ProcRes
GenFit2_FitTrack::process_event(event::Event & e) {
    auto & L = const_cast<GenFit2_FitTrack*>(this)->log();

    assert(_rootSetupExtension.cache());
    _rootSetupExtension.cache()->recache_setup();
    if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
        L << log4cpp::Priority::DEBUG
          << "                * * *    event " << e.id
          << "    * * *";
    }
    ProcRes pr = AbstractHitHandler<event::Track>::process_event(e);
    for( auto & p : _detGeoCache ) p.second->reset();
    _msrmnts2sfi.clear();
    return pr;
}

void
GenFit2_FitTrack::finalize() {
    _log << log4cpp::Priority::INFO
        << _nTracksConsidered << " tracks considered during lifetime, "
        << _nTracksFitted << " of them fitted; fully converged "
        << _nTracksConvergedFully << ", partially converged "
        << _nTrackConvergedPartially << ", did not converged "
        << _nTracksDidNotConverged << ".";
    if( _faultyNames.empty() ) return;
    std::ostringstream ofs;
    bool isFirst = true;
    for(const auto & p : _faultyNames) {
        ofs << (isFirst ? "" : ", ") << p.first << " (" << p.second << " times)";
        isFirst = false;
    }
    _log << log4cpp::Priority::WARN
         << "Following detector names were not found in geometry cache and"
            " did not participate in track fitting: " << ofs.str();
}

GenFit2_FitTrack::~GenFit2_FitTrack() {
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( GenFit2_FitTrack, cmgr, cfg,
        "Uses GenFit2 to fit events tracks" ) {
    // Instantiate the fitter
    genfit::AbsFitter * fitterPtr = nullptr;
    if(cfg["fitter"]) {
        fitterPtr = na64dp::VCtr::self().make<genfit::AbsFitter>(
                cfg["fitter"]["_type"].as<std::string>(), cmgr, cfg["fitter"] );
    }  // todo: otherwise some default fitter?
    const std::string copyIf = cfg["copyIf"] ? cfg["copyIf"].as<std::string>() : "convergedFully";
    na64dp::handlers::GenFit2_FitTrack::CopyMode copyMode;
    if( copyIf == "noCopy" ) {
        copyMode = na64dp::handlers::GenFit2_FitTrack::kNoCopy;
    } else if( copyIf == "convergedFully" ) {
        copyMode = na64dp::handlers::GenFit2_FitTrack::kCopyIfConvergedFully;
    } else if( copyIf == "convergedPartially" ) {
        copyMode = na64dp::handlers::GenFit2_FitTrack::kCopyIfConvergedPartially;
    } else if( copyIf == "always" ) {
        copyMode = na64dp::handlers::GenFit2_FitTrack::kCopyAlways;
    } else {
        NA64DP_RUNTIME_ERROR( "Bad value for \"copyIf\" parameter: \"%s\"."
                " Possible values are: \"always\", \"never\","
                " \"convergedFully\", \"convergedPartially\"."
                , copyIf.c_str() );
    }
    return new na64dp::handlers::GenFit2_FitTrack(cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["minHits"] ? cfg["minHits"].as<size_t>() : 2
            , copyMode
            //, cfg["collectUnbiasedResiduals"] ? cfg["collectUnbiasedResiduals"].as<bool>() : true
            , cfg["collectUnbiasedResidualsFor"]
                    ? cfg["collectUnbiasedResidualsFor"].as<std::string>()
                    : "none"
            , ::na64dp::aux::get_logging_cat(cfg)
            , cfg["collectRT"]
                    ? cfg["collectRT"].as<bool>()
                    : false
            , fitterPtr
            , cfg["showOnGenFitEventDisplay"] ? cfg["showOnGenFitEventDisplay"].as<bool>() : false
            // ...
            );
}

