#include "eventDisplay.hh"
#include "na64app/extension.hh"
#include "na64util/streambuf-redirect.hh"

namespace na64dp {

/**\brief Runtime extension to show event display
 *
 * ...
 * */
class GenFit2 : public iRuntimeExtension {
private:
    bool _enableDisplay  ///< turns on event display
       , _handleStreams  ///< turns on GF2 output handling
       ;
    /// Ptr to event display, if enabled
    EventDisplay * _ptr;
    /// Streambufs to handle output, used only if `_handleStreams`
    util::Log4Cpp_StreambufRedirect * _rdbufDbg
                                  , * _rdbufOut
                                  , * _rdbufErr
                                  ;
    /// Name of logging category, used only if `_handleStreams`
    std::string _logCatName;
public:
    GenFit2();
    void init( calib::Manager & cmgr
             , iEvProcInfo *) final;
    void set_parameter( const std::string & pn
                      , const std::string & pv
                      ) final;
    void finalize() final;

};  // class GenFit2

GenFit2::GenFit2()
    : _enableDisplay(false), _handleStreams(true)
    , _ptr(nullptr)
    , _rdbufDbg(nullptr), _rdbufOut(nullptr), _rdbufErr(nullptr)
    , _logCatName("genfit")
    {}

void
GenFit2::init( calib::Manager & cmgr, iEvProcInfo *) {
    if(_handleStreams) {  // create streambufs and handle GF2's output
        _rdbufDbg = new util::Log4Cpp_StreambufRedirect(
                log4cpp::Category::getInstance(_logCatName), log4cpp::Priority::DEBUG);
        _rdbufErr = new util::Log4Cpp_StreambufRedirect(
                log4cpp::Category::getInstance(_logCatName), log4cpp::Priority::WARN);
        _rdbufOut = new util::Log4Cpp_StreambufRedirect(
                log4cpp::Category::getInstance(_logCatName), log4cpp::Priority::INFO);
        // set buffers
        genfit::debugOut.rdbuf(_rdbufDbg);
        genfit::errorOut.rdbuf(_rdbufErr);
        genfit::printOut.rdbuf(_rdbufOut);
    }
    if(_enableDisplay) {  // turn on event display
        EventDisplay::getInstance(&cmgr);
    }
}

void
GenFit2::set_parameter( const std::string & pn
                      , const std::string & pv
                      ) {
    if(pn == "handleStreams") {
        _handleStreams = util::str_to_bool(pv);
    } else if(pn == "enableEventDisplay") {
        _enableDisplay = util::str_to_bool(pv);
    } else {
        NA64DP_RUNTIME_ERROR("Unsupported parameter (option) name for"
                " extension `GenFit2': \"%s\"", pn.c_str() );
    }
}

void
GenFit2::finalize() {
    if(_enableDisplay) {
        log4cpp::Category::getInstance("GenFit2") << log4cpp::Priority::INFO
            << "Entering EventDisplay event processing...";
        EventDisplay::getInstance()->open();
    }
}

REGISTER_EXTENSION( GenFit2 );

}  // namespace na4dp

