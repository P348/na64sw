/**\file
 * \brief Defines few simple magnetic field instances
 * */

#include "na64sw-config.h"

#include <TVector3.h>
#include <yaml-cpp/yaml.h>

#include "na64calib/placements.hh"
#include "eventDisplay.hh"  // used to creat m.field decorations

#include "stdMagFields.hh"

#if defined(TrackingTools_FOUND) && TrackingTools_FOUND
#   include "trackingTools/detector/MagneticFieldMap.hh"
#endif

namespace na64dp {

/**\brief A magnetic field defined in rectangular volume with edges parralel to
 *        the axes
 *
 * Usage:
 * \code{.yaml}
 *       _type: UniformParaxialBox
 *       from: [...]
 *       to: [...]
 *       B: [...]
 * \endcode
 *
 * Pretty simplistic implementation providing constant field within a
 * volumetric box with fixed coordinates. No fadeout/fringe field effects.
 * Only parallel to axes positioning is supported (i.e. no rotations)
 * */
class UniformParaxialBox : public iAbstractGenFitMField {
protected:
    const std::string _label;
    TVector3 _min, _max, _b;
public:
    UniformParaxialBox( const std::string & label
                      , const TVector3 & min_
                      , const TVector3 & max_
                      , const TVector3 & b_
                      ) : _label(label), _min(min_), _max(max_), _b(b_)
                      {}
    /// Returns constant vector if within `min` and `max`, zero vector otherwise.
    TVector3 get(const TVector3 & pos) const override;
    /// Returns one bounding box
    FieldFramesByPath labeled_field_frames() const override {
        FieldFramesByPath r;
        util::Vec3 bMin{{(Float_t)_min(0), (Float_t)_min(1), (Float_t)_min(2)}}
                 , bMax{{(Float_t)_max(0), (Float_t)_max(1), (Float_t)_max(2)}}
                 , o = (bMin + bMax)/2
                 , u{{ (Float_t) (_max(0) - o.r[0]), 0, 0 }}
                 , v{{ 0, (Float_t) (_max(1) - o.r[1]), 0 }}
                 , w{{ 0, 0, (Float_t) (_max(2) - o.r[2]) }}
                 ;
        util::Transformation t(o, u, v, w, 0, 0, 0);

        std::vector<util::Transformation> transList;
        transList.push_back(t);
        r.emplace(_label, transList);
        return r;
    }
};  // class ConstFieldBox

TVector3
UniformParaxialBox::get(const TVector3 & p) const {
    for(int i = 0; i < 3; ++i) {
        if( p[i] < _min[i] || p[i] > _max[i] )
            return TVector3(0, 0, 0);
    }
    return _b;
}

REGISTER_MAGNETIC_FIELD( UniformParaxialBox, cmgr, cfg
                       , "An uniform magnetic field defined in rectangular"
                         " volume, parallel to axes."
                       ) {
    if(na64dp::EventDisplay::exists()) {
        // TODO: create decorations
    }
    return new UniformParaxialBox( cfg["label"].as<std::string>()
                                 , TVector3( cfg["from"][0].as<float>()
                                           , cfg["from"][1].as<float>()
                                           , cfg["from"][2].as<float>() )
                                 , TVector3( cfg["to"][0].as<float>()
                                           , cfg["to"][1].as<float>()
                                           , cfg["to"][2].as<float>() )
                                 , TVector3( cfg["B"][0].as<float>()
                                           , cfg["B"][1].as<float>()
                                           , cfg["B"][2].as<float>() )
                                 );
}

/**\brief Sums up group of fields.
 *
 * Usage:
 * \code{.yaml}
 *       _type: Sum
 *       fields:
 *          - _type: ...
 *            ...
 *          - _type: ...
 *            ...
 * \endcode
 *
 * Sums up contribution of few magnetic fields.
 *
 * \todo full support for own transformation
 * */
class Group : public iAbstractGenFitMField
            , public std::vector<const iAbstractGenFitMField *>
            {
private:
    /// Label of the group
    const std::string _label;
    /// Transformation applied for the group
    util::Transformation _t;
public:
    Group( const std::string & label_
         , const std::list<iAbstractGenFitMField *> & fs )
        : std::vector<const iAbstractGenFitMField *>(fs.begin(), fs.end())
        , _label(label_)
        {}
    /// Returns sum
    TVector3 get(const TVector3 & pos) const override;
    /// Returns transformations for every field item in the group
    FieldFramesByPath labeled_field_frames() const override;
};

TVector3
Group::get(const TVector3 & p) const {
    TVector3 b(0, 0, 0);
    for(auto pp: *this) {
        b += pp->get(p);
    }
    return b;
}

iAbstractGenFitMField::FieldFramesByPath
Group::labeled_field_frames() const {
    FieldFramesByPath r;
    for(auto mfPtr : *this) {
        const auto ts = mfPtr->labeled_field_frames();
        for(const auto & item : ts) {
            auto tsVecCpy = item.second;
            tsVecCpy.push_back(_t);
            r.emplace( _label + "@" + item.first, tsVecCpy);
        }
    }
    return r;
}

REGISTER_MAGNETIC_FIELD( Group, cmgr, cfg
                       , "A group of magnetic field that'll be summed up") {
    std::list<iAbstractGenFitMField *> fields;
    for(auto cField : cfg["fields"]) {
        iAbstractGenFitMField * fieldPtr = VCtr::self().make<iAbstractGenFitMField>(
                cField["_type"].as<std::string>(), cmgr, cField );
        fields.push_back(fieldPtr);
    }
    // NOTE: groups does not create decorations
    return new Group( cfg["label"].as<std::string>()
                    , fields);
}

#if defined(TrackingTools_FOUND) && TrackingTools_FOUND

/**\brief Interface to `TrackingTools::MagneticFieldMap`
 *
 * Interfaces utils::MagneticFieldMap instance of TrackingTools to
 * `genfit::AbsBField`.
 *
 * GenFit2 (`genfit::AbsBField`) uses cm/kGauss system of units.
 *
 * \note `position` provided by the node considered of the same units as given
 *       in the field map's file.
 * \note `MagneticFieldMap` does not provide getters for offset/rotations, so
 *       we keep its copy within this descendant class. Unit transform is NOT
 *       included into the affine transform, and B vector is also transformed
 *       by `utils::MagneticFieldMap`.
 * \todo Magnetic field currents measurement calib info
 * */
class TrackingTools_FieldMap : public iAbstractGenFitMField
                             , public utils::MagneticFieldMap
                             //, public calib::Handle<MagnetsCurrent> // TODO?
                             {
protected:
    /// Length untis affects in/out conversion for length units; see note in class doc
    float _lengthUnitsFactor;
    /// Field scale affects the value returned by the instance.
    float _fieldScaleFactor;
    /// 
    util::Transformation _t;
public:
    /// Constructs the named instance
    TrackingTools_FieldMap( const std::string & name
                          , const util::Transformation & t
                          , float fieldScaleFactor=1.0
                          , float lengthUnitsFactor=1.0
                          )
        : utils::MagneticFieldMap(name)
        , _lengthUnitsFactor(1./lengthUnitsFactor)
        // ^^^ sic! factor used as r_map = f_length * r_genfit, i.e. from
        //     GenFit's units to fieldmap units, while:
        , _fieldScaleFactor(fieldScaleFactor)
        // ^^^ is used as B_genfit = f_fieldmap * r_fieldmap, i.e. from map's
        //     units to GenFit's units
        , _t(t)
        {}
    /// Returns field value
    ///
    /// Performs in/out conversion according to unit scaling factors.
    /// `AbsBField::get()` is always called with spatial vector in `[cm]`. We
    /// divide it by length unit scaling factor to get the field map's units.
    /// Returned field value is multiplied by field scaling factor.
    TVector3 get(const TVector3 & pos_) const override {
        const TVector3 pos = _lengthUnitsFactor*pos_;
        // recalc position in map's units.
        if( ! utils::MagneticFieldMap
            ::CheckRange(pos) ) return {0, 0, 0};
        return utils::MagneticFieldMap
            ::InterpolateFieldValue(pos)*_fieldScaleFactor;
    }

    /// Returns bounding box defining boundaries of the map in use
    FieldFramesByPath labeled_field_frames() const override {
        FieldFramesByPath r;
        std::vector<util::Transformation> transList;
        transList.push_back(_t);
        r.emplace(GetName(), transList);
        return r;
    }
};

REGISTER_MAGNETIC_FIELD( TrackingTools_FieldMap, cmgr, cfg
        , "Magnetic field map described within an ASCII file"
          " implemented by TrackingTools package." ) {
    #ifdef NA64_TT_FIELDMAP_DIR
    setenv("NA64_TT_FIELDMAP_DIR", NA64_TT_FIELDMAP_DIR, 0);
    #endif
    std::string path = cfg["mapURI"].as<std::string>();
    // NOTE: in 2022-mu a scheme was used where fieldmap had magnet's name in
    // the filename. To keep this useful feature, we impose a string
    // interpolation context with `{name}` placeholder. Although it may be no
    // point in config itself, it might be useful for external tools.
    na64dp::util::StrSubstDict ctx;
    if( cfg["name"] )       ctx["name"]      = cfg["name"].as<std::string>();
    if( cfg["techLabel"] )  ctx["techLabel"] = cfg["techLabel"].as<std::string>();

    // substitute context ignoring incomplete placeholders (may be shell
    // variables)
    path = na64dp::util::str_subst(path, ctx, false);
    // substitute shell variables
    path = na64dp::util::expand_name(path);
    // now require completion
    path = na64dp::util::str_subst(path, ctx, true);

    if( 0 != access(path.c_str(), F_OK) ) {
        NA64DP_RUNTIME_ERROR( "File \"%s\" does not exist.", path.c_str() );
    }
    float fieldFactor = cfg["fieldUnits"]
                      ? util::Units::get_units_conversion_factor( cfg["fieldUnits"].as<std::string>()
                                                                , "mag.induction" )
                      : 1.
        , lengthFactor = cfg["lengthUnits"]
                       ? util::Units::get_units_conversion_factor( cfg["lengthUnits"].as<std::string>()
                                                                 , "length"
                                                                 )
                       : 1.
        ;
    // instantiate proper transformation for magnetic field
    auto posVec = cfg["position"].as<std::vector<float>>()  // keep orig units
       , rotVec = cfg["rotation"].as<std::vector<float>>()
       ;
    util::Vec3 o;
    for(int i = 0; i < 3; ++i) {
        o.r[i] = posVec[i];
    }
    util::Transformation t( o, {{1, 0, 0}}, {{0, 1, 0}}, {{0, 0, 1}}
                          , rotVec[0], rotVec[1], rotVec[2]
                          , na64sw_Rotation_xyz
                          // ^^^ NOTE: tracking tools seem to use this rotation
                          //     order, in global frame (extrinsic rotations),
                          //     yet it is not explicitly defined anywhere 
                          );
    // Instantiate fieldmap
    auto fm = new TrackingTools_FieldMap( cfg["name"].as<std::string>(), t
                                        , fieldFactor
                                        , lengthFactor
                                        );
    fm->SetGlobalShift(TVector3(posVec.data()));
    fm->SetGlobalRotation(TVector3(rotVec.data()));
    fm->SetVerboseLevel(cfg["verboseLevel"] ? cfg["verboseLevel"].as<int>() : 0);
    fm->InitFieldGrid(path);

    auto & L = log4cpp::Category::getInstance(NA64SW_GENFIT_MAGNETS_LOGCAT);
    if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        oss << "Created TrackingTools-based field map instance "
            << cfg["name"].as<std::string>();
        if(cfg["techLabel"])
            oss << " (" << cfg["techLabel"] << ")";
        if(!path.empty()) {
            oss << " using fieldmap file " << path;
        }
        if(cfg["_sourcInfoFile"]) {
            oss << " based on " << cfg["_sourcInfoFile"].as<std::string>();
            if(cfg["_sourcInfoLine"])
                oss << ":" << cfg["_sourcInfoLine"].as<size_t>();
        }
        oss << "; unit factors length=" << lengthFactor << ", field="
            << fieldFactor
            ;
        oss << "; offset=" << o << ", rotation=(" << rotVec[0] << ", "
            << rotVec[1] << ", " << rotVec[2] << ").";
        L << log4cpp::Priority::DEBUG << oss.str();
    }
    // create decoration if event display is active
    if(na64dp::EventDisplay::exists()) {
        util::Vec3 mfBoxR = t.o()
                 , mfBoxUB{{(Float_t) fm->GetGrid().pXAxis.pMin*lengthFactor, 0, 0}}
                 , mfBoxUE{{(Float_t) fm->GetGrid().pXAxis.pMax*lengthFactor, 0, 0}}
                 , mfBoxVB{{0, (Float_t) fm->GetGrid().pYAxis.pMin*lengthFactor, 0}}
                 , mfBoxVE{{0, (Float_t) fm->GetGrid().pYAxis.pMax*lengthFactor, 0}}
                 , mfBoxWB{{0, 0, (Float_t) fm->GetGrid().pZAxis.pMin*lengthFactor}}
                 , mfBoxWE{{0, 0, (Float_t) fm->GetGrid().pZAxis.pMax*lengthFactor}}
                 ;
        t.apply(mfBoxUB);  t.apply(mfBoxUE);
        t.apply(mfBoxVB);  t.apply(mfBoxVE);
        t.apply(mfBoxWB);  t.apply(mfBoxWE);
        util::Vec3 mfBoxU = mfBoxUE - mfBoxUB
                 , mfBoxV = mfBoxVE - mfBoxVB
                 , mfBoxW = mfBoxWE - mfBoxWB
                 ;
        auto mfb = new na64dp::PlacedItems::MagFieldBox( cfg["name"].as<std::string>()
                , TEveVector((mfBoxR - mfBoxU/2 - mfBoxV/2 - mfBoxW/2).r)
                , TEveVector(mfBoxU.r)
                , TEveVector(mfBoxV.r)
                , TEveVector(mfBoxW.r)
                );
        na64dp::EventDisplay::getInstance()->add(mfb);
    }
    return fm;
}

#endif   // defined(TrackingTools_FOUND) && TrackingTools_FOUND

}  // namespace na64dp

