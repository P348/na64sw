#include "na64sw-config.h"

/**\file
 * \brief Standard GenFit fitters registered for na64sw vctr */

#include "absFitter.hh"

#include <DAF.h>
#include <KalmanFitter.h>
#include <KalmanFitterRefTrack.h>

static genfit::eMultipleMeasurementHandling
_genfit_mm_handling_str_to_num( const std::string & strExpr ) {
    if( strExpr.empty() || "unweightedAverage" == strExpr )
        return genfit::unweightedAverage;
    if( "weightedAverage" == strExpr )
        return genfit::weightedAverage;
    if( "weightedClosestToReference" == strExpr )
        return genfit::weightedClosestToReference;
    if( "unweightedClosestToReference" == strExpr )
        return genfit::weightedClosestToReference;
    if( "weightedClosestToPrediction" == strExpr )
        return genfit::weightedClosestToReference;
    if( "unweightedClosestToPrediction" == strExpr )
        return genfit::unweightedClosestToPrediction;
    if( "weightedClosestToReferenceWire" == strExpr )
        return genfit::weightedClosestToReferenceWire;
    if( "unweightedClosestToReferenceWire" == strExpr )
        return genfit::unweightedClosestToReferenceWire;
    if( "weightedClosestToPredictionWire" == strExpr )
        return genfit::weightedClosestToPredictionWire;
    if( "unweightedClosestToPredictionWire" == strExpr )
        return genfit::unweightedClosestToPredictionWire;
    NA64DP_RUNTIME_ERROR( "Unable to interpret string referencing GenFit's"
            " eMultipleMeasurementHandling method: \"%s\"", strExpr.c_str());
}

static void
_genfit_set_common_KalmanFitter_parameters( const YAML::Node & cfg
                                          , genfit::AbsKalmanFitter & n ) {
    if( cfg["multiHandling"] )
        n.setMultipleMeasurementHandling( _genfit_mm_handling_str_to_num(
                    cfg["multiHandling"].as<std::string>() ) );
    if( cfg["resetOffDiagonals"] )
        n.setResetOffDiagonals(cfg["resetOffDiagonals"].as<bool>());

    #define MPP( cfgName, method, type )                    \
    if( cfg[ #cfgName ] )                                   \
        n.set ## method (cfg[ #cfgName ].as< type >());
    MPP( minIterations, MinIterations,  int     );
    MPP( maxIterations, MaxIterations,  int     );
    MPP( deltaPVal,     DeltaPval,      double  );
    MPP( relChi2Change, RelChi2Change,  double  );
    MPP( blowUpFactor,  BlowUpFactor,   double  );
    MPP( blowUpMaxVal,  BlowUpMaxVal,   double  );
    MPP( maxFailedHits, MaxFailedHits,  int     );
    #undef MPP
}

/* \brief Instantiates a GenFit's simple Kalman fitter implementation
 *
 * Resulting class is `genfit::KalmanFitter`. Usage:
 * \code{.yaml}
 * _type: Kalman
 * # ... ; opt, int
 * maxIterations: 4
 * # ... ; opt, float
 * deltaPval: 1e-3
 * # ... ; opt, float
 * blowUpFactor: 1e3
 * # ... ; opt, bool
 * squareRootFormalism: false
 * # ... other common Kalman filter parameters (multiHandling)
 * \endcode
 */
REGISTER_GENFIT_FITTER( Kalman, mgr, cfg, "Simple Kalman track fitter" ) {
    auto p = new genfit::KalmanFitter(
            cfg["maxIterations"] ? cfg["maxIterations"].as<int>() : 4
          , cfg["deltaPval"] ? cfg["deltaPval"].as<double>() : 1e-3
          , cfg["blowUpFactor"] ? cfg["blowUpFactor"].as<double>() : 1e3
          , cfg["squareRootFormalism"] ? cfg["squareRootFormalism"].as<bool>() : false
        );
    _genfit_set_common_KalmanFitter_parameters(cfg, *p);
    return p;
}

/* \brief Instantiates deterministic annealing filter
 *
 * Resulting class is `genfit::DAF`. Usage:
 * \code{.yaml}
 * _type: DAF
 * # ... ; opt, bool
 * useRefKalman: true
 * # ... ; opt, float
 * deltaPval: 1e-3
 * # ... ; opt, float
 * deltaWeight: 1e-3
 * # ... other common Kalman filter parameters (multiHandling)
 * # ...
 * minIterations: 4
 * # ...
 * maxIterations: ...
 * \endcode
 */
REGISTER_GENFIT_FITTER( DAF, mgr, cfg, "Deterministic annealing filter" ) {
    auto p = new genfit::DAF(
            cfg["useRefKalman"] ? cfg["useRefKalman"].as<bool>() : true
          , cfg["deltaPval"] ? cfg["deltaPval"].as<double>() : 1e-3
          , cfg["deltaWeight"] ? cfg["deltaWeight"].as<double>() : 1e-3
        );
    _genfit_set_common_KalmanFitter_parameters(cfg, *p);
    // TODO: prob cuts
    if( cfg["annealingScheme"] ) {
        p->setAnnealingScheme( cfg["annealingScheme"][0].as<float>()
                             , cfg["annealingScheme"][1].as<float>()
                             , cfg["maxIterations"] ? cfg["maxIterations"].as<int>() : 5
                             );
    }
    if( cfg["convergenceDeltaWeight"] ) {
        p->setConvergenceDeltaWeight(cfg["convergenceDeltaWeight"].as<double>());
    }
    if(cfg["debugLvl"])
        p->setDebugLvl(cfg["debugLvl"].as<int>());
    return p;
}

/* \brief Kalman filter implem with linearization around a reference track
 *
 * Resulting class is `genfit::KalmanFitterRefTrack`. Usage:
 * \code{.yaml}
 * _type: KalmanRefTrack
 * # ... ; opt, int
 * maxIterations: 4
 * # ... ; opt, float
 * deltaPval: 1e-3
 * # ... ; opt, float
 * blowUpFactor: 1e3
 * # ... ; opt, bool
 * squareRootFormalism: false
 * # ... other common Kalman filter parameters (multiHandling)
 * \endcode
 */
REGISTER_GENFIT_FITTER( KalmanRefTrack, mgr, cfg
                      , "Kalman filter with linearization around a reference track" ) {
    auto p = new genfit::KalmanFitterRefTrack(
              cfg["maxIterations"] ? cfg["maxIterations"].as<int>() : 4
            , cfg["deltaPval"] ? cfg["deltaPval"].as<double>() : 1e-3
            , cfg["blowUpFactor"] ? cfg["deltaPval"].as<double>() : 1e3
            , cfg["squareRootFormalism"] ? cfg["squareRootFormalism"].as<bool>() : false
        );
    _genfit_set_common_KalmanFitter_parameters(cfg, *p);
    return p;
}

