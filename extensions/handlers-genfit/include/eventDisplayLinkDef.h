#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace na64dp;

// These need no special treatment.
#pragma link C++ class na64dp::EventDisplay+;
