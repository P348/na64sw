#pragma once

#include "na64sw-config.h"

/**\file
 * \brief Defines vctr features for GenFit's fitter classes
 * */

#include "na64util/vctr.hh"
#include "na64calib/manager.hh"

#include <yaml-cpp/yaml.h>

namespace genfit { class AbsFitter; }

namespace na64dp {
template<> struct CtrTraits<genfit::AbsFitter> {
    typedef genfit::AbsFitter * (*Constructor)
        (calib::Manager &, const YAML::Node &);
};
}  // namespace na64dp

/**\brief Registers new GenFit fitter class
 * \ingroup vctr-defs
 *
 * Registers subclasses of `genfit::AbsFitter` base to be created with `VCtr`.
 * Calibration manager reference may be used to subscribe for certain
 * runtime-related updates.
 *
 * \param clsName The name of the handler type
 * \param calibDsp The name of calibations manager instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param desc A fitter type description string
 */
# define REGISTER_GENFIT_FITTER( clsName, calibDsp, ni, desc )                 \
static genfit::AbsFitter * _new_ ## clsName ## _instance( na64dp::calib::Manager &  \
                                                        , const YAML::Node & );    \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<genfit::AbsFitter>( # clsName, _new_ ## clsName ## _instance, desc ); \
static genfit::AbsFitter * _new_ ## clsName ## _instance( __attribute__((unused)) na64dp::calib::Manager & calibDsp  \
                                                        , __attribute__((unused)) const YAML::Node & ni )

