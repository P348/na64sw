#pragma once

#include "na64sw-config.h"

/**\file
 * \brief Defines vctr features for GenFit's magnetic field classes
 * */

#include "na64util/vctr.hh"
#include "na64calib/manager.hh"
#include "na64util/transformations.hh"

#include <yaml-cpp/yaml.h>

#include <AbsBField.h>

///\def NA64SW_GENFIT_MAGNETS_LOGCAT
///\brief Category name for ROOT setup geometry cache
///\see NA64SW_GENFIT_LOGCAT
#ifndef NA64SW_GENFIT_MAGNETS_LOGCAT
#   define NA64SW_GENFIT_MAGNETS_LOGCAT "ROOTGeometry.magnets"
#endif

//
// Magnetic field

namespace na64dp {
struct iAbstractGenFitMField : public genfit::AbsBField {
    ///\brief Transformation defining field maps local frames, by name
    ///
    /// Returned map can contain few
    /// transformations denoting multiple non-zero regions. Key labels the
    /// magnetic field.
    ///
    /// \todo Use chained/merged transformation once it is done for `util::Transformation`
    typedef std::map<std::string, std::vector<util::Transformation>> FieldFramesByPath;
    ///\brief Returns transformations denoting meaningful boundaries of m.field
    ///
    /// Used for decoration purposes.
    virtual FieldFramesByPath labeled_field_frames() const = 0;
};

template<> struct CtrTraits<iAbstractGenFitMField> {
    typedef iAbstractGenFitMField * (*Constructor)
        (calib::Manager &, const YAML::Node &);
};
}  // namespace na64dp

/**\brief Registers new magnetic field type for GenFit interface
 * \ingroup vctr-defs
 *
 * Registers subclasses of `iAbstractGenFitMField` base to be created
 * with `VCtr`.
 *
 * \param clsName The name of the handler type
 * \param calibDsp The name of calibations manager instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param desc A magnetic field type description string
 */
# define REGISTER_MAGNETIC_FIELD( clsName, calibDsp, ni, desc )                 \
static iAbstractGenFitMField * _new_ ## clsName ## _instance( na64dp::calib::Manager &  \
                                                        , const YAML::Node & );    \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<iAbstractGenFitMField>( # clsName, _new_ ## clsName ## _instance, desc ); \
static iAbstractGenFitMField * _new_ ## clsName ## _instance( __attribute__((unused)) na64dp::calib::Manager & calibDsp  \
                                                        , __attribute__((unused)) const YAML::Node & ni )
