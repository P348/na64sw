#pragma once

#ifndef __CINT__
#include "na64sw-config.h"
#endif

#include <Track.h>
#include <AbsKalmanFitter.h>

#include <TEveBox.h>
#include <TVector3.h>
#include <string>
#include <vector>

#include <TGButton.h>
#include <TGNumberEntry.h>
#include <TGButtonGroup.h>

#include <TEveManager.h>  // TODO: subst w fwd
#include <TEvePointSet.h>  // TODO: subst w fwd
#include <TEveStraightLineSet.h>  // TODO: to implem

#ifndef __CINT__
#include "na64util/transformations.hh"
#include "na64calib/placements.hh"
#endif

namespace log4cpp { class Category; }  // fwd

namespace na64dp {

class GenFit2EvDisplay_Orig : public TNamed {
public:
    enum eFitterType {
        SimpleKalman,
        RefKalman,
        DafSimple,
        DafRef
    };
protected:
    log4cpp::Category & _log;  //!

    //
    // GenFit2 track fitting controls

    unsigned int eventId_; //!
    double errorScale_; //!
    std::vector< std::vector<genfit::Track*>* > events_; //!

    TGNumberEntry* guiEvent;
    TGNumberEntry* guiEvent2;

    TGCheckButton* guiDrawGeometry_;
    bool drawGeometry_;
    TGCheckButton* guiDrawDetectors_;
    bool drawDetectors_;
    TGCheckButton* guiDrawHits_;
    bool drawHits_;
    TGCheckButton* guiDrawErrors_;
    bool drawErrors_;

    TGCheckButton* guiDrawPlanes_;
    bool drawPlanes_;
    TGCheckButton* guiDrawTrackMarkers_;
    bool drawTrackMarkers_;

    TGCheckButton* guiDrawTrack_;
    bool drawTrack_;
    TGCheckButton* guiDrawRefTrack_;
    bool drawRefTrack_;
    TGCheckButton* guiDrawForward_;
    bool drawForward_;
    TGCheckButton* guiDrawBackward_;
    bool drawBackward_;

    TGCheckButton* guiDrawAutoScale_;
    bool drawAutoScale_;
    TGCheckButton* guiDrawScaleMan_;
    bool drawScaleMan_;
    TGNumberEntry* guiErrorScale_;

    bool drawSilent_;

    TGCheckButton* guiDrawCardinalRep_;
    bool drawCardinalRep_;
    TGNumberEntry* guiRepId_;
    unsigned int repId_;

    TGCheckButton* guiDrawAllTracks_;
    bool drawAllTracks_;
    TGNumberEntry* guiTrackId_;
    unsigned int trackId_;

    TGCheckButton* guiRefit_;
    bool refit_;
    TGNumberEntry* guiDebugLvl_;
    unsigned int debugLvl_;
    TGButtonGroup* guiFitterId_;
    eFitterType fitterId_;
    TGButtonGroup* guiMmHandling_;
    genfit::eMultipleMeasurementHandling mmHandling_;

    TGCheckButton* guiSquareRootFormalism_;
    bool squareRootFormalism_;
    TGNumberEntry* guiDPVal_;
    double dPVal_;
    TGNumberEntry* guiRelChi2_;
    double dRelChi2_;
    TGNumberEntry* guiDChi2Ref_;
    double dChi2Ref_;
    TGNumberEntry* guiNMinIter_;
    unsigned int nMinIter_;
    TGNumberEntry* guiNMaxIter_;
    unsigned int nMaxIter_;
    TGNumberEntry* guiNMaxFailed_;
    int nMaxFailed_;
    TGCheckButton* guiResort_;
    bool resort_;
protected:
    GenFit2EvDisplay_Orig();
    ~GenFit2EvDisplay_Orig();
    /// Build the buttons for event navigation.
    void make_gui();
    void drawEvent(unsigned int id, bool resetCam = true);
    /**\brief Create a box around o, oriented along u and v with widths ud, vd
     *        and depth and return a pointer to the box object. */
    TEveBox* boxCreator(TVector3 o, TVector3 u, TVector3 v, float ud, float vd, float depth);

    void makeLines( const genfit::StateOnPlane* prevState
                  , const genfit::StateOnPlane* state, const genfit::AbsTrackRep* rep
                  , const Color_t& color, const Style_t& style
                  , bool drawMarkers, bool drawErrors, double lineWidth = 2
                  , int markerPos = 1);
public:
    /**\brief Drop all events.*/
    void reset();
    /**\brief Add new event
     *
     * Add a new event. An event is a collection of Tracks which are displayed at the
     * the same time.
     *
     * The tracks are copied. */
    void addEvent(std::vector<genfit::Track*>& tracks);
    void addEvent(std::vector<const genfit::Track*>& tracks);
    /**\brief Add new event
     *
     * Add a new event consisting of one track. */
    void addEvent(const genfit::Track* tr);
    /// Go to the next event or step a certain number of events ahead.
    void next(unsigned int stp = 1);
    /// Go to the previous event or step a certain number of events back.
    void prev(unsigned int stp = 1);
    /// Go to event with index id.
    void gotoEvent(unsigned int id);
    /// Get the total number of events stored.
    int getNEvents();
    /**\brief Set the display options.
     *
     * The option string lets you steer the way the events are displayed. The following
     * options are available:\n
     * \n
     * 'A': Autoscale errors. The representation of hits are scaled with the error found
     *      their covariance matrix. This can lead to hits not being displayed beause the
     *      errors are too small. Autoscaling ensures that the errors are scaled up
     *      sufficiently to ensure all hits are displayed. However, this can lead to unwanted
     *      results if there are only a few hits with very small errors, as all hits are scaled
     *      by the same factor to ensure consistency.\n\n
     * 'B': Draw Backward Fit (track segments start at updates and end at predictions)\n\n
     * 'D': Draw detectors. This causes a simple representation for all detectors to be drawn. For
     *      planar detectors, this is a plane with the same position and orientation of the real
     *      detector plane, but with different size. For wires, this is a tube whose diameter
     *      is equal to the value measured by the wire. Spacepoint hits are not affected by this
     *      option.\n\n
     * 'E': Draw Error cones (position and direction uncertainties) around the track.\n\n
     * 'F': Draw Forward Fit (track segments start at updates and end at predictions)
     * 'H': Draw hits. This causes the hits to be visualized. Normally, the size of the hit
     *      representation is connected to the covariance matrix of the hit, scaled by the value
     *      set in setErrScale which is normally 1. See also option 'A' and 'S'. Normally used in
     *      connection with 'D'.\n\n
     * 'G': Draw geometry. Draw also the geometry in the gGeoManager. This feature is experimental
     *      and may lead to strang things being drawn.\n\n
     * 'M': Draw track markers. Draw the intersection points between the track and the virtual
     *      (and/or real) detector planes. Can only be used in connection with 'T'.\n\n
     * 'P': Draw detector planes. Draws the virtual (and/or real) detector planes.\n\n
     * 'S': Scale manually. This leads to the spacepoint hits (and only them up to now!) being drawn
     *      as spheres with radius 0.5 scaled with the error scale factor. Can be used if the scaling
     *      with errors leads to problems.\n\n
     * 'T': Draw Track. Draw the track as lines between the virtual (and/or real) detector
     *      planes.\n\n
     * 'X': Draw silent. Does not run the TApplication.
     *
     */
    void setOptions(std::string opts);
    /// Set the scaling factor for the visualization of the errors.
    void setErrScale(double errScale = 1.);
    /// Get the error scaling factor.
    double getErrScale();
    /// Open the event display.
    void open();
    /// GUI callback; called on event redraw
    void redraw_event();
    /**\brief GUI callback; called on event redraw
     *
     * It's unclear for whatever reason it was duplicated in the original
     * GenFit implem. */
    void redraw_event_2();
    /// GUI callback; sets the widgets states according to class state
    void gui_sync_draw_params();
    /// GUI callback; sets fitter implem used for track fitting
    void gui_select_fitter_id(int val);
    /// FUI callback; switch multihit handling mode
    void gui_select_mm_handling(int val);

    //
    // Interface
protected:
    /// Shall add various subsidiary entities
    virtual void add_items(TEveManager * eve) {}
};

/**\brief Defines container for items with assigned places
 *
 * Stores some graphics primitives, text annotations etc that do not directly
 * relate to the event scene.
 * */
class PlacedItems
            : public GenFit2EvDisplay_Orig
            , public util::Observable<calib::Placements>::iObserver  //!
            {
public:
    /// Represents supplementary, decorative entities to be drawn on scene(s)
    class iAnnotatedEntity {
    protected:
        std::string _annotation;
        TEveVector _putLabelAt;  // not used if X is NaN
        // ...
    public:
        iAnnotatedEntity() : _putLabelAt( std::nan("0")
                                        , std::nan("0")
                                        , std::nan("0")
                                        ) {}
        iAnnotatedEntity( const std::string & ann) : _annotation(ann)
                                                   , _putLabelAt( std::nan("0")
                                                                , std::nan("0")
                                                                , std::nan("0")
                                                                ) {}
        iAnnotatedEntity( const std::string & ann
                        , const TEveVector & lblAt
                        ) : _annotation(ann)
                          , _putLabelAt(lblAt) {}

        virtual void add_assets( TEveManager * eve ) {
            //auto txtPtr = new TEveText(annotation.c_str());
            //eve->AddElement();
        }

        const std::string & annotation() const { return _annotation; }
    };

    /// Coordinate sensitive plane
    ///
    ///\note `r` is coordinate of the corner, `u` and `v` are the edge vectors.
    struct WiredPlane : public iAnnotatedEntity {
    protected:
        TEveVector r, u, v;
    public:
        WiredPlane( const std::string & name
                  , const TEveVector & r_
                  , const TEveVector & u_
                  , const TEveVector & v_
                  ) : iAnnotatedEntity( name, r_ )
                    , r(r_), u(u_), v(v_) {}
        void add_assets( TEveManager * eve ) override {
            iAnnotatedEntity::add_assets(eve);
            // Draw parallelogram defining active bounds of wired plane
            auto lineSet = new TEveStraightLineSet(_annotation.c_str());
            lineSet->AddLine(r, r + u);
            lineSet->AddLine(r + u, r + u + v);
            lineSet->AddLine(r + u + v, r + v);
            lineSet->AddLine(r + v, r);
            eve->AddElement(lineSet);
        }
    };

    /// A (not necessarily) skewed box decorative item
    struct SkewedBox : public iAnnotatedEntity {
    protected:
        TEveVector r, u, v, w;
    public:
        SkewedBox( const std::string & name
                 , const TEveVector & r_
                 , const TEveVector & u_
                 , const TEveVector & v_
                 , const TEveVector & w_
                 ) : iAnnotatedEntity( name, r_ )
                   , r(r_), u(u_), v(v_), w(w_) {}
        void add_assets( TEveManager * eve ) override {
            iAnnotatedEntity::add_assets(eve);
            // Draw parallelogram defining active bounds of wired plane
            auto lineSet = new TEveStraightLineSet(_annotation.c_str());
            lineSet->AddLine(r,         r + u    );
            lineSet->AddLine(r + u,     r + u + v);
            lineSet->AddLine(r + u + v, r + v    );
            lineSet->AddLine(r + v,     r        );

            lineSet->AddLine(r,             r + w         );
            lineSet->AddLine(r + u,         r + w + u     );
            lineSet->AddLine(r + v,         r + w + v     );
            lineSet->AddLine(r + u + v,     r + u + v + w );

            lineSet->AddLine(r + w,         r + w + u    );
            lineSet->AddLine(r + w + u,     r + w + u + v);
            lineSet->AddLine(r + w + u + v, r + w + v    );
            lineSet->AddLine(r + w + v,     r + w        );
            eve->AddElement(lineSet);
        }
    };

    /// Volumetric detector depiction
    struct VolumetricDetector : public SkewedBox {
        VolumetricDetector( const std::string & name
                          , const TEveVector & r_
                          , const TEveVector & u_
                          , const TEveVector & v_
                          , const TEveVector & w_
                          ) : SkewedBox(name, r_, u_, v_, w_) {}
    };

    /// Rectangular magnetic field volume
    struct MagFieldBox : public SkewedBox {
        MagFieldBox( const std::string & name
                   , const TEveVector & r_
                   , const TEveVector & u_
                   , const TEveVector & v_
                   , const TEveVector & w_
                   ) : SkewedBox(name, r_, u_, v_, w_) {}
    };
protected:
    /// Collection of decorative items
    std::unordered_map<std::string, iAnnotatedEntity *> _decoItems;
protected:
    PlacedItems(calib::Manager * mgrPtr);
    /// Adds deco items
    void add_items(TEveManager *) override;
    /**\brief Updates decoration placements
     *
     * Updates decorations drawn -- detector active area markings, labels, etc.
     *
     * \note Data update by this method must not affect the tracking
     * */
    virtual void handle_update( const calib::Placements & ) override;
public:
    /// Adds decoration item entity
    std::pair< std::unordered_map<std::string, iAnnotatedEntity *>::iterator
             , bool >
    add( iAnnotatedEntity * item ) {
        return _decoItems.emplace(item->annotation(), item);
    }
};

/**\brief NA64sw event display implementation based on `ROOT::TEve`
 *
 * This event display is based on original implementation of GenFit2 that
 * offered means to visualize the track fitting results. It has been slightly
 * extended to draw detector geometry, deal with multiple customizable
 * projections, etc.
 * */
class EventDisplay : public PlacedItems {
protected:
    static EventDisplay * _self; //!
    EventDisplay(calib::Manager * mgrPtr=nullptr)
        : PlacedItems(mgrPtr)
        {}
public:
    ~EventDisplay();
    /// Returns (newly created, if need) instance of event display
    static EventDisplay* getInstance(calib::Manager * mgrPtr=nullptr);
    /// Returns `true` if active event display instance exists
    static bool exists() { return _self; }
public:
    ClassDef(EventDisplay,1)
};  // class EventDisplay
}  // namespace na64dp

