#pragma once

#include "na64sw-config.h"

#include <SharedPlanePtr.h>
#include <MeasuredStateOnPlane.h>
#include <MeasurementOnPlane.h>

#include "na64dp/abstractHitHandler.hh"
#include "ROOTSetupGeometryCache.hh"

namespace genfit {
class TrackPoint;
class AbsMeasurement;
class AbsBField;
class AbsFitter;
class TGeoMaterialInterface;
class Track;
}  // namespace genfit

namespace na64dp {
namespace event { struct Track; }
namespace handlers {
namespace aux {
/**\brief Maintains GenFit2 geometry wrt NA64SW placements
 *
 * This auxiliary class caches object implementing conversions from NA64SW
 * `TrackScore` into particular GenFit2 `AbsMeasurement` subclass instances.
 *
 * This class also keeps track on the GenFit2 `AbsDet` subclasses'
 * instances corresponding to detectors producing scores within a track.
 * This instances are used by GenFit2's `AbsMeasurement` subclasses.
 *
 * \todo Get rid of external debug log file in favour of standard (log4cpp) one
 * \todo Decouple with GenFit2 and move to dedicated class as, aside from raw
 *       GenFit2 measurements, this thing is needed in different contexts.
 * */
class GenfitGeometryCache : public util::Observable<calib::Placements>::iObserver
                          , public calib::Handle<nameutils::DetectorNaming>
                          {
public:
    /**\brief Base class for score conversion units keeping necessary info
     *
     * Base class for detector geometry description cache. Subclasses implement
     * storage for relevant geometrical information -- direction of measurement
     * vector's, wire distance, etc.
     *
     * Subclasses shall define how the NA64SW track score shall be converted
     * into GenFit2's measurements.
     * */
    struct iDetGeoEntry {
        /// Logging category
        log4cpp::Category & L;
        /// GenFit2 detector ID
        int gfDetID;
        /// Text detector name
        const std::string name;
        /// Set to `true` if local fit object must be set from unbiased
        /// fitted state
        const bool unbiasedFittedState;

        /// Inits public members
        iDetGeoEntry( log4cpp::Category & L_
                    , const std::string & name_
                    , bool unbiasedFittedState_=false
                    ) : L(L_)
                      , gfDetID(0x0)
                      , name(name_)
                      , unbiasedFittedState(unbiasedFittedState_)
                      {}
        /// Empty virtual dtr for polymorphic deletion
        virtual ~iDetGeoEntry(){}

        ///\brief Should create new measurement or return null, interface function
        ///
        /// Defines "to-GenFit" conversion.
        virtual genfit::AbsMeasurement * new_measurement(const event::TrackScore &, float &) = 0;

        ///\brief Should use measurements-on-plane to impose residuals information
        ///
        /// Defines "from-GenFit" conversion. Should copy/convert local fitted
        /// state and other subsidiary information from GF2 "measured state on
        /// plane" to score fit info.
        virtual void set_residuals( const std::vector<genfit::MeasurementOnPlane *> &
                                  , event::ScoreFitInfo & sfi
                                  , const genfit::MeasuredStateOnPlane &
                                  , bool isBiased
                                  ) = 0;
        /// Called at the end of event to drop the caches
        virtual void reset() {}
    };

    /**\brief A simple 1D plane (GEMs, MuMegas, hodoscopes, etc).
     *
     * Holds information of wired plane -- GEMs, MMs, silicon microstrips, etc.
     * Most common coordinate detector type used for many detectors measuring
     * single coordinate.
     * */
    struct WiredPlane : public iDetGeoEntry {
        /// An integer alias to cope GenFit's plane ID with NA64SW
        int gfPlaneID;
        /// A pointer to GenFit's plane object used for this wired plane
        genfit::SharedPlanePtr gfPlanePtr;
        /// Resolution to be used for as measurement error
        float resolution;
        /// Full measured size
        float measuredSize;

        /// When set, makes `new_measurement()` ignore drift features for
        /// drift-like hits. Useful to forcefully treat drift-like in
        /// "hodoscopic" mode. Also used to create an "idle" hit for R(T)
        /// extraction (so, logically it is supplementary parameter for
        /// `new_measurement()`).
        bool ignoreDriftFeatures;

        /// Hit counter. Used to enumerate GenFit2's hits within a single event.
        int nHit;
        /// Map of "idle" measurements that do not participate in tracking, yet
        /// are used to derive some useful information (e.g. R(T)).
        std::unordered_map<const event::TrackScore *, genfit::AbsMeasurement *>
            scores2idleMsrmnts;

        /// Constructs new entry
        WiredPlane(log4cpp::Category &, const calib::Placement &, DetID);
        /// Creates proper GenFit2's raw measurement instance wrt provided
        /// score
        genfit::AbsMeasurement * new_measurement(const event::TrackScore &, float &) override;
        /// Properly sets residuals
        void set_residuals( const std::vector<genfit::MeasurementOnPlane *> &
                          , event::ScoreFitInfo & sfi
                          , const genfit::MeasuredStateOnPlane &
                          , bool isBiased
                          ) override;
        /// Drops caches at the end of event.
        virtual void reset() override;
    };

    /**\brief Detector station
     *
     * Planar detectors measuring 2D coordinate, composed from multiple planes.
     * May combine wired planes (but produces different GenFit2 measurement
     * class instances).
     *
     * \todo unused currently, delete or use it
     * */
    class DetectorStation : public iDetGeoEntry {
    protected:
        std::unordered_map<PlaneKey, calib::Placement> _planePlacements;
        void _make_gf_plane();
    public:
        /// An integer alias to cope GenFit's plane ID with NA64SW
        int gfPlaneID;
        /// A pointer to GenFit's plane object used for this wired plane
        genfit::SharedPlanePtr gfPlanePtr;
        /// Resolution to be used for as measurement error
        float resolution[2];
        /// Hit counter
        int nHit;

        DetectorStation(log4cpp::Category &, const std::string &);
        /// Adds plane to station
        void add_plane(const calib::Placement &, DetID);
        /// Creates new measurement from the score (returns null if failed)
        genfit::AbsMeasurement * new_measurement(const event::TrackScore &, float &) override;
        /// Use measurements-on-plane to impose residuals information
        virtual void set_residuals( const std::vector<genfit::MeasurementOnPlane *> &
                                  , event::ScoreFitInfo & sfi
                                  , const genfit::MeasuredStateOnPlane &
                                  , bool isBiased
                                  ) override;
        /// Called at the end of event.
        virtual void reset() override;
    };

    /**\brief Special type of detector representation measuring spatial points
     *
     * For instance, BMS scores produced by ad-hoc handler generates scores as
     * spatial points defined with large (few mm or tens of mm) uncertainties
     * by their large slabs. Another type of spatial detectors can be
     * calorimeters with very fine spatial granularity, time-projection
     * chambers and so on.
     *
     * \note Produced residauls vector has to be decomposed in a delicate and
     *       special way.
     **/
    class VolumetricDetector : public iDetGeoEntry {
    protected:
        const std::string _name;
        int _nHit;
        util::Transformation _t;
    public:
        /// An integer alias to cope GenFit's plane ID with NA64SW
        int gfPlaneID;

        VolumetricDetector( DetID did
                          , const calib::Placement & pl
                          , log4cpp::Category & logCat);
        /// Creates instance of `ProlateSpacepointMeasurement`
        genfit::AbsMeasurement * new_measurement(const event::TrackScore &, float &) override;
        /**\brief Impose residuals information at score
         *
         * GenFit provides residuals defined as 2D vector (UV) on virtual plane
         * coming through POCA and oriented wrt to (resumably) perpendicular
         * line connecting POCA on the fitted track and measurement center. To
         * derive meaningful information from this we rather use global points
         * obtained from "smoothed state" (at POCA) and measurement.
         */
        void set_residuals( const std::vector<genfit::MeasurementOnPlane *> &
                          , event::ScoreFitInfo & sfi
                          , const genfit::MeasuredStateOnPlane &
                          , bool isBiased
                          ) override;
        /// Called at the end of event.
        void reset() override;
    };

    // ... other cached geometry types
private:
    /// Ref to logging category
    log4cpp::Category & _geoCacheLog;
protected:
    /// When set, a dedicated step will be performed to collect additional
    /// residuals for drift-like detectors wherever possible
    bool _collectRT;

    /// Updates `_detGeoCache`.
    void handle_update( const calib::Placements & ) override;
    /// Cache of the conversion entries
    std::unordered_map<DetID, iDetGeoEntry *> _detGeoCache;
    /// Names of detectors that could not be converted from scores to
    /// measurements for whatever reason (typically, missing info for geometry)
    mutable std::unordered_map<std::string, size_t> _faultyNames;
    /// Local cache mapping GenFit2 measurement and scores.
    /// \note To be cleared by user code!
    mutable std::unordered_map<const genfit::AbsMeasurement *, std::pair<DetID, mem::Ref<event::ScoreFitInfo>> >
            _msrmnts2sfi;

    GenfitGeometryCache( calib::Dispatcher & dsp
                       , bool collectRT
                       , log4cpp::Category & logCat
                       )
            : calib::Handle<nameutils::DetectorNaming>("default", dsp)
            , _geoCacheLog(logCat)
            , _collectRT(collectRT)
            {
        dsp.subscribe<calib::Placements>(*this, "default");
    }
    ~GenfitGeometryCache();

    const nameutils::DetectorNaming & naming() const {
        return calib::Handle<nameutils::DetectorNaming>::operator*();
    }
    
    /**\brief Populates map instance with GenFit2 track points from given track
     *
     * GenFit2 tracks need to be populated with sorted track points. To
     * do so we assemble a (sorted) map with track points based on
     * their sorting parameter and use it then to populate the track
     * rather than insert track points directly. Note that we permit
     * few GF TP to have the same sorting parameter (whatever it means...)
     *
     * This is a stateful algorithm:
     *  1. Iterate over scores within a track assuming scores for same det ID
     *     continiously appear during iteration (this is the case because
     *     multimap places them into buckets)
     *      1.1 Create new GF track point each time DetID changes its value,
     *          i.e. each new detector will provoke creation of GF TP
     *      1.2 For each score, create a GF "measurement" instance using 
     *          det.geo cache and put it into current GF TP
     *      1.3 Put TP into `gfTPs` map, sorted by some "sorting parameter"
     *  2. Iterate over sorted `gfTPs` map and create GF track populated with
     *     this sorted track points
     */
    void create_GF2_tps( std::multimap<float, genfit::TrackPoint *> & gfTPs
                       , const event::Track &) const;
};  // class GenfitGeometryCache
}  // namespace ::na64dp::aux


/**\brief An interface to GenFit2 track fitting procedure
 *
 * This class implements some common functionality for track reconstruction
 * routines:
 *  - conversion of NA64SW tracks into GenFit2's tracks;
 *  - track fitting using `genfit::AbsFitter` interface
 *  - conversion of GenFit2's fitted tracks into NA64SW tracks;
 *  - maintains geometrical description up to date
 *
 * \note `_collectRT` does _not_ turns on R(T) collection unless drift
 *       features are already allocated by score creation routine. If you wish
 *       to collect R(T) in hodoscopic mode (without any R(T) being used),
 *       consider plotting `lR[0]` vs time by `Histogram2D`.
 *
 * \ingroup handlers tracking-handlers
 * */
class GenFit2_FitTrack : public AbstractHitHandler<event::Track>
                       , public aux::GenfitGeometryCache
                       {
public:
    /// Defines in which case(s) to copy track fit info
    enum CopyMode {
        kNoCopy = 0,
        kCopyIfConvergedPartially = 0x2,
        kCopyIfConvergedFully = 0x4,
        kCopyAlways = 0x1
    };
protected:
    /// Pointer to a fitter instance
    genfit::AbsFitter * _fitter;
    /// Handle for ROOT setup geometry cache extension instance
    util::ROOTSetupGeometry & _rootSetupExtension;
    /// Number of hits restriction
    size_t _nHitsLimit;
    /// Selector for detector IDs to which unbiased residuals must be collected
    std::pair<std::string, DetSelect *> _unbiasedSelector;
    /// If set, unbiased residuals will be collected for all detectors
    bool _collectUnbiasedForAll;
    /// Speacial meaning var (-1 -- do, 0 -- don't)
    int _doCollectUnbiasedInt;
    /// Copy mode for the track after fitting attempt
    CopyMode _copyMode;
    /// When set AND event display is active, dispatches the track to event
    /// display
    bool _dispatch2GFEvDisplay;
    /// Overall tracks counters
    mutable size_t _nTracksConsidered
                 , _nTracksFitted
                 , _nTracksConvergedFully
                 , _nTrackConvergedPartially
                 , _nTracksDidNotConverged
                 ;
protected:
    /// Updates bound detector selectors
    void handle_update( const nameutils::DetectorNaming & ) override;
public:
    /// Makes new GF2 track object based on NA64SW track object
    genfit::Track * to_gf_track(const event::Track &) const;
    /// Copy info from GenFit2 output to NA64SW, if has fit status
    void copy_from_fitted_track(const genfit::Track &, event::Track &) const;
    /// Returns detector name for certain score ID (slow, used for error
    /// reporting only)
    std::string get_det_name_for_score(mem::Ref<event::TrackScore>) const;

    /// Ctr (fitter can be null)
    GenFit2_FitTrack( calib::Manager & cmgr
                    , const std::string & sel
                    , size_t nHitsLimit
                    , CopyMode copyMode
                    , const std::string & unbiasedSel
                    , log4cpp::Category & loggingCategory
                    , bool collectRT=false
                    , genfit::AbsFitter * fitter=nullptr
                    , bool dispatch2GFEvDisplay=false
                    , const std::string & debugOutput=""
            );
    ~GenFit2_FitTrack();

    /// Creates GF2 version of track, fits it and copies fit results
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Forwards to parent's and does the cleanup
    ProcRes process_event(event::Event &) override;
    /// Prints warning summary, if any
    void finalize() override;
};  // class AbstractGenFit_ScoreBasedTracking

}  // namespace ::na64dp::handlers
}  // namespace na64dp

