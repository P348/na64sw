#pragma once

#include <yaml-cpp/yaml.h>

#include "na64calib/setupGeoCache.hh"
#include "na64calib/mkROOTGeo.hh"
#include "na64app/extension.hh"

///\def NA64SW_GENFIT_LOGCAT
///\brief Category name for ROOT setup geometry cache
///\see NA64SW_GENFIT_MAGNETS_LOGCAT
#ifndef NA64SW_GENFIT_LOGCAT
#   define NA64SW_GENFIT_LOGCAT "ROOTGeometry"
#endif

namespace genfit {
class TGeoMaterialInterface;
}  // namespace genfit

namespace na64dp {
class iAbstractGenFitMField;  // fwd
namespace util {

/**\brief Helper utility class, aggregates setup description -- geometry and
 *        fields.
 *
 * This class encapsulates few global instance (ROOT GeoManager, GenFit2's
 * magnetic field manager, etc.
 */
class ROOTSetupGeometryCache
        : public calib::SetupGeometryCache {
private:
    /// Computed automatically
    Double_t _worldDims[3];
protected:
    /// Reference to calibration manager -- needed by some types of magnetic
    /// fields
    calib::Manager & _calibMgr;
    /// Steers validity of the geometry and magnetic field classes. When unset,
    /// `_recache_geometry()` and `_recache_mag_fields()` will be called right
    /// before track fitting via `_recache_setup()`
    bool _setupValid;
    /// Pointer to a (simplified) placements-based geometry. Not used if null.
    PlacementsBasedGeometry * _plBGeomPtr;
    /// ROOT's TGeoManager instance pointer, lazily created on first event
    /// arrival, after full set of "placements" update(s)
    TGeoManager * _geoMgr;
    /// If set to non-empty string, will force handler to use geometry from
    /// file instead of constructing it using "placements"
    const std::string _geomSrcURI;
    /// Magnetic field configuration parameters (copied from ctr).
    //const YAML::Node _magFieldsCfg;
    std::vector<sdc::SrcInfo<calib::Placement>> _magnetsPlacements;

    /// Segments zoning cache
    std::unordered_map<DetID_t, int> _zoneByDets;
    /// Set of zone IDs
    std::set<int> _zones;

    /// Should prevent tracking from consideration of material effects.
    bool _disableMaterialEffects;

    /// Magnetic field ptr (usually, a group of field instances)
    iAbstractGenFitMField * _magField;
    /// Material interface handler (GenFit's default)
    genfit::TGeoMaterialInterface * _matIface;
protected:
    void handle_single_placement_update(const calib::Placement &) override;
    /// (Re-)caches placements
    void handle_update( const calib::Placements & placements ) override;
    /// (Re-)caches geometry
    void _recache_geometry();
    /// (Re-)caches magnetic field(s)
    ///
    /// Uses collected placements information on the magnetic fields
    /// configurations to instantiate programmatic instance by means of `VCtr`
    void _recache_mag_fields();
public:
    ROOTSetupGeometryCache( calib::Manager & mgr
                          , log4cpp::Category & loggingCategory
                          , const std::string & geomSrcURI=""
                          , bool disableMatEffects=true
                          );
    /// (Re-)builds setup if need -- forwards execution to
    /// `_recache_geometry()` and `_recache_mag_fields()`
    void recache_setup();
};  // class ROOTSetupGeometryCache


class ROOTSetupGeometry
        : public iRuntimeExtension
        {
private:
    /// Dictionary of external parameters
    std::unordered_map<std::string, std::string> _externalParameters;
    /// Ptr to created setup geo cache
    ROOTSetupGeometryCache * _ptr;
public:
    /// Dft ctr
    ROOTSetupGeometry();
    /// Initializes `ROOTSetupGeometry` ptr to be used further
    void init( calib::Manager & cmgr
             , iEvProcInfo * //epi
             ) final;
    /// Sets parameter.
    virtual void set_parameter( const std::string & nm
                              , const std::string & val) final;
    ROOTSetupGeometryCache * cache() { return _ptr; }
    /// Deletes field manager instance and associated field instance(s)
    void finalize() override;
};  // class ROOTSetupGeometry

}  // namespace ::na64dp::util
}  // namespace na64dp

