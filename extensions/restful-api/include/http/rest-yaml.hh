#pragma once

#include "http/rest.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace util {
namespace http {

template<>
struct RESTTraits<YAML::Node> {
    static constexpr auto contentTypeStr = "text/x-yaml";
    static YAML::Node parse_request_body(const Msg::iContent *, log4cpp::Category &);
    static void set_content(ResponseMsg &, const YAML::Node &, log4cpp::Category &);
    static YAML::Node method_not_allowed();
};

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

