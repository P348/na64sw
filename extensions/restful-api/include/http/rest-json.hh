#pragma once

#include "http/rest.hh"

#include <nlohmann/json.hpp>

namespace na64dp {
namespace util {
namespace http {

using JSON = nlohmann::json;

template<>
struct RESTTraits<JSON> {
    static constexpr auto contentTypeStr = "application/json";
    static JSON parse_request_body(const Msg::iContent &);
    static void set_content(ResponseMsg &, const JSON &);
};

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp
