/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64dp/abstractEventSource.hh"

#include "http/rest.hh"
#include "http/rest-yaml.hh"

namespace na64dp {
namespace util {
namespace http {

template<typename T> class EventResource;

/**\brief Event resource endpoint
 *
 * Implements an endpoint to interact with the event source object (and,
 * optionally, with a data treatment pipeline object). Capable to return
 * current event data by GET request. Depending on the bound event source
 * object may be used to read next, previous or by-ID event.
 *
 * \note Beware of confusion with "events" endpoint or similar. GET request
 *       is idempotent and always returns *current* event (which is, however,
 *       modified by PATCH).
 * */
template<>
class EventResource<YAML::Node> : public SpecializedResource<YAML::Node> {
protected:
    /// Ptr to data source under supervision
    AbstractEventSource * _src;
    /// List of data source inputs used to initialize the data source
    const std::vector<std::string> & _inputs;
    /// Reference to reentrant event object used for reading/data processing
    event::Event & _evRef;
    /// Reference to local memory used to create event's items
    event::LocalMemory & _lmemRef;
    /// Reference to (external) detector naming handle
    calib::Handle<nameutils::DetectorNaming> * _namingHandlePtr;
    /// Pointer to created pipeline (can be null)
    Pipeline ** _pipelinePtr;
    /// Internal flag; set to `true` if event was processed
    bool _wasProcessed;
    /// Last event processing result
    std::pair<bool, bool> _eventProcessingResult;
public:
    EventResource( //const std::string & basePath
                   log4cpp::Category & logCat
                 , AbstractEventSource * src
                 , event::Event & evRef
                 , event::LocalMemory & lmemRef
                 , const std::vector<std::string> & inputs
                 , Pipeline ** pipelinePtr=nullptr
                 , calib::Handle<nameutils::DetectorNaming> * namingHandle=nullptr
                 )
        : SpecializedResource<YAML::Node>(logCat)
        , _src(src)
        , _inputs(inputs)
        , _evRef(evRef)
        , _lmemRef(lmemRef)
        , _namingHandlePtr(namingHandle)
        , _pipelinePtr(pipelinePtr)
        , _wasProcessed(false)
        {}

    std::shared_ptr<ResponseMsg>
        options( const RequestMsg &, const URLParameters &) const override;

    /**\brief Returns current event data
     *
     * Response object brings event data as JSON (\see `JSONWriter`). Response
     * header `X-NA64sw-Was-Processed` is set to `"1"`/`"0"` depending on
     * whether the event underwent (any) pipeline treatment, event if event
     * object was left intact by the pipeline.
     *
     * If event passed through the pipeline, following additional headers are
     * added to the response:
     *  - `X-NA64sw-Stop-Processing` set to `"1"`/`"0"` if any handler
     *    requested processing stop (lookup stop condition, or something)
     *  - `"X-NA64sw-Was-Discriminated"` set to `"1"`/`"0"` if event was
     *    discriminated, but preceeding `PATCH` request was made
     *    with `keepDiscriminated=true` query string parameter.
     * */
    YAML::Node get(const YAML::Node &, const URLParameters &) override;

    // POST is not allowed
    // PUT is not allowed

    /**\brief Acquires new event substituting existing instance
     *
     * Available for all source types, but with restricted functions. If source
     * does not support certain patch a 400 "Bad request" response is returned.
     *
     * Request payload should contain a YAML object with "eventID" being
     * either an event ID string (in a form that `EventID::from_str()` is
     * capable to handle) or a simple sting of "next"/"previous" alias). If
     * source supports the requested navigation, but corresponding C++
     * `AbstractEventSource::read...()` method returned `false` (what usually
     * means that source is depleted), a `409` "Conflict" is returned.
     *
     * Query string can contain following parameters:
     *  - `dontProcess` (true/false) orevents pipeline processing if set.
     *  - `keepDiscriminated` (true/false) is used to keep event as current
     *    even if some handler from the pipeline "discriminated" it. Has no
     *    effect if `dontProcess` is set.
     *
     * \todo Test logic for divverent query option parameters.
     *
     * \note View does not return `304` "Not Modified" in case of failed PATCH
     *       as it is not a suitable choice since PATCH is not safe and 3xx are
     *       actually redirection status codes (contrary to common
     *       misunderstanding).
     * */
    YAML::Node patch(const YAML::Node &, const URLParameters &) override;

    // DELETE is not allowed
};


}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

