#pragma once

#include "http/rest.hh"
#include "http/rest-yaml.hh"
#include "na64calib/setupGeoCache.hh"

namespace na64dp {
namespace util {
namespace http {

/**\brief Provides access to geometry used in tracking or MC routines
 *
 * This resource gets updated once new placements is retrieved within certain
 * process (so, currently no modification requests are supported).
 * */
class Geometry : public SpecializedResource<YAML::Node> 
               , public calib::SetupGeometryCache
               {
protected:
    log4cpp::Category & _L;
    /// Current update index
    size_t _cUpdateID;
    /// Node keeping active geometry
    ///
    /// \todo invalidate items?
    YAML::Node _geometry;
public:
    Geometry( calib::Dispatcher & cdsp
            , log4cpp::Category & logCat
            );

    void handle_single_placement_update(const calib::Placement &) override;

    /**\brief Retursn current placements cache
     *
     * Exploits rough caching mechanism: along with response an "ETag" is
     * returned that currenly can be specified with further requests to check
     * against cache changes; if geometry cache was not changed,
     * a 304 "Not Modified" response is returned.
     * */
    YAML::Node get(const YAML::Node &, const URLParameters &) override;
};

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

