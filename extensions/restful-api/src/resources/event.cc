#include "resources/event.hh"
#include "na64event/data/event.hh"
#include "na64dp/pipeline.hh"
#include "http/server.hh"
#include "na64event/json-serialize.hh"

namespace na64dp {
namespace util {
namespace http {

std::shared_ptr<ResponseMsg>
EventResource<YAML::Node>::options( const RequestMsg &
                                  , const URLParameters &) const {
    _L << log4cpp::Priority::DEBUG << "Handling OPTIONS request.";
    auto resp = std::make_shared<ResponseMsg>(Msg::NoContent);
    resp->set_header("Access-Control-Allow-Origin", "*");
    resp->set_header("Access-Control-Allow-Methods", "GET, PATCH");
    resp->set_header("Access-Control-Allow-Headers", "Content-Type");
    // ...
    return resp;
}

YAML::Node
EventResource<YAML::Node>::get( const YAML::Node & rq
                              , const URLParameters & urlParams
                              ) {
    _L << log4cpp::Priority::DEBUG << "Handling GET request.";
    if(!_src) {
        _L << log4cpp::Priority::DEBUG << "GET request: no source associated,"
            " returning empty content.";
        return YAML::Node(YAML::NodeType::Null);
    }
    // todo: content negotiation?

    _current_response_object().set_header("X-NA64sw-Was-Processed"
            , _wasProcessed ? "1" : "0");
    if(_wasProcessed) {
        _current_response_object().set_header("X-NA64sw-Stop-Processing"
                , _eventProcessingResult.first ? "1" : "0" );
        _current_response_object().set_header("X-NA64sw-Was-Discriminated"
                , _eventProcessingResult.second ? "1" : "0" );
    }
    
    // serialize event
    std::ostringstream oss;
    //_L << log4cpp::Priority::DEBUG << "XXX, GET request: returning event with "  // XXX
    //    << _evRef.tracks.size() << " tracks.";                                   // XXX
    event_to_JSON(_evRef, oss, _namingHandlePtr
                             ? &(_namingHandlePtr->get())
                             : nullptr
                             );
    //std::cout << "---" << std::endl << oss.str() << std::endl << "---" << std::endl;  // XXX
    #if 1
    // NOTE: YAML-cpp sometimes fails to parse our JSON string with error
    // "end of map flow not found", yet the same string gets correctly parse
    // with Python's YAML and jq, so we response here with plain JSON
    _current_response_object().content(std::make_shared<StringContent>(oss.str()));
    _current_response_object().set_header("Content-Type", "application/json");
    return YAML::Node(YAML::NodeType::Null);
    #else
    try {
        return YAML::Load(oss.str().c_str());
    } catch( YAML::Exception & e ) {
        // TOD: this print-out should be deleted at some point probably
        std::cout << "--- begin of malformed JSON dump ---" << std::endl
            << oss.str() << std::endl
            << "--- end of malformed JSON dump ---"
            << std::endl;
        _L << log4cpp::Priority::ERROR
           << "While parsing YAML from event's JSON, an error occured: "
           << e.what() << ", JSON string is dumped to stdout.";
    }
    #endif

    #if 0
    auto it = urlParams.find("eventID");
    if(it != urlParams.end() && it->second.empty()) {
        // request for particular event:
        return _get_event(it->second, rq, urlParams);
    }
    // request is for source info/events list

    YAML::Node r;
    //r["_links"] = YAML::Node(YAML::NodeType::Map);
    //r["features"] = YAML::Node(YAML::NodeType::Map);
    AbstractEventSource::Features_t fts = _src->features();
    r["features"]["backwardAccess"] = (bool) (AbstractEventSource::kBackwardReading & fts);
    //r["_links"]["backward"] = this->path_for({{"eventID", "prev"}});  // xxx
    r["features"]["randomAccess"] = (bool) (AbstractEventSource::kRandomAccess & fts);
    r["features"]["list"] = (bool) (AbstractEventSource::kProvidesEventsList & fts);

    // TODO: parse querystring, return config, get listing parameters if
    //       quirystring orders, pagination, etc
    //bool doAppendConfig = false;
    //try {
    //    auto it = urlParams.find("config");
    //    if(it != urlParams.end()) doAppendConfig = str_to_bool(it->second);
    //} catch(errors::InvalidOptionExpression & e) {
    //    throw errors::RequestError(e.what(), Msg::BadRequest);
    //}
    //#if 1  // XXX
    //for(auto & p : _current_request_object().uri().query()) {
    //    std::cout << " - \"" << p.first << "\": " << p.second << std::endl;
    //}
    //#endif
    //if( doAppendConfig ) {
    //    r["config"] = _srcCfg;
    //}

    r["inputs"] = _inputs;

    YAML::Node typeSpecific;
    _src->append_state_info(typeSpecific, urlParams);

    // TODO: events list with pagination
    #endif
}

YAML::Node
EventResource<YAML::Node>::patch( const YAML::Node & rq
                                , const URLParameters & urlParams
                                ) {
    _L << log4cpp::Priority::DEBUG << "Handling PATCH request.";
    if(!_src) {
        _L << log4cpp::Priority::DEBUG << "Handling PATCH request: no source"
            " associated, responding with \"Method Not Allowed\"";
        _current_response_object().status_code(Msg::MethodNotAllowed);
        return YAML::Node(YAML::NodeType::Null);
    }
    if(!rq["eventID"]) {
        _L << log4cpp::Priority::WARN << "Handling PATCH request: request"
            " did not requested with event ID.";
        throw errors::RequestError("No \"eventID\" specified", Msg::BadRequest);
    }
    const auto query = _current_request_object().uri().query();
    bool applyPipeline = true;
    {
        auto it = query.find("dontProcess");
        if( it != query.end() && !util::str_to_bool(it->second)) {
            _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                " \"dontProcess\" is set, will omit event pipeline processing.";
            applyPipeline = false;
        }
    }
    bool keepDiscriminated = false;
    if(applyPipeline) {
        auto it = query.find("keepDiscriminated");
        if( it != query.end() && util::str_to_bool(it->second)) {
            applyPipeline = true;
            _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                " \"keepDiscriminated\" is set, will respond even if"
                " event was discriminated by pipeline.";
        }
    }
   
    bool discriminated = true;
    do {
        bool done = false;
        if(rq["eventID"].IsScalar()) {
            const std::string val = rq["eventID"].as<std::string>();
            if(val == "next") {  // switch to next event
                util::reset(_evRef);
                _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                    " reading \"next\" event";
                _wasProcessed = false;
                done = _src->read(_evRef, _lmemRef);
            } else if( val == "previous" ) {  // switch to previous event
                if(!(_src->features() & AbstractEventSource::kBackwardReading)) {
                    _L << log4cpp::Priority::WARN << "Handling PATCH request:"
                        " \"previous\" event requested on source that does not"
                        " provide backward access";
                    throw errors::RequestError("Data source does not support"
                            " backward access", Msg::BadRequest);
                }
                util::reset(_evRef);
                _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                    " reading \"previous\" event";
                _wasProcessed = false;
                done = _src->read_prev(_evRef, _lmemRef);
            } else {  // try to interpret event ID and use random access
                if(!(_src->features() & AbstractEventSource::kRandomAccess)) {
                    _L << log4cpp::Priority::WARN << "Handling PATCH request:"
                        " event with specific ID requested on source that does not"
                        " provide random access";
                    throw errors::RequestError("Data source does not support"
                            " random access", Msg::BadRequest);
                }
                EventID eid;
                try {
                    eid = EventID::from_str(val);
                } catch(std::exception & e) {
                    throw errors::RequestError("Couldn't interpret event ID."
                            , Msg::BadRequest);
                }
                _wasProcessed = false;
                util::reset(_evRef);
                done = _src->read_by_id(eid, _evRef, _lmemRef);
            }
        } else {  // TODO: try to interpret composite ID, like run: ..., spill: ..., etc
            throw errors::RequestError("No \"eventID\" is not a scalar", Msg::BadRequest);
        }
        if(!done) {
            _current_response_object().status_code(Msg::NotModified);
        } else {
            _current_response_object().status_code(Msg::NoContent);
        }

        if(done) {
            _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                        " processing acquired event.";
            // process event if need
            if(applyPipeline && _pipelinePtr && *_pipelinePtr) {
                _eventProcessingResult = (*_pipelinePtr)->process(_evRef, _lmemRef);
                _wasProcessed = true;
                discriminated = _eventProcessingResult.second;
            } else {
                discriminated = false;
            }
            _L << log4cpp::Priority::DEBUG << "Handling PATCH request:"
                    " event was" << (_wasProcessed ? "" : " not")
               << " processed, was" << (discriminated ? "" : " not" )
               << " discriminated."
               ;
        }
    } while(discriminated && !keepDiscriminated);

    _L << log4cpp::Priority::DEBUG << "xxx Handling PATCH request: "    // XXX
        << _evRef.tracks.size() << " tracks in event";                  // XXX

    _L << log4cpp::Priority::DEBUG << "Handling PATCH request done.";

    YAML::Node empty;
    return empty;
}

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

