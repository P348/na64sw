#include "resources/geometry.hh"

namespace na64dp {
namespace util {
namespace http {


Geometry::Geometry( calib::Dispatcher & cdsp
                  , log4cpp::Category & logCat
                  ) : SpecializedResource<YAML::Node>(logCat)
                    , calib::SetupGeometryCache(cdsp, logCat)
                    , _L(logCat)
                    , _cUpdateID(0)
                    {
}

void
Geometry::handle_single_placement_update(const calib::Placement & pl) {
    YAML::Node item;
    bool computeMeasurementVectors = true;
    // ^^^ TODO: we're not in the request handling context now, so code block
    //     below will trigger segfault/assertion failure; move query param
    //     treatment to GET handler and remove "measurementVectors" data
    //     conditionally there from response node.
    //{   // If "measurementVectors" is given, append with transformed vectors
    //    auto it = _current_request_object().uri().query().find("measurementVectors");
    //    if( it != _current_request_object().uri().query().end()
    //            && str_to_bool(it->second) ) computeMeasurementVectors = true;
    //}

    item["center"] = YAML::Node(YAML::NodeType::Sequence);
    item["center"][0] = pl.center[0];
    item["center"][1] = pl.center[1];
    item["center"][2] = pl.center[2];

    item["rotation"] = YAML::Node(YAML::NodeType::Sequence);
    item["rotation"][0] = pl.rot[0];
    item["rotation"][1] = pl.rot[1];
    item["rotation"][2] = pl.rot[2];

    item["size"] = YAML::Node(YAML::NodeType::Sequence);
    item["size"][0] = pl.size[0];
    item["size"][1] = pl.size[1];
    item["size"][2] = pl.size[2];

    item["zone"] = pl.zone;
    item["material"] = pl.material;
    
    if( pl.suppInfoType == calib::Placement::kRegularWiredPlane ) {
        item["type"] = "regular-wired";
        item["nWires"] = pl.suppInfo.regularPlane.nWires;
        item["resolution"] = pl.suppInfo.regularPlane.resolution;
        if(computeMeasurementVectors) {
            item["measurementVectors"] = YAML::Node(YAML::NodeType::Map);
            util::Vec3 o, u, v;
            util::cardinal_vectors( pl.center, pl.size, pl.rot
                                  , o, u, v );

            item["measurementVectors"]["o"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["o"][0] = o.r[0];
            item["measurementVectors"]["o"][1] = o.r[1];
            item["measurementVectors"]["o"][2] = o.r[2];

            item["measurementVectors"]["u"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["u"][0] = u.r[0];
            item["measurementVectors"]["u"][1] = u.r[1];
            item["measurementVectors"]["u"][2] = u.r[2];

            item["measurementVectors"]["v"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["v"][0] = v.r[0];
            item["measurementVectors"]["v"][1] = v.r[1];
            item["measurementVectors"]["v"][2] = v.r[2];
        }
    } else if( pl.suppInfoType == calib::Placement::kIrregularWiredPlane ) {
        item["type"] = "irregular-wired";
        item["layout"] = pl.suppInfo.irregularPlane.layoutName;
        if(computeMeasurementVectors) {
            item["measurementVectors"] = YAML::Node(YAML::NodeType::Map);
            util::Vec3 o, u, v;
            util::cardinal_vectors( pl.center, pl.size, pl.rot
                                  , o, u, v );

            item["measurementVectors"]["o"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["o"][0] = o.r[0];
            item["measurementVectors"]["o"][1] = o.r[1];
            item["measurementVectors"]["o"][2] = o.r[2];

            item["measurementVectors"]["u"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["u"][0] = u.r[0];
            item["measurementVectors"]["u"][1] = u.r[1];
            item["measurementVectors"]["u"][2] = u.r[2];

            item["measurementVectors"]["v"] = YAML::Node(YAML::NodeType::Sequence);
            item["measurementVectors"]["v"][0] = v.r[0];
            item["measurementVectors"]["v"][1] = v.r[1];
            item["measurementVectors"]["v"][2] = v.r[2];
        }
    } else {
        NA64DP_RUNTIME_ERROR("Placement geometry sub-type %d is not supported"
                " by resource.", pl.suppInfoType );
    }

    _geometry[pl.name] = item;
    ++_cUpdateID;
}

YAML::Node
Geometry::get( const YAML::Node & rq, const URLParameters & ) {
    char bf[16];
    snprintf(bf, sizeof(bf), "%zu", _cUpdateID);
    {
        const std::string clientsVer = _current_request_object().get_header("If-Match", "*");
        if(clientsVer == bf) {
            _current_response_object().status_code(Msg::NotModified);
            return YAML::Node(YAML::NodeType::Null);
        }
    }
    // todo: geometry sub-entity?
    _current_response_object().set_header("ETag", bf);
    return _geometry;
}

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

