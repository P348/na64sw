#include "http/rest-json.hh"

namespace na64dp {
namespace util {
namespace http {

JSON
RESTTraits<JSON>::parse_request_body(const Msg::iContent & c) {
    // TODO: stream wrapper?
    std::string strJson;
    strJson.resize(c.size()+1);
    c.copy_to(strJson.data(), strJson.size());
    return JSON::parse(strJson);
}

void
RESTTraits<JSON>::set_content(ResponseMsg & msg, const JSON & js) {
    assert(!ResponseMsg.has_content());
    std::ostringstream oss;
    oss << js;
    msg.content(std::make_shared<StringContent>(oss.str()));
}

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp

