#include "http/rest-yaml.hh"

namespace na64dp {
namespace util {
namespace http {

YAML::Node
RESTTraits<YAML::Node>::parse_request_body( const Msg::iContent * c
                                          , log4cpp::Category & L
                                          ) {
    if((!c) || 0 == c->size()) return YAML::Node();
    // TODO: stream wrapper?
    std::string strYaml;
    strYaml.resize(c->size());
    c->copy_to(strYaml.data(), strYaml.size());
    try {
        return YAML::Load(strYaml);
    } catch( std::exception & e ) {
        L << log4cpp::Priority::ERROR
          << "Error while parsing request YAML: " << e.what();

        if( log4cpp::Priority::DEBUG <=
                log4cpp::Category::getInstance("httpServer.messageParsing").getPriority() ) {
            std::ostringstream ossd;
            ossd << "request yaml pl of size " << strYaml.size() << ": ";
            for(auto cc : strYaml) {
                if(!std::isspace(cc)) {
                    ossd << "'" << cc << "'";
                } else {
                    ossd << "' '";
                }
                ossd << " (" << (int) cc << "), ";
            }
        }  // (dbg) httpServer.messageParsing

        throw e;
    }
}

void
RESTTraits<YAML::Node>::set_content( ResponseMsg & msg
                                   , const YAML::Node & node
                                   , log4cpp::Category & L
                                   ) {
    assert(!msg.has_content());

    YAML::Emitter emitter;
    emitter << node;
    msg.content(std::make_shared<StringContent>(emitter.c_str()));
}

YAML::Node
RESTTraits<YAML::Node>::method_not_allowed() {
    YAML::Node node;
    node["errors"] = YAML::Node(YAML::NodeType::Sequence);
    node["errors"][0] = "Method not allowed";
    return node;
}

}  // namespace ::na64dp::util::http
}  // namespace ::na64dp::util
}  // namespace na64dp


