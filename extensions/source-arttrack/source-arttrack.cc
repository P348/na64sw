#include "source-arttrack.hh"
#include "artt-psource.hh"
#include "artt-track-model.hh"
#include "artt-ssurf.hh"

#include "na64event/data/event.hh"

#include <gsl/gsl_randist.h>

namespace na64dp {
namespace sources {

void
ArtTrackSource::handle_update( const na64dp::calib::Placements & placements ) {
    arttrack::create_sensitive_surfaces( _scene.detectors, placements );
    calib::Handle<calib::Placements>::handle_update(placements);
}

ArtTrackSource::ArtTrackSource( calib::Manager & mgr
                              , ::na64sw_runNo_t runNo
                              , const YAML::Node & cfg
                              , log4cpp::Category & logCat
                              , iEvProcInfo * epi
                              ) : AbstractNameCachedEventSource(mgr, logCat, "default", epi)
                                , calib::Handle<calib::Placements>("default", mgr)
                                , _runNo(runNo)
                                , _nEvent(0)
                                {
    // Create magnetic fields
    try {
        if( cfg["magField"] ) {
            arttrack::create_magnets(_scene.magnets, cfg["magField"]);
        }
    } catch( std::exception & e ) {
        _log.error( "Error while constructing magnets on ArtTrack scene: %s"
                , e.what() );
        throw;
    }
    // Sensitive surfaces will be created later
    // by `ArtTrackSource::handle_update(placements)`
    _scene.particleSource = arttrack::create_particle_source(cfg["beam"]);
    _nTracksPerEvent = cfg["nTracksPerEvent"].as<float>();
    _gslRng = gsl_rng_alloc(gsl_rng_taus);
    if(cfg["seed"])
        gsl_rng_set(_gslRng, cfg["seed"].as<int>());
}

void
ArtTrackSource::_copy_track( event::Event & event
                           , event::LocalMemory & lmem
                           , const std::vector<arttrack::Hit> & hits
                           , unsigned int mcTrackID
                           ) {
    const nameutils::DetectorNaming & nm 
        = calib::Handle<nameutils::DetectorNaming>::operator*();
    size_t sortP = 0;
    for( const arttrack::Hit & hit : hits ) {
        // Get the detector ID by name (that is same as sensitive surface's ID)
        // NOTE: this ID still has to be completed with wire number to identify
        //       a hit
        DetID did = nm[hit.surfacePtr->surfaceID];
        #if 0
        auto ir = event.apvClusters.emplace(did, lmem.create<event::APVCluster>(lmem));
        mem::Ref<event::APVCluster> clusRef = ir->second;
        util::reset(*clusRef);

        // Convert measured coordinate(s) into DAQ hits and clusters
        assert(std::isnan(hit.measurements[1]));  // TODO: support 2D, 3D hits. We consider only 1D hits currently.

        // Generate "measured" coordinate in wire units
        auto wpPtr = static_cast<const arttrack::WiredPlane*>(hit.surfacePtr);
        clusRef->position
            = (hit.measurements[0] * wpPtr->_nWires);

        clusRef->mcTruePosition[0] = hit.gHitTruth.c.x;
        clusRef->mcTruePosition[1] = hit.gHitTruth.c.y;
        clusRef->mcTruePosition[2] = hit.gHitTruth.c.z;
        #if 0  // XXX for debug
        std::cout << " generated " << hit.surfacePtr->surfaceID
                  << ": " << (hit.measurements[0] - 0.5) * wpPtr->_u.norm()*100
                  << "; " << hit.gHitTruth.c.x
                  << ", " << hit.gHitTruth.c.y
                  << ", " << hit.gHitTruth.c.z
                  << std::endl;  // XXX
        #endif
        #else
        // Create track score
        auto ir = event.trackScores.emplace(did, lmem.create<event::TrackScore>(lmem));
        mem::Ref<event::TrackScore> scoreRef = ir->second;
        util::reset(*scoreRef);
        scoreRef->mcTruth = lmem.create<event::MCTrueTrackScore>(lmem);
        util::reset(*scoreRef->mcTruth);

        // Convert measured coordinate(s) into DAQ hits and clusters
        assert(std::isnan(hit.measurements[1]));  // TODO: support 2D, 3D hits. We consider only 1D hits currently.

        // Generate "measured" coordinate in wire units
        //auto wpPtr = static_cast<const arttrack::WiredPlane*>(hit.surfacePtr);
        scoreRef->lR[0] = hit.measurements[0]; // * wpPtr->_nWires; (xxx, we use normalized u here)
        const auto lengthCFactor = util::Units::get_units_conversion_factor("m", "cm");
        for(int i = 0; i < 3; ++i) {
            scoreRef->mcTruth->globalPosition[i] = hit.gHitTruth.r[i]*lengthCFactor;
        }
        scoreRef->mcTruth->geant4TrackID = mcTrackID;
        scoreRef->sortPar = sortP;
        #endif
        ++sortP;
    }
}

bool
ArtTrackSource::read( event::Event & evRef
                    , event::LocalMemory & lmem
                    ) {
    // Trigger calib info updates
    evRef.id = EventID( _runNo, 0, _nEvent );
    calib_manager().event_id( evRef.id
                            , {0, 0} );
    // simulate number of tracks in event
    unsigned int nTracks = 0;
    while(0 == nTracks) nTracks = gsl_ran_poisson(_gslRng, _nTracksPerEvent);
    // simulate tracks
    for(unsigned int nTrack = 0; nTrack < nTracks; ++nTrack) {
        std::vector<arttrack::Hit> hits;
        arttrack::Vec3 particleStart, pc;
        const arttrack::ParticleDefinition * pDef;
        bool simulated = false;
        do {
            _scene.particleSource->get_new_particle( particleStart, pc, pDef );
            arttrack::State state;

            // TODO: assure that we start out of any magnetic field here or provide
            //       mechanism to start with helix model, if we are within magnetic
            //       field.
            state.trackPtr = new arttrack::LinearModel( particleStart, pc, *pDef );
            try {
                state.evaluate(_scene.detectors, _scene.magnets, hits);
            } catch( arttrack::errors::RangesViolation & rve ) {
                hits.clear();
                _log << log4cpp::Priority::ERROR
                     << "Helix/plane extrema period violation (numerical"
                        " instability?): period="
                    << rve.period << ", extrema={"
                    << rve.ex1 << " (+/-" << fabs(fabs(rve.period) - fabs(rve.ex1))
                    << "), "
                    << rve.ex2 << " (+/-" << fabs(fabs(rve.period) - fabs(rve.ex2))
                    << ")}";
                delete state.trackPtr;
                continue;
            } catch( arttrack::errors::BadInOutState & biose ) {
                hits.clear();
                _log << log4cpp::Priority::ERROR
                     << "Mag. field in/out condition failed, simulating another event.";
                delete state.trackPtr;
                continue;
            }
            delete state.trackPtr;
            simulated = true;
        } while(!simulated);
        if(hits.empty()) continue;
        // ^^^ TODO: flag to control whether it's possible to have no hits in
        //     event (yet, with unregistered tracks)
        ++_nEvent;

        // ... TODO digitize hits
        _copy_track( evRef, lmem, hits, nTrack );
    }

    return true;
}

ArtTrackSource::~ArtTrackSource() {
}

}  // namespace ::na64dp::surces
}  // namespace na64dp

REGISTER_SOURCE( ArtTrack
               , calibManager
               , cfg
               , ids
               , "Generates idealized tracks (for track reconstruction testing)."
               ) {
    std::string geomSrcURI;
    log4cpp::Category & logCat
        = log4cpp::Category::getInstance(cfg["_log"] ? cfg["_log"].as<std::string>() : "source");
    if( cfg["geometryFile"] ) {
        try {
            geomSrcURI = cfg["geometryFile"].as<std::string>();
        } catch(YAML::Exception & e) {
            log4cpp::Category::getInstance("vctr")
                << log4cpp::Priority::ERROR
                << "Failed to get \"geometryFile\" parameter as string from"
                   " arttrack config node: " << e.what();
            throw;
        }
    }
    ::na64sw_runNo_t runNo;
    try {
        runNo = cfg["runNo"].as<::na64sw_runNo_t>();
    } catch(YAML::Exception & e) {
        log4cpp::Category::getInstance("vctr")
            << log4cpp::Priority::ERROR
            << "Failed to get \"runNo\" parameter as integer from"
               " arttrack config node: " << e.what();
        throw;
    }
    return new na64dp::sources::ArtTrackSource( calibManager
            , runNo, cfg, logCat
            // ...
            //, epi
            );
}
