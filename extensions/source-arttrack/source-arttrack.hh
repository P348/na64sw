#pragma once

#include "na64sw-config.h"
#include "na64dp/abstractEventSource.hh"
#include "na64calib/setupGeoCache.hh"
#include "artt-scene.hh"

#include "na64calib/placements.hh"  // "placements" calib data type

#include "artt-state.hh"

#include <gsl/gsl_rng.h>

namespace na64dp {
namespace sources {

/**\brief Source creates artificial tracks.
 *
 * Useful for debugging tracking and estimation influence of various factors
 * like misalignment, resolution and noise impacts, etc.
 * */
class ArtTrackSource : public AbstractNameCachedEventSource
                     , public calib::Handle<calib::Placements>
                     //, public ...
                     {
protected:
    /// Scene instance
    arttrack::Scene _scene;
    /// Artificial run number
    const ::na64sw_runNo_t _runNo;
    /// Events counter
    size_t _nEvent;
    /// Average number of tracks per event
    float _nTracksPerEvent;

    gsl_rng * _gslRng;
protected:
    /// Constructs/updates scene geometry
    void handle_update( const calib::Placements & ) override;
    /// Fills event with artificial hits
    void _copy_track( event::Event &
                    , event::LocalMemory &
                    , const std::vector<arttrack::Hit> &
                    , unsigned int mcTrackID
                    );
public:
    /// Instantiates the object with YAML configuration
    ArtTrackSource( calib::Manager & mgr
                  , ::na64sw_runNo_t
                  , const YAML::Node & cfg
                  , log4cpp::Category & logCat
                  // ...
                  , iEvProcInfo * epi=nullptr );
    ~ArtTrackSource();
    /// Siulates a track and fills the event
    bool read( event::Event &, event::LocalMemory & lmem ) override;
};  // class ArtTrackSource

}  // namespace ::na64dp::sources
}  // namespace na64dp

