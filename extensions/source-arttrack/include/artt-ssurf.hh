#pragma once

#include "artt-types.hh"
#include "artt-surf.hh"

#include <string>

namespace arttrack {

struct iTrackModel;

struct iSensitiveSurf {
    /// A detector name or something meaningful, to distingush surfaces
    const std::string surfaceID;

    iSensitiveSurf(const std::string & surfaceID_) : surfaceID(surfaceID_) {}

    /// Fills hit and returns true if track crosses this sensitive surface
    /// If croesses, `t` reference is set to internal propagator's sorting
    /// parameter
    virtual bool probe_hit( const iTrackModel &
                          , Float_t tMax
                          , Hit & dest
                          , Float_t & t
                          ) const = 0;
    virtual ~iSensitiveSurf() {}
};  // struct iSensitiveSurf

/// A simple uniformly-wired plane measuring one local coordinate
struct WiredPlane : public iSensitiveSurf {
public:  // TODO: protected, made public for debug
    /// A bounded object defining the (planar) sensitive surface
    std::shared_ptr<Plane> _planePtr;
    /// Measurement unit vector
    Vec3 _u;
public:
    /// Ctr. parameterised with bounding rectangle and measurement vector
    WiredPlane( const std::string & name
              , std::shared_ptr<Plane> planePtr
              , const Vec3 & u
              ) : iSensitiveSurf(name)
                , _planePtr(planePtr)
                , _u(u)
                {}

    bool probe_hit( const iTrackModel &
                  , Float_t tMax
                  , Hit & dest
                  , Float_t & t
                  ) const override;

    /// Creates a hit from measurement vector (a vector from plane origin to
    /// intersection point)
    virtual Hit make_hit( const Vec3 & oX ) const;
};

}  // namespace arttrack

