#pragma once

#include <set>

#include "artt-surf.hh"

namespace arttrack {

struct iMagFieldVol {
public:
    /// Shall return field's bounding surfaces
    virtual const std::set<iSurface *> & boundaries() const = 0;
};  // struct iMagFieldVol

/// An implementation of the magnetic field defined by center,
/// box dimensions field. This simplistic representation is always a box with
/// edges parallel to axes of global frame and arbitrary orientation of the
/// uniform magnetic field vector within the volume.
class ParaxialFieldBox : public iMagFieldVol {
protected:
    Vec3 _c  ///< center of the box
       , _dims  ///< dimensions (width by X, depth by Y, height by Z)
       , _B  ///< magnetic induction vector
       ;
    /// Box faces
    std::set<iSurface *> _boundaries;
public:
    /// Creates a magnetic field (of `induction`) box with center at the
    /// `placement`, with `dimensions`
    ParaxialFieldBox( const Vec3 & placement
                    , const Vec3 & dims
                    , const Vec3 & induction
                    );
    ~ParaxialFieldBox();
    const Vec3 & B() const { return _B; }
    /// Returns box's boundary surfaces for track propagation
    virtual const std::set<iSurface *> & boundaries() const override {
        return _boundaries;
    }
};  // class ParaxialBox

}  // namespace arttrack

