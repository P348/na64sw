#pragma once

#include "artt-types.hh"

namespace arttrack {

struct iTrackModel;  // fwd

/**\brief Base surface class
 *
 * It was decided to make the surfaces virtual instead of the track models
 * because there are variety of analytical surfaces and for convenient
 * tracks there are few of them (generally, segment, helix).
 * */
struct iSurface {
    /**\brief Searches an intersection of current track and given surface
     *
     * Returned result is +1, -1, depending on sign of surfaces normal vector
     * projection. 0 returned if no intersection found.
     *
     * `sPt` and `tPt` are the out parameters referring to spatial point (a
     * 3-vector) and curve parameter of intersection point.
     * */
    virtual int intersection( const iTrackModel &
                            , Vec3 & sPt
                            , Float_t & tPt
                            , Float_t tStart=0
                            ) const = 0;
    virtual ~iSurface(){}
};

/// An infinite plane surface
struct Plane : public iSurface {
    Vec3 r, n;

    Plane(const Vec3 & r_, const Vec3 & n_) : r(r_), n(n_.unit()) {}

    /// Returns intersection with plane for given track model
    int intersection( const iTrackModel &
                    , Vec3 & sPt
                    , Float_t & tPt
                    , Float_t tStart
                    ) const override;
};

/// A parallelogram on plane.
///
/// Used to represent magnetic field box faces, coordinate-sensitive planes,
/// etc. A `Plane` subclass introducing checks for the out-of-active area
/// defined by finite dimensions.
///
/// U and V defines length of the parallelogram
struct Parallelogram : public Plane {
    Vec3 u, v;

    // NOTE: this is artificial restriction that is possible to overcome.
    //       Defines maximum number of intersection points of helix and plane
    //       to be tested for "within parallelogram". Currently our setup is
    //       pretty simple for this to have an impact...
    static int nPoints2Check;

    /// Defined by r0, u and v
    Parallelogram( const Vec3 & r
                 , const Vec3 & u_
                 , const Vec3 & v_
                 ) : Plane{ r, u_.cross(v_) }
                   , u(u_), v(v_)
                   {}

    /// Additionally to `Plane::intersection()` implementation, checks
    /// boundaries.
    ///
    /// \todo currently, considers only first few of intersection points to
    ///       be in the border of parallelogram
    int intersection( const iTrackModel &
                    , Vec3 & sPt
                    , Float_t & tPt
                    , Float_t tStart
                    ) const override;

    /// Checks if point lies within boundaries
    bool check_in_boundaries(const Vec3 &) const;
};

}  // namespace arttrack

