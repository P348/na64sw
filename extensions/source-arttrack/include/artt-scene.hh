#pragma once

#include "artt-types.hh"
#include "na64calib/placements.hh"

namespace YAML {
class Node;  // fwd
};

namespace arttrack {

class iParticleSource;

struct Scene {
    /// Set of detectors in scene
    Detectors detectors;
    /// Set of magnets defined in scene
    Magnets magnets;
    /// A particle source instance generating primary particles
    std::shared_ptr<iParticleSource> particleSource;
};

/// Constructs magnets from YAML description
int create_magnets(Magnets & dest, const YAML::Node & cfg);

/// Constructs sensitive surfaces (ArtTrack's "detectors") from placements
int create_sensitive_surfaces( Detectors & dest
                             , const na64dp::calib::Placements & placements
                             );

/// Creates a particle source instance
std::shared_ptr<iParticleSource> create_particle_source(const YAML::Node & cfg);

}  // namespace arttrack

