#pragma once

#include "na64util/numerical/linalg.hh"

#include <unordered_set>
#include <memory>
#include <cmath>
#include <cstring>

/**\file
 * \brief Defines simplistic linear algebra.
 *
 * Well, one would better borrow these arithmetics from GSL or ROOT, but it
 * might be a bit an overkill for such a simple task. We would like to keep
 * ArtTrack a rather academic excercise with minimal dependencies besides of
 * STL.
 *
 * Only 3-dimensional lineal vector algebra is defined with 3x3 matrix used
 * for rotations. Matrix inversion and vectorial product are done with static
 * formulas that shall bring up some performance.
 *
 * A transformation object is defined as 3x3 matrix + offset vector that
 * sometimes better to represent altogether with 4x4 matrix (that is not the
 * case for this small util). Direct transformation applies rotation first and
 * then makes an offset.
 * */

namespace arttrack {

using na64dp::util::Float_t;
using na64dp::util::Vec3;
using na64dp::util::Matrix3;
using na64dp::util::Transformation;

struct iSensitiveSurf;  // fwd (see artt-detector.hh)
struct iMagFieldVol;  // fwd (see artt-mag.hh)

typedef std::unordered_set< std::shared_ptr<iSensitiveSurf> > Detectors;
typedef std::unordered_set< std::shared_ptr<iMagFieldVol> > Magnets;

/// Internal hit type
struct Hit {
    /// Ptr to surface that created a hit
    const iSensitiveSurf * surfacePtr;
    /// Measured coordinates (up to three, if unused, set to NaN)
    Float_t measurements[3];
    /// Global coordinates of the hit point
    Vec3 gHitTruth;
};

struct ParticleDefinition {
    Float_t charge
          , mass;
    char name[64];

    /// A stub ctr -- creates dummy particle definition for tests only.
    ParticleDefinition() : charge(std::nan("0"))
                         , mass(std::nan("0"))
                         , name("nothing")
                         {}

    ParticleDefinition( Float_t charge_
                      , Float_t mass_
                      , const char * name_="dummy"
                      ) : charge(charge_)
                        , mass(mass_)
                        { strncpy(name, name_, sizeof(name)); }
};

}  // namespace arttrack
