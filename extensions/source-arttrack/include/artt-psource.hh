#pragma once

#include "artt-types.hh"

#include <random>
#include <map>
#include <list>
#include <algorithm>
#include <cassert>

namespace arttrack {

class iParticleSource {
public:
    virtual ~iParticleSource(){}
    /// Generates parameters for a new particle
    virtual void get_new_particle( Vec3 & startPosition
                                 , Vec3 & momentum
                                 , const ParticleDefinition *& pDef
                                 ) = 0;
};

template<typename EngineT>
class GenericRandomSource : public iParticleSource {
public:
    struct iGenerator {
        virtual Float_t get_value( EngineT & ) = 0;
        virtual ~iGenerator() {}
    };

    class Uniform : public iGenerator {
    protected:
        std::uniform_real_distribution<Float_t> _u;
    public:
        Uniform(Float_t a, Float_t b) : _u(a, b) {}
        Float_t get_value( EngineT & eng ) override { return _u(eng); }
    };

    struct Gaussian : public iGenerator {
    protected:
        std::normal_distribution<Float_t> _normal;
    public:
        Gaussian(Float_t c, Float_t sigma) : _normal(c, sigma) {}
        Float_t get_value( EngineT & eng ) override { return _normal(eng); }
    };
protected:
    // Random generators for position, direction, energy
    std::shared_ptr<iGenerator> position[3]
                              , direction[2]
                              , energy
                              ;
    /// Particle definitions
    std::vector<const ParticleDefinition *> pDefs;
    /// Uniform random number generator steering the distributions
    EngineT randomGen;
    /// Particle type distribution
    std::shared_ptr< std::discrete_distribution<> > pTypeDstrb;
public:
    GenericRandomSource( std::shared_ptr<iGenerator> posX
                       , std::shared_ptr<iGenerator> posY
                       , std::shared_ptr<iGenerator> posZ
                       , std::shared_ptr<iGenerator> theta
                       , std::shared_ptr<iGenerator> phi
                       , std::shared_ptr<iGenerator> energy_
                       , std::map<const ParticleDefinition *, int> pDefs_
                       ) : position{posX, posY, posZ}
                         , direction{theta, phi}
                         , energy(energy_)
                         {
        std::list<int> fracs;
        std::transform( pDefs_.begin(), pDefs_.end()
                      , std::back_inserter(fracs)
                      , [](std::pair<const ParticleDefinition * const, int> & p)
                            { return p.second; } );
        pTypeDstrb = std::make_shared< std::discrete_distribution<> >(fracs.begin(), fracs.end());
        for( auto p : pDefs_ ) pDefs.push_back(p.first);
    }

    /// Generates new particle
    void get_new_particle( Vec3 & startPosition
                         , Vec3 & momentum
                         , const ParticleDefinition *& pDef
                         ) override {
        // Particle type
        if(!pTypeDstrb) {
            assert( pDefs.size() == 1 );
            pDef = *pDefs.begin();
        } else {
            pDef = pDefs[(*pTypeDstrb)(randomGen)];
        }
        // Position
        for(int i = 0; i < 3; ++i) {
            startPosition.r[i] = position[i]->get_value(randomGen);
        }
        // Momentum -- magnitude
        Float_t pc = energy->get_value(randomGen);  // full energy
        pc = sqrt(pc*pc + 2*pDef->mass*pc);  // sqrt(E^2 - m^2)
        // Momentum -- direction
        Float_t theta = direction[0]->get_value(randomGen)
              , phi   = direction[1]->get_value(randomGen)
              ;
        momentum.c.x = cos(phi)*sin(theta)*pc;
        momentum.c.y = sin(phi)*sin(theta)*pc;
        momentum.c.z = cos(theta)*pc;
    }
};  // class GenericRandomSource<EngineT>

}  // namespace arttrack

