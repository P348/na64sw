#pragma once

#include "artt-types.hh"

#include <memory>
#include <tuple>
#include <vector>
#include <stdexcept>

namespace arttrack {
namespace errors {
class BadInOutState : public std::runtime_error {
public:
    BadInOutState(const char * s) : std::runtime_error(s) {}
};
}  // namespace ::arttrack::errors

struct iSensitiveSurf;
struct iMagFieldVol;
struct iTrackModel;

/**\brief State object corresponds to a single particle
 *
 * To evaluate a state object means trace particle through the scene till
 * current track model will become invalid due to whatever reason:
 *  1. Track goes to infinity (leaves scene)
 *  2. Track hits "border" of magnetic field
 *  3. (not implemented but can be) track scatters
 *  */
struct State {
    /// Set when track goes inside the magnetic field
    Magnets::value_type cField;

    /// Current track model used to propagate the track. Lifetime is strictly
    /// bound to the state object.
    iTrackModel * trackPtr;
    /// Track end `t`.
    Float_t tOut;

    State * nextState;

    State() : trackPtr(nullptr)
            , tOut(std::nan("0"))
            , nextState(nullptr)
            {}

    /// Recursively evaluates
    void evaluate( const Detectors & detectors
                 , const Magnets & fv
                 , std::vector<Hit> & hits
                 );
};

}  // namespace arttrack

