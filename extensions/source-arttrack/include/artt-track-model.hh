#pragma once

#include "artt-types.hh"
#include "na64sw-config.h"

#include <cassert>
#include <stdexcept>

#if defined(GTEST_FOUND) && GTEST_FOUND
#   include <gtest/gtest_prod.h>
class ArtTrack_HelixPlaneIntersection_Test;
class ArtTrack_HelixPlaneIntersectionGeneral_LowE_Test;
#endif

namespace arttrack {

struct Plane;

/**\brief A unit-dependent proportionality factor of Lorentz force
 *
 * Comes from \f$ F = k q \vec{v}(t) \times \bec{B}(\vec{r}(t)) \f$ relation.
 *
 * If the units are GeV/c for \f$p\f$, meter for \f$r\f$, and Tesla for
 * \f$B\f$, then \f$k = 0.29979 GeV/c T^{-1} m^{-1}\f$.
 * */
extern const Float_t k;

/// Abstract base of track model structs
struct iTrackModel {
    /// Type identifier (to avoid `dynamic_cast<>()`)
    const enum TrackModelType_t {
        kGeneric = 0,  ///< (not yet supported, once met shall be considered as mistake)
        kLinear = 1,  ///< linear track model
        kHelix = 2,  ///< helix track model
    } modelType;

    /// A (starting) momentum over charge
    const Vec3 p;
    /// Particle's definition
    const ParticleDefinition & pDef;

    /// Returns spatial vector based on the length parameter
    virtual Vec3 operator()(Float_t) const = 0;
protected:
    /// Base
    iTrackModel( TrackModelType_t tmTp
               , const Vec3 p_
               , const ParticleDefinition & pDef_
               ) : modelType(tmTp)
                 , p(p_)
                 , pDef(pDef_)
                 {}
public:
    virtual ~iTrackModel(){}
};  // class iTrackModel

struct LinearModel : public iTrackModel {
    Vec3 r0;

    LinearModel( Vec3 r0_
               , const Vec3 p_
               , const ParticleDefinition & pDef_
               ) : iTrackModel( kLinear
                              , p_
                              , pDef_
                              )
                 , r0(r0_)
                 {}

    /// Returns spatial vector based on the length parameter
    Vec3 operator()(Float_t t) const override {
        return r0 + p.unit()*t;
    }
};  // class LinearModel

/**\brief A helix track model
 *
 * Assumes the particle moving in constant magnetic field.
 * */
struct HelixModel : public iTrackModel {
    #if 0
    struct HelixPlaneProblem {
        /// Subject's helix
        const HelixModel & h;
        /// Subject's plane
        const Plane & plane;
        /// Function's extrema
        //const Float_t extrema[2];
        /// Coefficient for lookup
        //const Float_t A, B, C;
        /// Instantiates Helix problem caches from helix model and plane
        //static HelixPlaneProblem instantiate( const HelixModel & h_
        //                                    , const Plane & plane_ );
        //Float_t operator()(Float_t t) const
        //    { return A*sin(h.K*t) + B*cos(h.K*t) + C; }
    };

    static Float_t resolve_plane_intersection( Float_t initial
                                             , const HelixPlaneProblem & p );
    #endif
public:
    const Vec3 r0       ///< Helix starting position
             , h        ///< Normalized field vector \f$ \vec{B}/|B| \f$
             , tan0     ///< Initial momentum tangent vector \f$ \vec{p}/|p| \f$
             ;
    const Float_t alpha;  ///< \f$ |\vec{h} \times \vec{t}| \f$
    const Vec3 n0;        ///< Initial \f$(\vec{h} \times \vec{t})/\alpha\f$
    const Float_t delta   ///< \f$ \vec{h} \cdot \vec{t} \f$
                , K       ///< \f$ - k \psi |\vec{B}| \f$
                ;

    // these are not used in calculus, but needed for diagnostics
    const Vec3 origR0, origB, origP0;
    const ParticleDefinition & origPDef;

    HelixModel( const Vec3 & r0_
              , const Vec3 & B
              , const ParticleDefinition & pDef_
              , const Vec3 & p_
              ) : iTrackModel( kHelix, p_, pDef_ )
                , r0(r0_)
                , h(B.unit())
                , tan0(p.unit())
                , alpha(B.unit().cross(p.unit()).norm())
                , n0(h.cross(tan0)/alpha)
                , delta(h*tan0)
                , K( - k * (pDef.charge/p.norm()) * B.norm() )
                , origR0(r0_), origB(B), origP0(p_), origPDef(pDef_)
                {}

    /// Internal calculation type for values common to both `operator()`
    /// and `tangent_vector()` methods.
    //struct Cache;

    /// Returns tangent vector on length \f$t\f$.
    Vec3 tangent_vector(Float_t t) const;

    /// Returns spatial vector based on the length parameter
    Vec3 operator()(Float_t t) const override;

    Float_t period() const { return 2*M_PI/fabs(K); }
};

namespace errors {
class RangesViolation : public std::runtime_error {
public:
    const Float_t period, ex1, ex2;
    RangesViolation(Float_t T, Float_t e1, Float_t e2)
            : std::runtime_error("Found extrema are not within a period.")
            , period(T)
            , ex1(e1)
            , ex2(e2)
            {}
};
}  // namespace errors

/**\brief A representation of helix/plane intersection problem.
 * */
class HelixPlaneIntersection {
public:
    /**\brief Representation of ranges of periodic function
     *
     * For given period and points, yields a numbered sub-intervals.
     *
     *  -3             -2             -1              0              1    (period)
     *  |--a------b----|--a------b----|--a------b----|--a------b----|--a- (roots)
     *     |      |       |      |       |      |       |      |       |
     *                       -4     -3      -2      -1      0     1       (number of interval)
     * */
    struct Ranges {
        double x[2], T;

        Ranges( double T_, double x1, double x2 ) : x{x1, x2}, T(T_) {
            if( fabs(T_) < fabs(x1) || fabs(T) < fabs(x2) ) {
                throw errors::RangesViolation(T, x1, x2);
            }
            assert(fabs(T) >= fabs(x1));
            assert(fabs(T) >= fabs(x2));
            assert( (T >= 0 && x1 >= 0 && x2 >= 0)
                 || (T  < 0 && x1  < 0 && x2  < 0) );
            if(fabs(x1) > fabs(x2)) std::swap(x[1], x[0]);
        }

        std::pair<double, double>
        operator[](int n) const {
            std::pair<double, double> p;
            p.first  = x[abs(n  )%2];
            p.second = x[abs(n+1)%2];
            if(n > 0) {
                p.first  += ( n   /2)*T;
                p.second += ((n+1)/2)*T;
            } else {
                p.first  += ((n-1)/2)*T;
                p.second += (    n/2)*T;
            }
            if(T < 0) std::swap(p.first, p.second);
            return p;
        }

        int
        operator()(double v) const {
            int np;
            if(T > 0) {
                np = floor(v/T);
            } else {
                np = floor(v/T);
            }
            double b = np*T;
            np*=2;
            v -= b;
            if( fabs(v) <  fabs(x[0]) ) return np-1;
            if( fabs(v) >= fabs(x[1]) ) return np+1;
            return np;
        }
    };
protected:
    const HelixModel & _h;
    const Plane & _p;
    size_t _nMaxIter;
    double _epsAbs, _epsRel;
    // derived values
    double _extrema[2];

    std::ostream * _osPtr;

    /// Resolves plane/helix problem in the vicinity of given point
    double _resolve_monotonous(Float_t initial) const;
    /// Resolves plane/helix problem within the given range
    double _resolve_monotonous_in(double, double) const;

public:
    static double plane_intersection_eq( double t, void * p_ );
    static double plane_intersection_diff_eq( double t, void * p_ );
    static void plane_intersection_fdf( double x, void * p
                                      , double * y, double * dy );
    // this is not used
    static int elliptic_eq(const Float_t *, double *);

    HelixPlaneIntersection( const HelixModel & h
                          , const Plane & plane
                          , size_t nMaxIterMono=100
                          , double epsAbs=0
                          , double epsRel=1e-6
                          , std::ostream * os=nullptr
                          );
    Float_t resolve(Float_t start, int * nTurn=nullptr);
    #if defined(GTEST_FOUND) && GTEST_FOUND
    //FRIEND_TEST(ArtTrack, HelixPlaneIntersection);
    friend class ::ArtTrack_HelixPlaneIntersection_Test;
    friend class ::ArtTrack_HelixPlaneIntersectionGeneral_LowE_Test;
    #endif

    const HelixModel & helix_model() const { return _h; }
    const Plane & plane() const { return _p; }
    const double * extrema_array() const { return _extrema; }
};

}  // namespace arttrack

