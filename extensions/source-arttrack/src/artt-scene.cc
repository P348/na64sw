#include "artt-scene.hh"
#include "artt-mag.hh"
#include "artt-ssurf.hh"
#include "artt-psource.hh"

#include <yaml-cpp/yaml.h>

namespace arttrack {

// This factor defines length units conversion. In ArtTrack meters are used
// while in the rest of NA64 framework we're using cm.
static const Float_t lengthUnitFactor = .01  // cm to m
                   , magUnitsFactor = .1  // kGauss to Tesla
                   ;

int
create_magnets( Magnets & dest
              , const YAML::Node & cfg
              ) {
    // Construct "magnets"
    //      For magnetic field we only support a "Group" of "ParaxialBox"'es
    //      configuration currently. This document is expected to be similar
    //      to one defined in `na64geo/std-magfields.cc`
    if(!( cfg["_type"]
       && cfg["_type"].as<std::string>() == "Group"
       && cfg["fields"] )) {
        throw std::runtime_error("Only \"Group\" magnetic field of"
                " \"ParaxialBox\" entries is supported by ArtTrack so far.");
    }
    for(auto cField : cfg["fields"]) {
        Vec3 from{{ cField["from"][0].as<float>()
                  , cField["from"][1].as<float>()
                  , cField["from"][2].as<float>()
                  }}
           , to{{ cField["to"][0].as<float>()
                , cField["to"][1].as<float>()
                , cField["to"][2].as<float>() }}
           ;
        from *= lengthUnitFactor;
        to *= lengthUnitFactor;

        Vec3 B{{ cField["B"][0].as<float>()
               , cField["B"][1].as<float>()
               , cField["B"][2].as<float>()
               }};
        B = B*magUnitsFactor;
        Vec3 r = (to + from)/2;
        dest.insert(std::make_shared<ParaxialFieldBox>(r, to - from, B));
    }
    return 0;
}

int
create_sensitive_surfaces( Detectors & dest
                         , const na64dp::calib::Placements & placements
                         //, const YAML::Node & cfg
                         ) {
    // TODO: due to the SDC quirk entries are duplicated. We prevent
    //       repeatative occurences by names. This set shoud be removed once
    //       this behaviour will be fixed
    std::unordered_set<std::string> knownNames;
    for( auto e : dest ) {
        knownNames.emplace(e->surfaceID);
    }
    // iterate over placements
    for( auto pl_ : placements ) {
        const na64dp::calib::Placement & pl = pl_.data;
        auto ir = knownNames.emplace(pl.name);
        if(!ir.second) continue;  // already known (see comment above)
        if( pl.suppInfoType == na64dp::calib::Placement::kRegularWiredPlane ) {
            #if 0
            // for regular wired plane we create a rectanfular sensitive
            // surface
            // Quirk: rotation and movement must be applied wrt central point
            Vec3 r{{ pl.center[0], pl.center[1], pl.center[2] }}
               , d{{ pl.size[0],   pl.size[1],   pl.size[2]   }}
               ;
            r *= lengthUnitFactor;
            d *= lengthUnitFactor;
            Vec3 u{{d.r[0]/2, 0, 0}}
               , v{{0, d.r[1]/2, 0}}
               ;
            //r += d/2;
            Matrix3 rot = Matrix3::rotation( Vec3{{1, 0, 0}}, M_PI*pl.rot[0]/180)
                        * Matrix3::rotation( Vec3{{0, 1, 0}}, M_PI*pl.rot[1]/180)
                        * Matrix3::rotation( Vec3{{0, 0, 1}},-M_PI*pl.rot[2]/180)
                        ;
            // ^^^ TODO: investigate why it gives valid results for -rot_z...
            //     Wrong rotation matrix? left-side/right side CS?. Spent a week
            //     on this crap!
            u = (rot*u);
            v = (rot*v);
            r -= u;
            r -= v;
            u = u*2;
            v = v*2;
            #else
            Vec3 o, u, v;
            cardinal_vectors(pl.center, pl.size, pl.rot, o, u, v);
            o -= u/2;
            o -= v/2;
            o *= lengthUnitFactor;
            u *= lengthUnitFactor;
            v *= lengthUnitFactor;
            #endif
            auto geoPlane = std::make_shared<Parallelogram>(o, u, v);
            dest.insert(std::make_shared<WiredPlane>(pl.name, geoPlane, u));
        } 
        // else if ( ... ) {
        //    ...
        // }
        else {
            // this is normal; just warn user about warning on unknown entity
            std::cerr << pl_.srcDocID << ":" << pl_.lineNo << ": "
                << " entry \"" << pl.name << "\" has supplementary info"
                   " of unsupported type " << pl.suppInfoType << "."
                << std::endl;
        }
    }
    return 0;
}

// Default random engine type
typedef std::mt19937 EngineType;

static std::shared_ptr<GenericRandomSource<EngineType>::iGenerator>
_create_generator( const YAML::Node & cfg ) {
    typedef GenericRandomSource<EngineType> GRS;
    if( cfg[0].as<std::string>() == "uniform" ) {
        return std::make_shared<GRS::Uniform>( cfg[1].as<Float_t>()
                                             , cfg[2].as<Float_t>()
                                             );
    }
    if( cfg[0].as<std::string>() == "gaussian" ) {
        return std::make_shared<GRS::Gaussian>( cfg[1].as<Float_t>()
                                              , cfg[2].as<Float_t>()
                                              );
    }
    throw std::runtime_error("Unnknown distribution name.");
}

ParticleDefinition _particleTable[] = {
    {-1, 5.10998950e-7, "e-" },
    {+1, 5.10998950e-7, "e+" },
    {-1, 0.93827208816, "p-" },
    {+1, 0.93827208816, "p+" },
    {-1, 0.1056583755,  "mu-"},
    {+1, 0.1056583755,  "mu+"},
    {0, 0, ""}  // sentinel
};

std::shared_ptr<iParticleSource>
create_particle_source( const YAML::Node & cfg ) {
    if( cfg["_type"].as<std::string>() == "generic" ) {
        auto x = _create_generator( cfg["position"]["x"] )
           , y = _create_generator( cfg["position"]["y"] )
           , z = _create_generator( cfg["position"]["z"] )
           , theta = _create_generator( cfg["direction"]["theta"] )
           , phi   = _create_generator( cfg["direction"]["phi"] )
           , energy = _create_generator( cfg["energy"] )
           ;
        std::map<const ParticleDefinition *, int> pDefs;
        for( YAML::const_iterator it = cfg["particles"].begin()
           ; it != cfg["particles"].end(); ++it ) {
            const std::string & name = it->first.as<std::string>();
            auto c = _particleTable;
            for( ; '\0' != c->name[0]; ++c ) {
                if( name == c->name ) {
                    pDefs[c] = it->second.as<Float_t>();
                    break;
                }
            }
            if( '\0' == c->name[0] ) {
                throw std::runtime_error("Unknown particle type specified for"
                        " particle source" );
            }
        }
        return std::make_shared<GenericRandomSource<EngineType>>( x, y, z,
                theta, phi, energy, pDefs );
    }
    throw std::runtime_error("Unknown particle source type.");
}

}  // namespace arttrack

