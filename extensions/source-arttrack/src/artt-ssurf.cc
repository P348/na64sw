#include "artt-ssurf.hh"

#include <cassert>
#include <iostream>  // XXX

namespace arttrack {

bool
WiredPlane::probe_hit( const iTrackModel & track
                     , Float_t tMax
                     , Hit & dest
                     , Float_t & tRef
                     ) const {
    // check intersection
    Vec3 sPt;
    Float_t tPt;
    int dir = _planePtr->intersection( track, sPt, tPt, 0. );
    if(!dir) return false;  // no intersection
    if(tPt > tMax) return false;  // too far
    
    // vector from plane's center to intersection point
    Vec3 oX = sPt - _planePtr->r;
    // this is to check that "distance" vector lies within a plane
    if( std::fabs(oX*_planePtr->n) > 1e-4 ) {
        char errbf[256];
        snprintf( errbf, sizeof(errbf)
                , "Assertion failure: {%e, %e, %e}(=oX)*{%e, %e, %e}(=n)= %e > 1e-4"
                " (distance vec is outside the plane)", oX.c.x, oX.c.y, oX.c.z
                , _planePtr->n.c.x, _planePtr->n.c.y, _planePtr->n.c.z
                , std::fabs(oX*_planePtr->n)
                );
        throw std::runtime_error(errbf);
    }

    dest = make_hit(oX);
    dest.gHitTruth = sPt;
    tRef = tPt;
    return true;
}

Hit
WiredPlane::make_hit( const Vec3 & oX ) const {
    // now we project "distance" vector onto measurement direction
    //Float_t lU = _u.unit()*oX/_u.norm();
    Vec3 cv
        //= _planePtr->n.cross(_u).unit();
        = _u.cross(_planePtr->n).unit();
    Float_t lU = (oX.c.y * cv.c.x - oX.c.x * cv.c.y)
               / (_u.c.y * cv.c.x - _u.c.x * cv.c.y)
               ;
    #ifndef NDEBUG
    Float_t lV = (oX.c.y * _u.c.x - oX.c.x * _u.c.y)
               / (_u.c.x * cv.c.y - _u.c.y * cv.c.x)
               ;
    Vec3 check = _u*lU + cv*lV;
    assert( fabs(check.c.x - oX.c.x) < 1e-4 );
    assert( fabs(check.c.y - oX.c.y) < 1e-4 );
    assert( fabs(check.c.z - oX.c.z) < 1e-4 );
    #endif
    assert(lU <= 1);
    assert(lU >= 0);
    return {this, {lU, std::nan("0"), std::nan("0")}};
}

}  // namespace arttrack

