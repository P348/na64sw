#include "artt-mag.hh"
#include "artt-track-model.hh"

#include <gtest/gtest.h>

#include <random>

// uncomment this to get nice visualization
#define SPHERE_BOX_FILE

#ifdef SPHERE_BOX_FILE
#   include <fstream>
#endif

namespace arttrack {
namespace test {

static ParticleDefinition dummy;

TEST( ArtTrack, elementaryLinesCrossesParaxialBoxFaces ) {
    // Create a box at (1,1,1) of size 2x4x6
    ParaxialFieldBox box( Vec3{{1, 1, 1}}
                        , Vec3{{2, 4, 6}}
                        , Vec3{{0, 0, 0}}  // doesnt matter
                        );
    // Check (no) intersection with three lines
    {  // this line starts slightly above the box and shall not cross it
        LinearModel l( Vec3{{-4, 1,-3}}
                     , Vec3{{ 1, 0, 0}}
                     , dummy
                     );
        Vec3 sPt;
        Float_t tPt;
        for( const iSurface * sPtr : box.boundaries() ) {
            int rc = sPtr->intersection(l, sPt, tPt, 0);
            EXPECT_EQ(rc, 0);  // no intersection for all the boundaries
        }
    }
    {  // This shall intersect box twice, in "left" and "right" faces
        LinearModel l( Vec3{{-2, 1, 4}}
                     , Vec3{{ 1, 0,-2}}
                     , dummy
                     );
        Vec3 sPt;
        Float_t tPt;
        bool intersectLeft = false
           , intersectRight = false
           ;
        for( const iSurface * sPtr : box.boundaries() ) {
            int rc = sPtr->intersection(l, sPt, tPt, 0);
            auto * pargramPtr = dynamic_cast<const Parallelogram *>(sPtr);
            ASSERT_TRUE((bool) pargramPtr);
            if( rc == -1 ) {  // leaving the volume through the bottom face
                EXPECT_NEAR(sPt.c.x,  1, 1e-6);
                EXPECT_NEAR(sPt.c.y,  1, 1e-6);
                EXPECT_NEAR(sPt.c.z, -2, 1e-6);
                intersectRight = true;
                #if 0
                std::cout << " ..-1 at {" << sPt.r[0] << ", " << sPt.r[1] << ", " << sPt.r[2] << "}"
                          << " on pargram at {" << pargramPtr->r.c.x << ", "
                                                << pargramPtr->r.c.y << ", "
                                                << pargramPtr->r.c.z << "}"
                          << std::endl;
                #endif
            } else if( rc == 1) {  // incoming to the volume throught the left face
                EXPECT_NEAR(sPt.c.x,  0, 1e-6);
                EXPECT_NEAR(sPt.c.y,  1, 1e-6);
                EXPECT_NEAR(sPt.c.z,  0, 1e-6);
                intersectLeft = true;
                #if 0
                std::cout << " ..+1 at {" << sPt.r[0] << ", " << sPt.r[1] << ", " << sPt.r[2] << "}"
                          << " on pargram at {" << pargramPtr->r.c.x << ", "
                                                << pargramPtr->r.c.y << ", "
                                                << pargramPtr->r.c.z << "}"
                          << std::endl;
                #endif
            }
        }
        EXPECT_TRUE(intersectLeft);
        EXPECT_TRUE(intersectRight);
    }
}

TEST( ArtTrack, boxInSphere ) {
    // Create a box and put it somewhere, then imagine a spheric hull around it
    // and generate N random tracks directed from sphere surface towards
    // center. Assure that every generated track, if crosses faces, does it
    // exactly two times, "in" and "out".

    // Input parameters:
    //  - box center and size
    Vec3 boxCenter{{0, 0, 0}}
       , boxSizes{{1, 1, 1}}  //{{4, 16, 9}}
       ;
    // - random seed to use
    unsigned seed = 1997;
    // - number of samples
    size_t nSamples = 100;

    // Create a box
    ParaxialFieldBox box( boxCenter
                        , boxSizes
                        , Vec3{{0, 0, 0}}  // field, doesnt matter so far...
                        );
    // Calculate sphere radius to encompass the box
    Float_t r = boxSizes.norm()/2;
    
    std::mt19937 generator(seed);
    std::uniform_real_distribution<float> uniform01(0.0, 1.0);

    #ifdef SPHERE_BOX_FILE
    std::ofstream ofs("./box-in-sphere.dat");
    #endif

    for( size_t i = 0; i < nSamples; i++ ) {
        Float_t theta = 2 * M_PI * uniform01(generator);
        Float_t phi = acos(1 - 2 * uniform01(generator));
        // uniformely-distributed random point on sphere of radius `r'
        Vec3 t0{{ r * (Float_t) sin(phi) * (Float_t) cos(theta)
                , r * (Float_t) sin(phi) * (Float_t) sin(theta)
                , r * (Float_t) cos(phi)
                }};
        Vec3 start = boxCenter + t0;
        // create a "track" directed towards the sphere's center
        LinearModel l( start
                     , -t0.unit()
                     , dummy
                     );
        // collect intersections and make sure the "track" always goes "in" and
        // "out" of the box:
        Vec3 sPt;
        Float_t tPt;
        Vec3 vIn, vOut;
        bool goingIn = false
           , goingOut = false
           ;
        for( const iSurface * sPtr : box.boundaries() ) {
            int rc = sPtr->intersection(l, sPt, tPt, 0);
            auto * pargramPtr = dynamic_cast<const Parallelogram *>(sPtr);
            ASSERT_TRUE((bool) pargramPtr);
            if( rc == -1 ) {  // leaving the volume
                EXPECT_FALSE(goingOut);
                goingOut = true;
                vOut = sPt;
            } else if( rc == 1) {  // incoming to the volume
                EXPECT_FALSE(goingIn);
                goingIn = true;
                vIn = sPt;
            }
        }
        EXPECT_TRUE(goingIn);
        EXPECT_TRUE(goingOut);
        #ifdef SPHERE_BOX_FILE
        for( int i = 0; i < 3; ++i ) {
            ofs << start.r[i] << " ";
        }
        ofs << std::endl;
        for( int i = 0; i < 3; ++i ) {
            ofs << vIn.r[i] << " ";
        }
        ofs << std::endl << std::endl << std::endl;

        for( int i = 0; i < 3; ++i ) {
            ofs << vOut.r[i] << " ";
        }
        ofs << std::endl;
        Vec3 tEnd = l(2*r);
        for( int i = 0; i < 3; ++i ) {
            ofs << tEnd.r[i] << " ";
        }
        ofs << std::endl << std::endl << std::endl;
        #endif
    }
}

}  // namespace ::arttrack::test
}  // namespace arttrack


