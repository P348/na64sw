#include "artt-track-model.hh"
#include "artt-surf.hh"

#include <gtest/gtest.h>

TEST( ArtTrack, HelixPlaneRangesIterationIsCorrect ) {
    typedef arttrack::HelixPlaneIntersection::Ranges Ranges;
    arttrack::HelixPlaneIntersection::Ranges r[] = {
                Ranges( 10, 2, 7),
                Ranges(-10, -3, -8),
                Ranges( -.1, -2.44e-3, -3.33333e-2 ),
                Ranges( 100, 2e-3, 100 - 2e-6 )
            };

    for(unsigned int n = 0; n < sizeof(r)/sizeof(Ranges); ++n) {
        double prev = std::nan("0");
        for( int i = -6; i < 6; ++i ) {
            auto p = r[n][i];
            //std::cout << " #" << i << " : " << p.first << ", " << p.second << std::endl;
            EXPECT_LT(p.first, p.second);
            if( r[n].T > 0 ) {
                if(!std::isnan(prev)) {
                    EXPECT_NEAR(prev, p.first, 1e-6);
                }
                prev = p.second;
            } else {
                if(!std::isnan(prev)) {
                    EXPECT_NEAR(prev, p.second, 1e-6);
                }
                prev = p.first;
            }
        }
    }
}

TEST( ArtTrack, HelixPlaneRangeCorrectlyDefinedPositive ) {
    arttrack::HelixPlaneIntersection::Ranges r( 10, 2, 7 );
    struct Case {
        double v;
        int n;
    } cases[] = { {-15, -4}, {-11, -3}, {-9, -3}, {-4, -2}, {0, -1}
                , { 9, 1 }, {11, 1},  {13, 2}, {18, 3} };
    for(unsigned int i = 0; i < sizeof(cases)/sizeof(Case); ++i) {
        int n = r( cases[i].v );
        EXPECT_EQ(n, cases[i].n);
        auto p = r[n];
        EXPECT_LT( p.first, cases[i].v );
        EXPECT_GT( p.second, cases[i].v );
    }
}

TEST( ArtTrack, HelixPlaneRangeCorrectlyDefinedNegative ) {
    arttrack::HelixPlaneIntersection::Ranges rr( -10, -3, -8 );
    for(int i = -30; i < 30; ++i) {
        int n = rr(i);
        auto p = rr[n];
        ASSERT_LT(p.first, i);
        ASSERT_GE(p.second, i);
    }
}

//TEST( ArtTrack, HelixPlaneRangeCorrectlyDefinedNegative ) {
//}

static arttrack::ParticleDefinition electron(-1, 5.11e-4      )
                                  , hadron(  +1, 0.93827208816)
                                  ;

TEST( ArtTrack, na64HelixNumbers ) {
    using namespace arttrack;
    Float_t eKin = 100;
    HelixModel hm( Vec3{{0, 0, 0}}  // r_0
                 , Vec3{{0, 1.718, 0}}  // B, current =800A, B=1.718T
                 , electron  // particle definition
                 , Vec3{{0, 0, (Float_t) sqrt(eKin*eKin + 2*electron.mass*eKin) }}  // p
                 );
    // effective path in magnetic field, meters.
    // This value steers controlled values way too much for the precision to
    // which it is known in fact. Changing it by +-0.1m the beam offset at the
    // ECAL changes by 10-20mm and deflected angle by ~0.1mrad.
    const Float_t length = 3.9;

    Vec3 rOut = hm(length)  // effective length of two MBPLs?
       , tOut = hm.tangent_vector(length)
       ;
    Float_t deflection = atan2(tOut.r[0], tOut.r[2])
          , offsetAtECAL = (length + 10.2 + 3.5)*tan(deflection) + rOut.c.x
          ;

    EXPECT_NEAR(deflection, 0.020, 1e-3);
    EXPECT_NEAR(offsetAtECAL, 0.39, 1e-2);
}

TEST( ArtTrack, BendingMagnetCalculusTest ) {
    using namespace arttrack;
    // Values below are from ICTPs lecture for bending magnet calculus:
    //  https://indico.ictp.it/event/8728/session/6/contribution/22/material/slides/0.pdf
    // (see slides starting from #24)
    // We define a proton of 20 keV in a uniform magnetic field turning by 90
    // degrees, so that it approximately comes to the global frame's origin
    // (0, 0, 0.

    // A bit more accurate calculus
    const Float_t c = 2.99792458e8  // speed of light, SI
                , dimFact = 1e9/c /*1/sqrt(4*M_PI*alpha)*/  // 1/e, where e is Planck charge
                , orbRad = 0.22  // radius, m
                , eKin = 2e-5  // GeV
                , pc = sqrt(eKin*eKin + 2*eKin*hadron.mass)
                //, B = (pc * 1e9 /*eV*/ / c)/orbRad  // SI
                , B = - dimFact*pc/( hadron.charge * orbRad)
                ;

    HelixModel hm( Vec3{{-orbRad, -orbRad, 0}}  // r_0, starting point, shall bend by 90 degrees
                 , Vec3{{0, 0, B}}  // B
                 , hadron  // particle definition
                 , Vec3{{pc, 0, 0}}
                 );
    const Float_t length = M_PI*orbRad/2;  // radius of curvature

    // using this path length, calculate expected point and momentum for the
    // particle:
    Vec3 rOut = hm(length)
       , tOut = hm.tangent_vector(length)
       ;
    Float_t deflection = 180*atan2(tOut.r[0], tOut.r[1])/M_PI;

    // we expect the particle to be nearly at origin
    EXPECT_NEAR( rOut.c.x, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.y, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.z, 0, 1e-8 );
    // ant to be deflected nearly by pi/2
    EXPECT_NEAR( deflection, 0, 1e-3 );
    EXPECT_NEAR( tOut.norm(), 1, 1e-3 );
}

#if 0
TEST( ArtTrack, HelixParametersTest ) {
    // A bit more accurate calculus
    const Float_t c = 2.99792458e8  // speed of light, SI
                , dimFact = 1e9/c /*1/sqrt(4*M_PI*alpha)*/  // 1/e, where e is Planck charge
                , orbRad = 0.22  // radius, m
                , eKin = 2e-5  // GeV
                , pc = sqrt(eKin*eKin + 2*eKin*hadron.mass)
                //, B = (pc * 1e9 /*eV*/ / c)/orbRad  // SI
                , B = - dimFact*pc/( hadron.charge * orbRad)
                ;

    HelixModel h( Vec3{{0.34, -10, 12}}  // r_0, starting point, shall bend by 90 degrees
                //, Vec3{{0, 0, B}}  // B
                , Vec3{{0, 0, B}}  // B
                , hadron  // particle definition
                , Vec3{{-sqrt(pc/6), -sqrt(pc/3), -sqrt(pc/6)}}
                );

    // we expect that
    // 1. K is helix period
    {
        std::ofstream helixOfs("helix.dat")
                    , axisOfs("axis.dat")
                    //, rVecOfs("radius.dat")
                    ;
        int nPeriods = 1.5;
        for( int i = 0; i < 40; ++i ) {
            Float_t t = nPeriods * i*(2*M_PI/h.K)/40.;
            Vec3 r = h(t);
            helixOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << std::endl;
            Vec3 axis = h.r0 + h.h.unit() * h.delta * t + h.n0 * h.alpha / h.K;

            Vec3 delta = r - axis;
            axisOfs << axis.r[0] << "\t" << axis.r[1] << "\t" << axis.r[2] << "\t" //std::endl
                    << delta.c.x << "\t" << delta.c.y << "\t" << delta.c.z //<< std::endl
                    << std::endl;

            //Vec3 rv = ( h.n0*h.alpha*cos(h.K*t) + (h.h*h.delta - h.tan0)*sin(h.K*t) )*h.K + (axis - r);
            //rVecOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << "\t"//std::endl
            //        << rv.c.x << "\t" << rv.c.y << "\t" << rv.c.z << std::endl
            //        << std::endl;
        }
    }
    // 2. r = r0 + \theta \delta / K is axis line equation
    // 3. and ... is step


    #if 0
    const Float_t length = M_PI*orbRad/2;  // radius of curvature

    // using this path length, calculate expected point and momentum for the
    // particle:
    Vec3 rOut = hm(length)
       , tOut = hm.tangent_vector(length)
       ;
    Float_t deflection = 180*atan2(tOut.r[0], tOut.r[1])/M_PI;

    // we expect the particle to be nearly at origin
    EXPECT_NEAR( rOut.c.x, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.y, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.z, 0, 1e-8 );
    // ant to be deflected nearly by pi/2
    EXPECT_NEAR( deflection, 0, 1e-3 );
    EXPECT_NEAR( tOut.norm(), 1, 1e-3 );
    #endif
}
#endif

#if 0
TEST( ArtTrack, HelixPlaneIntersectionGeneral ) {
    using namespace arttrack;
    const Float_t c = 2.99792458e8  // speed of light, SI
                , dimFact = 1e9/c  // dimension factor
                , orbRad = 0.22  // radius, m
                , eKin = 2e-5  // GeV
                , pc = sqrt(eKin*eKin + 2*eKin*hadron.mass)
                //, B = (pc * 1e9 /*eV*/ / c)/orbRad  // SI
                , B = - dimFact*pc/( hadron.charge * orbRad)
                ;

    HelixModel h( Vec3{{0.34, -10, 12}}  // r_0, starting point
                //, Vec3{{0, 0, B}}  // B
                , Vec3{{0, 0, B}}  // B
                , hadron  // particle definition
                , Vec3{{-sqrt(pc/6), -sqrt(pc/6), -sqrt(pc/3)}}
                );
    Plane plane( Vec3{{-3, 12, -16}}
               , Vec3{{ 1,  1,   1}}.unit()
               );
    HelixPlaneIntersection problem(h, plane, 100, 0, 1e-6, &std::cout);
    {
        size_t nPeriods = 1;
        std::ofstream helixOfs("helix.dat")
                    , funcOfs("func.dat")
                    ;
        for( int i = 0; i < 40; ++i ) {
            Float_t t = nPeriods * i*(2*M_PI/h.K)/40.;
            Vec3 r = h(t);
            helixOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << std::endl;
            funcOfs << t << "\t"
                    << HelixPlaneIntersection::plane_intersection_eq(t, (void*) &problem)
                    << std::endl;
        }
    }
    double t = problem.resolve( 0. );
    // ASSERT_NEAR(-3.36, ) TODO

    #if 0
    // we expect that
    // 1. K is helix period
    {
        std::ofstream helixOfs("helix.dat")
                    , axisOfs("axis.dat")
                    , funcOfs("func.dat")
                    //, rVecOfs("radius.dat")
                    ;
        int nPeriods = 1.5;
        const HelixPlaneIntersection p(h, plane);
        for( int i = 0; i < 40; ++i ) {
            Float_t t = nPeriods * i*(2*M_PI/h.K)/40.;
            Vec3 r = h(t);
            helixOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << std::endl;
            Vec3 axis = h.r0 + h.h.unit() * h.delta * t + h.n0 * h.alpha / h.K;

            Vec3 delta = r - axis;
            axisOfs << axis.r[0] << "\t" << axis.r[1] << "\t" << axis.r[2] << "\t" //std::endl
                    << delta.c.x << "\t" << delta.c.y << "\t" << delta.c.z //<< std::endl
                    << std::endl;

            //funcOfs << t << "\t" << HelixModel::plane_intersection_eq( t, (void *) &p ) << std::endl;

            //Vec3 rv = ( h.n0*h.alpha*cos(h.K*t) + (h.h*h.delta - h.tan0)*sin(h.K*t) )*h.K + (axis - r);
            //rVecOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << "\t"//std::endl
            //        << rv.c.x << "\t" << rv.c.y << "\t" << rv.c.z << std::endl
            //        << std::endl;
        }
    }
    // 2. r = r0 + \theta \delta / K is axis line equation
    // 3. and ... is step
    #endif


    #if 0
    const Float_t length = M_PI*orbRad/2;  // radius of curvature

    // using this path length, calculate expected point and momentum for the
    // particle:
    Vec3 rOut = hm(length)
       , tOut = hm.tangent_vector(length)
       ;
    Float_t deflection = 180*atan2(tOut.r[0], tOut.r[1])/M_PI;

    // we expect the particle to be nearly at origin
    EXPECT_NEAR( rOut.c.x, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.y, 0, 1e-5 );
    EXPECT_NEAR( rOut.c.z, 0, 1e-8 );
    // ant to be deflected nearly by pi/2
    EXPECT_NEAR( deflection, 0, 1e-3 );
    EXPECT_NEAR( tOut.norm(), 1, 1e-3 );
    #endif
}
#endif

