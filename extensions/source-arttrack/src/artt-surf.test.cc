#include "artt-surf.hh"
#include "artt-track-model.hh"

#include <gtest/gtest.h>

namespace arttrack {
namespace test {

static ParticleDefinition dummy;

TEST( ArtTrack_Plane, simpleIntersectionsWithLineZ ) {
    Vec3 intersectionPoint;
    Float_t intersectionLength;

    Plane p1({{0, 0, 1}}, {{0, 0, -1}});
    LinearModel l({{0,0,0}}, {{0, 0, 1}}, dummy);
    int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
    ASSERT_TRUE( (bool)rc );
    EXPECT_EQ( rc, 1 );
    EXPECT_NEAR( intersectionLength,    1, 1e-6 );
    EXPECT_NEAR( intersectionPoint.c.x, 0, 1e-6 );
    EXPECT_NEAR( intersectionPoint.c.y, 0, 1e-6 );
    EXPECT_NEAR( intersectionPoint.c.z, 1, 1e-6 );
}

TEST( ArtTrack_Plane, simpleIntersectionsWithLine ) {
    Vec3 intersectionPoint;
    Float_t intersectionLength;

    Plane p1({{0, 0, 3}}, {{-1, -1, -1}});
    LinearModel l({{.5,.5,.5}}, {{1, 1, 1}}, dummy);
    int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
    ASSERT_TRUE( (bool) rc );
    EXPECT_EQ( rc, 1 );

    EXPECT_NEAR( intersectionLength, sqrt(3*.5*.5), 1e-6 );
    EXPECT_NEAR( intersectionPoint.c.x, 1, 1e-10 );
    EXPECT_NEAR( intersectionPoint.c.y, 1, 1e-10 );
    EXPECT_NEAR( intersectionPoint.c.z, 1, 1e-10 );
}

TEST( ArtTrack_Plane, noIntersectionWithParallelLine ) {
    Vec3 intersectionPoint;
    Float_t intersectionLength;

    Plane p1({{0, 0, 3}}, {{-1,  1, -1}});
    LinearModel l({{.5,.5,.5}}, {{1, 1, 0}}, dummy);
    int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
    ASSERT_EQ( 0, rc )
        << " intersection point is at t=" << intersectionLength
        << " {" << intersectionPoint.r[0]
        << ", " << intersectionPoint.r[1]
        << ", " << intersectionPoint.r[2]
        << "}";
}

TEST( ArtTrack_Pargram, simpleIntersectionChecks ) {
    // build romboid starting at 0, -1, -1
    Parallelogram p1( {{   -2, -2, -2 }}
                    , {{    4,  0,  0 }}
                    , {{    0,  0,  4 }}
                    );

    Vec3 intersectionPoint;
    Float_t intersectionLength;

    {  // check simple intersection
        LinearModel l({{0,0,0}}, {{0, -1, 0}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_TRUE( (bool)rc );
        EXPECT_EQ( rc, -1 );  // we intersecting it backward
        EXPECT_NEAR( intersectionLength,    2, 1e-6 );  // must be positive
        EXPECT_NEAR( intersectionPoint.c.x, 0, 1e-6 );
        EXPECT_NEAR( intersectionPoint.c.y,-2, 1e-6 );
        EXPECT_NEAR( intersectionPoint.c.z, 0, 1e-6 );
    }
    {  // check no intersection
        LinearModel l({{0,0,0}}, {{-2, -1, 0}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_EQ( 0, rc );
    }
    {  // check no intersection
        LinearModel l({{-1, 1,0}}, {{-1, -1, 0}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_EQ( 0, rc );
    }
}

TEST( ArtTrack_Pargram, intersectionChecks ) {
    // build romboid starting at 0, -1, -1
    Parallelogram p1( {{   -2, -2,  0 }}
                    , {{    2,  1,  1 }}
                    , {{    2, -1, -1 }}
                    );

    Vec3 intersectionPoint;
    Float_t intersectionLength;

    {  // check simple intersection
        LinearModel l({{0,0,0}}, {{0, -.2, 0}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_TRUE( (bool)rc );
        EXPECT_EQ( rc, 1 );
        EXPECT_NEAR( intersectionLength,    2, 1e-6 );
        EXPECT_NEAR( intersectionPoint.c.x, 0, 1e-6 );
        EXPECT_NEAR( intersectionPoint.c.y,-2, 1e-6 );
        EXPECT_NEAR( intersectionPoint.c.z, 0, 1e-6 );
    }
    {  // check no intersection
        LinearModel l({{0,0,0}}, {{-1, -1, 1 + 1e-3}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_EQ( 0, rc ) << " intersection point is at t=" << intersectionLength
            << " {" << intersectionPoint.r[0]
            << ", " << intersectionPoint.r[1]
            << ", " << intersectionPoint.r[2]
            << "}";
    }
    {  // check intersection
        LinearModel l({{0,0,0}}, {{-1 + 1e-3, -1, 1 - 1e-3}}, dummy);
        int rc = p1.intersection( l, intersectionPoint, intersectionLength, 0 );
        ASSERT_EQ( 1, rc );
    }
}

}  // namespace ::arttrack::test
}  // namespace arttrack


