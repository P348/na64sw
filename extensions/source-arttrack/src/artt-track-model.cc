#include "artt-track-model.hh"
#include "artt-surf.hh"
#include "na64util/gsl-integration.hh"

#include <iostream>
#include <cassert>
#include <fstream>  // XXX

#include <limits>

#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_linalg.h>

#include <cstdio>  // XXX

namespace arttrack {

const Float_t k = 0.299792458;


Vec3
HelixModel::tangent_vector(Float_t s) const {
    Float_t theta = K*s
          , cost = cos(theta)
          , sint = sin(theta)
          , cost1 = 1 - cost
          ;
    return h * delta * cost1 + tan0 * cost + n0 * alpha * sint;
}

Vec3
HelixModel::operator()(Float_t s) const {
    //Cache c;
    //Vec3 t = tangent_vector(s, &c);
    Float_t theta = K*s
          , cost = cos(theta)
          , sint = sin(theta)
          , cost1 = 1 - cost
          ;
    return r0
         + h * delta * (theta - sint) / K
         + tan0 * sint / K
         + n0 * alpha * cost1 / K
         ;
}

//
// Helix/plane problem

HelixPlaneIntersection::HelixPlaneIntersection( const HelixModel & h
                                              , const Plane & plane
                                              , size_t nMaxIterMono
                                              , double epsAbs
                                              , double epsRel
                                              , std::ostream * osPtr )
        : _h(h)
        , _p(plane)
        , _nMaxIter(nMaxIterMono)
        , _epsAbs(epsAbs)
        , _epsRel(epsRel)
        , _extrema{std::nan("0"), std::nan("0")}
        , _osPtr(osPtr)
        {
    // Calculate extrema points within a period
    const double A = h.alpha * ( h.n0 * plane.n )
               , B = (h.tan0 + h.h * h.delta) * plane.n
               , C = h.delta * (h.h * plane.n)
               , A2 = A*A, B2 = B*B
               , phi = atan2(B, A)
               , sqA2B2 = sqrt(A2 + B2)
               ;
    if( A ) {
        assert( fabs(sin(phi) - B/sqA2B2) < 1e-6 );
        assert( fabs(cos(phi) - A/sqA2B2) < 1e-6 );
        if( std::isfinite(phi) && sqA2B2 ) {
            double k = C/sqA2B2;
            assert( fabs(k) <= 1. );
            double asink = asin(k);
            _extrema[0] =           asink  - phi;
            _extrema[1] =   (M_PI - asink) - phi;
            for(int i = 0; i < 2; ++i) {
                if( _extrema[i] < 0 ) _extrema[i] += 2*M_PI;
                assert( fabs(B*cos(_extrema[i]) + A*sin(_extrema[i]) - C) < 1e-6 );
                _extrema[i] /= h.K;
                if(_osPtr) {
                    *_osPtr << "  extremum #" << i+1 << ": t(" << _extrema[i]
                            << " => " << (_extrema[i]*h.K/M_PI)
                            << "K*pi)*n_p(=" << _p.n << ") ~ "
                            << _h.tangent_vector(_extrema[i])*_p.n
                            << "; phase is " << phi/M_PI << "*pi, asin(k=" << k << ")="
                            << asink << std::endl;
                }
                //assert( fabs(_h.tangent_vector(_extrema[i])*_p.n) < 1e-6 );
            }
            if(_extrema[0] > _extrema[1]) std::swap(_extrema[0], _extrema[1]);
        }
    }
}

// A C-function calculating the helix/plane equation
double
HelixPlaneIntersection::plane_intersection_eq( double t
                                             , void * p_
                                             ) {
    auto p = reinterpret_cast<HelixPlaneIntersection *>(p_);
    return p->_h(t)*p->_p.n - p->_p.r*p->_p.n;
}

// A C-function calculating the helix/plane derivative
double
HelixPlaneIntersection::plane_intersection_diff_eq( double t
                                                  , void * p_
                                                  ) {
    auto p = reinterpret_cast<HelixPlaneIntersection *>(p_);
    return p->_h.tangent_vector(t)*p->_p.n;
}

void
HelixPlaneIntersection::plane_intersection_fdf( double x, void * p
                                              , double * y, double * dy ) {
    // TODO: benefit from it for faster convergence
    //auto p = reinterpret_cast<HelixPlaneProblem *>(p_);
    *y  = plane_intersection_eq( x, p );
    *dy = plane_intersection_diff_eq( x, p );
}

double
HelixPlaneIntersection::_resolve_monotonous(Float_t initial) const {
    int status;
    size_t iter = 0;
    gsl_root_fdfsolver * s;
    double x0, x = initial;
    gsl_function_fdf FDF;
    FDF.f = &plane_intersection_eq;
    FDF.df = &plane_intersection_diff_eq;
    FDF.fdf = &plane_intersection_fdf;
    FDF.params = const_cast<void*>((const void*) this);
    s = gsl_root_fdfsolver_alloc( gsl_root_fdfsolver_steffenson );
    gsl_root_fdfsolver_set( s, &FDF, x );
    do {
        iter++;
        try {
            status = gsl_root_fdfsolver_iterate( s );
        } catch( na64dp::errors::GSLError & e ) {
            std::cerr << e.what() << std::endl;  // XXX
            return std::nan("0");
        }
        x0 = x;
        x = gsl_root_fdfsolver_root( s );
        status = gsl_root_test_delta(x, x0, _epsAbs, _epsRel);
        if(status == GSL_SUCCESS)
            break;
    } while (status == GSL_CONTINUE && iter < _nMaxIter);
    gsl_root_fdfsolver_free( s );
    if(GSL_SUCCESS != status) return std::nan("0");
    return x;
}

double
HelixPlaneIntersection::_resolve_monotonous_in( double tLo
                                              , double tHi ) const {
    if( tLo == tHi ) return std::nan("0");
    if( tLo > tHi ) std::swap(tLo, tHi);
    int status = GSL_CONTINUE;
    double r = std::nan("0");
    gsl_function F;
    // set the subject function
    F.function = plane_intersection_eq;
    F.params = (void*) this;
    // allocate solver
    gsl_root_fsolver * sPtr
            = gsl_root_fsolver_alloc( gsl_root_fsolver_brent );
    gsl_root_fsolver_set(sPtr, &F, tLo, tHi);
    if( _osPtr )
        *_osPtr << "    bracketing in interval [" << tLo << ", "
                << tHi << "]" << std::endl;
    // iterate
    for( size_t nIt = 0
       ; nIt < _nMaxIter && status == GSL_CONTINUE
       ; nIt++) {
        status = gsl_root_fsolver_iterate( sPtr );
        r = gsl_root_fsolver_root( sPtr );
        tLo = gsl_root_fsolver_x_lower( sPtr );
        tHi = gsl_root_fsolver_x_upper( sPtr );
        status = gsl_root_test_interval( tLo, tHi, _epsAbs, _epsRel );
        if( _osPtr )
            *_osPtr << "      #" << nIt << "[" << tLo << ", " << tHi << "] "
                    << r << std::endl;
    }
    // free solver
    gsl_root_fsolver_free(sPtr);
    if(GSL_SUCCESS == status ) return r;
    // otherwise, no solution obtained
    return std::nan("0");
}

// aux class
struct _RangeIterator {
    double boundaries[4];
    int n;

    bool next(double & lower, double & upper) {
        if( n == 3 ) return false;
        lower = boundaries[n];
        upper = boundaries[++n];
        return true;
    }
};

Float_t
HelixPlaneIntersection::resolve(Float_t start, int * nRangePtr) {
    if( std::isnan(_extrema[0]) ) {
        // this means that function monotoneously changes by `t' -- no
        // subdivision must be applied and root finding is quite
        // straightforward
        assert(std::isnan(_extrema[1]));
        Float_t res = _resolve_monotonous(start);
        if(res < start) return std::nan("0");
        return res;
    }
    // otherwise assume sub-ranged task
    assert( (!std::isnan(_extrema[1])) && std::isfinite(_extrema[1]) );
    // For each turn starting from certain initial value, check values on
    // edge (and extrema)
    const Float_t period = 2*M_PI/_h.K;
    assert(period);
    int periodIncrement = period/fabs(period);
    if( _osPtr ) {
        *_osPtr << "Resolving plane/helix problem, extrema={"
               << _extrema[0]/period << ", " << _extrema[1]/period << "}, T="
               << period << "."
               << std::endl;
    }
    Ranges ranges(period, _extrema[0], _extrema[1]);
    {
        // Extrema define ranges of monotoneous change of the functions
        // under study. Within each range and intersection is present, if
        // function changes sign.
        double prevAvrg = std::nan("0");
        bool runAway = false;
        int nRange;
        if( nRangePtr && std::numeric_limits<int>::min() != *nRangePtr ) {
            nRange = *nRangePtr;
            if(_osPtr)
                *_osPtr << "  nRangePtr given and set to " << nRange
                        << ", using it to start lookup." << std::endl;
        } else {
            nRange = ranges(start);
            if(_osPtr)
                *_osPtr << "  nRangePtr not given or set to initial value"
                           ", starting with range #" << nRangePtr << " (of value "
                        << start << ")." << std::endl;
        }
        for(; !runAway; nRange += periodIncrement) {  // iterate over sub-ranges
            auto p = ranges[nRange];
            if(nRangePtr) *nRangePtr = nRange + periodIncrement;
            assert(p.first < p.second);
            double left  = plane_intersection_eq(p.first, this)
                 , right = plane_intersection_eq(p.second, this)
                 ;
            double fAvrg = 0;
            fAvrg += (left + right)/2;
            if( !std::isnan(prevAvrg) ) {
                // if absolute delta b/w this and previous iteration increases
                // we're moving away from solution => no intersection
                if( fabs(fAvrg) >= fabs(prevAvrg) ) {
                    if(_osPtr) *_osPtr << "  function is running away" << std::endl;
                    runAway = true;
                }
            }
            prevAvrg = fAvrg;
            if( _osPtr ) {
                *_osPtr << "    nRange=" << nRange << ", ["
                        << p.first  << " => " << left << ", "
                        << p.second << " => " << right << ")" << std::endl;
            }
            if( (left >= 0) == (right > 0) ) {
                if(_osPtr) *_osPtr << "    (interval does not contain root)" << std::endl;
                continue; // function does not change sign, skip sub-range
            }
            if( _osPtr )
                *_osPtr << "    looking for root in [" << p.first << ", "
                        << p.second << "]" << std::endl;
            double r = _resolve_monotonous_in(p.first, p.second);
            if(std::isnan(r)) {
                // This in principle must be considered as an error...
                if( _osPtr )
                    *_osPtr << "    did not converged." << std::endl;
                return r;
            }
            if( r && r <= start) {
                if( _osPtr )
                    *_osPtr << "    found root " << r << " is before desired"
                            << " starting value " << start << " (period is "
                            << period << "), continuing."
                            << std::endl;
                continue;
            } else if( _osPtr )
                *_osPtr << "    found root: " << r << "." << std::endl;
            return r;
        }
    }
    if( _osPtr ) {
        *_osPtr << "Exit interval iterating."
               << std::endl;
    }
    return std::nan("0");
}

int
HelixPlaneIntersection::elliptic_eq(const Float_t * pts, double *) {
    // resolve equation c1*x^2 + c2*y^2 + c3*x*y + c4*x + c5*y = c6
    double vs[25];
    double u[5] = {-1, -1, -1, -1, -1};
    for( int nc = 0; nc < 5; ++nc ) {
        const int ncn = nc*5;
        const Float_t x = pts[nc*2    ]
                    , y = pts[nc*2 + 1];
        //std::cout << "  xxx " << x << ", " << y << std::endl;  // XXX

        vs[ncn    ] = x*x;
        vs[ncn + 1] = y*y;
        vs[ncn + 2] = x;
        vs[ncn + 3] = y;
        vs[ncn + 4] = 1;

        u[nc] = -x*y;
    }
    gsl_matrix_view m = gsl_matrix_view_array(vs, 5, 5);
    gsl_vector_view f = gsl_vector_view_array(u, 5);
    gsl_vector * c = gsl_vector_alloc(5);
    int s;
    gsl_permutation * perms = gsl_permutation_alloc(5);
    gsl_linalg_LU_decomp(&m.matrix, perms, &s);
    gsl_linalg_LU_solve(&m.matrix, perms, &f.vector, c);
    //printf("c = \n");  // XXX
    //gsl_vector_fprintf(stdout, c, "  %g");  // XXX
    gsl_permutation_free(perms);

    double c1 = gsl_vector_get(c, 0)
         , c2 = gsl_vector_get(c, 1)
         , c3 = gsl_vector_get(c, 2)
         , c4 = gsl_vector_get(c, 3)
         , c5 = gsl_vector_get(c, 4)
         ;
    double k  = ( c3/(2*c1) - c4 ) / ( 2*c2 - 1./(2*c1) )
         , h  = ( - c3 - k ) /(2*c1)
         , B = 1/(c1*h*h + h*k + c2*k*k - c5)
         , A = c1*B
         , C = c2*B
         ;
    //std::cout << "  xxx k = " << k << std::endl
    //          << "  xxx h = " << h << std::endl;
    #if 0
    for( int nc = 0; nc < 5; ++nc ) {
        const Float_t x = pts[nc*2    ]
                    , y = pts[nc*2 + 1]
                    ;
        std::cout << "  0 ?= " << c1*x*x + c2*y*y + c3*x + c4*y + c5 + x*y << std::endl;
        std::cout << "  0 ?= " << A*(x*x + h*h - 2*x*h)
                                + B*(x*y - k*x - h*y + h*k)
                                + C*(y*y + k*k - 2*y*k)
                                - 1 << std::endl;
    }
    #endif     


    assert(A >= 0);
    assert(C >= 0);
    assert(B*B - 4*A*C < 0);
    double p = A - C
         , q = B*B + p*p
         , fac3 = sqrt(q*p*p)
         , fac1 = p*p/fac3
         #if 0
         , alpha = atan2( B, A - C)/2
         , cosAlpha = cos(alpha)
         , cosAlpha2 = cosAlpha*cosAlpha
         , sinAlpha = sin(alpha)
         , sinAlpha2 = sinAlpha*sinAlpha
         //, tanAlpha = tan(alpha)
         //, tanAlpha2 = tanAlpha*tanAlpha
         //, b = sqrt( cosAlpha2 - tanAlpha2 ) / (C - A*sinAlpha2)
         //, a = sqrt( cosAlpha2 / (A - sinAlpha2/(b*b)) )
         , a = sqrt(2/(A + C + 2*cosAlpha*sinAlpha))
         , b = sqrt(2/(A + C - 2*cosAlpha*sinAlpha))
         #endif
         ;
    double alpha[4], nom = 1 - fac1;
    
    if( nom < 0 ) {
        alpha[0] = alpha[1] = std::nan("0");
    } else {
        nom = sqrt(nom);
        alpha[0] = atan2( - nom,   B*p/(nom*fac3));
        alpha[1] = atan2(   nom, - B*p/(nom*fac3));
    }
    nom = 1 + fac1;
    assert(nom >= 0);
    nom = sqrt(nom);
    alpha[2] = atan2( - nom, - B*p/(nom*fac3));
    alpha[3] = atan2(   nom,   B*p/(nom*fac3));
    std::cout << "  #3 " << - nom << " / " << - B*p/(nom*fac3) << std::endl;  // XXX

    for( int i = 0; i < 4; ++i ) {
        std::cout << "  alpha_" << i << " = " << alpha[i]/M_PI << " pi ";
        double cosA = cos(alpha[i])
             , sinA = sin(alpha[i])
             , a, b
             ;
        int xxx = 0;
        if( alpha[i] < 0 && alpha[i] > -M_PI/2 ) {
            a = sqrt( 2.*cosA)/sqrt(  2*A*cosA + B*sinA);
            b = sqrt(-2.*sinA)/sqrt(- 2*A*sinA + B*cosA);
            xxx = 1;
        } else if( alpha[i] > 0 && alpha[i] < M_PI/2 ) {
            a = sqrt( 2.*sinA)/sqrt(  2*C*sinA + B*cosA);
            b = sqrt( 2.*cosA)/sqrt(  2*C*cosA - B*sinA);
            xxx = 2;
        } else /*if( 2*alpha[i] > M_PI/2 && 2*alpha[i] < -M_PI/2 )*/ {
            const double cosA2 = cosA*cosA
                       , sinA2 = sinA*sinA
                       //, cosA4 = cosA2*cosA2
                       //, sinA4 = sinA2*sinA2
                       ;
            a = sqrt(-cos(2*alpha[i]))/sqrt(C*sinA2 - A*cosA2);
            b = sqrt(-cos(2*alpha[i]))/sqrt(A*sinA2 - C*cosA2);
            xxx = 3;
        }
        #if 0
        if(cosA < 0 || sinA > 0) {
            a = b = std::nan("0");
        } else {
             a = sqrt( 2.*cosA)/sqrt(2*A*cosA + B*sinA);
                 //sqrt(sinA4 - cosA4)/sqrt(C*sinA2 - A*cosA2)
             b = sqrt(-2.*sinA)/sqrt(- 2*A*sinA + B*cosA);
                 //sqrt(sinA4 - cosA4)/sqrt(A*sinA2 - C*cosA2)
        }
        #endif
        std::cout << ", a = " << a
                  << ", b = " << b
                  << " (" << xxx << ")"
                  << std::endl
                  ;
    }

    #if 0
    std::cout << "  k = " << k << std::endl
              << "  h = " << h << std::endl
              << "  A = " << A << " ?= " << cosAlpha2/a2 + sinAlpha2/b2 << std::endl
              << "  B = " << B << " ?= " << 2*cosAlpha*sinAlpha*(1/a2 - 1/b2)  << std::endl
              << "  C = " << C << " ?= " << sinAlpha2/a2 + cosAlpha2/b2 << std::endl
              << "  alpha = " << alpha << "[=" << alpha*180/M_PI << " deg]" << std::endl
              << "  a = " << a << std::endl
              << "  b = " << b << std::endl
              << " xxx :: " << A + C << " ?= " << 1/a2 + 1/b2 << std::endl
              << " xxx = " << 2*h*c1 + k + c3 << std::endl
              << " xxx = " << 2*k*c2 + h + c4 << std::endl
              << " xxx = " << c1*h*h + h*k + c2*k*k - 1 - c5 << std::endl
              << (b*b + a*a)/(a*a*b*b) << " ?= " << (A+C) << std::endl
              << (b*b - a*a)/(a*a*b*b) << " ?= " << B/(2*cos(alpha)*sin(alpha)) << std::endl
            ;  // XXX
    #endif
    gsl_vector_free(c);
    return 0;
}

}  // namespace arttrack

