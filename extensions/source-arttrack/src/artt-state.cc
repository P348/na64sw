#include "artt-state.hh"
#include "artt-mag.hh"
#include "artt-ssurf.hh"
#include "artt-track-model.hh"

#include <limits>
#include <cassert>
#include <cmath>
#include <vector>
#include <map>

#include <iostream>  // XXX

namespace arttrack {

void
State::evaluate( const Detectors & detectors
               , const Magnets & fields
               , std::vector<Hit> & hits
               ) {
    assert(trackPtr);
    iTrackModel & track = *trackPtr;
    tOut = std::numeric_limits<Float_t>::infinity();  // "infinite" intersection
    // iterate over magnetic fields and find the closest intersection with
    // a "boundary" of magnetic field. This is only condition to change
    // a propagator.
    Float_t tPt;
    Vec3 sPt, sOut;
    int dir, lDir = 0;
    Magnets::value_type cMagVol;
    for( const auto & field : fields ) {
        for( const auto & bound : field->boundaries() ) {
            Float_t tPtThis;
            // get intersection with magnetic field boundary
            dir = bound->intersection( track  // current track model
                                     , sPt  // spatial vector of intersection
                                     , tPtThis  // track t-parameter of intersection
                                     , 0.  // "starting from" t-parameter
                                     );
            if(dir && 1e-6 >= std::fabs(tPtThis)) continue;  // crossing itself
            tPt = tPtThis;
            if(!dir) continue;
            if(tPt > tOut) continue;  // intersection is far than current closest one
            
            assert( tPt >= 0 );
            assert( std::isfinite(tPt) );  // for dir = +/-1, t must be finite

            // intersection is closer, update current "out"
            tOut = tPt;
            sOut = sPt;
            cMagVol = field;
            lDir = dir;
        }
    }
    // having valid `_in' points for current track model, collect
    // "hits". Since we would like to hits sorted by triggering order, we first
    // collect hits into a map automatically sorted by propagation
    // parameter `t'
    std::multimap<Float_t, Hit> thisPropHits;
    Float_t tPar;
    for( const auto & detector : detectors ) {
        Hit hit;
        auto plane = dynamic_cast<const WiredPlane*>(detector.get());  // XXX
        assert(plane);
        //auto geo = static_cast<const Parallelogram *>(plane->_planePtr.get());
        if( detector->probe_hit(track, tOut, hit, tPar) ) {
            thisPropHits.emplace(tPar, hit);
        }
    }
    // fill hits
    for(auto p : thisPropHits) hits.push_back(p.second);
    //std::cout << "  " << detectors.size() << " detectors considered." << std::endl;
    // If track does not intersect a magnetic field anymore, give up
    if( ! std::isfinite(tOut) ) {
        nextState = nullptr;
        return;
    }
    // Check that following conditions are satisfied:
    // - track falls into magnetic field, then current propagator is linear,
    //   the "last direction" is negative, cMagVol is set
    // - track leaves the magnetic field, then current propagator is helix,
    //   the "last direction" is positive, cMagVol is set
    assert(cMagVol);
    assert(lDir);

    // Otherwise, create and evaluate new state
    if( track.modelType == iTrackModel::kLinear ) {  // we're going into magnetic field in the next state
        if(lDir >= 0) {
            // This may happen if the particle was born in the magnetic field.
            // Another possibility is a bug when we missed its entering into
            // magnetic field. TODO This must be invetigated.
            throw errors::BadInOutState("Particle leaves volume with magnetic"
                    " field with linear track model.");
        }
        assert(lDir < 0);
        nextState = new State();
        nextState->cField = cMagVol;
        // todo: in principle we can choose different models, depending on the
        // magnetic field type here, but so far we're only implemented the
        // uniform field
        auto pfb = dynamic_cast<ParaxialFieldBox *>(cMagVol.get());
        assert(pfb);
        Vec3 B = pfb->B();
        nextState->trackPtr = new HelixModel( sOut
                                            , B
                                            , track.pDef
                                            , track.p
                                            );
    } else {  // next state must be a linear segment
        assert(lDir > 0);
        assert( track.modelType == iTrackModel::kHelix );
        auto & helix = static_cast<HelixModel &>(track);
        Vec3 newP = helix.tangent_vector(tOut).unit()*track.p.norm();

        nextState = new State();
        nextState->trackPtr = new LinearModel( sOut
                                             , newP
                                             , track.pDef
                                             );
    }
    nextState->evaluate(detectors, fields, hits);
}

}

