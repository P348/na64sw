#include "artt-mag.hh"

#include <iostream>  // XXX

namespace arttrack {

static const struct {
    int center[3];
    int u[3];
    int v[3];
} _paraxialBoxGuides[6] = {
    // uper face
    {{0, 0,  1}, {1, 0, 0}, { 0,  1, 0}},
    // down face 
    {{0, 0, -1}, {1, 0, 0}, { 0, -1, 0}},
    // left face
    {{-1, 0, 0}, {0, 0, 1}, { 0,  1, 0}},
    // right face
    {{ 1, 0, 0}, {0, 0, 1}, { 0, -1, 0}},
    // far face
    {{0, -1, 0}, {0, 0,-1}, { 1,  0, 0}},
    // near face
    {{0,  1, 0}, {0, 0, 1}, { 1,  0, 0}},
};

ParaxialFieldBox::ParaxialFieldBox( const Vec3 & placement
                                  , const Vec3 & dims
                                  , const Vec3 & induction
                                  ) : _c(placement)
                                    , _dims(dims)
                                    , _B(induction)
                                    {
    // create bounding surfaces: we define 6 rectangles (as parallelograms on
    // ortogonal edges) with normal vector oriented away from the box volume
    for( int i = 0; i < 6; ++i ) {
        auto & guides = _paraxialBoxGuides[i];
        Vec3 faceCenter = _c + Vec3{{ guides.center[0]*dims.r[0]/2.f
                                    , guides.center[1]*dims.r[1]/2.f
                                    , guides.center[2]*dims.r[2]/2.f
                                    }}
           , faceU = Vec3{{ guides.u[0]*dims.r[0]
                          , guides.u[1]*dims.r[1]
                          , guides.u[2]*dims.r[2]
                          }}
           , faceV = Vec3{{ guides.v[0]*dims.r[0]
                          , guides.v[1]*dims.r[1]
                          , guides.v[2]*dims.r[2]
                          }}
           ;
        faceCenter = faceCenter - faceU/2;
        faceCenter = faceCenter - faceV/2;

        //std::cout << "face: " << faceCenter << ", " << faceU << ", " << faceV << std::endl;  // XXX

        _boundaries.insert(new Parallelogram( faceCenter, faceU, faceV ));
    }
}

ParaxialFieldBox::~ParaxialFieldBox() {
    for(auto p: _boundaries) {
        delete p;
    }
}

}  // namespace arttrack

