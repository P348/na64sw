#include "artt-ssurf.hh"
#include "artt-track-model.hh"

#include <gtest/gtest.h>

namespace arttrack {
namespace test {

static ParticleDefinition dummy;

TEST( ArtTrack, infinitePlaneMeasurement ) {
    auto p = std::make_shared<Plane>( Vec3{{  0,  2, -1 }}
                                    , Vec3{{ -1, -1, 0 }}
                                    );
    WiredPlane wp("test", p, {{ 1, -1, 0 }});

    Hit hitObj = {nullptr, {0, 0, 0}};
    Float_t t;
    bool doHit = wp.probe_hit( LinearModel({{0, 0, 0}}, {{.1, .1, 0}}, dummy)
                             , 10
                             , hitObj
                             , t
                             );
    ASSERT_TRUE(doHit);
    EXPECT_EQ( hitObj.surfacePtr, &wp );
    EXPECT_FALSE( std::isnan(hitObj.measurements[0]) );
    EXPECT_NEAR( hitObj.measurements[0], 1, 1e-3 );
    EXPECT_TRUE( std::isnan(hitObj.measurements[1]) );
    EXPECT_TRUE( std::isnan(hitObj.measurements[2]) );
}

}  // namespace ::arttrack::test
}  // namespace arttrack

