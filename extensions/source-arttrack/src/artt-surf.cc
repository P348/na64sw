#include "artt-surf.hh"
#include "artt-track-model.hh"

#include <cmath>
#include <cassert>

#include <iostream>  // XXX

namespace arttrack {

int
Plane::intersection( const iTrackModel & track_
                   , Vec3 & sPt
                   , Float_t & tPt
                   , Float_t tStart
                   ) const {
    if( track_.modelType == iTrackModel::kGeneric ) {
        throw std::runtime_error("Generic track model intersecting the"
                " plane is not yet implemented.");
    }
    if( track_.modelType == iTrackModel::kLinear ) {
        const LinearModel & track = static_cast<const LinearModel &>(track_);
        Float_t a = n * track.p.unit();
        if( !a ) {  // track is parallel to the plane
            //printf( "{%.2e, %.2e, %.2e}*{%.2e, %.2e, %.2e} = %.2e\n"
            //      , n.r[0], n.r[1], n.r[2]
            //      , track.p.unit().r[0], track.p.unit().r[1], track.p.unit().r[2]
            //      , a );  // XXX
            tPt = std::numeric_limits<Float_t>::infinity();
            return 0;
        }
        tPt = n * (r - track.r0) / a;
        sPt = track(tPt);
        if(tPt < tStart) return 0;
        return a >= 0 ? 1 : -1;
    } else if( track_.modelType == iTrackModel::kHelix ) {
        // For helix/plane problem we simply take the first intersection point
        // after the given parameter and deny if it is not the case.
        const HelixModel & track = static_cast<const HelixModel &>(track_);
        HelixPlaneIntersection problem(track, *this);
        tPt = problem.resolve(tStart);
        if(std::isnan(tPt)) return 0;
        assert(tPt > tStart);
        sPt = track(tPt);
        return track.tangent_vector(tPt)*n >= 0 ? 1 : -1;  // TODO: adjust sign
    }
    throw std::runtime_error("Unknown model type.");
}

int Parallelogram::nPoints2Check = 10;

bool
Parallelogram::check_in_boundaries(const Vec3 & sPt) const {
    // vector from paralellogram's center to intersection point ("distance")
    Vec3 oX = sPt - r;
    // this is to check that "distance" vector lies within a plane... well,
    // more or less
    //if( std::fabs(oX*n) >= 1e-3 ) {
    //    std::cerr << "warning: " << oX << " X " << n << " = " << oX*n << std::endl;
    //}
    // u,v may be coplanar in one of the planes that will make the
    // decomposition to diverge
    Float_t lU = std::nan("0"), lV = std::nan("0");
    for(int k = 0; k < 3; ++k) {
        int l = (k+1)%3;
        lU = (oX.r[l]*v.r[k] - oX.r[k]*v.r[l]) / (u.r[l]*v.r[k] - u.r[k]*v.r[l]);
        lV = (oX.r[l]*u.r[k] - oX.r[k]*u.r[l]) / (u.r[k]*v.r[l] - u.r[l]*v.r[k]);
        if( std::isfinite(lU) && std::isfinite(lV) ) break;
    }
    //Float_t lU = oX*u.unit()/u.norm()
    //      , lV = oX*v.unit()/v.norm()
    //      ;
    // if projection is greater than vector's magnitude, it is not an
    // intersection
    if(lU > 1. || lU < 0) return false;
    if(lV > 1. || lV < 0) return false;
    // otherwise, we're in "active area"
    return true;
}

int
Parallelogram::intersection( const iTrackModel & track_
                           , Vec3 & sPt
                           , Float_t & tPt
                           , Float_t tStart=0
                           ) const {
    // invoke parent to get the intersection point, if it exists
    int rc = 0;
    if( track_.modelType == iTrackModel::kHelix ) {
        // For helix/finite plane problem we also keep track of the
        // intersection points to find out whether the track runs away from
        // parallelogram
        const HelixModel & track = static_cast<const HelixModel &>(track_);
        //try {  // TODO (see catch() block)
            HelixPlaneIntersection problem(track, *this);
            int nRange = std::numeric_limits<int>::min();
            for(int nPoint = 0; nPoint < nPoints2Check; ++nPoint) {
                tPt = problem.resolve(tStart, &nRange);
                if(std::isnan(tPt)) break;  // no intersections at all, give up
                sPt = track_(tPt);
                // Otherwise, the plane has intersection, but one need to check
                // the boundaries
                if( check_in_boundaries(sPt) ) {
                    Float_t a = n * track.tangent_vector(tPt);
                    rc = a > 0 ? 1 : -1;  // TODO: check this
                    break;
                }
            }
        //} catch( errors::RangesViolation & rve ) {            
            // TODO: binary dump of "orig..." values in helix and plane
            //       up to now we just ignore this events in callers
        //    throw;
        //}
    } else {
        rc = Plane::intersection(track_, sPt, tPt, tStart);
    }
    if(!rc) return rc;  // no intersection
    if(check_in_boundaries(sPt))
        return rc;
    else
        return 0;
}

}  // namespace arttrack

