#include "artt-scene.hh"
#include "artt-psource.hh"  // xxx?
#include "artt-state.hh"
#include "artt-track-model.hh"
#include "artt-ssurf.hh"

#include "na64app/app.hh"
#include "na64util/uri.hh"

#include <yaml-cpp/yaml.h>

#include <iostream>
#include <cstring>

#define PLOT_DIR "./helix-test/"  // dir to put plotting assets

static void
print_usage(const char * appName, std::ostream & os) {
    os << "ArtTrack application -- a simplistic simulation of track"
          " propagation." << std::endl
       << "This application is designed to test various functions of the"
          " simulation. `full' uses config file to test the full setup defined"
          " in YAML config, `planehelix' is a fixture to test basic plane/helix"
          " intersection." << std::endl
       << "Usage:" << std::endl
       << "  $ " << appName << "[full|planehelix]" << " (-h,--help|filename)"
       << std::endl
       << "In `full' mode the app generates \"hits\" produced by tracks"
          " passing through the set of magnetic fields ans sensitive surfaces."
       << "In `planehelix' mode the app builds a plane and helix and tries"
          " to find the intersection points producing .dat files and gnuplot"
          " scripts to visualize the results."
       << "In `ellipse` mode runs ellipse reconstruction on plane by five"
          " points. This routine is abandoned and not used by code anymore."
       << std::endl;
    // ^^^ todo: elaborate help a bit, add link to .pdf note
}

int
main_full( const YAML::Node & appCfg 
         , const std::string & cfgFile
         ) {
    // A scene object, will contain magnet fields and detectors descriptions
    arttrack::Scene scene;

    // Create magnetic field objects
    try {
        if( appCfg["magField"] ) {
            arttrack::create_magnets(scene.magnets, appCfg["magField"]);
        }
    } catch( std::exception & e ) {
        std::cerr << "Error while building up the magnets on scene (file \""
                  << cfgFile << "\"): " << e.what() << std::endl;
        return -2;
    }

    // For this application we construct detectors from the configuration file.
    // For the "source object" this positions will be taken from calibration
    // info.
    try {
        const int runNo = appCfg["runNo"].as<int>();
        // Load placements directly from file
        na64dp::calib::Placements placements; {
            const std::string filename
                = na64dp::util::expand_name(appCfg["detectors"].as<std::string>());
            
            sdc::Documents<p348reco::RunNo_t> docs;
            auto extCSVLoader = std::make_shared< sdc::ExtCSVLoader<p348reco::RunNo_t> >();
            extCSVLoader->defaults.dataType = "placements";
            docs.loaders.push_back(extCSVLoader);
            
            if( !docs.add(filename) ) {
                char errBuf[256];
                snprintf( errBuf, sizeof(errBuf)
                        , "Unable to pre-parse placements"
                          " description from file \"%s\"."
                        , filename.c_str() );
                throw std::runtime_error(errBuf);
            }

            placements = docs.load<sdc::SrcInfo<na64dp::calib::Placement>>(
                            p348reco::RunNo_t{(size_t) runNo});

            #if 0  // TODO: see FIXME note in SDC ExtCSVLoader::parse_stream()
            for( auto & p : placements ) {
                std::cout << p.data.name << ", ";
            }
            std::cout << std::endl;
            #endif
            
            if(  placements.empty() ) {
                // TODO: more details on error here
                throw std::runtime_error("Empty placements info loaded from file.");
            }
        }
        arttrack::create_sensitive_surfaces( scene.detectors
                                           , placements
                                           //, appCfg["digitization"]
                                           );
    } catch( std::exception & e ) {
        std::cerr << "Error while building up the detectors on scene (file \""
                  << cfgFile << "\"): " << e.what() << std::endl;
        return -2;
    }

    // create the particle emitter object
    scene.particleSource = arttrack::create_particle_source( appCfg["beam"] );

    // run the simulation loop
    std::vector<arttrack::Hit> hits;
    int nEvents = appCfg["nEvents"].as<size_t>();
    std::ofstream ofs("tracks.dat")
                , hitsOfs("hits.dat")
                , hitBarsOfs("hit-segms.dat")
                ;
    for(int nEvent = 0; nEvent < nEvents; ++nEvent ) {
        hits.clear();
        arttrack::Vec3 pos, pc;
        const arttrack::ParticleDefinition * pDef;
        scene.particleSource->get_new_particle( pos, pc, pDef );
        arttrack::State state;
        // TODO: assure that we start out of any magnetic field here or provide
        //       mechanism to start with helix model, if we are within magnetic
        //       field.
        state.trackPtr = new arttrack::LinearModel( pos, pc, *pDef );

        //std::cout << pDef->name << " at " << pos << " with pc=" << pc << std::endl;  // XXX
        try {
            state.evaluate(scene.detectors, scene.magnets, hits);
        } catch( arttrack::errors::RangesViolation & rve ) {
            std::cerr << "Helix/plane extrema period violation (numerical"
                         " instability?): period="
                << rve.period << ", extrema={"
                << rve.ex1 << " (+/-" << fabs(fabs(rve.period) - fabs(rve.ex1))
                << "), "
                << rve.ex2 << " (+/-" << fabs(fabs(rve.period) - fabs(rve.ex2))
                << ")}" << std::endl;
            std::cerr << "Simulation of event dropped due to"
                         " numerical instability." << std::endl;
            if(nEvent) --nEvent;
            continue;
        }

        // dump states chain
        for( arttrack::State * s = &state; s; s = s->nextState ) {
            Float_t tEnd = s->tOut;
            if(!std::isfinite(tEnd)) {
                tEnd = 20;  // TODO: configurable!
            }
            if( s->trackPtr->modelType == arttrack::iTrackModel::kLinear ) {
                arttrack::LinearModel & ltm = static_cast<arttrack::LinearModel &>(*(s->trackPtr));
                arttrack::Vec3 sBgn = ltm(0), sEnd = ltm(tEnd);
                ofs << sBgn.r[0] << "\t" << sBgn.r[1] << "\t" << sBgn.r[2] << std::endl
                    << sEnd.r[0] << "\t" << sEnd.r[1] << "\t" << sEnd.r[2] << std::endl;
            } else if( s->trackPtr->modelType == arttrack::iTrackModel::kHelix ) {
                arttrack::HelixModel & htm = static_cast<arttrack::HelixModel &>(*(s->trackPtr));
                for(int i = 0; i < 6; ++i) {
                    arttrack::Float_t t = (i * tEnd)/5.;
                    arttrack::Vec3 sPt = htm(t);
                    ofs << sPt.r[0] << "\t" << sPt.r[1] << "\t" << sPt.r[2]
                        << std::endl;
                }
            }
        }
        ofs << std::endl << std::endl;

        #if 0
        std::cout << "event #" << nEvent << std::endl;  // XXX
        for( const arttrack::Hit & hit : hits ) {
            std::cout << "  " << hit.surfacePtr->surfaceID << ": ";
            for(int i = 0; i < 3; ++i) {
                if( std::isnan(hit.measurements[i]) ) break;
                std::cout << hit.measurements[i] << ", ";
            }
            std::cout << std::endl;
        }
        #endif
        #if 1
        for( const arttrack::Hit & hit : hits ) {
            hitsOfs << hit.gHitTruth.r[0] << "\t"
                    << hit.gHitTruth.r[1] << "\t"
                    << hit.gHitTruth.r[2] << std::endl;
            // Draw linear segment corresponding to measurement for wired planes
            {
                auto wp = dynamic_cast<const arttrack::WiredPlane *>(hit.surfacePtr);
                assert(wp);
                auto parPtr = static_cast<const arttrack::Parallelogram *>(wp->_planePtr.get());
                assert(parPtr);
                arttrack::Vec3 bgn = parPtr->r + parPtr->u * hit.measurements[0]
                             , end = bgn + parPtr->v;
                hitBarsOfs << bgn.r[0] << "\t" << bgn.r[1] << "\t" << bgn.r[2] << std::endl << std::endl
                           << end.r[0] << "\t" << end.r[1] << "\t" << end.r[2] << std::endl
                           << std::endl << std::endl;
            }
        }
        #endif
    }

    std::ofstream detsOfs("detectors.dat")
                , script3d("full.gpl")
                ;
    int nDet = 0;  // XXX
    for( auto detPtr : scene.detectors ) {
        //if(nDet > 2) break;  // XXX
        auto wpPtr  = static_cast<arttrack::WiredPlane *>(detPtr.get());
        auto parPtr = static_cast<arttrack::Parallelogram *>(wpPtr->_planePtr.get());
        script3d << "set label at " << parPtr->r.c.x << ", "
                 << parPtr->r.c.y << ", " << parPtr->r.c.z
             << " \"" << detPtr->surfaceID << "\" point pt 7" << std::endl;
        const arttrack::Vec3 c[5] = {
                parPtr->r,
                parPtr->r + parPtr->u,
                parPtr->r + parPtr->u + parPtr->v,
                parPtr->r + parPtr->v,
                parPtr->r
            };
        for( int nc = 0; nc < 5; ++nc ) {
            detsOfs << c[nc].c.x << "\t" << c[nc].c.y << "\t" << c[nc].c.z << std::endl;
        }
        detsOfs << std::endl << std::endl;
        ++nDet;  // XXX
    }
    script3d << "splot \"tracks.dat\" w lines notitle,"
                " \"detectors.dat\" w lines notitle,"
                " \"hits.dat\" w points notitle,"
                " \"hit-segms.dat\" w lines notitle"
             << std::endl;
    script3d << "pause mouse close" << std::endl;

    return 0;
}

//                               ______________________________________________
// ____________________________/ Parametric ellipsis/parallelogram by 5 points
// For given 5 points and parallelogram, find an elliptic arc in terms of
// parametric ellipse that is within a given parallelogram.

struct Ellipse {
    arttrack::Float_t a, b, h, k, alpha;
    std::pair<arttrack::Float_t, arttrack::Float_t> operator()(double t) const {
        std::pair<arttrack::Float_t, arttrack::Float_t> p = {
                (a*cos(t) + h),
                (b*sin(t) + k)
            };
        return { p.first*cos(alpha) - p.second*sin(alpha)
               , p.first*sin(alpha) + p.second*cos(alpha)
               };
    }
};

int
main_ellipse( const YAML::Node & cfg_
            , const std::string & cfgFilename
            , const std::string & testCase ) {
    const YAML::Node cfg = cfg_[testCase];
    const YAML::Node & eln = cfg["ellipse"];
    Ellipse el{ eln["x0"].as<Float_t>()
              , eln["y0"].as<Float_t>()
              , eln["h"].as<Float_t>()
              , eln["w"].as<Float_t>()
              , (arttrack::Float_t) (eln["rotate"].as<Float_t>()*M_PI/180)
              };
    // Generate five random points on the ellipse
    arttrack::Float_t pts[5][2];
    std::srand(cfg["seed"] ? cfg["seed"].as<int>() : 1337);
    for(int i = 0; i < 5; ++i) {
        auto p = el(std::rand()*2*M_PI/RAND_MAX);
        pts[i][0] = p.first;
        pts[i][1] = p.second;
        //std::cout << "  xxy " << pts[i][0] << ", " << pts[i][1] << std::endl;  // XXX
    }

    double coeffs[5];
    arttrack::HelixPlaneIntersection::elliptic_eq((const arttrack::Float_t *) pts, coeffs);

    std::ofstream ofs("ellipse.dat");
    for(int i = 0; i < 100; ++i) {
        auto p = el(2*i*M_PI/99);
        ofs << p.first << "\t" << p.second << std::endl;
    }

    std::ofstream scriptFile("ellipse.gpl");
    for(int i = 0; i < 5; ++i) {
        scriptFile << pts[i][0] << " " << pts[i][1] << std::endl;
        //pts[i][0] = p.first;
        //pts[i][1] = p.second;
        //std::cout << "  xxy " << pts[i][0] << ", " << pts[i][1] << std::endl;  // XXX
    }

    return 0;

    #if 0
    using namespace arttrack;
    Float_t pts[5][2];
    // read points
    const auto ptsc = cfg["points"];
    for( int i = 0; i < 5; ++i ) {
        pts[i][0] = ptsc[i][0].as<Float_t>();
        pts[i][1] = ptsc[i][1].as<Float_t>();
    }
    #endif
}

//                                                         ____________________
// ______________________________________________________/ Plane/Helix problem

#if 0
static const _HelixPlaneCases & _get_conditions(const std::string & name) {
    auto c = _helixPlaneCases;
    for(; !c->label.empty(); ++c) {
        if( c->label == name ) return *c;
    }
    throw std::runtime_error(name);  // no conditions named ...
}
// Following asssets represents various cases of plane/helix problem together
// with expectations for starting and/or intersection points
static struct _HelixPlaneCases {
    std::string label;
    const arttrack::ParticleDefinition & particleDef;
    arttrack::Float_t momDir[3]
          , eKin
          , bDir[3]
          , B
          , planeR0[3]
          , planeN[3]
          , particleStart[3]
          ;
} _helixPlaneCases[] = {
/* label                particle,   momentum dir            energy      field dir       field       plane r0                plane normal    particle start r*/
{ "lowE",               hadron,     {-1, -1,  2},           2e-5,       {-.2,-2,-1},    -0.0928867, {0, -10, 12},  {-1, -1, -1},      {0.34, -10, 12} },
{ "parallelInf",        hadron,     { 1,  1,  1},           2e-5,       {  1, 0, 0},    -0.01,      {0,   0,  0},  {0,   0,  1},            {0, 0, 0} },
{ "", hadron }  // sentinel
};
#endif

/* A plane/helix intersection */
static int
main_planehelix( const YAML::Node & appCfg
               , const std::string & cfgFilename
               , const std::string & testCase
               ) {
    using namespace arttrack;
    const YAML::Node & cfg = appCfg[testCase];
    std::ostream * osPtr = &std::cout; // <- !
    //
    // Instantiate objects: field and helix equations according to conditions
    Float_t eKin = cfg["particle"]["energy"].as<Float_t>();
    ParticleDefinition particleDef = {
            cfg["particle"]["charge"].as<Float_t>(),
            cfg["particle"]["mass"].as<Float_t>()
        };
    HelixModel h( Vec3{{ cfg["particle"]["start"][0].as<Float_t>()
                       , cfg["particle"]["start"][1].as<Float_t>()
                       , cfg["particle"]["start"][2].as<Float_t>()
                       }}
                , Vec3{{ cfg["field"]["dir"][0].as<Float_t>()
                       , cfg["field"]["dir"][1].as<Float_t>()
                       , cfg["field"]["dir"][2].as<Float_t>()
                       }}.unit()*cfg["field"]["induction"].as<Float_t>()
                , particleDef
                , Vec3{{ cfg["particle"]["dir"][0].as<Float_t>()
                       , cfg["particle"]["dir"][1].as<Float_t>()
                       , cfg["particle"]["dir"][2].as<Float_t>()
                       }}.unit()*sqrt(eKin*eKin + 2*eKin*particleDef.mass)
                );
    Plane plane( Vec3{{ cfg["plane"]["r0"][0].as<Float_t>()
                      , cfg["plane"]["r0"][1].as<Float_t>()
                      , cfg["plane"]["r0"][2].as<Float_t>()
                      }}
               , Vec3{{ cfg["plane"]["n"][0].as<Float_t>()
                      , cfg["plane"]["n"][1].as<Float_t>()
                      , cfg["plane"]["n"][2].as<Float_t>()
                      }}.unit() );
    //
    // instantiate problem solver
    HelixPlaneIntersection problem( h, plane
                                  , cfg["solver"]["maxIter"].as<int>()
                                  , cfg["solver"]["epsabs"].as<double>()
                                  , cfg["solver"]["epsrel"].as<double>()
                                  , osPtr
                                  );
    //
    // Init a 3D plotting script to visualize the problem
    const std::string helixCurveScript
        = na64dp::util::format( PLOT_DIR "/%s-helix.gpl", testCase.c_str());
    std::ofstream script3d(helixCurveScript);
    // put plane pivot point and normal arrow vector
    script3d << "set label at " << plane.r.c.x << ", " << plane.r.c.y << ", " << plane.r.c.z
             << " \"pivot\" point pt 7" << std::endl;
    script3d << "set arrow 1 from "
             << plane.r.c.x << ", " << plane.r.c.y << ", " << plane.r.c.z
             << " rto " << (plane.n.c.x)/10 << ", "
                        << (plane.n.c.y)/10 << ", "
                        << (plane.n.c.z)/10
                        << std::endl;
    // put point start
    script3d << "set arrow 2 from "
             << h.r0.c.x << ", " << h.r0.c.y << ", " << h.r0.c.z
             << " rto " << (h.p.unit().c.x)/10 << ", "
                        << (h.p.unit().c.y)/10 << ", "
                        << (h.p.unit().c.z)/10
                        << std::endl;

    //
    // Resolve problem

    // depict intersection points, if any, keep the limits to draw the curve
    bool hasIntersections = false;
    double lastIntersection = -std::numeric_limits<double>::infinity();
    {
        double t;
        int nInt = std::numeric_limits<int>::min();
        size_t nRoot = 0;
        while( (!std::isnan(t = problem.resolve(0, &nInt)))
            && (nRoot < cfg["solver"]["nMaxRoots"].as<size_t>())
            ) {
            assert( nInt != std::numeric_limits<int>::min() );
            hasIntersections = true;
            Vec3 ir = h(t);
            script3d << "set label at " << ir.c.x << ", " << ir.c.y << ", " << ir.c.z
                     << " \"#" << nInt << "\" point pt 6" << std::endl;
            lastIntersection = t;
            ++nRoot;
        }
    }

    // This used for visual inspection of results. Enable block to get the
    // .dat files for plotting
    const std::string
          scriptFName = na64dp::util::format(PLOT_DIR "/%s-func.gpl", testCase.c_str()),
          helixOfsName = na64dp::util::format(PLOT_DIR "/%s-helix.dat", testCase.c_str()),
          funcOfsName = na64dp::util::format(PLOT_DIR "/%s-func.dat", testCase.c_str())
          ; {
        int nPeriods = 2;  // number of periods to dump the helix, used if no intersection met
        int helixNPoints = cfg["plot"]["nPoints"].as<int>();  // number of points to dump the helix per period
        Float_t start = cfg["plot"]["helixStart"].as<Float_t>();  // start parameter for helix (period units)
        std::ofstream scriptF(scriptFName)
                    , helixOfs(helixOfsName)  // helix 3d .dat file
                    , funcOfs(funcOfsName)  // intersection function 2d .dat file
                    ;
        scriptF << "set zeroaxis" << std::endl
                << "set xtics (";
        Float_t A = h.alpha * ( h.n0 * plane.n )
              , B = (h.tan0 + h.h * h.delta) * plane.n
              , C = h.delta * (h.h * plane.n)
              //, phi = atan2(B, A)
              //, sqA2B2 = sqrt(A*A + B*B)
              ;
        if( !hasIntersections ) {
            lastIntersection = nPeriods*2*M_PI/fabs(h.K);
        }
        //for( int nPeriod = 0; nPeriod < nPeriods; ++nPeriod ) {
        //scriptF << (nPeriod ? ", " : "") << "\"" << nPeriod + start << "\" " << nPeriod*h.period();
        for( int nPoint = 0; nPoint < helixNPoints; ++nPoint ) {
            //Float_t t = (nPeriod + start + Float_t(nPoint)/helixNPoints)*(2*M_PI/fabs(h.K));
            //Float_t t = start + i/helixNPoints //(start + nPeriods) * i*(2*M_PI/h.K)/helixNPoints;
            Float_t t = start + Float_t(nPoint)/(helixNPoints-1)*lastIntersection;
            Vec3 r = h(t);
            helixOfs << r.c.x << "\t" << r.c.y << "\t" << r.c.z << std::endl;
            funcOfs << t << "\t"
                    << HelixPlaneIntersection::plane_intersection_eq(t, (void*) &problem) << "\t"
                    << problem.helix_model().tangent_vector(t)*problem.plane().n << "\t"
                    << A*sin(h.K*t) + B*cos(h.K*t) - C
                    << std::endl;
        }
        //scriptF << ", \"" << nPeriod << ":1\" " << nPeriod*h.period() + problem.extrema_array()[0] << ", "
        //        << "\"" << nPeriod << ":2\" " << nPeriod*h.period() + problem.extrema_array()[1];
        //}
        scriptF << ")" << std::endl
                << "plot \"" << funcOfsName << "\" u 1:2 w lines title \"f(t)\", "
                    "\"" << funcOfsName << "\" u 1:3 w lines title \"f'(t)\", "
                    "\"" << funcOfsName << "\" u 1:4 w lines title \"f_m'(t)\", "
                << std::endl
                << "pause mouse close" << std::endl;
    }


    script3d << "splot \"" << helixOfsName << "\" w lines, (" << plane.n*plane.r
                            << " - (" << plane.n.c.x << ")*x"
                            << " - (" << plane.n.c.y << ")*y"
                            << ")/(" << plane.n.c.z << ")" << std::endl;
    script3d << "pause mouse close" << std::endl;

    return 0;
}

static int
main_helix_parallelogram( const YAML::Node & appCfg
                        , const std::string & cfgFilename
                        , const std::string & testCase) {
    using namespace arttrack;
    const YAML::Node & cfg = appCfg[testCase];
    //
    // Instantiate objects: field and helix equations according to conditions
    Float_t eKin = cfg["particle"]["energy"].as<Float_t>();
    ParticleDefinition particleDef = {
            cfg["particle"]["charge"].as<Float_t>(),
            cfg["particle"]["mass"].as<Float_t>()
        };
    HelixModel h( Vec3{{ cfg["particle"]["start"][0].as<Float_t>()
                       , cfg["particle"]["start"][1].as<Float_t>()
                       , cfg["particle"]["start"][2].as<Float_t>()
                       }}
                , Vec3{{ cfg["field"]["dir"][0].as<Float_t>()
                       , cfg["field"]["dir"][1].as<Float_t>()
                       , cfg["field"]["dir"][2].as<Float_t>()
                       }}.unit()*cfg["field"]["induction"].as<Float_t>()
                , particleDef
                , Vec3{{ cfg["particle"]["dir"][0].as<Float_t>()
                       , cfg["particle"]["dir"][1].as<Float_t>()
                       , cfg["particle"]["dir"][2].as<Float_t>()
                       }}.unit()*sqrt(eKin*eKin + 2*eKin*particleDef.mass)
                );
    Parallelogram pargram( Vec3{{ cfg["parallelogram"]["r"][0].as<Float_t>()
                                , cfg["parallelogram"]["r"][1].as<Float_t>()
                                , cfg["parallelogram"]["r"][2].as<Float_t>()
                                }}
                         , Vec3{{ cfg["parallelogram"]["u"][0].as<Float_t>()
                                , cfg["parallelogram"]["u"][1].as<Float_t>()
                                , cfg["parallelogram"]["u"][2].as<Float_t>()
                                }}
                         , Vec3{{ cfg["parallelogram"]["v"][0].as<Float_t>()
                                , cfg["parallelogram"]["v"][1].as<Float_t>()
                                , cfg["parallelogram"]["v"][2].as<Float_t>()
                                }}
                         );
    // Get intersection
    Vec3 sPt;
    Float_t tPt;
    int rc = pargram.intersection( h, sPt, tPt, 0 );

    // Make gnuplot assets
    const std::string helixDatName = na64dp::util::format(PLOT_DIR "/%s-pghlx.gpl", testCase.c_str());
    std::ofstream script3d( na64dp::util::format(PLOT_DIR "/%s-pgram.gpl", testCase.c_str()) )
                , helixDat( helixDatName )
                ;
    
    // Parallelogram pivot, normal vector and edges
    script3d << "set label at "
                    << pargram.r.c.x << ", "
                    << pargram.r.c.y << ", "
                    << pargram.r.c.z
                    << " \"pivot\" point pt 7"
                    << std::endl;
    // normal vector
    script3d << "set arrow 1 from "
             << pargram.r.c.x << ", " << pargram.r.c.y << ", " << pargram.r.c.z
             << " rto " << pargram.n.c.x << ", "
                        << pargram.n.c.y << ", "
                        << pargram.n.c.z
                        << std::endl;
    // u
    script3d << "set arrow 2 from "
             << pargram.r.c.x << ", " << pargram.r.c.y << ", " << pargram.r.c.z
             << " rto " << pargram.u.c.x << ", "
                        << pargram.u.c.y << ", "
                        << pargram.u.c.z
                        << std::endl;
    // v
    script3d << "set arrow 3 from "
             << pargram.r.c.x << ", " << pargram.r.c.y << ", " << pargram.r.c.z
             << " rto " << pargram.v.c.x << ", "
                        << pargram.v.c.y << ", "
                        << pargram.v.c.z
                        << std::endl;
    // u'
    Vec3 origU = pargram.r + pargram.v;
    script3d << "set arrow 4 from "
             << origU.c.x << ", " << origU.c.y << ", " << origU.c.z
             << " rto " << pargram.u.c.x << ", "
                        << pargram.u.c.y << ", "
                        << pargram.u.c.z
                        << std::endl;
    // v'
    Vec3 origV = pargram.r + pargram.u;
    script3d << "set arrow 5 from "
             << origV.c.x << ", " << origV.c.y << ", " << origV.c.z
             << " rto " << pargram.v.c.x << ", "
                        << pargram.v.c.y << ", "
                        << pargram.v.c.z
                        << std::endl;

    if(!rc) {  // if no intersection, plot N turns of the helix
        tPt = cfg["plot"]["noIntNturns"].as<int>()*h.period();
    } else {
        // otherwise, put intersection marker
        script3d << "set label at "
                 << sPt.c.x << ", " << sPt.c.y << ", " << sPt.c.z
                 << " \"intersection" << (rc < 0 ? "-" : "+")
                 << "\" point pt 1"
                 << std::endl;
    }

    // depict helix
    const int N = cfg["plot"]["nPoints"].as<int>();
    for(int n = 0; n < N; ++n) {
        Vec3 v = h(n*tPt/N);
        helixDat << v.c.x << "\t" << v.c.y << "\t" << v.c.z << std::endl;
    }

    script3d << "splot \"" << helixDatName << "\" w lines title \"helix\""
             << std::endl
             << "pause mouse close" << std::endl;

    return 0;
}

int
main(int argc, char * argv[]) {
    // Environment setup
    na64dp::util::set_std_environment_variables();
    if(argc != 3 && argc != 4) {
        std::cerr << "Error: wrong number of arguments specified." << std::endl;
        print_usage(argv[0], std::cout);
        return -1;
    } else if( argc > 2 && (!(strcmp(argv[1], "-h") && strcmp(argv[1], "--help")))) {
        print_usage(argv[0], std::cout);
        return 0;
    }
    // Read app configuration
    YAML::Node appCfg;
    try {
        appCfg = YAML::LoadFile(argv[2]);
    } catch( std::exception & e ) {
        std::cerr << "Error loading app config file \"" << argv[2] << "\": "
            << e.what() << std::endl;
        return -1;
    }
    
    if(!strcmp(argv[1], "full")) {
        return main_full(appCfg, argv[2]);
    } else if(!strcmp(argv[1], "planehelix")) {
        if(argc != 4) {
            std::cerr << "Error: wrong number of arguments specified." << std::endl;
            print_usage(argv[0], std::cout);
            return -1;
        }
        return main_planehelix(appCfg, argv[2], argv[3]);
    } else if(!strcmp(argv[1], "ellipse")) {
        if(argc != 4) {
            std::cerr << "Error: wrong number of arguments specified." << std::endl;
            print_usage(argv[0], std::cout);
            return -1;
        }
        return main_ellipse(appCfg, argv[2], argv[3]);
    } else if(!strcmp(argv[1], "pargram")) {
        if(argc != 4) {
            std::cerr << "Error: wrong number of arguments specified." << std::endl;
            print_usage(argv[0], std::cout);
            return -1;
        }
        return main_helix_parallelogram(appCfg, argv[2], argv[3]);
    }
    std::cerr << "Error: wrong subroutine specified: \""
              << argv[1] << "\"." << std::endl;
    print_usage(argv[0], std::cout);
    return -1;
}

