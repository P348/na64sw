# Runtime Extensions Directory

This directory contains common runtime extensions of the NA64sw project.

* `handlers-exp/` are the *experimental* handlers that provides
functionality that needs to be tested and verified. They may be not well
documented or be a semi-functional prototype of someone's ongoing development.
* `handlers/` are the *standard* handlers, more or less tested,
reliable and usually having decent documentation.
* `mc/` contains various Geant4-related stuff. Still at the prototyping stage,
not yet well standardized to deserve explainatory documentation.
* `source-ddd/` is a data source for `DaqDataDecoding` library and raw data
  indexing utils.
* `source-arttrack/` data source provides artificial track source to test the
  track reconstruction routines (a kind of simplified Monte-Carlo simulation
  to test tracking)
* `source-mkmc/` implements a data source for reading Mikhail Kitsanov's MC
  files.
* `source-p348reco/` wraps `p348daq/p348reco` reconstruction into a data source
  class.

Please, add your new handlers into `handlers-exp/` dir if not sure on their
reliability (both *standard* and *experimental* extensions are by default
visible to common pipeline app).
