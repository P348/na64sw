#include "na64calib/manager.hh"
#include "na64calib/evType.hh"
#include "na64dp/abstractHandler.hh"
#include "na64event/data/event.hh"
#include "na64util/str-fmt.hh"
#include <log4cpp/Category.hh>

namespace na64dp {

/**\brief Simple discriminator by event trigger or type
 *
 * Handler stops propagation of events based on bit flags condition.
 * */
template<typename T, typename MaskAttrT>
class DropByBits
        : public AbstractHandler
        , public calib::Handle<EventBitTags>
        {
protected:
    const std::vector<std::string> _maskStr;
    T _bitmask;
    bool (DropByBits<T, MaskAttrT>::*_mtd)(T) const;
    T event::Event::*_evAttr;
    MaskAttrT EventBitTags::*_ebtAttr;
    const bool _invert;

    void handle_update(const EventBitTags & ebt) override {
        calib::Handle<EventBitTags>::handle_update(ebt);
        _bitmask = 0x0;
        for(const auto & tok : _maskStr) {
            auto it = (ebt.*_ebtAttr).find(tok);
            if(it == (ebt.*_ebtAttr).end()) {
                NA64DP_RUNTIME_ERROR("No semantic bit named \"%s\"", tok.c_str() );
            }
            _bitmask |= it->second;
        }
        _log.info( "Discrimination bitmask updated: %#x"
                 , _bitmask );
    }
public:
    DropByBits( calib::Dispatcher & cdsp
              , const std::vector<std::string> & maskStr
              , bool (DropByBits<T, MaskAttrT>::*mtd)(T) const
              , T event::Event::*evAttr
              , MaskAttrT EventBitTags::*ebtAttr
              , log4cpp::Category & L
              , bool invert=false
              ) : AbstractHandler(L)
                , calib::Handle<EventBitTags>("default", cdsp)
                , _maskStr(maskStr)
                , _bitmask(0x0)
                , _mtd(mtd)
                , _evAttr(evAttr)
                , _ebtAttr(ebtAttr)
                , _invert(invert)
                {}

    ProcRes process_event(event::Event & event) override {
        bool r = (this->*_mtd)( event.*_evAttr );
        if(_invert) r = !r;
        if(r) {
            return kDiscriminateEvent;
        } else {
            return kOk;
        }
    }

    bool match_exact_equals(T toTest) const
        { return _bitmask == toTest; }
    bool match_contains_any_of(T toTest) const
        { return _bitmask & toTest; }
};

}  // namespace na64dp

REGISTER_HANDLER( DropIfTriggerBits, cdsp, cfg
                , "Discriminates event by trigger bit(s)") {
    // Define type for trigger discrimination
    typedef na64dp::DropByBits<decltype(na64dp::event::Event::trigger)
        , decltype(na64dp::EventBitTags::triggers) > HandlerType;
    // get "invert" flag
    bool invert = cfg["invert"] ? cfg["invert"].as<bool>() : false;
    // get bitset
    if((!cfg["bits"]) || !cfg["bits"].IsSequence()) {
        NA64DP_RUNTIME_ERROR("Handler config requires \"bits\" parameter ("
                "sequence of strings).");
    }
    auto bitsStr = cfg["bits"].as<std::vector<std::string>>();
    if(bitsStr.empty()) {
        NA64DP_RUNTIME_ERROR("Empty \"bits\" parameter");
    }
    // if "requireAllSet: true", then demand exact match
    bool requireAllSet = cfg["requireAllSet"] ? cfg["requireAllSet"].as<bool>() : false;
    return new HandlerType( cdsp
            , bitsStr
            , requireAllSet ? &HandlerType::match_exact_equals
                            : &HandlerType::match_contains_any_of
            , &na64dp::event::Event::trigger
            , &na64dp::EventBitTags::triggers
            , na64dp::aux::get_logging_cat(cfg)
            , invert
            );
}

REGISTER_HANDLER( DropIfEventTypes, cdsp, cfg
                , "Discriminates event by event type(s)") {
    // Define type for event type discrimination
    typedef na64dp::DropByBits<decltype(na64dp::event::Event::evType)
        , decltype(na64dp::EventBitTags::types) > HandlerType;
    // get "invert" flag
    bool invert = cfg["invert"] ? cfg["invert"].as<bool>() : false;
    // get bitset
    if((!cfg["bits"]) || !cfg["bits"].IsSequence()) {
        NA64DP_RUNTIME_ERROR("Handler config requires \"bits\" parameter ("
                "sequence of strings).");
    }
    auto bitsStr = cfg["bits"].as<std::vector<std::string>>();
    if(bitsStr.empty()) {
        NA64DP_RUNTIME_ERROR("Empty \"bits\" parameter");
    }
    // if "requireAllSet: true", then demand exact match
    bool requireAllSet = cfg["requireAllSet"] ? cfg["requireAllSet"].as<bool>() : false;
    return new HandlerType( cdsp
            , bitsStr
            , requireAllSet ? &HandlerType::match_exact_equals
                            : &HandlerType::match_contains_any_of
            , &na64dp::event::Event::evType
            , &na64dp::EventBitTags::types
            , na64dp::aux::get_logging_cat(cfg)
            , invert
            );
}

