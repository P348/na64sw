cmake_minimum_required( VERSION 3.12 )

# --- TODO: to be moved to dedicated extension once std-handlers will install
#           their headers
find_package(catsc)
# ^^^

#
# Standard handlers library (module)
set( na64std_MODULE std-handlers )
set( na64std_MODULE_SOURCES
        # Common handlers
        dummyHandler.cc
        pickEventsRange.cc
        reconstruction/pickMasterTime.cc
        # Generic handlers (operate with hits in general)
        generic/timeSeries.cc
        generic/discriminate.cc
        generic/JenksClassify.cc
        generic/hitHistogram1D.cc
        generic/hitHistogram2D.cc
        generic/hitHistogram3D.cc
        generic/getterBasedROOTHistHandler2D.cc
        generic/count1D.cc
        generic/sub.cc
        generic/events-range-enable.cc
        dropByTrigOrType.cc
        #generic/saveMeanValues.cc
        # APV detectors
        reconstruction/apv/calcARatios.cc
        reconstruction/apv/apvCalcTime.cc
        reconstruction/apv/clusteredHitsSelect.cc
        reconstruction/apv/findClusters.cc
        reconstruction/apv/apvClusterCalcValues.cc
        reconstruction/apv/apvConjugate.cc
        reconstruction/apv/rmSortedClusters.cc
        reconstruction/apv/collectAmbigClusters.cc
        reconstruction/apv/rmAmbigClusters.cc
        reconstruction/apv/dumpAmbigClusters.cc
        # SADC
        reconstruction/sadc/appendPeds.cc
        reconstruction/sadc/applySADCCalibsType1.cc
        reconstruction/sadc/assignPedestals.cc
        reconstruction/sadc/directSum.cc
        reconstruction/sadc/findMaxima.cc
        reconstruction/sadc/findMaxSample.cc
        reconstruction/sadc/getPedestalsByFront.cc
        reconstruction/sadc/linearSum.cc
        #reconstruction/sadc/movStatPeds.cc     # TODO: requires some handler to become hdr+src
        reconstruction/sadc/subtractPedestals.cc
        reconstruction/sadc/plot-wf.cc
        reconstruction/sadc/plot-maxMult.cc
        reconstruction/sadc/plot-maxsTime.cc
        #reconstruction/sadc/wfFit.cc  # TODO: requires union/oneof type
        reconstruction/sadc/wfAnalyze.cc  # xxx, dev handler
        reconstruction/sadc/timeAtMaxRisingEdge.cc
        reconstruction/sadc/timeAtMaxCoM.cc
        reconstruction/sadc/selectMax.cc
        reconstruction/sadc/cutOutOffTimeHits.cc
        # StwTDC
        reconstruction/stwtdc/findClusters.cc
        reconstruction/stwtdc/collectRT.cc
        reconstruction/stwtdc/rt.cc
        reconstruction/stwtdc/strawConjugate.cc
        # BMS
        reconstruction/bms/plot-timeVsmasterTime.cc
        reconstruction/bms/f1Conjugate.cc
        reconstruction/bms/bmsLayout.cc
        reconstruction/bms/plot-bms.cc
        reconstruction/bms/createBMSScores.cc
        
        # Tracking
        tracking/util-1d-hits.c
        tracking/createScore.cc
        tracking/decomposeScores.cc
        tracking/makeTracks.cc
        tracking/setSeeds.cc
        tracking/leastZPositionSeed.cc
        tracking/averageDirectionSeed.cc
        tracking/offsetPositionSeed.cc
        #tracking/mergeTracks.cc  # fixme
        tracking/collectAlignmentData.cc
        tracking/trackScoreIteratingHandler.cc
        tracking/recognizeTracks_zoned.cc
        tracking/recognizeTracks_single.cc
        tracking/resolveConcurrentTracks.cc
        tracking/removeOrphaneScores.cc
        #TODO: to be a subject for dedicated extension module
        tracking/catsc.cc

        # Calorimeters
        calo/mkCaloHit.cc
        #calo/hcalEcal.cc  # TODO: reimplement this
        calo/preshowerVsRest.cc
        # ...

        # other stuff
        reconstruction/trigTime.cc
    )

# collect headers recursively by wildcard; used only by install target
file(GLOB_RECURSE na64std_HEADERS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "*.hh" "*.h" "*.tcc" "*.hpp" )

add_library(${na64std_MODULE} MODULE ${na64std_MODULE_SOURCES})
#target_include_directories(${na64std_MODULE} PUBLIC SYSTEM ${na64_COMMON_SYS_INCLUDE_DIRS} )
#target_include_directories(${na64std_MODULE} PUBLIC ${na64_COMMON_INCLUDE_DIRS} )

target_include_directories(${na64std_MODULE} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> )
#target_link_libraries(${na64std_MODULE} PUBLIC ${app_LIB} )  # XXX

#    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
set_target_properties( ${na64std_MODULE} PROPERTIES VERSION ${na64sw_VERSION}
                                                  SOVERSION ${na64sw_SOVERSION}
                       INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib/na64/modules
                       PUBLIC_HEADER "${na64std_HEADERS}"
                     )
target_include_directories(${na64std_MODULE} PRIVATE $<TARGET_PROPERTY:${dp_LIB},INTERFACE_INCLUDE_DIRECTORIES>)

target_link_libraries(${na64std_MODULE} PUBLIC ${na64common_LIB})

if( ROOT_FOUND )
    target_link_libraries(${na64std_MODULE} PUBLIC ${ROOT_LIBRARIES})
#    # see not on the target_compile_options() for na64util_LIB in
#    # master CMakeLists.txt
#    separate_arguments(ROOT_CXX_FLAGS_ UNIX_COMMAND ${ROOT_CXX_FLAGS})
#    target_compile_options(${na64std_MODULE} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:${ROOT_CXX_FLAGS_}>)
#    target_link_libraries(${na64std_MODULE} PUBLIC ${ROOT_LIBRARIES})
endif( ROOT_FOUND )
# --- TODO: to be moved to dedicated extension once std-handlers will install
#           their headers
if( catsc_FOUND )
    target_link_libraries(${na64std_MODULE} PUBLIC catsc::catsc )
    target_compile_definitions(${na64std_MODULE} PUBLIC catsc_FOUND=1 )
    target_include_directories(${na64std_MODULE} PUBLIC SYSTEM ${catsc_INCLUDE_DIR} )
endif( catsc_FOUND )
# ^^^
install( TARGETS ${na64std_MODULE} EXPORT ${na64std_MODULE} LIBRARY
         DESTINATION ${NA64SW_STDEXT_DIR}
         PUBLIC_HEADER DESTINATION include/na64sw/extensions/std
       )
# This is to provide working configuration in the build directory
add_custom_target( std-handlers-ext-bld-symlink ALL
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_BINARY_DIR}/lib${na64std_MODULE}.so ../${na64std_MODULE} )

#
# Unit testing application

# TODO: fix
#if(GTEST_FOUND)
#    file( GLOB_RECURSE na64stdHandlers_test_SOURCES *.test.cc )
#    add_executable(${na64std_MODULE}-test ${na64stdHandlers_test_SOURCES} ${CMAKE_CURRENT_SOURCE_DIR}/../../utils/main-utests.cc)
#    target_include_directories(${na64std_MODULE}-test PUBLIC ${na64_COMMON_INCLUDE_DIRS} )
#    #target_include_directories(${na64std_MODULE}-test PUBLIC  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
#
#    set(THREADS_PREFER_PTHREAD_FLAG ON)
#    find_package(Threads REQUIRED)
#
#    target_link_libraries( ${na64std_MODULE}-test PUBLIC ${GTEST_BOTH_LIBRARIES} )
#    target_link_libraries( ${na64std_MODULE}-test PUBLIC #${na64dp_LIB}
#                                               #${na64event_LIB}
#                                               #${na64detID_LIB}
#                                               #${na64util_LIB}
#                                               #${na64calib_LIB}
#                                               ${utest_LIB}
#                                               #${na64std_MODULE}
#                                               ${na64app_LIB}
#                                               Threads::Threads )
#endif(GTEST_FOUND)
