#include "na64dp/abstractHandler.hh"

#include <climits>

namespace na64dp {

/**\brief Handler for event selection in certain range (orderly number)
 *
 * One of the simplest handlers.
 *
 * Handler is parameterised with event number (not ID!) range, `(from, to)` to
 * propagate only events certain orderly number -- that fall or do not fall (if
 * negated) the specified range.
 *
 * Using this handler one may cut our only certain number of events from the
 * reading source to, say, skip first 10000 and read next 5000. Useful for
 * parallel processing where few processes are working on the data chunk, each
 * on certain portion of data.
 * */
class PickEventsRange : public AbstractHandler {
public:
    typedef unsigned long EventNumber_t;
private:
    EventNumber_t _from, _to;
    bool _negate;
    EventNumber_t _counter;
public:
    PickEventsRange( EventNumber_t from_
                   , EventNumber_t to_
                   , bool negate_);
    // Returns starting number of events to be left for further processing.
    EventNumber_t from() const { return _from; }
    // Returns final number of events to read.
    EventNumber_t to() const { return _to; }
    // Returns true if current event is allowed for read.
    virtual ProcRes process_event(event::Event & event) override;
};

//                          * * *   * * *   * * *

PickEventsRange::PickEventsRange( EventNumber_t from_
                                , EventNumber_t to_
                                , bool negate_=false
                                ) :  _from(from_)
                          , _to(to_)
                          , _negate(negate_)
                          , _counter(0) {
    std::cout << "initialized " << this
              << ", range [" << _from << ", " << _to << "], negate="
              << (_negate ? "true" : "false") << std::endl;
}

AbstractHandler::ProcRes
PickEventsRange::process_event( event::Event & ) {
    ++_counter;
    return _negate != (_counter > _from && _counter < _to) ? kOk : kStopProcessing;
}

REGISTER_HANDLER( PickEventsRange, ch, yamlNode
                , "Performs event discrimantion by internal count (events range)" ){
    return new PickEventsRange( yamlNode["from"] ? yamlNode["from"].as<unsigned long>()
                                             : 0
                          , yamlNode["to"] ? yamlNode["to"].as<unsigned long>()
                                             : ULONG_MAX
                          , yamlNode["negate"] ? yamlNode["negate"].as<bool>()
                                               : false );
}

}

