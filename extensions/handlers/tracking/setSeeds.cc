#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"

#ifdef ROOT_FOUND
#   include <TDatabasePDG.h>
#endif

namespace na64dp {
namespace handlers {

/**\brief Sets seeding values for further track fitting
 *
 * This handler just sets some constant initial values for track fitting:
 * momentum, position, covariance, particle PDG code, etc.
 * */
class SetTrackSeeds : public AbstractHitHandler<event::Track> {
private:
    /// PDG code for the particle. If <=0 PDG won't be touched
    int _pdg;
    /// Initial momentum hypothesis. If first is set to NaN, won't be touched
    const double _p[3], _pos[3];
    double _cov[6];
protected:
    bool process_hit(EventID, TrackID, event::Track &) override;
public:
    SetTrackSeeds( calib::Manager & mgr, const std::string & sel
                 , int pdg, const double * p, const double * pos, const double * cov
                 , log4cpp::Category & logCat
                 , const std::string & namingClass="default"
                 )
        : AbstractHitHandler<event::Track>(mgr, sel, logCat, namingClass)
        , _pdg(pdg)
        , _p{p[0], p[1], p[2]}
        , _pos{pos[0], pos[1], pos[2]}
        {
        memcpy(_cov, cov, sizeof(_cov));
    }
};  // class SetTrackSeeds

bool
SetTrackSeeds::process_hit( EventID
                          , TrackID trackID
                          , event::Track & track
                          ) {
    if(!track.fitInfo) {
        track.fitInfo = lmem().create<event::TrackFitInfo>(lmem());
        util::reset(*(track.fitInfo));
    }
    // Particle code
    if(_pdg) {
        track.pdg = _pdg;
    }
    // Initial momentum hypothesis
    if(!std::isnan(_p[0])) {
        track.fitInfo->momentumSeed[0] = _p[0];
        track.fitInfo->momentumSeed[1] = _p[1];
        track.fitInfo->momentumSeed[2] = _p[2];
    }
    // Position
    if(!std::isnan(_pos[0])) {
        track.fitInfo->positionSeed[0] = _pos[0];
        track.fitInfo->positionSeed[1] = _pos[1];
        track.fitInfo->positionSeed[2] = _pos[2];
    }
    // Covariance
    if(!std::isnan(_cov[0])) {
        for(int i = 0; i < 6; ++i) {
            track.fitInfo->covarSeed[i] = _cov[i];
    	}
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SetTrackSeeds, cmgr, cfg
                , "Sets initial (seeding) parameters for track fitting." ) {
    Int_t pdgCode = 0;
    if( cfg["PDG"] ) {
        try {
            pdgCode = cfg["PDG"].as<int>();
        } catch( YAML::BadConversion & ) {
            #ifdef ROOT_FOUND
            auto particlePtr = TDatabasePDG::Instance()->GetParticle(
                    cfg["PDG"].as<std::string>().c_str() );
            if( !particlePtr ) {
                NA64DP_RUNTIME_ERROR("Unable to interpret PDG"
                        " \"%s\" as numeric code/name."
                        , cfg["PDG"].as<std::string>().c_str());
            } else {
                log4cpp::Category::getInstance("handlers").info("PDG "
                    " hypothesis is \"%s\" (%d)."
                    , particlePtr->GetName(), particlePtr->PdgCode() );
            }
            pdgCode = particlePtr->PdgCode();
            #else
            log4cpp::Category::getInstance("handlers").error("ROOT was not"
                    " suported by the build, can not obtain particle PDG"
                    " code from string.");
            throw;
            #endif
        }
    }
    // momentum
    double p[3] = {std::nan("0")};
    if(cfg["momentum"]) {
        for(int i = 0; i < 3; ++i) {
            p[i] = cfg["momentum"][i].as<double>();
        }
    }
    // position
    double pos[3] = {std::nan("0")};
    if(cfg["position"]) {
        for(int i = 0; i < 3; ++i) {
            pos[i] = cfg["position"][i].as<double>();
        }
    }
    // diagonal of the initial covariance matrix
    double cov[6];
    memset(cov, 0, sizeof(cov));
    if(cfg["covariance"]) {
        if( cfg["covariance"].IsScalar() ) {
            // populate diagonal of the matrix with single number
            double val = cfg["covariance"].as<double>();
            for(int i = 0; i < 6; ++i) {
                cov[i] = val;
            }
        } else if( cfg["covariance"].IsSequence() ) {
            auto vals = cfg["covariance"].as<std::vector<double>>();
            if(vals.size() == 6) {
                for(int i = 0; i < 6; ++i) {
                    cov[i] = cfg["covariance"][i].as<double>();
                }
            //} else if(vals.size() == 21) {  
            } else {
                NA64DP_RUNTIME_ERROR("Wrong length for \"covariance\" config node.");
            }
        } else {
            NA64DP_RUNTIME_ERROR("Bad type for \"covariance\" config node.");
        }
    }
    return new na64dp::handlers::SetTrackSeeds( cmgr,
            na64dp::aux::retrieve_det_selection(cfg),
            pdgCode, p, pos, cov,
            ::na64dp::aux::get_logging_cat(cfg),
            ::na64dp::aux::get_naming_class(cfg)
            );
}

