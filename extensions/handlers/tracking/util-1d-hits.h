#ifndef H_CATS_UTIL_1D_HITS_H
#define H_CATS_UTIL_1D_HITS_H

#include "catsc/config.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef CATS_COORDINATE_TYPE Real_t;

/**\brief Finds 3rd point for "2-points 1-line" case
 *
 * This function itself does not check any threshold by itself, but is used by
 * to find 3rd point for case of two spatial points and one line maximizing
 * the angle.
 */
void
catsc_util_2p1l_find_point( const Real_t * p1
                          , const Real_t * p2
                          , const Real_t * l  // l, r0
                          , Real_t * p
                          );

/**\brief Finds line going through the point and two lines
 *
 * Point is expected to be of `p1:float[3]`, the lines have to be of
 * `l:float[6]` with direction vector \f$ \vec{l} \f$ in 1st triplet (three elements)
 * and some point on the line in 2nd triplet \f$ \vec{r}_0 \f$ . Returned `l:float[3]`
 * is the direction vector of the line going through the `l1`, `l2`, `p1`.
 * 
 * \param[in] p1 three-element array defining a point
 * \param[in] l1 six-element array defining 1st line ( \f$\vec{l}_1, \vec{r}_{0,1}\f$ )
 * \param[in] l2 six-element array defining 2nd line ( \f$\vec{l}_2, \vec{r}_{0,2}\f$ )
 * \param[out] l resulting line ( \f$ \vec{l}, \vec{r}_0 \f$ ).
 *
 * \todo optimization and profiling
 * \todo I've used GSL for 2x2 for stability -- check/optimize
 * \todo check if \f$\vec{l}_1\f$ and is parallel
 * */
void
catsc_util_1p2l_find_line( const Real_t * p1
                         , const Real_t * l1
                         , const Real_t * l2
                         , Real_t * l
                         );

/**\brief Computes planar angle defined by points triplet
 *
 * \todo Simplified implementation of `acos()` function
 * */
Real_t
catsc_util_3p_angle( const Real_t * p1
                   , const Real_t * p2
                   , const Real_t * p3
                   );

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif  /* H_CATS_UTIL_1D_HITS_H */
