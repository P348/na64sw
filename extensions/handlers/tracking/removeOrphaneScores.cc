#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"

#include <unordered_set>

namespace na64dp {
namespace handlers {

static void _visit_score(std::unordered_set<const event::TrackScore *> & used
        , const event::TrackScore * scorePtr ) {
    used.insert(scorePtr);
    for(auto & hr : scorePtr->hitRefs) {
        if(std::holds_alternative<mem::Ref<event::TrackScore>>(hr.second)) {
            _visit_score(used, std::get<mem::Ref<event::TrackScore>>(hr.second).get());
        }
    }
}

class RemoveOrphanedScores
        : public AbstractHandler
        , public SelectiveHandlerMixin
        {
public:
    RemoveOrphanedScores( calib::Dispatcher & cdsp 
                        , const std::string & keepScoresStrexpr
                        , log4cpp::Category & logCat
                        , const std::string namingClass="default"
                        ) : AbstractHandler(logCat)
                          , SelectiveHandlerMixin(cdsp, keepScoresStrexpr, logCat, namingClass)
                          {}

    ProcRes process_event(event::Event & ev) override {
        std::unordered_set<const event::TrackScore *> used;
        // iterate over tracks, collect ptrs on scores that participate in
        // tracks (including scores within scores)
        for(auto & trackPair: ev.tracks) {
            for(auto & scorePair: trackPair.second->scores) {
                assert(scorePair.second->score);
                _visit_score(used, scorePair.second->score.get());
            }
        }
        std::vector<decltype(event::Event::trackScores)::iterator> toRemove;
        // iterate over existing scores, collect iterators to remove
        for( auto it = ev.trackScores.begin()
           ; it != ev.trackScores.end()
           ; ++it ) {
            if(is_selective() && matches(it->first)) continue;
            // if score is not used, keep iterator for deletion
            if(used.find(it->second.get()) != used.end()) continue;
            toRemove.push_back(it);
        }
        // remove scores
        for(auto it : toRemove) {
            ev.trackScores.erase(it);
        }
        return kOk;
    }
};  // class RemoveOrphanedScores

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( RemoveOrphanedScores, cmgr, cfg
                , "Deletes track scores not used by track from event." ) {
    return new na64dp::handlers::RemoveOrphanedScores( cmgr
            , cfg["keep"] ? cfg["keep"].as<std::string>() : ""
            , na64dp::aux::get_logging_cat(cfg));
}

