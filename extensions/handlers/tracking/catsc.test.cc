#include "tracking/catsc.hh"
#include "na64utest/testing-fixture.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace test {

class TestIsotropicAngleMultifilter
            : public na64dp::geo::iIsotropicAngleMultifilter
            , public na64dp::test::BasicEventDataOperations
            {
public:
    DetID testDetID;
    util::Vec3 l;
    util::Vec3 r0;
    mutable bool detIDMatched;
protected:
    void _copy_score_val_line( HitCoordinateReal_t * dest
                             , DetID did
                             , const event::TrackScore & src
                             ) const override {
        detIDMatched = (did == testDetID);
        for(int i = 0; i < 3; ++i) {
            dest[i    ] = l.r[i];
            dest[i + 3] = r0.r[i];
        }
    }
public:
    TestIsotropicAngleMultifilter()
        : na64dp::geo::iIsotropicAngleMultifilter(3./4)
        , detIDMatched(false)
        {}
};  // class TestIsotropicAngleMultifilter

TEST_F( TestIsotropicAngleMultifilter, FindsSimpleAngle ) {
    _reallocate_event_buffer(5*1024);
    using util::Vec3;
    Vec3 p2{{ 1, 0, 0 }}
       , p1{{ 0, 1, 0 }}
       //, p0{{ 0, 0, 0 }}
       , l1{{ 0, 0, 1 }}
       , r0_{{ 0, 0,-1 }}
       ;

    decltype(event::Event::trackScores) scores(lmem());
    auto ir1 = scores.emplace( DetID(1, 1, 1), lmem().create<event::TrackScore>(lmem()) )
       , ir2 = scores.emplace( DetID(1, 1, 2), lmem().create<event::TrackScore>(lmem()) )
       , ir3 = scores.emplace( DetID(1, 1, 3), lmem().create<event::TrackScore>(lmem()) )
       ;
    util::reset(*ir1->second);
    util::reset(*ir2->second);
    util::reset(*ir3->second);

    // we expect line copying being called with 2nd score (ir2)
    testDetID = ir2->first;

    for(int i = 0; i < 3; ++i) {
        ir1->second->gR[i] = p2.r[i];
        ir3->second->gR[i] = p1.r[i];
    }
    ir2->second->lR[0] = 0;
    l = l1;
    r0 = r0_;

    //TestIsotropicAngleMultifilter f(3*M_PI/4);
    EXPECT_NEAR(1./2, weight_(ir1, ir2, ir3), 1e-4);
    EXPECT_TRUE(detIDMatched);
}

TEST_F( TestIsotropicAngleMultifilter, FindsGenericSimpleAngle ) {
    _reallocate_event_buffer(5*1024);
    using util::Vec3;
    Vec3 p1{{ 1, 1, 1 }}
       , p2{{ 3, 3, 3 }}
       , p3{{ 4, 6, 5 }}  // < angle between those points is ~157.79deg
       //, p0{{ 0, 0, 0 }}
       , l3{{ -1, -1, 1 }}
       , r0_ = p3 + l3
       ;

    decltype(event::Event::trackScores) scores(lmem());
    auto ir1 = scores.emplace( DetID(1, 1, 1), lmem().create<event::TrackScore>(lmem()) )
       , ir2 = scores.emplace( DetID(1, 1, 2), lmem().create<event::TrackScore>(lmem()) )
       , ir3 = scores.emplace( DetID(1, 1, 3), lmem().create<event::TrackScore>(lmem()) )
       ;
    util::reset(*ir1->second);
    util::reset(*ir2->second);
    util::reset(*ir3->second);

    // we expect line copying being called with 3rd score (ir3)
    testDetID = ir3->first;

    for(int i = 0; i < 3; ++i) {
        ir1->second->gR[i] = p1.r[i];
        ir2->second->gR[i] = p2.r[i];
    }
    ir3->second->lR[0] = 0;
    l = l3;
    r0 = r0_;

    //TestIsotropicAngleMultifilter f(3*M_PI/4);
    EXPECT_NEAR(157.79/180., weight_(ir1, ir2, ir3), 1e-4);
    EXPECT_TRUE(detIDMatched);
}

}  // namespace ::na64dp::test
}  // namespace na64dp


