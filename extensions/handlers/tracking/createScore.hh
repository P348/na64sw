#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64calib/placements.hh"
#include "na64event/data/event.hh"
#include "na64util/isCompleteType.hh"

// this headers provide traits specialization for hit types capable to produce
// track scores
#include "reconstruction/apv/apvConjugate.hh"
//#include "reconstruction/sadc/hodoscopeConjugate.hh"  // TODO
#include "reconstruction/stwtdc/strawConjugate.hh"
#include "reconstruction/bms/f1Conjugate.hh"

#include "na64dp/DSuLConjugationRule.hh"
#include "na64calib/setupGeoCache.hh"

namespace na64dp {
namespace errors {
/// Can be thrown by rules to indicate mismatch in placements type
class WrongPlacementTypeProvided : public errors::GenericRuntimeError {
public:
    WrongPlacementTypeProvided(const std::string & msg) :
        GenericRuntimeError(msg.c_str()) {}
};  // WrongPlacementTypeProvided

class MissingInfoForDetector : public GenericRuntimeError {
public:
    const DetID detID;
    MissingInfoForDetector(DetID did)
        : GenericRuntimeError(util::format("No supp. info entry found"
                    " in rule cache for detector %x", did.id).c_str())
        , detID(did) {}
    MissingInfoForDetector(DetID did, const char * tp)
        : GenericRuntimeError(util::format("No %s entry found"
                    " in rule cache for detector %x", tp, did.id).c_str())
        , detID(did) {}
};  // class MissingPlacementsForAPVDetector
}  // namespace ::na64dp::errors
namespace handlers {

template< typename HitT
        , bool=::na64dp::util::is_complete_type<aux::TrackScoreTraits<HitT>>::value >
class CombinedSubScoreProducer;

/// Partial implementation stub for conjugated handler that can not produce
/// scores based on individual detector entity hit
///
/// \ingroup track-combining-hits
template<typename HitT>
class CombinedSubScoreProducer<HitT, false> {
public:
    constexpr static bool canProduceSubscores = false;
protected:
    CombinedSubScoreProducer( calib::Dispatcher &
                            , log4cpp::Category &
                            , const std::string & namingClass="default"
                            , const std::string & placementsClass="default"
                            ) {}

    std::pair<DetID_t, mem::Ref<event::TrackScore> >
    make_score( LocalMemory & lmem
              , typename event::Association<event::Event, HitT>::Collection::iterator it
              , typename AbstractHitHandler<HitT>::HitKey hk
              ) {
        NA64DP_RUNTIME_ERROR("Logic error -- execution reached unforeseen call"
                " (combined sub-score producer stub can't create scores).");
    }
};

/**\brief Partial implementation for handler producing scores based on
 *        individual detector entity hit
 * 
 * This partial specialization represents certain placements subscription.
 * Its `make_score()` method forwards execution to
 * `TrackScoreTraits::create()` method to create a score directly from
 * a hit of certain type with relevant placement information.
 *
 * This specialization is used for hit types with `aux::TrackScoreTraits`
 * fully defined.
 *
 * Needs to be parameterised with external selector to filter placements
 * updates.
 *
 * \ingroup track-combining-hits */
template<typename HitT>
class CombinedSubScoreProducer<HitT, true>
        : public util::Observable<calib::Placements>::iObserver
        , protected calib::Handle<nameutils::DetectorNaming>
        {
public:
    constexpr static bool canProduceSubscores = true;
protected:
    /// Hit type-specific cache
    typename aux::TrackScoreTraits<HitT>::Cache _cache;
private:
    log4cpp::Category & _L;
    size_t _nWrongPlacementsType;
protected:
    /// Shortcut returning current naming instance
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::get(); }

    ///\brief Updates hit-specific cache using placements info
    ///
    /// Uses track score type traits to recache placements info, converting
    /// it from generic form into one consumable by placements. Note, that
    /// hit type restriction is not applied at this level (i.e. all the
    /// placements get propagated to `recache_placement()`).
    void handle_update( const calib::Placements & placements ) override {
        for( const auto & placement : placements ) {
            if(!placement.is_detector()) continue;
            try {
                aux::TrackScoreTraits<HitT>::recache_placement( _cache
                                , PlaneKey(this->naming()[placement.name])
                                , placement
                                );
                if(_L.getPriority() >= log4cpp::Priority::DEBUG) {
                    // this output is helpful for debug (in the rule context
                    // detector ID mapping is not available).
                    _L << log4cpp::Priority::DEBUG
                       << "Recached placements info for item "
                       << placement.name << " (" << PlaneKey(this->naming()[placement.name]).id
                       << " of hit conjugation rule" //" using "
                       //<< placement.srcDocID << ":" << placement.lineNo
                       ;
                }
            } catch(errors::WrongPlacementTypeProvided & e) {
                ++_nWrongPlacementsType;
            }
        }
    }

    size_t wrong_placements_error_occured_n_times() { return _nWrongPlacementsType; }

    CombinedSubScoreProducer( calib::Dispatcher & cdsp
                            , log4cpp::Category & L
                            , const std::string & namingClass="default"
                            , const std::string & placementsClass="default"
                            ) : calib::Handle<nameutils::DetectorNaming>("default", cdsp)
                              , _L(L)
                              , _nWrongPlacementsType(0)
                              {
        cdsp.subscribe<calib::Placements>(*this, placementsClass);
    }

    std::pair<DetID_t, mem::Ref<event::TrackScore> >
    make_score( LocalMemory & lmem
              , typename event::Association<event::Event, HitT>::Collection::iterator it
              , typename AbstractHitHandler<HitT>::HitKey hk
              ) {
        return ::na64dp::aux::TrackScoreTraits<HitT>::create(lmem
                    , it->second, hk, _cache);
    }
};

/**\brief Generic rule combining two 1D scores into one 2D (into a spatial
 *        point)
 *
 * This is pretty generic rule to combine two 1D scores into a spatial point,
 * a very common case (e.g. two-layered strip detectors).
 *
 * Uses `na64sw_projected_lines_intersection()` to derive the
 * pseudo-intersection point of two lines defined by 1D hits. This spatial
 * point then defines a new score.
 *
 * \todo Sight vector parameter can be of use here.
 *
 * \ingroup track-combining-hits
 * */
class TwoLayerScoreCombinationRule
        : public util::DSuLHitCombinationRule<2, event::TrackScore>
        , public calib::SetupGeometryCache {
public:
    typedef event::TrackScore HitType;
    static constexpr util::Arity_t arity=2;
    /// Structure keeping geometrical information, relevant to clusters
    /// conjugation
    struct PlacementsCache {
        util::Transformation t;
        double resolution;  ///< resolution explicitly set for this plane
    };
protected:
    std::unordered_map<PlaneKey, PlacementsCache> _placementsCache;
    size_t _n_set(HitRef_t did) const override;
public:
    TwoLayerScoreCombinationRule( calib::Dispatcher & cdsp
            , const std::string & sel
            , log4cpp::Category & logCat
            , bool permitPartialCombinations
            , typename iPerStationHitCombinationRule<HitRef_t, 2>::GeneratorCtr dftCtr
                    =iPerStationHitCombinationRule<HitRef_t, 2>::construct_basic_cartesian_generator
            , const std::string & detNamingSubclass="default"
            , const std::string & placementsCalibClass="default"
            ) : DSuLHitCombinationRule<2, event::TrackScore>(cdsp, sel, logCat, permitPartialCombinations
                , dftCtr, detNamingSubclass)
              , calib::SetupGeometryCache( cdsp
                                         , logCat
                                         , detNamingSubclass
                                         , placementsCalibClass )
              {}

    const nameutils::DetectorNaming & naming() const { return calib::SetupGeometryCache::get(); }

    void handle_single_placement_update(const calib::Placement &) override;

    ///\brief Sets local X, Y coordinates using scores
    ///
    /// Sets first and second element of `drsXY` to X and Y coordinates in
    /// detector reference system (correspondingly). Third element (if
    /// allocated) of `drsXY` is not accessed. Rotation matrices and ofsets of
    /// both planes are not taken into account.
    void calc_score_values( event::TrackScore &
                          , const std::array<HitRef_t, 2> & ) override;
};


/**\brief Creates track scores based on detector digits
 *
 * Combines geametrical information and measurements of the detector into
 * `TrackScore` -- these objects are supposed then to be used by various track
 * pre-selection and/or track reconstruction routines.
 *
 * This handler is responsible for creation of simplest-possible (elementary)
 * scores that are not combined (conjugated) from projections, i.e. one
 * hit/cluster item should be converted to a single score.
 *
 * ~~~{.yaml}
 *     - _type: CreateScores
 *       # hit type name; str, required
 *       subject: ...
 * ~~~
 *
 * This class is somewhat versatile track scores creator. It is extensible via
 * template `CombinedSubScoreProducer` traits.
 *
 * \ingroup track-score-handlers
 * */
template<typename HitT>
class CreateTrackScores : public AbstractHitHandler<HitT>
                        , public CombinedSubScoreProducer<HitT>
                        {
protected:
    /// Collection of failed keys with missed placements, etc
    std::set<std::string> _faultyNames;

    /// Uses selection to filter placements of certain type and call parent
    void handle_update( const calib::Placements & placements ) override {
        calib::Placements placementsFiltered;
        // make filtered copy of the placements info
        for( const auto & placement : placements ) {
            if(!placement.is_detector()) {
                AbstractHitHandler<HitT>::_log << log4cpp::Priority::DEBUG
                    << "Non-detector placement ignored (\""
                    << placement.name << "\").";
                continue;  // omit non-detector items
            }
            auto key = this->AbstractHitHandler<HitT>::naming()[placement.name];
            if(!AbstractHitHandler<HitT>::matches(key)) {
                AbstractHitHandler<HitT>::_log << log4cpp::Priority::DEBUG
                    << "Ignored non-matching detector placement (\""
                    << placement.name << "\").";
                continue;
            } else {
                AbstractHitHandler<HitT>::_log << log4cpp::Priority::DEBUG
                    << "Detector placement \""
                    << placement.name << "\" will be used for update.";
            }
            placementsFiltered.push_back(placement);
        }
        CombinedSubScoreProducer<HitT>::handle_update(placementsFiltered);
    }
public:
    CreateTrackScores( calib::Dispatcher & dsp
                     , const std::string & selection
                     , log4cpp::Category & logCat
                     , const std::string & namingClass="default"
                     , const std::string & placementsClass="default"
                     ) : AbstractHitHandler<HitT>(dsp, selection, logCat, namingClass)
                       , CombinedSubScoreProducer<HitT>(dsp, logCat, namingClass, placementsClass)
                       {}
    /// \brief Creates track score for certain hit type
    ///
    /// Forwards execution to corresponding traits trying to instantiate the
    /// track score using cache and binds the score with hit.
    virtual bool process_hit( EventID eventID
                            , typename AbstractHitHandler<HitT>::HitKey hk
                            , HitT & hit
                            ) override {
        auto pair = CombinedSubScoreProducer<HitT>::make_score(this->lmem()
                , this->_current_hit_iterator(), hk);
        if(!pair.second) {
            _faultyNames.emplace(AbstractHitHandler<HitT>::naming()[hk]);
            return true;
        }
        AbstractHitHandler<HitT>::_log << log4cpp::Priority::DEBUG
                    << "Created elementary score "
                    << AbstractHitHandler<HitT>::naming()[DetID(pair.first)]
                    << " based on hit "
                    << AbstractHitHandler<HitT>::naming()[DetID(this->_current_hit_iterator()->first)]
                    << ": {"
                    << pair.second->lR[0] << ", "
                    << pair.second->lR[1] << ", "
                    << pair.second->lR[2] << "}"
                    ;
        this->_current_event().trackScores.emplace(pair.first, pair.second);
        return true;
    }
    /// Prints the information of faulty planes at the end of data processing.
    void finalize() override {
        if( !_faultyNames.empty() ) {
            std::ostringstream oss;
            bool isFirst = true;
            for( auto nm : _faultyNames ) {
                if(isFirst) isFirst = false; else oss << ", ";
                oss << "\"" << nm << "\"";
            }
            this->log().warn( "Following detector had issues with converting"
                    " their hits to track score values (missing placements"
                    " info?): %s", oss.str().c_str());
        }
        if( CombinedSubScoreProducer<HitT>::wrong_placements_error_occured_n_times() ) {
            this->log().warn( "Score creating"
                    " handler got type mismatch error"
                    " for placements update %zu times. This can affect"
                    " performance."
                    , CombinedSubScoreProducer<HitT>::wrong_placements_error_occured_n_times()
                    );
        }
    }
};  // class CreateTrackScores


namespace aux {
struct ScoreCreation {
    enum Mode {
        kPerHit,
        kSubScores,
        kSingular
    };
    static Mode from_str(const std::string &);
};
}  // namespace ::na64dp::handlers::aux

/**\brief Template implementation for rule-based hit conjugation handler
 *
 * A generic handler that needs to be parameterised with rule class to generate
 * handler for hit conjugation.
 *
 * Usage (common part):
 * \code{.yaml}
 *     - _type: CreateScores
 *       # Defines the rule and subject; str, required
 *       subject: ...
 *       # Wether (and how) to combine hits; str, optional
 *       combinationRule: ...
 *       # prevents enables units conversion for certain detectors (usually,
 *       # wire units to metric)
 *       convertUnits: true
 *       # hit selection expression; optional, str
 *       applyTo: null
 *       # defines logging category; optional, str
 *       loggingCategory: "handlers.conjugate"
 *       # defines naming info subclass; optional, str
 *       detectorNamingCategory: "default"
 *       # ... other arguments, specific to particular subject
 * \endcode
 *
 * The handler relies on the presence of 1D cluster/hit objects in the event
 * structure that were previously being added with clustering handler
 * (like `APVFindClusters`). If `combinationRule` is not set or set to `null`,
 * no combination/conjugation will be performed for hits, a score will
 * be created for every "hit" of the subject. Otherwise, various n-tuplets are
 * supported by handler:
 *
 *  - `doublets` -- simple conjugation rule for two-layered APV detectors.
 *  - `quadruplets` -- complex conjugation rule for four-layered Sraw detectors
 *
 * For non-null `combinationRule` there are different ways to treat hits:
 * 
 *   1. Scores can be created one-per-hit. This way `doublets` will cause two
 *      scores created within the event. This mode is useful when conjugation
 *      rule provides some hit discrimination logic, so hits that, say, have no
 *      paired ones won't be considered in track finding/fitting.
 *   2. One score per every hit is created and unified within some extra,
 *      "owning" score. Say for two-layered wire detector, two 1-dimensional
 *      scores are created and subjugated to some third 2-dimensional score.
 *   3. One score is created per entire n-tuplet.
 *
 * Parameter controlling the mode of scores creation is called `mode`
 * and can be of following variants: "perHit" (1), "subScores" (2) and
 * "singular" (3).
 *
 * \image html handlers/mm4.png
 *
 * `convertUnits` option might be used to prevent handler from converting
 * internal detector's units of cluster or hit (usually, wires) into
 * length (cm). Might be useful for certain types of debug, but generally
 * handlers assumes corresponding fields of track scores to be in length untis
 * (so careless usage of this option may cause severe mistake).
 *
 * \note This handler does not rely on any coordinates or plane positioning
 *       information
 * \todo "applyTo" (only detectors) is currently being ignored
 * \see APVFindClusters
 * \ingroup track-score-handlers
 * */
template<typename RuleT>
class ConjugateHits : public AbstractHitHandler<typename RuleT::HitType>
                    , protected RuleT
                    , public CombinedSubScoreProducer<typename RuleT::HitType>
                    {
public:
    typedef event::Association<event::Event, typename RuleT::HitType> Association;
    typedef CombinedSubScoreProducer<typename RuleT::HitType> SubScoreMaker;
    typedef event::Association<event::Event, event::TrackScore>::Collection EventScores;
private:
    /// Reentrant continer for station IDs
    std::vector<StationKey> _sks;
    /// Reentant container for plane IDs
    std::array<typename RuleT::HitRef_t, RuleT::arity> _hitRefs;
    /// Keeps set of detector IDs with missing calibrations and/or placements
    std::unordered_set<DetID> _missingCalibs;
private:
    /// Method of scores creation: superscore with subscores
    bool _make_complex_scores(StationKey sk, EventScores &);
    /// Method of scores creation: singular scores
    bool _make_singular_scores(StationKey sk, EventScores &);
    /// Method of scores creation: one score per hit in combination
    bool _make_individual_scores(StationKey sk, EventScores &);
    /// Choosen method for score creation
    bool (ConjugateHits::*_make_scores)(StationKey sk, EventScores &);
public:
    template<typename ... RuleSuppArgsT>
    ConjugateHits( calib::Dispatcher & ch
                 , const std::string & only
                 , const std::string & namingSubclass
                 , log4cpp::Category & logCat
                 , aux::ScoreCreation::Mode scoreCreationMode
                 , RuleSuppArgsT &&...args
                 ) : AbstractHitHandler<typename RuleT::HitType>( ch, "", logCat, namingSubclass )
                   , RuleT(ch, only, logCat, args ...)
                   , SubScoreMaker(ch, logCat)
                   , _hitRefs( util::create_array<RuleT::arity>(typename RuleT::HitRef_t(false, {}) ) )
                   {
        // for `perHit' and `subScores`, assure we have defined traits to
        // create scores for individual hits
        if( aux::ScoreCreation::kSingular != scoreCreationMode
         && !CombinedSubScoreProducer<typename RuleT::HitType>::canProduceSubscores ) {
            NA64DP_RUNTIME_ERROR("Hits of the subject type can not"
                    " be used to produce track scores.");
        }
        if( aux::ScoreCreation::kSubScores == scoreCreationMode ) {
            _make_scores = &ConjugateHits::_make_complex_scores;
        } else if( aux::ScoreCreation::kPerHit == scoreCreationMode ) {
            _make_scores = &ConjugateHits::_make_individual_scores;
        } else if( aux::ScoreCreation::kSingular == scoreCreationMode ) {
            _make_scores = &ConjugateHits::_make_singular_scores;
        } else {
            throw std::runtime_error("Unknown enum type.");  // no option in enum
        }
    }
    
    /// Collects the 1D scores
    bool process_hit( EventID eventID
                    , typename AbstractHitHandler<typename RuleT::HitType>::HitKey hk
                    , typename RuleT::HitType & hit
                    ) override {
        NA64DP_RUNTIME_ERROR("Logic error: execution reached unforeseen call"
                " (process hit mtd of generic hit conjugation handler).");
    }

    /// Collects 1D scores, performs the conjugation producing 2D scores
    virtual AbstractHandler::ProcRes process_event( event::Event & e) override {
        auto & L = this->log();
        auto & nm = RuleT::naming();
        // collect & sort hit keys to group 'em into sequences ready for
        // iteration, according to combination rule
        // (!do not call AbstractHitHandler<typename RuleT::HitType>::process_event(e);)
        auto & hits = Association::map(e);
        std::map<std::string, size_t> useCounters;
        bool collectStats4debug = L.getPriority() >= log4cpp::Priority::DEBUG ? true : false;
        for( auto it = hits.begin(); it != hits.end(); ++it ) {
            StationKey sk(DetID_t(it->first));
            bool used
                = RuleT::consider_hit( typename RuleT::HitRef_t(true, it), sk );
            if(collectStats4debug && used)
                ++(useCounters.emplace(nm[it->first], 0).first->second);
        }
        if( collectStats4debug ) {
            std::ostringstream oss;
            for(const auto & p : useCounters) {
                oss << p.first << ": " << p.second << ", ";
            }
            L << log4cpp::Priority::DEBUG
              << "Hits used: " << oss.str();
        }
        // get generators per station, iterate over them and conjugate the hits
        // according to conjugation rule
        _sks.clear();
        RuleT::collection_ids(_sks);
        size_t nCreated = 0;  // to log the accounted results
        for(StationKey sk: _sks) {
            auto combinations = RuleT::hits_generator(sk);
            if(!combinations) {
                L << log4cpp::Priority::DEBUG
                    << "No hit combinations on station \"" << nm[sk] << "\".";
                continue;
            }
            L << log4cpp::Priority::DEBUG
                    << "Combining hits on station \"" << nm[sk] << "\".";
            size_t nCombinations = 0;
            while(combinations->get(_hitRefs)) {  // get combination from generator
                ++nCombinations;
                // iterate over hit refs looking for non-empty one(s)
                bool isEmpty = true;
                for(typename RuleT::HitRef_t hr : _hitRefs) {
                    if(!hr.first) continue;
                    isEmpty = false;
                    break;
                }
                if(L.getPriority() >= log4cpp::Priority::DEBUG) {
                    std::ostringstream oss;
                    bool isFirst = true;
                    for(typename RuleT::HitRef_t hr : _hitRefs) {
                        if(hr.first) {
                            if(isFirst) isFirst = false; else oss << ", ";
                            oss << nm[hr.second->first];
                        }
                    }
                    L << log4cpp::Priority::DEBUG
                      << "Combination {" << oss.str() << "} is considered as"
                      << (isEmpty ? "" : " not") << " empty.";
                      ;
                }
                if(isEmpty) continue;
                // create score from non-empty
                assert(_make_scores);
                bool scoresCreated = false;
                try {
                    scoresCreated = (this->*_make_scores)(sk, e.trackScores);
                } catch(errors::MissingInfoForDetector & e) {
                    auto ir = _missingCalibs.emplace(e.detID);
                    if(ir.second) {
                        L << log4cpp::Priority::ERROR
                          << "Error creating track score for "
                          << RuleT::naming()[e.detID] << ": " << e.what()
                          << ", other occurencies will be quietly ignored."
                          ;
                    }
                }
                // ^^^ either:
                //  - _make_complex_scores()
                //  - _make_singular_scores()
                //  - _make_individual_scores()
                // which anyhow forward execution to
                // `RuleT::calc_score_values()` which can deny creating a
                // score based on combination. Particular reason can differ.
                if(scoresCreated) ++nCreated;
                if( L.getPriority() >= log4cpp::Priority::DEBUG ) {
                    L << log4cpp::Priority::DEBUG
                      << "Score(s) " << (scoresCreated ? "" : "not ")
                      << "created from aforementioned non-empty combination.";
                }
            }
            L << log4cpp::Priority::DEBUG
                << "Done combining on station \"" << nm[sk] << "\": "
                << nCombinations << " combinations resulted in " << nCreated
                << " track scores being combined or derived."
                ;
        }

        _hitRefs = util::create_array<RuleT::arity>(typename RuleT::HitRef_t(PlaneKey(), nullptr));
        RuleT::reset();
        return AbstractHandler::kOk;
    }

    void finalize() {
        if(!_missingCalibs.empty()) {
            std::set<std::string> orderedNames;
            std::transform( _missingCalibs.begin(), _missingCalibs.end()
                          , std::inserter(orderedNames, orderedNames.begin())
                          , [this](DetID did) { return this->RuleT::naming()[did]; }
                          );
            this->log() << log4cpp::Priority::WARN
                << "Couldn't combine/conjugate hits from following detector(s): "
                << util::str_join(orderedNames.begin(), orderedNames.end())
                ;
        }
    }
};

//
// Implementation of score creating methods

// superscore with subscores
template<typename RuleT> bool
ConjugateHits<RuleT>::_make_complex_scores(StationKey sk, EventScores & evScores) {
    bool created = false;
    // Create and initialize new score object (a superscore)
    mem::Ref<event::TrackScore> tp
        = this->lmem().template create<event::TrackScore>(this->lmem());
    util::reset(*tp);

    for(typename RuleT::HitRef_t hr : _hitRefs) {
        // for every hit in combination, do:
        if(!hr.first) continue;  // omit "empty" hit
        // Create sub-score and push it into "owning" superscore. Note, here
        // the same to-traits forwarding method is used as in plain
        // `CreateTrackScores<>` method.
        auto pair
            = SubScoreMaker::make_score(this->lmem(), hr.second, hr.second->first);
        if(!pair.second) {  // failed to create a score
            continue;
        }
        tp->hitRefs.insert(pair);
        // insert sub-score into event
        evScores.insert(pair);
        created = true;
    }
    RuleT::calc_score_values(*tp, _hitRefs);
    evScores.emplace(sk, tp);
    return created;
}

// singular score
template<typename RuleT> bool
ConjugateHits<RuleT>::_make_singular_scores(StationKey sk, EventScores & evScores) {
    // Create and initialize new score object (a superscore)
    mem::Ref<event::TrackScore> tp
        = this->lmem().template create<event::TrackScore>(this->lmem());
    util::reset(*tp);

    for(typename RuleT::HitRef_t hr : _hitRefs) {
        // for every hit in combination, do:
        if(!hr.first) continue;  // omit "empty" hit
        // Insert just a hit into the score
        tp->hitRefs.insert(*hr.second);
    }
    if(tp->hitRefs.empty()) return false;
    // get local coordinates for this (super)score
    RuleT::calc_score_values(*tp, _hitRefs);
    evScores.emplace(sk, tp);
    return true;
}

// one score per hit in combination
template<typename RuleT> bool
ConjugateHits<RuleT>::_make_individual_scores(StationKey sk, EventScores & evScores) {
    bool created = false;
    for(typename RuleT::HitRef_t hr : _hitRefs) {
        // for every hit in combination, do:
        if(!hr.first) continue;  // omit "empty" hit
        // Create one simple score per hit
        auto pair
            = SubScoreMaker::make_score(this->lmem(), hr.second, hr.second->first);
        if(!pair.second) {  // failed to create a score
            continue;
        }
        created = true;
        // Put score into an event
        evScores.insert(pair);
    }
    return created;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

