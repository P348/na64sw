#include "na64dp/abstractHitHandler.hh"
#include "na64calib/setupGeoCache.hh"

namespace na64dp {
namespace handlers {

/**\brief Sets track direction seed based on position seed and averaged centers
 *        of other planes
 *
 * Utility handler that finds averaged direction based on geometrical
 * information.
 *
 * \todo Care about extended attributes in placement updates as it currently
 *       just supersedes major ID.
 * */
class AverageDirSeed : public AbstractHitHandler<event::Track>
                     , public calib::SetupGeometryCache
                     {
protected:
    /// Dictionary of spatial points to be connected with detector ID
    std::unordered_map< DetID_t, std::array<float, 3> > _detCenters;
    /// Dictionary of missed entities counters, to be printed at handler done
    std::unordered_map< std::string, size_t > _missedEntities;
public:
    /// Ctr, same semantics as for selective tracks handler
    AverageDirSeed(calib::Manager &, const std::string &, log4cpp::Category &);
    /// Iterates over track's scores seeking for spatial point with least Z
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Updates "det ID to spatial points" cache
    void handle_single_placement_update(const calib::Placement &) override;
    /// Prints warning for missed entities
    void finalize() override;
};  // class PositionSeed


AverageDirSeed::AverageDirSeed( calib::Manager & cdsp
                              , const std::string & sel
                              , log4cpp::Category & logCat
                              )
        : AbstractHitHandler<event::Track>(cdsp, sel, logCat)
        , calib::SetupGeometryCache(cdsp, logCat)
        {
    cdsp.subscribe<calib::Placements>(*this, "default");
}

bool
AverageDirSeed::process_hit( EventID eid
                           , TrackID trackID
                           , event::Track & track ) {
    if(track.scores.empty()) return true;  // empty track
    std::array<float, 3> dir = {0, 0, 0};
    for( auto scorePair : track.scores ) {
        DetID did = scorePair.first;
        auto it = _detCenters.find(did);
        if( _detCenters.end() == it ) {
            auto missedIt = _missedEntities.emplace(naming()[did], 0).first;
            ++(missedIt->second);
            continue;
        }
        for(int i = 0; i < 3; ++i)
            dir[i] += it->second[i];
    }
    assert(track.fitInfo);
    // average and compute squared sum to derive direction unit vector
    {
        double sum = 0;
        for(int i = 0; i < 3; ++i) {
            dir[i] /= track.scores.size();
            dir[i] -= track.fitInfo->positionSeed[i];
            sum += dir[i]*dir[i];
        }
        sum = sqrt(sum);
        for(int i = 0; i < 3; ++i) {
            dir[i] /= sum;
        }
    }
    {  // This is rather a debug (assertion-like) check, xxx?
        float chck = dir[0]*dir[0]
                   + dir[1]*dir[1]
                   + dir[2]*dir[2]
                   ;
        chck = sqrt(chck);
        if( fabs(chck - 1) >= 1e-6 ) {
            NA64DP_RUNTIME_ERROR("Failed to derive unit vector"
                    " (|%.2e, %.2e, %.2e| - 1 is %.2e)."
                    , dir[0], dir[1], dir[2]
                    , chck - 1 );
        }
    }
    // Calculate momentum magnitude and direct it towards the averaged point,
    // keeping the magnitude
    {
        double mom = 0;
        for(int i = 0; i < 3; ++i) {
            mom += track.fitInfo->momentumSeed[i]*track.fitInfo->momentumSeed[i];
        }
        mom = sqrt(mom);
        for(int i = 0; i < 3; ++i) {
            track.fitInfo->momentumSeed[i] = dir[i]*mom;
        }
    }
    return true;
}

void
AverageDirSeed::handle_single_placement_update(const calib::Placement & pl) {
    if(!(((int) calib::Placement::kDetectorsGeometry) & ((int) pl.suppInfoType))) return;
    DetID did = naming()[pl.name];
    std::array<float, 3> c = {pl.center[0], pl.center[1], pl.center[2]};
    _detCenters[did] = c;
}

void
AverageDirSeed::finalize() {
    if( _missedEntities.empty() ) return;
    std::ostringstream oss;
    bool isFirst = true;
    for( auto p : _missedEntities ) {
        oss << (isFirst ? "" : ", ") << p.first << " (" << p.second << ")";
    }
    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::WARN
         << "There were missed entities for track seeding (least z): "
         << oss.str()
         << ".";
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( AverageDirSeed, cmgr, cfg,
        "Sets momentum direction seed to averaged center of triggered detectors" ) {
    return new na64dp::handlers::AverageDirSeed( cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , ::na64dp::aux::get_logging_cat(cfg)
            );
}

