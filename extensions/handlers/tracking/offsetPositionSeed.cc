#include "na64dp/abstractHitHandler.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

/**\brief Adds constant offset to track's position seed
 * */
class OffsetPoistionSeed : public AbstractHitHandler<event::Track> {
protected:
    float _offset[3];
public:
    /// Ctr, same semantics as for selective tracks handler
    OffsetPoistionSeed(calib::Manager & cdsp, const std::string & sel
                      , float xOffs, float yOffs, float zOffs
                      , log4cpp::Category & logCat
                      )
        : AbstractHitHandler<event::Track>(cdsp, sel, logCat)
        , _offset{xOffs, yOffs, zOffs}
        {}
    /// Modifies position seed by constant offset
    bool process_hit(EventID, TrackID, event::Track &) override;
};  // class PositionSeed

bool
OffsetPoistionSeed::process_hit( EventID
                               , TrackID
                               , event::Track & track ) {
    assert(track.fitInfo);
    for(int i = 0; i < 3; ++i) {
        assert(!std::isnan(track.fitInfo->positionSeed[i]));
        track.fitInfo->positionSeed[i] += _offset[i];
    }
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( OffsetPoistionSeed, cmgr, cfg,
        "Adds constant offset to track's position seed." ) {
    return new na64dp::handlers::OffsetPoistionSeed( cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["by"][0].as<float>()
            , cfg["by"][1].as<float>()
            , cfg["by"][2].as<float>()
            , ::na64dp::aux::get_logging_cat(cfg)
            );
}

