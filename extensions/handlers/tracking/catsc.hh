#pragma once

#if defined(catsc_FOUND) && catsc_FOUND

/**\file
 * \brief Integration classes for CATS(C) track finding library
 *
 * This header file provides classes implementing some of the CATS(C)
 * interfaces to make CATS(C) work with NA64sw score data. Utility classes
 * defining reentrant track collector and various filters are also provided.
 *
 * There are multiple filters provided to let analysis pipeline benefit from
 * the setup geometry. For instance, if we're looking for tracks crossing the
 * magnetic field, this can be properly accounted by the geometrical filter
 * loosening track finding condition on projected angle (anisotropic angular
 * filters).
 *
 * To facilitate unit testing routines and provide user code with the ability
 * to combine given filter strategies, a rather complex object scheme is
 * provided here: `CATS` class offers base interfaces for weighted
 * (`CATS::iWeightedFilter`) and unweighted ("logic", `CATS::iLogicFilter`)
 * filters. To provide elementary demonstration or satisfy
 * most simplistic case (for MC-simulated data, for instance), a very simple
 * `UniformAngleThreshold` filter implements the unweighted filter immediately,
 * specialized only on spatial points.
 *
 * Then, `iBaseMultifilter` class offers some runtime helpers for dealing with
 * 1D hits (track measurements defined as lines or line segments). It can be
 * configured with discriminatory/permissive policies to deal with particular
 * geometrical cases, depending on detector ID, line/point hits configuration,
 * etc. `iBaseMultifilter` does not inherit `CATS::iLogicFilter` or
 * `CATS::iWeightedFilter` directly, but its descendants are supposed to
 * parameterisse `CATSC_LogicMultiFilter_implem<T>` or
 * `CATSC_WeightedMultiFilter_implem<T>` helper templates to include this
 * kind of inheritance. Though this complex scheme allows to test
 * different aspects of angular filters, it impacts the performance (due to
 * C++ virtual function resolution done numerous times) and can be discouraged
 * for production usage in the future in favour of more opaque implementations.
 * */

#include <catsc/cats.hh>

#include "na64calib/placements.hh"
#include "recognizePatterns.hh"
#include "na64util/array-unwinding.hh"
#include "na64util/numerical/linalg.hh"
#include "na64event/data/event.hh"
#include "na64calib/detectorsOrder.hh"
#include "na64detID/TBName.hh"
#include "na64dp/abstractHitHandler.hh"
#include "util-1d-hits.h"

namespace na64dp {
namespace geo {

/**\brief A wrapper over CATS(C) algo for track finding adapting CATS to NA64sw
 *
 * Explots cellular automaton-based track finding algorithm to collection of
 * available track scores producing track candidates (represented in the event
 * as `Track`s) provided by CATS(C) library.
 *
 * Uses externally-provided sorting detectors information cache to provide
 * CATS(C) with the "layers" topology. Implements
 * `iTrackPatternRecognitionMethod` interface.
 *
 * For meaning of "soft limits" see reference of the original C/C++ CATS(C)
 * interface. Soft limits is optional parameter that can be safely omitted in
 * config.
 *
 * Usage (within `MakeTracks::method` node):
 * \code{.yaml}
 *     - _type: CATS
 *      # Track collection strategy (see CATS(C) docs); str, opt
 *      collectingStrategy: strict
 *      # Restrict minimum scores per track candidate; int, opt
 *      minTrackLength: 3
 *      # Permit missing layers per trackcandidate; int, opt
 *      missingLayers: 1
 *      # Geometric filter to use within CATS; required, object
 *      filter:
 *          ...
 *      # Pre-allocated pools (affects performance); obj, opt
 *      softLimits:
 *          # Number of connection between hits; int, required
 *          nCells: 10000
 *          # Foreseen max number of hits, overall; int, required
 *          nHits: 500
 *          # Foreseen max number of backward connections; int, required
 *          nRefs: 100
 * \endcode
 *
 * \todo Document filters, pre-fitter, etc.
 * \todo Support for collecting strategies
 *
 * \note In order CATS to work, at least three stations capable of producing
 *       track scores must be available.
 *
 * \ingroup track-finding-methods
 * */
class CATS : public iTrackPatternRecognitionMethod {
public:
    /// Detector order instance type
    typedef calib::AbstractSpatialDetectorsOrder< std::map<DetID, unsigned int> > DetectorsOrder;
    /// Max arity of conjugated track point
    static constexpr util::Arity_t nMaxScoresPerHit = 8;
    /// Hit type that CATS(C) has to deal with
    typedef event::Association<event::Event, event::TrackScore>::Collection::iterator Hit;
    /// Internal filter interface, introducing supplementary method to
    /// determine whether filter is suitable for certain score ("hit")
    class iFilter {
    protected:
        virtual ~iFilter() {}
    public:
        /// Shall return whether filter can handle score
        virtual bool can_handle_score( Hit ) const = 0;
        /// Does static cast
        virtual catsc::TrackFinder<Hit>::iTripletFilterBase & as_catsc_filter() = 0;
    };  // class iFilter

    /// Shimming class for unweighted filter
    struct iLogicFilter : public iFilter
                        , public catsc::TrackFinder<Hit>::iTripletFilter {
        virtual catsc::TrackFinder<Hit>::iTripletFilterBase & as_catsc_filter() final
            { return *this; }
    };  // class iLogicFilter

    /// Shimming class for weighted filter
    struct iWeightedFilter : public iFilter
                           , public catsc::TrackFinder<Hit>::iWeightedTripletFilter {
        virtual catsc::TrackFinder<Hit>::iTripletFilterBase & as_catsc_filter() final
            { return *this; }
    };  // class iWeightedFilter

    /// Reentrant track candidates collector object
    ///
    /// Copies (pre-fits, if need) hit sequences for further track creation.
    ///
    ///\warning Draft, simplistic implementation, probably to be substituted
    class Collector : public catsc::TrackFinder<Hit>::iTrackCandidateCollector
                    , public std::vector<std::vector<Hit>>
                    {
    public:
        /// Allocates nothing
        Collector();
        /// Deletes reentrant pool
        virtual ~Collector();
        /// Re-sets reentrant pool and forwards exec to parent's method
        void reset();  // xxx?
        void collect(const cats_HitData_t *, size_t) override;
    };  // class CATS::Collector

    /// Shim class implementing track composition (within event lifecycle)
    ///
    /// Implements `iTrackPatternRecognitionMethod::iTrackConstructor` -- a
    /// guard type proxying calls to track retrieval procedures. Forwards
    /// calls to collector
    class TrackComposer : public iTrackPatternRecognitionMethod::iTrackConstructor {
    private:
        CATS & _cats;
        size_t _currentNTrack;
    public:
        /// Shall be initialized with owning class reference and local memory
        /// in use
        TrackComposer(CATS &, LocalMemory &);
        ~TrackComposer();
        /// Forwards call to `catsc::TrackFinder::add()`
        void add_score(ScoreEntry) override;
        /// Composes `event::Track` from track candidate
        std::pair<TrackID, mem::Ref<event::Track>> get_track_candidate() override;
    };  // class CATS::TrackComposer

    /// Collection strategy method type
    typedef void (catsc::TrackFinder<Hit>::*CollectionStrategy)
                (catsc::TrackFinder<Hit>::iTrackCandidateCollector &, cats_LayerNo_t);
protected:
    /// Ref to logger (used also by nested class(es)
    log4cpp::Category & _log;
    /// Ptr to externally-provided detectors ordering implementation
    DetectorsOrder * _detectorsOrder;
    /// Set of auxiliary counters to warn user if there were no scores during lifetime
    std::unordered_map<size_t, size_t> _scoresCounts;
    /// Filter object
    iFilter * _filterPtr;
    size_t _catscSoftLimitNCells  ///< soft limit for max number of cells per event
         , _catscSoftLimitNHits  ///< soft limit for max number of hits per event
         , _catscSoftLimitRefs  ///< soft limit for max number of reference per event
         ;
    unsigned int _minLength  ///< min length (# of layers) per track
               , _nMissingLayers  ///< max number of missed layers per track
               ;
    /// Ptr to CATS(C) instance itself (maintains automaton)
    catsc::TrackFinder<Hit> * _tf;
    /// Ptr to collection method currently in use
    CollectionStrategy _collectionStrategy;
    /// Reentrant collector instance
    Collector _collector;
    /// Set to true if CATS instance evaluated succcessfully
    bool _evaluated;
protected:
    /// Returns reference to track finder instance (allocating it if need)
    catsc::TrackFinder<Hit> & _catsc_implem();
    /// Returns reentrant filter instance
    iFilter & _filter()
        { return *_filterPtr; }
    /// Evaluates CATS(C) implementation with added scores.
    void _evaluate_and_collect();
    /// Called by track constructor's destructor to drop the cached states
    void _reset();
public:
    /// Creates shim handle to collect scores and produce tracks
    std::shared_ptr<iTrackConstructor> new_constructor(LocalMemory &) override;
    /// Subscribes created object to calibration updates
    CATS( CollectionStrategy collectionStrategy
        , iFilter * filterPtr
        , DetectorsOrder * order
        , cats_LayerNo_t minTrackLength
        , cats_LayerNo_t nMissingLayers
        , log4cpp::Category & logCat
        , size_t softLimitNCells=10000
        , size_t softLimitNHits=100
        , size_t softLimitNRefs=1000
        );
    /// Prints warning about missed placements/incomplete scores
    void finalize() override;
    bool evaluated() const { return _evaluated; }
    ///\brief Concatenates (with `and`) additional detector selection criteria
    ///
    /// CATS uses static, pre-computed topology, so number (order) of permitted
    /// layers should be re-considered once additional selection criteria
    /// appear.
    void additional_selection_criterion(const std::pair<std::string, DetSelect *> &) override;
    ///\brief Sets permitted zone number(s)
    ///
    /// Zone number affects expected layers topology, so set of allowed zone
    /// number(s) must be handled accordingly.
    void set_zone_numbers(const std::vector<ZoneID_t> & zoneNum) override;
};  // class CATS



/**\brief Simple flat angle filter for 2D hits only
 *
 * This is probaly a simplest possible CATS filter calculating angle between
 * two linear segments provided within track finding procedure. If angle
 * exceeds the threshold, triplet is considered bad and will not contribute
 * to any track candidate.
 *
 * This filter is useful for basic track finding strategies, mostly for MC
 * track points. The filter will provide input for fitting procedure and track
 * selection is then done based by (potentially elaborated) track fitting
 * assesment.
 *
 * This implementation assumes that hit's MRS coordinate is set (point-like
 * scores -- 2D hits, spatial points, etc). The filter won't consider 1D
 * scores.
 *
 * \ingroup catsc-filters
 * */
class UniformAngleThreshold : public CATS::iLogicFilter {
private:
    float threshold;
public:
    UniformAngleThreshold(float a) : threshold(a) {}
    /// Returns true for scores with global coordinates set
    ///
    /// Selects only scores with global coordinates set.
    bool can_handle_score(CATS::Hit pl) const override;
    /// Checks triplet angle against threshold
    bool matches( const CATS::Hit & a_
                , const CATS::Hit & b_
                , const CATS::Hit & c_ ) const override;
};

/**\brief Base class of elaborated geometrical filter
 *
 * This is the base class for weighted or discrete implementations of CATS(C)
 * geometrical filters. It is designed to resolve cases with 1D hits.
 *
 * Considering both 1D scores and scores providing spatial points, with given
 * tiplet of hits, one can derive following geometrical cases:
 *
 *  1) Case of three points (`3p`)
 *  2) Case of two points and one line (`2p1l`)
 *  3) Case of one point and three line (`1p2l`)
 *  4) Case of three lines (`3l`)
 *
 * While 1st and 2nd cases can be efficiently treated by (possibly anisotropic)
 * angular measures, 3rd and 4th are more complicated ones since with given
 * point and two lines there is always a line connecting them and in case of
 * three lines one can build a linear surface.
 *
 * This class implements a base to dispatch these cases among certain virtual
 * methods that should be further implemented by descendant classes.
 *
 * \ingroup catsc-filters
 * */
class iBaseMultifilter {
public:
    /// Floating point type used for geometrical description within the filter
    ///
    /// Coincides with score's type
    typedef std::remove_extent<decltype(event::TrackScore::gR)>::type HitCoordinateReal_t;

    /// Flag making filter to ignore triplets of spatial points, returned
    /// weight is `-1.0`.
    static constexpr uint32_t kDismiss_3p   = 0x1;
    /// Flag making filter to unconditionally accept triplets of spatial
    /// points (with weight `1.0`)
    static constexpr uint32_t kPass_3p      = 0x2;
    // (if none of above opts not set, points triplet will be forwarded to
    // `_consider_3p()` method).

    /// Flag making filter to ignore hit triplets consisting of two spatial
    /// points and one line (returned weight `-1.0`)
    static constexpr uint32_t kDismiss_2p1l = 0x4;
    /// Flag making filter to unconditionally accept hit triplets consisting of
    /// two spatial points and one line (with weight `1.0`)
    static constexpr uint32_t kPass_2p1l   = 0x8;
    // (if none of above opts not set, 2p1l case will be forwarded
    // to `_consider_2pl1l()` )

    /// Flag making filter to ignore hit triplets consisting of one spatial
    /// point and two lines (returned weight `-1.0`)
    static constexpr uint32_t kDismiss_1p2l = 0x10;
    /// Flag making filter to unconditionally accept hit triplets consisting of
    /// one spatial point and two lines (with weight `1.0`)
    static constexpr uint32_t kPass_1p2l    = 0x20;

    static constexpr uint32_t kDismiss_3l   = 0x40;
    static constexpr uint32_t kPass_3l      = 0x80;
private:
    /// Flags steering the procedure
    uint32_t _flags;
    /// Reference to logger
    log4cpp::Category & _L;
protected:
    virtual cats_Weight_t _consider_3p( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const = 0;
    virtual cats_Weight_t _consider_2p1l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const = 0;
    virtual cats_Weight_t _consider_1p2l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const = 0;
    virtual cats_Weight_t _consider_3l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const = 0;
    /// Parameterises dispatching with given flags
    iBaseMultifilter( uint32_t flags
                    , log4cpp::Category & log_ ) : _flags(flags)
                                                 , _L(log_)
                                                 {}
public:
    /// Returns true for all the scores
    bool can_handle_score_(CATS::Hit pl) const { return true; }
    /// Dispatches the cases to virtual methods
    cats_Weight_t weight_( const CATS::Hit & a_
                         , const CATS::Hit & b_
                         , const CATS::Hit & c_ ) const;
    /// Returns (const-casted) logger instance
    log4cpp::Category & log() const
        { return const_cast<log4cpp::Category&>(_L); }
};  // class Multifilter


/**\brief Most basic multifilter, compares 3p and 2p1l cases vs angular threshold
 *
 * Basic implementation of the multifilter, similar to `UniformAngleThreshold`,
 * but support also `2p1l` case.
 *
 * \note This is abstract base for actual implementation, without calib cache
 *
 * \ingroup catsc-filters
 * */
class iIsotropicAngleMultifilter : public iBaseMultifilter {
private:
    /// Discrimination threshold
    double _weightThreshold;
protected:
    /// Copies score's global coordinates to dest array (2D hit)
    void _copy_score_val_point(HitCoordinateReal_t *, DetID, const event::TrackScore &) const;
    /// Copies score's global direction and r0 to dest array representing
    /// spatial line (1D hit)
    virtual void _copy_score_val_line( HitCoordinateReal_t * dest
                                     , DetID
                                     , const event::TrackScore & src
                                     ) const = 0;
    /// Returns `true` if given three points can be a tracklet
    virtual cats_Weight_t _check_3p( DetID, DetID, DetID
                                   , const HitCoordinateReal_t *
                                   , const HitCoordinateReal_t *
                                   , const HitCoordinateReal_t *
                                   ) const;

    /// Checks point triplet versus angular threshold
    virtual cats_Weight_t _consider_3p( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const override;
    /// Finds a point on line defined by perpendicular angle on other two
    /// points. Then checks the point triplet versus angular threshold
    virtual cats_Weight_t _consider_2p1l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const override;
    /// Raises error (forbidden case)
    virtual cats_Weight_t _consider_1p2l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const override;
    /// Raises error (forbidden case)
    virtual cats_Weight_t _consider_3l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const override;
protected:
    iIsotropicAngleMultifilter( double angularThreshold
                              , log4cpp::Category & l
                              , uint32_t flags = kDismiss_3l | kDismiss_1p2l
                              )
        : iBaseMultifilter(flags, l)
        , _weightThreshold(1. - angularThreshold/M_PI)
        {}
    /// Returns weight threshold currently in use (weights below this value
    /// will be ignored).
    ///
    /// This is the `1 - angle/pi` value where `angle` is what provided in ctr.
    double weight_threshold() const { return _weightThreshold; }
};

///\brief Implementation of isotropic angular multifilter
///
/// This class implements geometrical information handling, for data taken from
/// placements calib data for `iIsotropicAngleMultifilter`.
///
/// \ingroup catsc-filters
class IsotropicAngleMultifilter : public iIsotropicAngleMultifilter
                                , public calib::SetupGeometryCache {
private:
    /// Cache of vectors for the planes
    std::unordered_map<PlaneKey, util::Transformation> _pc;
protected:
    /// Copies score's global direction and r0 to dest array representing
    /// spatial line (1D hit)
    void _copy_score_val_line( HitCoordinateReal_t *, DetID, const event::TrackScore &) const override;
public:
    /// Sets flags to dismiss 1D hits for `1p2l` and `3l` cases
    IsotropicAngleMultifilter( double angularThreshold
                             , calib::Dispatcher & cdsp
                             , log4cpp::Category & logCat
                             , uint32_t flags= kDismiss_3l | kDismiss_1p2l
                             , const std::string & detNamingSubclass="default"
                             , const std::string & placementsCalibClass="default"
                             )
        : iIsotropicAngleMultifilter(angularThreshold, logCat, flags)
        , calib::SetupGeometryCache( cdsp
                                   , logCat
                                   , detNamingSubclass
                                   , placementsCalibClass )
        {}
    /// Updates stored u0/v vectors for planes (to derive linear hits)
    void handle_single_placement_update(const calib::Placement &) override;
};

/// Implements `CATS::iLogicFilter` with one of 
template<typename T>
class CATSC_LogicMultiFilter_implem
        : public CATS::iLogicFilter
        , public T {
public:
    bool can_handle_score( CATS::Hit h ) const override
        { return T::can_handle_score_(h); }
    
    bool matches( const CATS::Hit & a_
                , const CATS::Hit & b_
                , const CATS::Hit & c_ ) const override
        { return T::weight_(a_, b_, c_) > 0; }
    // fwding ctr
    template<typename ... ArgsTs>
    CATSC_LogicMultiFilter_implem( ArgsTs &&  ... args )
        : T(args...)
        {}
};

template<typename T>
class CATSC_WeightedMultiFilter_implem
        : public CATS::iWeightedFilter
        , public T {
public:
    bool can_handle_score( CATS::Hit h ) const override
        { return T::can_handle_score_(h); }

    cats_Weight_t weight( const CATS::Hit & a_
                        , const CATS::Hit & b_
                        , const CATS::Hit & c_ ) const override
        { return T::weight_(a_, b_, c_); }
    // fwding ctr
    template<typename ... ArgsTs>
    CATSC_WeightedMultiFilter_implem( ArgsTs &&  ... args )
        : T(args...)
        {}
};

}  // namespace ::na64dp::geo
}  // namespace na64dp

#endif

