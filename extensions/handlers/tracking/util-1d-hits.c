#include "util-1d-hits.h"

#include <math.h>
#include <gsl/gsl_linalg.h>

void
catsc_util_2p1l_find_point( const Real_t * p1
                          , const Real_t * p2
                          , const Real_t * l  // l, r0
                          , Real_t * p
                          ) {
    const double a[]
        = { p2[0] - p1[0]
          , p2[1] - p1[1]
          , p2[2] - p1[2] }  // 1st base vector for the plane
        , a2 = a[0]*a[0] + a[1]*a[1] + a[2]*a[2]  // a^2
        , al = a[0]*l[0] + a[1]*l[1] + a[2]*l[2]  // a.l
        ;
    double nom = 0, denom = 0;
    for(int j = 0; j < 3; ++j) {
        double factor = a[j]*al - l[j]*a2;
        nom   += (p1[j] - l[3 + j])*factor;
        denom += l[j]*factor;
    }
    const double t = nom / denom;
    //printf("  %f = %f / %f\n", t, nom, denom);  // XXX
    p[0] = l[0]*t + l[3];
    p[1] = l[1]*t + l[4];
    p[2] = l[2]*t + l[5];
    #if 0  // in-plane check
    double rt = 0;
    for(int i = 0; i < 3; ++i) {
        rt += (p[i] - p1[i])*(a[i]*al - l[i]*a2);
    }
    printf(" in-plane: %f\n", rt);
    #endif
}

#if 0
//     Real_t p1[] = {-1.5, 0, -3}
// , p2[] = {   1, 0, 2 }
// , l[]  = {-2, 0, -1,-3, 1, 4}
// , p3[3]
//;
int
main(int argc, char * argv[]) {
    #if 0
    Real_t p1[] = {-1.1, 1.23, 12}
         , p2[] = {-100.5, 15.1, -10}
         , l[]  = {1, 0, 0,-1, 1, 2}
         , p3[3]
         ;
    #else
    Real_t p1[] = {-1.5, 0, -3}
         , p2[] = {   1, 0, 2 }
         , l[]  = {-2, 0, -1,-3, 1, 4}
         , p3[3]
         ;
    #endif
    find_3rd_ptr_2p1l(p1, p2, l, p3);
    printf( " -> (%f, %f, %f)\n", p3[0], p3[1], p3[2] );
    return 0;
}
#endif

void
catsc_util_1p2l_find_line( const Real_t * p1
                         , const Real_t * l1
                         , const Real_t * l2
                         , Real_t * l
                         ) {
    // alias r0 for both lines
    const Real_t * r10 = l1 + 3
               , * r20 = l2 + 3
               ;
    // pre-calc differences (upper row of determinant)
    const double d1[3] = { p1[0] - r10[0], p1[1] - r10[1], p1[2] - r10[2]}
               , d2[3] = {-p1[0] + r20[0],-p1[1] + r20[1],-p1[2] + r20[2]}
               ;
    //printf( "  %.2f  %.2f  %.2f\n  %.2f  %.2f  %.2f\n  m n p\n"  // XXX
    //      , d2[0], d2[1], d2[2]
    //      , l2[0], l2[1], l2[2]
    //      );
    // calc determinant parts
    const double c[2][3] = {
        { d1[1]*l1[2] - d1[2]*l1[1]
        ,-d1[0]*l1[2] + d1[2]*l1[0]
        , d1[0]*l1[1] - d1[1]*l1[0]
        },
        { d2[1]*l2[2] - d2[2]*l2[1]
        ,-d2[0]*l2[2] + d2[2]*l2[0]
        , d2[0]*l2[1] - d2[1]*l2[0]
        }
    };
    //printf(" c1: %f, %f, %f\n", c[0][0], c[0][1], c[0][2]);  // XXX
    //printf(" c2: %f, %f, %f\n", c[1][0], c[1][1], c[1][2]);  // XXX
    // Now determinants c1,c2 defines linear equations of the form
    //  c1[0]*l_x + c1[1]*l_y + c1[2]*l_z = 0
    //  c2[0]*l_x + c2[1]*l_y + c2[2]*l_z = 0
    // Or
    //  c1[0]*(l_x/l_z) + c1[1]*(l_y/l_z) = - c1[2]
    //  c2[0]*(l_x/l_z) + c2[1]*(l_y/l_z) = - c2[2]
    //
    // Note, that practically, l_x -> 0, l_y -> 0, l_z != 0 for most of the
    // times and we can tolerate loss of the precision for l_z -> 0. Yet,
    // numerical stability is extremely important question here...
    //
    // I am in doubt, whether hand-written GE will be more performant/stable
    // here than GSL
    #if 0
    int n = 0, m = 1;
    if(fabs(c[0][0]) < 1e-8 && fabs(c[1][0]) > 1e-8) { n = 1; m = 0; }
    // ...
    # else
    double a_[] = { c[0][0], c[0][1]
                  , c[1][0], c[1][1] }
         , b_[] = {-c[0][2],-c[1][2] }
         , x_[2];
    gsl_matrix_view m = gsl_matrix_view_array(a_, 2, 2);
    gsl_vector_view b = gsl_vector_view_array(b_, 2);

    //gsl_vector_fprintf(stdout, &b.vector, "%g");  // XXX
    //gsl_matrix_fprintf(stdout, &m.matrix, "%g");  // XXX
    
    //gsl_vector * x = gsl_vector_alloc (2);
    gsl_vector_view x = gsl_vector_view_array(x_, 2);
    int s;
    gsl_permutation * p = gsl_permutation_alloc(2);
    gsl_linalg_LU_decomp(&m.matrix, p, &s);
    gsl_linalg_LU_solve(&m.matrix, p, &b.vector, &x.vector);
    //printf ("x = \n");
    //gsl_vector_fprintf (stdout, x, "%g");
    gsl_permutation_free(p);
    // Obtained X is {l_x/l_z, l_y/l_z}, lets return it in normalized form
    double lz = sqrt(1/(x_[0]*x_[0] + x_[1]*x_[1] + 1));
    l[0] = x_[0]*lz;
    l[1] = x_[1]*lz;
    l[2] = lz;
    #endif
}

Real_t
catsc_util_3p_angle( const Real_t * a
                   , const Real_t * b
                   , const Real_t * c
                   ) {
    Real_t p[3][3] = {
        { a[0], a[1], a[2] },
        { b[0], b[1], b[2] },
        { c[0], c[1], c[2] }
    };
    Real_t scProduct = 0
         , mod1 = 0
         , mod2 = 0
         ;
    // p1 = a - b, p2 = c - b
    // acos(p1.p2/sqrt(|p1^2||p2^2|))
    for( int i = 0; i < 3; ++i ) {
        p[0][i] -= p[1][i];
        mod1 += p[0][i]*p[0][i];
        p[2][i] -= p[1][i];
        mod2 += p[2][i]*p[2][i];

        scProduct += p[0][i]*p[2][i];
    }
    if( 0. == mod1 || 0. == mod2 || isnan(scProduct)
     || isnan(mod1)|| isnan(mod2) ) {
        return nan("0");
    }
    // TODO: substitute this with approximation
    return acos(scProduct/sqrt(mod1*mod2));
}

#if 0
static const Real_t ps[][3] = {
    {1, 1, 1},
    {0, 0, 0}
    // ...
};

static const Real_t ls[][6] = {
    { 1, 2, 3,  0,  0,  0 },
    { 2, 1, 4,  1,  2,  3 },
    { 2, 1, 4,  0,  0,  0 },
    // ...
};

int
main(int argc, char * argv[]) {
    const Real_t * p  = ps[0];
    const Real_t * l1 = ls[0]
               , * l2 = ls[2]
               ;
    Real_t l[3];
    case_1p2l(p, l1, l2, l);
    printf("{%f, %f, %f}\n", l[0], l[1], l[2]);
}
#endif

