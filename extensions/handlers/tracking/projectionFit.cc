#include "na64dp/abstractHitHandler.hh"
#include "na64util/numerical/planarFit.h"

namespace na64dp {
namespace handlers {

/**\brief Performs linear fit of hits on the same projection
 *
 * For set of scores provided by a track candidate computes a fitting track by
 * following algorithm:
 *  1. Fit all the 1D scores with a plane P defined by notmal vector N and some
 *     spatial point A.
 *  2. For every 1D score intersecting P get an intersection point and angle.
 *  3. Within the P, fit resulting intersection points with a line
 *  ...
 *
 *  \todo implement
 * */
class FitProjectedScores : public AbstractHitHandler<event::Track> {
};  // class FitProjectedScores

}  // namespace ::na64dp::handlers
}  // namespace na64dp

