#pragma once

#include "na64calib/dispatcher.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/track.hh"

namespace na64dp {
namespace handlers {

/**\brief Auxiliary class for iterating scores within a track.
 *
 * This is utility class restricting iteration over track scores that are only
 * used by (i.e. included in) some track.
 *
 * \note `process_event()` for subordinate handlers will not be called.
 * */
class TrackScoreIteratingHandler : public AbstractHitHandler<event::Track>
                                 {
protected:
    /// List of subordinate handler ptrs
    std::vector<AbstractHitHandler<event::TrackScore>*> _subpipe;
public:
    TrackScoreIteratingHandler( calib::Dispatcher & cdsp
                              , const std::string & selection
                              , std::vector<AbstractHitHandler<event::TrackScore>*> & subpipe
                              , log4cpp::Category & logCat
                              )
        : AbstractHitHandler<event::Track>(cdsp, selection, logCat)
        , _subpipe(subpipe)
    {}
    /// Invoked for each track; forwards execution to per-score handlers
    ///
    /// Note that forwarded execution bypasses the `process_event()` method
    /// of subordinate handlers.
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Dispatches call to all the subordinate score's handlers
    void finalize() override;
};  // class TrackScoreIterator

}  // namespace ::na64dp::handlers
}  // namespace na64dp

