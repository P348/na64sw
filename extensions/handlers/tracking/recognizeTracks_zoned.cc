#include "recognizePatterns.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace errors {
/// Thrown by zoning wrapper; means that geometrical zone is not described
class NoZoneAssignedForDetector : public GenericRuntimeError {
public:
    const DetID_t did;
    const std::string detName;
    NoZoneAssignedForDetector( DetID_t did_
                             , const std::string & name
                             ) : GenericRuntimeError( util::format("No geometrical zone"
                                     " assigned for detector \"%s\" (%#x)"
                                     , name.c_str(), did_ ).c_str() )
                               , did(did_)
                               , detName(name)
                               {}
};
/// Thrown by zoning wrapper; means that geometrical zone is described, but no
/// method is assigned for it
class NoPatternRecognitionForZone : public GenericRuntimeError {
public:
    const int zoneID;
    NoPatternRecognitionForZone( int zoneID_
                                ) : GenericRuntimeError( util::format("No pattern recognition"
                                             " method assigned for zone (%d)"
                                            , zoneID_ ).c_str() )
                                  , zoneID(zoneID_)
                                  {}
};
/// Thrown by zoning wrapper; means that multiple methods has been assigned for
/// geometrical zone.
class MultipleMethodsForSameZone : public GenericRuntimeError {
public:
    const int zoneID;
    MultipleMethodsForSameZone( int zoneID_
                              ) : GenericRuntimeError( util::format("Multiple"
                                             " methods assigned for zone (%d)"
                                            , zoneID_ ).c_str() )
                                  , zoneID(zoneID_)
                                  {}
};
}  // namespace ::na64dp::errors

namespace geo {

/**\brief An intermediate-level pattern recognition shim
 *
 * This is not a pattern recognition method per se, but provides a way to
 * address individual pattern recognition techniques within geometrical zones.
 * It does so by dispatching calls based on subsidiary placements information.
 *
 * It is useful for certain tracking strategies -- if, for instance, track
 * can be broken onto linear segments and helices, one can think on building
 * the linear tracks first.
 * */
class ZonedPatternRecognitionMethods
        : public iTrackPatternRecognitionMethod
        , public util::Observable<calib::Placements>::iObserver
        , public calib::Handle<nameutils::DetectorNaming>
        , protected std::map<int, std::shared_ptr<iTrackPatternRecognitionMethod> >
        {
public:
    /**\brief Dispatches call to zoned method proxies, constructing them if
     *        need
     *
     * Keeps index of proxies per individual zone. Note that it won't create
     * a proxy for zone until first score of this zone appears.
     * */
    class ZonedTrackConstructor
                : public iTrackConstructor
                , protected std::map<int, std::shared_ptr<iTrackConstructor> >
                {
    private:
        ZonedPatternRecognitionMethods & _zonedMethods;
        /// Current zone iterator
        std::map<int, std::shared_ptr<iTrackConstructor> >::iterator _it;
        /// Set to true if iteration over zones should start from the beginning
        bool _startFromFirstZone;
        /// Enumerates tracks within an event. Used if subsidiary (per-zone)
        /// method does not provide track ID.
        size_t _nTrackInEvent;
    public:
        ZonedTrackConstructor( LocalMemory & lmem_
                             , ZonedPatternRecognitionMethods & m
                             )
            : iTrackConstructor(lmem_)
            , _zonedMethods(m)
            , _startFromFirstZone(true)
            , _nTrackInEvent(0)
            {}
        /// Dispatches score to zone-specific method
        ///
        /// May raise `NoZoneAssignedForDetector` and `NoPatternRecognitionForZone`
        /// depending on flags set by ctr.
        virtual void add_score(ScoreEntry) override;
        /// Returns track candidates from each zone.
        ///
        /// Zones are iterated iteratively in ascending order. Order of tracks
        /// within a zone depends on particular method. Empty track means that the
        /// sequence is depleted.
        virtual std::pair<TrackID, mem::Ref<event::Track>> get_track_candidate() override;

        friend class ZonedPatternRecognitionMethods;
    };
protected:
    log4cpp::Category & _log;
    /// When set, a score from detector without assigned zone will be quietly
    /// ignored.
    bool _ignoreScoreIfUnknownZone;
    /// When set, a score in zone without assigned pattern recognition method
    /// will be quietly ignored.
    bool _ignoreScoreIfNoMethodInZone;
    /// Maps detector ID to particular zone
    std::unordered_map<DetID_t, int> _detID2Zones;

    /// Refreshes DetID-to-zone cache
    void handle_update(const calib::Placements &) override;
public:
    /// Constructs empty the shimmering object.
    ///
    /// \note It is not practically useful until user code will specify
    ///       concrete zone-specific methods.
    ZonedPatternRecognitionMethods( calib::Dispatcher & mgr
                                  , bool ignoreNoMethod=false
                                  , bool ignoreNoZone=false
                                  );
    /// Creates new track constructor proxy (dispatching calls to per-zone
    /// constructors) bound to this method
    std::shared_ptr<iTrackConstructor> new_constructor(LocalMemory &) override;
    /// Specifies method for zone
    void method_for_zone(int, std::shared_ptr<iTrackPatternRecognitionMethod> );

    ///\brief Adds selection criterion to be combined by terminal methods
    ///
    ///\todo Is it needed, though? Design unclear...
    void additional_selection_criterion(const std::pair<std::string, DetSelect *> &) override;
    /// Throws error (can't assign zone number to zoned method)
    void set_zone_numbers(const std::vector<ZoneID_t> &) override;
    /// Forwards call to associated methods
    void finalize() override;
};  // class ::na64dp::geo::ZonedPatternRecognitionMethods


//
// Implementation
////////////////

ZonedPatternRecognitionMethods::ZonedPatternRecognitionMethods( calib::Dispatcher & mgr
                                                              , bool ignoreNoMethod
                                                              , bool ignoreNoZone
                             ) : calib::Handle<nameutils::DetectorNaming>("default", mgr)
                               , _log(log4cpp::Category::getInstance("handlers.tracking.pattern.zones"))
                               , _ignoreScoreIfUnknownZone(ignoreNoZone)
                               , _ignoreScoreIfNoMethodInZone(ignoreNoZone)
                               {
    mgr.subscribe<calib::Placements>(*this, "default");
}

void
ZonedPatternRecognitionMethods::method_for_zone( int zoneID
        , std::shared_ptr<iTrackPatternRecognitionMethod> mtd ) {
    auto ir = this->emplace(zoneID, mtd);
    // assure there is no collisions
    if( ! ir.second )
        throw errors::MultipleMethodsForSameZone(zoneID);
}

void
ZonedPatternRecognitionMethods::ZonedTrackConstructor::add_score(ScoreEntry se_) {
    DetID did = se_->first;
    // Get zone ID for the score
    int zoneID;
    {
        auto zoneNoIt = _zonedMethods._detID2Zones.find(did);
        if( _zonedMethods._detID2Zones.end() == zoneNoIt ) {
            if( _zonedMethods._ignoreScoreIfUnknownZone ) return;
            throw errors::NoZoneAssignedForDetector( did,
                        _zonedMethods.calib::Handle<nameutils::DetectorNaming>::operator*()[did]
                    );
        }
        zoneID = zoneNoIt->second;
    }
    // Find constructor proxy for the zone
    auto ctrIt = this->find(zoneID);
    if(this->end() == ctrIt) {
        // No proxy for this zone, create one
        auto mtdIt = _zonedMethods.find(zoneID);
        if(_zonedMethods.end() == mtdIt) {
            // no method assigned for zone
            if( _zonedMethods._ignoreScoreIfNoMethodInZone ) return;
            throw errors::NoPatternRecognitionForZone(zoneID);
        }
        ctrIt = this->emplace(zoneID, mtdIt->second->new_constructor(lmem)).first;
    }
    ctrIt->second->add_score(se_);
}

std::pair<TrackID, mem::Ref<event::Track>>
ZonedPatternRecognitionMethods::ZonedTrackConstructor::get_track_candidate() {
    typedef std::map<int, std::shared_ptr<iTrackConstructor> > Parent;
    // Initialize per-zone iterator with the beginning of track sequence, if
    // need
    if(_startFromFirstZone) {
        _it = Parent::begin();
        _startFromFirstZone = false;
    }
    // If zones sequence is not yet depleted, do
    if( _it != Parent::end() ) {
        do {
            ++_nTrackInEvent;
            std::pair<TrackID, mem::Ref<event::Track>> rp
                    = _it->second->get_track_candidate();
            // if track ID is provided by subsidiary method, keep it;
            // Otherwise, set it to current zone ID + persistent counter.
            TrackID & tid = rp.first;
            if(!tid.zones()) {
                tid.zones(_it->first);
            }
            if(!tid.number()) {
                tid.number(_nTrackInEvent);
            }
            mem::Ref<event::Track> r = rp.second;
            if((!r) || r->scores.empty()) {
                _zonedMethods._log << log4cpp::Priority::DEBUG
                    << "No more tracks provided by zone #" << _it->first
                    << " (" << (r ? "empty track returned" : "null track returned")
                    << ")"
                    ;
                ++_it;  // go to next zone
            } else {
                _zonedMethods._log << log4cpp::Priority::DEBUG
                    << "Track provided by zone #"
                    << _it->first << ".";
                return rp;  // return current track
            }
        } while(_it != Parent::end());
    }
    _zonedMethods._log << log4cpp::Priority::DEBUG
        << "No more tracks provided by zones.";
    return std::pair<TrackID, mem::Ref<event::Track>>(TrackID(0x0, 0)
            , nullptr);  // empty
}

void
ZonedPatternRecognitionMethods::handle_update(const calib::Placements & placements) {
    for( auto & pl : placements ) {
        //auto & pl = pl_.data;
        if(!(((int) calib::Placement::kDetectorsGeometry) & ((int) pl.suppInfoType))) continue;
        DetID did = calib::Handle<nameutils::DetectorNaming>::operator*()[pl.name];
        _detID2Zones[did] = pl.zone;
        _log << log4cpp::Priority::DEBUG
            << "Detector \"" << pl.name << "\" assigned to zone #" << (int) pl.zone;
    }
}

std::shared_ptr<iTrackPatternRecognitionMethod::iTrackConstructor>
ZonedPatternRecognitionMethods::new_constructor(LocalMemory & lm) {
    return std::make_shared<ZonedTrackConstructor>(lm, *this);
}

void
ZonedPatternRecognitionMethods::additional_selection_criterion(
        const std::pair<std::string, DetSelect *> & sel) {
    _log << log4cpp::Priority::DEBUG
         << "Delivering selector " << sel.first << " to subjugated"
            " (zoned) track finding methods";
    std::map<int, std::shared_ptr<iTrackPatternRecognitionMethod> > & self = *this;
    for( auto & zoneMtd : self ) {
        zoneMtd.second->additional_selection_criterion(sel);
    }
}

void
ZonedPatternRecognitionMethods::set_zone_numbers(const std::vector<ZoneID_t> &) {
    NA64DP_RUNTIME_ERROR("Setting zone for \"Zoned\" method itslef is prohibited.");
    // TODO: unclear, whether this can be usefult, though.
}

void
ZonedPatternRecognitionMethods::finalize() {
    for(auto & p : *this) {
        p.second->finalize();
    }
}

NA64SW_REGISTER_TRACK_PATTERN_RECOGNITION_METHOD( Zoned, calibMgr, cfg,
        "Track recognition specified for individual zones." ) {
    // Get the ignore flags for the zoned wrapper
    const auto doIgnoreCases_ = cfg["tolerateMissing"]
                              ? cfg["tolerateMissing"].as<std::vector<std::string>>()
                              : std::vector<std::string>()
                              ;
    std::set<std::string> doIgnoreCases(doIgnoreCases_.begin(), doIgnoreCases_.end());
    // Instantiate (empty) zoned wrapper
    auto r = new na64dp::geo::ZonedPatternRecognitionMethods( calibMgr
            , doIgnoreCases.find("method") != doIgnoreCases.end()
            , doIgnoreCases.find("zone") != doIgnoreCases.end()
            );
    // Populate the wrapper with per-zone instances
    for( const auto & perZoneCfg : cfg["zones"] ) {
        // Get list of zones to be used
        auto zoneIDs = perZoneCfg["_zones"].as<std::vector<int>>();
        // Instantiate the method
        std::shared_ptr<na64dp::geo::iTrackPatternRecognitionMethod> mtd;
        mtd.reset(
            na64dp::VCtr::self().make<na64dp::geo::iTrackPatternRecognitionMethod>(
                    perZoneCfg["_type"].as<std::string>(),
                    calibMgr,
                    perZoneCfg
                ) );
        // Notify method with zone number(s)
        //_log << log4cpp::Priority::DEBUG
        //     << "Delivering " << zoneIDs.size() << " zone number(s) to method "
        //     << (void*) mtd.get()
        //     ;
        mtd->set_zone_numbers(std::vector<ZoneID_t>(zoneIDs.begin(), zoneIDs.end()));
        // Put the method into aggregative wrapper
        for( int zoneID : zoneIDs ) {
            r->method_for_zone(zoneID, mtd);
        }
    }
    // Return populated wrapper
    return r;
}

}  // namespace ::na64dp::geo
}  // namespace na64dp

