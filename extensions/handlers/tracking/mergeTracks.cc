#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Merges tracks from given zones into adjoint ones
 *
 * From set of tracks provided within an event, picks up tracks with certain
 * zone patterns and builds a merged track. Scores from source tracks can be
 * selected by various restrictive criteria.
 * */
class MergeTracks : public AbstractHitHandler<event::Track> {
public:
    struct ZoneSettings {
        /// Track is refused from merging if number of scores is less than this
        /// number
        size_t minScores;  // TODO: make use of it
        /// Minimal significance level for accepted tracks
        double minSignificance;
        /// Chi squared per ndf cut
        double maxChiSqPerNDF;
        // TODO: ... other thresholds for track selection
        
        /// Scores with this (or lesser) weight will be denied
        double minWeight;
        /// Scores with unbiased residual greater than this will be denied
        double maxUResiduals[3];
        // TODO: ... other thresholds for individual scores

        /// Whether or not the track passes requirements
        virtual bool track_matches(const event::Track &) const;

        /// Whether or not score passes requirements
        virtual bool score_matches(DetID_t, const event::TrackScore &) const;
    };
protected:
    /// List of zone IDs to be considered
    std::unordered_map< ZoneID_t, std::vector< mem::Ref<event::Track> > > _cache;
    /// Patterns to exclude/include
    std::unordered_set<ZoneID_t> _permittedPatterns, _forbiddenPatterns;
    /// Individual zone settings
    std::unordered_map<ZoneID_t, ZoneSettings> _zoneSettings;
    // ... other settings
protected:
    /// Helper object providing all possible track combinations based on cache
    class Generator {
    private:
        /// Keeps iterators over tracks collected in zones: index vs cache
        /// entry
        std::vector< std::pair< size_t, decltype(_cache)::iterator > > state;
        /// Sequentially incrments ind
        bool next_combination() {
            for( auto p : state ) {
                if(p.first + 1 < p.second->second.size()) {
                    // incrment index of track for current zone until cache
                    // entry depleted
                    ++p.first;
                    return true;
                }
            }
            // no available cache entries remain
            return false;
        }
        /// Prevents from re-iterating
        bool _stateIsValid;
    public:
        Generator(decltype(_cache) & c) : _stateIsValid(true) {
            for(auto it = c.begin(); it != c.end(); ++it) {
                state.push_back( typename decltype(state)::value_type(0, it) );
            }
        }
        /// Fills args with new sequence, if available. Returns `false` if failed.
        bool get_track_sequence( std::unordered_map< ZoneID_t, mem::Ref<event::Track> > & sq
                               , ZoneID_t & commonPattern
                               ) {
            sq.clear();
            commonPattern = 0x0;
            if(!_stateIsValid) return false;
            for( auto p : state ) {
                if( p.second->second.empty() ) continue;
                auto ir = sq.emplace( p.second->first  // zone pattern (cache's key)
                                    , p.second->second[p.first]  // track
                                    );
                assert(ir.second);
                commonPattern |= p.second->first;
            }
            _stateIsValid = next_combination();
            return !sq.empty();
        }
    };
public:
    /// Iterates over the tracks wth parent's method, then builds new tracks
    ProcRes process_event(event::Event & e) override;
    /// Populates cache with track instances
    bool process_hit(EventID, TrackID, event::Track &) override;

    MergeTracks( calib::Manager & cmgr
               , const std::string & sel
               , const std::list<ZoneID_t> & zl
               , log4cpp::Category & logCat
               ) : AbstractHitHandler<event::Track>(cmgr, sel, logCat)
                 {
        if(zl.empty())
            NA64DP_RUNTIME_ERROR("Empty list of zone IDs for merging.");
        // initialize empty cache objects for further usage
        for( auto zid : zl )
            _cache.emplace(zid, decltype(_cache)::mapped_type());
    }
};  // class MergeTracks

namespace aux {
MergeTracks::ZoneSettings
parse_merge_tracks_zone_settings( const YAML::Node & n ) {
    MergeTracks::ZoneSettings zs;
    zs.minScores = n["minScores"] ? n["minScores"].as<int>() : 1;
    zs.minSignificance = n["minSignificance"]
                       ? n["minSignificance"].as<double>()
                       : 0;
    zs.maxChiSqPerNDF = n["maxChiSqPerNDF"]
                      ? n["maxChiSqPerNDF"].as<double>()
                      : std::numeric_limits<double>::infinity();
    // ...
    return zs;
}
}  // namespace ::na64dp::handlers::aux

bool
MergeTracks::ZoneSettings::track_matches(const event::Track & track) const {
    if( track.fitInfo && track.fitInfo->pval < minSignificance ) return false;
    if( track.fitInfo && track.fitInfo->chi2/track.fitInfo->ndf > maxChiSqPerNDF ) return false;
    // ... other checks
    return true;
}

bool
MergeTracks::ZoneSettings::score_matches( DetID_t scoreID
                                        , const event::TrackScore & score) const {
    if( minWeight && minWeight > score.weight ) return false;
    for(int i = 0; i < 3; ++i) {
        if( maxUResiduals[i] && maxUResiduals[i] > score.lRUErr[i] ) return false;
    }
    // ... other scores checks
    return true;
}

AbstractHandler::ProcRes
MergeTracks::process_event(event::Event & e) {
    // Fill cache entries
    AbstractHandler::ProcRes procResult
        = AbstractHitHandler<event::Track>::process_event(e);

    if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        bool isFirst = true;
        for( const auto & cacheEntry : _cache ) {
            oss << (isFirst ? "" : ", ")
                << "zone#" << std::hex << cacheEntry.first
                << " (" << cacheEntry.second.size() << " entries)"
                ;
            isFirst = false;
        }
        _log << log4cpp::Priority::DEBUG
             << "Building tracks from cache: "
             << (_cache.empty() ? std::string("(cache empty)") : oss.str()) << "."
             ;
    }

    // Iterate over resulting combinations of track pieces (generator and
    // destination variables)
    Generator g(_cache);  // yields combinations
    ZoneID_t newTrackZonePattern;
    std::unordered_map< ZoneID_t, mem::Ref<event::Track> > newTrackPieces;

    size_t nTracksBuilt = 0;
    while(g.get_track_sequence(newTrackPieces, newTrackZonePattern)) {
        assert( !newTrackPieces.empty() );  // internal generator logic error
        if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
            char bf[32];
            snprintf(bf, sizeof(bf), "%#x", newTrackZonePattern);
            _log << log4cpp::Priority::DEBUG
                 << "Processing combination of " << newTrackPieces.size()
                 << " tracks of pattern "
                 << (int) newTrackZonePattern << "."
                 ;
        }

        // If combination satisfies conditions by zone pattern, number of hits,
        // etc, create a track
        if(!_permittedPatterns.empty()) {
            auto it = _permittedPatterns.find(newTrackZonePattern);
            if(it == _permittedPatterns.end()) {
                _log << log4cpp::Priority::DEBUG
                     << "Won't merge: zone pattern is not among permitted patterns.";
                continue;
            }
        }

        if(!_forbiddenPatterns.empty()) {
            auto it = _permittedPatterns.find(newTrackZonePattern);
            if(it != _permittedPatterns.end()) {
                _log << log4cpp::Priority::DEBUG
                     << "Won't merge: zone pattern is among forbidden patterns.";
                continue;
            }
        }

        if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
            std::ostringstream oss;
            bool isFirst = true;
            for(auto pieceEntry : newTrackPieces) {
                oss << (isFirst ? "" : ", ")
                    << std::hex << pieceEntry.first
                    << " (" << pieceEntry.second << ")";
                isFirst = false;
            }
            _log << log4cpp::Priority::DEBUG
                 << "Considering pieces: " << oss.str().c_str() << ".";
        }
        
        // new track scores kept here to be further sorted
        std::unordered_map<DetID_t, mem::Ref<event::TrackScore>> newTrackScores;
        for(auto pieceEntry : newTrackPieces) {
            //ZoneID_t pieceZoneID = pieceEntry.first;
            auto zoneSettingsIt = _zoneSettings.find(pieceEntry.first);
            if(zoneSettingsIt == _zoneSettings.end()) {
                // unconditionally add scores from this zone to new track
                for( auto s : *pieceEntry.second ) {
                    newTrackScores.insert(s);
                }
            } else {
                if( !zoneSettingsIt->second.track_matches(*pieceEntry.second) ) continue;
                // conditionally add scores from this zone into new track
                for( auto s : *pieceEntry.second ) {
                    if(zoneSettingsIt->second.score_matches(s.first, *s.second))
                        newTrackScores.insert(s);
                }
            }
        }
        if(newTrackScores.size() < 2) {
            _log << log4cpp::Priority::DEBUG
                 << "Won't create a merged track as it would have "
                 << newTrackScores.size() << " scores.";
            continue;
        }
        // Create and fill new track with scores copies
        auto mergedTrackRef = lmem().create<event::Track>(lmem());
        for( auto scoreEntry : newTrackScores ) {
            auto scoreCopy = lmem().create<event::TrackScore>(lmem());
            *scoreCopy = *scoreEntry.second;
            #warning "reset score's values irrelevant to new track"
            // TODO: reset score's values irrelevant to new track OR copy only relevant...
            e.trackScores.emplace(scoreEntry.first, scoreCopy);
            mergedTrackRef->emplace(scoreEntry.first, scoreCopy);
        }
        size_t nTrack = 0;
        bool duplicateTrackID;
        do {
            TrackID tid(newTrackZonePattern, ++nTrack);
            duplicateTrackID = (e.tracks.find(tid) != e.tracks.end());
        } while(duplicateTrackID);
        auto ir = e.tracks.emplace(TrackID(newTrackZonePattern, nTrack), mergedTrackRef);
        if(_log.getPriority() >= log4cpp::Priority::DEBUG ) {
            char bf[64];
            snprintf(bf, sizeof(bf), "%#x", ir.first->first.code);
            _log << log4cpp::Priority::DEBUG
                 << "Merged track #" << bf << " created.";
        }
        ++nTracksBuilt;
    }  // iterate over sequences

    // Clear caches
    for(auto & p : _cache) p.second.clear();

    _log << log4cpp::Priority::DEBUG
         << nTracksBuilt << " new track(s) built in event by merging.";

    return procResult;
}

bool
MergeTracks::process_hit(EventID, TrackID tid, event::Track &) {
    // Find corresponding cache entry
    auto it = _cache.find(tid.zones());
    if(_cache.end() == it) return true;
    // Add track to corresponding cache entries
    it->second.push_back(_current_hit_ref());
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MergeTracks, cmgr, cfg
                , "Joins tracks to create new ones."
                ) {
    // NOTE: use transform here as ZoneID is not necessarily `int` and this
    //       may cause wrong conversion with YAMLCpp's `.as<std::vector<ZoneID>>`
    auto patLine = cfg["patterns"].as< std::vector<int> >();
    std::list<na64dp::ZoneID_t> patterns;
    // Populate `zonesList' with zone IDs from "zoneIDs" YAML node
    std::transform( patLine.begin(), patLine.end()
                  , std::back_inserter(patterns)
                  , [](int i){return i;}
                  );
    // Set `zoneList' parameters
    //zonesList.tolerateMissing = patternEntry["tolerateMissing"]
    //                          ? patternEntry["tolerateMissing"].as<int>()
    //                          : 0
    //                          ;
    //zonesList.thrPVal = patternEntry["pValueThreshold"]
    //                  ? patternEntry["pValueThreshold"].as<double>()
    //                  : 0.  // TODO: take all?
    //                  ;
    // zonesList.thrBR ...
    // zonesList.thrUR ...
    // zonesList.renewPDG ...
    // ...
    // TODO: forbidden/permitted patterns
    return new na64dp::handlers::MergeTracks(cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , patterns
            , ::na64dp::aux::get_logging_cat(cfg)
            );
}

