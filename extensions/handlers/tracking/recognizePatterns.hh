#pragma once

#include "na64event/data/track.hh"
#include "na64calib/manager.hh"
#include "na64calib/placements.hh"
#include "na64event/data/event.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace errors {
class NoZoneAssignedForDetector;
class NoPatternRecognitionForZone;
}  // namespace ::na64dp::errors
namespace geo {

/**\brief Interface for track pattern recognition method.
 *
 * Interfaces wrapper of various track pre-patterning or recognition methods.
 *
 * Implementation of this interface shall provide interface for stateful proxy
 * objects of two-staged per-event lifecycle:
 *
 *  1. Add scores to consideration with `add_score()`
 *  2. Retrieve the track candidates with `get_track_candidate()`
 *
 * At the end of the event treatment a proxy object gets deleted.
 *
 * Subclasses of (factory) `iTrackPatternRecognitionMethod` class has extended
 * lifecycle comparing to the produced proxies. Typically factory subclasses
 * are associated to pipeline handler and keep persisting caches to create
 * proxy objects efficiently.
 * */
class iTrackPatternRecognitionMethod {
public:
    /// Type of score reference used
    typedef event::Association<event::Event, event::TrackScore>::Collection::iterator
            ScoreEntry;
    /// A proxy object
    struct iTrackConstructor {
        /// Reference to local memory object used to allocate tracks
        LocalMemory & lmem;
        /// Sets the reference to local memory object in use
        iTrackConstructor(LocalMemory & lmem_) : lmem(lmem_) {}
        /// Adds track score to consideration
        virtual void add_score(ScoreEntry) = 0;
        /// Performs recognition if need and returns next track candidate from set
        virtual std::pair<TrackID, mem::Ref<event::Track>> get_track_candidate() = 0;
    };
    /// Creates new proxy. Proxies uses local memory instance to allocate tracks
    virtual std::shared_ptr<iTrackConstructor> new_constructor(LocalMemory &) = 0;
    /// Called by owning handler's `finalize()`
    virtual void finalize() {}
    /// Virtual dtr to properly address subclass instance deletion
    virtual ~iTrackPatternRecognitionMethod() {}
    ///\brief Delivers additional detector selection criteria
    ///
    /// This method is called by external code (usually, owning handler) to
    /// notify the instance that detector topology may be different. Default
    /// does nothing, but some track recognition methods are sensitive to this.
    /// Interested implementation must adjust topology accounting this
    /// selector.
    virtual void additional_selection_criterion(const std::pair<std::string, DetSelect *> &) {}
    ///\brief Delivers zone number as an additional selection criterion
    ///
    /// This method is called by external code (usually, owning handler) to
    /// notify the instance that detector topology may be different. Default
    /// does nothing, but some track recognition methods are sensitive to this.
    /// Interested implementation must adjust topology to given zone number.
    virtual void set_zone_numbers(const std::vector<ZoneID_t> & zoneNum) {}
};

}  // namespace ::na64dp::geo

/**\brief Virtual constructor traits for pattern recognition methods */
template<> struct CtrTraits<geo::iTrackPatternRecognitionMethod> {
    typedef geo::iTrackPatternRecognitionMethod * (*Constructor)
        ( calib::Manager &, const YAML::Node & );
};

}  // namespace na64dp

#define NA64SW_REGISTER_TRACK_PATTERN_RECOGNITION_METHOD( clsName, calibDsp, ni, desc ) \
static na64dp::geo::iTrackPatternRecognitionMethod * _new_ ## clsName ## _instance( na64dp::calib::Manager &  \
                                                      , const YAML::Node & );    \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::geo::iTrackPatternRecognitionMethod>( # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::geo::iTrackPatternRecognitionMethod * _new_ ## clsName ## _instance( __attribute__((unused)) na64dp::calib::Manager & calibDsp  \
                                                      , __attribute__((unused)) const YAML::Node & ni )
