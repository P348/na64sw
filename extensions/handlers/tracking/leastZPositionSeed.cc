#include "na64dp/abstractHitHandler.hh"
#include "na64calib/placements.hh"
#include "na64calib/setupGeoCache.hh"

namespace na64dp {
namespace handlers {

/**\brief Sets track position Z to the least by Z
 *
 * Utility handler that finds least Z position based on scores information
 * and puts it as position seed.
 *
 * \todo Care about extended attributes in placement updates as it currently
 *       just supersedes major ID.
 * */
class LeastZPositionSeed : public AbstractHitHandler<event::Track>
                         , public calib::SetupGeometryCache
                         {
protected:
    /// Dictionary of spatial points to be connected with detector ID
    std::unordered_map< DetID_t, std::array<float, 3> > _detCenters;
    /// Dictionary of missed entities counters, to be printed at handler done
    std::unordered_map< std::string, size_t > _missedEntities;
public:
    /// Ctr, same semantics as for selective tracks handler
    LeastZPositionSeed(calib::Manager &, const std::string &, log4cpp::Category &);
    /// Iterates over track's scores seeking for spatial point with least Z
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Updates "det ID to spatial points" cache
    void handle_single_placement_update(const calib::Placement &) override;
    /// Prints warning for missed entities
    void finalize() override;
};  // class PositionSeed


LeastZPositionSeed::LeastZPositionSeed( calib::Manager & cdsp
                                      , const std::string & sel
                                      , log4cpp::Category & logCat
                                      )
        : AbstractHitHandler<event::Track>(cdsp, sel, logCat)
        , calib::SetupGeometryCache(cdsp, logCat)
        {
    cdsp.subscribe<calib::Placements>(*this, "default");
}

bool
LeastZPositionSeed::process_hit( EventID eid
                               , TrackID trackID
                               , event::Track & track ) {
    std::array<float, 3> cLeast = {(float) std::nan("0")};
    for( auto scorePair : track.scores ) {
        DetID did = scorePair.first;
        auto it = _detCenters.find(did);
        if( _detCenters.end() == it ) {
            auto missedIt = _missedEntities.emplace(naming()[did], 0).first;
            ++(missedIt->second);
            continue;
        }
        if( std::isnan(cLeast[0]) || it->second[2] < cLeast[2] ) {
            cLeast = it->second;
            // ...
        }
    }
    if( std::isnan(cLeast[0]) ) return true;
    assert(track.fitInfo);
    for(int i = 0; i < 3; ++i)
        track.fitInfo->positionSeed[i] = cLeast[i];
    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::DEBUG
         << "Track #" << (int) trackID.code << " position seed set to {"
         << track.fitInfo->positionSeed[0] << ", "
         << track.fitInfo->positionSeed[1] << ", "
         << track.fitInfo->positionSeed[2]
         << "}.";
    return true;
}

void
LeastZPositionSeed::handle_single_placement_update(const calib::Placement & pl) {
    if(!(((int) calib::Placement::kDetectorsGeometry) & ((int) pl.suppInfoType))) return;
    DetID did = naming()[pl.name];
    std::array<float, 3> c = {pl.center[0], pl.center[1], pl.center[2]};
    _detCenters[did] = c;
}

void
LeastZPositionSeed::finalize() {
    if( _missedEntities.empty() ) return;
    std::ostringstream oss;
    bool isFirst = true;
    for( auto p : _missedEntities ) {
        oss << (isFirst ? "" : ", ") << p.first << " (" << p.second << ")";
    }
    AbstractHitHandler<event::Track>::_log << log4cpp::Priority::WARN
         << "There were missed entities for track seeding (least z): "
         << oss.str()
         << ".";
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( LeastZPositionSeed, cmgr, cfg,
        "Sets track position seed to the spatial point with least Z" ) {
    return new na64dp::handlers::LeastZPositionSeed( cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , na64dp::aux::get_logging_cat(cfg)
            );
}

