#include "na64dp/abstractHitHandler.hh"
#include "recognizePatterns.hh"

namespace na64dp {
namespace handlers {

/**\brief Roughcast track instances using track pattern recongition method(s)
 *
 * This handler incorporates certain track pattern recognition method into
 * NA64sw data processing pipeline. It wraps an instance of
 * `iTrackPatternRecognitionMethod` in the way individual events can be coped
 * with this instance -- filling with event's `TrackScore`, construction of the
 * tracks.
 *
 * \ingroup handlers handlers-tracking
 * */
class MakeTracks : public AbstractHitHandler<event::TrackScore> {
protected:
    /// Ptr to track pattern recognition method currently in use
    std::shared_ptr<geo::iTrackPatternRecognitionMethod> _mtd;
    /// Ptr to current track constructor proxy object; valid only for a single
    /// event
    std::shared_ptr<geo::iTrackPatternRecognitionMethod::iTrackConstructor>
        _trackCtrProxy;
public:
    ///\brief Constructs a handler over certain pattern recognition object
    MakeTracks( calib::Dispatcher & cdsp
              , const std::string & selection
              , std::shared_ptr<geo::iTrackPatternRecognitionMethod> mtd
              , log4cpp::Category & logCat
              )
        : AbstractHitHandler<event::TrackScore>(cdsp, selection, logCat)
        , _mtd(mtd)
        , _trackCtrProxy(nullptr)
        {
        // notify track finding/recognition method about handler's `applyTo'
        // detector selection as it may account for topology
        if(this->is_selective()) {
            this->log() << log4cpp::Priority::DEBUG << "Pushing additional detector"
                " selector \"" << detector_selection().first
                << "\" to track finding method instance(s)";
            mtd->additional_selection_criterion(detector_selection());
        } else {
            this->log() << log4cpp::Priority::DEBUG << "No own detector selector"
                " to notify track finding method instance(s)";
        }
    }
    /// Adds (selected) track score into associated method instance
    bool process_hit(EventID, HitKey, event::TrackScore &) override;

    ///\brief Creates track objects and populates it with scores for further
    ///       processing
    ///
    /// Generally, just forwards execution to method in use, invoking
    /// `iTrackPatternRecognitionMethod::reset()` for new events.
    ProcRes process_event(event::Event & e) override;

    void finalize() override;
};  // class MakeTracks

bool
MakeTracks::process_hit( EventID
                       , HitKey hk
                       , event::TrackScore & score
                       ) {
    assert(_mtd);
    if(!_trackCtrProxy) {
        _trackCtrProxy = _mtd->new_constructor(lmem());
        this->log() << log4cpp::Priority::DEBUG
            << "Instantiated track constructor proxy " << (void*) _trackCtrProxy.get() << ".";
    }
    _trackCtrProxy->add_score(_current_hit_iterator());
        if( log4cpp::Priority::DEBUG <= this->log().getPriority() ) {
        this->log() << log4cpp::Priority::DEBUG
            << "Score " << naming()[hk]
            << " considered by track ctr proxy " << (void*) _trackCtrProxy.get() << ".";
    }
    return true;
}

AbstractHandler::ProcRes
MakeTracks::process_event(event::Event & e) {
    // Iterate over track scores filling the method with hits of choosen
    // detectors (forwards to `process_hit()`)
    AbstractHitHandler<event::TrackScore>::process_event(e);
    // If track ctr was not created, no track score were found in an event
    if(!_trackCtrProxy) return kOk;
    // Populate event with tracks from the method
    for( auto trackP = _trackCtrProxy->get_track_candidate()
       ; trackP.second && (! trackP.second->scores.empty())
       ; trackP = _trackCtrProxy->get_track_candidate()
       ) {
        // Assure track ID is unique
        auto ir = e.tracks.insert( trackP );
        if(!ir.second) {
            NA64DP_RUNTIME_ERROR("Duplicated track ID: %#x", trackP.first.code);
        }
        assert(trackP.second);
        this->log() << log4cpp::Priority::DEBUG
            << "Track #" << (int) trackP.first.code
            << "(zones=" << (int) trackP.first.zones()
            << ", num=" << (int) trackP.first.number()
            << ") of " << trackP.second->scores.size() << " scores has been added to"
               " event.";
    }
    // we don't need this track ctr anymore
    this->log() << log4cpp::Priority::DEBUG
            << "Track constructor proxy " << (void*) _trackCtrProxy.get()
            << " has been reset (to be deleted).";
    _trackCtrProxy.reset();
    return kOk;
}

void
MakeTracks::finalize() {
    if(_mtd)
        _mtd->finalize();
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( MakeTracks
                , cmgr, cfg
                , "Creates tracks from track scores"
                ) {
    // Instantiate pattern recognition method
    std::shared_ptr<na64dp::geo::iTrackPatternRecognitionMethod> mtd;
    // NOTE: depending on usage context, `MakeTracks` may introduce
    // filtering/selection (zone number, `applyTo' selector, etc) to the
    // `iTrackPatternRecognitionMethod' object instantiated here, so this ctr
    // may be not a full initialization of created object.
    mtd.reset(
        na64dp::VCtr::self().make<na64dp::geo::iTrackPatternRecognitionMethod>(
                cfg["method"]["_type"].as<std::string>(),
                cmgr,
                cfg["method"]
            ) );
    // Create the track pattern recognition method based on config provided
    return new na64dp::handlers::MakeTracks( cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , mtd
            , log4cpp::Category::getInstance(cfg["_log"] ? cfg["_log"].as<std::string>() : "handlers.tracking")
            );
}

