#include "recognizePatterns.hh"

namespace na64dp {
namespace geo {

/**\brief Merges scores for eindividual planes
 *
 * A simples possible "track recognition method" that trivially merges any
 * multiple scores originating from certain plane into one score (or just puts
 * them like they are) into the single track. Can be useful as
 * utility base class building multiple scores or used as is for simplest
 * possible scheme of singular track fitting.
 *
 * Its single parameter `mergeHits` defines what to do with multiple scores
 * related to the same detector ID. If set, a new score will be created for
 * every unique detector and all the hits (clusters) will be put into that
 * new score. This new score is than used to build the track. If `mergeHits`
 * is not set, scores will be added into the track just as they are.
 *
 * \note For GenFit-based tracking, the `mergeHits` must NOT be
 *       set (corresponding handler relies on its own merging).
 * */
class SingleTrackHypothesis : public iTrackPatternRecognitionMethod {
private:
    /// Whether to merge hits into a single score (otherwise, one score will be
    /// created per hit)
    const bool _mergeHits;
public:
    class TrackConstructor : public iTrackConstructor {
    protected:
        /// Inherited from the owner
        const bool _mergeHits;
        /// Reference to the single track in use
        mem::Ref<event::Track> _cTrackPtr;
        /// Enumerates tracks within an event. Used if subsidiary (per-zone)
        /// method does not provide track ID.
        size_t _nTrackInEvent;
    public:
        TrackConstructor(LocalMemory & lmem_, bool mergeHits)
            : iTrackConstructor(lmem_)
            , _mergeHits(mergeHits)
            , _nTrackInEvent(0)
            {}
        /// Merges scores from single plane into one (new)
        void add_score(ScoreEntry) override;
        /// Returns track consisting of merged scores
        std::pair<TrackID, mem::Ref<event::Track>> get_track_candidate() override;
    };
public:
    SingleTrackHypothesis(bool mergeHits=false) : _mergeHits(mergeHits) {}
    /// Creates new track constructor proxy instance bound to this method
    std::shared_ptr<iTrackConstructor> new_constructor(LocalMemory &) override;
};


// Implementation
////////////////

std::shared_ptr<iTrackPatternRecognitionMethod::iTrackConstructor>
SingleTrackHypothesis::new_constructor(LocalMemory & lm) {
    return std::make_shared<TrackConstructor>(lm, _mergeHits);
}

void
SingleTrackHypothesis::TrackConstructor::add_score(ScoreEntry se_) {
    assert(se_->second);
    if(!_cTrackPtr) {
        _cTrackPtr = lmem.create<event::Track>(lmem);
        util::reset(*_cTrackPtr);
    }
    DetID did(se_->first);
    if(_mergeHits) {
        // This feature is not utilized for the GenFit-based tracking (the
        // GenFit2 handler relies on its own merging), yet it still may be
        // useful for some generic cases.
        auto it = _cTrackPtr->scores.find(did);
        if( _cTrackPtr->scores.end() == it ) {
            // We create new score to combine multiple
            mem::Ref<event::TrackScore> newScorePtr
                    = lmem.create<event::TrackScore>(lmem);
            util::reset(*newScorePtr);

            mem::Ref<event::ScoreFitInfo> newSFI
                    = lmem.create<event::ScoreFitInfo>(lmem);
            util::reset(newSFI);
            newSFI->score = newScorePtr;

            it = _cTrackPtr->scores.emplace(did, newSFI);
            *newScorePtr = *(se_->second);
        } else {
            // get ptr to dest score
            mem::Ref<event::TrackScore> mergedScorePtr
                = it->second->score;
            // merge given score with scores that have the same track ID
            for( auto & hitPair : se_->second->hitRefs ) {
                mergedScorePtr->hitRefs.insert(hitPair);
            }
        }
    } else {  // just puts scores into the track without making copies
        mem::Ref<event::ScoreFitInfo> newSFI
                    = lmem.create<event::ScoreFitInfo>(lmem);
        util::reset(*newSFI);
        newSFI->score = se_->second;
        _cTrackPtr->scores.emplace(se_->first, newSFI);
    }
}

std::pair<TrackID, mem::Ref<event::Track>>
SingleTrackHypothesis::TrackConstructor::get_track_candidate() {
    if(_cTrackPtr) {
        auto r = _cTrackPtr;
        _cTrackPtr.reset();
        return std::pair<TrackID, mem::Ref<event::Track>>(TrackID(0x0, ++_nTrackInEvent)
                    , r);
    }
    return std::pair<TrackID, mem::Ref<event::Track>>(TrackID(0x0, ++_nTrackInEvent)
                , _cTrackPtr);
}

NA64SW_REGISTER_TRACK_PATTERN_RECOGNITION_METHOD( SingleTrack, calibMgr, cfg,
        "Merges scores into one for each individual detector ID." ) {
    return new SingleTrackHypothesis( cfg["mergeHits"]
                ? cfg["mergeHits"].as<bool>()
                : false );
}

}  // namespace ::na64dp::geo
}  // namespace na64dp

