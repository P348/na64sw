#include "na64calib/manager.hh"
#include "na64util/observer.hh"
#include "na64util/numerical/biplaneIntersection.h"

#include "createScore.hh"

namespace na64dp {
namespace handlers {

size_t
TwoLayerScoreCombinationRule::_n_set(HitRef_t hr) const {
    assert(hr.first);
    DetID did(hr.second->first);
    assert(did.is_payload_set());
    auto p = did.payload_as<WireID>()->proj();
    if( p == WireID::kX || p == WireID::kU ) {
        return 0;  // X/U -- 1st
    }
    if( p == WireID::kY || p == WireID::kV ) {
        return 1;  // Y/V -- 2nd
    }
    NA64DP_RUNTIME_ERROR("Score detector \"%s\" has unsupported projection label"
            " -- unable to figure out tuplet's number of element."
            , this->calib::SetupGeometryCache::get()[did].c_str());
}

void
TwoLayerScoreCombinationRule::handle_single_placement_update(
        const calib::Placement & pl) {
    PlaneKey planeID(naming()[pl.name]);
    PlacementsCache pc;
    pc.t = util::transformation_by(pl);
    switch(pl.suppInfoType) {
    case calib::Placement::kRegularWiredPlane:
        pc.resolution = pl.suppInfo.regularPlane.resolution;
        break;
    //case calib::Placement::kIrregularWiredPlane:
    //    pc.resolution = pl.suppInfo.irregularPlane.  // TODO
    default:
        NA64DP_RUNTIME_ERROR("Can't get resolution for plane \"%s\""
                " (unsupported type)", pl.name.c_str());
    }
    this->_placementsCache.emplace(planeID, pc);
}

void
TwoLayerScoreCombinationRule::calc_score_values( event::TrackScore &
                                               , const std::array<HitRef_t, 2> & ) {
    throw std::runtime_error("TODO: combine two scores on layers");
}

#if 0
void
TwoLayerScoreCombinationRule::drs( Float_t * drsCoords
                                 , const std::array<HitRef_t, 2> & hrs
                                 ) {
    drsCoords[0] = hrs[0].second->second->lR[0];
    drsCoords[1] = hrs[1].second->second->lR[1];
}

void
TwoLayerScoreCombinationRule::mrs( Float_t * mrsXYZ
                                 , const std::array<HitRef_t, 2> & hrs
                                 ) {
    assert(mrsXYZ);
    assert(hrs[0].first);  // both hits are set
    assert(hrs[1].first);
    // retrieve placements info from cache
    auto it1 = _placementsCache.find(PlaneKey(hrs[0].second->first))
       , it2 = _placementsCache.find(PlaneKey(hrs[1].second->first))
       ;
    if( it1 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[0].second->first].c_str() );
    }
    if( it2 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[1].second->first].c_str() );
    }
    const PlacementsCache & c1 = it1->second
                        , & c2 = it2->second;
    // To combine hits from two scores we are looking for closest point of two
    // lines defined as:
    //  \vec{l}_x = \vec{v}_x * t_x + \vec{r}_{0x}
    //  \vec{l}_y = \vec{v}_y * t_y + \vec{r}_{0y}
    // Where
    //  \vec{r}_{0x} = \vec{c}_x + u_x*\vec{u}_x
    //  \vec{r}_{0y} = \vec{c}_y + u_y*\vec{u}_y
    util::Vec3 r01 = c1.t({{hrs[0].second->second->lR[0], 0, 0}})
             , r02 = c2.t({{hrs[1].second->second->lR[0], 0, 0}})
             ;
    //util::Vec3 r01 = c1.c + c1.u0 * (hrs[0].second->second->lR[0] - .5)
    //         , r02 = c2.c + c2.u0 * (hrs[1].second->second->lR[0] - .5)
    //         ;
    int rc = na64sw_projected_lines_intersection(
            c1.t.v().r, c2.t.v().r, NULL,
            r01.r, r02.r,
            mrsXYZ, NULL);
    if(rc) {
        NA64DP_RUNTIME_ERROR("Failed to conjugate hits of %s and %s:"
                " na64sw_projected_lines_intersection() return code %d"
                " (parallel hits?)"
                , naming()[hrs[0].second->first].c_str()
                , naming()[hrs[1].second->first].c_str()
                , rc
                );
    }
    if(_log.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        oss << "("  << naming()[hrs[0].second->first] << " at u="
            << hrs[0].second->second->lR[0] << " as "
            << c1.t.v() << "*t_1 + " << r01 << ") with ("
            << naming()[hrs[1].second->first] << " at u="
            << hrs[1].second->second->lR[0] << " as "
            << c2.t.v() << "*t_2 + " << r02 << ") into {"
            << mrsXYZ[0] << ", " << mrsXYZ[1] << ", " << mrsXYZ[2]
            << "}";
            //<< ", " << naming()[hrs[1].second->first]
        _log << log4cpp::Priority::DEBUG
             << "Conjugation rule " << (void*) this << " combined "
             << oss.str();
    }
}
#endif

namespace aux {

ScoreCreation::Mode ScoreCreation::from_str(const std::string & s) {
    if("perHit" == s)       return ScoreCreation::kPerHit;
    if("subScores" == s)    return ScoreCreation::kSubScores;
    if("singular" == s)     return ScoreCreation::kSingular;
    NA64DP_RUNTIME_ERROR("Bad score creation mode: \"%s\"."
            " Available: `perHit'/`subScores'/`singular'.", s.c_str());
}

}  // namespace ::na64dp::handlers::aux
}  // namespace ::na64dp::handlers::
}  // namespace na64dp

#if 0
REGISTER_HANDLER( CreateScore
                , mgr, cfg
                , "Creates track score corresponding to certain hit type"
                ) {
    // Get detector selection
    const auto sel = na64dp::aux::retrieve_det_selection(cfg);
    // Get the subject
    const std::string subj = cfg["subject"].as<std::string>();
    // Instantiate the handler
    if("APVCluster" == subj) {
        return new na64dp::handlers::CreateTrackScores<na64dp::event::APVCluster>(mgr, sel);
    } else if("StwTDCHit" == subj) {
        return new na64dp::handlers::CreateTrackScores<na64dp::event::StwTDCHit>(mgr, sel);
    } else if("F1Hit" == subj) {
        return new na64dp::handlers::CreateTrackScores<na64dp::event::F1Hit>(mgr, sel);
    }
    // else if() ...
    NA64DP_RUNTIME_ERROR( "Unknown \"subject\" provided to"
                          " GenFit_CreateMeasurement handler: \"%s\""
                        , subj.c_str()
                        );
}
#endif

REGISTER_HANDLER( CreateScore, cdsp, cfg
                , "From the combinations of the clusters/hits within a single"
                  " station produces track scores for fitting." ) {
    if( ! cfg["subject"] ) {
        NA64DP_RUNTIME_ERROR("Field \"subject\" is required for"
                " `ConjugateHits' handler.");
    }
    const std::string & subject = cfg["subject"].as<std::string>();
    const auto sel = na64dp::aux::retrieve_det_selection(cfg);
    const std::string detNamingCat = ::na64dp::aux::get_naming_class(cfg);
    const std::string & rule = cfg["combinationRule"]
                             ? cfg["combinationRule"].as<std::string>()
                             : "";
    const std::string createScores = cfg["mode"]
                                   ? cfg["mode"].as<std::string>()
                                   : "perHit";
    bool permitPartialCombinations = cfg["permitPartialCombinations"]
                                   ? cfg["permitPartialCombinations"].as<bool>()
                                   : true;

    if(createScores != "perHit" && rule.empty() ) {
        log4cpp::Category::getInstance("handlers.configuration")
            << log4cpp::Priority::WARN
            << "Score creation handler got `createScores'"
               " parameter set to \"" << createScores << "\", but it will not"
               " have an effect since no rule is used to combine hits or"
               " scores.";
    }
    auto createScoreMode
        = ::na64dp::handlers::aux::ScoreCreation::from_str(createScores);

    log4cpp::Category & logCat = ::na64dp::aux::get_logging_cat(cfg);

    if("apvClusters" == subject || "APVCluster" == subject) {
        if(rule.empty()) {
            return new na64dp::handlers::CreateTrackScores<na64dp::event::APVCluster>(
                    cdsp, sel, logCat, detNamingCat);
        } else if("doublets" == rule) {
            return new na64dp::handlers::ConjugateHits<na64dp::TwoLayerAPVCombinationRule>( cdsp
                    , sel
                    , detNamingCat
                    , logCat
                    , createScoreMode
                    , permitPartialCombinations
                    );
        } else {
            NA64DP_RUNTIME_ERROR("Combination rule \"%s\" is not supported"
                    " for subject \"%s\"; available rules are: doublets."
                    , rule.c_str(), subject.c_str() );
        }
    }
    if("stwHits" == subject || "StwTDCHit" == subject) {
        if(rule.empty()) {
            return new na64dp::handlers::CreateTrackScores<na64dp::event::StwTDCHit>(
                    cdsp, sel, logCat, detNamingCat);
        } else if("quadruplets" == rule) {
            return new na64dp::handlers::ConjugateHits<na64dp::FourLayerStrawCombinationRule>( cdsp
                    , sel
                    , detNamingCat
                    , logCat
                    , createScoreMode
                    , permitPartialCombinations
                    );
        } else {
            NA64DP_RUNTIME_ERROR("Combination rule \"%s\" is not supported"
                    " for subject \"%s\"; available rules are: quadruplets."
                    , rule.c_str(), subject.c_str() );
        }
    } else if("F1Hits" == subject) {
        if(!rule.empty()) {
            NA64DP_RUNTIME_ERROR("No combination rule \"%s\" is supported"
                    " for subject \"%s\" (only plain hit-to-score conversion"
                    " is foreseen)."
                    , rule.c_str(), subject.c_str() );
        }
        return new na64dp::handlers::CreateTrackScores<na64dp::event::F1Hit>(
                cdsp, sel, logCat, detNamingCat);
    } else if("trackScores" == subject || "TrackScore" == subject) {
        if("doublets" == rule) {
            return new na64dp::handlers::ConjugateHits<na64dp::handlers::TwoLayerScoreCombinationRule>( cdsp
                    , sel
                    , detNamingCat
                    , logCat
                    , createScoreMode
                    , permitPartialCombinations
                    );
        } /*else if("quadruplets" == rule) {
            return new na64dp::handlers::ConjugateHits<na64dp::FourLayerScoreCombinationRule>( cdsp
                    , sel
                    , detNamingCat
                    , logCat
                    , createScoreMode
                    );
        }*/ else {
            NA64DP_RUNTIME_ERROR("Combination rule \"%s\" is not supported"
                    " for subject \"%s\"; available rules are: doublets, quadruplets."
                    , rule.c_str(), subject.c_str() );
        }
    }
    // ... other subjects ?

    NA64DP_RUNTIME_ERROR("No class implements score creation for subject \"%s\"."
            , subject.c_str());
}

