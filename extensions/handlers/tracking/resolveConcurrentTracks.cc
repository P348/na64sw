#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handler {

/**\brief Discriminates tracks based on fitting results
 *
 * This handler performs removal of similar tracks.
 *
 * It is based on pairwise selection of *similar* tracks. Two rracks considered
 * *similar* according to some similarity function \f$s(t_1, t_2)\f$ with
 * respect to some threshold \f$s(t_1, t_2) > s_{0}\f$. After index of similar
 * tracks has been built (one can represent it as a linked graph), handler
 * delegates execution to one of the track selection algorithms.
 *
 * In the simplest case the similarity function is just a number of
 * common (shared) track scores and track selection algorithm is just a p-value
 * cut.
 * */
class ResolveConcurrentTracks : public AbstractHitHandler<event::Track> {
public:
    /// Type of the track entry considered by similarity function
    typedef decltype(event::Event::tracks)::iterator TrackEntry;
    /// Similarity function type
    typedef std::function<double(TrackEntry, TrackEntry)> SimilarityFunction;
    /// Quality function type
    typedef std::function<double(ResolveConcurrentTracks::TrackEntry)> QualityFunction;
    /// Similarity measure type
    typedef SimilarityFunction::result_type Similarity_t;
    /// Track quality measuer type
    typedef QualityFunction::result_type Quality_t;
    /// Type of track pair with its similarity measure cached
    struct TracksPair {
        Similarity_t similarity;
        Quality_t qA, qB;
        TrackEntry a, b;
    };
    /// Base type for third-party selection algorithm
    struct iConcurrentSelector {
        /// Shall return tracks to remove
        virtual std::vector<TrackEntry> execute( const std::vector<TracksPair> & ) = 0;
    };
protected:
    /// Temporary storage of tracks passing selection
    std::vector<TrackEntry> _entries;
    /// Similarity function in use
    SimilarityFunction _sf;
    /// Quality function in use
    QualityFunction _qf;
    /// Similarity value threshold
    Similarity_t _sThreshold;
    /// Cached track pairs to operate with
    std::vector<TracksPair> _pairs;
    /// Track selection algorithm
    std::shared_ptr<iConcurrentSelector> _concurrentSelectionPtr;
public:
    ResolveConcurrentTracks( const std::unordered_set<int> permittedPatterns
            , int permittedZones
            , bool invert
            , SimilarityFunction sf, Similarity_t sThr
            , QualityFunction qf
            , std::shared_ptr<iConcurrentSelector> cSelAlgo
            , log4cpp::Category & logCat
            )
        : AbstractHitHandler<event::Track>(permittedPatterns, permittedZones, invert, logCat)
        , _sf(sf), _qf(qf)
        , _sThreshold(sThr)
        , _concurrentSelectionPtr(cSelAlgo)
        {}
    /// Adds track to consideration
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Invokes `process_hit()` for tracks matching zone pattern and evaluates
    /// the competition, removing loosing tracks
    ProcRes process_event(event::Event &) override;
};  // class ResolveConcurrentTracks

bool
ResolveConcurrentTracks::process_hit(EventID, TrackID, event::Track &) {
    // calculate similarity
    for(const auto & it : _entries) {
        Similarity_t s = _sf(it, this->_cTrackIt);
        if(s > _sThreshold) {
            // we have to guarantee that qA >= qB
            Quality_t qA = _qf(it)
                    , qB = _qf(this->_cTrackIt)
                    ;
            if( qA >= qB ) {
                _pairs.push_back(TracksPair{s
                        , qA, qB
                        , it, this->_cTrackIt
                        });
            } else {
                _pairs.push_back(TracksPair{s
                        , qB, qA
                        , this->_cTrackIt, it
                        });
            }
        }
    }
    // put the track into collection
    _entries.push_back(this->_cTrackIt);
    return true;
}

AbstractHandler::ProcRes
ResolveConcurrentTracks::process_event(event::Event & e) {
    // let parent's function to collect the tracks
    AbstractHitHandler<event::Track>::process_event(e);
    // if no concurrent ("similar") tracks found, just keep going
    if(_pairs.empty()) {
        _entries.clear();
        return kOk;
    }
    std::vector<TrackEntry> tracksToRemove
        = _concurrentSelectionPtr->execute(_pairs);
    for(auto & it2remove: tracksToRemove) {
        e.tracks.erase(it2remove);
    }
    _entries.clear();
    _pairs.clear();
    return kOk;
}

/**\brief Graph-based implementation of track selection
 *
 * This selector class builds a directed weighted acyclic graph with following
 * feature:
 *
 *     - a node in a graph corresponds to a track;
 *     - two nodes considered connected with weight \f$s\f$;
 *     - every node maintains a collection of neighbours sorted by \f$S\f$ and
 *       its own internal \f$q\f$;
 *     - topological property of the graph is that its first-level nodes have
 *       highest \f$q\f$ while deepest one(s) are with lowest \f$q\f$.
 *
 * With this graph being built (within an `execute()` call) the following
 * procedure is performed:
 *
 *    1) First level node (with highest \f$q\f$) is considered;
 *    2) Its closest neighbour (one most similar to current one) is flagged and
 *       its closest neighbour is considered than as in 1) This procedure;
 *       continues till all the even nodes are flagged in current "most similar"
 *       branch (depth-first);
 *    3) Then procedure continues depth-first, starting from node that is not
 *       being flagged.
 *
 * ... TODO: finish doc, demo?
 *
 * \todo not implemented yet
 */
class TrackSelector : public ResolveConcurrentTracks::iConcurrentSelector {
public:
    /// Represetns a node of the similarity graph
    struct Node {
        /// Denotes that node is scheduled for removal
        bool remove;
        /// Reference to this track
        const ResolveConcurrentTracks::TrackEntry self;
        /// Similar tracks ordered by similarity to this one
        std::multimap< ResolveConcurrentTracks::Similarity_t
                     , Node *
                     > neighbours;
        Node(ResolveConcurrentTracks::TrackEntry self_)
            : remove(false), self(self_) {}
    };
private:
    /// Built graph
    std::list<Node> _g;  // graph
    // ^^^ we use list here since it does not invalidates iterators, albeit
    //     it can lead to memory fragmentation. We do not expect this graph
    //     to be large, though.
    /// Index of nodes in graph by quality
    std::multimap<ResolveConcurrentTracks::Quality_t, Node *> _nodesByQuality;
    /// Index of nodes in graph by track item
    std::unordered_map<event::Track *, Node *> _nodesByTrack;
protected:
    ///\brief Considers track item
    Node * _consider_item( ResolveConcurrentTracks::TrackEntry item
                         , ResolveConcurrentTracks::Quality_t q
                         ) {
        auto it = _nodesByTrack.find(item->second.get());
        Node * nodePtr;
        if(it == _nodesByTrack.end()) {
            // add new
            _g.push_back(item);
            Node * nodePtr = &(_g.back());
            _nodesByQuality.emplace(q, nodePtr);
            _nodesByTrack.emplace(item->second.get(), nodePtr);
        } else {
            return it->second;
        }
        return nodePtr;
    }
public:
    std::vector<ResolveConcurrentTracks::TrackEntry>
        execute( const std::vector<ResolveConcurrentTracks::TracksPair> & pairs ) override {
        std::vector<ResolveConcurrentTracks::TrackEntry> toRemove;
        // build a graph using pairwise similarities
        for(const auto & pair : pairs) {
            Node * nodeA = _consider_item(pair.a, pair.qA)
               , * nodeB = _consider_item(pair.b, pair.qB);
            assert(pair.qA >= pair.qB);
            nodeA->neighbours.emplace(pair.similarity, nodeB);
            throw std::runtime_error("TODO");  // ... TODO: process
        }
        return toRemove;
    }
};  // TrackSelector


}  // namespace ::na64dp::handler
}  // namespace na64dp

