#include "trackScoreIteratingHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Decomposes composite scores into simpler ones
 *
 * Track score can be a union of another, simpler scores -- for instance two
 * planes of a single station that measure X and Y can be combined
 * ("conjugated") to provide 2D score (e.g., see `ConjugateAPVHits`,
 * `ConjugateStrawHits` etc handlers).
 *
 * Having conjugated scores is useful for certain tasks, like track finding
 * where it is convenient to operate with spatial points, clusters of hits,
 * etc.
 *
 * This handler performs a reverse procedure by splitting composite scores
 * into simpler ones. It may be not quite useful per se, but within a track
 * it can help to provide more information for subsequent track fitting
 * procedure: for instance, elaborated track fitting procedure can prefer 1D
 * track scores over combined 2D hits if one of the projection planes is not
 * properly aligned. To run this handler *within a track* consider
 * subordinating it within `TrackScoreIteratingHandler`'s pipeline.
 *
 * \note Does not split spatial scores onto local ones; for that purpose
 *       see `BreakTrackScores`.
 *
 * \ingroup handlers tracking-handlers
 * */
class DecomposeScoresInTrack : public AbstractHitHandler<event::Track>
                             , public calib::Handle<nameutils::DetectorNaming> {
private:
    /// For debugging -- collection of scores having both scores and raw hits;
    /// filled during whole lifetime to print warning at `finalize()`.
    std::unordered_map<std::string, size_t> _mixedScores;
    /// Collection of composite scores within a track, per-track validity
    std::stack<decltype(event::Track::scores)::iterator> _compositeScores;

    /// If non-empty, contains detector selection expression
    const std::string _detSelectionStr;
    /// If not null, used to select certain detectors
    DetSelect * _detSelection;
protected:
    /// util method, collects scores for dissaciation
    void _collect_complex_scores(event::Track &);
    /// Updates detector selector if needed
    virtual void handle_update(const nameutils::DetectorNaming &) override;
public:
    /// Detectors naming shortcut
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::get(); }
    /// Processes a track
    bool process_hit(EventID, TrackID, event::Track &) override;
    /// Ctr, takes two selectors: for track and for detectors within a track
    DecomposeScoresInTrack( calib::Dispatcher & cdsp
                          , const std::string & trackSelection
                          , const std::string & detectorsSelection
                          , log4cpp::Category & logCat
                          );
};

DecomposeScoresInTrack::DecomposeScoresInTrack( calib::Dispatcher & cdsp
                          , const std::string & trackSelection
                          , const std::string & detectorsSelection
                          , log4cpp::Category & logCat
                          ) : AbstractHitHandler<event::Track>(cdsp, trackSelection, logCat)
                            , calib::Handle<nameutils::DetectorNaming>("default", cdsp)
                            , _detSelectionStr(detectorsSelection)
                            , _detSelection(nullptr)
                            {}

void
DecomposeScoresInTrack::handle_update(const nameutils::DetectorNaming & nm) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    if(_detSelectionStr.empty()) return;
    if(_detSelection) delete _detSelection;
    _detSelection = new DetSelect( _detSelectionStr.c_str()
                                 , util::gDetIDGetters
                                 , nm );
}

void
DecomposeScoresInTrack::_collect_complex_scores(event::Track & track) {
    for(auto scoreIt = track.scores.begin(); scoreIt != track.scores.end(); ++scoreIt ) {
        event::TrackScore & score = *scoreIt->second->score;
        // iterate over hit references within current score and check if there are
        // simpler track cores within
        bool hasSubScores = false
           , hasRawHits = false
           ;
        if( _detSelection && ! _detSelection->matches(DetID(scoreIt->first)) ) continue;
        for( const auto & scoreDatum : score.hitRefs ) {
            if( std::holds_alternative<mem::Ref<event::TrackScore>>(scoreDatum.second) ) {
                hasSubScores = true;
                continue;
            }
            hasRawHits = true;
            if(hasSubScores) {
                // score has mixed origin -- it holds both, some hit type and simpler
                // score. Memorize detector name to warn the user later on (perhaps,
                // it is better to exclude this detector) and omit this score.
                // NOTE: since this code may not be called if not-score entry is
                //       the first entry in hit, it will not provide complete list
                //       of mixed scores.
                auto it = _mixedScores.emplace(naming()[scoreIt->first], 0).first;
                ++(it->second);
            }
            break;
        }
        if(hasRawHits) continue;
        if(!hasSubScores) continue;  // empty score?
        // memorize score's iterator for further removal
        _compositeScores.push(scoreIt);
    }
}

bool
DecomposeScoresInTrack::process_hit( EventID eid
                                   , TrackID trackID
                                   , event::Track & track) {
    assert(_compositeScores.empty());
    // collect composite scores
    _collect_complex_scores(track);
    // dissacociate scores in the track
    std::vector<std::pair<DetID, mem::Ref<event::TrackScore>>> forInsertion;

    if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        bool isFirst = true;
        for(auto & te : track.scores) {
            oss << (isFirst ? "" : ", ") << naming()[te.first];
            isFirst = false;
        }
        _log << log4cpp::Priority::DEBUG
             << "Track scores before decomposition: "
             << oss.str() << ".";
    }

    while( !_compositeScores.empty() ) {
        auto it = _compositeScores.top();
        forInsertion.push_back(std::pair<DetID, mem::Ref<event::TrackScore>>(
                    it->first, it->second->score));
        track.scores.erase(it);
        _compositeScores.pop();
    };
    for(auto it = forInsertion.begin(); forInsertion.end() != it; ++it) {
        for(auto & scoreRefPair : it->second->hitRefs) {
            DetID did(scoreRefPair.first);
            auto scoreRef = std::get<mem::Ref<event::TrackScore>>(scoreRefPair.second);
            auto newSFIRef = lmem().create<event::ScoreFitInfo>(lmem());
            util::reset(*newSFIRef);
            newSFIRef->score = scoreRef;
            track.scores.emplace(did, newSFIRef);
        }
    }

    if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
        std::ostringstream oss;
        bool isFirst = true;
        for(auto & te: track.scores) {
            oss << (isFirst ? "" : ", ") << naming()[te.first];
            isFirst = false;
        }
        _log << log4cpp::Priority::DEBUG
             << "Track scores after decomposition: "
             << oss.str() << ".";
    }

    return true;
}

REGISTER_HANDLER( DecomposeScoresInTrack
                , cdsp, cfg, "Decomposes complex scores into simpler ones") {
    return new DecomposeScoresInTrack( cdsp
                                     , cfg["applyToTracks"]
                                            ? cfg["applyToTracs"].as<std::string>()
                                            : ""
                                     , cfg["applyToDetectors"]
                                            ? cfg["applyToDetectors"].as<std::string>()
                                            : ""
                                     , ::na64dp::aux::get_logging_cat(cfg)
                                     );
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

