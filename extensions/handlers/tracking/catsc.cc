
#if defined(catsc_FOUND) && catsc_FOUND

#include "catsc.hh"

namespace na64dp {
namespace geo {

#if 0
void
CATS::handle_update( const nameutils::DetectorNaming & nm ) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    if( _selector.first.empty() ) return;
    if( ! _selector.second ) {
        _selector.second = new DetSelect( _selector.first.c_str()
                                        , util::gDetIDGetters
                                        , nm );
    } else {
        _selector.second->reset_context( nm );
    }
}
#endif


CATS::CATS( CollectionStrategy collectionStrategy
          , iFilter * filterPtr
          , DetectorsOrder * orderPtr
          , cats_LayerNo_t minTrackLength
          , cats_LayerNo_t nMissingLayers
          , log4cpp::Category & logCat
          , size_t softLimitNCells
          , size_t softLimitNHits
          , size_t softLimitNRefs
          ) 
      : _log(logCat)
      , _detectorsOrder(orderPtr)
      , _filterPtr(filterPtr)
      , _catscSoftLimitNCells(softLimitNCells)
      , _catscSoftLimitNHits(softLimitNHits)
      , _catscSoftLimitRefs(softLimitNRefs)
      , _minLength(minTrackLength)
      , _nMissingLayers(nMissingLayers)
      , _tf(nullptr)
      , _collectionStrategy(collectionStrategy)
      , _evaluated(true)
      {
    _log << log4cpp::Priority::DEBUG
         << "CATS(C) handler constructed with soft limits:"
            " cells=" << softLimitNCells
         << ", hits=" << softLimitNHits
         << ", refs=" << softLimitNRefs
         ;
}

void
CATS::additional_selection_criterion(const std::pair<std::string, DetSelect *> & sel) {
    _log << log4cpp::Priority::DEBUG
         << "forwarding selector \"" << sel.first << "\" to order instance";
    _detectorsOrder->additional_selection_criterion(sel);
}

void
CATS::set_zone_numbers(const std::vector<ZoneID_t> & zoneNums) {
    _log << log4cpp::Priority::DEBUG
         << "forwarding " << zoneNums.size() << " zones IDs to order instance";
    _detectorsOrder->set_zone_numbers(zoneNums);
}

catsc::TrackFinder<CATS::Hit> &
CATS::_catsc_implem() {
    assert(_detectorsOrder);
    //auto orderedDets = _detectorsOrder->spatial_ordered_detectors();
    if( _tf && _detectorsOrder->n_sorted_groups() > _tf->n_layers() ) {
        delete _tf;
        _tf = nullptr;
    }
    if( !_tf ) {
        _tf = new catsc::TrackFinder<Hit>( _detectorsOrder->n_sorted_groups()
                , _catscSoftLimitNCells, _catscSoftLimitNHits
                , _catscSoftLimitRefs);
        _log << log4cpp::Priority::DEBUG << "CATS(C) instance constructed"
            " for " << _tf->n_layers() << " stations.";
    }
    assert(_tf);
    return *_tf;
}

void
CATS::_evaluate_and_collect() {
    if( !_tf ) {
        _log << log4cpp::Priority::DEBUG << "CATS(C) "
            " was not allocated; refusing to evaluate (no scores?).";
        return;
    }
    if(!(_evaluated = _tf->evaluate(_filter().as_catsc_filter(), _nMissingLayers))) {
        _log << log4cpp::Priority::DEBUG << "CATS(C)"
            " did not evaluated (#missed layers=" << _nMissingLayers << ")";
    } else {
        _log << log4cpp::Priority::DEBUG << "CATS(C)"
            " evaluated (#missed layers=" << _nMissingLayers << ")";
    }
    (_tf->*_collectionStrategy)(_collector, _minLength);
    if( _log.getPriority() >= log4cpp::Priority::DEBUG ) {
        _log << log4cpp::Priority::DEBUG << "Collected " << _collector.size()
             << " tracks from CATS(C) (min length=" << _minLength << ")";
        // ... TODO: details
    }
}

void
CATS::finalize() {
    // Print warning if there were layers (detectors) with no hits considered.
    if(!_detectorsOrder) return;
    std::ostringstream oss;
    auto m = _detectorsOrder->spatial_ordered_detectors();
    bool hasMissedDetectors = false;
    for(const auto & p : _scoresCounts) {
        if(0 == p.second) {
            hasMissedDetectors = true;
            DetID did(0x0);
            // do reverse lookup in map (this is not performance-critical
            // function, so we're fine with it)
            for(auto mappedItem : m) {
                if(mappedItem.second != p.first) continue;
                did = mappedItem.first;
                break;
            }
            assert(did.id);  // failed to find detector ID matching the layer number
            if(!did.id) {
                // failed to find detector ID;
                oss << " #" << (int) p.first << "?, ";
            } else {
                oss << " " << _detectorsOrder->naming()[did] << ", ";
            }
        }
    }
    if(!hasMissedDetectors) {  // all fine
        if(!_scoresCounts.empty()) {
            _log << log4cpp::Priority::DEBUG
                 << "All " << _scoresCounts.size() << " layers provided"
                 " scores -- CATS(C) detector topology was consistent.";
        } else {
            _log << log4cpp::Priority::WARN
                 << "CATS(C) scores counters are empty (no scores processed).";
        }
        return;  
    }
    _log << log4cpp::Priority::WARN
        << "Following detector(s) were not considered by CATS(C)"
        << oss.str() << "but appeared in topology (mismatching selection"
            " criteria?).";
}

std::shared_ptr<CATS::iTrackConstructor>
CATS::new_constructor(LocalMemory & lmem) {
    return std::make_shared<TrackComposer>(*this, lmem);
}

//
// Implementation of track constructor methods

CATS::TrackComposer::TrackComposer(CATS & cats, LocalMemory & lmem_)
            : iTrackConstructor(lmem_)
            , _cats(cats)
            , _currentNTrack(0)
            {
    _cats._log << log4cpp::Priority::DEBUG
               << "A (proxy) per-event track composer instance " << this
               << " created.";
}

CATS::TrackComposer::~TrackComposer() {
    _cats._log << log4cpp::Priority::DEBUG
               << "Track composer instance " << this << " destroyed.";
    _cats._reset();
}

void
CATS::TrackComposer::add_score(ScoreEntry se) {
    // TODO: log string can create performance issues here
    auto & nm = _cats._detectorsOrder->naming();
    if( ! _cats._detectorsOrder->det_selector().empty() ) {
        for( auto * ptr : _cats._detectorsOrder->det_selector() ) {
            if(!ptr->second->matches(se->first)) {
                // skip score if it does not match the selector (if given)
                _cats._log << log4cpp::Priority::DEBUG
                    << "Track composer " << this << " ignored score "
                    << nm[se->first] << " because of the selector";
                return;
            }
        }
    }
    // find the layer number by layer ID
    const auto orderMapping = _cats._detectorsOrder->spatial_ordered_detectors();
    auto layerIt = orderMapping.find(se->first);
    if( orderMapping.end() == layerIt ) {
        _cats._log << log4cpp::Priority::DEBUG
            << "Track composer " << this << " ignored score "
            << nm[se->first] << " the missed CATS layer number";
        return;
    }
    if( ! _cats._filter().can_handle_score(se) ) {
        _cats._log << log4cpp::Priority::DEBUG
            << "Track composer " << this << " ignored score "
            << nm[se->first] << " the filter can not handle this"
               " type of score";  // todo: details? 1D, 2D?
        return;
    }
    assert(se->second.get());  // score ref actually brought data
    _cats._catsc_implem().add(layerIt->second, {se});
    _cats._log << log4cpp::Priority::DEBUG
            << "Track composer " << this << " inserted score "
            << nm[se->first] << ".";  // todo: details? 1D, 2D?
    // incrment corresponding score counter
    auto scoreCountIR = _cats._scoresCounts.emplace(se->first, 0);
    ++(scoreCountIR.first->second);
}

std::pair<TrackID, mem::Ref<event::Track>>
CATS::TrackComposer::get_track_candidate() {
    if( !_cats.evaluated() ) {
        if( _cats._log.getPriority() >= log4cpp::Priority::DEBUG ) {
            std::ostringstream oss;
            for(cats_LayerNo_t i = 0; i < _cats._catsc_implem().n_layers(); ++i ) {
                oss << "#" << (int) i << ": " << _cats._catsc_implem().n_points(i)
                    << ", ";
            }
            _cats._log << log4cpp::Priority::DEBUG
                << "Evaluating CATS(C) with hits on layers: " << oss.str();
        }
        _cats._evaluate_and_collect();
        if(!_cats.evaluated()) {
            _cats._log << log4cpp::Priority::DEBUG << "CATS(C) failed to evaluate,"
                " proxy returns null track candidate.";
            return {TrackID(0x0, 0x0), nullptr};
        }
    }
    if( 0 == _cats._collector.size()
     || _cats._collector.size() == _currentNTrack ) return {TrackID(0x0, 0x0), nullptr};
    std::pair<TrackID, mem::Ref<event::Track>> p( TrackID(0x0, _currentNTrack + 1)
            , lmem.create<event::Track>(lmem)
            );
    _cats._log << log4cpp::Priority::DEBUG << "CATS(C) returns track #"
        << _currentNTrack + 1 << " of " << _cats._collector.size() << ".";
    // Fill track with hits from intermediate representation
    std::vector<CATS::Hit> & scores
            = _cats._collector.at(_currentNTrack);
    for( size_t nHit = 0; nHit < scores.size(); ++nHit ) {
        auto sfi = lmem.create<event::ScoreFitInfo>(lmem);
        util::reset(*sfi);
        sfi->score = scores[nHit]->second;
        p.second->scores.emplace(scores[nHit]->first, sfi);
    }
    ++_currentNTrack;
    return p;
}

void
CATS::_reset() {
    if(_tf) {
        _tf->reset();
        _collector.reset();
        _log << log4cpp::Priority::DEBUG << "CATS reset.";
    }
    _evaluated = false;
}

//
// Implementation of reentrant collector class methods

CATS::Collector::Collector() {}

CATS::Collector::~Collector()  {}

void
CATS::Collector::collect( const cats_HitData_t * hits_
                        , size_t nHits ) {
    // reallocate reentrant buffer if need
    std::vector<Hit> r;
    for(size_t i = 0; i < nHits; ++i) {
        r.push_back(*reinterpret_cast<const Hit *>(hits_[i]));
    }
    push_back(r);
}

void
CATS::Collector::reset() {
    std::vector<std::vector<Hit>>::clear();
}

// Uniform threshold

bool
UniformAngleThreshold::can_handle_score(CATS::Hit pl) const {
    return ! std::isnan(pl->second->gR[0]);
}


bool
UniformAngleThreshold::matches( const CATS::Hit & a_
                              , const CATS::Hit & b_
                              , const CATS::Hit & c_ ) const {
    assert(a_->second.get());
    assert(b_->second.get());
    assert(c_->second.get());
    const float * a = a_->second->gR
              , * b = b_->second->gR
              , * c = c_->second->gR;
    assert(a);
    assert(b);
    assert(c);
    assert(!std::isnan(a[0]));  // NOTE: this check should be removed in
    assert(!std::isnan(a[1]));  //       production; it is rather to warn
    assert(!std::isnan(a[2]));  //       user about filter is incompatible
    #if 1
    const Real_t angle = catsc_util_3p_angle(a, b, c);
    #else
    float p[3][3] = {
        { a[0], a[1], a[2] },
        { b[0], b[1], b[2] },
        { c[0], c[1], c[2] }
    };
    float scProduct = 0, mod1 = 0, mod2 = 0;
    // p1 = a - b, p2 = c - b
    // acos(p1.p2/sqrt(|p1^2||p2^2|))
    for( int i = 0; i < 3; ++i ) {
        p[0][i] -= p[1][i];
        mod1 += p[0][i]*p[0][i];
        p[2][i] -= p[1][i];
        mod2 += p[2][i]*p[2][i];

        scProduct += p[0][i]*p[2][i];
    }
    if( 0. == mod1 || 0. == mod2 || std::isnan(scProduct)
     || std::isnan(mod1)|| std::isnan(mod2) ) {
        return 0;
    }
    const float angle = acos(scProduct/sqrt(mod1*mod2));
    //std::cout << " xxx considered angle "
    //          << angle << " wrt " << threshold << " => " << (angle > threshold ? 1 : 0)
    //          << std::endl;  // XXX
    #endif
    return angle > threshold ? 1 : 0;
}

//                                                            ________________
// _________________________________________________________/ Base Multifilter

cats_Weight_t
iBaseMultifilter::weight_( const CATS::Hit & a_
                         , const CATS::Hit & b_
                         , const CATS::Hit & c_ ) const {
    auto & L = this->log();
    bool debugOutput = L.getPriority() >= log4cpp::Priority::DEBUG;
    assert(a_->second.get());
    assert(b_->second.get());
    assert(c_->second.get());
    const DetID aID = a_->first
              , bID = b_->first
              , cID = c_->first
              ;
    const event::TrackScore & a = *a_->second.get()
                          , & b = *b_->second.get()
                          , & c = *c_->second.get()
                          ;
    // Figure out configuration
    uint32_t flags = 0x0;  // set bit corresponds to point, unset -- to line
    // Rely on 3rd coordinate of the global score to define whether given
    // score defines a spatial point
    if(!std::isnan(a.gR[2])) flags |= 0x1;
    if(!std::isnan(b.gR[2])) flags |= 0x2;
    if(!std::isnan(c.gR[2])) flags |= 0x4;
    cats_Weight_t (iBaseMultifilter::*m)( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const = nullptr;
    // check vs flags set to decline prohibited cases
    cats_Weight_t w;
    if(flags == 0x0) {
        if( _flags & kDismiss_3l )     w = -1.;
        else if( _flags & kPass_3l )   w =  1.;
        else m = &iBaseMultifilter::_consider_3l;
    } else if(flags == 0x7) {
        if( _flags & kDismiss_3p )     w = -1.;
        else if( _flags & kPass_3p )   w =  1.;
        else m = &iBaseMultifilter::_consider_3p;
    } else if( (flags == 0x1 || flags == 0x2 || flags == 0x4) ) {
        if( _flags & kDismiss_1p2l )   w = -1.;
        else if( _flags & kPass_1p2l ) w =  1.;
        else m = &iBaseMultifilter::_consider_1p2l;
    } else if( flags == 0x6 || flags == 0x5 || flags == 0x3 ) {
        if( _flags & kDismiss_2p1l )   w = -1.;
        else if( _flags & kPass_2p1l ) w =  1.;
        else m = &iBaseMultifilter::_consider_2p1l;
    }
    if(m)
        w = (this->*m)(flags, aID, bID, cID, a, b, c);
    if(debugOutput) {
        L << log4cpp::Priority::DEBUG
          << "Considered triplet (" << aID.id << ", " << bID.id << ", " << cID.id << ")"
          << " of spatial configuration class #" << flags << "; obtained weight: "
          << w
          ;
    }
    return w;
}

//                                                 ___________________________
// _____________________________________________ / Isotropic Angle Multifilter

void
iIsotropicAngleMultifilter::_copy_score_val_point( HitCoordinateReal_t * dest
                                                , DetID did
                                                , const event::TrackScore & score) const {
    dest[0] = score.gR[0];
    dest[1] = score.gR[1];
    dest[2] = score.gR[2];
}

cats_Weight_t
iIsotropicAngleMultifilter::_check_3p( DetID aID, DetID bID, DetID cIC
                                    , const HitCoordinateReal_t * a
                                    , const HitCoordinateReal_t * b
                                    , const HitCoordinateReal_t * c
                                    ) const {
    return catsc_util_3p_angle(a, b, c)/M_PI;
}

cats_Weight_t
iIsotropicAngleMultifilter::_consider_3p( uint32_t flags
                                      , const DetID aID, const DetID bID, const DetID cID
                                      , const event::TrackScore & a
                                      , const event::TrackScore & b
                                      , const event::TrackScore & c
                                      ) const {
    HitCoordinateReal_t cs[3][6];
    assert(flags == 0x7);
    // Copy score values to consider algebraically
    _copy_score_val_point(cs[0], aID, a);
    _copy_score_val_point(cs[1], bID, b);
    _copy_score_val_point(cs[2], cID, c);
    return _check_3p( aID,   bID,   cID
                    , cs[0], cs[1], cs[2] );
}

cats_Weight_t
iIsotropicAngleMultifilter::_consider_2p1l( uint32_t flags
                                      , const DetID aID, const DetID bID, const DetID cID
                                      , const event::TrackScore & a
                                      , const event::TrackScore & b
                                      , const event::TrackScore & c
                                      ) const {
    HitCoordinateReal_t cs[3][6]
                      , third[6];
    // Copy score values to consider algebraically
    if(0x1 & flags) _copy_score_val_point(cs[0], aID, a);
    else            _copy_score_val_line( cs[0], aID, a);
    if(0x2 & flags) _copy_score_val_point(cs[1], bID, b);
    else            _copy_score_val_line( cs[1], bID, b);
    if(0x4 & flags) _copy_score_val_point(cs[2], cID, c);
    else            _copy_score_val_line( cs[2], cID, c);
    switch(flags) {
        case 0x3:
            catsc_util_2p1l_find_point( cs[0]  // p1
                                      , cs[1]  // p2
                                      , cs[2]  // l
                                      , third );
            return _check_3p( aID, bID, cID
                            , cs[0], cs[1], third );
        case 0x5:
            catsc_util_2p1l_find_point( cs[0]  // p1
                                      , cs[2]  // p2
                                      , cs[1]  // l
                                      , third );
            return _check_3p( aID, bID, cID
                            , cs[0], third, cs[1] );
        case 0x6:
            catsc_util_2p1l_find_point( cs[1]  // p1
                                      , cs[2]  // p2
                                      , cs[0]  // l
                                      , third );
            return _check_3p( aID, bID, cID
                            , third, cs[1], cs[0] );
        default:
            NA64DP_RUNTIME_ERROR("Bad flags for 2p1l case: %u", flags);
    }
}

cats_Weight_t
iIsotropicAngleMultifilter::_consider_1p2l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const {
    NA64DP_RUNTIME_ERROR("Forbidden method (1pl2 for IsotropicAngleMultifilter)");
}
 
cats_Weight_t
iIsotropicAngleMultifilter::_consider_3l( uint32_t
                                      , const DetID, const DetID, const DetID
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      , const event::TrackScore &
                                      ) const {
    NA64DP_RUNTIME_ERROR("Forbidden method (3l for IsotropicAngleMultifilter)");
}

// implementation with placements

void
IsotropicAngleMultifilter::_copy_score_val_line( HitCoordinateReal_t * dest
                                               , DetID did
                                               , const event::TrackScore & score) const {
    // derive and copy score vectors
    auto it = _pc.find(PlaneKey(did));
    if(_pc.end() == it) {
        NA64DP_RUNTIME_ERROR("No geometrical info on detector \"%s\""
                , naming()[did].c_str() );
    }
    //util::Vec3 r0 = it->second.c + it->second.u * (score.lR[0] - .5);
    util::Vec3 r0 = it->second({{score.lR[0], 0, 0}});
    // copy direction vector (\vec{l}) from l*t + r_0
    dest[0] = it->second.v().c.x;
    dest[1] = it->second.v().c.y;
    dest[2] = it->second.v().c.z;
    // copy measurement origin vector (\vec{l}) from l*t + r_0
    dest[3] = r0.c.x;
    dest[4] = r0.c.y;
    dest[5] = r0.c.z;
}

void
IsotropicAngleMultifilter::handle_single_placement_update(const calib::Placement & pl) {
    if(!(((int) calib::Placement::kDetectorsGeometry) & ((int) pl.suppInfoType))) return;
    // Note: valid for planar (regular and irregular wired) detectors
    PlaneKey planeID(naming()[pl.name]);
    _pc.emplace(planeID, util::transformation_by(pl));
}

// Virtual ctr

static uint32_t
_special_flags_for( const std::string & mode
                  , const std::vector<std::string> & strToks ) {
    if( mode != "dismiss" && mode != "pass" ) {
        NA64DP_RUNTIME_ERROR("Bad mode for special tokens: \"%s\", expected"
                " either \"dismiss\" or \"pass\".", mode.c_str());
    }
    uint32_t flags = 0x0;
    for(const std::string & tok : strToks) {
        if(tok == "3p")
            return "dismiss" == mode ? iBaseMultifilter::kDismiss_3p
                                     : iBaseMultifilter::kPass_3p
                                     ;
        if(tok == "2p1l" || tok == "1l2p")
            return "dismiss" == mode ? iBaseMultifilter::kDismiss_2p1l
                                     : iBaseMultifilter::kPass_2p1l
                                     ;
        if(tok == "1p2l" || tok == "2l1p")
            return "dismiss" == mode ? iBaseMultifilter::kDismiss_1p2l
                                     : iBaseMultifilter::kPass_1p2l
                                     ;
        if(tok == "3l")
            return "dismiss" == mode ? iBaseMultifilter::kDismiss_3l
                                     : iBaseMultifilter::kPass_3l
                                     ;
        NA64DP_RUNTIME_ERROR("Bad special case token: \"%s\". Allowed ones:"
                " 3p, 2p1l, 1p2l, 3l.", tok.c_str());
    }
    return flags;
}

uint32_t
_parse_special_flags_at_node( const YAML::Node & node
                            , uint32_t default_
                            ) {
    if(!(node["dismiss"] || node["pass"])) return default_;
    uint32_t flags = 0x0;
    if(node["dismiss"])
        flags |= _special_flags_for("dismiss"
                    , node["dismiss"].as<std::vector<std::string>>());
    if(node["pass"])
        flags |= _special_flags_for("pass"
                    , node["pass"].as<std::vector<std::string>>());
    return flags;
}

NA64SW_REGISTER_TRACK_PATTERN_RECOGNITION_METHOD( CATS
        , calibMgr, cfg
        , "Cellular automata-based algorithm to find track candidates."
        ) {
    // strategy
    const std::string & strategy = cfg["collectingStrategy"]
                                 ? cfg["collectingStrategy"].as<std::string>()
                                 : "strict"
                                 ;
    CATS::CollectionStrategy strategyMtdPtr = nullptr;
    if( strategy == "excessive" ) {
        strategyMtdPtr = &catsc::TrackFinder<CATS::Hit>::collect_excessive;
    } else if( strategy == "moderate" ) {
        strategyMtdPtr = &catsc::TrackFinder<CATS::Hit>::collect;
    } else if( strategy == "strict" ) {
        strategyMtdPtr = &catsc::TrackFinder<CATS::Hit>::collect_strict;
    } else if( strategy == "longest" ) {
        strategyMtdPtr = &catsc::TrackFinder<CATS::Hit>::collect_longest;
    } else if( strategy == "winning" ) {
        strategyMtdPtr = &catsc::TrackFinder<CATS::Hit>::collect_winning;
    } else {
        NA64DP_RUNTIME_ERROR( "Unsupported CATS track candidate collection"
                " strategy: \"%s\". Permitted options: excessive, moderate,"
                " strict, longest, winning.", strategy.c_str() );
    }
    // instantiate filter object
    if( !cfg["filter"] ) {
        NA64DP_RUNTIME_ERROR("CATS requires option: \"filter\".");
    }
    if( !cfg["filter"]["_type"] ) {
        NA64DP_RUNTIME_ERROR("CATS \"filter\" requires option: \"_type\".");
    }
    const std::string logCatStr = cfg["_log"] ? cfg["_log"].as<std::string>()
                                              : "tracking.trackFinding"
                                              ;
    const std::string detNamingCls = cfg["detNamingCls"]  ? cfg["detNamingCls"].as<std::string>()  : "default"
                    , placementsCls = cfg["placementsCls"] ? cfg["placementsCls"].as<std::string>() : "default"
                    ;
    CATS::iFilter * filterPtr = nullptr;
    {
        const std::string filterType = cfg["filter"]["_type"].as<std::string>();
        if("unweighted-points-angle-isotropic-threshold" == filterType) {
            // simple points-only filter
            const float angle = cfg["filter"]["angleThreshold"].as<float>();
            filterPtr = new UniformAngleThreshold(M_PI - angle);
        } else if( "unweighted-angle-isotropic" == filterType ) {
            // logic multifilter (accepts 1D) with angular threshold
            const float angle = cfg["filter"]["angleThreshold"].as<float>();
            uint32_t flags
                = _parse_special_flags_at_node( cfg["filter"]
                                              , iBaseMultifilter::kDismiss_1p2l
                                              | iBaseMultifilter::kDismiss_3l
                                              );
            filterPtr = new CATSC_LogicMultiFilter_implem<IsotropicAngleMultifilter>( angle
                    , calibMgr
                    , log4cpp::Category::getInstance(logCatStr + ".filter")
                    , flags
                    , detNamingCls
                    , placementsCls
                    );
        } else if( "weighted-angle-isotropic" == filterType ) {
            // weighted multifilter (accepts 1D) with angular threshold
            const float angle = cfg["filter"]["angleThreshold"].as<float>();
            uint32_t flags
                = _parse_special_flags_at_node( cfg["filter"]
                                              , iBaseMultifilter::kDismiss_1p2l
                                              | iBaseMultifilter::kDismiss_3l
                                              );
            filterPtr = new CATSC_WeightedMultiFilter_implem<IsotropicAngleMultifilter>( angle
                    , calibMgr
                    , log4cpp::Category::getInstance(logCatStr + ".filter")
                    , flags
                    , detNamingCls
                    , placementsCls
                    );
        } else {
            NA64DP_RUNTIME_ERROR("Unknown filter type: %s", filterType.c_str());
        }
    }
    assert(filterPtr);
    // get soft limits parameters, if given
    size_t softLimitNCells = 10000
         , softLimitNHits = 500
         , softLimitNRefs = 100;
    if(cfg["softLimits"]) {
        if(cfg["softLimits"]["nCells"])
            softLimitNCells = cfg["softLimits"]["nCells"].as<size_t>();
        if(cfg["softLimits"]["nHits"])
            softLimitNHits = cfg["softLimits"]["nHits"].as<size_t>();
        if(cfg["softLimits"]["nRefs"])
            softLimitNRefs = cfg["softLimits"]["nRefs"].as<size_t>();
    }
    int minTrackLength = cfg["minTrackLength"] ? cfg["minTrackLength"].as<int>() : 3;
    if(minTrackLength < 3) {
        NA64DP_RUNTIME_ERROR("CATS(C) is not intended to find tracks of length"
                " less than 3 hits.");
    }
    int nMissingLayers = cfg["missingLayers"] ? cfg["missingLayers"].as<int>() : 1;

    // TODO: make customizable order
    auto orderPtr = new calib::OrderedStations( calibMgr
            , log4cpp::Category::getInstance(logCatStr + ".detectorsOrder")
            , calib::OrderedStations::z_center
            , detNamingCls
            , placementsCls
            );  // TODO: maintain deletion
    // instantiate CATS wrapper
    return new CATS( strategyMtdPtr
                   , filterPtr
                   , orderPtr
                   , minTrackLength, nMissingLayers
                   , log4cpp::Category::getInstance(logCatStr)
                   , softLimitNCells, softLimitNHits, softLimitNRefs
                   );
}  // NA64SW_REGISTER_TRACK_PATTERN_RECOGNITION_METHOD()

}  // namespace ::na64dp::geo
}  // namespace na64dp

#endif  // defined(tricktrack_FOUND) && tricktrack_FOUND


