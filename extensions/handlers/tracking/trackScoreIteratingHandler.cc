#include "trackScoreIteratingHandler.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/track.hh"
#include "na64util/mm-layout.h"
#include "na64util/str-fmt.hh"
#include "na64util/vctr.hh"
#include <exception>

namespace na64dp {
namespace handlers {

bool
TrackScoreIteratingHandler::process_hit( EventID eid
                                       , TrackID trackID
                                       , event::Track & track
                                       ) {
    if( track.scores.empty() ) return true;
    // pre-set the handlers
    for( auto h : _subpipe ) {
        h->set_local_memory(lmem());
    }
    // Iterate over scores in track
    for( auto scoreEntry : track.scores ) {
        // Iterate over handlers, stop propagating score if score handler
        // returns `false`
        for( AbstractHitHandler<event::TrackScore> * handler : _subpipe ) {
            if( ! handler->process_hit(eid, scoreEntry.first, *scoreEntry.second->score) )
                break;
        }
    }
    for( auto h : _subpipe ) {
        h->reset_local_memory();
    }
    return true;
}

void
TrackScoreIteratingHandler::finalize() {
    for(auto hPtr : _subpipe) {
        hPtr->finalize();
    }
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( TrackScoreIteratingHandler, cdsp, cfg
                , " Processes track scores within an existing track.") {
    std::vector<na64dp::AbstractHitHandler<na64dp::event::TrackScore>*> subordinates;
    if((!cfg["pipeline"]) || !cfg["pipeline"].IsSequence()) {
        NA64DP_RUNTIME_ERROR("No node \"pipeline\" in handler or it is"
                " not a sequence.");
    }
    size_t nNode = 1;
    for( auto subCfg : cfg["pipeline"] ) {
        na64dp::AbstractHitHandler<na64dp::event::TrackScore> * hPtr = nullptr;
        std::string hName;
        try {
            hName = subCfg["_type"].as<std::string>();
            auto hPtr_ = na64dp::VCtr::self().make<na64dp::AbstractHandler>(
                    hName, cdsp, subCfg);
            hPtr = dynamic_cast<na64dp::AbstractHitHandler<na64dp::event::TrackScore>*>(hPtr_);
            if(!hPtr) {
                NA64DP_RUNTIME_ERROR("Handler \"%s\" does not process track scores."
                        , hName.c_str() );
            }
        } catch(const std::exception & e) {
            NA64DP_RUNTIME_ERROR("Error during instantiation subordinate"
                    " handler %zu: %s"
                    , nNode, hName.c_str() );
        }
        assert(hPtr);
        subordinates.push_back(hPtr);
        ++nNode;
    }
    return new na64dp::handlers::TrackScoreIteratingHandler( cdsp
            , na64dp::aux::retrieve_det_selection(cfg)
            , subordinates
            , ::na64dp::aux::get_logging_cat(cfg)
            );
}


