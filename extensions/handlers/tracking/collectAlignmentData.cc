#include "na64dp/abstractHitHandler.hh"
#include "na64calib/placements.hh"
#include "na64util/pair-hash.hh"

#include <fstream>
#include <iomanip>
#include <cmath>

#ifndef NA64SW_AL_ENTRY_START_MAGIC_BYTE
#   define NA64SW_AL_ENTRY_START_MAGIC_BYTE 0x7e
#endif

#ifndef NA64SW_AL_DICT_START_MAGIC_BYTE
#   define NA64SW_AL_DICT_START_MAGIC_BYTE 0xd
#endif

namespace na64dp {
namespace handlers {

/**\brief Produces binary file containing information relevant to alignment
 *
 * Provides extended set of track score getters to obtain local derivatives
 * and other specialized stuff usually needed by alignment routines. Produced
 * binary files are meant to be consumed then by a dedicated application and
 * do not depend on third party software to mimize dependencies.
 * */
class CollectAlignmentData : public AbstractHitHandler<event::Track>
                           , public calib::Handle<nameutils::DetectorNaming>
                           , public calib::Handle<calib::Placements>
                           {
public:
    /// Numeric encoding of the getter label
    ///
    /// Type must have eough lenth to enumerate all the getters defined for all
    /// the detectors
    typedef uint32_t LabelID_t;
    /// Floating point type for values get
    typedef event::StdFloat_t Value_t;
    /// Number of scores per track
    typedef uint16_t NScoresPerTrack_t;

    typedef Value_t (CollectAlignmentData::*MethodGetter) (
                    decltype(event::Track::scores)::const_iterator ) const;
    struct ScoreGetter {
        bool isOrdinary;
        union {
            event::Traits<event::ScoreFitInfo>::Getter ordinary_getter;
            MethodGetter extended_getter;
        } cllb;
        Value_t operator() ( decltype(event::Track::scores)::const_iterator scoreIt
                           , const CollectAlignmentData & self
                           ) const {
            if(isOrdinary) return static_cast<Value_t>(cllb.ordinary_getter(*(scoreIt->second)));
            return (self.*cllb.extended_getter)(scoreIt);
        }
    };

    /// Type of named getter for global or local derivative
    typedef std::pair<std::string, ScoreGetter> NamedGetter;
private:
    /// Gets initialized on initial file writing; denotes position in the
    /// preambule where dictionary offset should be written
    std::fstream::pos_type _indexOffsetPos;
    /// Output file
    std::ofstream _ofs;
    /// Getters used for every detector seen by a handler
    const std::vector<NamedGetter> _getters;
    /// Labels dictionary
    std::unordered_map< std::pair<DetID, std::string>
                      , LabelID_t
                      , util::PairHash
                      > _labels;
protected:
    /// Returns label for certain getter and detector ID
    ///
    /// Extens the `_labels` dictionary if need
    LabelID_t _label_for(DetID, const std::string &);
public:
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::operator*(); }

    CollectAlignmentData( calib::Manager & cmgr
                        , const std::string & sel
                        , const std::string & outFile 
                        , log4cpp::Category & logCat
                        , const std::list<NamedGetter> & getters
                        , const std::string & detNameClass="default"
                        , const std::string & placementsClass="default"
                        ) : AbstractHitHandler<event::Track>(cmgr, sel, logCat)
                          , calib::Handle<nameutils::DetectorNaming>(detNameClass, cmgr)
                          , calib::Handle<calib::Placements>(placementsClass, cmgr)
                          , _indexOffsetPos(0)
                          , _ofs(outFile, std::ios::out | std::ios::binary)
                          , _getters(getters.begin(), getters.end())
                          {
        // write api version
        const char apiVer[] = "na64sw-al-v0.1";
        _ofs.write(apiVer, sizeof(apiVer));
        // write type sizes
        uint8_t typeSize;
        #define M_write_type_size(tp) \
        typeSize = sizeof(tp); \
        _ofs.write(reinterpret_cast<char *>(&typeSize), sizeof(typeSize));
        M_write_type_size(size_t);  // ................ platform-specific `size_t' size
        M_write_type_size(na64sw_EventID_t); // ....... event ID type size
        M_write_type_size(TrackID_t);  // ............. track ID type size
        M_write_type_size(LabelID_t);  // ............. label ID type size
        M_write_type_size(Value_t);  // ............... value type size
        M_write_type_size(NScoresPerTrack_t);  // ..... number of scores per track type size
        #undef M_write_type_size
        if(_getters.size() > std::numeric_limits<unsigned short>::max()) {
            NA64DP_RUNTIME_ERROR("Number of getters specified for the handler"
                    " exceeds permittedl imit.");
        }
        // write placeholder for offset, memorize its position (updated in
        // `finalize()`)
        _indexOffsetPos = _ofs.tellp();
        size_t offs = 0x0;
        _ofs.write(reinterpret_cast<char*>(&offs), sizeof(offs));
        // write number of getters as two-byte int
        uint16_t nGetters = static_cast<uint16_t>(_getters.size());
        _ofs.write(reinterpret_cast<char *>(&nGetters), sizeof(nGetters));
        _log << log4cpp::Priority::DEBUG
             << "Score data header written.";
    }

    /// Appends output file with values extracted from track and its scores
    bool process_hit( EventID eid, TrackID tid, event::Track & track ) override;
    /// Appends file with labels dictionary
    void finalize() override;

    //
    // Advanced getters

    /// Automatically resolves score's resolution
    ///
    /// If score provides uncertainty, it will be used, otherwise tries to
    /// resolve using placement
    Value_t get_uncertainty_for(decltype(event::Track::scores)::const_iterator) const;
};  // class CollectAlignmentData

CollectAlignmentData::LabelID_t
CollectAlignmentData::_label_for(DetID detID, const std::string & getterName) {
    auto ir = _labels.emplace( std::pair<DetID, std::string>(detID, getterName)
                , _labels.size() );
    if(_labels.size() >= std::numeric_limits<LabelID_t>::max() ) {
        NA64DP_RUNTIME_ERROR("Labels limit reached, can't reserve more.");
    }
    return ir.first->second;
}

bool
CollectAlignmentData::process_hit( EventID eid
                                 , TrackID tid
                                 , event::Track & track
                                 ) {
    if(track.scores.empty()) return true;
    // write magic byte(s) to denote start of new entry
    char entryStartMagic = NA64SW_AL_ENTRY_START_MAGIC_BYTE;
    _ofs.write(reinterpret_cast<char*>(&entryStartMagic), sizeof(entryStartMagic));
    // write special header for new entry: event ID + track ID
    na64sw_EventID_t eidPlain = eid;
    _ofs.write(reinterpret_cast<char*>(&eidPlain), sizeof(eidPlain));
    _ofs.write(reinterpret_cast<char*>(&tid.code), sizeof(tid.code));
    // track's p-value, momentum and ndf
    // ... todo?
    // number of scores in the track (unsigned short)
    assert(track.scores.size() < std::numeric_limits<NScoresPerTrack_t>::max());
    NScoresPerTrack_t nScores = (NScoresPerTrack_t) track.scores.size();
    _ofs.write(reinterpret_cast<char*>(&nScores), sizeof(nScores));
    for( auto scoreIt = track.scores.begin(); track.scores.end() != scoreIt; ++scoreIt ) {
        for( const auto & getterEntry : _getters ) {
            LabelID_t label = _label_for(scoreIt->first, getterEntry.first);
            Value_t value = static_cast<Value_t>(getterEntry.second(scoreIt, *this));
            _ofs.write(reinterpret_cast<char*>(&label), sizeof(label));
            _ofs.write(reinterpret_cast<char*>(&value), sizeof(value));
        }
    }
    return true;
}

void
CollectAlignmentData::finalize() {
    // memorize current position
    std::fstream::pos_type dictOffset_ = _ofs.tellp();
    size_t dictOffset;
    assert( ((long unsigned int) dictOffset_)
         <= ((long unsigned int) std::numeric_limits<size_t>::max()));
    dictOffset = static_cast<size_t>(dictOffset_);
    // write magic byte(s) to denote start of labels block
    char labelsStartMagic = NA64SW_AL_DICT_START_MAGIC_BYTE;
    _ofs.write(reinterpret_cast<const char*>(&labelsStartMagic), sizeof(labelsStartMagic));
    // write number of labels
    size_t nLabels = _labels.size();
    _ofs.write(reinterpret_cast<char *>(&nLabels), sizeof(nLabels));
    for(const auto & labelEntry : _labels) {
        const std::string detName = naming()[labelEntry.first.first];
        _ofs.write(detName.c_str(), detName.size() + 1);
        _ofs.write(labelEntry.first.second.c_str(), labelEntry.first.second.size() + 1);
        LabelID_t thisLabel = labelEntry.second;
        _ofs.write(reinterpret_cast<char*>(&thisLabel), sizeof(thisLabel));
    }
    // get back to the position where dict offset is expected to be written
    // and write it
    _ofs.seekp(_indexOffsetPos, std::ios_base::beg);
    _ofs.write(reinterpret_cast<char *>(&dictOffset), sizeof(dictOffset));
    _ofs.close();
    _log << log4cpp::Priority::DEBUG
             << "Score data footer written.";
}

//
// custom ("extended") getters

static CollectAlignmentData::Value_t
_alignment_derivative_zero(const event::ScoreFitInfo &) {
    return 0.0;
}

static CollectAlignmentData::Value_t
_alignment_derivative_identity(const event::ScoreFitInfo &) {
    return 1.0;
}

CollectAlignmentData::Value_t
CollectAlignmentData::get_uncertainty_for(decltype(event::Track::scores)::const_iterator scoreIt) const {
    const auto & score = *(scoreIt->second->score);
    if(!std::isnan(score.lRErr[0])) {
        int n;
        Value_t r = score.lRErr[0]*score.lRErr[0];
        for(n = 1; n < 3; ++n) {
            if(std::isnan(score.lRErr[n])) break;
            r += score.lRErr[n]*score.lRErr[n];
        }
        return sqrt(r);
    }
    throw std::runtime_error("TODO: score did not provide its own uncertainty,"
            " other ways are to be implemented.");  // TODO
}

// ...

static const CollectAlignmentData::NamedGetter
gExtNamedGetters[] = {
    // These ones have specific meaning for residual derivatives that are not
    //   or identically changes with derivative variable; used a lot for
    //   simple positional alignment:
    { "zero",           {true, {_alignment_derivative_zero} } },
    { "identity",       {true, {_alignment_derivative_identity} } },
    // ...
    // Sigma relates to certain score; this is not actually a derivative
    //   itself, but is defined per score. Has to be obtained from score or
    //   placements
    { "uncertainty",    {false, { .extended_getter=&CollectAlignmentData::get_uncertainty_for}} }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( CollectAlignmentData
                , cmgr, cfg
                , "Produces dump for alignment routines" ) {
    auto & L = na64dp::aux::get_logging_cat(cfg);
    std::list<na64dp::handlers::CollectAlignmentData::NamedGetter> getters; {
        const auto strGettersList
            = cfg["quantities"].as<std::vector<std::string>>();
        for(const auto & name : strGettersList) {
            bool resolved = false;
            // try to resolve getter, first with current (overriden) getters
            for( size_t n = 0
               ; n < sizeof(na64dp::handlers::gExtNamedGetters)
                   / sizeof(*na64dp::handlers::gExtNamedGetters)
               ; ++n ) {
                if(name == na64dp::handlers::gExtNamedGetters[n].first) {
                    getters.push_back(na64dp::handlers::gExtNamedGetters[n]);
                    resolved = true;
                    L << log4cpp::Priority::DEBUG
                      << "Extended score getter \"" << name << "\" resolved.";
                    break;
                }
            }
            if(resolved) continue;
            // ...then from system ones (throw exception if not found)
            auto g = na64dp::util::value_getter<na64dp::event::ScoreFitInfo>(name);
            L << log4cpp::Priority::DEBUG
              << "Standard score getter \"" << name << "\" resolved.";
            getters.push_back(na64dp::handlers::CollectAlignmentData::NamedGetter(name, {true, {g}}));
        }
    }
    return new na64dp::handlers::CollectAlignmentData(cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["outputFile"].as<std::string>()
            , L
            , getters
            , na64dp::aux::get_naming_class(cfg)
            , na64dp::aux::get_placements_class(cfg)
            );
}

