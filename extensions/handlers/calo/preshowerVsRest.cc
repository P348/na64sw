#include "na64dp/abstractHitHandler.hh"
#include "na64detID/cellID.hh"

#include <TH2F.h>

namespace na64dp {
namespace handlers {

/** A correlation "preshower-vs-rest" energy deposition plot
 *
 * Usage:
 * \code{.yaml}
 *     - name PreshowerVsRestECAL
 *       # A base name for the histogram; str, required
 *       histogramName: ...
 *       # TH2D description string template; str, required
 *       histogramTitle: ...
 *       # Number of bins of preshower (vertical) axis; int, required
 *       preshowerNBins: ...
 *       # Range of preshower (vertical) axis; list of two floats, required
 *       preshowerRange: [..., ...]
 *       # Number of bins of main part (horizontal) axis; int, required
 *       restNBins: ...
 *       # Range of main part (horizontal) axis; list of two floats, required
 *       restRange: [..., ...]
 * \endcode
 *
 * Depicts sums of energy deposition in preshower vs rest part of ECAL.
 * Use selection to restrict cells for summation
 *
 * \todo Remove.
 * \note deprecated, to be removed in favour of generic plotting handler
 * \ingroup handlers calo-handlers
 * */
class PreshowerVsRestECAL : public AbstractHitHandler<event::SADCHit> {
private:
    size_t _nBinsPreshower;
    double _preshMin, _preshMax;
    size_t _nBinsRest;
    double _restMin, _restMax;
    double _preshowerSum, _restSum;
    const std::string _hstName, _hstTitle;

    TH2F * _histogram;
    DetID _ecalDID;
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    PreshowerVsRestECAL( calib::Dispatcher & cdsp
                       , const std::string & selection
                       , const std::string & hstName
                       , const std::string & hstTitle
                       , size_t nBinsPreshower, double preshMin, double preshMax
                       , size_t nBinsRest, double restMin, double restMax
                       );
    ~PreshowerVsRestECAL();
    /// Fills the histogram
    virtual ProcRes process_event( event::Event & ) override;
    /// Increases the sums
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & hit ) override;
    /// Assures the writinh
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

using event::SADCHit;

PreshowerVsRestECAL::PreshowerVsRestECAL( calib::Dispatcher & cdsp
                                        , const std::string & selection
                                        , const std::string & hstName
                                        , const std::string & hstTitle
                                        , size_t nBinsPreshower, double preshMin, double preshMax
                                        , size_t nBinsRest, double restMin, double restMax
                                        )
                    : AbstractHitHandler<SADCHit>(cdsp, selection)
                    , _nBinsPreshower(nBinsPreshower), _preshMin(preshMin), _preshMax(preshMax)
                    , _nBinsRest(nBinsRest), _restMin(restMin), _restMax(restMax)
                    , _hstName(hstName), _hstTitle(hstTitle)
                    , _histogram(nullptr)
                    {}

void
PreshowerVsRestECAL::handle_update( const nameutils::DetectorNaming & nm ) {
    _ecalDID = nm["ECAL"];
}

AbstractHandler::ProcRes
PreshowerVsRestECAL::process_event(event::Event & e) {
    _preshowerSum = _restSum = 0.;
    auto res = AbstractHitHandler<SADCHit>::process_event(e);
    if( !_histogram ) {
        _histogram = new TH2F( _hstName.c_str()
                             , _hstTitle.c_str()
                             , _nBinsRest, _restMin, _restMax
                             , _nBinsPreshower, _preshMin, _preshMax
                             );
    }
    _histogram->Fill( _restSum, _preshowerSum );
    return res;
}

bool
PreshowerVsRestECAL::process_hit( EventID
                                , DetID did
                                , SADCHit & currentHit) {
    CellID cid(did.payload());
    if( DetID(did).chip() != _ecalDID.chip() || DetID(did).kin() != _ecalDID.kin() )
        return true;
    assert( 0 == cid.get_z()|| 1 == cid.get_z());
    if( 0 == cid.get_z() ) {
        _preshowerSum += currentHit.eDep;
    } else {
        _restSum += currentHit.eDep;
    }
    return true;
}

void
PreshowerVsRestECAL::finalize() {
    if( _histogram ) {
        _histogram->Write();
    }
}

PreshowerVsRestECAL::~PreshowerVsRestECAL() {
    if( _histogram ) {
        delete _histogram;
    }
}

}

REGISTER_HANDLER( PreshowerVsRestECAL, ch, yamlNode
                , "energy sum in preshower vs rest part of ECAL" ) {
    return new handlers::PreshowerVsRestECAL( ch
                           , aux::retrieve_det_selection(yamlNode)
                           , yamlNode["histogramName"].as<std::string>()
                           , yamlNode["histogramTitle"].as<std::string>()
                           , yamlNode["preshowerNBins"].as<int>()
                           , yamlNode["preshowerRange"][0].as<float>()
                           , yamlNode["preshowerRange"][1].as<float>()
                           , yamlNode["restNBins"].as<int>()
                           , yamlNode["restRange"][0].as<float>()
                           , yamlNode["restRange"][1].as<float>()
                           );
}

}

