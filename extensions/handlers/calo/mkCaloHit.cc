#include "na64dp/abstractHitHandler.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

/**\brief Creates a `CaloHit` item in an event, based on per-cells hits.
 *
 * Builds up entire calorimeter hit as a whole, based on enrgy deposition in
 * particular cells. Depending on configuration, may create ECAL, HCAL, BGO,
 * SRD etc. Adds hits (instances of `CaloHit` structure) to an event.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: MakeCaloHit
 *       # optional, hit selection expression
 *       applyTo: null
 *       # list of detector names to apply; list of strings, required
 *       detectors: [..., ...]
 * \endcode
 *
 * Detectors that supposed to be the subject of hits creation should be
 * provided as their names, like "HCAL" (for entire HCAL) or "HCAL3" (for HCAL
 * module #3), "ECAL", "ECAL0", "ECAL1", etc.
 *
 * Resulting hits will be imposed into main event's `caloHits` map.
 *
 * \ingroup handlers calo-handlers
 * */
class MakeCaloHit : public AbstractHitHandler<event::SADCHit> {
private:
    /// Collection of tags
    std::set<std::string> _tags;
protected:
    /// A collection of hits to be imposed once event is done.
    std::unordered_map< DetID, mem::Ref<event::CaloHit> > _collected;

    /// Re-caches the calorimeter kins
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    /// Creates new instance parameterised with calorimeter station selection
    /// to apply the sums
    MakeCaloHit( calib::Dispatcher &  ch
               , const std::string & selection
               , const std::set<std::string> & tags_
               ) : AbstractHitHandler<event::SADCHit>(ch, selection)
                 , _tags(tags_) {}

    /// Re-sets the computed sums at each the event
    virtual ProcRes process_event(event::Event & event) override;

    /// Modifies the hit accordingly
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & currentHit ) override;
};

//                          * * *   * * *   * * *

void
MakeCaloHit::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<event::SADCHit>::handle_update(nm);  // fwd to parent
    // re-cache the sums
    _collected.clear();
    for( auto tag : _tags ) {
        _collected[nm[tag]] = nullptr;
    }
}

bool
MakeCaloHit::process_hit( EventID
                        , DetID detID
                        , event::SADCHit & hit ) {
    for( auto & p : _collected ) {
        if( ! p.first.matches(detID) ) continue;
        // add energy deposition to the tagged collection
        if( !p.second ) {
            p.second = lmem().create<event::CaloHit>(lmem());
            util::reset(*p.second);
            p.second->eDep = 0.;
            //std::cout << " xxx created " << naming()[p.first] << std::endl;  // XXX
        }
        if ( !std::isnan(hit.eDep ) ) {
			p.second->eDep += hit.eDep;
            //std::cout << " xxx " << naming()[detID]
            //          << " added to \"" << naming()[p.first]
            //          << "\""
            //          << std::endl;  // XXX
		}
    }
    return true;
}

AbstractHandler::ProcRes
MakeCaloHit::process_event(event::Event & event) {
    auto r = AbstractHitHandler<event::SADCHit>::process_event(event);
    // create calo hits and reset cached sums
    for( auto & p : _collected ) {
        if( ! p.second ) continue;  // omit zeroes
        auto ir = event.caloHits.emplace(p.first, nullptr);
        //if( ! ir.second ) {
        //    NA64DP_RUNTIME_ERROR( "Failed to impose an instance of CaloHit"
        //            " structure for \"%s\" detector"
        //            , naming()[p.first].c_str() );
        //}
        // reset collected for further usage
        p.second.swap(ir->second);
    }
    return r;
}

}

REGISTER_HANDLER( MakeCaloHit, ch, yamlNode
                , "Sums up energy deposition from individual stations to calorimeter energy." ) {
    auto tags = yamlNode["detectors"].as<std::vector<std::string> >();
    return new handlers::MakeCaloHit( ch
                , aux::retrieve_det_selection(yamlNode)
                , std::set<std::string>(tags.begin(), tags.end())
                //, yamlNode["nullifyNegative"] ? yamlNode["nullifyNegative"].as<bool>() : false
                //, yamlNode["errorOnMissed"] ? yamlNode["errorOnMissed"].as<bool>() : true
                );
}

}


