#include "na64dp/abstractHitHandler.hh"
#include <TH2F.h>

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>

namespace na64dp {
namespace handlers {

/**\brief Plots energy deposition distribution of ECAL vs HCAL as ROOT:TH2F
 *
 * Events without no hits in ECAL or HCAL won't be considered.
 *
 * \note deprecated handler, to be removed; use generic `BiHitHistogram_Calo_Calo` instead
 * \todo remove in favour of `BiHitHistogram_Calo_Calo`
 * \ingroup handlers calo-handlers
 * */
class ECALvsHCAL : public AbstractHitHandler<event::CaloHit> {
private:
    /// Histogram instance
    TH2F * hstBiPlot;
    /// Energy deposition in each of the calorimeters, re-set on each new event
    double hcalEDep, ecalEDep;
public:
    /// Constructs histogram
    ECALvsHCAL( calib::Dispatcher & cdsp
                  , const std::string & only
                  , const std::string & hstBaseName
                  , const std::string & hstDescription
                  , Int_t ecalNBins, Double_t ecalMin, Double_t ecalMax
                  , Int_t hcalNBins, Double_t hcalMin, Double_t hcalMax
                  , const std::string & overridePath=""
                  );
    virtual ProcRes process_event(Event &) override;
    virtual bool process_hit( EventID, DetID, HitT &) override;
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

ECALvsHCAL::ECALvsHCAL() : AbstractHandler() {
    eDep = new TH2F( "HCAL vs ECAL" ,"eDep"
                   , 100, 0, 150
                   , 100, 0, 150
                   );
}

ECALvsHCAL::ProcRes
ECALvsHCAL::process_event(Event & ev) {
    double hcal = ev.hcalEdep;
    double ecal = ev.ecalEdep;
    eDep->Fill( ecal, hcal );    
    return kOk;
}

void
ECALvsHCAL::finalize(){
    hstBiPlot->Write();
}

REGISTER_HANDLER( ECALvsHCAL, ch, cfg
                , "Handler to plot Hcal vs Ecal energy deposition" ) {    
    return new HcalEcal( ch, aux::retrieve_det_selection(cfg) );
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp
