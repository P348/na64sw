#include "na64dp/abstractHitHandler.hh"

#include <TH2F.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * \todo remove in favour of `BiHitHistogram_Calo_Calo`
 * \note deprecated handler, to be removed; use generic `BiHitHistogram_Calo_Calo` instead
 * \todo remove in favour of `BiHitHistogram_Calo_Calo`
 * \ingroup handlers calo-handlers
 * */
class HcalEcal : public AbstractHitHandler<event::CaloHit> {
private:
    /// Momentum resolution
    TH2F *eDep;
public:
    HcalEcal( calib::Dispatcher & dsp
            , const std::string & only );
    
    virtual ProcRes process_event(Event & ) override;
    
    virtual bool process_hit( EventID
                            , DetID_t
                            , event::CaloHit & hit ) override {assert(false);}                            
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

HcalEcal::HcalEcal( calib::Dispatcher & cdsp
                              , const std::string & only
                              )
        : AbstractHitHandler<event::CaloHit>(cdsp, only) {
    
    eDep = new TH2F("HCAL vs ECAL" ,"eDep",100, 0, 200, 100, 0, 200);
    
}

HcalEcal::ProcRes
HcalEcal::process_event(Event * evPtr) {
    
    
    double & hcal = evPtr->hcalEdep;
    double & ecal = evPtr->ecalEdep;
		
	eDep->Fill ( ecal, hcal );

    
    return kOk;
}

void
HcalEcal::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
    c1->cd(1);
    eDep->Draw();   
    
}

REGISTER_HANDLER( HcalEcal, banks, ch, cfg
                , "Handler to plot Hcal vs Ecal energy deposition" ) {
    
    return new HcalEcal( ch
                             , aux::retrieve_det_selection(cfg)
                             );
}
}
}
