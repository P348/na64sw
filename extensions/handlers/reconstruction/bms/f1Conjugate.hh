#pragma once

/**\file
 * \brief Declares F1 hit-to-score conversion classes
 *
 * This header defines specialization of `TrackScoreTraits` for 1D hit
 * provided b F1-based BMS detectors.
 * */

#include "tracking/createScore.hh"
#include "na64calib/placements.hh"
#include "na64dp/DSuLConjugationRule.hh"

namespace na64dp {
namespace aux {
template<typename HitT> struct TrackScoreTraits;  // fwd (undefined)

/**\brief Converts BMS hit into score
 *
 * This traits relies on data provided by `BMSLayout` interface implementation
 * to produce track score positions.
 *
 * \todo remove?
 * */
template<>
struct TrackScoreTraits<event::F1Hit> {
    /// Placement cache subject: a plane + standard resolution
    struct PlaneEntry {
        enum TypeCode { kRegularStride, kIrregularStride } typeCode;
        /// String name for det
        std::string detName;
        /// Wire length
        float wl;
        /// Wire rotation angles
        float angles[3];
        /// Plane's center
        TVector3 o;
        struct RegularStride {
                /// Stride (aka step, pitch) -- distance between tubes.
                float stride;
                /// Number of tubes in the plane
                int nWires;
        };
        struct IrregularStride {
            const float * wireMapPtr;
            int nWires;
        };
        union {
            RegularStride regular;
            IrregularStride irregular;
        } pl;
    };
    /// A conversion cache type -- plane description excerpt per plane ID
    typedef std::unordered_map<PlaneKey, PlaneEntry> Cache;
    /// Converts placement into cache entry and adds an entry into general cache
    ///
    /// We only interested here in Y size of the plane as it is input for 
    static void recache_placement( Cache & cache
                                 , PlaneKey pk
                                 , const calib::Placement & placement
                                 );

    /// Copies/converts F1 hit into track score
    ///
    /// Position calculus for BMS is rather tricky and depends on particular
    /// station. Fortunately, order/positions do not seem to change much.
    static std::pair<DetID_t, mem::Ref<event::TrackScore> >
    create( LocalMemory & lmem
          , mem::Ref<event::F1Hit> c
          , DetID did
          , Cache & cache
          );
};

}  // namespace ::na64dp::aux
}  // namespace na64dp

