#include "bmsLayout.hh"
#include "na64util/str-fmt.hh"

#include <cassert>

namespace na64dp {
namespace util {

/* Code below was inherited from COMPASS CORAL BMS reconstruction package.
 *
 * Originally written by Pawel Sznajder in 20010-2013. References:
 *  - Discussion on COMPASS AM on the Z misalignment and layout patterns
 *    of BMS by P.Sznajder: https://wwwcompass.cern.ch/compass/software/analysis/transparencies/2013/am_130613/sznajder_130613.pdf
 *  - BMS reconstruction from COMPASS software used for code snippets below:
 *    https://gitlab.cern.ch/P348/p348-daq/-/blob/master/coral/src/beam/rec_method_1/CsBMSReconstruction.cc
 */

static float
_BMS_mappings_1( int chn ) {
    const int ll = chn + 1;
    if (ll<=4)
        return -87.5+5.*(ll-1);
    else if((ll>4)&&(ll<=60))
        return -67.5+5.*((ll-5)/2);
    else
        return  72.5+5.*(ll-61);
}

static float
_BMS_mappings_2( int chn ) {
    const int ll = chn + 1;
    if(ll<=8)
        return -42.5+5.*((ll-1)/2);
    else if((ll>8 )&&(ll<=20))
        return -22.5+5.*((ll-9)/4);
    else if((ll>20)&&(ll<=44))
        return  -7.5+5.*((ll-21)/6);
    else if((ll>44)&&(ll<=56))
        return 12.5+5.*((ll-45)/4);
    else
        return 27.5+5.*((ll-57)/2);
}

static float
_BMS_mappings_3( int chn ) {
    const int ll = chn + 1;
    if(ll<=12)
        return -47.5+5.*((ll-1)/2);
    else if((ll>12)&&(ll<=20))
        return -17.5+5.*((ll-13)/4);
    else if((ll>20)&&(ll<=44))
        return  -7.5+5.*((ll-21)/6);
    else if((ll>44)&&(ll<=54))
        return  12.5+5.*((ll-45)/4);
    else
        return  27.5+5.*((ll-55)/2);
}

static float
_BMS_mappings_4( int chn ) {
    const int ll = chn + 1;
    if(ll<=14)
        return -112.5+5.*(ll-1);
    else if((ll>14)&&(ll<=50))
        return -42.5+5.*((ll-15)/2);
    else
        return  47.5+5.*(ll-51);
}

static const float _gZCorr[64][4] = {
      {	   -50.0,	   -90.0,	   110.0,	   -50.0	},
      {	   -10.0,	    90.0,	  -110.0,	    90.0	},
      {	    50.0,	   -50.0,	   -90.0,	    30.0	},
      {	    10.0,	    50.0,	    90.0,	    10.0	},
      {	    30.0,	   -10.0,	   -50.0,	   110.0	},
      {	   -30.0,	    10.0,	    50.0,	   -70.0	},
      {	    70.0,	    30.0,	   -10.0,	   -90.0	},
      {	   -70.0,	   -30.0,	    10.0,	    50.0	},
      {	   110.0,	    70.0,	    30.0,	   -10.0	},
      {	  -110.0,	   -50.0,	   -30.0,	   -30.0	},
      {	   -90.0,	    50.0,	    70.0,	    70.0	},
      {	    90.0,	   -70.0,	   -70.0,	  -110.0	},
      {	   -50.0,	   110.0,	   -90.0,	   -90.0	},
      {	    50.0,	   -10.0,	   -10.0,	    50.0	},
      {	   -10.0,	    10.0,	    10.0,	   -10.0	},
      {	    10.0,	  -110.0,	    90.0,	    10.0	},
      {	    30.0,	   -50.0,	   -50.0,	    30.0	},
      {	   -30.0,	    30.0,	    30.0,	   -30.0	},
      {	    70.0,	   -30.0,	   -30.0,	    70.0	},
      {	   -70.0,	    50.0,	    50.0,	   -70.0	},
      {	   110.0,	   -10.0,	   -10.0,	   110.0	},
      {	  -110.0,	    70.0,	    70.0,	  -110.0	},
      {	   -90.0,	   -90.0,	   -90.0,	   -90.0	},
      {	    90.0,	    90.0,	    90.0,	    90.0	},
      {	   -50.0,	   -70.0,	   -70.0,	   -50.0	},
      {	    50.0,	    10.0,	    10.0,	    50.0	},
      {	   -10.0,	    30.0,	    30.0,	   -10.0	},
      {	    10.0,	   110.0,	   110.0,	    10.0	},
      {	    30.0,	   -50.0,	   -50.0,	    30.0	},
      {    -30.0,	    50.0,	    50.0,	   -30.0	},
      {    110.0,	  -110.0,	  -110.0,	   110.0	},
      {	  -110.0,	   -30.0,	   -30.0,	  -110.0	},
      {	    70.0,	   -10.0,	   -10.0,	    70.0	},
      {	   -70.0,	    70.0,	    70.0,	   -70.0	},
      {	    30.0,	   -90.0,	   -90.0,	    30.0	},
      {	   -30.0,	    90.0,	    90.0,	   -30.0	},
      {	   -10.0,	   -70.0,	   -70.0,	   -10.0	},
      {	    10.0,	    10.0,	    10.0,	    10.0	},
      {	   -50.0,	   -50.0,	   -50.0,	   -50.0	},
      {	    50.0,	    30.0,	    30.0,	    50.0	},
      {	   -90.0,	   110.0,	   110.0,	   -90.0	},
      {	    90.0,	  -110.0,	  -110.0,	    90.0	},
      {	   110.0,	   -30.0,	   -30.0,	   110.0	},
      {	  -110.0,	    50.0,	    50.0,	  -110.0	},
      {	    70.0,	   -90.0,	   -90.0,	    70.0	},
      {	   -70.0,	    70.0,	    70.0,	   -70.0	},
      {	    30.0,	   -70.0,	   -70.0,	    30.0	},
      {	   -30.0,	    90.0,	    90.0,	   -30.0	},
      {	   -10.0,	   110.0,	   -10.0,	   -10.0	},
      {	    10.0,	    30.0,	    30.0,	    10.0	},
      {	   -50.0,	   -30.0,	   -30.0,	    50.0	},
      {	    50.0,	  -110.0,	    10.0,	   -90.0	},
      {	   -90.0,	    70.0,	    70.0,	  -110.0	},
      {	    90.0,	   -10.0,	   -70.0,	    70.0	},
      {	   110.0,	    10.0,	    30.0,	   -30.0	},
      {	  -110.0,	   -70.0,	   -30.0,	   -10.0	},
      {	    70.0,	    30.0,	   -10.0,	    50.0	},
      {	   -70.0,	   -30.0,	    10.0,	   -90.0	},
      {	    30.0,	   -10.0,	   -50.0,	   -70.0	},
      {	   -30.0,	    10.0,	    50.0,	   110.0	},
      {	    10.0,	   -50.0,	   -90.0,	    10.0	},
      {	    50.0,	    50.0,	    90.0,	    30.0	},
      {	   -10.0,	   -90.0,	   110.0,	    90.0	},
      {	   -50.0,	    90.0,	  -110.0,	   -50.0	}
};

// 64-slabs station of 60x180x20mm
class BMSLayout1 : public BMSLayout {
public:
    BMSLayout1() : BMSLayout( 64, 12, 60., 180., 240 ) {}
    void slab_info( uint16_t nSlab
                  , float & cx, float & cy, float & cz
                  , float & w, float & h, float & depth
                  ) const override {
        if(nSlab < 4 || nSlab >= 60) {
            cx = 30;
            w = 60;
        } else {
            cx = 15 + (nSlab%2)*30;  // TODO: check L/R
            w = 30;
        }
        cy = _BMS_mappings_1(nSlab);
        cz = _gZCorr[nSlab][0];
        h = 5;
        depth = 20;
        cx -= width/2;
    }
};

// 64-slabs station of 120x90x20mm of complicated layout
class BMSLayout2 : public BMSLayout {
public:
    BMSLayout2() : BMSLayout( 64, 12, 120., 90., 240 ) {}
    void slab_info( uint16_t n
                  , float & cx, float & cy, float & cz
                  , float & w, float & h, float & depth
                  ) const override {
        if(n < 8 || n > 55) {
            cx = 30 + 60*(n%2);
            w = 60;
        } else {
            if( n < 20 || n > 43 ) {
                if(0 == n%4) {
                    w = 45;
                    cx = 45./2;
                } else if(3 == n%4) {
                    w = 45;
                    cx = 120 - 45/2.;
                } else {
                    w = 15;
                    cx = 60 + (15./2)*((n%2) ? -1 : 1);
                }
            } else {
                const float widths[6] = {35,     15, 10, 10, 15,      35 }
                          , x[6]      = {17.5, 42.5, 55, 65, 77.5, 102.5 };
                          ;
                const int nc = (n-20)%6;
                assert(nc<6);
                w = widths[nc];
                cx    = x[nc];
            }
        }
        cy = _BMS_mappings_2(n);
        cz = _gZCorr[n][1];
        h = 5;
        depth = 20;
        cx -= width/2;
    }
};

// 64-slabs station of 120x100x20mm of complicated layout
// Differs from #2 layout by four extra lines of 45+15+15+45 instead of
// two 35+15+10+10+15+35 in the middle.
class BMSLayout3 : public BMSLayout {
public:
    BMSLayout3() : BMSLayout( 64, 12, 120., 100., 240 ) {}
    void slab_info( uint16_t n
                  , float & cx, float & cy, float & cz
                  , float & w, float & h, float & depth
                  ) const override {
        if(n < 12 || n > 51) {
            cx = 30 + 60*(n%2);
            w = 60;
        } else {
            if( n < 20 || n > 43 ) {
                if(0 == n%4) {
                    w = 45;
                    cx = 45./2;
                } else if(3 == n%4) {
                    w = 45;
                    cx = 120 - 45/2.;
                } else {
                    w = 15;
                    cx = 60 + (15./2)*((n%2) ? -1 : 1);
                }
            } else {
                const float widths[6] = {35,     15, 10, 10, 15,      35 }
                          , x[6]      = {17.5, 42.5, 55, 65, 77.5, 102.5 };
                          ;
                const int nc = (n-20)%6;
                assert(nc<6);
                w  = widths[nc];
                cx = x[nc];
            }
        }
        cy = _BMS_mappings_3(n);
        cz = _gZCorr[n][1];
        h = 5;
        depth = 20;
        cx -= width/2;
    }
};

// 64-slabs station of 60x180x20mm
class BMSLayout4 : public BMSLayout {
public:
    BMSLayout4() : BMSLayout( 64, 12, 60., 230., 240. ) {}
    void slab_info( uint16_t nSlab
                  , float & cx, float & cy, float & cz
                  , float & w, float & h, float & depth
                  ) const override {
        if(nSlab < 14 || nSlab >= 50) {
            cx = 30;
            w = 60;
        } else {
            cx = 15 + (nSlab%2)*30;  // TODO: check L/R
            w = 30;
        }
        cy = _BMS_mappings_4(nSlab);
        cz = _gZCorr[nSlab][0];
        h = 5;
        depth = 20;
        cx -= width/2;
    }
};

class RegularBMSLayout : public BMSLayout {
public:
    RegularBMSLayout( uint16_t nSlabs
                    , uint16_t nSubPlanes_
                    , float width_
                    , float height_
                    , float depth_
                    ) : BMSLayout(nSlabs, nSubPlanes_, width_, height_, depth_)
                      {}
    void slab_info( uint16_t n
                  , float & cx, float & cy, float & cz
                  , float & w, float & h, float & depth
                  ) const override {
        cx = 0;
        cy = height*((n + .5)/nSlabs - .5);
        cz = 0;  // TODO: 4 double layers with partial intersection
        w = width;
        h = height/nSlabs;
    }
};

std::shared_ptr<BMSLayout>
BMSLayout::instantiate(int n) {
    if(1 == n) return std::make_shared<BMSLayout1>();
    if(2 == n) return std::make_shared<BMSLayout2>();
    if(3 == n) return std::make_shared<BMSLayout3>();
    if(4 == n) return std::make_shared<BMSLayout4>();
    if(5 == n) return std::make_shared<RegularBMSLayout>( 64, 8, 120, 160, 240);  // FIXME
    if(6 == n) return std::make_shared<RegularBMSLayout>(128, 8, 120, 160, 240);
    NA64DP_RUNTIME_ERROR("Bad station number: %d", n);
}


}  // namespace ::na64dp::util
}  // namespace na64dp

