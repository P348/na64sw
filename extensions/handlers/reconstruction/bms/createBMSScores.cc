#include "na64dp/abstractHitHandler.hh"
#include "bmsLayout.hh"
#include "na64util/numerical/linalg.hh"
#include "na64util/transformations.hh"

namespace na64dp {
namespace handlers {

static double sqrt12 = 1./sqrt(12.);

/**\brief Ad-hoc handler to clusterize BMS hits
 *
 * Combines BMS hits into track scores using timing information. This is an
 * alternative to standard score-creation scheme (guided by combination rules).
 *
 * First, collects per-station hits references from the event, then applies
 * rough clusterization procedure to combine paired hits or create singular
 * scores where combination is not possible. Relies on hardcoded BMS layaout.
 * */
class CreateBMSScore : public AbstractHitHandler<event::F1Hit> {
protected:
    /// Combines static layout info (provided by `BMSLayout` class) and
    /// corrections provided by calibration data (placements info)
    struct BMSLayoutEntry {
        std::shared_ptr<util::BMSLayout> layoutObject;
        float yThreshold;  ///< max delta by Y position to assume correlation
        float timeThreshold;  ///< max time delta to assume correlation
        // ...
    };
    /// (Temporary) aux struct created for every hit
    struct HitInfo {
        decltype(event::Event::f1Hits)::iterator it;
        util::Vec3 c  ///< slab center
                 , dims;  ///< hit dimensions (slab dims)
    };
    /// Temporary cache valid for current event
    std::unordered_map<DetID, std::vector<HitInfo> >
        _hitmap;
    /// Layout entries by station number
    std::unordered_map<DetNumber_t, BMSLayoutEntry> _layouts;
private:
    const float _defaultYThreshold
              , _defaultTimeThreshold
              ;
public:
    /// Main ctr of the handler
    CreateBMSScore( calib::Dispatcher & ch
                  , log4cpp::Category & log
                  , const std::string & selection
                  , float yThreshold=0.
                  , float timeThreshold=0.
                  ) : AbstractHitHandler<event::F1Hit>(ch, selection, log)
                    , _defaultYThreshold(yThreshold)
                    , _defaultTimeThreshold(timeThreshold)
                    {}
    virtual ~CreateBMSScore() {}
    /// After standard `process_event()` is invoked, clusterization and hit
    /// creation is performed.
    ProcRes process_event(event::Event &) override;
    /// Fills up per-station hits collection
    bool process_hit( EventID
                    , DetID
                    , event::F1Hit &) override;
};

AbstractHandler::ProcRes
CreateBMSScore::process_event(event::Event & event) {
    auto & L = this->log();
    const bool doDebugPrintout
            = L.getPriority() >= log4cpp::Priority::DEBUG;
    // call parent implem to (selectively) collect BMS hits
    AbstractHitHandler<event::F1Hit>::process_event(event);
    //assert(std::fabs(lengthFact - 0.1) < 1e-6);  // XXX
    // combine hits, create scores
    for(auto & stationEntry: _hitmap) {
        if( doDebugPrintout )
            L << log4cpp::Priority::DEBUG
              << "Considering " << naming()[stationEntry.first].c_str() << ":";
        auto layoutIt = _layouts.find(stationEntry.first.number());
        assert(layoutIt != _layouts.end());
        std::unordered_set<const event::F1Hit *> taken;
        for( auto itA = stationEntry.second.begin()
           ; itA != stationEntry.second.end()
           ; ++itA ) {
            if(taken.end() != taken.find(itA->it->second.get())) continue;
            taken.emplace(itA->it->second.get());
            // ^^^ omit already considered hits, for the rest create the score
            auto scoreRef = lmem().create<event::TrackScore>(lmem());
            util::reset(*scoreRef);
            // add A hit to new score
            scoreRef->hitRefs.emplace(itA->it->first, itA->it->second);
            // to track cluster mean values for correlation
            auto & clusYSum = scoreRef->lR[0];  // u
            clusYSum = itA->c.c.y;
            auto & clusTimeSum = scoreRef->time;
            clusTimeSum = itA->it->second->rawData->timeDecoded;
            // this values are not used for correlation lookup, but desired to
            // be calculated for the score
            auto & clusXSum = scoreRef->lR[1]  // v
               , & clusZSum = scoreRef->lR[2];  // w
            clusXSum = itA->c.c.x;
            clusZSum = itA->c.c.z;
            for( auto itB = itA
               ; itB != stationEntry.second.end()
               ; ++itB ) {
                if( doDebugPrintout )
                    L << log4cpp::Priority::DEBUG
                      << "  Considering hits A=" << itA->c
                      << ", B=" << itB->c;
                if(taken.end() != taken.find(itB->it->second.get())) {
                    if( doDebugPrintout )
                        L << log4cpp::Priority::DEBUG
                          << "    ...B is already taken, skip.";
                    continue;
                }
                // check positional correlation of hit B vs current cluster
                // (built *at least* on hit A)
                if( std::fabs( clusYSum/scoreRef->hitRefs.size()
                             - itB->c.c.y )
                    > layoutIt->second.yThreshold ) {
                    if( doDebugPrintout )
                        L << log4cpp::Priority::DEBUG
                          << "    y=|"
                          << clusYSum/scoreRef->hitRefs.size() << " - "
                          << itB->c.c.y << "| > " << layoutIt->second.yThreshold
                          << ", skip.";
                    continue;  // no positional (Y) correlation
                }
                // check correlation
                if( std::fabs( clusTimeSum/scoreRef->hitRefs.size()
                             - itB->it->second->rawData->timeDecoded
                             )
                    > layoutIt->second.timeThreshold ) {
                    if( doDebugPrintout )
                        L << log4cpp::Priority::DEBUG
                          << "    t: |"
                          << clusTimeSum/scoreRef->hitRefs.size() << " - "
                          << itB->it->second->rawData->timeDecoded << "| > "
                          << layoutIt->second.timeThreshold
                          << ", skip.";
                    continue;  // no time correlation
                }
                // mark B hit as "considered"
                taken.emplace(itB->it->second.get());
                // advance cluster sums
                clusXSum += itB->c.c.x;
                clusYSum += itB->c.c.y;
                clusZSum += itB->c.c.z;
                clusTimeSum += itB->it->second->rawData->timeDecoded;
                scoreRef->hitRefs.emplace(itB->it->first, itB->it->second);
                if( doDebugPrintout )
                        L << log4cpp::Priority::DEBUG
                          << "    added to a cluster.";
            }
            // Note: we're uncertain about whether X position of slabs obey the
            //       layout since in principle, BMS were not designed to
            //       provide X, so mapping mismatch in this term is permitted,
            //       thus all slabs are assumed to provide same local V with
            //       covariance spanning as BMS width.
            // finalize score values as average position
            const int clusSize = scoreRef->hitRefs.size();
            clusXSum    /= clusSize;
            clusYSum    /= clusSize;
            clusZSum    /= clusSize;
            // again: local 1st coordinate is global Y, BUT: layout (and
            // its dimensions) is given in global coordinates, so "width"
            // is max by X, "height" is max by Y, etc.
            clusXSum /= layoutIt->second.layoutObject->width;
            clusYSum /= layoutIt->second.layoutObject->height;
            clusZSum /= layoutIt->second.layoutObject->depth;
            //clusXSum -= .5;
            //clusYSum -= .5;  // already accounted?
            //clusZSum -= .5;
            clusTimeSum /= clusSize;
            // calc uncertainties (local)
            // TODO: this is not correct estimation in general case actually as
            //       it does not take into account cluster size
            // TODO: NORMALIZE IT1
            scoreRef->lRErr[0] = (5./layoutIt->second.layoutObject->height)*sqrt12;
            // ^^^ std.dev of 5mm slab in cm, normalized
            scoreRef->lRErr[1] = sqrt12;
            scoreRef->lRErr[2] = sqrt12;
            // add new score to event
            #ifndef NDEBUG
            for(int i = 0; i < 3; ++i) {
                if( scoreRef->lR[i] >= -.5 && scoreRef->lR[i] <= .5) continue;
                char cc[] = "UVW";
                NA64DP_RUNTIME_ERROR("BMS layout assertion failure for %s:"
                        " normalized coordinate %c is %f (outside [-0.5;0.5])."
                        , naming()[stationEntry.first].c_str()
                        , cc[i], scoreRef->lR[i]);
            }
            #endif
            event.trackScores.emplace(PlaneKey(stationEntry.first), scoreRef);
            if( doDebugPrintout )
                L << log4cpp::Priority::DEBUG
                  << "  Added new BMS cluster of "
                  << clusSize << " hits, w coords {"
                  << scoreRef->lR[0] << ", " << scoreRef->lR[1] << ", " << scoreRef->lR[2]
                  << "}, covs {"
                  << scoreRef->lRErr[0] << ", " << scoreRef->lRErr[1] << ", " << scoreRef->lRErr[2]
                  << "}. Unnormalized Y="
                  << clusYSum*layoutIt->second.layoutObject->height
                  ;
        }
    }
    // clear per-event cache
    for(auto & m : _hitmap) {
        m.second.clear();
    }
    return kOk;
}

bool
CreateBMSScore::process_hit( EventID
                           , DetID did
                           , event::F1Hit & f1Hit) {
    if(!f1Hit.rawData) return true;  // omit hits with no raw data
    auto layoutIt = _layouts.find(did.number());
    if(layoutIt == _layouts.end()) {
        layoutIt = _layouts.emplace(did.number(), BMSLayoutEntry{
                util::BMSLayout::instantiate(did.number()),  // static layout shared ptr
                _defaultYThreshold,  // Y threshold for corrs; TODO: calib?
                _defaultTimeThreshold,  // time threshold for corrs; TODO: calib?
                }).first;
    }

    auto stIt = _hitmap.emplace(PlaneKey(did), decltype(_hitmap)::mapped_type()).first;
    HitInfo hi{ _cHitIt };
    // append hit info using layout info and create entry for further
    // clustering/score creation
    layoutIt->second.layoutObject->slab_info( did.payload_as<WireID>()->wire_no()
            , hi.c.c.x, hi.c.c.y, hi.c.c.z
            , hi.dims.c.x, hi.dims.c.y, hi.dims.c.z
            );
    stIt->second.push_back(hi);
    return true;
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( CreateBMSScores, ch, cfg
                , "Creates BMS scores (clustering)" ) {
    return new handlers::CreateBMSScore( ch
            , na64dp::aux::get_logging_cat(cfg)
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["yThreshold_mm"] ? cfg["yThreshold_mm"].as<float>() : 0.
            , cfg["timeThreshold_hwu"] ? cfg["timeThreshold_hwu"].as<float>() : 0.
            );
}

}  // namespace na64dp
