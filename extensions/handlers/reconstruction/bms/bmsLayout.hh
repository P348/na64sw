#pragma once

#include "na64sw-config.h"

#include <cstdint>
#include <memory>

namespace na64dp {
namespace util {

/**\brief Interface class for BMS station slab layout
 *
 * Defines number of slabs for the n-th station and layout scheme.
 * */
class BMSLayout {
public:
    const uint16_t nSlabs  ///< Total number of slabs in the layout
                 , nSubPlanes  ///< Number of sub-planes of diff-t Z in the station
                 ;
    const float width  ///< Station width (X), mm
              , height  ///< Station height (Y), mm
              , depth  ///< Station depth (Z), mm
              ;
    /// Returns N-ths slab parameters
    virtual void slab_info( uint16_t nSlab
                          , float & cx, float & cy, float & cz
                          , float & width, float & height, float & depth
                          ) const = 0;

    /// Returns ref to global BMS layout instance
    static std::shared_ptr<BMSLayout> instantiate(int);
protected:
    BMSLayout( uint16_t nSlabs_, uint16_t nSubPlanes_
             , float w, float h, float d
             ) : nSlabs(nSlabs_), nSubPlanes(nSubPlanes_)
               , width(w), height(h), depth(d) {}
};

}  // namespace ::na64dp::util
}  // namespace na64dp

