#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2I.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Plots 2D histogram of time measured by F1 chip on plane vs master
 *        time
 *
 * Experimental handler
 *
 * \code{.yaml}
 *     - _type: BMSPlotTimeVsMaster
 *       # optional, hit selection expression
 *       applyTo: null
 *       # histogram base name; str, opt
 *       baseName: "maxMult"
 * \endcode
 *
 * \ingroup handlers bms-handlers
 * */
class BMSPlotTimeVsMaster : public util::TDirAdapter
                          , public AbstractHitHandler<event::F1Hit> {
private:
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::unordered_map<DetID, TH2I *> _hists;
    /// Updated at the beginning of each event
    float _cMasterTime;
public:
    /// Main ctr of the handler
    BMSPlotTimeVsMaster( calib::Dispatcher & ch
                       , const std::string & selection
                       , const std::string & hstBaseName="amp"
                       );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~BMSPlotTimeVsMaster();
    /// Caches master time value to be used later
    ProcRes process_event(event::Event & e) override {
        _cMasterTime = e.masterTime;
        return AbstractHitHandler<event::F1Hit>::process_event(e);
    }
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::F1Hit & ) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};  // BMSPlotTimeVsMaster

//                          * * *   * * *   * * *

BMSPlotTimeVsMaster::BMSPlotTimeVsMaster( calib::Dispatcher & ch
                                        , const std::string & selection
                                        , const std::string & baseName
                                        ) : util::TDirAdapter(ch)
                                          , AbstractHitHandler(ch, selection)
                                          , _hstBaseName(baseName) {}

bool
BMSPlotTimeVsMaster::process_hit( EventID
                                , DetID did
                                , event::F1Hit & hit) {
    if( !hit.rawData ) return true;  // no raw data associated with hit
    PlaneKey pk(did);
    auto it = _hists.find( pk );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(pk, histName)->cd();
        TH2I * newHst = new TH2I( histName.c_str()
                                , "Time correlation; station time, ns ; master time, ns"
                                , 3000, -1500, 1500
                                , 80, 60, 140  // TODO: parameterize
                                );
        auto ir = _hists.emplace( pk, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }
    // calc number of peaks and fill the histo
    it->second->Fill(hit.rawData->timeDecoded, _cMasterTime);
    return true;
}

void
BMSPlotTimeVsMaster::finalize() {
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

BMSPlotTimeVsMaster::~BMSPlotTimeVsMaster() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( BMSPlotTimeVsMaster, ch, cfg
                , "Plots BMS plane time vs master time" ) {
    return new handlers::BMSPlotTimeVsMaster( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["baseName"] ? cfg["baseName"].as<std::string>() : "timeVsmasterTime"
            );
}

}

