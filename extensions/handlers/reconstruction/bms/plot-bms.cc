#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"
#include "bmsLayout.hh"

#include <TH2Poly.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Plots beam image as seen by BMS
 *
 * This handler depicts a 2D histogram for BMS stations using `TH2Poly`.
 *
 * \code{.yaml}
 *    - _type: BMSPlot
 *      # optional, hit selection expression
 *      applyTo: null
 *      # histogram base name
 *      baseName: "bms-image"
 * \endcode
 *
 * Chooses layout in use based on station name.
 *
 * \see TDirAdapter
 * \ingroup handlers bms-handlers
 * */
class BMSPlot : public util::TDirAdapter
              , public AbstractHitHandler<event::F1Hit> {
private:
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::unordered_map<DetID_t, TH2Poly *> _hists;
    std::unordered_map<DetID, std::unordered_map<int, Int_t> > _binMaps;
public:
    /// Main ctr of the handler
    BMSPlot( calib::Dispatcher & ch
           , const std::string & selection
           , const std::string & hstBaseName="bms-image"
           );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~BMSPlot();
    /// Puts samples onto a histogram
    bool process_hit( EventID
                    , DetID
                    , event::F1Hit &) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

/** \param ch Calibration dispatcher instance
 *  \param selection Detector selection expression in use
 *  \param baseName Histogram name suffix to name the corresponding TH2F
 *  */
BMSPlot::BMSPlot( calib::Dispatcher & ch
                , const std::string & selection
                , const std::string & baseName
                ) : util::TDirAdapter(ch)
                  , AbstractHitHandler(ch, selection)
                  , _hstBaseName(baseName) {}

bool
BMSPlot::process_hit( EventID
                    , DetID did_
                    , event::F1Hit & currentHit) {
    if( !currentHit.rawData ) return true;  // no raw data associated with hit
    PlaneKey did(did_);
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        auto l = util::BMSLayout::instantiate(did.number());
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        TH2Poly * newHst = new TH2Poly( histName.c_str()
                                      , "BMS image; X, mm; Y, mm"
                                      , -l->width/2, l->width
                                      , -l->height/2, l->height/2
                                      );
        auto & m = _binMaps.emplace(did, decltype(_binMaps)::mapped_type()).first->second;
        Double_t slabXs[4], slabYs[4];
        for(unsigned short i = 0; i < l->nSlabs; ++i) {
            float cx, cy, cz, slabW, slabH, slabD;
            l->slab_info(i, cx, cy, cz, slabW, slabH, slabD);
            slabXs[0] = cx - slabW/2;     slabYs[0] = cy - slabH/2;
            slabXs[1] = cx - slabW/2;     slabYs[1] = cy + slabH/2;
            slabXs[2] = cx + slabW/2;     slabYs[2] = cy + slabH/2;
            slabXs[3] = cx + slabW/2;     slabYs[3] = cy - slabH/2;
            m.emplace(i, newHst->AddBin(4, slabXs, slabYs));
        }
        auto ir = _hists.emplace( did, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }
    // fill the histogram
    auto binNoIt = _binMaps[did].find(currentHit.rawData->channel);
    assert(binNoIt != _binMaps[did].end());  // channel number is within the range permitted by layout
    //it->second->AddBinContent(binNoIt->second);
    Int_t nBin = binNoIt->second;
    it->second->SetBinContent(nBin, it->second->GetBinContent(nBin) + 1);
    return true;
}

void
BMSPlot::finalize() {
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

BMSPlot::~BMSPlot() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( BMSPlot, ch, cfg
                , "Builds 2D histograms of hits in the BMS station" ) {
    return new handlers::BMSPlot( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["baseName"] ? cfg["baseName"].as<std::string>() : "bms-image"
            );
}

}
