#include "f1Conjugate.hh"
#include "na64event/data/f1.hh"

namespace na64dp {
namespace aux {

// for source functions, see utils/bms-layout.C
static const float _bmsLayout1[] = {
 -87.5,  -82.5,  -77.5,  -72.5,     -67.5,  -67.5,  -62.5,  -62.5, 
 -57.5,  -57.5,  -52.5,  -52.5,     -47.5,  -47.5,  -42.5,  -42.5, 
 -37.5,  -37.5,  -32.5,  -32.5,     -27.5,  -27.5,  -22.5,  -22.5, 
 -17.5,  -17.5,  -12.5,  -12.5,      -7.5,   -7.5,   -2.5,   -2.5, 
   2.5,    2.5,    7.5,    7.5,      12.5,   12.5,   17.5,   17.5, 
  22.5,   22.5,   27.5,   27.5,      32.5,   32.5,   37.5,   37.5, 
  42.5,   42.5,   47.5,   47.5,      52.5,   52.5,   57.5,   57.5, 
  62.5,   62.5,   67.5,   67.5,      72.5,   77.5,   82.5,   87.5,
};
static const float _bmsLayout2[] = {
 -42.5,  -42.5,  -37.5,  -37.5,     -32.5,  -32.5,  -27.5,  -27.5, 
 -22.5,  -22.5,  -22.5,  -22.5,     -17.5,  -17.5,  -17.5,  -17.5, 
 -12.5,  -12.5,  -12.5,  -12.5,      -7.5,   -7.5,   -7.5,   -7.5, 
  -7.5,   -7.5,   -2.5,   -2.5,      -2.5,   -2.5,   -2.5,   -2.5, 
   2.5,    2.5,    2.5,    2.5,       2.5,    2.5,    7.5,    7.5, 
   7.5,    7.5,    7.5,    7.5,      12.5,   12.5,   12.5,   12.5, 
  17.5,   17.5,   17.5,   17.5,      22.5,   22.5,   22.5,   22.5, 
  27.5,   27.5,   32.5,   32.5,      37.5,   37.5,   42.5,   42.5,
};
static const float _bmsLayout3[] = {
 -47.5,  -47.5,  -42.5,  -42.5,     -37.5,  -37.5,  -32.5,  -32.5, 
 -27.5,  -27.5,  -22.5,  -22.5,     -17.5,  -17.5,  -17.5,  -17.5, 
 -12.5,  -12.5,  -12.5,  -12.5,      -7.5,   -7.5,   -7.5,   -7.5, 
  -7.5,   -7.5,   -2.5,   -2.5,      -2.5,   -2.5,   -2.5,   -2.5, 
   2.5,    2.5,    2.5,    2.5,       2.5,    2.5,    7.5,    7.5, 
   7.5,    7.5,    7.5,    7.5,      12.5,   12.5,   12.5,   12.5, 
  17.5,   17.5,   17.5,   17.5,      22.5,   22.5,   27.5,   27.5, 
  32.5,   32.5,   37.5,   37.5,      42.5,   42.5,   47.5,   47.5,
};
static const float _bmsLayout4[] = {
-112.5, -107.5, -102.5,  -97.5,     -92.5,  -87.5,  -82.5,  -77.5, 
 -72.5,  -67.5,  -62.5,  -57.5,     -52.5,  -47.5,  -42.5,  -42.5, 
 -37.5,  -37.5,  -32.5,  -32.5,     -27.5,  -27.5,  -22.5,  -22.5, 
 -17.5,  -17.5,  -12.5,  -12.5,      -7.5,   -7.5,   -2.5,   -2.5, 
   2.5,    2.5,    7.5,    7.5,      12.5,   12.5,   17.5,   17.5, 
  22.5,   22.5,   27.5,   27.5,      32.5,   32.5,   37.5,   37.5, 
  42.5,   42.5,   47.5,   52.5,      57.5,   62.5,   67.5,   72.5, 
  77.5,   82.5,   87.5,   92.5,      97.5,  102.5,  107.5,  112.5,
};

void
TrackScoreTraits<event::F1Hit>::recache_placement( Cache & cache
                                                 , PlaneKey pk
                                                 , const calib::Placement & placement
                                                 ) {
    // assure it is ordinary plane's placement
    if( calib::Placement::kRegularWiredPlane   == placement.suppInfoType ) {
        // try to find a plane to update it instead of (re)creation
        PlaneEntry & planeEntry = cache.emplace(pk, PlaneEntry {
                    PlaneEntry::kRegularStride,  // layout type
                    placement.name,  // det name
                    placement.size[1], // wire length
                    { placement.rot[0]
                    , placement.rot[1]
                    , placement.rot[2]
                    } ,  // angles
                    TVector3( placement.center[0]
                            , placement.center[1]
                            , placement.center[2] ),  // position
                    {PlaneEntry::RegularStride()}  // layout-dependant payload
                }).first->second;
        planeEntry.pl.regular.nWires = placement.suppInfo.regularPlane.nWires;
        if(planeEntry.pl.regular.nWires == 0) {
            NA64DP_RUNTIME_ERROR("Bad wires no for \"%s\": %d (regular).",
                    placement.name.c_str(), placement.suppInfo.regularPlane.nWires );
        }
        // a stride parameter defining inter-wire distance (pitch)
        planeEntry.pl.regular.stride = placement.size[0]/planeEntry.pl.regular.nWires;
        if(std::isnan(planeEntry.pl.regular.stride) || planeEntry.pl.regular.stride <= 0) {
            NA64DP_RUNTIME_ERROR("Bad stride for \"%s\": %f (regular).",
                    placement.name.c_str(), planeEntry.pl.regular.stride );
        }
        planeEntry.wl = placement.size[1];
        if(std::isnan(planeEntry.wl) || 0. == planeEntry.wl ) {
            NA64DP_RUNTIME_ERROR("Bad wire length for \"%s\": %f (regular).",
                    placement.name.c_str(), planeEntry.pl.regular.stride );
        }
        for(int i = 0; i < 3; ++i)
            planeEntry.angles[i] = placement.rot[i];
    } else if( calib::Placement::kIrregularWiredPlane == placement.suppInfoType ) {
        // try to find a plane to update it instead of (re)creation
        PlaneEntry & planeEntry = cache.emplace(pk, PlaneEntry {
                    PlaneEntry::kIrregularStride,  // layout type
                    placement.name,  // det name
                    placement.size[1], // wire length
                    { placement.rot[0]
                    , placement.rot[1]
                    , placement.rot[2]
                    } ,  // angles
                    TVector3( placement.center[0]
                            , placement.center[1]
                            , placement.center[2] ),  // position
                    {}  // layout-dependant payload
                }).first->second;
        const std::string nm = placement.suppInfo.irregularPlane.layoutName;
        if( nm == "BMS1" ) {
            planeEntry.pl.irregular.wireMapPtr = _bmsLayout1;
            planeEntry.pl.irregular.nWires = sizeof(_bmsLayout1)/sizeof(*_bmsLayout1);
        } else if( nm == "BMS2" ) {
            planeEntry.pl.irregular.wireMapPtr = _bmsLayout2;
            planeEntry.pl.irregular.nWires = sizeof(_bmsLayout2)/sizeof(*_bmsLayout2);
        } else if( nm == "BMS3" ) {
            planeEntry.pl.irregular.wireMapPtr = _bmsLayout3;
            planeEntry.pl.irregular.nWires = sizeof(_bmsLayout3)/sizeof(*_bmsLayout3);
        } else if( nm == "BMS4" ) {
            planeEntry.pl.irregular.wireMapPtr = _bmsLayout4;
            planeEntry.pl.irregular.nWires = sizeof(_bmsLayout4)/sizeof(*_bmsLayout4);
        } else {
            NA64DP_RUNTIME_ERROR("Bad layout name \"%s\" for irregular mapping"
                    " on F1 detector entry \"%s\"", nm.c_str()
                    , placement.name.c_str() );
        }
    } else {
        NA64DP_RUNTIME_ERROR( "Wrong type of placement information"
                " entry for \"%s\" (expected regular/irregular plane)"
                , placement.name.c_str() );
        // ^^^ TODO dedicated exception for better msging
    }
}

std::pair<DetID_t, mem::Ref<event::TrackScore> >
TrackScoreTraits<event::F1Hit>::create( LocalMemory & lmem
                                      , mem::Ref<event::F1Hit> c
                                      , DetID did
                                      , Cache & cache
                                      ) {
    // IMPORTANT: detector ID converted to plane key cuts away information
    // about wires (this is deliberate conversion as scores must be
    // identified by planes).
    PlaneKey pk(did);
    assert(!WireID(pk.payload()).wire_no_is_set());
    WireID wid(did.payload());
    assert(wid.wire_no_is_set());
    // find plane description
    auto it = cache.find(pk);
    if(cache.end() == it) return {did, nullptr};  // no placement for this detector
    const PlaneEntry & pe = it->second;
    unsigned int nChannels;
    if( pe.typeCode == PlaneEntry::kRegularStride ) {
        nChannels = pe.pl.regular.nWires;
    } else if( pe.typeCode == PlaneEntry::kIrregularStride ) {
        nChannels = pe.pl.irregular.nWires;
    } else {
        assert(false);
    }
    if( wid.wire_no() > nChannels ) {
        NA64DP_RUNTIME_ERROR( "F1 hit to-score conversion error:"
                " provided number of wire %d is greater than number of"
                " wires in the plane (%d)."
                , (int) wid.wire_no()
                , (int) nChannels );
    }
    auto dest = lmem.create<event::TrackScore>(lmem);
    util::reset(*dest);
    if( pe.typeCode == PlaneEntry::kRegularStride ) {
        // set single local coordinate based on stride
        dest->lR[0] = (wid.wire_no() + .5 - nChannels/2.) * pe.pl.regular.stride;
        assert(!std::isnan(dest->lR[0]));
        dest->lR[1] = std::nan("0");
    } else if( pe.typeCode == PlaneEntry::kIrregularStride ) {
        if(c->rawData->channel > (int) nChannels) {
            NA64DP_RUNTIME_ERROR("Irregular channel number %d on F1 detector %d"
                    "exceeds max number %d."
                    , (int) c->rawData->channel
                    , (int) did.id
                    , (int) nChannels
                    );
        }
        dest->lR[0] = pe.pl.irregular.wireMapPtr[c->rawData->channel];
    }
    // associate hit
    dest->hitRefs.emplace(did, c);
    assert(pk.is_payload_set());
    return {pk, dest};
}

#if 0  // XXX, deprecated
// Wire-to-coordinate mapping function for BMS stations
// Albeit being in general run-dependant information those mappings practically
// were not changed for ages on T6 beamline (neither it is anticipated).
// Written originally by A.Shevelev (TPU) based on CORAL reconstruction code
static float
_BMS_position_1( int32_t wirePos, float sizeY2, float & fibersize ) {
    fibersize = .5;
    if ( wirePos <= 4 )
		return fibersize * ( wirePos - 1) - sizeY2;
    if ( wirePos >4 && wirePos <= 60 )
		return 2.0 + fibersize * ( (wirePos -5 ) / 2) - sizeY2;
    return 16.0 + fibersize * (wirePos -61 ) - sizeY2;
}

static float
_BMS_position_2( int32_t wirePos, float sizeY2, float & fibersize ) {
    fibersize = 0.5;
    if ( wirePos <= 8 )
        return fibersize * ( wirePos - 1 )/2 - sizeY2;
    if ( wirePos >8 && wirePos <= 20 )
        return 2.0 + fibersize * ( (wirePos -9 ) / 4) - sizeY2;
    if ( wirePos >20 && wirePos <= 44 )
        return 3.5 + fibersize * ( (wirePos -21 ) / 6) - sizeY2;
    if ( wirePos >44 && wirePos <= 56 )
        return 5.5 + fibersize * ( (wirePos -45 ) / 4) - sizeY2;
    return 6.0 + fibersize * ( (wirePos -57 ) / 2) - sizeY2;
}

static float
_BMS_position_3( int32_t wirePos, float sizeY2, float & fibersize ) {
    fibersize = 0.5;
    if ( wirePos <= 12 )
        return fibersize * (wirePos -1 )/2 - sizeY2;
    if ( wirePos >12 && wirePos <= 20 )
        return 3.0 + fibersize * ( (wirePos -13 ) / 4) - sizeY2;
    if ( wirePos >20 && wirePos <= 44 )
        return 4.0 + fibersize * ( (wirePos -21 ) / 6) - sizeY2;
    if ( wirePos >44 && wirePos <= 54 )
        return 6.0 + fibersize * ( (wirePos -45 ) / 4) - sizeY2;
    return 7.5 + fibersize * ( (wirePos -55 ) / 2) - sizeY2;
}

static float
_BMS_position_4( int32_t wirePos, float sizeY2, float & fibersize ) {
    fibersize = 0.5;
    if ( wirePos <= 14 )
        return fibersize * (wirePos -1) - sizeY2;
    if ( wirePos >14 && wirePos <= 50 )
        return 7.0 + fibersize * ( (wirePos -15 ) / 2) - sizeY2;
    return 16.0 + fibersize * (wirePos -51 ) - sizeY2;
}

static float
_BMS_position_5( int32_t wirePos, float sizeY2, float & fibersize ) {
	fibersize = 0.25;
	return fibersize * wirePos - sizeY2;
}

static float
_BMS_position_6( int32_t wirePos, float sizeY2, float & fibersize ) {
	fibersize = 0.125;
    return fibersize * wirePos - sizeY2;
}
#endif

}  // namespace ::na64dp::aux
}  // namespace na64dp

