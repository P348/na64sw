#include "na64dp/abstractHitHandler.hh"
#include <cmath>

namespace na64dp {
namespace handlers {

/**\brief Uses \f$t_{01}\f$ and \f$t_{12}\f$ and error estimations to calculate time
 *        and hit time error for `APVHit`
 *
 * The time and time error will be estimated by the following formulae:
 *
 *  $$
 *      t = (\\frac{t_{02}}{\\sigma_{02}(t)^2} + \\frac{t_{12}}{\\sigma_{12}(t)^2}) / (\\frac{1}{\\sigma_{02}(t)^2} + \\frac{1}{\\sigma_{12}(t)^2}), \\\\
 *      \\delta t = (\\frac{1}{\\sigma_{02}(t)^2} + \\frac{1}{\\sigma_{12}(t)^2})^{-1/2}
 *  $$
 *
 * Usage info:
 *
 * \code{.yaml}
 *     - _type: APVCalcTime
 *       # optional, hit selection expression
 *       applyTo: null
 * \endcode
 *
 * This handler relies on pre-calculated values within APV hit that usually
 * acquired using some calibration data by other handler(s):
 *
 *  - `t02`
 *  - `t12`
 *  - `t02sigma`
 *  - `t12sigma`
 *
 *  To obtain these values, consider using of `CalcAPVTimeType1` handler.
 *
 * \see CalcAPVTimeType1
 * \ingroup handlers apv-handlers
 * */
class APVCalcTime : public AbstractHitHandler<event::APVHit> {
public:
    /// Ctr: forwards calib dispatcher and detector selection args to parent
    /// class
    APVCalcTime( calib::Dispatcher & cdsp
               , const std::string & only
               , log4cpp::Category & logCat
               , const std::string & namingClass="default"
               ) : AbstractHitHandler<event::APVHit>(cdsp, only, logCat, namingClass) {}
    /// Unconditionally performs APV hit time calculus based on time ratios
    virtual bool process_hit( EventID, DetID, event::APVHit & hit ) override {
        using std::pow;
        hit.time = hit.t02 / pow(hit.t02sigma, 2)
                 + hit.t12 / pow(hit.t12sigma, 2)
                 ;
        hit.timeError = 1 / pow(hit.t02sigma, 2)
                      + 1 / pow(hit.t12sigma, 2)
                      ;
        hit.time /= hit.timeError;
        hit.timeError = 1/sqrt(hit.timeError);
        return true;
    }
};

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( APVCalcTime, mgr, cfg
                , "Calculates time and errors for APV-based detectors using APV"
                  " time ratios" ) {
    return new handlers::APVCalcTime( mgr
                                    , aux::retrieve_det_selection(cfg)
                                    , aux::get_logging_cat(cfg)
                                    , aux::get_naming_class(cfg)
                                    );
}

}  // namespace na64dp
