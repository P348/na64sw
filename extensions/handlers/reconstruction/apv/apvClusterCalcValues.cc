#include "na64dp/abstractHitHandler.hh"
#include <cmath>

namespace na64dp {
namespace handlers {

using event::APVCluster;
using event::APVHit;

/**\brief Calculates properties of APV cluster: charge, position, time, etc.
 *
 * Depending on settings, currently provides:
 *  - charge sum
 *  - position (mean or weighted by charge)
 *  - time (mean or weighted by charge)
 *  - time error (root of sum of squared time errors of hits, weighted or not
 *      by charge)
 *
 * \todo Find out how to derive time error properly.
 * */
class APVClusterCalcValues : public AbstractHitHandler<APVCluster> {
private:
    uint8_t _doCalcCharge:2
          , _doCalcPos:2
          , _doCalcTime:2
          , _doCalcTimeError:2
          ;
public:
    virtual bool process_hit(EventID, HitKey, APVCluster & cluster) override {
        double prevCharge = cluster.charge;
        // init sum values
        cluster.charge = 0.;
        if( _doCalcPos       ) cluster.position   = 0.;
        if( _doCalcTime      ) cluster.time       = 0.;
        if( _doCalcTimeError ) cluster.timeError  = 0.;
        // iterate over hits within a cluster, collecting the sums
        for( const auto & apvHitPair : cluster.hits ) {
            const auto wireNo = apvHitPair.first;  //< physical wire number
            const APVHit & apvHit = *apvHitPair.second;
            cluster.charge += apvHit.maxCharge;
            if( _doCalcPos       ) cluster.position  += wireNo      * (0x2 &  _doCalcPos ? apvHit.maxCharge : 1);
            if( _doCalcTime      ) cluster.time      += apvHit.time * (0x2 & _doCalcTime ? apvHit.maxCharge : 1);
            // TODO: this is probably not correct:
            if( _doCalcTimeError ) cluster.timeError += apvHit.timeError*apvHit.timeError
                        * (0x2 & _doCalcTimeError ? apvHit.maxCharge : 1);
        }
        size_t nHits = cluster.hits.size();
        // divide sums by number of hits or by charge, if requested
        cluster.position  /= (0x2 & _doCalcPos       ? cluster.charge : nHits );
        cluster.time      /= (0x2 & _doCalcTime      ? cluster.charge : nHits );
        cluster.timeError /= (0x2 & _doCalcTimeError ? cluster.charge : nHits );
        if( _doCalcTimeError ) cluster.timeError = std::sqrt(cluster.timeError);
        // restore charge value, if calculation of charge was not requested
        if( ! _doCalcCharge ) {
            cluster.charge = prevCharge;
        }
        return true;
    }

    APVClusterCalcValues( calib::Dispatcher & cdsp
                        , const std::string & selection
                        , uint8_t doCalcCharge
                        , uint8_t doCalcPos
                        , uint8_t doCalcTime
                        , uint8_t doCalcTimeError
                        ) : AbstractHitHandler(cdsp, selection)
                          , _doCalcCharge(doCalcCharge)
                          , _doCalcPos(doCalcPos)
                          , _doCalcTime(doCalcTime)
                          , _doCalcTimeError(doCalcTimeError)
                          {}

};

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( APVClusterCalcValues, mgr, cfg,
        "Calculates main APV cluster values: charge, position, time, etc." ) {
    uint8_t doCalcCharge = 0x0
          , doCalcPos = 0x0
          , doCalcTime = 0x0
          , doCalcTimeError = 0x0
          ;
    const auto & chW_ =  cfg["chargeWeighted"].as< std::vector<std::string> >();
    const auto & mean_ = cfg["mean"          ].as< std::vector<std::string> >();

    const std::set<std::string> chW(chW_.begin(), chW_.end());
    const std::set<std::string> mean(mean_.begin(), mean_.end());

    if( mean.end() != mean.find("charge")   ) doCalcCharge    = 0x1;
    if( mean.end() != mean.find("position") ) doCalcPos       = 0x1;
    if( mean.end() != mean.find("time")     ) doCalcTime      = 0x1;
    if( mean.end() != mean.find("timeError")) doCalcTimeError = 0x1;

    if( chW.end() != chW.find("charge")   ) doCalcCharge    = 0x3;
    if( chW.end() != chW.find("position") ) doCalcPos       = 0x3;
    if( chW.end() != chW.find("time")     ) doCalcTime      = 0x3;
    if( chW.end() != chW.find("timeError")) doCalcTimeError = 0x3;

    return new handlers::APVClusterCalcValues( mgr
            , aux::retrieve_det_selection(cfg)
            , doCalcCharge
            , doCalcPos
            , doCalcTime
            , doCalcTimeError
            );
}

}  // namespace na64dp
