#pragma once

/**\file
 * \brief Declares APV cluster-to-score conversion classes
 *
 * This header defines specialization of `TrackScoreTraits` for 1D hit
 * conversion and hit conjugation rule(s) for APV-based detectors.
 * */

#include "tracking/createScore.hh"
#include "na64calib/placements.hh"
#include "na64dp/DSuLConjugationRule.hh"
#include "na64util/numerical/linalg.hh"

namespace na64dp {
namespace aux {

template<typename HitT> struct TrackScoreTraits;  // fwd (undefined)

/**\brief Specialization for 1D scores creation
 *
 * Creates one dimensional `TrackScore` based on `APVCluster`. Used for
 * "1D hits" provided by single strip detector plane.
 *
 * Created score contains only first `lR` and `lRErr`, normalized to detecto
 * size.
 *
 * \ingroup track-combining-hits
 * */
template<>
struct TrackScoreTraits<event::APVCluster> {
    /// Placement cache subject: just a plane + standard resolution
    struct PlaneEntry {
        /// Number of wires in the plane
        int nWires;
        /// Uncertainty set if `APVCluster` did not bring one
        Float_t defaultUncertainty;
    };
    /// A conversion cache type -- plane description excerpt per plane ID
    typedef std::unordered_map<PlaneKey, PlaneEntry> Cache;
    /// Converts placement into cache entry and adds an entry into general cache
    static void recache_placement( Cache & cache
                                 , PlaneKey pk
                                 , const calib::Placement & placement
                                 );

    /// Copies/converts APV cluster into track score (1D for APV clusters)
    ///
    /// Position will be calculattaken from `position` field of `APVCluster`
    /// assuming it to be in wire units. Geometrical information of the plane
    /// is used to convert position and uncertainty from wire units to
    /// normalized detector units.
    ///
    /// Does not create a score and returns null reference if could not convert
    ///
    /// \returns reference to null pointer if can't convert
    static std::pair<DetID_t, mem::Ref<event::TrackScore> >
    create( LocalMemory & lmem
          , mem::Ref<event::APVCluster> c
          , PlaneKey pk
          , Cache & cache
          );
};
}  // namespace ::na64dp::aux

/**\brief Hit conjugation rule for APV detectors
 *
 * This is an utility class that may be used in various contexts to combine and
 * conjugate APV clusters providing 1D measurement of track intersection point
 * into 2D point in DRS or 3D MRS.
 *
 * Assumes two-layered detector, each layer measuring one coordinate of the
 * hit. Layers define affine transform (see `util::Transformation`). This class
 * finds the POCA following geometrical assumptions:
 *
 *  * Each plane define a pair (\f$n=1,2\f$) of skew lines of the form:
 *       \f$[
 *          \vec{v}_n * m_n + \vec{r}_{0,n} = \vec{l}_n.
 *       \f$]
 *  * here \f$\vec{v}_n\f$ is the 2nd vector of local basis.
 *  * and \f$\vec{r}_{0,n} = m_n \vec{u}_n\f$ where \f$m_{1,2}\f$ -- normalized
 *    local (*measured*) coordinates.
 *
 * Then, mid (mutual POCA) for both \f$\vec{l}_{1,2}\f$ assumed to be a result
 * of two scores combination.
 *
 * \ingroup track-combining-hits
 * */
class TwoLayerAPVCombinationRule
        : public util::DSuLHitCombinationRule<2, event::APVCluster>
        , public util::Observable<calib::Placements>::iObserver
        {
public:
    typedef event::APVCluster HitType;
    static constexpr util::Arity_t arity=2;
    /// Structure keeping geometrical information, relevant to clusters
    /// conjugation
    struct PlacementsCache {
        unsigned int nWires;
        util::Transformation t;
        //double stride  ///< wires stride (cm per wire)
        //     , zero  ///< offset of center with respect to 0 wire
        //     ;
        //util::Vec3 u0  ///< measurement unit vector
        //         , v  ///< measured 1D hit line direction vector
        //         , c  ///< plane center
        //         ;
        //double resolution;  ///< resolution explicitly set for this plane
        /// Does not directly participate in calculus
        //double size[2];
        /// Converts wire units to position in cm
        //double wire_units_to_drs_position(float wireUnits) const;
    };
protected:
    /// Cache of placements object to perform conjugation/units transformation
    std::unordered_map<PlaneKey, PlacementsCache> _placementsCache;

    /// Returns number of set to put DetID with WireID payload specific for two
    /// layered detectors
    size_t _n_set(HitRef_t did) const override;
    /// Updates per-plane placements cache
    void handle_update(const calib::Placements &) override;
public:
    TwoLayerAPVCombinationRule( calib::Dispatcher & cdsp
            , const std::string & sel
            , log4cpp::Category & logCat
            , bool permitPartialCombinations
            , typename iPerStationHitCombinationRule<HitRef_t, 2>::GeneratorCtr dftCtr
                    =iPerStationHitCombinationRule<HitRef_t, 2>::construct_basic_cartesian_generator
            , const std::string &detNamingSubclass="default"
            ) : DSuLHitCombinationRule<2, event::APVCluster>(cdsp, sel, logCat
                , permitPartialCombinations, dftCtr, detNamingSubclass)
              {
        cdsp.subscribe<calib::Placements>(*this, "default");
    }
    ///\brief Sets score values using hits information
    void calc_score_values( event::TrackScore &
                          , const std::array<HitRef_t, 2> & ) override;
};  // class TwoLayerAPVCombinationRule

}  // namespace na64dp

