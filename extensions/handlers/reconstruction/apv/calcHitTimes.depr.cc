#include "na64dp/abstractHitHandler.hh"
#include "na64detID/wireID.hh"

#include <cmath>

#include <stdexcept>

namespace na64dp {
namespace calib {

//
// Define own calibration data types

struct MMCalibData {
    //const std::string planeName;
    double a02timeParam[4];
    double a02timeError[6];
    double a12timeParam[4];
    double a12timeError[6];
};

struct MMWireCalibData {
    //const std::string planeName;
    double chCalib[64];
};

struct GEMCalibData {
    //const std::string planeName;
    double a02timeParam[5];
    double a12timeParam[5]; 
};

typedef std::unordered_map<DetID, MMCalibData >             MMCalibs;
typedef std::unordered_map<DetID, MMWireCalibData>          MMWireCalibs;
typedef std::unordered_map<DetID, GEMCalibData>             GEMCalibs;

//
// Define own calibration loaders (based on CSV files)

bool
insert_mm_calib( nameutils::DetectorNaming & nm, MMCalibs & dest
               , std::string TBname, std::string aType
               , double, double, double, double
               , double, double, double, double
               ) {
    // ...
    return true;
}

bool
insert_mm_wire_calib( nameutils::DetectorNaming & nm, MMWireCalibs & dest
                    , std::string & TBname
                    , const std::array<double, 64> & data
                    ) {
    // ...
    return true;
}

bool
insert_gm_calib( nameutils::DetectorNaming & nm, GEMCalibs & dest
               , std::string & TBname
               , const std::array<double, 10> & data
               ) {
    // ...
    return true;
}

#if 0
class MMCalibCSVLoader : public iCSVFilesDataIndex< MMCalibData
                                                  , std::string, std::string
                                                  , double, double, double, double
                                                  , double, double, double, double
                                                  > {
public:
    /// Binds name handle to dispatcher
    MMCalibCSVLoader( Dispatcher & cdsp )
            : _naming("default", cdsp)
            , _log(log4cpp::Category::getInstance("calibrations.mm")) {}
    virtual void append_index_from_tuple( MMCalibData & dest
                                        , const Tuple & t ) override {
        dest[]
    }
};
#endif

}  // namespace ::na64dp::calib
namespace handlers {

using event::APVHit;

/**\brief Adds timing information to APV hit
 *
 * This handler uses special calibrations defined for APV-based detectors.
 * */
class APVCalcHitTimes : public AbstractHitHandler<APVHit> {
private:
    DetKin_t _gmKinID  ///< GEMs kin ID cache
           , _mmKinID  ///< MuMegas kin ID cache
           ;
    calib::Handle<calib::MMCalibs> _mmCalibs;
    calib::Handle<calib::MMWireCalibs> _mmWireCalibs;
    calib::Handle<calib::GEMCalibs> _gmCalibs;
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

    virtual double _get_hit_timeMM( DetID_t did , const APVHit & cHit, double & ratio, int mode) const;
    virtual double _get_hit_timeRise( DetID_t did , const APVHit & cHit, double & ratio, double time) const;
    virtual double _get_hit_sigmaMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const;
public:
    /// Default ctr
    APVCalcHitTimes( calib::Dispatcher & dsp, const std::string & select );
    virtual bool process_hit( EventID
                            , HitKey
                            , APVHit & ) override;
};

//                      * * *   * * *   * * *

APVCalcHitTimes::APVCalcHitTimes( calib::Dispatcher & cdsp
                                , const std::string & only
                                ) : AbstractHitHandler<APVHit>(cdsp, only)
                                  , _mmCalibs("default", cdsp)
                                  , _mmWireCalibs("default", cdsp)
                                  , _gmCalibs("default", cdsp)
                                  {}

void
APVCalcHitTimes::handle_update( const nameutils::DetectorNaming & nm ) {
    // Update `naming()` handle by forwarding call to parent class
    AbstractHitHandler<APVHit>::handle_update(nm);
    // Recache the GM/MM kin ids
    _gmKinID = nm.kin_id("GM").second;
    _mmKinID = nm.kin_id("MM").second;
    // (no need to cache/check for APV chip as this handler processes only APV
    // hits anyway)

    // Recache the calibrations
    #if 0
    for( long unsigned int i = 0; i < sizeof(gMMCalibs)/sizeof(MMCalibData); ++i ) {
        DetID_t did = nm.id(gMMCalibs[i].planeName);
        _mmCalibs.emplace(did, gMMCalibs + i); // TODO: Ask Renat why gMMCalibs + i
    }

    for( long unsigned int i = 0; i < sizeof(gMMWireCalibs)/sizeof(MMWireCalibData); ++i ) {
        DetID_t did = nm.id(gMMWireCalibs[i].planeName);
        _mmWireCalibs.emplace(did, gMMWireCalibs + i); // TODO: Ask Renat why gMMCalibs + i
    }
    
    for( long unsigned int i = 0; i < sizeof(gGEMCalibs)/sizeof(GEMCalibData); ++i ) {
        DetID_t did = nm.id(gGEMCalibs[i].planeName);
        _gemCalibs.emplace(did, gGEMCalibs + i);
    }
    #endif
}

bool
APVCalcHitTimes::process_hit( EventID
                            , HitKey did
                            , APVHit & cHit ) {
    if( ! cHit.rawData ) return true;  // no raw data in hit, continue processing
    // Get analog samples from APV chip
    const double & A0 = cHit.rawData->samples[0]
               , & A1 = cHit.rawData->samples[1]
               , & A2 = cHit.rawData->samples[2]
               ;
    double maxCharge(0), wCharge(0), charge(0), averageT(0);
    // Iterate over analog samples    
    for (int i = 0; i < 3; ++i) {
        const double q = cHit.rawData->samples[i];
        wCharge += q * (i);
        charge  += q;
        if (maxCharge < q) maxCharge = q;
    }
    
    cHit.maxCharge = maxCharge;
    averageT = wCharge / charge;
    cHit.averageT = averageT;
    
    // Calculate A ratios
    cHit.a02 = A0/A2;
    cHit.a12 = A1/A2;
    
    // obtain APV detector ID without wire number {{{
    
    DetID stationID(did);
    if ( _mmKinID == stationID.kin() ) {
        WireID pl(stationID.payload());
        pl.unset_wire_no();
        stationID.payload(pl.id);
        // }}}
        auto entryIt = _mmCalibs->find(stationID);
        assert(entryIt != _mmCalibs->end());
        const calib::MMCalibData & cMMCalibs = entryIt->second;
        if (A2 > 0) {
            // Calculate hit time ratios and errors
            if ( cMMCalibs.a02timeParam[0] > cHit.a02 && cHit.a02 != 0 ) {
                cHit.t02 = _get_hit_timeMM( did, cHit, cHit.a02, 0 );
                cHit.t02sigma = _get_hit_sigmaMM( did, cHit, cHit.a02, 0 );
            }
            if ( cMMCalibs.a12timeParam[0] > cHit.a12 && cHit.a12 != 0 ) {
                cHit.t12 = _get_hit_timeMM( did, cHit, cHit.a12, 1 );
                cHit.t12sigma = _get_hit_sigmaMM( did, cHit, cHit.a12, 1 );
            }
        }
    } else if ( _gmKinID == stationID.kin() ) {
        WireID pl(stationID.payload());
        pl.unset_wire_no();
        stationID.payload(pl.id);
        
        // }}}
        auto entryIt = _gmCalibs->find(stationID);
        assert(entryIt != _gmCalibs->end());
        const calib::GEMCalibData & cGEMCalibs = entryIt->second;

        // Reverse sign and add fixed constant in order to be
        // compatible with new time calculation
        const double tcsT0 = 40.;

        if (A2 > 0) {
            // Calculate hit time ratios and errors
            if ( cHit.a02 > 0 && ( cGEMCalibs.a02timeParam[4] / cHit.a02  >= 1 ) &&
                       cHit.a02 >= cGEMCalibs.a02timeParam[0] &&
                       cHit.a02 <= cGEMCalibs.a02timeParam[1] ) {
                
                cHit.t02 = cGEMCalibs.a02timeParam[2] +
                           cGEMCalibs.a02timeParam[3] *
                           ::log(cGEMCalibs.a02timeParam[4] / cHit.a02);
                                                 
                cHit.t02sigma = 17.;
                cHit.t02 = -1. * cHit.t02 + tcsT0; }
                
            if ( cHit.a12 > 0 && ( cGEMCalibs.a12timeParam[4] / cHit.a12  >= 1 ) &&
                       cHit.a12 >= cGEMCalibs.a12timeParam[0] &&
                       cHit.a12 <= cGEMCalibs.a12timeParam[1] ) {
                
                cHit.t12 = cGEMCalibs.a12timeParam[2] +
                           cGEMCalibs.a12timeParam[3] *
                           ::log(cGEMCalibs.a12timeParam[4] / cHit.a12);
                                                 
                cHit.t12sigma = 17.;
                cHit.t12 = -1. * cHit.t12 + tcsT0; }
        }
        else { _set_hit_erase_flag(); }
    }
    
    if ( cHit.t02 != 0 && cHit.t12 != 0 ) {
        cHit.time = ( cHit.t02 / pow(cHit.t02sigma,2) )
                             + ( cHit.t12 / pow(cHit.t12sigma,2) );
        cHit.timeError = ( 1 / pow(cHit.t02sigma,2) )
                             + ( 1 / pow(cHit.t12sigma,2) );
        cHit.time = cHit.time / cHit.timeError;
        cHit.timeError = ( 1 / sqrt( cHit.timeError ) );
        if (cHit.timeError > 100 ) { _set_hit_erase_flag(); }
    } else { _set_hit_erase_flag(); }
    if ( _mmKinID == stationID.kin() ) {
        // TEMPORARY FUNCTION TO CALCULATE HIT TIME RISE 
        cHit.timeRise = _get_hit_timeRise( did, cHit, cHit.a02, cHit.time );
    }
    
    return true;
}

double
APVCalcHitTimes::_get_hit_timeMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const {
    
    // obtain APV detector ID without wire number {{{
    DetID stationID(did);
    WireID pl(stationID.payload());
    pl.unset_wire_no();
    stationID.payload(pl.id);
    // }}}
    auto entryIt = _mmCalibs->find(stationID);
    assert(entryIt != _mmCalibs->end());
    const calib::MMCalibData & cMMCalibs = entryIt->second;
    
    double r0(0), t0(0), a0(0);
    
    if (mode == 0) {
        r0 = cMMCalibs.a02timeParam[0];
        t0 = cMMCalibs.a02timeParam[1];
        a0 = cMMCalibs.a02timeParam[2]; 
    } else if (mode == 1) {
        r0 = cMMCalibs.a12timeParam[0];
        t0 = cMMCalibs.a12timeParam[1];
        a0 = cMMCalibs.a12timeParam[2]; 
    }
    
    return t0 + a0 * ::log((r0 / ratio) - 1); 
}

// TEMPORARY FUNCTION
double
APVCalcHitTimes::_get_hit_timeRise( DetID_t did , const APVHit & cHit, double & ratio, double time ) const {
    
    // obtain APV detector ID without wire number {{{
    DetID stationID(did);
    WireID pl(stationID.payload());
    pl.unset_wire_no();
    stationID.payload(pl.id);
    // }}}
    auto entryIt = _mmCalibs->find(stationID);
    assert(entryIt != _mmCalibs->end());
    const calib::MMCalibData & cMMCalibs = entryIt->second;
    
    double r0(0), t0(0);  // a0(0) unused
    
    r0 = cMMCalibs.a02timeParam[0];
    t0 = cMMCalibs.a02timeParam[1];
    //a0 = cMMCalibs->a02timeParam[2];  // unused
    
    return (time - t0) / ::log((r0 / ratio) - 1); 
}

double
APVCalcHitTimes::_get_hit_sigmaMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const {
    
    // obtain APV detector ID without wire number {{{
    DetID stationID(did);
    WireID pl(stationID.payload());
    pl.unset_wire_no();
    stationID.payload(pl.id);
    // }}}
    auto entryIt1 = _mmCalibs->find(stationID);
    assert(entryIt1 != _mmCalibs->end());
    const calib::MMCalibData & cMMCalibs = entryIt1->second;
    
    auto entryIt2 = _mmWireCalibs->find(stationID);
    assert(entryIt2 != _mmWireCalibs->end());
    const calib::MMWireCalibData & cMMWireCalibs = entryIt2->second;
    
    double r0(0), a0(0);
    double r02(0), t02(0), a02(0), r0t0(0), r0a0(0), t0a0(0);
    double sigmaN(0), sigmaR(0), sigmaT(0), dtdr(0), dtda0(0), dtdt0(0), dtdr0(0);
    double A(0), B(0);
    
    // TODO: Check wire number type
    auto wireNo = cHit.rawData->wireNo;
    
    sigmaN = cMMWireCalibs.chCalib[wireNo]; 
    
    if (mode == 0) {
        r0 = cMMCalibs.a02timeParam[0]; 
        a0 = cMMCalibs.a02timeParam[2];
        
        r02 = cMMCalibs.a02timeError[0]; t02 = cMMCalibs.a02timeError[1];
        a02 = cMMCalibs.a02timeError[2]; r0t0 = cMMCalibs.a02timeError[3];
        r0a0 = cMMCalibs.a02timeError[4]; t0a0 = cMMCalibs.a02timeError[5];
        
        A = cHit.rawData->samples[0];
        B = cHit.rawData->samples[2];
                
    } else if (mode == 1) {
        r0 = cMMCalibs.a12timeParam[0];
        a0 = cMMCalibs.a12timeParam[2];

        r02 = cMMCalibs.a12timeError[0]; t02 = cMMCalibs.a12timeError[1];
        a02 = cMMCalibs.a12timeError[2]; r0t0 = cMMCalibs.a12timeError[3];
        r0a0 = cMMCalibs.a12timeError[4]; t0a0 = cMMCalibs.a12timeError[5];
        
        A = cHit.rawData->samples[1];
        B = cHit.rawData->samples[2];
    }

    sigmaR = ( sigmaN + (1 / sqrt(12) ) ) *
             ( ( sqrt(pow(A,2) + pow(B,2)) ) / pow(B,2) );
             
    dtdr =   ( a0 * r0 ) /
             pow(ratio,2) * ( r0 / ratio - 1 );
             
    dtda0 = ::log(( r0 / ratio )-1);
    
    dtdt0 = 1;
    
    dtdr0 = a0 / ( ratio * ( r0 / ratio - 1 ) );
    
    sigmaT = sqrt( ( pow(dtdr,2) * pow(sigmaR,2) ) + ( pow(dtdr0,2) * r02 )
                 + ( pow(dtdt0,2) * t02 ) + ( pow(dtda0,2) * a02 ) 
                 + 2 * ( dtdt0 * dtdr0 * r0t0 ) + 2 * ( dtda0 * dtdr0 * r0a0 )
                 + 2 * ( dtda0 * dtdt0 * t0a0 ) );
    
    return sigmaT;
}

}

REGISTER_HANDLER( APVCalcHitTimes, ch, cfg
                , "Calculates time and errors for APV-based detectors" ) {

    ch.add_simple_data_type_from_csv<calib::MMCalibs>( calib::insert_mm_calib );
    ch.add_array_data_type_from_csv<calib::MMWireCalibs>( calib::insert_mm_wire_calib );
    ch.add_array_data_type_from_csv<calib::GEMCalibs>( calib::insert_gm_calib );

    //ch.add_simple_data_type<calib::MMCalibData, calib::CSVReaderMult<64> >(
    //        convert_line<calib::> );
    // This handler involves its own custom type(s) of calibrations that is (are)
    // not used by any handlers outside => let's register index for these
    // data type
    return new handlers::APVCalcHitTimes(ch, aux::retrieve_det_selection(cfg));
}

}

