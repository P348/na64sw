#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Base abstract class accumulating ambiguous handlers.
 *
 * By "ambiguous clusters" we imply (a couple or more) clusters that share same
 * physical wire (glvanically). This scheme was designed deliberately in NA64
 * to decrease amount of readout electronics assuming relatively low rate of
 * tracking detectors (MuMegas).
 *
 * This class is not an instantiable handler itself, but serves as a base class
 * for some handlers performing certain operations on the set of ambiguous
 * clusters
 *
 * */
class CollectAmbigClusters : public AbstractHitHandler<event::APVCluster> {
public:
    /// Internal type for clusters references sorted w.r.t. DAQ channel ID
    typedef std::unordered_multimap< event::APVDAQWire_t
                                   , mem::Ref<event::APVCluster>
                                   > ClustersByWire;
private:
    /// Keeps temporary indexes created for an event
    std::unordered_map<PlaneKey, ClustersByWire> _idx;

    /// Fills cluster-by channel number cache.
    void _add_cluster( HitKey did, mem::Ref<event::APVCluster> cluster );
protected:
    /// This method shall perform treatment of ambiguous clusters
    virtual void _handle_clusters( std::unordered_map<PlaneKey, ClustersByWire> &
                                 , event::Event &
                                 ) = 0;
public:
    CollectAmbigClusters( calib::Dispatcher & ch
                        , const std::string & only
                        );
    /// At the end of event processing removes clusters being marked for
    /// deletion.
    virtual AbstractHandler::ProcRes process_event( event::Event & ) override;
    /// A stub for abstract method; should never be called.
    virtual bool process_hit( EventID
                            , PlaneKey did
                            , event::APVCluster & cHit ) override;
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

