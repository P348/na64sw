#include "na64dp/abstractHitHandler.hh"
#include <cmath>

namespace na64dp {
namespace handlers {

using event::APVHit;

/**\brief Calculates ratios \f$r_{1,2}\f$ and \f$r_{1,3}\f$ for hits on APV detector.
 *
 * A-ratios are the useful coefficients for APV hits data quality assurance.
 * $$
 * r_{1,2} = a_1/a_2, r_{1,3} = a_1/a_3
 * $$
 *
 * \image html handlers/apv-a-ratio.png
 *
 * where \f$a_i, i = 1, 2, 3\f$ is the amplitude samples usually brought by APV
 * chip.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: APVCalcARatios
 *       # optional, hit selection expression
 *       applyTo: null
 * \endcode
 *
 * This handler has no parameters except for usual optional `applyTo` detector
 * selection.
 *
 * \ingroup handlers apv-handlers
 */
class APVCalcARatios : public AbstractHitHandler<APVHit> {
public:
    /// Default ctr
    APVCalcARatios( calib::Dispatcher & ch
                  , const std::string & select
                  , log4cpp::Category & logCat
                  , const std::string & namingClass="default"
                  );
    /// Writes ratios to `APVHit::a12` and `APVHit::a13`
    virtual bool process_hit( EventID
                            , HitKey
                            , APVHit & cHit );
};

//                      * * *   * * *   * * *

APVCalcARatios::APVCalcARatios( calib::Dispatcher & ch
                              , const std::string & only
                              , log4cpp::Category & logCat
                              , const std::string & namingClass
                              ) : AbstractHitHandler<APVHit>(ch, only, logCat, namingClass) {}

/** If $A_2$ is null or NaN, the `APVHit::a12` will be left intact.
 *  If $A_3$ is null or NaN, the `APVHit::a13` will be left intact.
 *
 * \returns always `true`.
 */
bool
APVCalcARatios::process_hit( EventID
                           , HitKey
                           , APVHit & cHit ) {
    if( ! cHit.rawData ) return true;  // no raw data, continue processing
    const double A0 = cHit.rawData->samples[0]
               , A1 = cHit.rawData->samples[1]
               , A2 = cHit.rawData->samples[2]
               ;
    if( A0 && ! std::isnan(A2) ) cHit.a02 = A0/A2;
    if( A1 && ! std::isnan(A2) ) cHit.a12 = A1/A2;
    return true;
}

}

REGISTER_HANDLER( APVCalcARatios, ch, cfg
                , "Calculates a12 and a23 ratios for APV-based detectors" ) {
    return new handlers::APVCalcARatios( ch
                                       , aux::retrieve_det_selection(cfg)
                                       , aux::get_logging_cat(cfg)
                                       , aux::get_naming_class(cfg)
                                       );
}

}

