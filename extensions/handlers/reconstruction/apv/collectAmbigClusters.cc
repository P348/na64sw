#include "collectAmbigClusters.hh"

namespace na64dp {
namespace handlers {

using na64dp::event::APVCluster;
using na64dp::event::Traits;
using na64dp::event::Event;


CollectAmbigClusters::CollectAmbigClusters( calib::Dispatcher & ch
                                        , const std::string & detSelection
                                        )
        : AbstractHitHandler<APVCluster>(ch, detSelection) {}

bool
CollectAmbigClusters::process_hit( EventID
                                , HitKey did
                                , APVCluster & cluster ) {
    assert(false);  // this stub must never be called
}

void
CollectAmbigClusters::_add_cluster( HitKey did
                                 , mem::Ref<APVCluster> cluster ) {
    // Retrieve the index by detector ID
    auto idxIt = _idx.find( did );
    if( _idx.end() == idxIt ) {
        auto ir = _idx.emplace( did, ClustersByWire() );
        assert( ir.second );
        idxIt = ir.first;
    }
    ClustersByWire & clustersByChannel = idxIt->second;
    // Add cluster reference by physical channel
    for( auto hitPair : cluster->hits ) {
        NA64DP_ASSERT_EVENT_DATA( hitPair.second->rawData,
                "Raw APV hit in cluster." );
        clustersByChannel.emplace( hitPair.second->rawData->wireNo
                , cluster );
    }
}

AbstractHandler::ProcRes
CollectAmbigClusters::process_event( Event & ev ) {
    // Accumulate wire-to-cluster map (event's cache)
    for( auto p : ev.apvClusters ) {
        if( is_selective() && ! matches(p.first) ) continue;
        _add_cluster( p.first, p.second );
    }
    _handle_clusters(_idx, ev);
    _idx.clear();
    return kOk;
}

/*
 * Usage:
 * \code{.yaml}
 *     - _type: CollectAmbigClusters
 *       # optional, hit selection expression
 *       applyTo: null
 * \endcode
 *
 * For APV detectors occupying the same DAQ channels (multiwired, multiplexed)
 * assures that clusters being present occupies the unique set of DAQ channels.
 *
 * Disambiguity is resolved by removing the clusters with respect to some
 * criterion (charge sum).
 *
 * \note For the time being, only MMs exploits the multiplexing scheme, so
 *       handler is appliable only to MMs, in fact.
 * \ingroup handlers apv-cluster-handlers
 */

}  // namespace ::na64dp::handlers

#if 0
REGISTER_HANDLER( CollectAmbigClusters, ch, cfg
                , "Removes ambigiuos APV clusters from an event leaving"
                  " one(s) leading by some value." ) {
    std::string sortBy = cfg["sortBy"] ? cfg["sortBy"].as<std::string>() : "charge";
    bool leaveGreatest = cfg["leaveGreatest"] ? cfg["leaveGratest"].as<bool>() : true;
    return new handlers::CollectAmbigClusters( ch
                        , aux::retrieve_det_selection(cfg)
                        , util::value_getter<event::APVCluster>(sortBy)
                        , leaveGreatest
                        , cfg["leaveN"] ? cfg["leaveN"].as<int>() : 1
                        );
}
#endif

}

