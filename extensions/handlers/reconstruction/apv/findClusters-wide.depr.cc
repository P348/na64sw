#include "na64dp/abstractHitHandler.hh"
//#include "na64util/mm-layout.h"
#include "na64util/str-fmt.hh"
#include "na64calib/manager-utils.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

using event::Event;
using event::APVHit;
using event::APVCluster;
using event::APVDAQWire_t;
using event::APVPhysWire_t;

/**\brief Finds hit clusters on the APV detectors planes
 *
 * Ad-hoc handler, to be removed once dynamic calibrations appear.
 * Same as APVFindCluster, but performs clustering of wider MuMegas as well.
 *
 * \note deprecated handler, to be deleted
 * \todo delete it in favor of handler based on generic calibrations
 * \ingroup handlers apv-cluster-handlers
 * */
class APVFindClusterWide : public AbstractHitHandler<APVHit> {
private:
    const float _minClusterWidth  ///< minimum cluster width requirement in wire units
              , _maxSparseness  ///< maximum number of wires missed within a cluster
              , _maxTimeDiscrepancy;

    DetKin_t _mmKinID  ///< MuMegas kin ID cache
           ;
    std::vector< std::array<int, 5> > _design;
protected:
    /// Storage for hits IDs sorted by a wire number, for single plane
    typedef std::map< APVDAQWire_t, DetID > SortedHits;

    /// Hits by plane, accumulated for the event
    ///
    /// \todo use a custom allocator for performance
    std::unordered_map< PlaneKey, SortedHits> _byPlaneCaches;

    /// Upon naming change, retrieves values for MM and GM kin IDs
    virtual void handle_update(const nameutils::DetectorNaming &) override;
    /// Discovers clusters on the planes
    virtual size_t _find_clusters( Event &, PlaneKey, const SortedHits & );
    /// Returns true if provided hit seems to be part of cluster candidate
    virtual bool _possibly_belongs_to_cluster( const APVCluster &
                                             , APVPhysWire_t physWireNo
                                             , const APVHit & ) const;
    /// Adds certain hit to the cluster -- appends hits collection and
    /// increments (weighted) sums
    virtual void _add_hit_to_cluster( APVCluster &, APVPhysWire_t, mem::Ref<APVHit> );
    /// Finalizes cluster by calculating weighted sums and pushes it to the
    /// event's collection
    virtual void _add_cluster_to_event( Event &, PlaneKey, mem::Ref<APVCluster> );
public:
    APVFindClusterWide( calib::Dispatcher & ch
                   , const std::string & only
                   , const std::vector< std::array<int, 5> > design_
                   , float clusterMinWidth=1.
                   , float timeThreshold=std::nan("0")
                   , float sparseness=0.
                   , const std::string & mmLayoutSubtype="default"
                   );
    /// Forwards execution to parent to iterate over hits and collect them into
    /// sorted by-wire containers. Once all hits are collected, forwards
    /// execution to `_find_clusters()`.
    virtual AbstractHandler::ProcRes process_event( Event & e ) override;
    /// Fills sorted hits caches by plane ID (`_byPlaneCaches`)
    virtual bool process_hit( EventID, DetID, APVHit & ) override;
};

//                      * * *   * * *   * * *

APVFindClusterWide::APVFindClusterWide( calib::Dispatcher & ch
                                , const std::string & detSelection
                                , const std::vector< std::array<int, 5> > design_
                                , float clusterMinWidth
                                , float sparseness
                                , float timeThreshold
                                , const std::string & mmLayoutSubtype
                                ) : AbstractHitHandler(ch, detSelection)
                                  , _minClusterWidth(clusterMinWidth)
                                  , _maxSparseness(sparseness)
                                  , _maxTimeDiscrepancy(timeThreshold)
                                  , _design(design_)
                                  {
    if(_minClusterWidth < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"minWidth\" value provided to"
                " APVClusterAssemble handler." );
    }
    if(_maxSparseness < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"sparseness\" value provided to"
                " APVFindClusterWide handler." );
    }
}

static const int wireMap_64_m5[][6] = {
    //#0:channel #1-N: strips
    {  0,  0,  64, 128, 192, 256},
    {  1,  1, 119, 133, 219, 297},
    {  2,  2, 110, 138, 246, 274},
    {  3,  3, 101, 143, 209, 315},
    {  4,  4,  92, 148, 236, 292},
    {  5,  5,  83, 153, 199, 269},
    {  6,  6,  74, 158, 226, 310},
    {  7,  7,  65, 163, 253, 287},
    {  8,  8, 120, 168, 216, 264},
    {  9,  9, 111, 173, 243, 305},
    { 10, 10, 102, 178, 206, 282},
    { 11, 11,  93, 183, 233, 259},
    { 12, 12,  84, 188, 196, 300},
    { 13, 13,  75, 129, 223, 277},
    { 14, 14,  66, 134, 250, 318},
    { 15, 15, 121, 139, 213, 295},
    { 16, 16, 112, 144, 240, 272},
    { 17, 17, 103, 149, 203, 313},
    { 18, 18,  94, 154, 230, 290},
    { 19, 19,  85, 159, 193, 267},
    { 20, 20,  76, 164, 220, 308},
    { 21, 21,  67, 169, 247, 285},
    { 22, 22, 122, 174, 210, 262},
    { 23, 23, 113, 179, 237, 303},
    { 24, 24, 104, 184, 200, 280},
    { 25, 25,  95, 189, 227, 257},
    { 26, 26,  86, 130, 254, 298},
    { 27, 27,  77, 135, 217, 275},
    { 28, 28,  68, 140, 244, 316},
    { 29, 29, 123, 145, 207, 293},
    { 30, 30, 114, 150, 234, 270},
    { 31, 31, 105, 155, 197, 311},
    { 32, 32,  96, 160, 224, 288},
    { 33, 33,  87, 165, 251, 265},
    { 34, 34,  78, 170, 214, 306},
    { 35, 35,  69, 175, 241, 283},
    { 36, 36, 124, 180, 204, 260},
    { 37, 37, 115, 185, 231, 301},
    { 38, 38, 106, 190, 194, 278},
    { 39, 39,  97, 131, 221, 319},
    { 40, 40,  88, 136, 248, 296},
    { 41, 41,  79, 141, 211, 273},
    { 42, 42,  70, 146, 238, 314},
    { 43, 43, 125, 151, 201, 291},
    { 44, 44, 116, 156, 228, 268},
    { 45, 45, 107, 161, 255, 309},
    { 46, 46,  98, 166, 218, 286},
    { 47, 47,  89, 171, 245, 263},
    { 48, 48,  80, 176, 208, 304},
    { 49, 49,  71, 181, 235, 281},
    { 50, 50, 126, 186, 198, 258},
    { 51, 51, 117, 191, 225, 299},
    { 52, 52, 108, 132, 252, 276},
    { 53, 53,  99, 137, 215, 317},
    { 54, 54,  90, 142, 242, 294},
    { 55, 55,  81, 147, 205, 271},
    { 56, 56,  72, 152, 232, 312},
    { 57, 57, 127, 157, 195, 289},
    { 58, 58, 118, 162, 222, 266},
    { 59, 59, 109, 167, 249, 307},
    { 60, 60, 100, 172, 212, 284},
    { 61, 61,  91, 177, 239, 261},
    { 62, 62,  82, 182, 202, 302},
    { 63, 63,  73, 187, 229, 279},
};

static const int wireMap_64_m5_new[][6] = {
    //0:channel #1-N: strips
    { 0,0,64,128,192,256},
    { 1,1,69,181,241,317},
    { 2,2,74,170,226,314},
    { 3,3,79,159,211,311},
    { 4,4,84,148,196,308},
    { 5,5,89,137,245,305},
    { 6,6,94,190,230,302},
    { 7,7,99,179,215,299},
    { 8,8,104,168,200,296},
    { 9,9,109,157,249,293},
    { 10,10,114,146,234,290},
    { 11,11,119,135,219,287},
    { 12,12,124,188,204,284},
    { 13,13,65,177,253,281},
    { 14,14,70,166,238,278},
    { 15,15,75,155,223,275},
    { 16,16,80,144,208,272},
    { 17,17,85,133,193,269},
    { 18,18,90,186,242,266},
    { 19,19,95,175,227,263},
    { 20,20,100,164,212,260},
    { 21,21,105,153,197,257},
    { 22,22,110,142,246,318},
    { 23,23,115,131,231,315},
    { 24,24,120,184,216,312},
    { 25,25,125,173,201,309},
    { 26,26,66,162,250,306},
    { 27,27,71,151,235,303},
    { 28,28,76,140,220,300},
    { 29,29,81,129,205,297},
    { 30,30,86,182,254,294},
    { 31,31,91,171,239,291},
    { 32,32,96,160,224,288},
    { 33,33,101,149,209,285},
    { 34,34,106,138,194,282},
    { 35,35,111,191,243,279},
    { 36,36,116,180,228,276},
    { 37,37,121,169,213,273},
    { 38,38,126,158,198,270},
    { 39,39,67,147,247,267},
    { 40,40,72,136,232,264},
    { 41,41,77,189,217,261},
    { 42,42,82,178,202,258},
    { 43,43,87,167,251,319},
    { 44,44,92,156,236,316},
    { 45,45,97,145,221,313},
    { 46,46,102,134,206,310},
    { 47,47,107,187,255,307},
    { 48,48,112,176,240,304},
    { 49,49,117,165,225,301},
    { 50,50,122,154,210,298},
    { 51,51,127,143,195,295},
    { 52,52,68,132,244,292},
    { 53,53,73,185,229,289},
    { 54,54,78,174,214,286},
    { 55,55,83,163,199,283},
    { 56,56,88,152,248,280},
    { 57,57,93,141,233,277},
    { 58,58,98,130,218,274},
    { 59,59,103,183,203,271},
    { 60,60,108,172,252,268},
    { 61,61,113,161,237,265},
    { 62,62,118,150,222,262},
    { 63,63,123,139,207,259},
};

static const int wireMap_192_m5[][6] = {
    // 0:channel #1-N: strips
    {0,0,192,384,576,768},
    {1,1,269,551,643,911},
    {2,2,346,526,710,862},
    {3,3,231,501,585,813},
    {4,4,308,476,652,956},
    {5,5,193,451,719,907},
    {6,6,270,426,594,858},
    {7,7,347,401,661,809},
    {8,8,232,568,728,952},
    {9,9,309,543,603,903},
    {10,10,194,518,670,854},
    {11,11,271,493,737,805},
    {12,12,348,468,612,948},
    {13,13,233,443,679,899},
    {14,14,310,418,746,850},
    {15,15,195,393,621,801},
    {16,16,272,560,688,944},
    {17,17,349,535,755,895},
    {18,18,234,510,630,846},
    {19,19,311,485,697,797},
    {20,20,196,460,764,940},
    {21,21,273,435,639,891},
    {22,22,350,410,706,842},
    {23,23,235,385,581,793},
    {24,24,312,552,648,936},
    {25,25,197,527,715,887},
    {26,26,274,502,590,838},
    {27,27,351,477,657,789},
    {28,28,236,452,724,932},
    {29,29,313,427,599,883},
    {30,30,198,402,666,834},
    {31,31,275,569,733,785},
    {32,32,352,544,608,928},
    {33,33,237,519,675,879},
    {34,34,314,494,742,830},
    {35,35,199,469,617,781},
    {36,36,276,444,684,924},
    {37,37,353,419,751,875},
    {38,38,238,394,626,826},
    {39,39,315,561,693,777},
    {40,40,200,536,760,920},
    {41,41,277,511,635,871},
    {42,42,354,486,702,822},
    {43,43,239,461,577,773},
    {44,44,316,436,644,916},
    {45,45,201,411,711,867},
    {46,46,278,386,586,818},
    {47,47,355,553,653,769},
    {48,48,240,528,720,912},
    {49,49,317,503,595,863},
    {50,50,202,478,662,814},
    {51,51,279,453,729,957},
    {52,52,356,428,604,908},
    {53,53,241,403,671,859},
    {54,54,318,570,738,810},
    {55,55,203,545,613,953},
    {56,56,280,520,680,904},
    {57,57,357,495,747,855},
    {58,58,242,470,622,806},
    {59,59,319,445,689,949},
    {60,60,204,420,756,900},
    {61,61,281,395,631,851},
    {62,62,358,562,698,802},
    {63,63,243,537,765,945},
    {64,64,320,512,640,896},
    {65,65,205,487,707,847},
    {66,66,282,462,582,798},
    {67,67,359,437,649,941},
    {68,68,244,412,716,892},
    {69,69,321,387,591,843},
    {70,70,206,554,658,794},
    {71,71,283,529,725,937},
    {72,72,360,504,600,888},
    {73,73,245,479,667,839},
    {74,74,322,454,734,790},
    {75,75,207,429,609,933},
    {76,76,284,404,676,884},
    {77,77,361,571,743,835},
    {78,78,246,546,618,786},
    {79,79,323,521,685,929},
    {80,80,208,496,752,880},
    {81,81,285,471,627,831},
    {82,82,362,446,694,782},
    {83,83,247,421,761,925},
    {84,84,324,396,636,876},
    {85,85,209,563,703,827},
    {86,86,286,538,578,778},
    {87,87,363,513,645,921},
    {88,88,248,488,712,872},
    {89,89,325,463,587,823},
    {90,90,210,438,654,774},
    {91,91,287,413,721,917},
    {92,92,364,388,596,868},
    {93,93,249,555,663,819},
    {94,94,326,530,730,770},
    {95,95,211,505,605,913},
    {96,96,288,480,672,864},
    {97,97,365,455,739,815},
    {98,98,250,430,614,958},
    {99,99,327,405,681,909},
    {100,100,212,572,748,860},
    {101,101,289,547,623,811},
    {102,102,366,522,690,954},
    {103,103,251,497,757,905},
    {104,104,328,472,632,856},
    {105,105,213,447,699,807},
    {106,106,290,422,766,950},
    {107,107,367,397,641,901},
    {108,108,252,564,708,852},
    {109,109,329,539,583,803},
    {110,110,214,514,650,946},
    {111,111,291,489,717,897},
    {112,112,368,464,592,848},
    {113,113,253,439,659,799},
    {114,114,330,414,726,942},
    {115,115,215,389,601,893},
    {116,116,292,556,668,844},
    {117,117,369,531,735,795},
    {118,118,254,506,610,938},
    {119,119,331,481,677,889},
    {120,120,216,456,744,840},
    {121,121,293,431,619,791},
    {122,122,370,406,686,934},
    {123,123,255,573,753,885},
    {124,124,332,548,628,836},
    {125,125,217,523,695,787},
    {126,126,294,498,762,930},
    {127,127,371,473,637,881},
    {128,128,256,448,704,832},
    {129,129,333,423,579,783},
    {130,130,218,398,646,926},
    {131,131,295,565,713,877},
    {132,132,372,540,588,828},
    {133,133,257,515,655,779},
    {134,134,334,490,722,922},
    {135,135,219,465,597,873},
    {136,136,296,440,664,824},
    {137,137,373,415,731,775},
    {138,138,258,390,606,918},
    {139,139,335,557,673,869},
    {140,140,220,532,740,820},
    {141,141,297,507,615,771},
    {142,142,374,482,682,914},
    {143,143,259,457,749,865},
    {144,144,336,432,624,816},
    {145,145,221,407,691,959},
    {146,146,298,574,758,910},
    {147,147,375,549,633,861},
    {148,148,260,524,700,812},
    {149,149,337,499,767,955},
    {150,150,222,474,642,906},
    {151,151,299,449,709,857},
    {152,152,376,424,584,808},
    {153,153,261,399,651,951},
    {154,154,338,566,718,902},
    {155,155,223,541,593,853},
    {156,156,300,516,660,804},
    {157,157,377,491,727,947},
    {158,158,262,466,602,898},
    {159,159,339,441,669,849},
    {160,160,224,416,736,800},
    {161,161,301,391,611,943},
    {162,162,378,558,678,894},
    {163,163,263,533,745,845},
    {164,164,340,508,620,796},
    {165,165,225,483,687,939},
    {166,166,302,458,754,890},
    {167,167,379,433,629,841},
    {168,168,264,408,696,792},
    {169,169,341,575,763,935},
    {170,170,226,550,638,886},
    {171,171,303,525,705,837},
    {172,172,380,500,580,788},
    {173,173,265,475,647,931},
    {174,174,342,450,714,882},
    {175,175,227,425,589,833},
    {176,176,304,400,656,784},
    {177,177,381,567,723,927},
    {178,178,266,542,598,878},
    {179,179,343,517,665,829},
    {180,180,228,492,732,780},
    {181,181,305,467,607,923},
    {182,182,382,442,674,874},
    {183,183,267,417,741,825},
    {184,184,344,392,616,776},
    {185,185,229,559,683,919},
    {186,186,306,534,750,870},
    {187,187,383,509,625,821},
    {188,188,268,484,692,772},
    {189,189,345,459,759,915},
    {190,190,230,434,634,866},
    {191,191,307,409,701,817},
};

void
APVFindClusterWide::handle_update(const nameutils::DetectorNaming & nm) {
    AbstractHitHandler<APVHit>::handle_update(nm);
    try {
        _mmKinID = nm.kin_id("MM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find Micromega kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
}


AbstractHandler::ProcRes
APVFindClusterWide::process_event( event::Event & e ) {
    AbstractHandler::ProcRes pr = AbstractHitHandler<APVHit>::process_event( e );
    // for each plane: find clusters
    for( auto planePair : _byPlaneCaches ) {
        _find_clusters( e, planePair.first, planePair.second );
    }
    // clear hits cache
    _byPlaneCaches.clear();
    return pr;
}

bool
APVFindClusterWide::process_hit( EventID
                            , DetID did
                            , APVHit & cHit ) {
    // iDid -- incomplete detector ID; for APV detectors this will be 
    // defined with chip, kin, station number and projection ID. No wire number
    // is written within iDid
    PlaneKey pk(did);
    auto cIt = _byPlaneCaches.find(pk);
    if( _byPlaneCaches.end() == cIt ) {
        // No cache entry for this detector plane found -- create one
        cIt = _byPlaneCaches.emplace( pk, SortedHits() ).first;
    }
    //auto wIt = cIt->second.find( cHit.rawData.wireNo );
    auto wIt = cIt->second.lower_bound( cHit.rawData->wireNo );
    if( cIt->second.end() != wIt && wIt->first == cHit.rawData->wireNo ) {
        // Hit on this plane with the same wire number already exists at the
        // current event. This should be interpreted as an error since it is
        // technically impossible. Did you forget to clear the cache between
        // events?
        // Note that this "wire number" is not yet a phyiscal one, so we are
        // safe for MuMega detectors that have multiple physical wire connected
        // to single APV chip channel.
        NA64DP_RUNTIME_ERROR( "Multiple APV hits on the same wire." );
    }
    // Add full hit ID to the by-wire index
    cIt->second.emplace_hint( wIt, cHit.rawData->wireNo, did );
    return true;
}

size_t
APVFindClusterWide::_find_clusters( Event & e
                                  , PlaneKey pk
                                  , const SortedHits & hitsMap ) {
    // whether this plane requires a special treatment (multiple physical wires
    // connected to a single DAQ channel)
    const bool isMuMega = _mmKinID == pk.kin();
    // map of possibly duplicating hits
    // TODO: pool allocator for performance
    std::map<APVPhysWire_t, mem::Ref<APVHit> > dh;
    //std::cout << naming()[pk] << std::endl;  // XXX
    // Fill hits map (possibly duplicating)
    for( auto hitPair : hitsMap ) {
        const DetID & did = hitPair.second;
        // get ptr to hit
        mem::Ref<APVHit> hitPtr = e.apvHits[did];
        // get the DAQ wire number of this hit
        const uint16_t rawWireNo = hitPair.first;  // "virtual" (DAQ) wire no
        assert(rawWireNo == did.payload_as<WireID>()->wire_no());  // assure DAQ wire is the same
        if(isMuMega) {  // TODO
            // for MuMega x64 design -- add 5 hits to the reverse index
            if( rawWireNo >= _design.size() ) {
                NA64DP_RUNTIME_ERROR( "MuMega plane \"%s\" has layout of"
                        " %zu wires while acquired hit is on wire %d"
                        , naming()[pk].c_str()
                        , _design.size()
                        , (int) rawWireNo
                        );
            }
            const auto & chs = _design[rawWireNo];
            //std::cout << rawWireNo << " <- ";  // XXX
            for(auto ch : chs) {
                auto ir = dh.emplace( ch, hitPtr );
                //std::cout << ch << " ";  // XXX
                //std::cout.flush();  // XXX
                if( ! ir.second ) {
                    NA64DP_RUNTIME_ERROR( "MuMega reverse mapping failure:"
                            " DAQ wire %d corresponds to %d and %d."
                            , (int) rawWireNo
                            , (int) ch
                            , (int) ir.first->first
                            );
                }
                assert(ir.second);
            }
            //std::cout << std::endl;  // XXX
            continue; // do not continue with standard scheme
        }
        // standard layout (no multiplexing)
        auto ir = dh.emplace( rawWireNo, hitPtr );
        assert(ir.second);
    }
    // Find clusters, relying on `_possibly_belongs_to_cluster()` result.
    // Note that within `dh', the hits are sorted by their real wire number

    #if 0
    // Create new cluster instance to fill
    mem::Ref<APVCluster> c = lmem().create<APVCluster>(lmem());
    util::reset(*c);
    size_t nClustersFound = 0;
    // Iterate over hits sorted by wire number
    for( auto dHitPair : dh ) {
        NA64DP_APVStripNo_t physWireNo = dHitPair.first;
        mem::Ref<APVHit> hitPtr = dHitPair.second;
        // if hit belongs to current cluster, add and continue
        if ( _possibly_belongs_to_cluster(*c, physWireNo, *hitPtr) ) {
            _add_hit_to_cluster( *c, physWireNo, hitPtr );
        } else if( c->size() >= _minClusterWidth ) {
            // Cluster assembly is done, its size is ge _minClusterWidth
            // => finalize the current cluster [and create new adding next]
            _add_cluster_to_event(e, pk, c);
            nClustersFound++;
        } else {
            // cluster building is interrupted and its size is < _minClusterWidth
            assert(c->size() < _minClusterWidth);
            util::reset(*c);
        }
    }
    // loop above may end up on the last wire without finalizing non-empty cluster
    if(!c->empty() && c->size() >= _minClusterWidth ) {
        _add_cluster_to_event(e, pk, c);
        nClustersFound++;
    }
    #else
    // Following algorithm below:
    //  0) Create cluster object
    //  1) Take hit and phys. wire number from a sequence
    //  2) If cluster is empty, add hit to current cluster and go to 1)
    //  3) Check if hit possibly belongs to cluster
    //  3.1) If not, check if current cluster satisfies criteria
    //  3.1.1) If cluster satisfies criteria, add cluster and create new
    //  3.1.2) If cluster does not fit criteria, reset it
    //  4) Add current hit to cluster
    mem::Ref<APVCluster> clusPtr = lmem().create<APVCluster>(lmem());
    size_t nClustersFound = 0;
    for( auto p : dh ) {  // TODO: optimize selection
        APVPhysWire_t phWireNo = p.first;
        mem::Ref<APVHit> hitPtr = p.second;
        if( clusPtr->empty() ) {
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            continue;
        }
        if( _possibly_belongs_to_cluster(*clusPtr, phWireNo, *hitPtr) ) {
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            continue;
        }
        // break the cluster building
        if( clusPtr->size() >= _minClusterWidth ) {
            // current cluster is ok
            _add_cluster_to_event(e, pk, clusPtr);
            #if 0  // XXX
            std::cout << "Cluster (" << WireID::proj_label(pk.proj()) << "): ";
            for( auto hitPair : *clusPtr ) {
                std::cout << (int) hitPair.first << " ";
            }
            std::cout << std::endl;
            #endif
            ++nClustersFound;
            clusPtr = lmem().create<APVCluster>(lmem());
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            continue;
        } else {
            // current cluster does not fit to criteria => drop it and create
            // new cluster starting from current hit
            clusPtr->clear();
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
        }
    }
    if( clusPtr->size() > _minClusterWidth ) {
        _add_cluster_to_event(e, pk, clusPtr);
        ++nClustersFound;
    }
    #endif
    return nClustersFound;
}

bool
APVFindClusterWide::_possibly_belongs_to_cluster( const APVCluster & clus
                                             , APVPhysWire_t wNo
                                             , const APVHit & cHit ) const {    
    if( clus.empty() ) {
        // empty cluster -- let the first hit to be affiliated to it
        return true;
    }
    // Take the last from behind cluster (the latest inserted) and compare its
    // wire number with current one's to decide with respect of maximum allowed
    // number of missed ones
    const std::pair<APVPhysWire_t, mem::Ref<APVHit> > & le = *(clus.rbegin());
    bool r = _maxSparseness >= wNo - le.first - 1.;
    if( !std::isnan(_maxTimeDiscrepancy) ) {
        r &= fabs(clus.time - cHit.time) < _maxTimeDiscrepancy;
    }
    return r;
}

void
APVFindClusterWide::_add_hit_to_cluster( APVCluster & dest
                                    , APVPhysWire_t stripNo
                                    , mem::Ref<APVHit> hitPtr ) {
    // associate the hit with the cluster
    dest.emplace( stripNo, hitPtr );
    // keep last inserted hit time as cluster time for time comparison
    dest.time = hitPtr->time;
}

void
APVFindClusterWide::_add_cluster_to_event( Event & e
                                      , PlaneKey pk
                                      , mem::Ref<APVCluster> c ) {
    util::reset(c->time);
    e.apvClusters.emplace(pk, c);
}

}

REGISTER_HANDLER( APVFindClustersWide, mgr, yamlNode
                , "Finds 1D clusters on the planes" ) {
    using na64dp::handlers::wireMap_64_m5;
    using na64dp::handlers::wireMap_64_m5_new;
    using na64dp::handlers::wireMap_192_m5;

    const std::string designVer = yamlNode["design"].as<std::string>();
    std::vector< std::array<int, 5> > design;
    if( "64-old" == designVer ) {
        for( size_t i = 0
           ; i < sizeof(wireMap_64_m5)/sizeof(*wireMap_64_m5)
           ; ++i ) {
            std::array<int, 5> a = { wireMap_64_m5[i][1]
                                , wireMap_64_m5[i][2]
                                , wireMap_64_m5[i][3]
                                , wireMap_64_m5[i][4]
                                , wireMap_64_m5[i][5]
            };
            design.push_back(a);
        }
    } else if( "64-new" == designVer ) {
        for( size_t i = 0
           ; i < sizeof(wireMap_64_m5_new)/sizeof(*wireMap_64_m5_new)
           ; ++i ) {
            std::array<int, 5> a = { wireMap_64_m5_new[i][1]
                                , wireMap_64_m5_new[i][2]
                                , wireMap_64_m5_new[i][3]
                                , wireMap_64_m5_new[i][4]
                                , wireMap_64_m5_new[i][5]
                                };
            design.push_back(a);
        }
    } else if( "192-new" == designVer ) {
        for( size_t i = 0
           ; i < sizeof(wireMap_192_m5)/sizeof(*wireMap_192_m5)
           ; ++i ) {
            std::array<int, 5> a = { wireMap_192_m5[i][1]
                                , wireMap_192_m5[i][2]
                                , wireMap_192_m5[i][3]
                                , wireMap_192_m5[i][4]
                                , wireMap_192_m5[i][5]
            };
            design.push_back(a);
        }
    }
    return new handlers::APVFindClusterWide( mgr
                              , aux::retrieve_det_selection(yamlNode)
                              , design
                              , yamlNode["minWidth"].as<float>()
                              , yamlNode["sparseness"].as<float>()
                              , yamlNode["timeThreshold"].as<float>(std::nan("0"))
                              );
}

}

