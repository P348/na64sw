#include "na64dp/abstractHitHandler.hh"
#include "na64util/functions.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace handlers {

/**\brief Leave/remove APV hits belong to certain cluster
 *
 * Removes hits from an event by taking into account values of their clusters.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: APVClusteredHitsSelect
 *       # optional, hit selection expression
 *       applyTo: null
 *       # APV cluster getter; str, required
 *       value: ...
 *       # Comparison expression; str, required
 *       condition: ...
 *       # A threshold value to compare with; float, required
 *       threshold: ...
 * \endcode
 *
 * With respect to comparator, the cluster's value is the first argument while
 * the threshold is second.
 *
 * \ingroup apv-cluster-handlers
 * */
class APVClusteredHitsSelect : public AbstractHitHandler<event::APVCluster> {
protected:
    /// Value getter function
    event::Traits<event::APVCluster>::Getter _getter;
    /// Value to compare with
    const double _threshold;
    /// Comparison function
    functions::Comparator _compare;
public:
    APVClusteredHitsSelect( calib::Dispatcher & ch
                          , const std::string & only
                          , event::Traits<event::APVCluster>::Getter getter
                          , functions::Comparator comparator
                          , double threshold );

    virtual bool process_hit( EventID
                            , HitKey
                            , event::APVCluster & ) override;
};

//                      * * *   * * *   * * *
using event::APVCluster;

APVClusteredHitsSelect::APVClusteredHitsSelect( calib::Dispatcher & ch
        , const std::string & only
        , event::Traits<APVCluster>::Getter getter
        , functions::Comparator compare
        , double threshold ) : AbstractHitHandler<APVCluster>(ch, only)
                             , _getter(getter)
                             , _threshold(threshold)
                             , _compare(compare) {}

bool
APVClusteredHitsSelect::process_hit( EventID
                                   , HitKey detId
                                   , APVCluster & c ) {
    if( !_compare( _getter(c), _threshold ) ) {
        // fails check (comparison yields `false') -- remove this cluster's
        // hits and cluster itself
        for( auto it = c.hits.begin()
           ; c.hits.end() != it
           ; ++it ) {
            // to find out which hit to remove, build the APV detector ID with
            // hit's wire information
            DetID wID(detId);
            if( ! it->second->rawData ) {
                NA64DP_RUNTIME_ERROR( "Raw data info provided for APV hit"
                        " which is a part of APV cluster.");
            }
            wID.payload( it->second->rawData->wireNo );
            auto evMapIt = _current_event().apvHits.find( DetID(wID.id) );
            if( _current_event().apvHits.end() == evMapIt ) {
                // This means that cluster refers to hit that is not indexed
                // within APV hits map. This situation shall be considered as
                // an indication of a bug.
                NA64DP_RUNTIME_ERROR( "Event integrity violated: APV cluster"
                                          " refers to non-existing hit." );
            }
            // wipe hit from event
            typedef event::Association<event::Event, event::APVHit> Association;
            Association::remove( Association::map(_current_event())
                               , evMapIt
                               );
            // (former, prior to evstruct-refactoring)
            //Traits<event::APVHit>::remove_from_event(
            //        _current_event(), evMapIt, naming() );
        }
        // mark current cluster for removal
        this->_set_hit_erase_flag();
    }
    return true;  // continue processing
}

}

REGISTER_HANDLER( APVClusteredHitsSelect, ch, cfg
                , "Removes clusters together with corresponding APV hits"
                  " from an event, following by some cluster selection criterion." ) {
    return new handlers::APVClusteredHitsSelect( ch
            , aux::retrieve_det_selection(cfg)
            , util::value_getter<event::APVCluster>( cfg["value"].as<std::string>() )
            , functions::get_comparator( cfg["condition"].as<std::string>().c_str() )
            , cfg["threshold"].as<double>()
            );
}

}
