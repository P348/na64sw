#include "collectAmbigClusters.hh"

namespace na64dp {
namespace handlers {

/**\brief Removes ambigiuos APV clusters from an event
 *
 * Usage:
 * \code{.yaml}
 *     - _type: APVRemAmbigClusters
 *       # optional, hit selection expression
 *       applyTo: null
 * \endcode
 *
 * For APV detectors occupying the same DAQ channels (multiwired, multiplexed)
 * assures that clusters being present occupies the unique set of DAQ channels.
 *
 * Disambiguity is resolved by removing the clusters with respect to some
 * criterion (charge sum).
 *
 * \note For the time being, only MMs exploits the multiplexing scheme, so
 *       handler is appliable only to MMs, in fact.
 * \ingroup handlers apv-cluster-handlers
 * */
class APVRemAmbigClusters : public CollectAmbigClusters {
public:
    typedef std::set<const event::APVCluster *> BestClusters;
private:
    /// Clusters, best for wires
    std::map<PlaneKey, BestClusters> _bestClusterIDs;
    /// Getter for sorting value
    typename event::Traits<event::APVCluster>::Getter _sortGetter;
    /// Leave one(s) with largest (true) or lowest value (false)
    bool _leaveGreatest;
    /// Leave at most N largest or lowest
    size_t _leaveN;
protected:
    /// Removes ambiguous clusters from current event, for certain detector
    void _collect_ambiguous_clusters( PlaneKey, ClustersByWire & );
    const BestClusters & best_for(PlaneKey) const;
    BestClusters & best_for(PlaneKey, bool create=false);
    void _handle_clusters( std::unordered_map<PlaneKey, ClustersByWire> &
                         , event::Event & ) override;
public:
    APVRemAmbigClusters( calib::Dispatcher & ch
                       , const std::string & only
                       , typename event::Traits<event::APVCluster>::Getter getter
                       , bool ascending
                       , size_t leaveN
                       );
};  // APVRemAmbigClusters

//                          * * *   * * *   * * *

using na64dp::event::APVCluster;
using na64dp::event::Traits;
using na64dp::event::Event;


APVRemAmbigClusters::APVRemAmbigClusters( calib::Dispatcher & ch
                                        , const std::string & detSelection
                                        , typename event::Traits<event::APVCluster>::Getter g
                                        , bool leaveGreatest
                                        , size_t leaveN
                                        )
        : CollectAmbigClusters(ch, detSelection)
        , _sortGetter(g)
        , _leaveGreatest(leaveGreatest)
        , _leaveN(leaveN)
        {
    if(_leaveN < 1)
        NA64DP_RUNTIME_ERROR("\"leaveN\" must be >= (got %zu)", _leaveN);
}

const APVRemAmbigClusters::BestClusters &
APVRemAmbigClusters::best_for(PlaneKey did) const {
    auto it = _bestClusterIDs.find(did);
    assert( _bestClusterIDs.end() != it );
    return it->second;
}

APVRemAmbigClusters::BestClusters &
APVRemAmbigClusters::best_for(PlaneKey did, bool create) {
    auto it = _bestClusterIDs.find(did);
    if( _bestClusterIDs.end() == it ) {
        if(create) {
            auto ir = _bestClusterIDs.emplace(did, BestClusters());
            assert( ir.second );
            it = ir.first;
        } else {
            NA64DP_RUNTIME_ERROR( "No bests for plane \"%s\"."
                , AbstractHitHandler<APVCluster>::naming()[did].c_str() );
        }
    }
    return it->second;
}

void
APVRemAmbigClusters::_handle_clusters( std::unordered_map<PlaneKey, ClustersByWire> & idx
                                     , event::Event & ev) {
    // For each detector record, discriminate ambiguous clusters
    for( auto detEntry : idx ) {
        _collect_ambiguous_clusters( detEntry.first, detEntry.second );
    }
    // Build a reverse map to speed up cluster removal: cluster iterator by its
    // offset ID
    std::map< const APVCluster *
            , typename event::Association<Event, APVCluster>::Collection::iterator
            > cIts;
    for( auto it = ev.apvClusters.begin()
       ; it != ev.apvClusters.end()
       ; ++it ) {
        if( is_selective() && !matches((*it).first) ) continue;
        cIts.emplace( (*it).second.get(), it );
    }
    // Remove all except for "best" (on certain wire) clusters
    for( auto cPair : cIts ) {
        // curret cluster ptr
        const APVCluster * ptr = cPair.first;
        DetID_t planeID = (*cPair.second).first;
        //const PoolRef<APVCluster> & cPoolRef = (*cPair.second).second;
        // do not remove if in "best" clusters
        const auto & worst = best_for(planeID);
        if( worst.end() == worst.find(ptr) ) continue;
        // std::cout << "  ..." << cOffset << " not in best" << std::endl;
        // resolve reverse and remove
        event::Association<Event, APVCluster>::remove(
                ev.apvClusters, cPair.second );
    }  
    _bestClusterIDs.clear();
}

void
APVRemAmbigClusters::_collect_ambiguous_clusters( PlaneKey planeID
                                                , ClustersByWire & clusters ) {
    BestClusters & worst = best_for(planeID, true);
    for( auto it = clusters.begin(), next = clusters.equal_range(it->first).second
       ; it != clusters.end()
       ; ++it ) {
                
        next = clusters.equal_range(it->first).second;
        
        // channelNo = it->first
        //if( 1 == std::distance(it, next) ) continue;  // single cluster
        // create index of clusters sorted by criteria
        std::multimap<double, const APVCluster *> byValue;
        # if 1
        std::transform( it, next
                      , std::inserter(byValue, byValue.end())
                      , [this]( const std::pair<DetID_t, mem::Ref<APVCluster> > & ce ) {
                return std::make_pair( _sortGetter(*ce.second)  // use configurable getter here
                                     , ce.second.get() );
            } );
        if(byValue.size() < _leaveN) continue;
        # else
        for( auto iit = it; iit != next; ++iit ) {
            auto ir = byValue.emplace( iit->second->charge, iit->second );
            if( ! ir.second && iit->second.offset_id() != ir.first->second.offset_id() ) {
                std::cout << "collision, charge =" << iit->second->charge
                          << ", detID=" << iit->first
                          << ", new cluster offset=" << iit->second.offset_id()
                          << ", exist. cluster offset=" << ir.first->second.offset_id()
                          << std::endl;
            }  // XXX
        }
        # endif
        if( byValue.size() > _leaveN ) {
            if( _leaveGreatest ) {
                // byValue sorted in ascending order => last N has to be left
                auto it = byValue.rbegin();
                std::advance(it, _leaveN);

                for( ; it != byValue.rend(); ++it ) {
                    worst.emplace( it->second );
                }
            } else {
                // byValue sorted in ascending order => first N has to be left
                auto it = byValue.begin();
                std::advance(it, _leaveN);

                for( ; it != byValue.end(); ++it ) {
                    worst.emplace( it->second );
                }
            }
        }
    }   
}

}

REGISTER_HANDLER( APVRemAmbigClusters, ch, cfg
                , "Removes ambigiuos APV clusters from an event leaving"
                  " one(s) leading by some value." ) {
    std::string sortBy = cfg["sortBy"] ? cfg["sortBy"].as<std::string>() : "charge";
    bool leaveGreatest = cfg["leaveGreatest"] ? cfg["leaveGreatest"].as<bool>() : true;
    return new handlers::APVRemAmbigClusters( ch
                        , aux::retrieve_det_selection(cfg)
                        , util::value_getter<event::APVCluster>(sortBy)
                        , leaveGreatest
                        , cfg["leaveN"] ? cfg["leaveN"].as<int>() : 1
                        );
}

}

