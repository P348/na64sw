#include "collectAmbigClusters.hh"
#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Dumps ambigious clusters to an ASCII file
 * */
class APVDumpAmbigClusters
        : public CollectAmbigClusters {
private:
    /// Ref to the dest file
    std::ofstream _outfile;

    /// Dumps collected clusters to a file
    void _handle_clusters( std::unordered_map<PlaneKey, ClustersByWire> &
                         , event::Event & ) override;
public:
    APVDumpAmbigClusters( calib::Manager & cmgr
                        , const std::string & sel
                        , const std::string & outfilePath
                        ) : CollectAmbigClusters(cmgr, sel)
                          , _outfile(outfilePath)
                          { }
};  // class APVDumpAmbigClusters

//                      * * *   * * *   * * *

void
APVDumpAmbigClusters::_handle_clusters( std::unordered_map<PlaneKey, ClustersByWire> & idx
                                      , event::Event & event ) {
    _outfile << "event " << (na64sw_EventID_t) event.id << std::endl;
    for( auto & p : idx ) {
        _outfile << "plane " << naming()[p.first] << std::endl;
        for( auto & pp : p.second ) {
            event::APVDAQWire_t wno = pp.first;
            _outfile << "wire " << (int) wno << " " << pp.second << std::endl;
        }
    }
    for( auto & p : event.apvClusters ) {
        const event::APVCluster & c = *p.second;
        _outfile << "cluster "  << p.second
                 << " pos "     << c.position
                 << " charge "  << c.charge
                 << " time "    << c.time
                 << " timeErr " << c.timeError
                 << " size "    << c.hits.size()
                 << std::endl;
    }
}

}  // namespace :::na4dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( APVDumpAmbigClusters
                , cmgr, cfg
                , "Dumps ambigious clusters to an ASCII file" ) {
    return new na64dp::handlers::APVDumpAmbigClusters( cmgr
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["output"].as<std::string>()
            );
}

