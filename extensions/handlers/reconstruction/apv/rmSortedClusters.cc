#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/apv.hh"
#include "na64event/data/event.hh"

#include <algorithm>

namespace na64dp {
namespace handlers {

/**\brief APV Cluster in-place remove handler
 *
 * Sorts clusters wrt some value obtained by getter function and removes from
 * event all except for some leading ones per plane.
 *
 * Depending on `ascending` value, clusters with either the highest
 * (`ascending=true`) or lowest (`ascending=false`) values will avoid removal.
 */
class APVRemoveClusters : public AbstractHandler
                        , public SelectiveHitHandler<event::APVCluster>
                        {
private:
    /// Value-retrieval function obtaining sorting value
    event::Traits<event::APVCluster>::Getter _getter;
    /// Number of leading clusters to left, per plane
    size_t _nLeave;
    /// Wether to leave max values
    bool _ascending;
public:
    /**\brief Main ctr for cluster-removing handler
     *
     * \param cdsp calibration dispatcher instance to subscribe
     * \param sel Detector selection expression
     * \param getter A getter function obtaining value from cluster instance
     * \param logCat logging category ref
     * \param nLeave how many leading cluster have to remain
     * \param ascending Defines direction of sort
     * \param detNamingClass Class of name mapping for detectors
     * */
    APVRemoveClusters( calib::Dispatcher & cdsp
                     , const std::string & sel
                     , event::Traits<event::APVCluster>::Getter getter
                     , log4cpp::Category & logCat
                     , int nLeave=1
                     , bool ascending=true
                     , const std::string & detNamingClass="default"
                     );

    /// Removes clusters from event that does not match the configured criteria
    virtual ProcRes process_event(event::Event &);
};

//                          * * *   * * *   * * *

using na64dp::event::APVCluster;
using na64dp::event::Traits;
using na64dp::event::Event;

APVRemoveClusters::APVRemoveClusters( calib::Dispatcher & cdsp
                                    , const std::string & sel
                                    , Traits<APVCluster>::Getter getter
                                    , log4cpp::Category & logCat
                                    , int nLeave
                                    , bool ascending
                                    , const std::string & detNamingClass
                        ) : SelectiveHitHandler<event::APVCluster>(cdsp, sel, logCat, detNamingClass)
                          , _getter(getter)
                          , _nLeave(nLeave)
                          , _ascending(ascending) {}

AbstractHandler::ProcRes
APVRemoveClusters::process_event(Event & e) {
    if(e.apvClusters.empty())
        return kOk;  // no clusters, nothing to process within an event

    // cluster object indexed by detector ID, will contain iterators sorted by
    // some value
    typedef event::Association<Event, APVCluster>::Collection APVClusters;
    std::map< DetID_t
            , std::multimap< double
                           , typename APVClusters::iterator
                           > > sortedClusters;
    // iterate over all the clusters, filling sorted clusters index
    for( auto it = e.apvClusters.begin()
       ; e.apvClusters.end() != it
       ; ++it ) {
        if(!matches(it->first)) continue;
        DetID_t did = it->first;
        const APVCluster & c = *(*it).second;
        // get the sorted clusters entry for certain plane
        auto entryIt = sortedClusters.find(did);
        if( sortedClusters.end() != entryIt ) {
            continue;  // plane is already been accounted
        }
        // plane has nto been accounted -- use equal range to get all the
        // clusters for certain plane
        auto planeClusters = e.apvClusters.equal_range(did);
        std::multimap<double, typename APVClusters::iterator> sortedByValue;
        //sortedByValue.reserve( std::distance( planeClusters.first
        //                                    , planeClusters.second ) );
        for( auto iit = planeClusters.first
           ; iit != planeClusters.second
           ; ++ iit ) {
            const double v = _getter( c );
            sortedByValue.emplace(v, iit);
        }
        sortedClusters.insert( std::make_pair(did, sortedByValue) );
    }
    // remove all clusters except last few for each plane
    // note that std guarantees all the remaining iterators remain valid on
    // element erase
    if( _ascending ) {
        for( auto entryIt = sortedClusters.begin()
           ; sortedClusters.end() != entryIt
           ; ++entryIt ) {
            // determine whether there are clusters to be removed actually; if
            // number of sorted clusters is le number to leave, we can not actually
            // remove anything
            if( entryIt->second.size() <= _nLeave ) continue;
            auto it = entryIt->second.rbegin();
            // advance reverse iterator to retain some leading clusters (by
            // previous checks it is guaranteed that there are enough elements
            // to afvance)
            std::advance( it, _nLeave );
            for( ; entryIt->second.rend() != it; ++it ) {
                e.apvClusters.erase( it->second );
            }
        }
    } else {
        for( auto entryIt = sortedClusters.begin()
           ; sortedClusters.end() != entryIt
           ; ++entryIt ) {
            if( entryIt->second.size() <= _nLeave ) continue;
            auto it = entryIt->second.begin();
            std::advance( it, _nLeave );
            for( ; entryIt->second.end() != it; ++it ) {
                e.apvClusters.erase( it->second );
            }
        }
    }
    
    return kOk;
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( APVRemoveClusters, ch, cfg
                , "Retains 1D clusters from APV detector wrt some criteria" ) {
    return new handlers::APVRemoveClusters( ch
            , aux::retrieve_det_selection(cfg)
            , util::value_getter<event::APVCluster>(cfg["sortBy"].as<std::string>())
            , aux::get_logging_cat(cfg)
            , cfg["leaveN"].as<int>()
            , cfg["ascending"] ? cfg["ascending"].as<bool>() : false
            , aux::get_naming_class(cfg)
            );
}

}  // namespace na64dp

