#include "na64dp/abstractHitHandler.hh"
//#include "na64util/mm-layout.h"
#include "na64util/str-fmt.hh"
#include "na64calib/manager-utils.hh"

#include "na64common/calib/sdc-integration.hh"  // for string routines
#include "na64common/calib/mm-design.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

using event::Event;
using event::APVHit;
using event::APVCluster;
using event::APVDAQWire_t;
using event::APVPhysWire_t;

/**\brief Creates clusters on the APV detectors planes from individual hits
 *
 * Usage:
 * \code{.yaml}
 *     - _type: APVFindClusters
 *       # optional, hit selection expression
 *       applyTo: null
 *       # minimum number of wires to form a cluster; float, required
 *       minWidth: ...
 *       # minimum "missing wires" within a cluster; float, optional
 *       sparseness: 0.0
 *       # Maximum allowed time discrepancy between times of two adjacent
 *       # hits within a cluster. Has to be imperically adjusted; float, optional
 *       timeThreshold: nan
 * \endcode
 *
 * A "sparseness" parameter defines how many wires may be "missed" within a
 * cluster. In principle, should help to stabilize clustering reconstruction
 * for tripping wires or lithographic defects, but seems to be practically
 * meaningless so far.
 *
 * "timeThreshold" parameter defines maximum allowed time difference between
 * two adjacent wires to form a cluster. (fixme: what if first hit in a
 * sequence provides value that is too far from mean hit time?)
 *
 * Handler has a calibration data dependency: `MuMegaWiresDesignEntry`.
 *
 * Currently, one of the "heaviest" handlers, perhaps has to be optimized.
 *
 * \ingroup handlers apv-cluster-handlers calib-handlers
 * \todo optimize this handler for performance
 * \todo once calib data unloading problem resolved, invalidate calib cache
 * */
class APVFindClusters : public AbstractHitHandler<APVHit>
                      , public calib::Handle<calib::MuMegaWiresDesignEntry>
                      {
protected:
    const float _minClusterWidth  ///< minimum cluster width requirement in wire units
              , _maxSparseness  ///< maximum number of wires missed within a cluster
              , _maxTimeDiscrepancy;

    DetKin_t _mmKinID  ///< MuMegas kin ID cache
           ;

    /// MuMega design cache entry, updated by `handle_update()`
    struct MuMegaWiresDesignCache {
        /// Selection str-expression (for diagnostics)
        std::string selectionStrExpr;
        /// Detectors matching the selection will be considered as ones having this
        /// design.
        DetSelect * selection;
        /// Wire layout scheme for this detector
        calib::MuMegaWiresDesign design;
        /// Ctr
        MuMegaWiresDesignCache( const std::string & selection_
                              , const calib::MuMegaWiresDesign & design_
                              , const nameutils::DetectorNaming & nm
                              ) : selectionStrExpr(selection_)
                                , selection( new DetSelect( selectionStrExpr.c_str()
                                                          , util::gDetIDGetters
                                                          , nm )
                                                          )
                                , design(design_)
                                {}
        MuMegaWiresDesignCache(const MuMegaWiresDesignCache &) = delete;
        ~MuMegaWiresDesignCache() {
            delete selection;
        }
    };
    ///\brief A stack of the designs with selection expressions
    ///
    /// Due to current design limitation with calibration data subsystem (no
    /// unloading/erasing of previous updates) we pick up first design matching
    /// the selection.
    std::list<MuMegaWiresDesignCache> _muMegaDesign;
    /// A cached entry (avoids detector selection check)
    ///
    /// \todo is it really faster?
    mutable std::unordered_map<DetID_t, const calib::MuMegaWiresDesign *> _pkCache;
protected:
    /// Storage for hits IDs sorted by a wire number, for single plane
    typedef std::map< APVDAQWire_t, DetID > SortedHits;

    /// Hits by plane, accumulated for the event
    ///
    /// \todo use a custom allocator for performance
    std::unordered_map< PlaneKey, SortedHits> _byPlaneCaches;

    /// Upon naming change, retrieves values for MM and GM kin IDs
    virtual void handle_update(const nameutils::DetectorNaming &) override;
    /// Discovers clusters on the planes
    virtual size_t _find_clusters( Event &, PlaneKey, const SortedHits & );
    /// Returns true if provided hit seems to be part of cluster candidate
    virtual bool _possibly_belongs_to_cluster( const APVCluster &
                                             , APVPhysWire_t physWireNo
                                             , const APVHit & ) const;
    /// Adds certain hit to the cluster -- appends hits collection and
    /// increments (weighted) sums
    virtual void _add_hit_to_cluster( APVCluster &, APVPhysWire_t, mem::Ref<APVHit> );
    /// Finalizes cluster by calculating weighted sums and pushes it to the
    /// event's collection
    virtual void _add_cluster_to_event( Event &, PlaneKey, mem::Ref<APVCluster> );
public:
    APVFindClusters( calib::Dispatcher & ch
                   , const std::string & only
                   , float clusterMinWidth=1.
                   , float timeThreshold=std::nan("0")
                   , float sparseness=0.
                   , const std::string & mmLayoutSubtype="default"
                   );
    /// Updates design cache
    void handle_update( const calib::MuMegaWiresDesignEntry & upd ) override;
    /// Forwards execution to parent to iterate over hits and collect them into
    /// sorted by-wire containers. Once all hits are collected, forwards
    /// execution to `_find_clusters()`.
    virtual AbstractHandler::ProcRes process_event( Event & e ) override;
    /// Fills sorted hits caches by plane ID (`_byPlaneCaches`)
    virtual bool process_hit( EventID, DetID, APVHit & ) override;
    /// Returns multiplexing design appropriate for certain MuMega among
    /// existing caches
    const calib::MuMegaWiresDesign & design_for(PlaneKey) const;
};

//                      * * *   * * *   * * *

APVFindClusters::APVFindClusters( calib::Dispatcher & ch
                                , const std::string & detSelection
                                , float clusterMinWidth
                                , float sparseness
                                , float timeThreshold
                                , const std::string & mmLayoutSubtype
                                ) : AbstractHitHandler(ch, detSelection)
                                  , calib::Handle<calib::MuMegaWiresDesignEntry>("default", ch)
                                  , _minClusterWidth(clusterMinWidth)
                                  , _maxSparseness(sparseness)
                                  , _maxTimeDiscrepancy(timeThreshold)
                                  {
    if(_minClusterWidth < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"minWidth\" value provided to"
                " APVClusterAssemble handler." );
    }
    if(_maxSparseness < 0.) {
        NA64DP_RUNTIME_ERROR( "Negative \"sparseness\" value provided to"
                " APVFindClusters handler." );
    }
}

void
APVFindClusters::handle_update(const nameutils::DetectorNaming & nm) {
    AbstractHitHandler<APVHit>::handle_update(nm);
    try {
        _mmKinID = nm.kin_id("MM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find Micromega kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
}

void
APVFindClusters::handle_update( const calib::MuMegaWiresDesignEntry & upd ) {
    // no need to propagate to parent's handle_update() here.
    try {
        _muMegaDesign.emplace_front(upd.first, upd.second, naming());
    } catch(na64dp::errors::SelectorBuildupFailure & e) {
        _log.error("While applying update with selector expression \"%s\""
                " an exception was raised.", upd.first.c_str());
        throw;
    }
    _pkCache.clear();
}

AbstractHandler::ProcRes
APVFindClusters::process_event( event::Event & e ) {
    AbstractHandler::ProcRes pr = AbstractHitHandler<APVHit>::process_event( e );
    // for each plane: find clusters
    for( auto planePair : _byPlaneCaches ) {
        _find_clusters( e, planePair.first, planePair.second );
    }
    // clear hits cache
    _byPlaneCaches.clear();
    return pr;
}

bool
APVFindClusters::process_hit( EventID
                            , DetID did
                            , APVHit & cHit ) {
    // iDid -- incomplete detector ID; for APV detectors this will be 
    // defined with chip, kin, station number and projection ID. No wire number
    // is written within iDid
    PlaneKey pk(did);
    auto cIt = _byPlaneCaches.find(pk);
    if( _byPlaneCaches.end() == cIt ) {
        // No cache entry for this detector plane found -- create one
        cIt = _byPlaneCaches.emplace( pk, SortedHits() ).first;
    }
    //auto wIt = cIt->second.find( cHit.rawData.wireNo );
    auto wIt = cIt->second.lower_bound( cHit.rawData->wireNo );
    if( cIt->second.end() != wIt && wIt->first == cHit.rawData->wireNo ) {
        // Hit on this plane with the same wire number already exists at the
        // current event. This should be interpreted as an error since it is
        // technically impossible. Did you forget to clear the cache between
        // events?
        // Note that this "wire number" is not yet a phyiscal one, so we are
        // safe for MuMega detectors that have multiple physical wire connected
        // to single APV chip channel.
        NA64DP_RUNTIME_ERROR( "Multiple APV hits on the same wire." );
    }
    // Add full hit ID to the by-wire index
    cIt->second.emplace_hint( wIt, cHit.rawData->wireNo, did );
    return true;
}

size_t
APVFindClusters::_find_clusters( Event & e
                               , PlaneKey pk
                               , const SortedHits & hitsMap ) {
    // whether this plane requires a special treatment (multiple physical wires
    // connected to a single DAQ channel)
    const bool isMuMega = _mmKinID == pk.kin();
    // Map of possibly duplicating hits: maps physical (real wire) number to
    // a cluster. Wire numbers are unique here, but same cluster may be refrred
    // multiple times.
    std::map<APVPhysWire_t, mem::Ref<APVHit> > dh;  // TODO: pool allocator for performance
    //std::cout << naming()[pk] << std::endl;  // XXX
    // Fill hits map (possibly duplicating)
    for( auto hitPair : hitsMap ) {
        const DetID & did = hitPair.second;
        // get ptr to hit
        mem::Ref<APVHit> hitPtr = e.apvHits[did];
        // get the DAQ wire number of this hit
        const uint16_t rawWireNo = hitPair.first;  // "virtual" (DAQ) wire no
        assert(rawWireNo == did.payload_as<WireID>()->wire_no());  // assure DAQ wire is the same
        if(isMuMega) {  // TODO
            const calib::MuMegaWiresDesign & design = design_for(pk);
            // for MuMega x64 design -- add 5 hits to the reverse index
            if( rawWireNo >= design.size() ) {
                NA64DP_RUNTIME_ERROR( "MuMega plane \"%s\" has layout of"
                        " %zu wires while acquired hit is on wire %d"
                        , naming()[pk].c_str()
                        , design.size()
                        , (int) rawWireNo
                        );
            }
            const auto & chs = design[rawWireNo];
            //std::cout << rawWireNo << " <- ";  // XXX
            for(auto ch : chs) {
                auto ir = dh.emplace( ch, hitPtr );
                //std::cout << ch << " ";  // XXX
                //std::cout.flush();  // XXX
                if( ! ir.second ) {
                    NA64DP_RUNTIME_ERROR( "MuMega reverse mapping failure:"
                            " physical wire #%d corresponds to #%d and #%d"
                            " DAQ wire channels."
                            , (int) rawWireNo
                            , (int) ch
                            , (int) ir.first->first
                            );
                }
                assert(ir.second);
            }
            //std::cout << std::endl;  // XXX
            continue; // do not continue with standard scheme
        }
        // standard layout (no multiplexing)
        auto ir = dh.emplace( rawWireNo, hitPtr );
        assert(ir.second);
    }
    // Find clusters, relying on `_possibly_belongs_to_cluster()` result.
    // Note that within `dh', the hits are sorted by their real wire number

    #if 0
    // Create new cluster instance to fill
    mem::Ref<APVCluster> c = lmem().create<APVCluster>(lmem());
    util::reset(*c);
    size_t nClustersFound = 0;
    // Iterate over hits sorted by wire number
    for( auto dHitPair : dh ) {
        NA64DP_APVStripNo_t physWireNo = dHitPair.first;
        mem::Ref<APVHit> hitPtr = dHitPair.second;
        // if hit belongs to current cluster, add and continue
        if ( _possibly_belongs_to_cluster(*c, physWireNo, *hitPtr) ) {
            _add_hit_to_cluster( *c, physWireNo, hitPtr );
        } else if( c->size() >= _minClusterWidth ) {
            // Cluster assembly is done, its size is ge _minClusterWidth
            // => finalize the current cluster [and create new adding next]
            _add_cluster_to_event(e, pk, c);
            nClustersFound++;
        } else {
            // cluster building is interrupted and its size is < _minClusterWidth
            assert(c->size() < _minClusterWidth);
            util::reset(*c);
        }
    }
    // loop above may end up on the last wire without finalizing non-empty cluster
    if(!c->empty() && c->size() >= _minClusterWidth ) {
        _add_cluster_to_event(e, pk, c);
        nClustersFound++;
    }
    #else
    // Following algorithm below:
    //  0) Create cluster object
    //  1) Take hit and phys. wire number from a sequence
    //  2) If cluster is empty, add hit to current cluster and go to 1)
    //  3) Check if hit possibly belongs to cluster
    //  3.1) If not, check if current cluster satisfies criteria
    //  3.1.1) If cluster satisfies criteria, add cluster and create new
    //  3.1.2) If cluster does not fit criteria, reset it
    //  4) Add current hit to cluster
    mem::Ref<APVCluster> clusPtr = lmem().create<APVCluster>(lmem());
    size_t nClustersFound = 0;
    for( auto p : dh ) {  // TODO: optimize selection
        APVPhysWire_t phWireNo = p.first;
        mem::Ref<APVHit> hitPtr = p.second;
        if(  clusPtr->hits.empty()
          || _possibly_belongs_to_cluster(*clusPtr, phWireNo, *hitPtr)
          ) {
            //std::cout << " " << p.first;  // XXX
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            continue;
        }
        // break the cluster building (hit does not seem to belong to current
        // cluster)
        if( clusPtr->hits.size() >= _minClusterWidth ) {
            // current cluster is ok, add it to event and start new one with
            // current hit
            _add_cluster_to_event(e, pk, clusPtr);
            //std::cout << " <- cluster;" << std::endl;
            #if 0  // XXX
            std::cout << "Cluster (" << WireID::proj_label(pk.proj()) << "): ";
            for( auto hitPair : *clusPtr ) {
                std::cout << (int) hitPair.first << " ";
            }
            std::cout << std::endl;
            #endif
            ++nClustersFound;
            clusPtr = lmem().create<APVCluster>(lmem());
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            //std::cout << " " << p.first;  // XXX
            continue;
        } else {
            // current cluster does not fit to criteria => drop it and create
            // new cluster starting from current hit
            clusPtr->hits.clear();
            _add_hit_to_cluster( *clusPtr, phWireNo, hitPtr );
            //std::cout << " <- short seq;" << std::endl;
            //std::cout << " " << p.first;  // XXX
        }
    }
    if( clusPtr->hits.size() >= _minClusterWidth ) {
        _add_cluster_to_event(e, pk, clusPtr);
        //std::cout << " <- cluster;" << std::endl;
        ++nClustersFound;
    } //else {
    //    std::cout << " <- short seq;" << std::endl;
    //}
    //std::cout << std::endl;  // XXX
    #endif
    return nClustersFound;
}

bool
APVFindClusters::_possibly_belongs_to_cluster( const APVCluster & clus
                                             , APVPhysWire_t wNo
                                             , const APVHit & cHit ) const {    
    if( clus.hits.empty() ) {
        // empty cluster -- let the first hit to be affiliated to it
        return true;
    }
    // Take the last from behind cluster (the latest inserted) and compare its
    // wire number with current one's to decide with respect of maximum allowed
    // number of missed ones
    const std::pair<APVPhysWire_t, mem::Ref<APVHit> > & le = *(clus.hits.rbegin());
    bool r = _maxSparseness >= wNo - le.first - 1;
    if( !std::isnan(_maxTimeDiscrepancy) ) {
        r &= fabs(clus.time - cHit.time) < _maxTimeDiscrepancy;
    }
    return r;
}

void
APVFindClusters::_add_hit_to_cluster( APVCluster & dest
                                    , APVPhysWire_t stripNo
                                    , mem::Ref<APVHit> hitPtr ) {
    // associate the hit with the cluster
    dest.hits.emplace( stripNo, hitPtr );
    // keep last inserted hit time as cluster time for time comparison
    dest.time = hitPtr->time;
}

void
APVFindClusters::_add_cluster_to_event( Event & e
                                      , PlaneKey pk
                                      , mem::Ref<APVCluster> c ) {
    assert(c);
    util::reset(c->time);
    e.apvClusters.emplace(pk, c);
}

const calib::MuMegaWiresDesign &
APVFindClusters::design_for(PlaneKey pk) const {
    {   // try to find in cache
        auto it = _pkCache.find(pk);
        if(_pkCache.end() != it) return *(it->second);
    }
    // iterate forward (backward from insertion) and take first appropriate
    for( auto it = _muMegaDesign.begin()
       ; _muMegaDesign.end() != it
       ; ++it
       ) {
        if( it->selection->matches(pk) ) {
            _pkCache[pk] = &(it->design);
            return it->design;
        }
    }
    NA64DP_RUNTIME_ERROR( "Unable to find appropriate multiplexing MuMega"
            " design for plane %s.", naming()[pk].c_str() );
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( APVFindClusters, mgr, yamlNode
                , "Finds 1D clusters on the planes" ) {
    mgr.get_loader<na64dp::calib::YAMLDocumentLoader>("yaml-doc")
            .define_conversion(
                      "MuMegaLayout"  //< this must coincide with 2nd arg of REGISTER_CALIB_DATA_TYPE()
                    , ::na64dp::calib::convert_YAML_description_to_MuMegaLayout
                    );
    // make na64sw always load naming before MuMegaLayout calib data
    na64dp::calib::CIDataAliases::self().add_dependency("MuMegaLayout", "naming");
    return new na64dp::handlers::APVFindClusters( mgr
                              , na64dp::aux::retrieve_det_selection(yamlNode)
                              , yamlNode["minWidth"].as<float>()
                              , yamlNode["sparseness"]
                                        ? yamlNode["sparseness"].as<float>()
                                        : 0
                              , yamlNode["timeThreshold"].as<float>(std::nan("0"))
                              );
}


