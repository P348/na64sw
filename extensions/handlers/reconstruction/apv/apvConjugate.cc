#include "apvConjugate.hh"
#include "na64util/numerical/biplaneIntersection.h"

namespace na64dp {
namespace aux {

void
TrackScoreTraits<event::APVCluster>::recache_placement( Cache & cache
                                                      , PlaneKey pk
                                                      , const calib::Placement & placement
                                                      ) {
    if(!placement.is_detector()) return;
    // assure it is ordinary plane's placement
    if( calib::Placement::kRegularWiredPlane != placement.suppInfoType ) {
        return;
    }
    // try to find a plane to update it instead of (re)creation
    PlaneEntry & planeEntry = cache.emplace(pk, PlaneEntry{}).first->second;
    planeEntry.nWires = placement.suppInfo.regularPlane.nWires;
    assert(planeEntry.nWires);
    if(!std::isnan(placement.suppInfo.regularPlane.resolution)) {
        planeEntry.defaultUncertainty
            = placement.suppInfo.regularPlane.resolution/placement.size[0];
    } else {
        // set to standard deviation of stride-sized uniform distribution.
        // For APV-based detectors this is known to be rather pessimistic
        // estimation, but fine enough as initial approach.
        planeEntry.defaultUncertainty
            = 1./(placement.suppInfo.regularPlane.nWires*sqrt(12.));
    }
}

std::pair<DetID_t, mem::Ref<event::TrackScore> >
TrackScoreTraits<event::APVCluster>::create( LocalMemory & lmem
                                           , mem::Ref<event::APVCluster> c
                                           , PlaneKey pk
                                           , Cache & cache
                                           ) {
    assert(!WireID(pk.payload()).wire_no_is_set());  // assure it is not a full ID
    // find plane description
    auto it = cache.find(pk);
    if(cache.end() == it)
        return {pk, nullptr};  // no placement has been found for this detector
    const PlaneEntry & pe = it->second;
    if( c->position > pe.nWires
     || c->position < 0
     || std::isnan(c->position)
     || !std::isfinite(c->position)
     ) {
        NA64DP_RUNTIME_ERROR( "APV detector to-score conversion error:"
                " measured cluster position (in wire units =%f) is"
                " greater than number of wires in the"
                " plane (=%d), negative, not set or is not a finite real number."
                , c->position, pe.nWires );
    }
    // hit coordinate
    auto dest = lmem.create<event::TrackScore>(lmem);
    util::reset(*dest);
    // set single local coordinate based on stride
    //dest->lR[0] = (c->position + .5 - pe.nWires/2.) * pe.stride;
    // ^^^ in wire units (deprecated)
    dest->lR[0] = util::wire_to_normalized_DRS(c->position, pe.nWires);
    // ^^^ normalized, -0.5 - 0.5, half-stride offset is disabled for clusters
    assert( dest->lR[0] >= -.5 );
    assert( dest->lR[0] <=  .5 );
    assert(!std::isnan(dest->lR[0]));
    dest->lR[1] = std::nan("0");
    if(std::isnan(c->positionError)) {
        dest->lRErr[0] = c->positionError;
    } else {
        dest->lRErr[0] = pe.defaultUncertainty;
    }
    dest->lRErr[1] = std::nan("0");
    // associate hit
    dest->hitRefs.emplace(pk, c);
    assert(pk.is_payload_set());
    return {pk, dest};
}

}  // namespace ::na64dp::aux

//
// Simple 2D combination/conjugation rule implementation

size_t
TwoLayerAPVCombinationRule::_n_set(HitRef_t hr) const {
    assert(hr.first);
    DetID did(hr.second->first);
    assert(did.is_payload_set());
    auto p = did.payload_as<WireID>()->proj();
    if( p == WireID::kX || p == WireID::kU ) {
        return 0;  // X/U -- 1st
    }
    if( p == WireID::kY || p == WireID::kV ) {
        return 1;  // Y/V -- 2nd
    }
    NA64DP_RUNTIME_ERROR("APV detector \"%s\" has unsupported projection label"
            " -- unable to figure out tuplet's number of element."
            , naming()[did].c_str());
}

void
TwoLayerAPVCombinationRule::handle_update(const calib::Placements & pls) {
    for( const auto & pl : pls ) {
        if(!pl.is_detector()) continue;
        // get plane ID of the placement entry
        PlaneKey planeID(naming()[pl.name]);
        // omit irrelevant placements
        if(!SelectiveHitHandler<event::APVCluster>::matches(planeID)) continue;
        
        #if 0
        // for rest -- create the entry
        util::Vec3 o, u, v, w;
        util::cardinal_vectors( pl.data.center
                              , pl.data.size
                              , pl.data.rot
                              , o, u, v, w
                              );
        u = u.unit();
        v = v.unit();
        PlacementsCache pc = { pl.data.size[0]/pl.data.suppInfo.regularPlane.nWires
                             , -pl.data.size[0]/2
                             , u, v, o
                             , pl.data.suppInfo.regularPlane.resolution
                             , {pl.data.size[0], pl.data.size[1]}
                             };
        #endif
        _placementsCache[planeID] = {
                pl.suppInfo.regularPlane.nWires,
                na64dp::util::transformation_by(pl)
            };
    }
}

//double
//TwoLayerAPVCombinationRule::PlacementsCache::wire_units_to_drs_position(float wireUnits) const {
//    return (wireUnits + .5)/nWires - .5;
//}

void
TwoLayerAPVCombinationRule::calc_score_values( event::TrackScore & s
                                             , const std::array<HitRef_t, 2> & hitRefs ) {
    assert(hitRefs[0].first);
    assert(hitRefs[1].first);
    auto plIt1 = _placementsCache.find(hitRefs[0].second->first)
       , plIt2 = _placementsCache.find(hitRefs[1].second->first)
       ;

    // TODO: dedicated exception with plane key to catch and report user on
    //       what is missing upward on stack, where naming is available
    if(plIt1 == _placementsCache.end())
        throw errors::MissingInfoForDetector(hitRefs[0].second->first, "placements");
    if(plIt2 == _placementsCache.end())
        throw errors::MissingInfoForDetector(hitRefs[1].second->first, "placements");

    assert( std::isfinite(hitRefs[0].second->second->position) );
    assert( std::isfinite(hitRefs[1].second->second->position) );

    // NOTE: order is important here
    s.lR[0] = util::wire_to_normalized_DRS( hitRefs[0].second->second->position
                                          , plIt1->second.nWires);
    s.lR[1] = util::wire_to_normalized_DRS( hitRefs[1].second->second->position
                                          , plIt2->second.nWires);
    s.lR[2] = std::nan("0");  // sentinel
    //std::cout << hitRefs[0].second->second->position << "/" << plIt1->second.nWires << " -> "
    //          << s.lR[0] << ", "
    //          << hitRefs[1].second->second->position << "/" << plIt2->second.nWires << " -> "
    //          << s.lR[1] << std::endl;  // XXX, to global log?

    // build measured vectors
    // - extremities in space local to plane:
    util::Vec3 uUp{{s.lR[0], .5, 0}}, uLo{{s.lR[0], -.5, 0}}
             , vUp{{s.lR[1], .5, 0}}, vLo{{s.lR[1], -.5, 0}}
             ;
    // - apply affine transform to get extremities, _without offset_
    plIt1->second.t.apply(uUp); plIt1->second.t.apply(uLo);
    plIt2->second.t.apply(vUp); plIt2->second.t.apply(vLo);
    // - get vectors denoting the measurement directions
    util::Vec3 dU = uUp - uLo
             , dV = vUp - vLo;
    
    // TODO: following function finds out POCA of two skew lines, but we should
    //       probably take into account extremities
    util::Vec3 x1, x2;  //< points of closest approach on two measured segments
    int rc = na64sw_projected_lines_intersection( dU.unit().r, dV.unit().r, NULL
                                                , uLo.r, vLo.r
                                                , x1.r, x2.r);
    if(rc) {
        // todo: details?
        NA64DP_RUNTIME_ERROR("Failed to conjugate APV measurements"
                " (parallel planes?)");
    }
    // Note: intersection points can be of use at some point. For now we just
    // take POCA as averaged point
    util::Vec3 poca = (x1 + x2)*.5;
    s.gR[0] = poca.c.x;
    s.gR[1] = poca.c.y;
    s.gR[2] = poca.c.z;
}

#if 0
void
TwoLayerAPVCombinationRule::mrs( float * mrsXYZ
                               , const std::array<HitRef_t, 2> & hrs
                               ) {
    assert(mrsXYZ);
    assert(hrs[0].first);  // both hits are set
    assert(hrs[1].first);
    auto it1 = _placementsCache.find(hrs[0].second->first)
       , it2 = _placementsCache.find(hrs[1].second->first)
       ;
    if( it1 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for APV plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[0].second->first].c_str() );
    }
    if( it2 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for APV plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[1].second->first].c_str() );
    }
    const PlacementsCache & c1 = it1->second
                        , & c2 = it2->second;

    #if 1
    // Convert wire coordinates to normalized ones
    Float_t locPos1 = util::wire_to_normalized_DRS(hrs[0].second->second->position, c1.nWires)
          , locPos2 = util::wire_to_normalized_DRS(hrs[1].second->second->position, c2.nWires)
          ;
    // Use affine transform defined by plane to derive r_{0,n} point for line
    // equation
    util::Vec3 r01 = c1.t({{locPos1, 0, 0}})
             , r02 = c2.t({{locPos2, 0, 0}})
             ;
    // Find mutual POCA for two lines
    Float_t x1[3], x2[3];  // output vectors
    int rc = na64sw_projected_lines_intersection(
                c1.t.v().r, c2.t.v().r, NULL,
                r01.r, r02.r,
                x1, x2);
    #else
    const auto & pos1 = c1.wire_units_to_drs_position(hrs[0].second->second->position)
             , & pos2 = c2.wire_units_to_drs_position(hrs[1].second->second->position);
    // find MRS point as "projected intersection" of two measurements
    #if 0
    double r01[3] = { c1.u0[0]*pos1 + c1.c[0]
                    , c1.u0[1]*pos1 + c1.c[1]
                    , c1.u0[2]*pos1 + c1.c[2]
                    }
         , r02[3] = { c2.u0[0]*pos2 + c2.c[0]
                    , c2.u0[1]*pos2 + c2.c[1]
                    , c2.u0[2]*pos2 + c2.c[2]
                    }
         ;
    #else
    util::Vec3 r01 = c1.u0*pos1 + c1.c
             , r02 = c2.u0*pos2 + c2.c
             ;
    #endif
    Float_t x1[3], x2[3];  // output vectors
    int rc = na64sw_projected_lines_intersection(
            it1->second.v.r, it2->second.v.r, NULL,
            r01.r, r02.r,
            x1, x2);
    #endif
    if(rc) {
        NA64DP_RUNTIME_ERROR("Failed to conjugate hits of %s and %s:"
                " na64sw_projected_lines_intersection() return code %d"
                , naming()[hrs[0].second->first].c_str()
                , naming()[hrs[1].second->first].c_str()
                , rc
                );
    }
    // set MRS to be mean point of two "intersections"
    for(int i = 0; i < 3; ++i) {
        mrsXYZ[i] = (x1[i] + x2[i])/2;
    }
    assert(!std::isnan(mrsXYZ[0]));
}
#endif

#if 0  // XXX, deprecated
void
TwoLayerAPVCombinationRule::handle_request( const YAML::Node & rq
                                          , YAML::Node & resp ) const {
    // TODO: think about how to avoid updating static geometry every event?
    //if(rq["updateOnRecache"] && (!rq["updateOnRecache"].as<bool>()))

    // assure destination sequence exists in response
    if(!resp["staticGeometry"]) { resp["staticGeometry"] = YAML::Node(YAML::NodeType::Map); }
    if(!resp["staticGeometry"]["entities"]) {
        resp["staticGeometry"]["entities"] = YAML::Node(YAML::NodeType::Sequence);
    }

    for( const auto & cacheE : _placementsCache ) {
        YAML::Node entry;
        entry["_type"] = "FiniteCoordinateSensitivePlane";
        entry["label"] = naming()[cacheE.first];

        entry["o"] = YAML::Node(YAML::NodeType::Sequence);
        entry["o"].push_back(cacheE.second.c[0]);
        entry["o"].push_back(cacheE.second.c[1]);
        entry["o"].push_back(cacheE.second.c[2]);

        entry["u"] = YAML::Node(YAML::NodeType::Sequence);
        entry["u"].push_back(cacheE.second.u0[0]);
        entry["u"].push_back(cacheE.second.u0[1]);
        entry["u"].push_back(cacheE.second.u0[2]);

        entry["v"] = YAML::Node(YAML::NodeType::Sequence);
        entry["v"].push_back(cacheE.second.v[0]);
        entry["v"].push_back(cacheE.second.v[1]);
        entry["v"].push_back(cacheE.second.v[2]);

        entry["size"] = YAML::Node(YAML::NodeType::Sequence);
        entry["size"].push_back(cacheE.second.size[0]);
        entry["size"].push_back(cacheE.second.size[1]);
        
        resp["staticGeometry"]["entities"].push_back(entry);
    }
}
#endif

// for handler implem, see createScore.cc

}   // namespace na64dp

