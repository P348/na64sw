#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Finds maximum sample in SADC waveform
 *
 * This is simple straightforward max finder. Iterates over 32 samples finding
 * the greatest amplitude and writes this value and corresponding sample number
 * to `maxValue` and `maxSample` fields of SADC hit.
 *
 * \code{.yaml}
 *     - _type: SADCFindMaxSimple
 *       # optional, hit selection expression
 *       applyTo: ...
 * \endcode
 *
 * YAML config only accepts standard detector-by-name selection argument.
 *
 * \ingroup handlers sadc-handlers
 * \todo remove this simple one in favour of more generic selectMax
 * */
class SADCFindMaxSimple : public AbstractHitHandler<event::SADCHit> {
public:
    SADCFindMaxSimple( calib::Dispatcher &  ch
                     , const std::string & only )
                            : AbstractHitHandler<event::SADCHit>(ch, only) {}
    ~SADCFindMaxSimple(){}

    /// Locates maximum sample -- value and closes sample number, filling the
    /// hit's `maxValue` and `maxSample` fields.
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & hit ) override;
};

//                          * * *   * * *   * * *

bool
SADCFindMaxSimple::process_hit( EventID
                              , DetID did
                              , event::SADCHit & hit ) {
    NA64DP_ASSERT_EVENT_DATA( hit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    int maxSampleNo = 0;
    for( int i = 1; i < 32; ++i ) {
        if( hit.rawData->wave[i] > hit.rawData->wave[maxSampleNo] ) {
            maxSampleNo = i;
        }
    }
    hit.rawData->maxAmp = hit.rawData->wave[maxSampleNo];
    hit.rawData->maxSample = maxSampleNo;
    return true;
}

}  // namespace ::na64dp::handlers


REGISTER_HANDLER( SADCFindMaxSimple, ch, cfg
                , "Locates maximum sample in SADC waveform by direct lookup"
                  " of each sample's amplitude" ) {
    return new handlers::SADCFindMaxSimple( ch
                                          , aux::retrieve_det_selection(cfg)
                                          );
}

}



