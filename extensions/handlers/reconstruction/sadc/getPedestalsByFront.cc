#include "getPedestalsByFront.hh"

namespace na64dp {
namespace handlers {

bool
SADCGetPedestalsByFront::process_hit( EventID, DetID did
                                    , event::SADCHit & hit ) {
    NA64DP_ASSERT_EVENT_DATA( hit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    float p1 = hit.rawData->wave[0]
        , p2 = hit.rawData->wave[1]
        , ranges[2][2] = { {hit.rawData->wave[0], hit.rawData->wave[0]}
                         , {hit.rawData->wave[1], hit.rawData->wave[1]}
                         }
      ;

    for( int i = 1; i < _nFirstSamples; ++i ) {
        p1 += hit.rawData->wave[2*i  ];
        p2 += hit.rawData->wave[2*i+1];
        if( ranges[0][0] > hit.rawData->wave[2*i  ] ) ranges[0][0] = hit.rawData->wave[2*i  ];
        if( ranges[0][1] < hit.rawData->wave[2*i  ] ) ranges[0][1] = hit.rawData->wave[2*i  ];
        if( ranges[1][0] > hit.rawData->wave[2*i+1] ) ranges[1][0] = hit.rawData->wave[2*i+1];
        if( ranges[1][1] < hit.rawData->wave[2*i+1] ) ranges[1][1] = hit.rawData->wave[2*i+1];
    }
    p1 /= _nFirstSamples;
    p2 /= _nFirstSamples;

    if( _maxPedestalSpanning ) {
        struct {
            float & p;
            float * rPtr[2];
        } vs[2] = { {p1, ranges[0]}
                  , {p2, ranges[1]}
                  };

        for( int i = 0; i < 2; ++i ) {
            auto & c = vs[i];
            if( (*c.rPtr)[1] - (*c.rPtr)[0] <= _maxPedestalSpanning ) continue;
            // Seems to be affected by pile-up; iterate till the end, use
            // minimum value as a pedestal
            for( int j = i; j < 32; j += 2 ) {
                if( c.p > hit.rawData->wave[j] )
                    c.p = hit.rawData->wave[j];
            }
        }
    }

    hit.rawData->pedestals[0] = p1;
    hit.rawData->pedestals[1] = p2;
    return true;
}

}

REGISTER_HANDLER( SADCGetPedestalsByFront, ch, cfg
                , "calculate SADC pedestals by first N values (default is 4)" ) {
    return new handlers::SADCGetPedestalsByFront( ch
                , aux::retrieve_det_selection(cfg)
                , cfg["nSamples"] ? cfg["nSamples"].as<int>() : 4
                , cfg["useMinIfDeltaExceeds"] ? cfg["useMinIfDeltaExceeds"].as<int>() : 20
                );
}

}

