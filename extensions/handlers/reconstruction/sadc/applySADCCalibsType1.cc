#include "na64calib/dispatcher.hh"
#include "na64calib/manager.hh"
#include "na64detID/TBName.hh"
#include "na64detID/detectorID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64common/calib/msadc-PedLED.hh"
#include "na64detID/cellID.hh"

#include <libgen.h>

namespace na64dp {
namespace handlers {

using event::SADCHit;

struct CommonLEDCalib { double inSpill, offSpill; };
typedef std::unordered_map<DetID, CommonLEDCalib> LEDValues;

/**\brief Interfaces Donskov's LED calibration source
 *
 * This helper class subscribes its instances to updates of MSADC calibrations
 */
class LEDCorrectionsDonskov : 
    public calib::Handle< p348reco::MSADCCalibPedLedType1Collection<p348reco::MSADCCalibPedLedType1> > {
private:
    /// Logging category ref
    log4cpp::Category & _L;
    /// Reference to destination map to add the LED levels
    LEDValues & _coeffs;
    /// Reference to naming handle
    calib::Handle<nameutils::DetectorNaming> & _naming;
public:
    /// Shortcut for calibrations collection type
    typedef p348reco::MSADCCalibPedLedType1Collection<p348reco::MSADCCalibPedLedType1> LEDCalibs;
    /// Updates values at `_coeffs` based on provided calibrations data
    void handle_update(const LEDCalibs & collection) override {
        auto cnv = collection.get_mapped(_naming.get(), &_L);
        // todo: use selector here to selectively apply LED from different sources?
        for(auto & p : cnv) {
            _coeffs[p.first] = {p.second.inSpillLED, p.second.offSpillLED};
        }
    }

    LEDCorrectionsDonskov( const std::string & selection
                         , calib::Dispatcher & mgr
                         , LEDValues & dest
                         , calib::Handle<nameutils::DetectorNaming> & namingHandle
                         , log4cpp::Category & L
                         ) : calib::Handle<LEDCalibs>("default", mgr)
                           , _L(L)
                           , _coeffs(dest)
                           , _naming(namingHandle)
                           {}
};

static float gMeanSpillLED, gLEDRef;  // XXX
static EventID gEvID;

/**\brief Uses "standard" SADC calibrations from p348reco of SDC format
 *
 * \code{.yaml}
 *     - _type: ApplySADCCalibsType1
 *       # optional, hit selection expression
 *       applyTo: null
 *       # if set, will raise error if no calib data defined; bool, opt
 *       requireAllDefined: true
 *       # calibration data subclass; str, opt
 *       calibSubclass: "default"
 *       # Set this to apply LED corrections of type#1 (S.Donskov). Besides of
 *       # usual detector selection logic expression, accepts "none" (disables
 *       # LEDs corrections) and "all"; str, opt
 *       applyLEDType1To: none
 * \endcode
 *
 * May use LED corrections, if certain type is 
 *
 * \todo Currently selection expression for LEDs is not fully supported.
 *       Setting it to not "none" string will turn it on unconditionally
 * \ingroup handlers sadc-handlers calib-handlers
 * */
class ApplySADCCalibsType1 : public AbstractHitHandler<SADCHit>
                           , public calib::Handle< calib::Collection<calib::SADCCalib> >
                           {
private:
    ///\brief When set, forces handler to throw exception if calibration data
    ///       is not available
    ///
    /// Useful in selective mode, to control that certain (selected) detector
    /// entity is calibrated,
    bool _requireCalibs;
    /// Cached calibrations -- amplitude coefficient
    std::unordered_map<DetID, float> _calibs;
    /// Cached calibrations -- LED reference values, used only if LED of some
    /// type are provided
    std::unordered_map<DetID, float> _refLED;
    /// Set of files used for updates
    std::unordered_set<std::string> _usedSources;
    // ... TODO: list of missing calibs?

    /// Additional coefficients derived from LEDs, not used if empty
    LEDValues _corrections;
    /// Names of detector items with missed LEDs that were met during lifetime
    std::unordered_set<std::string> _missedLEDs;
    /// Names of detector items with missed LED reference values that were met
    /// during lifetime
    std::unordered_set<std::string> _missedLEDRefs;
    
    LEDCorrectionsDonskov * _donskovLEDs;
protected:
    ///\brief (Re-)caches calibration data index
    ///
    /// Converts SDC calibration entry for `p348reco` into `SADCCalib` object
    /// and places it into the `_calibs` index by corresponding name.
    void handle_update( const calib::Collection<calib::SADCCalib> & cc ) override {
        calib::Handle< calib::Collection<calib::SADCCalib> >::handle_update(cc);
        for(const auto & c : cc) {
            if(std::isnan(c.data.peakpos) )
                continue;  // omit no-calibration cases
            _usedSources.emplace(c.srcDocID);
            // convert name to detector ID
            DetID did = nameutils::sdc_id( naming(), c.data.name
                                         , c.data.x, c.data.y, c.data.z
                                         , c.srcDocID, c.lineNo //< optional
                                         );
            // copy the calibration data to the cache
            _calibs[did] = (float) (c.data.factor/c.data.peakpos);
            _refLED[did] = (float) c.data.refLED;
        }
    }

    double _correct_amplitude_coefficient(DetID & did) {
        if( _corrections.empty() ) return 1.;
        auto iit = _corrections.find(did);
        if(_corrections.end() == iit) {
            if(_requireCalibs) {
                NA64DP_RUNTIME_ERROR("No LED corrctions provided for %s"
                        , naming()[did].c_str());
            }  // otherwise, just memorize detector ID and leave cc as is
            _missedLEDs.insert(naming()[did]);
            return 1.;
        }
        // prefer in-spill LEDs
        double meanSpillLED = std::isnan(iit->second.inSpill)
                       ? iit->second.offSpill
                       : iit->second.inSpill
                       ;
        if(!(std::isnan(meanSpillLED) || meanSpillLED <= 20.) ) {  //< NOTE: le20.0 condition was introduced to match original S.Donskov's code
            auto iiit = _refLED.find(did);
            if(_refLED.end() == iiit || std::isnan(iiit->second)) {
                if(_requireCalibs) {
                    NA64DP_RUNTIME_ERROR("No LED reference value provided for %s"
                        , naming()[did].c_str());
                }  // otherwise, just memorize detector ID and leave cc as is
                _missedLEDRefs.insert(naming()[did]);
                return 1.;
            }
            // correct for LED; here, `meanSpillLED` value should contain
            // mean LED response over current burst. We renormalize
            // calibration coefficient `cc` by the ratio of reference
            // LED value obtained from Type#1 (M)SADC calibrations
            // and this mean value
            #if 0  // XXX
            CellID cid(did.payload());
            gLEDRef = iiit->second;
            gMeanSpillLED = meanSpillLED;
            //std::cout << " xxx "
            //    << iiit->second / meanSpillLED << " = "
            //    << iiit->second << " / " << meanSpillLED << std::endl;
            #endif
            return iiit->second / meanSpillLED;
            // Original line:
            // 2465 | if ((LedCorr || !offline) && (loc < led_cells) && (cell[loc].LedSp > 20.)) cale = cale * cell[loc].LedCal/cell[loc].LedSp;
        } else {
            // LED values are NaN. That must not be left unattended, mark
            // missing LED and leave value as is
            if(_missedLEDs.end() == _missedLEDs.find(naming()[did])) {
                _missedLEDs.insert(naming()[did]);
            }
            return 1.;
        }
    }
public:
    ApplySADCCalibsType1( calib::Dispatcher & cdsp
                        , const std::string & selection
                        , log4cpp::Category & logCat
                        , const std::string & selectionType1LEDs
                        , bool requireCalibs=true
                        , const std::string & calibClass="default"
                        , const std::string & namingSubclass="default"
                        ) : AbstractHitHandler<SADCHit>(cdsp, selection, logCat, namingSubclass)
                          , calib::Handle< calib::Collection<calib::SADCCalib> >(calibClass, cdsp)
                          , _requireCalibs(requireCalibs)  
                          , _donskovLEDs(nullptr)
                          {
        if( selectionType1LEDs != "none" ) {
            _donskovLEDs = new LEDCorrectionsDonskov(
                      selectionType1LEDs == "all" ? "" : selectionType1LEDs
                    , cdsp, _corrections, *this, _log);
        }
    }

    /**\brief Applies SADC calibration, if exists
     *
     * If no calibration data exists for certain detector entity and
     * `_requireCalibs` flag is set, throws an exception. If calibration data
     * exists, multiplies hit's maximum value by the calibration cache
     * ("peakpos"/"factor"). */
    bool process_hit( EventID eid, DetID did, SADCHit & cHit ) override {
        //NA64DP_ASSERT_EVENT_DATA( cHit.rawData, "Raw data for ..." );
        if( ! cHit.rawData ) return true;
        auto it = _calibs.find(did);
        if( _calibs.end() == it ) {
            if( !_requireCalibs ) {
                return true;  // continue processing
            }
            NA64DP_RUNTIME_ERROR( "No calibration data for SADC detector"
                    " \"%s\".", naming()[did].c_str());
        }
        event::StdFloat_t cc = cHit.rawData->calibCoeff = it->second;
        // Consider LED corrections
        cHit.rawData->ledCorr = _correct_amplitude_coefficient(did);
        #if 0
        if(eid != gEvID) {
            gEvID = eid;
            printf("    * * * EVENT %u/%u.%d * * *\n"
                , eid.run_no()
                , eid.spill_no()
                , eid.event_no()
                );  // XXX
        }
        if(std::make_pair(did.chip(), did.kin()) == naming().kin_id("ECAL")) {
            CellID cpl;
            cpl.cellID = did.payload();
            printf( "%dx%dx%d peds=(%.3f,%.3f) rawAmpMax=%.4f cal=%.2f LEDRef=%.4f LEDSp=%.4f; wf:"
                  , cpl.get_x(), cpl.get_y(), cpl.get_z()
                  , cHit.rawData->pedestals[0], cHit.rawData->pedestals[1]
                  , cHit.rawData->maxAmp, cc*1000
                  , gLEDRef, gMeanSpillLED
                  );
            for(int i = 0; i < 32; ++i) {
                printf(" %d", (int) cHit.rawData->wave[i]);
            }
            putc('\n', stdout);
        }
        #endif
        cHit.eDep = cc * cHit.rawData->ledCorr * cHit.rawData->maxAmp;
        return true;
    }

    /**\brief May print some warnings if inconsistencies were met
     *
     * Detects, if no calibrations were applied during handler lifetime, prints
     * short summary on sources with calibrations being used.
     * */
    void finalize() override {
        if(_usedSources.empty()) {
            _log << log4cpp::Priority::WARN
                << "No (M)SADC calibrations of type#1 were applied during"
                " handler lifetime.";
            return;
        }
        if(!_missedLEDs.empty()) {
            std::set<std::string> sortedNames(_missedLEDs.begin(), _missedLEDs.end());
            _log << log4cpp::Priority::WARN
                << "Following (M)SADC detectors had missing mean per-spill LED"
                  " values: "
                << util::str_join(sortedNames.begin(), sortedNames.end())
                << ".";
        }
        if(!_missedLEDRefs.empty()) {
            std::set<std::string> sortedNames(_missedLEDRefs.begin(), _missedLEDRefs.end());
            _log << log4cpp::Priority::WARN
                << "Following (M)SADC detectors had missing reference LED"
                  " values: "
                << util::str_join(sortedNames.begin(), sortedNames.end())
                << ".";
        }
        if(_log.getPriority() < log4cpp::Priority::INFO) return;
        std::map<std::string, std::set<std::string>> filesByDirs;
        for(const std::string & fullPath : _usedSources) {
            char * fullPathBf1 = strdup(fullPath.c_str())
               , * fullPathBf2 = strdup(fullPath.c_str())
               ;
            std::string dirPath = dirname(fullPathBf1)
                      , fileName = basename(fullPathBf2);
            auto ir = filesByDirs.emplace(dirPath, std::set<std::string>());
            ir.first->second.insert(fileName);
            free(fullPathBf1);
            free(fullPathBf2);
        }
        std::ostringstream oss;
        bool isFirst = true;
        for(const auto & dirEntry: filesByDirs) {
            if(isFirst) isFirst = false; else oss << "; ";
            oss << dirEntry.second.size() << " item(s) from dir " << dirEntry.first
                << ": " << util::str_join(dirEntry.second.begin(), dirEntry.second.end());
        }
        _log << log4cpp::Priority::INFO << "(M)SADC calibrations type#1 applied: "
            << oss.str().c_str();
    }
};  // ApplySADCCalibsType1

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( ApplySADCCalibsType1, cdsp, cfg
                , "Utilizes SADC calibration files via SDC wrapper. Calculates"
                  " energy deposition in various SADC-based detectors like"
                  " calorimeters, veto detectors, etc. Relies on `maxima'"
                  " field in SADC hit."
                ) {
    return new na64dp::handlers::ApplySADCCalibsType1( cdsp
            , na64dp::aux::retrieve_det_selection(cfg)
            , ::na64dp::aux::get_logging_cat(cfg)
            , cfg["applyLEDType1To"] ? cfg["applyLEDType1To"].as<std::string>() : "none"
            , cfg["requireAllDefined"] ? cfg["requireAllDefined"].as<bool>() : true
            , cfg["calibSubclass"] ? cfg["calibSubclass"].as<std::string>() : "default"
            , ::na64dp::aux::get_naming_class(cfg)
            );
}

