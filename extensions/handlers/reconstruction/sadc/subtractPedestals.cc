#include "subtractPedestals.hh"
#include <cmath>

namespace na64dp {
namespace handlers {

bool
SADCSubtractPedestals::process_hit( EventID
                                  , DetID detID
                                  , event::SADCHit & hit ) {
    if( !hit.rawData ) return true;  // no raw data associated with hit
    if( std::isnan(hit.rawData->pedestals[0]) || std::isnan(hit.rawData->pedestals[1]) ) {
        if( _errorOnMissed )
            NA64DP_RUNTIME_ERROR( "Missed pedestal value for \"%s\""
                                , naming()[detID].c_str() );
        return true;
    }
    for(int i = 0; i < 32; ++i){
        float v = hit.rawData->wave[i] -= hit.rawData->pedestals[i%2];
        if( _nullifyNegative && v < 0 ) {
            hit.rawData->wave[i] = 0;
        } else {
            hit.rawData->wave[i] = v;
        }
    }
    return true;
}

}

REGISTER_HANDLER( SADCSubtractPedestals, ch, yamlNode
                , "Subtract pedestal values from SADC waveforms." ) {
    return new handlers::SADCSubtractPedestals( ch
                , aux::retrieve_det_selection(yamlNode)
                , yamlNode["nullifyNegative"] ? yamlNode["nullifyNegative"].as<bool>() : true
                , yamlNode["errorOnMissed"] ? yamlNode["errorOnMissed"].as<bool>() : true
                );
}

}

