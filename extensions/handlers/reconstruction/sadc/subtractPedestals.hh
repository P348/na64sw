#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Subtracts SADC pedestal values written in hit instance from waveform
 *
 * This handler relies on presence of pedestal values in `pedestal` field of
 * `SADCHit` class. It will subtract corresponding (even or odd) pedestal value
 * from `waveform` entries of `SADCHit`, modifying its value.
 *
 * Use `SADCGetPedestalsByFront`, `MovStatPeds` or similar handler to retreive
 * the pedestal values for the hit(s).
 *
 *
 * \code{.yaml}
 *     - _type: SADCSubtractPedestals
 *       # negative samples to zero; bool, optional
 *       nullifyNegative: true
 *       # opt, generates error if pedestal value is missed; bool, optional
 *       errorOnMissed: true
 * \endcode
 *
 * \see SADCAppendPeds reverse transformation
 * \see SADCGetPedestalsByFront a handler assuming pedestals based on first N samples
 * \see MovStatPeds a handler finding avrg pedestals in "moving window"
 * \ingroup handlers sadc-handlers
 * */
class SADCSubtractPedestals : public AbstractHitHandler<event::SADCHit> {
private:
    /// If true, makes the negative amplitudes to become zero
    bool _nullifyNegative;
    /// If true, will throw runtime exception on missed
    bool _errorOnMissed;
public:
    SADCSubtractPedestals( calib::Dispatcher &  ch
                         , const std::string & selection
                         , bool nullifyNegative=false
                         , bool errorOnMissed=true
                         ) : AbstractHitHandler<event::SADCHit>(ch, selection)
                           , _nullifyNegative(nullifyNegative)
                           , _errorOnMissed(errorOnMissed) {}
    /// Modifies the hit accordingly
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & currentHit ) override;
};

}
}

