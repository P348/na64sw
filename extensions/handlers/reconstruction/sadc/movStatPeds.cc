#include "getPedestalsByFront.hh"
#include "subtractPedestals.hh"

#include "na64util/numerical/online.hh"
#include "na64calib/SADC/pedestals.hh"

#include <unistd.h>

namespace na64dp {
namespace handlers {

/**\brief Moving statistics pedestals calculus
 *
 * This handler implements more robust pedestals derivation than
 * straightforward algorithm `SADCGetPedestalsByFront` by involving windowed
 * (rolling, sliding) statistical state that takes into account some fixed
 * amount of pedestals being previously calculated. Particular
 * implementation of the sliding statistics is choosen by `iMovStats`
 * implementation supplied by ctr.
 *
 * \code{.yaml}
 *      - _type: SADCMovStatPeds
 *        applyTo: "kin == HCAL"
 *        nSamples: 4
 *        dropOutlierEvent: true
 *        scorer:
 *          type: klein
 *          window: 500
 *          excludeOutliers: true
 *          recacheOutliers: true
 *          recachingFrac: 0.
 * \endcode
 * 
 * YAML parameters:
 *  * `name` -- see `AbstractHitHandler`
 *  * `applyTo` -- see `AbstractHitHandler`
 *  * `nSamples` -- number of samples within the waveform to use (see
 *  `SADCGetPedestalsByFront`)
 *  * `dropOutlierEvent` -- whether to discard the hit and stop event
 *  propagation if hit is identified as an outlier.
 *  * `scorer` -- type of scorer to use. Possible values: `double` for plain
 *  scorer, fast but prone to catastrophic cancellation error or `klein` for
 *  more precise but slower one.
 *    * `window` -- number of hits stored for sliding stats calculus
 *    * `excludeOutliers` -- whether to exclude outliers
 *    (\f$x_i - \mu > 3 \sigma\f$) from statistics calculus
 *    * `recacheOutliers` -- whether to re-cache sums when accounted outliers
 *    leave the window of moving statistics
 *    * `recachingFrac` -- part (in the units of window) of stored events that
 *    makes scorer to recalculate moving statistics. E.g. for
 *    `recachingFrac=1.5` of `window=100`, every 150 events will cause
 *    re-caching of sums. Used as a side measure to circumvent the catastrophic
 *    cancellation. Set to 0 to prevent from this type of movstat recaching.
 *
 * \todo Support for `savedScorers` parameter -- a template path string
 * denoting the place where the scorer state has to be saved. Useful for
 * reentrant usage to stabilize sliding statistics during the initial read-out.
 *
 * \see SADCGetPedestalsByFront
 * \ingroup handlers sadc-handlers
 * */
class SADCMovStatPeds : public SADCGetPedestalsByFront
                      , public calib::Handle<calib::Pedestals> {
public:
    struct MovStatArgs {
        size_t winSize;
        double excludeOutliers, recacheOutliers;
        double recachingFrac;
    };
    struct MovStatEntry {
        numerical::iMovStats * stat[2];
    };
    typedef numerical::iMovStats * (*MovStatCtr)( const MovStatArgs & );
private:
    /// Sets threshold (in units of \f$\sigma\f$) for pedestals to be
    /// considered as pedestal; otherwise, scorer's mean value will be used
    double _dropOutlierEventThreshold;
    /// Constructor proxy for moving statistics insertion
    MovStatCtr _movStatEntryCtr;
    /// Common arguments
    const MovStatArgs _movStatArgs;
    /// Windowed (sliding) pedestal statistics, one entry per detector
    std::map<DetID_t, MovStatEntry> _stats;
    /// True if handler has to use average values before initial sum is
    /// accumulated.
    bool _useAvrgVals;
protected:
    /// Saves scorers by given template path argument, does not set `scorersAreSaved`.
    //void _save_scorers( const MovStatEntry & );  // XXX
public:
    /// Default ctr 
    SADCMovStatPeds( calib::Dispatcher & cdsp
                   , const std::string & only
                   , MovStatArgs & msArgs
                   , MovStatCtr msCtr
                   , unsigned char nfsa=4
                   , double dropOutlierEvent=0.
                   , bool avrgInit=false
                   ) : SADCGetPedestalsByFront(cdsp, only, nfsa)
                     , calib::Handle<calib::Pedestals>("pedestals", cdsp)
                     , _dropOutlierEventThreshold(dropOutlierEvent)
                     , _movStatEntryCtr(msCtr)
                     , _movStatArgs(msArgs)
                     , _useAvrgVals(avrgInit)
                     {}
    /// Computes average values and modifies the hit accordingly
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & currentHit ) override;
};

//                          * * *   * * *   * * *

bool
SADCMovStatPeds::process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & hit ) {
    NA64DP_ASSERT_EVENT_DATA( hit.rawData, "Raw SADC data for %s"
            , naming()[detID].c_str() );
    // Call usual dynamic pedestals calculus by first N samples.
    SADCGetPedestalsByFront::process_hit( eventID, detID, hit );
    auto pedEntryIt = _stats.find(detID);
    if( _stats.end() == pedEntryIt ) {
        // No moving statistics instance exists for current detector ID --
        // create one using the constructor function
        auto ir =
            _stats.emplace( detID, MovStatEntry{
                    { _movStatEntryCtr( _movStatArgs )
                    , _movStatEntryCtr( _movStatArgs )
                    }
                } );
        assert(ir.second);
        pedEntryIt = ir.first;
        // Try to retrieve existing pedestals values to mitigate initial
        // shiver
        if( _useAvrgVals ) {
            calib::Handle<calib::Pedestals> & pedsHandle = *this;
            if( pedsHandle->find(detID) != pedsHandle->end() ) {
                #if 0
                const calib::MovStatPedestals & p
                                = pedEntryIt->second.bgnState
                                = calibs()[detID].get<calib::MovStatPedestals>();
                msg_debug( _log, "Setting initial sliding state mock for \"%s\":"
                        " mean=%e, sigma=%e, N=%d"
                        , calibs().naming()(detID).c_str()
                        , p.mean, p.stdDev, (int) p.n );
                #else
                NA64DP_RUNTIME_ERROR( "TODO" );
                #endif
            }
        }
    }
    auto stat = pedEntryIt->second.stat;
    // Compare calculated pedestals with mean +/- stddev to figure out
    // outliers. If this hit is an outlier, depending on setting, either
    // discriminate an entire event, or use windowed mean as pedestal instead
    // of calculated one.
    bool useMean = false;
    for( int i = 0; i < 2; ++i ) {
        if( _dropOutlierEventThreshold
         && stat[i]->is_outlier(hit.rawData->pedestals[i], _dropOutlierEventThreshold) ) {
            // otherwise, use mean
            useMean = true;
        }
    }
    if( useMean ) {
        hit.rawData->pedestals[0] = stat[0]->mean();
        hit.rawData->pedestals[1] = stat[1]->mean();
    } else {
        stat[0]->account(hit.rawData->pedestals[0]);
        stat[1]->account(hit.rawData->pedestals[1]);
    }
    return true;
}

static numerical::iMovStats *
plain_ctr( const SADCMovStatPeds::MovStatArgs & a ) {
    return new numerical::MovingStats<double>( a.winSize
                , a.excludeOutliers
                , a.recacheOutliers
                , a.recachingFrac
                );
}

static numerical::iMovStats *
klein_ctr( const SADCMovStatPeds::MovStatArgs & a ) {
    return new numerical::MovingStats<numerical::KleinScorer>( a.winSize
                , a.excludeOutliers
                , a.recacheOutliers
                , a.recachingFrac
                );
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( SADCMovStatPeds, ch, cfg
                , "calculate SADC pedestals by first N values (default is 4)"
                  " and compare it with windowed mean/stddev values to"
                  " drop outliers" ) {
    handlers::SADCMovStatPeds::MovStatArgs a;
    a.winSize = cfg["scorer"]["window"].as<size_t>();
    a.excludeOutliers = cfg["scorer"]["excludeOutliers"].as<double>();
    a.recacheOutliers = cfg["scorer"]["recacheOutliers"].as<double>();
    a.recachingFrac = cfg["scorer"]["recachingFrac"].as<double>();
    handlers::SADCMovStatPeds::MovStatCtr msctr;
    if( cfg["scorer"]["type"].as<std::string>() == "klein" ) {
        msctr = handlers::klein_ctr;
    } else if( cfg["scorer"]["type"].as<std::string>() == "plain" ) {
        msctr = handlers::plain_ctr;
    } else {
        NA64DP_RUNTIME_ERROR( "Wrong mov. stat. scorer type"
                " specification: \"%s\"."
                , cfg["scorer"]["type"].as<std::string>().c_str() );
    }

    return new handlers::SADCMovStatPeds( ch
                , aux::retrieve_det_selection(cfg)
                , a
                , msctr
                , cfg["nSamples"] ? cfg["nSamples"].as<int>() : 4
                , cfg["dropOutlierEvent"] ? cfg["dropOutlierEvent"].as<double>() : 0.
                , cfg["initWithAverages"] ? cfg["initWithAverages"].as<bool>() : false
                );
}

}

