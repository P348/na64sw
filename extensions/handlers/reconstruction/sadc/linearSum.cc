#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Computes linear sum of SADC waveform
 *
 * This handler calculates the integral approximation ("sum") on the SADC hit
 * waveform by taking mean values between two adjacent sample values. It may
 * be assumed as more precise estimation of the waveform integral than direct
 * sum of samples.
 *
 * ~~~{.yaml}
 *     - _type: SADCLinearSum
 *       # negative samples will be zeroed, bool, optional
 *       zeroNegativeValues: true
 * ~~~
 *
 * Accepts single parameter "zeroNegativeValues:bool" that, when set to `true'
 * makes the handler to set negative samples to 0. Can be omitted, default
 * value is `false'.
 *
 * \see SADCDirectSum
 * \ingroup handlers sadc-handlers
 * */
class SADCLinearSum : public AbstractHitHandler<event::SADCHit> {
private:
    bool _doZeroNegative;
public:
    SADCLinearSum( calib::Dispatcher & ch
                 , const std::string & only
                 , bool doZeroNegative=false ) : AbstractHitHandler<event::SADCHit>(ch, only)
                                               , _doZeroNegative(doZeroNegative)
                                               {}
    virtual bool process_hit( EventID, DetID
                            , event::SADCHit & currentHit);
};

//                          * * *   * * *   * * *

bool
SADCLinearSum::process_hit( EventID, DetID did
                          , event::SADCHit & cHit ){
    NA64DP_ASSERT_EVENT_DATA( cHit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    cHit.rawData->sum = 0;
    for( int i = 0; i < 30; ++i ){
        auto a = cHit.rawData->wave[i]
           , b = cHit.rawData->wave[i + 1]
           ;
        if( _doZeroNegative ) {
            if( a < 0 ) a = 0;
            if( b < 0 ) b = 0;
        }
        cHit.rawData->sum += ( a + b ) / 2; 
    }
    return true;
}

}

REGISTER_HANDLER( SADCLinearSum, ch, yamlNode
                , "Computes linear sum of SADC waveform" ){
    bool zeroNegatives = yamlNode["zeroNegativeValues"] ? yamlNode["zeroNegativeValues"].as<bool>()
                                                        : false;
    return new handlers::SADCLinearSum( ch
            , aux::retrieve_det_selection(yamlNode)
            , zeroNegatives );
}

}
