#include "na64dp/abstractHitHandler.hh"
#include "na64common/calib/sdc-integration.hh"

namespace na64dp {
namespace handlers {

/**\brief Applies time cut for SADC hits
 *
 * This handler uses Type#1 SADC calibration information to identify hits
 * with maxima value being off from (assumed) average values. It is
 * parameterised with width (in terms of RMS) of window for permitted values.
 *
 * \note `isRelativeToMaster` calibration parameter is interpreted as whether
 *       "time provided in calibrations is with respect to master" (i.e. master
 *       time has been accounted by calibration).
 * */
class SADCCutOutOfTimeHits : public AbstractHitHandler<event::SADCHit>
                           , public calib::Handle< calib::Collection<calib::SADCCalib> >
                           {
public:
    struct TimeCutCalib {
        float tMean, tSigma;
        bool isRelativeToMaster;
    };
protected:
    /// Keeps sigma time value for certain detector
    std::unordered_map<DetID, TimeCutCalib > _calibs;
    /// N sigma (width) to tolerate
    float _nSigma;
    ///\brief (Re-)caches calibration data index (time sigmas)
    ///
    /// Converts SDC calibration entry for `p348reco` into `SADCCalib` object
    /// and places it into the `_calibs` index by corresponding name.
    void handle_update( const calib::Collection<calib::SADCCalib> & cc ) override {
        calib::Handle< calib::Collection<calib::SADCCalib> >::handle_update(cc);
        std::unordered_set<std::string> docIDs;  // for debug
        for(const auto & c : cc) {
            if( std::isnan(c.data.time) )
                continue;  // omit no-calibration cases
            // convert name to detector ID
            DetID did = nameutils::sdc_id( naming(), c.data.name
                                         , c.data.x, c.data.y, c.data.z
                                         , c.srcDocID, c.lineNo //< optional
                                         );
            // copy the calibration data to the cache
            _calibs[did] = TimeCutCalib{ c.data.time
                                       , c.data.timesigma
                                       , c.data.is_relative
                                       };
        }
    }
public:
    SADCCutOutOfTimeHits( calib::Dispatcher & cdsp
                        , const std::string & selection
                        , float nSigma
                        , log4cpp::Category & logCat
                        , const std::string & calibClass="default"
                        , const std::string & namingSubclass="default"
                        ) : AbstractHitHandler<event::SADCHit>(cdsp, selection, logCat, namingSubclass)
                          , calib::Handle< calib::Collection<calib::SADCCalib> >(calibClass, cdsp)
                          , _nSigma(nSigma)
                          {}
    bool process_hit( EventID, DetID did, event::SADCHit & cHit ) override {
        //NA64DP_ASSERT_EVENT_DATA( cHit.rawData, "Raw data for ..." );
        if( ! cHit.rawData ) return true;
        auto it = _calibs.find(did);
        if( _calibs.end() == it ) {
            return true;  // continue processing
        }
        float timeDiff = cHit.time;
        if( it->second.isRelativeToMaster ) {
            timeDiff -= _current_event().masterTime;
        }
        timeDiff -= it->second.tMean;
        timeDiff = fabs(timeDiff);
        timeDiff /= it->second.tSigma;
        if( timeDiff > _nSigma )
            this->_set_hit_erase_flag();
        log().debug( "%s cut out-of-time check comparing t=%.3e vs t0=%.3e, tolernace=%.2f*%.2f=%.3e (%s)"
               , naming()[did].c_str()
               , it->second.isRelativeToMaster ? cHit.time - _current_event().masterTime : cHit.time
               , it->second.tMean
               , _nSigma, it->second.tSigma
               , _nSigma*it->second.tSigma
               , timeDiff > _nSigma ? "discr." : "passed"
               );
        return true;
    }
};

}
}

REGISTER_HANDLER( SADCCutOutOfTimeHits, cdsp, cfg
                , "Discriminates SADC events/hits based on timing calibration (type1)."
                ) {
    return new na64dp::handlers::SADCCutOutOfTimeHits( cdsp
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["nSigma"] ? cfg["nSigma"].as<float>() : 4.0
            , ::na64dp::aux::get_logging_cat(cfg)
            , cfg["calibSubclass"] ? cfg["calibSubclass"].as<std::string>() : "default"
            , ::na64dp::aux::get_naming_class(cfg)
            );
}


