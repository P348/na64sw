#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2F.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Combined plot of the time estimations for maxima
 *
 * \code{.yaml}
 *     - _type: SADCPlotMaxs
 *       # number of bins for vertical axis of 2D histogram; int, required
 *       ampNBins: ...
 *       # histogram range for vertical axis; list of two floats, required
 *       ampRange: [..., ...]
 *       # histogram's base name; str required
 *       baseName: ...
 * \endcode
 *
 * \ingroup handlers sadc-handlers
 * */
class SADCPlotMaxs : public util::TDirAdapter
                   , public AbstractHitHandler<event::SADCHit> {
private:
    /// Number of bins by vertical axis
    size_t _ampNBins;
    /// Amplitude range bounds
    double _ampRange[2];
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::map<DetID_t, TH2F *> _hists;
public:
    /// Main ctr of the handler
    SADCPlotMaxs( calib::Dispatcher & ch
                , const std::string & selection
                , size_t nAmpBins
                , double ampMin, double ampMax
                , const std::string & hstBaseName="maxAmp"
                );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADCPlotMaxs();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & currentEcalHit) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

SADCPlotMaxs::SADCPlotMaxs( calib::Dispatcher & ch
                          , const std::string & selection
                          , size_t nAmpBins
                          , double ampMin, double ampMax
                          , const std::string & baseName
                          ) : util::TDirAdapter(ch)
                            , AbstractHitHandler(ch, selection)
                            , _ampNBins(nAmpBins)
                            , _ampRange{ampMin, ampMax}
                            , _hstBaseName(baseName) {
}

bool
SADCPlotMaxs::process_hit( EventID
                         , DetID did
                         , event::SADCHit & currentHit) {
    if( !currentHit.rawData ) return true;  // no raw data associated with hit
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        this->log().debug("Creating %s (for %zu)", histName.c_str(), did.id );
        TH2F * newHst = new TH2F( histName.c_str()
                                , "Maxima vs time; Channels; Amplitude"
                                , 256, 0, 480
                                , _ampNBins, _ampRange[0], _ampRange[1] );
        auto ir = _hists.emplace( did, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }
    // fill the histogram
    for( auto & c : currentHit.rawData->maxima ) {
        if(!std::isfinite(c.second->time)) continue;
        it->second->Fill(c.second->time, currentHit.rawData->wave[c.first]);
    }
    return true;
}

void
SADCPlotMaxs::finalize() {
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

SADCPlotMaxs::~SADCPlotMaxs() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( SADCPlotMaxs, ch, cfg
                , "Builds 2D histograms of maxs vs time per SADC hit" ) {
    return new handlers::SADCPlotMaxs( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["ampNBins"].as<int>()
            , cfg["ampRange"][0].as<float>()
            , cfg["ampRange"][1].as<float>()
            , cfg["baseName"].as<std::string>()
            );
}

}
