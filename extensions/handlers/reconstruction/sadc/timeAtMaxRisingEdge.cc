#include "na64dp/abstractHitHandler.hh"
#include "na64detID/cellID.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

using event::SADCHit;

/**\brief Calculates time estimations for the peak maxima for SADC waveform
 *        by "rising edge" algorithm
 *
 * Relies on `RawDataSADC::maxima` field of SADC hit to find out time
 * estimations for the mixima (peaks) of sadc waveform.
 *
 * \code{.yaml}
 *     - _type: SADCTimeAtMaxByRisingEdge
 *       # optional, hit selection expression
 *       applyTo: null
 *       # A threshold for rising edge. Maxima with lower inclination won't be
 *       # considered; float, opt
 *       risingEdgeThreshold: 0.5
 *       # enables "half height" mode; bool, opt
 *       halfHeight: false
 *       # Use this value as time interval between two samples (nanoseconds);
 *       # float, optional
 *       nsPerSample: 12.5
 * \endcode
 *
 * Time estimations are in nanoseconds, the scaling unit (nanoseconds per
 * sample) is defined by parameter "nsPerSample".
 *
 * May set `NaN` for those peaks where algorithm failed to find rising edge
 * with given threshold.
 *
 * \ingroup handlers, sadc-handlers
 * */
class SADCTimeAtMaxByRisingEdge : public AbstractHitHandler<SADCHit> {
protected:
    /// A rising edge threshold for amplitude lookup
    float _risingEdgeThreshold;
    /// A rising edge half-height point position will be taken, if enabled,
    /// instead of the beginning of rising edge
    bool _risingEdgeHH;
    /// Nanoseconds per sample
    float _nsPerSample;

    ///\brief Peak time information calculated by the rising edge
    ///
    /// It was shown to be one of the efficient methods for noise suppression
    /// in terms of efficiency/precision by IHEP people. For reference, see
    /// Donskov's talk:
    ///     
    ///     https://indico.cern.ch/event/569549/#26-signal-time-extraction
    ///
    /// It is widely used to determine SADC time in NA64 on SADC-based
    /// detectors.
    ///
    /// \note For multiple peaks it is possible to have the same time value
    ///       estimaton.
    bool _peak_time__rising_edge( event::RawDataSADC & rd );
public:
    SADCTimeAtMaxByRisingEdge( calib::Dispatcher & cdsp
                 , const std::string & only
                 , float risingEdgeThreshold
                 , log4cpp::Category & logCat
                 , bool halfHeight=false
                 , float nsPerSample=12.5
                 , const std::string & detNamingClass="default"
                 ) : AbstractHitHandler<SADCHit>(cdsp, only, logCat, detNamingClass)
                   , _risingEdgeThreshold(risingEdgeThreshold)
                   , _risingEdgeHH(halfHeight)
                   , _nsPerSample(nsPerSample)
                   {}

    /// Applies selected method to raw data of SADC hit filling
    /// up `RawDataSADC::maximaTimes`.
    bool process_hit( EventID, HitKey k, SADCHit & cHit) {
        if( !cHit.rawData ) return true;  // do noting if no raw data
        #if 0
        std::cout << naming()[k] << ", peaks: ";
        for( auto * c = cHit.rawData->maxima
           ; *c != std::numeric_limits<uint8_t>::max()
           ; ++c ) {
            std::cout << (int) *c << "(" << cHit.rawData->wave[*c] << "), ";
        }
        std::cout << std::endl;
        #endif
        _peak_time__rising_edge(*cHit.rawData);
        #if 0
        std::cout << naming()[k] << ", time(s): ";
        const float * maxT = cHit.rawData->maximaTimes;
        for( auto * c = cHit.rawData->maxima
           ; *c != std::numeric_limits<uint8_t>::max()
           ; ++c, ++maxT ) {
            std::cout << (int) *c << "(" << *maxT << "), ";
        }
        std::cout << std::endl;
        // ---
        #endif
        return true;
    }
};  // class SADCTimeAtMaxByRisingEdge

bool
SADCTimeAtMaxByRisingEdge::_peak_time__rising_edge( event::RawDataSADC & rd ) {
    bool atLeastOneFound = false;
    // iterate over the maxima field, for each maxima (peak) value
    const float noiseThr = 1.;  // TODO: make it external parameter
    for( auto & maximum : rd.maxima ) {
        // set the current "time of max" to be NaN
        maximum.second->time = std::nan("0");

        // do not consider maxima at 1st sample
        if(0 == maximum.first) continue;
        // get the threshold value wrt current max
        float aMin = rd.wave[maximum.first];
        float thr = rd.wave[maximum.first]*_risingEdgeThreshold;
        // omit 0 amplitude in sample; also prevents segfault for lookbehind
        if( !thr ) continue;
        // look previous sample, till amplitude will drop below the threshold
        // needed
        int i;
        // iterate samples backwards, until ...
        for(i = maximum.first - 1; i; --i) {
            if( rd.wave[i] > aMin + noiseThr ) break;  // slope ended
            if( aMin < rd.wave[i] ) aMin = rd.wave[i];  // sic! (useless?)
            if( rd.wave[i] > thr ) continue;  // not below threshold yet
            if( rd.wave[i+1] <= rd.wave[i] ) continue;  // segment is not rising
            // get the intersection and write it into "time" of the peak
            maximum.second->time = (i+1) - rd.wave[i+1]/float(rd.wave[i+1] - rd.wave[i]);
            if(_risingEdgeHH)
                maximum.second->time += .5*rd.wave[maximum.first]/float(rd.wave[i+1] - rd.wave[i]);  // TODO: check
            // rescale time from samples# units to nsec
            maximum.second->time *= _nsPerSample;
            atLeastOneFound = true;
            break;  // found
        }
    }
    return atLeastOneFound;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCTimeAtMaxByRisingEdge, cdsp, cfg
                , "Calculates time estimations for the peak maxima for SADC"
                  " waveform using forward front (\"rising edge\")." ) {
    return new na64dp::handlers::SADCTimeAtMaxByRisingEdge( cdsp 
                , na64dp::aux::retrieve_det_selection(cfg)
                , cfg["risingEdgeThreshold"] ? cfg["risingEdgeThreshold"].as<float>() : .5
                , ::na64dp::aux::get_logging_cat(cfg)
                , cfg["halfHeight"] ? cfg["halfHeight"].as<bool>() : false
                , cfg["nsPerSample"] ? cfg["nsPerSample"].as<float>() : 12.5
                , ::na64dp::aux::get_naming_class(cfg)
                );
}

