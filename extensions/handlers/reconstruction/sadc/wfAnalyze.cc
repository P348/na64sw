#include "na64dp/abstractHitHandler.hh"
#include "../../generic/plotsSet.hh"

#include <limits>
#include <cmath>

#include <TH1I.h>
#include <TH2F.h>

namespace na64dp {
namespace handlers {

/**\brief Development handler.
 *
 * This handler is not intended for general usage; it's a development prototype
 * for finer waveform inspection.
 *
 * \note To be (re)moved.
 *
 * \ingroup handlers sadc-handlers dev-handlers
 * */
class SADCWfAnalyze : public AbstractHitHandler<event::SADCHit> {
private:
    /// Extrema (multiplicity) 1D hst
    PlotsPerID<TH1I, DetID> _extrema;
    /// (Discrete) derivative w/f hsts
    PlotsPerID<TH2F, DetID> _wfDeriv;
    /// Inclination angle at extrema
    PlotsPerID<TH2F, DetID> _inclination;


    /// Minimal sum threshold to consider
    float _sumThreshold;
    /// If set, hits with sums below threshold will be removed
    bool _removeZeroLikeHit;
    /// Derivative
    float _derivative[31];

    /// Histograms with extrema multiplicity (per entity)
    //std::unordered_map<DetID, TH1I*> _extremaHsts;
    /// Histograms with derivative plot (per entity)
    //std::unordered_map<DetID, TH1I*> _extremaHsts;
public:
    SADCWfAnalyze( calib::Dispatcher & dsp
                 , const std::string & only
                 , float sumThreshold
                 , bool removeZeroLikeHits
                 , const std::string & maximaHstBaseName
                 , const std::string & maximaHstDescr
                 , const std::string & derivHstBaseName
                 , const std::string & derivHstDescr
                 ) : AbstractHitHandler<event::SADCHit>(dsp, only)
                   , _extrema( dsp
                             , maximaHstBaseName
                             , maximaHstDescr)
                   , _wfDeriv( dsp
                             , derivHstBaseName
                             , derivHstDescr)
                   , _inclination( dsp
                                 , "inclination"
                                 , "Inclination to extrema "
                                 )
                   , _sumThreshold(sumThreshold)
                   , _removeZeroLikeHit(removeZeroLikeHits)
                   {}
    ~SADCWfAnalyze(){}

    /// Locates maximum sample -- value and closest sample number, filling the
    /// hit's `maxValue` and `maxSample` fields.
    bool process_hit( EventID eventID
                    , DetID detID
                    , event::SADCHit & hit ) override;
    /// Writes histograms
    void finalize() override;
};  // class SADCWfAnalyze

//                      * * *   * * *   * * *

using event::SADCHit;

bool
SADCWfAnalyze::process_hit( EventID
                          , DetID did
                          , SADCHit & hit ) {
    // if no raw data, do not process this hit
    if( (!hit.rawData) || std::isnan(hit.rawData->wave[0]) ) return true;  // no raw data in hit
    // if no sum calculated, do not process this hit
    if( std::isnan(hit.rawData->sum) ) return true;  // no sum calcd by previous handler
    // otherwise, compare hit's
    if( !std::isnan( _sumThreshold ) ) {
        if( hit.rawData->sum < _sumThreshold ) {
            // remove hit with zero-like sum, if need
            if( _removeZeroLikeHit ) _set_hit_erase_flag();
            return true;
        }
    }
    // calulate derivative, find maxima, extrema and absolute max value
    int nMaxSample = 0;
    float maxAmp = hit.rawData->wave[nMaxSample];
    float deriv[31];
    int extrema[31];  // TODO: make use of it
    int nExtrema = 0;

    auto inclHst = _inclination.get_or_create( did
                                             , 31, 1, 32
                                             , 100, -90, 90
                                             );

    for( int i = 1; i < 32; ++i ) {
        // calc deriv
        deriv[i-1] = hit.rawData->wave[i] - hit.rawData->wave[i-1];
        // update abs max sample
        if( deriv[i] > maxAmp ) {
            maxAmp = deriv[i];
            nMaxSample = i;
        }
        // if `deriv' crosses 0, memorize extrema
        if( i && ((deriv[i-2] >= 0) == (deriv[i-1] >= 0) )) {
            extrema[nExtrema++] = i;
            inclHst->Fill(i, std::atan2(1, deriv[i])*180/M_PI );
        }
    }

    _extrema.get_or_create(did, 31, 0, 31)->Fill(nExtrema);
    auto p = _wfDeriv.get_or_create( did
                , 31, 0, 31
                , 5000, -25000, 25000
                );
    for(int i = 0; i < 31; ++i) {
        p->Fill(i, deriv[i]);
    }
    
    return true;
}

void
SADCWfAnalyze::finalize() {
    _extrema.finalize();
    _wfDeriv.finalize();
    _inclination.finalize();
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCWfAnalyze, ch, cfg
                , "(under development) ..." ) {
    return new na64dp::handlers::SADCWfAnalyze( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["sumThreshold"] ? cfg["sumThreshold"].as<float>() : std::nan("0")
            , cfg["removeZeroLikeHits"] ? cfg["removeZeroLikeHits"].as<bool>() : false
            , cfg["extremaBasename"] ? cfg["extremaBasename"].as<std::string>() : "extremaNo"
            , cfg["extremaDescr"] ? cfg["extremaDescr"].as<std::string>() : "Number of extrema"
            , cfg["derivHstBasename"] ? cfg["derivHstBasename"].as<std::string>() : "wfDeriv"
            , cfg["derivHstDescr"] ? cfg["derivHstDescr"].as<std::string>() : "Waveform derivative"
            //, cfg["removeFailedHits"] ? false : cfg["removeFailedHits"].as<bool>()
            );
}

