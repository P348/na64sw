#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64common/calib/sdc-integration.hh"
#include "na64detID/cellID.hh"

#include <cmath>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <unordered_set>

namespace na64dp {
namespace handlers {

using event::SADCHit;

/**\brief Takes maximum (peak) value from SADC waveform
 *
 * Retrieves the global maximum peak position within a given SADC waveform
 * (globally, within required samples range only, or within range given by
 * timing sigma from calib info) and writes it to `RawDataSADC::maxAmp` and
 * its sample to `RawDataSADC::maxSample` of SADCHit's `SADCHit::rawData`.
 *
 * \code{.yaml}
 *     - _type: SADCSelectMax
 *       # optional, hit selection expression
 *       applyTo: null
 *       # Which methods to apply to pick up certain maximum, in order of
 *       # preference; list of strings, required
 *       methods: [...]
 *       # What to do if none of the methods succeed with maximum selection.
 *       # Possible choices: "ignore", "warn", "removeHit", "discriminateEvent",
 *       # "emitError"; str, optional
 *       onFailure: "emitError"
 *       # When set, makes handler do nothing in events when no master time
 *       # is set; bool, optional
 *       tolerateNoMaster: false
 *       # Makes handler to consider only maxima values in this range (sample
 *       # number); list of two ints, optional
 *       lookupRange: [0, 255]
 * \endcode
 *
 * Requires set of maxima (SADCHit's `RawData::maxima`) to be filled (for
 * instance, by handler `SADCFindMaxima`). If time-selection method is
 * involved, peak timing info must be available in the hit as well
 * (`RawDataSADC::maximaTimes`, filled, for instance with
 * `SADCTimeAtMaxByRisingEdge` handler).
 *
 * Methods:
 * 
 *  - "closestTime" -- tries to use calibration information about average peak
 *    position with respect to master time/own mean time of the detector.
 *    Note, that if master time is not set and corresponding calibration time
 *    relies on it, behaviour varies depending on option "onFailure" parameter.
 *    Affected by "lookupRange".
 *  - "maxAmp" -- just takes maximum peak position (in "lookupRange", if
 *    set).
 *
 * \note Somewhat analoguous to `Cell::peak_BestTiming()` and
 *       `Cell::peak_Best()` of `p348reco`.
 *
 * \note sets `SADCHit::time` to _uncalibrated_ value (in sample units). This
 *       was decided because some peak selection algorithms sometimes
 *       provide a good timing estimation with precision better than integer
 *       sample number.
 *
 * \ingroup handlers sadc-handlers
 *
 * \todo (minor opt) Respect "applyTo" in `handle_update()`
 * */
class SADCSelectMax : public AbstractHitHandler<SADCHit>
                    , public calib::Handle< calib::Collection<calib::SADCCalib> >
                    {
public:
    /// Time calibration entry
    struct TimeCalibEntry {
        /// Was this time measurement done relatively to the master time?
        bool isRealtiveToMasterTime;
        float meanTime  ///< mean time measurement for the detector
            , timeSigma  ///< sigma of the mean time measurement
            ;
    };

    enum OnFailure {
        kIgnore,
        kWarn,
        kRemoveHit,
        kDiscriminateEvent,
        kEmitError
    };
private:
    /// Cache of the timing calibrations
    std::unordered_map<DetID, TimeCalibEntry> _calibs;
    /// Methods for the peak selection; if one returns `false`, next will be
    /// taken
    std::vector<bool (SADCSelectMax::*)(DetID, SADCHit &)> _methods;
    /// Will make handler to throw an exception if no max found
    OnFailure _mode;
    /// Makes "closestTime" to generate error if calibs require master time and
    /// it is not available.
    bool _tolerateNoMaster;
    /// Lookup range (sample units)
    uint8_t _rng[2];
protected:
    void handle_update( const calib::Collection<calib::SADCCalib> & cc ) override {
        calib::Handle<calib::Collection<calib::SADCCalib>>::handle_update(cc);
        std::unordered_set<std::string> docIDs;  // for debug
        for( auto & c : cc ) {
            if(std::isnan(c.data.time)) return;  // no time calibration available
            // convert `name' to detector ID
            DetID did = nameutils::sdc_id( naming(), c.data.name
                                         , c.data.x, c.data.y, c.data.z
                                         , c.srcDocID, c.lineNo //< optional
                                         );
            if(  SelectiveHitHandler<SADCHit>::is_selective() 
             && !SelectiveHitHandler<SADCHit>::matches(did)
              ) {
                // do not recache detectors excluded by "applyTo" expression
                continue;
            }
            // copy the calibration data to the cache
            _calibs[did] = TimeCalibEntry{ c.data.is_relative  // isRealtiveToMasterTime
                                         , c.data.time  // meanTime
                                         , c.data.timesigma  // timeSigma
                                         };
            docIDs.emplace(c.srcDocID);
        }
        for( const auto & p : docIDs ) {
            _log.debug( "Updated SADC timing calib from \"%s\" (%zu entries)."
                      , p.c_str()
                      , cc.size()
                      );
        }
    }

    // criteria
    /// "maxAmp" method -- just takes sample greatest amplitude
    bool _criterion__max_amp(DetID, SADCHit & h);
    /// "closestTime" methods -- looks peak closest to expected
    ///
    /// Requires expected mean time to be available in the calibration info.
    /// If calibration time is defined with respect to master time and master
    /// time is not available and "tolerateNoMaster" is not set, will raise
    /// a runtime error.
    bool _criterion__closest_time(DetID, SADCHit & h);
    // ...
public:
    SADCSelectMax( calib::Dispatcher & cdsp
                 , const std::string & only
                 , const std::vector<std::string> methods
                 , log4cpp::Category & logCat
                 , OnFailure mode=kEmitError
                 , bool tolerateNoMaster=false
                 , uint8_t rngMin=0
                 , uint8_t rngMax=std::numeric_limits<uint8_t>::max()
                 , const std::string & caloCalibSubclass="default"
                 ) : AbstractHitHandler<SADCHit>( cdsp, only
                        , logCat )
                   , calib::Handle< calib::Collection<calib::SADCCalib> >(
                                caloCalibSubclass, cdsp )
                   , _mode(mode)
                   , _tolerateNoMaster(tolerateNoMaster)
                   , _rng{rngMin, rngMax}
                   {
        for( const auto & mNm : methods ) {
            if( "closestTime" == mNm ) {
                _methods.push_back(&SADCSelectMax::_criterion__closest_time);
            } else if( "maxAmp" == mNm ) {
                _methods.push_back(&SADCSelectMax::_criterion__max_amp);
            }
            // else if(...)
            else {
                NA64DP_RUNTIME_ERROR( "Unknown criterion name: \"%s\""
                                    , mNm.c_str());
            }
        }
    }

    /// Selects the peak according to the current settings.
    bool process_hit( EventID
                    , HitKey k
                    , SADCHit & cHit) override;
};  // class SADCSelectMax

bool
SADCSelectMax::process_hit( EventID eid
                          , HitKey k
                          , SADCHit & cHit) {
    NA64DP_ASSERT_EVENT_DATA( cHit.rawData,
            "No raw data for %s", naming()[k].c_str() );
    if(cHit.rawData->maxima.empty()) {
        if( kEmitError == _mode ) {
            NA64DP_RUNTIME_ERROR( "No peaks found for"
                    " MSADC detector %s at event %s."
                    , naming()[k].c_str()
                    , eid.to_str().c_str()
                    );
        } else if( kWarn == _mode ) {
            _log.warn( "No peaks found for"
                    " MSADC detector %s at event %s."
                    , naming()[k].c_str()
                    , eid.to_str().c_str()
                    );
        } else if( kRemoveHit == _mode ) {
            _set_hit_erase_flag();
        } else if( kDiscriminateEvent == _mode ) {
            _set_event_processing_result( AbstractHandler::kDiscriminateEvent );
            return false;  // no need to iterate other hits in an event
        }
        return true;  // proceed with other hits
    }

    // apply peak-finding methods
    for( const auto & m : _methods ) {
        if((this->*m)(k, cHit)) {
            return true;
        }
    }

    if( kEmitError == _mode ) {
        NA64DP_RUNTIME_ERROR( "None peak-selection method was able to"
                " find best peak for %s for event %s."
                , naming()[k].c_str()
                , eid.to_str().c_str()
                );
    } else if( kWarn == _mode ) {
        _log.warn( "None peak-selection method was able to"
                " find best peak for %s for event %s."
                , naming()[k].c_str()
                , eid.to_str().c_str()
                );
    } else if( kRemoveHit == _mode ) {
        _set_hit_erase_flag();
    } else if( kDiscriminateEvent == _mode ) {
        _set_event_processing_result(AbstractHandler::kDiscriminateEvent);
        if( log4cpp::Priority::DEBUG <= _log.getPriority() ) {
            _log.debug("Event %s discriminated due to max peak selection failure"
                    " in detector %s by handler %p"
                    , eid.to_str().c_str()
                    , naming()[k].c_str()
                    , this );
        }
    }
    return true;
}

bool
SADCSelectMax::_criterion__max_amp(DetID, SADCHit & h) {
    uint8_t nMaxSample = 0;
    // iterate over found maxima and take one with largest amp
    for( auto & maximum : h.rawData->maxima ) {
        assert( maximum.first >= 0 );
        assert( static_cast<size_t>(maximum.first) < sizeof(h.rawData->wave)/sizeof(*h.rawData->wave) );

        if( _rng[0] >  maximum.first ) continue;
        if( _rng[1] <= maximum.first ) continue;
        if( !std::isfinite(maximum.second->time) ) continue;  // omit peaks without time

        if( h.rawData->wave[maximum.first] > h.rawData->wave[nMaxSample]
         && std::isfinite(maximum.second->time)
          ) {
            nMaxSample = maximum.first;
            h.time = maximum.second->time;
        }
    }
    h.rawData->maxSample = nMaxSample;
    h.rawData->maxAmp = h.rawData->wave[nMaxSample];
    return !std::isnan(h.time);
}

bool
SADCSelectMax::_criterion__closest_time(DetID did, SADCHit & h) {
    auto it = _calibs.find(did);
    if(_calibs.end() == it) {
        // TODO: throw?
        return false;  // no calibrations
    }
    const TimeCalibEntry & ce = it->second;

    // Use master time if need
    float timeOffset = 0.;
    if( ce.isRealtiveToMasterTime ) {
        if( std::isnan(_current_event().masterTime) ) {
            // no master time, but calibration entry is given implying it is
            // available.
            if( _tolerateNoMaster ) return false;
            NA64DP_RUNTIME_ERROR( "No master time is available at event %s,"
                    " but SADC timing calib info is given with respect to"
                    " it for plane %s."
                    , _current_event().id.to_str().c_str()
                    , naming()[did].c_str() );
        }
        timeOffset = _current_event().masterTime;
        //std::cout << " xxx "
        //          << ce.meanTime << " + "
        //          << timeOffset
        //          << std::endl;  // XXX
    }
    // Expected mean time, with or without time offset defined by mster
    // time, ns
    float meanTime = ce.meanTime + timeOffset;

    // find peak closest to the expected time
    uint8_t nMaxSample = 0;
    // iterate over found maxima and take one closest to mean time
    float closestTimeDiff = std::numeric_limits<float>::infinity();
    //std::cout << "Detector: " << naming()[did] << ":" << std::endl
    //          << "... calib mean time: " << ce.meanTime << std::endl
    //          << "... current masterT: " << _current_event().masterTime << std::endl
    //          << "........... prob pt: " << meanTime << std::endl
    //          ;
    //std::cout << "... times:";  // XXX
    //std::cout << "Detector: " << naming()[did] << ":"  << std::endl
    //          << "........... prob pt: " << meanTime << std::endl
    //          << ".............. hits: ";
    //          ;  // XXX
    for( auto & maximum : h.rawData->maxima ) {
        assert( maximum.first >= 0 );
        assert( static_cast<size_t>(maximum.first) < sizeof(h.rawData->wave)/sizeof(*h.rawData->wave) );
        //std::cout << " " << *cTime << "(" << (int) *nIdx << ")";  // XXX
        //log().debug( "%s comparing t=%.3e vs t0=%.3e"
        //       , naming()[did].c_str()
        //       , ce.isRealtiveToMasterTime ? maximum.second->time - _current_event().masterTime : maximum.second->time
        //       , ce.meanTime
        //       );
        if( _rng[0] >  maximum.first ) continue;
        if( _rng[1] <= maximum.first ) continue;
        if( !std::isfinite(maximum.second->time) ) continue;  // omit peaks without time
        //std::cout << " " << *cTime << "(" << (int) *nIdx << ")";  // XXX
        float cTimeDiff = std::fabs(maximum.second->time - meanTime);
        if( cTimeDiff < closestTimeDiff ) {
            nMaxSample = maximum.first;
            closestTimeDiff = cTimeDiff;
            h.time = maximum.second->time;
            h.rawData->maxAmp = maximum.second->amp;
            //if( ce.isRealtiveToMasterTime ) {
            //    assert(!std::isnan(_current_event().masterTime));
            //    h.time += _current_event().masterTime;
            //}
            //h.timeError = ...  ; // TODO estimate time error?
        }
    }
    if( log4cpp::Priority::DEBUG <= _log.getPriority() ) {
        _log.debug("Closest t.diff for %s: %f", naming()[did].c_str(), closestTimeDiff );
    }
    //std::cout << std::endl
    //          << "=> selected: " << (int) nMaxSample << "("
    //          << h.time << ")" << std::endl;  // XXX
    if( !std::isinf(closestTimeDiff) ) {
        h.rawData->maxSample = nMaxSample;
        return true;
    } else if( log4cpp::Priority::DEBUG <= _log.getPriority() ) {
        _log.debug("None peak were found for detector %s to select max by"
                " closest time.", naming()[did].c_str() );
    }
    return false;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCSelectMaximum, cdsp, cfg
                , "Choose maxima of mSADC waveform to be used as max E-dep peak" ) {
    // convert on-failure mode from string argument to enum item
    na64dp::handlers::SADCSelectMax::OnFailure mode;
    const std::string strMode = cfg["onFailure"]
                              ? cfg["onFailure"].as<std::string>()
                              : "emitError"
                              ;
    if( "ignore" == strMode ) {
        mode = na64dp::handlers::SADCSelectMax::kIgnore;
    } else if( "warn" == strMode ) {
        mode = na64dp::handlers::SADCSelectMax::kWarn;
    } else if( "removeHit" == strMode ) {
        mode = na64dp::handlers::SADCSelectMax::kRemoveHit;
    } else if( "discriminateEvent" == strMode ) {
        mode = na64dp::handlers::SADCSelectMax::kDiscriminateEvent;
    } else if( "emitError" == strMode ) {
        mode = na64dp::handlers::SADCSelectMax::kEmitError;
    } else {
        NA64DP_RUNTIME_ERROR( "Unknown on-failure mode for peak selection"
                " handler: \"%s\".", strMode.c_str() );
    }
    uint8_t rng[2];
    if( cfg["lookupRange"] ) {
        rng[0] = cfg["lookupRange"][0].as<int>();
        rng[1] = cfg["lookupRange"][1].as<int>();
    } else {
        rng[0] = 0;
        rng[1] = std::numeric_limits<uint8_t>::max();
    }
    // instantiate handler object
    return new na64dp::handlers::SADCSelectMax( cdsp
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["methods"].as<std::vector<std::string>>()
            , na64dp::aux::get_logging_cat(cfg)
            , mode
            , cfg["tolerateNoMaster"] ? cfg["tolerateNoMaster"].as<bool>() : false
            , rng[0], rng[1]
            );
}
