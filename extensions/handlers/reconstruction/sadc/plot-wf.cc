#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TProfile.h>
#include <TH2F.h>
#include <TH2.h>
#include <TColor.h>
#include <TStyle.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Combined plot of the sampled amplitude from SADC detectors
 *
 * This handler depicts a somewhat combined image of multiple waveforms from
 * SADC detector in a single histogram. It might be useful for various
 * qualitative checks and assurance considerations.
 *
 * \code{.yaml}
 *    - _type: SADCPlotWaveform
 *      # if set, a TProfile is used instead of TH2F. When set handler
 *      # constructor will ignore `ampNBins` and `ampRange`
 *      # parameters; optional, bool
 *      profile: false
 *      # optional, hit selection expression
 *      applyTo: null
 *      # histogram base name; str, required
 *      baseName: "amps-pedCorrected"
 *      # number of bins by vertical axis; int, required
 *      ampNBins: 200
 *      # histogram range by vertical axis; list of two floats, required
 *      ampRange: [0, 4000]
 * \endcode
 *
 * SADC typically provides a time-descrete measurement of the voltage/current
 * waveform produced by PMT or SiPM detector as an array of 32 measurements.
 * Each time detector entity was triggered this handler puts all 32 digits onto
 * the 2D histogram. After number of events processed, a somwhat of "averaged"
 * picture can be observed for qualitative checks. For example, the picture
 * below indicates that previous handlers did not apply any pedestals
 * subtraction to the waveform leading to what whan may call "saw-shape".
 *
 * \image html handlers/sadc-amp-before.png
 *
 * Cf. with the same picture (produced by the same handler) after dynamic
 * pedestals subtraction handler (`DynSubtractPeds`) applied:
 *
 * \image html handlers/sadc-amp-after.png
 *
 * Since SADC detectors in NA64 always produce 32 samples per hit, this value
 * is hardcoded within this handler implementation.
 *
 * This handler uses `TDirAdapter` class to put the particular histogram
 * into approprite `TDirectory` instance within ROOT's `TFile`.
 *
 * Other types of histograms considering some scalar hit-related properties may
 * be produced with templated version of the 1D/2D histogram, -- see
 * `HitHistogram1D` and `HitHistogram2D`, but for this type of plot it required
 * a standalone handler.
 *
 * \see TDirAdapter
 * \ingroup handlers sadc-handlers
 * */
class SADCPlotWaveform : public util::TDirAdapter
                       , public AbstractHitHandler<event::SADCHit> {
private:
    /// Number of bins by vertical axis
    size_t _ampNBins;
    /// Amplitude range bounds
    double _ampRange[2];
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::map<DetID_t, TNamed *> _hists;
    /// When set, a TProfile is used instead of TH2F
    const bool _profile;
public:
    /// Main ctr of the handler
    SADCPlotWaveform( calib::Dispatcher & ch
                    , const std::string & selection
                    , size_t nAmpBins
                    , double ampMin, double ampMax
                    , bool profile=false
                    , const std::string & hstBaseName="amp"
                    );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADCPlotWaveform();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & currentEcalHit) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

/** \param ch Calibration dispatcher instance
 *  \param selection Detector selection expression
 *  \param nAmpBins Number of bins by vertical axis
 *  \param ampMin Lower bound of amplitude histogram range (vertical axis)
 *  \param ampMax Upper bound of amplitude histogram range (vertical axis)
 *  \param baseName Histogram name suffix to name the corresponding TH2F
 *  */
SADCPlotWaveform::SADCPlotWaveform( calib::Dispatcher & ch
                                  , const std::string & selection
                                  , size_t nAmpBins
                                  , double ampMin, double ampMax
                                  , bool profile
                                  , const std::string & baseName
                                  ) : util::TDirAdapter(ch)
                                    , AbstractHitHandler(ch, selection)
                                    , _ampNBins(nAmpBins)
                                    , _ampRange{ampMin, ampMax}
                                    , _hstBaseName(baseName)
                                    , _profile(profile)
                                    {}

bool
SADCPlotWaveform::process_hit( EventID
                             , DetID did
                             , event::SADCHit & currentHit) {
    if( !currentHit.rawData ) return true;  // no raw data associated with hit
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        TNamed * newHst;
        if(!_profile) {
            newHst = new TH2F( histName.c_str()
                             , "SADC Waveform; Channels; Amplitude"
                             , 32, 0, 32
                             , _ampNBins, _ampRange[0], _ampRange[1] );
        } else {
            newHst = new TProfile( histName.c_str()
                                 , "SADC Waveform; Channels; Amplitude"
                                 , 32, 0, 32 );
        }
        auto ir = _hists.emplace( did, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }
    // fill the histogram
    if(_profile) {
        for(int i = 0; i < 32; ++i){
            static_cast<TProfile*>(it->second)->Fill( i, currentHit.rawData->wave[i] );
        }
    } else {
        for(int i = 0; i < 32; ++i){
            static_cast<TH2*>(it->second)->Fill( i, currentHit.rawData->wave[i] );
        }
    }
    return true;
}

void
SADCPlotWaveform::finalize() {
    for( auto idHstPair : _hists ) {
        if(!_profile) {
            static_cast<TH2*>(idHstPair.second)->SetOption("colz");
        }
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

SADCPlotWaveform::~SADCPlotWaveform() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( SADCPlotWaveform, ch, cfg
                , "Builds 2D histograms of SADC amplitude waveform" ) {
    bool profile = cfg["profile"] && cfg["profile"].as<bool>();
    return new handlers::SADCPlotWaveform( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , profile ? 10 : cfg["ampNBins"].as<int>()
            , profile ? -1 : cfg["ampRange"][0].as<float>()
            , profile ?  1 : cfg["ampRange"][1].as<float>()
            , profile
            , cfg["baseName"].as<std::string>()
            );
}

}
