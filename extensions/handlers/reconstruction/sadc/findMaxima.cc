#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Finds local maxima in SADC waveform
 *
 * Usage:
 * \code{.yaml}
 *     - _type: SADCFindMaxima
 *       # optional, hit selection expression
 *       applyTo: ...
 *       # A type of criterion applied; str, opt
 *       criterion: simple
 *       # a threshold used by some criteria; double, optional
 *       threshold: 0.
 *       # if set, hits with zero amplitude or amp below the threshold will
 *       # be removed; bool, optional
 *       removeZeroLikeHits: false
 * \endcode
 *
 * Handler will iterates over 1-31 samples of SADC waveform (`RawDataSADC::wave`)
 * finding ones which:
 *
 * * for `criterion: simple` \-- \f$i\f$-th sample where both \f$(i+1)\f$-th and
 *   \f$(i-1)\f$-th samples are below \f$i\f$-th and their average difference
 *   from \f$i\f$-th smaple is more than `threshold` value specified
 * * for `criterion: p348reco` \-- \f$i\f$-th sample that is strictly above both
 *   \f$(i-1)\f$-th and \f$(i+1)\f$-th.
 *
 * Found samples numbers ("maxima") will populate `RawDataSADC::maxima` field,
 * delimiting end with max value for `uint8_t` type. Note that this
 * handler won't set `maximaTimes` yet \-- the nSample-to-time conversion is
 * the subject of dedicate handlers as algoritm may not be trivial. For
 * instance, see SADCTimeAtMaxByCoM, SADCTimeAtMaxByRisingEdge, etc.
 *
 * YAML config accepts standard \see commonApplyTo argument.
 *
 * \note With "p348reco" peak-slection criterion is equivalent to
 *       `Cell::searchPeaks()` procedure of p348reco.
 * \ingroup handlers sadc-handlers
 * \todo Document criteria
 * */
class SADCFindMaxima : public AbstractHitHandler<event::SADCHit> {
private:
    /// threshold for minimum amplitude of local maximum
    double _threshold;
    /// When set, hits without peaks will be enqueued for deletion.
    bool _removeZeroLikeHits;
    /// Ignores hit's amp threshold when set
    bool _ignoreMaxAmp;
protected:
    /// Identical to "official" p348reco
    bool _p348reco_peak_criterion( const float * wave ) const {
        const float A0 = *(wave - 1)
                  , A1 = *wave
                  , A2 = *(wave + 1)
                  ;
        const bool riseOnLeft = (A1 >= A0)
                 , fallOnRight = (A1 >= A2)
                 , isPlateau = (A1 == A0 && A1 == A2)
                 ;
        return (!isPlateau) && riseOnLeft && fallOnRight;
    }
    /// A bit simplified peak lookup criterion with threshold
    bool _thresholded_alterating_sign( const float * wave ) const {
        if ( ( *(wave - 1) < *wave )
          && ( *(wave + 1) < *wave ) ) {
            // If peak amplitude is larger than specified threshold
            if ( *(wave) - (*(wave-1) + *(wave+1))/2 > _threshold ) {
                return true;
            }
        }
        return false;
    }

    bool (SADCFindMaxima::*_check_peak)(const float *) const;
public:
    SADCFindMaxima( calib::Dispatcher & cdsp
                  , const std::string & only
                  , const std::string & method="simple" 
                  , double threshold=0.
                  , bool ignoreMaxAmp=false
                  , bool removeZeroLikeHits=false
                  );

    /**\brief Fills the `maxima` field of SADC hit's raw data
     *
     * To distinguish a peak (local maximum), uses criteria choosen from ctr.
     * Results may slightly differ for various methods. Refer to cource code
     * for info on particular method.
     *
     * \todo document criteria
     * */
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & hit ) override;

    //virtual ProcRes process_event(event::Event & e) override {  // XXX
    //    auto rc = AbstractHitHandler<event::SADCHit>::process_event(e);
    //    std::cout << std::endl;
    //    return rc;
    //}
};  // class SADCFindMaxima

//                          * * *   * * *   * * *

SADCFindMaxima::SADCFindMaxima( calib::Dispatcher & cdsp
                              , const std::string & only
                              , const std::string & method
                              , double threshold
                              , bool ignoreMaxAmp
                              , bool removeZeroLikeHits
                              )
        : AbstractHitHandler<event::SADCHit>(cdsp, only)
        , _threshold(threshold)
        , _removeZeroLikeHits(removeZeroLikeHits)
        , _ignoreMaxAmp(ignoreMaxAmp)
        {
    if(method == "simple" || (method == "" && threshold != 0)) {
        _check_peak = &SADCFindMaxima::_thresholded_alterating_sign;
    } else if(method == "p348reco") {
        _check_peak = &SADCFindMaxima::_p348reco_peak_criterion;
    } else {
        NA64DP_RUNTIME_ERROR( "Unknown peak-finding criterion \"%s\"."
                , method.c_str() );
    }
}

bool
SADCFindMaxima::process_hit( EventID
                           , DetID did
                           , event::SADCHit & cHit ){
    NA64DP_ASSERT_EVENT_DATA( cHit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    for( unsigned int i = 1; i < 31; ++i ) {
        // check if the sign alterates as for maximum at the point;
        // skips alteration between certain threshold
        if ( (this->*_check_peak)( cHit.rawData->wave + i ) ) {
            if((!_ignoreMaxAmp) && !std::isnan(cHit.rawData->ampThreshold)) {
                if( cHit.rawData->wave[i] <= cHit.rawData->ampThreshold )
                    continue;
            }
            auto peakPtr = lmem().create<event::MSADCPeak>(lmem());
            util::reset(*peakPtr);
            cHit.rawData->maxima.emplace(i, peakPtr);
        }
    }
    if( _removeZeroLikeHits && cHit.rawData->maxima.empty())
        _set_hit_erase_flag();
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCFindMaxima, cdsp, cfg
                , "Get local maxima of mSADC waveform" ) {
    return new na64dp::handlers::SADCFindMaxima( cdsp
            , na64dp::aux::retrieve_det_selection(cfg)
            // TODO: nsamples
            , cfg["criterion"] ? cfg["criterion"].as<std::string>() : "simple"
            , cfg["threshold"] ? cfg["threshold"].as<double>() : 0.
            , cfg["ignoreAmpThreshold"] ? cfg["ignoreAmpThreshold"].as<bool>() : false
            , cfg["removeZeroLikeHits"] ? cfg["removeZeroLikeHits"].as<bool>() : false
            );
}

