#include "na64dp/abstractHitHandler.hh"
#include "na64common/calib/msadc-PedLED.hh"
#include "na64event/reset-values.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace calib {
struct PedestalsForItem { event::StdFloat_t odd, even, varOdd, varEven; };
typedef std::unordered_map<DetID, PedestalsForItem> Pedestals;
}  // namespace ::na64dp::calib
namespace handlers {

/// Helper class adding the pedestals
struct PedestalsDonskov
    : public calib::Handle< p348reco::MSADCCalibPedLedType1Collection<p348reco::MSADCCalibPedLedType1> > {
public:
    typedef p348reco::MSADCCalibPedLedType1Collection<p348reco::MSADCCalibPedLedType1> PedLEDCalibs;
private:
    /// Logging category ref
    log4cpp::Category & _L;
    /// Reference to destination map to add the LED levels
    calib::Pedestals & _target;
    /// Reference to naming handle
    calib::Handle<nameutils::DetectorNaming> & _naming;
protected:
    /// Updates values at `_coeffs` based on provided calibrations data
    void handle_update(const PedLEDCalibs & collection) override {
        auto cnv = collection.get_mapped(_naming.get(), &_L);
        // todo: use selector here to selectively apply LED from different sources?
        for(auto & p : cnv) {
            _target[p.first] = { p.second.pedestals[0], p.second.pedestals[1]
                , event::StdFloat_t(std::nan("0")), event::StdFloat_t(std::nan("0")) };
        }
    }
public:
    PedestalsDonskov( calib::Dispatcher & mgr
                    , calib::Pedestals & dest
                    , calib::Handle<nameutils::DetectorNaming> & namingHandle
                    , log4cpp::Category & L )
        : calib::Handle<PedLEDCalibs>("default", mgr)
        , _L(L)
        , _target(dest)
        , _naming(namingHandle)
        {}
    ~PedestalsDonskov() {}
};  // PedestalsDonskov

// ... other pedestals calibrations classes

/**\brief Assigns pedestals values from calibration data
 *
 * Appends (M)SADC hit with pedestals provided by extern calibration source.
 *
 * If entry does not exist in calibration data, the pedestals value is kept
 * intact or set to `std::nan(0)` depending on boolean option
 * `setNonExistingToNan`.
 *
 * Handler can behave like a fall-back source of the pedestals: using
 * parameter `ifCurrentExceedsNSigma` one makes this handler to compare
 * pedestal value(s) brought by the hit with mean value in terms of the sigma.
 * */
class AssignPedestals
        : public AbstractHitHandler<event::SADCHit>
        {
protected:
    /// Set to `NaN` pedestals that were not found
    bool _setNonExistingToNan;
    /// Pedestals source ID
    const std::string _src;
    /// Global index with pedestals
    calib::Pedestals _pedestals;
    /// Threshold (optional)
    float _nSigmaThreshold;

    /// Pointer to SVD's pedestals calibration updater
    PedestalsDonskov * _svdPedsHelper;
public:
    void handle_update(const nameutils::DetectorNaming & nm) override;

    AssignPedestals( calib::Manager & cdsp
                   , const std::string & detSelection
                   , float nSigmaThreshold
                   , const std::string & source
                   , bool setNonExistingToNan
                   , log4cpp::Category & L
                   );
    ~AssignPedestals();

    virtual bool process_hit( EventID, DetID, event::SADCHit & ) override;
};

//                      * * *   * * *   * * *

AssignPedestals::AssignPedestals( calib::Manager & cdsp
                                , const std::string & detSelection
                                , float nSigmaThreshold
                                , const std::string & source
                                , bool setNonExistingToNan
                                , log4cpp::Category & L
                                )
        : AbstractHitHandler<event::SADCHit>(cdsp, detSelection, L)
        , _setNonExistingToNan(setNonExistingToNan)
        , _src(source)
        , _nSigmaThreshold(nSigmaThreshold)
        , _svdPedsHelper(nullptr)
        {
    if(_src == "@calib-svd") {
        _svdPedsHelper = new PedestalsDonskov(cdsp, _pedestals, *this, _log);
    }
}

AssignPedestals::~AssignPedestals() {
    if(_svdPedsHelper)
        delete _svdPedsHelper;
}

void
AssignPedestals::handle_update(const nameutils::DetectorNaming & nm) {
    if(_src[0] != '@') {
        // open file and read freqs mean and stddev, line by line
        std::ifstream ifs(_src);
        std::string line;
        DetID did;
        size_t nLine = 0;
        while(std::getline(ifs, line)) {
            ++nLine;
            if(line.empty()) continue;
            calib::PedestalsForItem ps;
            auto toks = sdc::aux::tokenize(line, ',');
            size_t nTok = 0;
            for(const auto & tok : toks) {
                if(nTok < 5) {
                    if(0 == nTok) {
                        did = nm[tok];
                        ++nTok;
                        continue;
                    } else if(1 == nTok) {
                        ps.odd = sdc::aux::lexical_cast<float>(tok);
                        ++nTok;
                        continue;
                    } else if(2 == nTok) {
                        ps.varOdd = sdc::aux::lexical_cast<float>(tok);
                        ++nTok;
                        continue;
                    } else if(3 == nTok) {
                        ps.even = sdc::aux::lexical_cast<float>(tok);
                        ++nTok;
                        continue;
                    } else if(4 == nTok) {
                        ps.varEven = sdc::aux::lexical_cast<float>(tok);
                        ++nTok;
                        continue;
                    }
                }
            }
            log().debug("Loaded %s:%zu: set to %f (even), %f (odd)"
                    , _src.c_str(), nLine, ps.even, ps.odd );
            if(nTok != 5) {
                NA64DP_RUNTIME_ERROR("%s:%zu invalid line: \"%s\" (%zu tokens)"
                        , _src.c_str(), nLine, line.c_str(), nTok );
            }
            _pedestals[did] = ps;
        }
    }
    AbstractHitHandler<event::SADCHit>::handle_update(nm);
}

bool
AssignPedestals::process_hit( EventID
                            , DetID detID
                            , event::SADCHit & hit ) {
    auto it = _pedestals.find( detID );
    if( _pedestals.end() == it ) {
        // pedestals values found
        if( _setNonExistingToNan ) {
            hit.rawData->pedestals[0] = hit.rawData->pedestals[1] = std::nan("0");
        }
        return true;
    }
    // pedestals found
    const auto & pp = it->second;
    if( std::isnan(_nSigmaThreshold)
     || std::isnan(hit.rawData->pedestals[0])
     || std::isnan(hit.rawData->pedestals[1])
     ) {
        hit.rawData->pedestals[0] = pp.odd;
        hit.rawData->pedestals[1] = pp.even;
    } else {
        if( std::fabs(hit.rawData->pedestals[0] - pp.odd)  > _nSigmaThreshold*pp.varOdd
         || std::fabs(hit.rawData->pedestals[1] - pp.even) > _nSigmaThreshold*pp.varEven) {
            //std::cout << "xxx: "
            //    << std::fabs(hit.rawData->pedestals[0] - pp.odd)
            //    << " > "
            //    << _nSigmaThreshold << "*" << pp.varOdd
            //    << " OR "
            //    << std::fabs(hit.rawData->pedestals[1] - pp.even)
            //    << " > "
            //    << _nSigmaThreshold << "*" << pp.varEven
            //    << std::endl;  // XXX
            hit.rawData->pedestals[0] = pp.odd;
            hit.rawData->pedestals[1] = pp.even;
        }
    }
    return true;
}

}

REGISTER_HANDLER( SADCAssignPedestals, dispatcher, cfg
                , "Assigns pedestals from available calibration data to SADC hit" ) {
    return new handlers::AssignPedestals( dispatcher
                                        , aux::retrieve_det_selection(cfg)
                                        , cfg["ifCurrentExceedsNSigma"] ? cfg["ifCurrentExceedsNSigma"].as<float>() : std::nan("0")
                                        , cfg["source"].as<std::string>()
                                        , cfg["setNonExistingToNan"]
                                            ? cfg["setNonExistingToNan"].as<bool>()
                                            : false
                                        , aux::get_logging_cat(cfg)
                                        );
}

}

