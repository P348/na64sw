#include "na64dp/abstractHitHandler.hh"
#include "na64detID/cellID.hh"
#include "na64util/str-fmt.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

using event::SADCHit;

/**\brief Calculates time estimations for the peak maxima for SADC waveform by
 *        "center of mass" algorithm
 *
 * Relies on `RawDataSADC::maxima` field of SADC hit to find out time estimations
 * (in number-of-sample units) for the mixima (peaks) of sadc waveform.
 *
 * \code{.yaml}
 *     - _type: SADCTimeAtMaxByCoM
 *       # optional, hit selection expression
 *       applyTo: null
 *       # Take into account samples in this range; list of two ints, optional
 *       signalRange: [15, 32]
 *       # Whether time estimation is relative wrt max value; bool, opt
 *       isRelative: false
 *       # Use this value as time interval between two samples (nanoseconds);
 *       # float, optional
 *       nsPerSample: 12.5
 * \endcode
 *
 * Time estimations are in nanoseconds, the scaling unit (nanoseconds per
 * sample) is defined by parameter "nsPerSample". "isRelative" parameter
 * defines whether or not search window is defined in scale absolute or
 * relative to max, i.e. "isRelateve=false" for "signalRange"=[15,32] will mean
 * that weighted mean will be computed from 15 to 32 sample while
 * "isRelative=true" for "signalRange"=[-3,2] will mean that weigthed mean is
 * computed within in a window relative to current max that starts 3 samples
 * before and ends 2 samples after max being considered.
 *
 * \ingroup handlers, sadc-handlers
 * */
class SADCTimeAtMaxByCoM : public AbstractHitHandler<SADCHit> {
protected:
    /// Samples window to calculate "center of mass"
    uint8_t _range[2];
    ///\brief When set, the range will be interpreted as relative window
    ///
    /// When set, the `_range` value interpreted as "N samples before" and
    /// "N samples after" sample at peak.
    bool _windowIsRelative;
    /// nanoseconds per MSADC sample
    float _nsPerSample;
    /// Peak time information calculated by "center of mass" in a fixed window
    bool _peak_time__com( event::RawDataSADC & rd );
    /// Peak time information calculated by "center of mass" in a relative window
    bool _peak_time__com_relative( event::RawDataSADC & rd );
public:
    SADCTimeAtMaxByCoM( calib::Dispatcher & cdsp
                      , const std::string & only
                      , int from, int to
                      , log4cpp::Category & logCat
                      , bool windowIsRelative=false
                      , float nsPerSample=12.5
                      , const std::string & detNamingClass="default"
                      ) : AbstractHitHandler<SADCHit>(cdsp, only, logCat, detNamingClass)
                        , _range{(uint8_t) from, (uint8_t) to}
                        , _windowIsRelative(windowIsRelative)
                        , _nsPerSample(nsPerSample)
                        {
        if( from >= to && !_windowIsRelative ) {
            NA64DP_RUNTIME_ERROR( "Fixed window required to be [%d, %d]"
                    " (\"from\" must be less than \"to\".", from, to );
        }
    }

    /// Applies selected method to raw data of SADC hit filling
    /// up `RawDataSADC::maximaTimes`.
    bool process_hit( EventID, HitKey k, SADCHit & cHit) {
        if( !cHit.rawData ) return true;  // do noting if no raw data
        if( !_windowIsRelative ) {
            _peak_time__com(*cHit.rawData);
        } else {
            _peak_time__com_relative(*cHit.rawData);
        }
        return true;
    }
};  // class SADCTimeAtMaxByCoM

bool
SADCTimeAtMaxByCoM::_peak_time__com( event::RawDataSADC & rd ) {
    // iterate over the maxima field, for each maxima (peak) value
    float Sa = 0, Sax = 0;
    for( auto & maximum : rd.maxima ) {
        maximum.second->time = std::nan("0");
        if((!Sa) && maximum.first >= _range[0] && maximum.first < _range[1] ) {
            for(int i = _range[0]; i < _range[1]; ++i) {
                Sa  += rd.wave[i];
                Sax += rd.wave[i]*i;
            }
            if(!Sa) continue;
            // rescale time from samples# units to nsec
            maximum.second->time = (Sax/Sa)*_nsPerSample;
        }
    }
    return (bool) Sa;
}

bool
SADCTimeAtMaxByCoM::_peak_time__com_relative( event::RawDataSADC & rd ) {
    #if 1
    NA64DP_RUNTIME_ERROR("TODO: _peak_time__com_relative() has to be re-written"
            " respecting MSADC max items");  // TODO: API changed slightly...
    #else  // previous implementation:
    // iterate over the maxima field, for each maxima (peak) value
    float * maxT = rd.maximaTimes;
    float Sa = 0, Sax = 0;
    const size_t nSamplesMax = sizeof(event::RawDataSADC::wave)
                             / sizeof(event::RawDataSADC::wave[0])
                             ;
    for( auto * c = rd.maxima
       ; *c != std::numeric_limits<uint8_t>::max()
       ; ++c, ++maxT ) {
        *maxT = std::nan("0");
        {
            const auto * from = *c - _range[0] < 0
                              ? rd.wave
                              : rd.wave + *c - _range[0]
                     ,   * to = *c + _range[1] > nSamplesMax
                              ? rd.wave + nSamplesMax
                              : rd.wave + *c + _range[1]
                              ;
            int nSample = *c;
            for(auto * cc = from; cc != to; ++cc, ++nSample) {
                Sa  += *cc;
                Sax += *cc*nSample;
            }
            if(!Sa) continue;
            // rescale time from samples# units to nsec
            *maxT = (Sax/Sa)*_nsPerSample;
        }
    }
    return (bool) Sa;
    #endif
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SADCTimeAtMaxByCoM, cdsp, cfg
                , "Calculates time estimations for the peak maxima for SADC"
                  " waveform using weightem mean (\"center of mass\")." ) {
    int rng[2];
    if( cfg["signalRange"] ) {
        rng[0] = cfg["signalRange"][0].as<int>();
        rng[1] = cfg["signalRange"][1].as<int>();
    } else {
        rng[0] = 15;
        rng[1] = 32;
    }
    return new na64dp::handlers::SADCTimeAtMaxByCoM( cdsp 
                , na64dp::aux::retrieve_det_selection(cfg)
                , rng[0], rng[1]
                , ::na64dp::aux::get_logging_cat(cfg)
                , cfg["isRelative"] ? cfg["isRelative"].as<bool>() : false
                , cfg["nsPerSample"] ? cfg["nsPerSample"].as<float>() : 12.5
                , ::na64dp::aux::get_naming_class(cfg)
                );
}

