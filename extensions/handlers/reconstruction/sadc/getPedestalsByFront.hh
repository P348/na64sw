#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Takes into account first N samples assuming mean values as pedestals
 *
 * Parameterized by number of samples `nSamples` taken for *each* SADC channel.
 * E.g. for `nSamples: 4`, 4*2=8 initial values (from 32) will be taken. Two
 * mean values for `SADCHit::pedestals[0,1]` will be then written in
 * corresponding hit at `RawDataSADC::pedestals`.
 *
 * Usaeg:
 * \code{.yaml}
 *     - _type: SADCGetPedestalsByFront
 *     # Number of samples per each channel to consider; int, opt
 *     nSamples: 4
 *     # Threshold value in raw ADC scale that, if set to non-zero and exceeded
 *     # makes the handler to take minimal value per channel instead of
 *     # mean; int, opt
 *     useMinIfDeltaExceeds: 20
 * \endcode
 *
 * Parameter `maxPedestalSpanning`, if greater than zero, sets threshold for
 * min/max difference among the considered samples. If this threshold is
 * exceeded, the pedestal considered as one affected by the event pile-up
 * and minimal value will be then searched among all the 16 samples for this
 * channel. Setting `maxPedestalSpanning` to 0 disables this behaviour.
 *
 * This handler does *not* perform subtraction. Use dedicated handler
 * to apply pedestal subtraction afterwards (e.g. `SADCSubtractPedestals`).
 *
 * Result of applying the subtraction with pedestals derived with this handler
 * can be demonstrated visually by means of `SADCAmpHists` handler. Before
 * subtracting pedestals:
 *
 * \image html handlers/sadc-amp-before.png
 *
 * After subtraction:
 *
 * \image html handlers/sadc-amp-after.png
 *
 * YAML config node examples:
 *
 * \code{.yaml}
 *      - _type: SADCGetPedestalsByFront
 * \endcode
 * or
 * \code{.yaml}
 *      - _type: SADCGetPedestalsByFront
 *        nSamples: 4
 * \endcode
 *
 * \ingroup handlers sadc-handlers
 * \see SADCSubtractPedestals
 * */
class SADCGetPedestalsByFront : public AbstractHitHandler<event::SADCHit> {
private:
    /// Number of samples to take from each channel
    unsigned char _nFirstSamples;
    /// If not 0 makes algorithm to consider `min`/`max` delta and take `min`
    /// if sample exceeds this threshold.
    int _maxPedestalSpanning;
public:
    /// Default ctr 
    SADCGetPedestalsByFront( calib::Dispatcher &  ch
                           , const std::string & only
                           , unsigned char nfsa=4
                           , unsigned short maxPedestalSpanning=20
                           ) : AbstractHitHandler<event::SADCHit>(ch, only)
                             , _nFirstSamples(nfsa)
                             , _maxPedestalSpanning(maxPedestalSpanning)
                             {}
    /// Computes average values
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & currentHit ) override;
};

}
}
