#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Sums up pedestal value(s) and SADC waveform
 *
 * A MSADC handler that performs a reverse transformation with MSADC
 * waveform with respect to `SADCSubtractPedestals`.
 *
 * ~~~{.yaml}
 *     - _type: SADCAppendPeds
 * ~~~
 *
 * \see SADCSubtractPedestals
 * \ingroup handlers sadc-handlers
 * */
class SADCAppendPeds : public AbstractHitHandler<event::SADCHit> {
public:
    SADCAppendPeds( calib::Dispatcher & cdsp
                  , const std::string & selection )
            : AbstractHitHandler<event::SADCHit>(cdsp, selection) {}
    ~SADCAppendPeds(){}
    virtual bool process_hit( EventID eventID
                            , DetID detID
                            , event::SADCHit & hit ) override;
};

//                      * * *   * * *   * * *

bool
SADCAppendPeds::process_hit( EventID, DetID
                           , event::SADCHit & currentEcalHit) {
    // Add Mean pedestal values back to the Event
    for(int i = 0; i < 32; ++i) {
        if ((i % 2) == 0) {
            currentEcalHit.rawData->wave[i] += currentEcalHit.rawData->pedestals[0];
        } else {
            currentEcalHit.rawData->wave[i] += currentEcalHit.rawData->pedestals[1];
        }
    }
    return true;
}

}

REGISTER_HANDLER( SADCAppendPeds, ch, yamlNode
                , "Adds pedestal values back to the waveform" ) {
    return new handlers::SADCAppendPeds(ch, aux::retrieve_det_selection(yamlNode));
}

}

