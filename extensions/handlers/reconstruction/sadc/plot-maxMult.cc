#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2F.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief 1D histogram depicting maxima multiplicity (per hit)
 *
 * This handler is somewhat useful to tune threshold-parameters for max-finding
 * algorithms.
 *
 * \code{.yaml}
 *     - _type: SADCPlotPeakMult
 *       # optional, hit selection expression
 *       applyTo: null
 *       # histogram base name; str, opt
 *       baseName: "maxMult"
 * \endcode
 *
 * \ingroup handlers sadc-handlers
 * */
class SADCPlotPeakMult : public util::TDirAdapter
                       , public AbstractHitHandler<event::SADCHit> {
private:
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::unordered_map<DetID, TH1I *> _hists;
public:
    /// Main ctr of the handler
    SADCPlotPeakMult( calib::Dispatcher & ch
                    , const std::string & selection
                    , const std::string & hstBaseName="amp"
                    );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADCPlotPeakMult();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID
                            , event::SADCHit & ) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

//                          * * *   * * *   * * *

SADCPlotPeakMult::SADCPlotPeakMult( calib::Dispatcher & ch
                                  , const std::string & selection
                                  , const std::string & baseName
                                  ) : util::TDirAdapter(ch)
                                    , AbstractHitHandler(ch, selection)
                                    , _hstBaseName(baseName) {
}

bool
SADCPlotPeakMult::process_hit( EventID
                             , DetID did
                             , event::SADCHit & hit) {
    if( !hit.rawData ) return true;  // no raw data associated with hit
    auto it = _hists.find( did );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(did, histName)->cd();
        TH1I * newHst = new TH1I( histName.c_str()
                                , "Max peak multiplicity per hit; N ; Hits"
                                , 32, 0, 32 );
        auto ir = _hists.emplace( did, newHst );
        it = ir.first;
        assert(ir.second);  // something wrong with detector ID
    }
    // calc number of peaks and fill the histo
    it->second->Fill(hit.rawData->maxima.size());
    return true;
}

void
SADCPlotPeakMult::finalize() {
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

SADCPlotPeakMult::~SADCPlotPeakMult() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}

REGISTER_HANDLER( SADCPlotPeakMult, ch, cfg
                , "Plots max peaks multiplicity per SADC hit" ) {
    return new handlers::SADCPlotPeakMult( ch
            , na64dp::aux::retrieve_det_selection(cfg)
            , cfg["baseName"] ? cfg["baseName"].as<std::string>() : "maxMult"
            );
}

}

