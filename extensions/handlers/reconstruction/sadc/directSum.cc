#include "na64dp/abstractHitHandler.hh"

/**\file
 *
 * MSADC direct sum handler declaration and implementation. */

namespace na64dp {
namespace handlers {

/**\brief Computes linear sum of SADC waveform
 * \ingroup Handlers
 *
 * This handler calculates the sum on the SADC hit waveform by summing up all
 * the 32 samples.
 *
 * ~~~{.yaml}
 *     - _type: SADCDirectSum
 *       # negative samples will be zeroed, bool, optional
 *       zeroNegativeValues: false
 * ~~~
 *
 * Accepts single parameter "zeroNegativeValues:bool" that, when set to `true'
 * makes the handler to set negative samples to 0. Can be omitted, default
 * value is `false'.
 *
 * \see SADCLinearSum
 * \ingroup handlers sadc-handlers
 * */
class SADCDirectSum : public AbstractHitHandler<event::SADCHit> {
private:
    bool _doZeroNegative;
public:
    SADCDirectSum( calib::Dispatcher & ch
                 , const std::string & only
                 , bool doZeroNegative=false ) : AbstractHitHandler<event::SADCHit>(ch, only)
                                               , _doZeroNegative(doZeroNegative)
                                               {}
    virtual bool process_hit( EventID, DetID, event::SADCHit & ) override;
};

//                      * * *   * * *   * * *

bool
SADCDirectSum::process_hit( EventID
                          , DetID did
                          , event::SADCHit & cHit ){
    NA64DP_ASSERT_EVENT_DATA( cHit.rawData, "Raw SADC data for %s"
            , naming()[did].c_str() );
    cHit.rawData->sum = 0;
    for( int i = 0; i < 32; ++i ){
        auto a = cHit.rawData->wave[i];
        if( _doZeroNegative ) {
            if( a < 0 ) a = 0;
        }
        cHit.rawData->sum += a / 2; 
    }
    return true;
}

}

REGISTER_HANDLER( SADCDirectSum, ch, yamlNode
                , "Computes direct sum of SADC waveform" ){
    bool zeroNegatives = yamlNode["zeroNegativeValues"] ? yamlNode["zeroNegativeValues"].as<bool>()
                                                        : false;
    return new handlers::SADCDirectSum( ch
            , aux::retrieve_det_selection(yamlNode)
            , zeroNegatives );
}

}

