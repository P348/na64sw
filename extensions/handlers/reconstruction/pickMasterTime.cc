#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"
#include "na64event/data/event.hh"
#include "na64detID/TBName.hh"
#include "na64detID/cellID.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

/**\brief Copies time estimation from certain detector to a `masterTime` value
 *
 * Relies on `(masterTimeDetName;std::string)` "calibration" data type that
 * provides name of the detector being used as one providing master time info.
 * For each event will pick the time from hit's time field and copy it to the
 * `Event::masterTime` field.
 *
 * \code{.yaml}
 *     - _type: PickMasterTime
 *       # Defines handler's reaction if no master time hit is found in an event.
 *       # Possible choices are: "omit", "drop", "emitError"; str, optional
 *       onNoHitInMaster: "emitError"
 *       # If set, forces handler to use this detector name string instead of
 *       # one brought by run information (calib info "masterTimeSource");
 *       # str, optional
 *       override: ""
 *       # Offset added to the extracted master time; opt, float
 *       offset: 0.
 * \endcode
 *
 * Depends on "naming" and "masterTimeSource" calibration data.
 *
 * \note Currently, support only SADC detector as master time source (unclear,
 *       whether we will ever use one of another type).
 * \ingroup handlers sadc-handlers calib-handlers
 * \todo Shall we here consider non-physical events with no master time?
 * */
class PickMasterTime : public AbstractHandler
                     , public calib::Handle< std::pair<std::string, float> >  // "masterTimeSource"
                     , public calib::Handle<nameutils::DetectorNaming>
                     {
public:
    /// Type describing what to do if no master hit is present in an event
    enum OnNoHit {
        kOmit,  ///< handler will pass event by without modification
        //kWarnNDrop  // todo
        //kWarnNSkip  // todo
        kDrop,  ///< handler will stop event propagation
        kEmitError,  ///< handler will throw an error
    };
protected:
    /// Cache of local detector ID
    DetID _masterDetID;
    /// Chips cache
    struct {
        DetChip_t kSADC
                , kNA64TDC;
    } _chipIDsCache;
    /// If set to be non-empty string, forces handler to use this detector name
    /// as source of master time (otherwise one from calibrations is used).
    const std::string _override;
    /// What to do with events with no hit from master detector
    OnNoHit _onNoHit;

    /// Master time offset
    float _offset;
protected:
    /// Updates current master time source
    void handle_update( const nameutils::DetectorNaming & nm ) override {
        calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
        _chipIDsCache.kSADC    = nm.chip_id("SADC");
        _chipIDsCache.kNA64TDC = nm.chip_id("NA64TDC");
    }

    /// Updates current master time source
    void handle_update( const std::pair<std::string, float> & masterTimeInfo ) override {
        calib::Handle< std::pair<std::string, float> >::handle_update(masterTimeInfo);
        if( ! _override.empty() ) return;
        const auto & nm = calib::Handle<nameutils::DetectorNaming>::get();
        _masterDetID = nm[masterTimeInfo.first];
        _offset = masterTimeInfo.second;
        _log.info("Master time source set to \"%s\"", nm[_masterDetID].c_str());
        //if(!std::isnan(_offset))
        #if 0
        // Note: for SADC detectors payload will be unset, while hits are
        // always identified with some set payload.
        CellID cid(0, 0, 0);
        _masterDetID.payload(cid.cellID);
        #endif
    }

    ProcRes _on_no_hit( EventID eid ) {
        #if 0
        for( const auto & p : event.sadcHits ) {  // XXX
            std::cout << " - " << calib::Handle<nameutils::DetectorNaming>::get()[p.first] << std::endl;
        }
        #endif
        if( kOmit == _onNoHit ) return kOk;  // quietly ignore
        if( kDrop == _onNoHit ) return kDiscriminateEvent;  // stop ev propagation
        NA64DP_RUNTIME_ERROR("There is no hit in an"
                " event %s for detector \"%s\" (%s) that provides master"
                " time."
                , eid.to_str().c_str()
                , calib::Handle<std::pair< std::string, float> >::get().first.c_str()
                , calib::Handle<nameutils::DetectorNaming>::get()[_masterDetID].c_str()
                );
    }

    ProcRes _on_no_time(EventID eid) {
        NA64DP_RUNTIME_ERROR("A time value is not set for detector %s"
                " in an event %s (yet, hit exists in event)."
                , calib::Handle< std::pair<std::string, float> >::get().first.c_str()
                , eid.to_str().c_str()
                );
    }
public:
    PickMasterTime( calib::Dispatcher & cdsp
                  , const std::string & overrideWith=""
                  , float offset=0
                  , OnNoHit onNoHit=kEmitError
                  , const std::string & masterTimeTriggerType="physics"
                  ) : calib::Handle< std::pair<std::string, float> >(masterTimeTriggerType, cdsp)
                    , calib::Handle<nameutils::DetectorNaming>("default", cdsp)
                    , _override(overrideWith)
                    , _onNoHit(onNoHit)
                    , _offset(offset)
                    {}

    /// Selects the peak according to the current settings.
    ProcRes process_event(event::Event & event) override {
        if( ! _masterDetID.id ) {
            /*auto & shallFail = */calib::Handle< std::pair<std::string, float> >::get();
            throw errors::PrematureDereferencing(
                    calib::Handle< std::pair<std::string, float> >::name_str().c_str(),
                    typeid(std::pair<std::string, float>) );
        }
        if( _masterDetID.chip() == _chipIDsCache.kSADC ) {
            // Master time defined by one of SADC chip is used in 2015-2021
            // (e-) runs
            auto it = event.sadcHits.find(_masterDetID);
            if( event.sadcHits.end() == it ) return _on_no_hit(event.id);
            if( std::isnan(it->second->time) ) return _on_no_time(event.id);
            event.masterTime = it->second->time;
        } else if( _masterDetID.chip() == _chipIDsCache.kNA64TDC ) {
            // NA64TDC on one of the STs is used as master time in 2021mu,
            // original from p348reco:
            //      name == STT0 && tdc->GetChannel() == 32 (sic!)
            //      tdct0_phase = (CS::int16_t) tdc->GetTime();
            #if 0  // check for single/multiple hit(s)
            auto it = event.stwtdcHits.find(_masterDetID);
            if(event.stwtdcHits.end() == it) return _on_no_hit(event.id);
            #else  // check for single/multiple hit(s)
            auto eqr = event.stwtdcHits.equal_range(_masterDetID);
            if( event.stwtdcHits.end() == eqr.first ) return _on_no_hit(event.id);
            auto it = eqr.first;
            if( std::distance(eqr.first, eqr.second) > 1 ) {
                #if 0  // warn/take greatest kludge
                const auto & nm = calib::Handle<nameutils::DetectorNaming>::get();
                std::ostringstream oss;
                bool isFirst = true;
                for( auto it = eqr.first; it != eqr.second; ++it ) {
                    if(isFirst) isFirst = false; else oss << ", ";
                    oss << "{wireNo=" << it->second->rawData->wireNo
                        << ", time=" << it->second->rawData->time
                        << ", timeDecoded=" << it->second->rawData->timeDecoded
                        << "}"
                        ;
                }
                log().warn( "Multiple hits got for the master time entity %s"
                            " NA64TDC: %s"
                          , nm[_masterDetID].c_str()
                          , oss.str().c_str() );
                //NA64DP_RUNTIME_ERROR( "Multiple hits got for NA64TDC master"
                //        " time: %zu", std::distance(eqr.first, eqr.second) );
                #else  // warn/take greatest kludge
                float greatestTime = eqr.first->second->rawData->time;
                for( auto iit = eqr.first; iit != eqr.second; ++iit ) {
                    if( greatestTime > iit->second->rawData->time ) continue;
                    it = iit;
                    greatestTime = iit->second->rawData->time;
                }
                #endif  // warn/take greatest kludge
            }
            #endif  // check for single/multiple hit(s)
            if(std::isnan(it->second->rawData->time)) return _on_no_time(event.id);
            event.masterTime = it->second->rawData->time;
            if( !std::isnan(_offset) )
                event.masterTime += _offset;
        } else {
            NA64DP_RUNTIME_ERROR( "Unsupported chip type of detector \"%s\""
                    " to retrieve event master time."
                    , calib::Handle<nameutils::DetectorNaming>::get()[_masterDetID].c_str()
                    );
        }
        return kOk;
    }
};  // class SADCSelectMax

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( PickMasterTime, cdsp, cfg
                , "Copy time value from certain detector to an event's master"
                  " time field." ) {
    na64dp::handlers::PickMasterTime::OnNoHit onNoHit;
    if( ! cfg["onNoHitInMaster"] ) onNoHit = na64dp::handlers::PickMasterTime::kEmitError;
    else {
        const std::string & onNoHitStrexpr
            = cfg["onNoHitInMaster"].as<std::string>();
        if( "omit" == onNoHitStrexpr )
            onNoHit = na64dp::handlers::PickMasterTime::kOmit;
        else if( "drop" == onNoHitStrexpr )
            onNoHit = na64dp::handlers::PickMasterTime::kDrop;
        else if( "emitError" == onNoHitStrexpr )
            onNoHit = na64dp::handlers::PickMasterTime::kEmitError;
        else
            NA64DP_RUNTIME_ERROR( "Unknown mode for events without master time:"
                    " \"%s\". Permitted variants: omit, drop, emitError."
                    , onNoHitStrexpr.c_str() );
    }
    return new na64dp::handlers::PickMasterTime( cdsp
            , cfg["override"] ? cfg["override"].as<std::string>() : ""
            , cfg["offset"] ? cfg["offset"].as<float>() : 0
            , onNoHit
            );
}

