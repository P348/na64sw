#include "na64calib/evType.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/stwTDC.hh"
#include "na64util/str-fmt.hh"
#include <cmath>
#include <limits>
#include <log4cpp/Priority.hh>

namespace na64dp {
namespace handlers {

/**\brief Computes "software trigger signature"
 *
 * In 2023 a special device (DAQ-integrated scaler) was introduced to provide
 * mixture of events of various types. These types differ by trigger (physical
 * condition firing data recording). In principle, correct NA64 trigger
 * bits must be proveded/set by DAQ equipment and/or DAQ decoding library.
 * However (at least for 2023, 2024) it is not the case so far.
 *
 * This handler is designed to look for presence of certain channels of NA64TDC
 * chip (of `event::StwTDCHit`), compare recorded values with time values and
 * logic-OR trigger bit expression.
 *
 * \code{.yaml}
 *     - _type: InferTriggerBitsTDC
 *       # bit number to set if signal is in the range; required, int
 *       nBit: ...
 *       # TDC selector
 *       applyTo: ... # example: "kin == STT && wireNo == 37"
 *       # offset to apply; float, default
 *       offset: 0
 *       # range to check the time value after offset is applied; opt list of float
 *       range: [-.inf, .inf]
 * \endcode
 *
 * An instance of this trigger will set certain bit of event's `trigger`
 * attribute if requested channel has time matching the window (`timeMin`,
 * `timeMax`).
 *
 * \note Reulting time is calculated as t = t0 - t', where t0 is offset and t'
 *       is the "corrected time" of TDC detector.
 * */
class InferTriggerBitsTDC
            : public AbstractHitHandler<event::StwTDCHit>
            , public calib::Handle<EventBitTags>
            {
public:
    typedef decltype(event::Event::trigger) TriggerInt_t;
protected:
    /// Ctr arg, converted to `_bits` upon trigger tags update
    const std::set<std::string> _bitTagsTxt;
    /// Bit number to set
    TriggerInt_t _bits;
    /// Allowed range for `time`
    const float _timeRange[2];
    /// Expected offset, will be subtracted (TDC modified)
    const float _expectedOffset;
    /// Whether to set event's master time based on the value
    bool _setMasterTime;

    /// Updates mapping
    void handle_update(const EventBitTags & tagCodes) override {
        if(!_bitTagsTxt.empty()) {
            _bits = 0x0;
            for(const auto & tag : _bitTagsTxt) {
                auto it = tagCodes.triggers.find(tag);
                if(tagCodes.triggers.end() == it) {
                    NA64DP_RUNTIME_ERROR("Trigger tags semantics update does not"
                            " provide code for \"%s\" trigger.", tag.c_str());
                }
                if(0x0 == it->second) {
                    log().warn("Trigger tag \"%s\" corresponds to none bits set"
                            " -- handler will not set any trigger bits.", tag.c_str());
                }
                _bits |= it->second;
            }
        }
    }
public:
    InferTriggerBitsTDC( calib::Dispatcher & cdsp
                       , const std::string & sel
                       , TriggerInt_t bits
                       , const std::set<std::string> & bitText
                       , float timeMin, float timeMax
                       , float expectedOffset
                       , log4cpp::Category & logCat
                       , bool setMasterTime=false
                       , const std::string & namingClass="default"
                       )
        : AbstractHitHandler<event::StwTDCHit>(cdsp, sel, logCat, namingClass)
        , calib::Handle<EventBitTags>("default", cdsp)
        , _bitTagsTxt(bitText)
        , _bits(bits)
        , _timeRange{timeMin, timeMax}
        , _expectedOffset(expectedOffset)
        , _setMasterTime(setMasterTime)
        {}

    bool process_hit(EventID eid, DetID did, event::StwTDCHit & tdcHit) override {
        if(!tdcHit.rawData) {
            // error, because data source must provide this info if this
            // handler is active and there is hit available for certain
            // detector ID (TDC channel)
            NA64DP_RUNTIME_ERROR("No `rawData' set for hit item \"%s\""
                    , naming()[did].c_str() );
        }
        if(std::isnan(tdcHit.correctedTime)) {
            tdcHit.correctedTime = _expectedOffset - tdcHit.rawData->time;
        }
        if( tdcHit.correctedTime > _timeRange[0]
         && tdcHit.correctedTime <= _timeRange[1] ) {
            _current_event().trigger |= _bits;
        }
        if(_setMasterTime && std::isfinite(tdcHit.correctedTime)) {
            _current_event().masterTime = tdcHit.correctedTime;
        }
        return true;
    }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( InferTriggerBitsTDC, cdsp, cfg
        , "Infers trigger bits based on TDC channel(s)") {
    auto & L = na64dp::aux::get_logging_cat(cfg);
    na64dp::handlers::InferTriggerBitsTDC::TriggerInt_t t = 0x0;
    if( cfg["nBit"] && cfg["nBit"].as<size_t>() ) {
        t |= (1 << cfg["nBit"].as<size_t>());
    }
    if( cfg["bits"] && cfg["bits"].as<na64dp::handlers::InferTriggerBitsTDC::TriggerInt_t>() ) {
        if(t) {
            // todo: better to raise error?
            L << log4cpp::Priority::WARN
              << "\"bits\" parameter overrides \"nBit\", latter will be ignored.";
        }
        t = cfg["bits"].as<na64dp::handlers::InferTriggerBitsTDC::TriggerInt_t>();
    }
    std::set<std::string> bitSemantics;
    if( cfg["bitSemantics"] ) {
        const auto vec = cfg["bitSemantics"].as< std::vector<std::string> >();
        if(t) {
            // todo: better to raise error?
            L << log4cpp::Priority::WARN
              << "\"bitSemantics\" parameter will supersede any \"bits\""
              " or \"nBit\" parameter(s) given upon next update of trigger"
              " bit semantics.";
        }
        bitSemantics.insert(vec.begin(), vec.end());
    }
    float timeMin = -std::numeric_limits<float>::infinity()
        , timeMax =  std::numeric_limits<float>::infinity()
        ;
    if(cfg["range"]) {
        timeMin = cfg["range"][0].as<float>();
        timeMax = cfg["range"][1].as<float>();
    }
    float offset = 0.;
    if(cfg["offset"]) {
        offset = cfg["offset"].as<float>();
    }
    const bool setMasterTime = cfg["setMasterTime"].as<bool>(false);
    const std::string sel = na64dp::aux::retrieve_det_selection(cfg);
    L << log4cpp::Priority::INFO
      << "Trigger bit(s) " << std::hex << std::showbase << (unsigned long) t
      << " will be set if time on TDC channel matching selection \""
      << sel << "\" is in range (" << timeMin << ";" << timeMax << "].";
    return new na64dp::handlers::InferTriggerBitsTDC( cdsp, sel, t
            , bitSemantics
            , timeMin, timeMax, offset, L
            , setMasterTime, na64dp::aux::get_naming_class(cfg) );
}

