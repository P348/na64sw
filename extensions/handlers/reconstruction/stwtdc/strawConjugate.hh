#pragma once

/**\file
 * \brief Declares straw hit-to-score conversion classes
 *
 * This header defines specialization of `TrackScoreTraits` for 1D hit
 * conversion and hit conjugation rule(s) for NA64TDC-based detectors (mainly
 * straws).
 *
 * \ingroup track-combining-hits
 * */

#include "tracking/createScore.hh"
#include "na64calib/placements.hh"
#include "na64dp/DSuLConjugationRule.hh"

namespace na64dp {
namespace aux {
template<typename HitT> struct TrackScoreTraits;  // fwd (undefined)

/**\brief Track score traits for straw hit
 *
 * Defines (local) geometrical reconstruction operation from
 * StwTDCHit into a TrackScore. For simplest case ("straw in hodoscopic mode",
 * when `StwTDCHit::distance` is not set) just converts wire number into
 * local normalized coordinate of newly created straw. When `distance` is set
 * appends score info with `DriftDetScoreFeatures` instance by
 * `TrackScore::driftFts` attribute.
 * */
template<>
struct TrackScoreTraits<event::StwTDCHit> {
    /// Placement cache subject: a plane + standard resolution + 
    struct PlaneEntry {
        /// String name for det
        std::string detName;
        /// Number of tubes in the plane
        int nWires;
        /// Own affine transform induced by the plane, only used to compute
        /// wire extremities if drift distance is provided
        util::Transformation t;
        /// Resolution (normalized) -- provided by placement or defined
        /// as `stride/sqrt(12)`. Used only in hodoscopic mode.
        Float_t resolution;
    };
    /// A conversion cache type -- plane description excerpt per plane ID
    typedef std::unordered_map<PlaneKey, PlaneEntry> Cache;
    /// Converts placement into cache entry and adds an entry into general cache
    static void recache_placement( Cache & cache
                                 , PlaneKey pk
                                 , const calib::Placement & placement
                                 );

    ///\brief Copies/converts straw hit into track score, 1D score
    ///
    /// Position will be calculated based on wire number detector ID.
    /// Geometrical information of the plane is used to perform conversion
    /// from wire units to metrical ones.
    ///
    /// Does not create a score and returns null reference if could not
    /// convert.
    ///
    /// \note Returned detector ID loses information about wire (i.e. it
    ///       becomes a plane ID).
    ///
    /// \returns reference to null pointer if can't convert
    static std::pair<DetID_t, mem::Ref<event::TrackScore> >
    create( LocalMemory & lmem
          , mem::Ref<event::StwTDCHit> c
          , DetID did
          , Cache & cache
          );
};

}  // namespace ::na64dp::aux


/**\brief Hit conjugation rule for straw detectors
 *
 * Assumes four-layered detector with X1&X2 and Y1&Y2 set of paired layers.
 *
 * \ingroup track-combining-hits
 * */
class FourLayerStrawCombinationRule
        : public util::DSuLHitCombinationRule<4, event::StwTDCHit> {
public:
    typedef event::StwTDCHit HitType;
    static constexpr util::Arity_t arity=4;
    /// Structure keeping geometrical information, relevant to clusters
    /// conjugation
    struct PlacementsCache {
        // ...
    };

    ///\brief Special ID generator for straws
    ///
    /// This generator yields clustered combinations
    struct StrawLayersCombinations : public util::iCombinedHitsGenerator<HitRef_t, 4> {
    private:
    public:
        StrawLayersCombinations() {}
        /// ...
        bool get(std::array<HitRef_t, 4> & dest) override;
        /// Re-sets generator for reentrant usage.
        void reset() override;
    };
protected:
    /// Omits hits with unset projection ID (already conjugated, service ones,
    /// etc.) and forwards to parent if ok
    bool _check_hit_ref(HitRef_t p) const override {
        WireID wid(p.second->first.payload());
        if( (!wid.proj_is_set()) || wid.proj_is_special() ) return false;
        return util::DSuLHitCombinationRule<arity, event::StwTDCHit>::_check_hit_ref(p);
    }
    /// Returns number of set to put DetID with WireID payload specific for
    /// four-layered detectors
    size_t _n_set(HitRef_t did) const override;
public:
    static std::shared_ptr< util::iCombinedHitsGenerator<HitRef_t, 4> >
        construct_straw_hit_combinations_generator(StationKey
                , const std::array<Hits, 4> & set
                , const iPerStationHitCombinationRule<HitRef_t, 4> &);

    FourLayerStrawCombinationRule( calib::Dispatcher & cdsp
            , const std::string & sel
            , log4cpp::Category & logCat
            , bool permitPartialCombinations=true
            , typename iPerStationHitCombinationRule<HitRef_t, 4>::GeneratorCtr dftCtr
                    //=iPerStationHitCombinationRule<HitRef_t, 4>::construct_basic_cartesian_generator
                    =construct_straw_hit_combinations_generator
            , const std::string &detNamingSubclass="default"
            ) : DSuLHitCombinationRule<4, event::StwTDCHit>(cdsp, sel, logCat, permitPartialCombinations
                , dftCtr, detNamingSubclass)
              {}
    /// Sets score values coordinates using hits and placement information
    void calc_score_values( event::TrackScore &
                          , const std::array<HitRef_t, 4> & ) override;
};  // class FourLayerStrawCombinationRule

}  // namespace na64dp

