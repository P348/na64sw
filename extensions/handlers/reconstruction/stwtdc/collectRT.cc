#include "na64dp/abstractHitHandler.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include "na64util/TDirAdapter.hh"

#include <cmath>

#include <TH2F.h>

namespace na64dp {
namespace handlers {

/*\brief Creates R(T) plots for straw detectors, based on tracking residuals
 *
 * Iterates over scores included in the fitted tracks (so only those hits
 * which where considered in tracking are included, obviously) with wire number
 * being set and fills 2D histogram of residuals vs. time.
 *
 * If no `driftFts` field provided by the score or its `uError` field is empty,
 * relies on the unbiased residuals information and behaves just like generic
 * `Histogram2D` with appropriate getters (hit time vs. unbiased residual).
 *
 * \note Use with caution in the permissive mode of applying R(T) -- a mixture
 *       of both behaviours may be or may be not be your purpose.
 *
 * \note This handler requires track fitting to be performed as it uses
 *       residuals information to build the R(T) plot.
 *
 * \ingroup handlers tracking-handlers
 * */
class CollectRT : public AbstractHitHandler<event::Track>
                , public util::TDirAdapter {
protected:
    /// Local cache of straws chip ID
    DetChip_t _stwChipID;
    /// Local cache of straws kin ID
    DetKin_t _stwKinID;
    /// Minimum weight to consider scores
    double _minWeight;
    /// Base name for histograms
    const std::string _hstBaseName;
    /// Cache of histograms
    std::unordered_map<PlaneKey, TH2F *> _hists;
    /// Number of bins in histograms (distance, time)
    int _nBins[2];
    /// Ranges for histogram axes (distance, time)
    float _ranges[2][2];

    /// Straw TDC cache (2022-specific)
    int32_t _timeXY, _timeUV;
protected:
    void handle_update( const nameutils::DetectorNaming & nm ) override {
        util::TDirAdapter::handle_update(nm);
        auto p = nm.kin_id("ST");
        _stwChipID = p.first;
        _stwKinID = p.second;
    }
public:
    CollectRT( calib::Dispatcher & cdsp
             , const std::string & trackSelection
             , const std::string & hstBaseName
             , int nBinsDistance, float distanceMin, float distanceMax
             , int nBinsTime, float timeMin, float timeMax
             , double minWeight
             , log4cpp::Category & logCat
             , const std::string namingClass="default"
             ) : AbstractHitHandler<event::Track>(cdsp, trackSelection, logCat, namingClass)
               , util::TDirAdapter(cdsp)
               , _stwChipID(0x0), _stwKinID(0x0)
               , _minWeight(minWeight)
               , _hstBaseName(hstBaseName)
               , _nBins{nBinsDistance, nBinsTime}
               , _ranges{ {distanceMin, distanceMax}
                        , {timeMin, timeMax}
                        }
               {
        cdsp.subscribe<nameutils::DetectorNaming>(*this, "default");
    }

    virtual ~CollectRT();

    ProcRes process_event(event::Event &) override;

    /// Iterates over track's score retrieving hits for straws
    bool process_hit(EventID, TrackID, event::Track &) override;

    void finalize() override;
};

AbstractHandler::ProcRes
CollectRT::process_event(event::Event & e) {
    DetID sttXY = naming()["STT0:XY-64"]
        , sttUV = naming()["STT0:UV-64"]
        ;
    auto it = e.stwtdcHits.find(sttXY);
    if( e.stwtdcHits.end() != it ) {
        _timeXY = it->second->rawData->time;
        //std::cout << " !!! XY: " << _timeXY << std::endl;
    }
    it = e.stwtdcHits.find(sttUV);
    if( e.stwtdcHits.end() != it ) {
        _timeUV = it->second->rawData->time;
        //std::cout << " !!! UV: " << _timeUV << std::endl;
    }
    return AbstractHitHandler<event::Track>::process_event(e);
}

bool
CollectRT::process_hit( EventID
                      , TrackID
                      , event::Track & track
                      ) {
    assert(_stwChipID);
    assert(_stwKinID);
    for( auto & sfiPair : track.scores ) {
        // omit non-straw detectors
        if( sfiPair.first.chip() != _stwChipID || sfiPair.first.kin() != _stwKinID ) continue;
        PlaneKey pk(sfiPair.first);
        if( ! sfiPair.second->score ) continue;
        event::TrackScore & score = *(sfiPair.second->score);
        if( (!std::isnan(_minWeight)) && sfiPair.second->weight < _minWeight) continue;  // xxx?
        auto it = _hists.find(pk);
        if( _hists.end() == it ) {
            // no histogram exists for this detector entity -- create and insert
            std::string histName = _hstBaseName;
            dir_for(pk, histName)->cd();
            TH2F * newHst = new TH2F( histName.c_str()
                                    , "R(T); distance; time"
                                    , _nBins[0], _ranges[0][0], _ranges[0][1]
                                    , _nBins[1], _ranges[1][0], _ranges[1][1]
                                    );
            auto ir = _hists.emplace( pk, newHst );
            it = ir.first;
            assert(ir.second);  // something wrong with detector ID
        }
        TH2F * hst = it->second;
        for( auto hitP : score.hitRefs ) {
            DetID hitID(hitP.first);
            WireID wid(hitID.payload());
            float uResidual; // = score.lRUErr[0];
            if( std::isnan(uResidual) ) continue;
            float time = std::get< mem::Ref<event::StwTDCHit> >(hitP.second)->rawData->time;
            // Prefer residuals provided by `driftFts.uError` if possible
            if( score.hitRefs.size() == 1
             && score.driftFts
             && !std::isnan(score.driftFts->uError[0]) ) {
                uResidual = score.driftFts->uError[0];
            } else {
                uResidual = sfiPair.second->lRUErr[0];
            }
            hst->Fill( uResidual // distance
                     , time - _timeXY// time
                     );
            //std::cout << time << " - " << _timeXY << " = " << time - _timeXY << std::endl;
            //std::cout << " xxx " << uResidual << ", " << time << std::endl;  // XXX
        }
    }
    return true;
}

void
CollectRT::finalize() {
    for( auto idHstPair : _hists ) {
        auto dir = tdirectories()[DetID(idHstPair.first)];
        assert(dir);
        dir->cd();
        idHstPair.second->Write();
    }
}

CollectRT::~CollectRT() {
    for( auto idHstPair : _hists ) {
        delete idHstPair.second;
    }
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( CollectRT, cdsp, cfg
                , "Builds R(T) plots for Straws. Requires unbiased residuals."
                ) {
    return new ::na64dp::handlers::CollectRT( cdsp
                , na64dp::aux::retrieve_det_selection(cfg)
                , cfg["histName"] ? cfg["histName"].as<std::string>() : "RT"
                , cfg["nBinsDistance"] ? cfg["nBinsDistance"].as<int>() : 100
                , cfg["distanceRange"][0].as<float>(), cfg["distanceRange"][1].as<float>()
                , cfg["nBinsTime"] ? cfg["nBinsTime"].as<int>() : 100
                , cfg["timeRange"][0].as<float>(), cfg["timeRange"][1].as<float>()
                , cfg["minWeight"] ? cfg["minWeight"].as<double>() : 0
                , ::na64dp::aux::get_logging_cat(cfg)
                , ::na64dp::aux::get_naming_class(cfg)
                );
}

#endif // defined(ROOT_FOUND) && ROOT_FOUND

