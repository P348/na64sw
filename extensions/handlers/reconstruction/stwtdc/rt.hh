#pragma once
#include "na64common/calib/sdc-integration.hh"  // for string routines

/**\file
 * \brief Defines common routines for dealing with R(T) distributions
 *
 * This file defines common types to be used in applications and handlers
 * that use R(T) as calibration data type -- function type, etc.
 * */

namespace na64dp {
namespace calib {

/// This is R(T) function taken from some of the Volkov's code.
/// Must be parameterised with, like a=~1/11.23, b=~1/1.203, t0 and tWidth
/// were introduced to additionaly stabilize the function
struct RT {
    float a  ///< a coefficient defining spanning of the R(T) curve
        , b  ///< exponential coefficient, defining curvative and spanning of R(T)
        , t0  ///< T0 coefficient, defining R(T) origin
        , maxR  ///< cut-off distance value
        ;
    /// This kludgy flag was introduced to cope with time axis being somehow
    /// inverted in certain runs.
    bool invertTime;
    /// Returns single R value for certain T.
    float operator()(const float t) {
        float arg = t - t0;
        if(invertTime) arg *= -1;
        return a*pow(arg, b);
    }
};

/**\brief Intermediate type used to update the R(T)-depending objects
 *
 * This type is transmitted from calibration data loader to a consumer in
 * order the latter to build a cached result, optimized for subsequent
 * usage (with compiled selector). */
typedef std::pair<std::string, RT> RTCalibEntry;

}  // namespace ::na64dp::calib
}  // namespace na64dp

namespace sdc {
template<>
struct CalibDataTraits<na64dp::calib::RTCalibEntry> {
    /// Type's name alias
    static constexpr auto typeName = "na64sw/rt";
    /// A collection type for parsed R(T) objects
    template<typename T=na64dp::calib::RTCalibEntry>
        using Collection=std::unordered_map< typename T::first_type
                                           , typename T::second_type
                                           >;
    /// Action to append collection of parsed objects
    template<typename T=na64dp::calib::RTCalibEntry>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t lineNo
                              ) { col[e.first] = e.second; }
    /// Parses the CSV line into `na64dp::calib::RTCalibEntry` data object
    ///
    /// Parsing can be affected by the parser state to handle changes within
    /// a file. Namely, metadata "columns=name1,name2..." may denote number of
    /// token with certain semantics. By default, "name,x,y,peakpos" is assumed
    ///
    /// The `lineNo` argument can be used for diagnostic purposes (e.g.
    /// to report a format error).
    static na64dp::calib::RTCalibEntry
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      );
};  // struct CalibDataTraits<RTCalibEntry>
}  // namespace sdc

