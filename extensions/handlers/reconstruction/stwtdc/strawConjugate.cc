#include "strawConjugate.hh"
#include "na64util/numerical/biplaneIntersection.h"

namespace na64dp {
namespace aux {

void
TrackScoreTraits<event::StwTDCHit>::recache_placement( Cache & cache
                                                     , PlaneKey pk
                                                     , const calib::Placement & placement
                                                     ) {
    // assure it is ordinary plane's placement
    if( calib::Placement::kRegularWiredPlane != placement.suppInfoType ) {
        throw errors::WrongPlacementTypeProvided(
                util::format( "Wrong type of placement information"
                " entry for \"%s\"", placement.name.c_str() ).c_str());
    }
    PlaneEntry & planeEntry = cache.emplace(pk, PlaneEntry{}).first->second;
    planeEntry.detName = placement.name;
    planeEntry.nWires = placement.suppInfo.regularPlane.nWires;
    planeEntry.t = util::transformation_by(placement);
    // normalized resolution
    if(!std::isnan(placement.suppInfo.regularPlane.resolution)) {
        planeEntry.resolution = placement.suppInfo.regularPlane.resolution;
    } else {
        planeEntry.resolution = 1./planeEntry.nWires;
        planeEntry.resolution /= sqrt(12.);
    }
    // normalize resolution to measured size
    planeEntry.resolution /= placement.size[0];
}

std::pair<DetID_t, mem::Ref<event::TrackScore> >
TrackScoreTraits<event::StwTDCHit>::create( LocalMemory & lmem
          , mem::Ref<event::StwTDCHit> c
          , DetID did
          , Cache & cache
          ) {
    // IMPORTANT: detector ID converted to plane key cuts away information
    // about wires (this is deliberate conversion as scores must be
    // identified by planes at the most detailed level).
    PlaneKey pk(did);
    assert(!WireID(pk.payload()).wire_no_is_set());
    WireID wid(did.payload());
    assert(wid.wire_no_is_set());
    // find plane description
    auto it = cache.find(pk);
    if(cache.end() == it) {
        return {did, nullptr};  // no placement for this detector
    }
    const PlaneEntry & pe = it->second;
    if( wid.wire_no() > pe.nWires ) {
        NA64DP_RUNTIME_ERROR( "Straw-TDC hit to-score conversion error:"
                " provided number of wire %d is greater than number of"
                " wires in the plane (%d)."
                , (int) wid.wire_no()
                , pe.nWires );
    }
    auto dest = lmem.create<event::TrackScore>(lmem);
    util::reset(*dest);
    // set single local coordinate based on stride
    //dest->lR[0] = (wid.wire_no() + .5 - pe.nWires/2.) * pe.stride;
    // ^^^ in wire units (deprecated)
    //dest->lR[0] = (wid.wire_no() + .5 - pe.nWires/2.)/pe.nWires;
    dest->lR[0] = util::wire_to_normalized_DRS(wid.wire_no(), pe.nWires);
    // ^^^ normalized, -0.5 - 0.5
    assert( dest->lR[0] >= -.5 );
    assert( dest->lR[0] <=  .5 );
    assert(!std::isnan(dest->lR[0]));
    dest->lR[1] = std::nan("0");

    // If `distance' is not set for the hit, we use Straw in a "hodoscopic
    // mode", e.g. do not account for it's drift properties. Resolution is
    // bad, but hit can still be useful for certain purposes. If it is set,
    // we append the score with drift-specific info
    if( !std::isnan(c->distance) ) {
        // check for outliers first
        dest->driftFts
            = lmem.create<event::DriftDetScoreFeatures>(lmem);
        util::reset(*dest->driftFts);
        dest->driftFts->distance = c->distance;
        assert(dest->driftFts->distance >= 0);
        assert(!std::isnan(dest->driftFts->distance));
        dest->driftFts->distanceError = c->distanceError;
        // this is known to be useful for some track reconstruction algorithms
        const util::Transformation & t = it->second.t;
        const float stride = t.u().norm()/it->second.nWires;
        dest->driftFts->maxDistance = stride/2;
        if( c->distance > dest->driftFts->maxDistance ) {
            NA64DP_RUNTIME_ERROR( "Drift distance %.3ecm for detector %s"
                    " exceeds max distance of %.3ecm."
                    , c->distance
                    , pe.detName.c_str()
                    , dest->driftFts->maxDistance
                    );
        }
        // Calculate wire's ends (extremities)
        util::Vec3 up{{dest->lR[0], +.5, 0}}
                 , lo{{dest->lR[0], -.5, 0}}  // TODO: why swapping Y changes results?
                 ;
        t.apply(up);
        t.apply(lo);

        for(int i = 0; i < 3; ++i) {
            dest->driftFts->wireCoordinates[i  ] = lo.r[i];
            dest->driftFts->wireCoordinates[i+3] = up.r[i];
        }
    } else {
        // set measurement uncertainty for hodoscopic mode
        dest->lRErr[0] = pe.resolution;
    }

    // associate hit
    dest->hitRefs.emplace(did, c);
    assert(pk.is_payload_set());
    return {pk, dest};
}

}  // namespace ::na64dp::aux


//
// Rule

size_t
FourLayerStrawCombinationRule::_n_set(HitRef_t hr) const {
    DetID did(hr.second->first);
    assert(did.is_payload_set());
    assert(did.payload_as<WireID>()->proj_is_set());
    auto p = did.payload_as<WireID>()->proj();
    if( p == WireID::kX || p == WireID::kU ) {
        return 0;  // X1/U1 -- 1st
    }
    if( p == WireID::kX2 || p == WireID::kU2 ) {
        return 1;  // X2/U2 -- 2nd
    }
    if( p == WireID::kY || p == WireID::kV ) {
        return 2;  // Y1/V1 -- 3rd
    }
    if( p == WireID::kY2 || p == WireID::kV2 ) {
        return 3;  // Y2/V2 -- 4th
    }
    NA64DP_RUNTIME_ERROR("NA64 Straw TDC detector \"%s\" has unsupported"
            " projection label -- unable to figure out tuplet's number of"
            " element."
            , naming()[did].c_str());
}

void
FourLayerStrawCombinationRule::calc_score_values( event::TrackScore & s
                                  , const std::array<HitRef_t, 4> & hrs ) {
    for(util::Arity_t n = 0; n < 4; ++n) {
        if(!hrs[n].first) continue;
        //if(hrs[n].second->second->distance)
    }
    throw std::runtime_error("TODO: straw drs");
    //drsXY[0] = hrs[0]->second->position;
    //drsXY[1] = hrs[1]->second->position;
}

std::shared_ptr< util::iCombinedHitsGenerator<FourLayerStrawCombinationRule::HitRef_t, 4> >
FourLayerStrawCombinationRule::construct_straw_hit_combinations_generator(
                  StationKey
                , const std::array<Hits, 4> & set
                , const iPerStationHitCombinationRule<HitRef_t, 4> &) {
    throw std::runtime_error("TODO: instantiate generator");
}

#if 0
void
FourLayerStrawCombinationRule::mrs( float 
                                  , const std::array<HitRef_t, 4> & hrs
                                  ) {
    throw std::runtime_error("TODO: straw mrs");
    #if 0
    assert(mrsXYZ);
    auto it1 = _placementsCache.find(hrs[0]->first)
       , it2 = _placementsCache.find(hrs[1]->first)
       ;
    if( it1 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for APV plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[0]->first].c_str() );
    }
    if( it2 == _placementsCache.end() ) {
        NA64DP_RUNTIME_ERROR("No placements info provided for APV plane"
                " \"%s\" -- unable to conjugate hits."
                , naming()[hrs[1]->first].c_str() );
    }
    const PlacementsCache & c1 = it1->second
                        , & c2 = it2->second;
    const auto & pos1 = hrs[0]->second->position
             , & pos2 = hrs[1]->second->position;
    // find MRS point as "projected intersection" of two measurements
    double r01[3] = { c1.u0[0]*pos1, c1.u0[1]*pos1, c1.u0[2]*pos1 }
         , r02[3] = { c2.u0[0]*pos2, c2.u0[1]*pos2, c2.u0[2]*pos2 };
    double x1[3], x2[3];  // output vectors
    int rc = na64sw_projected_lines_intersection(
            it1->second.v, it2->second.v, NULL,
            r01, r02,
            x1, x2);
    if(rc) {
        NA64DP_RUNTIME_ERROR("Failed to conjugate hits of %s and %s:"
                " na64sw_projected_lines_intersection() return code %d"
                , naming()[hrs[0]->first].c_str()
                , naming()[hrs[1]->first].c_str()
                , rc
                );
    }
    // set MRS to be mean point of two "intersections"
    for(int i = 0; i < 3; ++i) {
        mrsXYZ[i] = (x1[i] + x2[i])/2;
    }
    #endif
}
#endif

// for handler implem, see createScore.cc

}  // namespace na64dp
