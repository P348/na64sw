#include "rt.hh"

#include "na64event/data/track.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64common/calib/sdc-integration.hh"

namespace sdc {

na64dp::calib::RTCalibEntry
CalibDataTraits<na64dp::calib::RTCalibEntry>::parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & srcID
                      , sdc::aux::LoadLog * loadLogPtr
                      ) {
    #ifndef NDEBUG
    const size_t lineNo_ = m.get<size_t>("@lineNo");
    assert(lineNo_ == lineNo);
    #endif
    // create object
    na64dp::calib::RTCalibEntry obj;
    // Create and validate columns order
    auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
            //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
            .interpret(aux::tokenize(line), loadLogPtr);
    // ^^^ note: every CSV block must be prefixed by columns order,
    // according to Anton's specification
    // get columns
    obj.first = csv("name");
    obj.second.a = csv("a", 1.);
    obj.second.b = csv("b", 1.);
    obj.second.t0 = csv("t0", 0.);
    obj.second.maxR = csv("maxR", 0.);
    obj.second.invertTime = m.get<bool>("invertTimeAxis", false, lineNo);
    obj.second.invertTime = csv("invertTimeAxis", obj.second.invertTime);
    return obj;
}

}  // namespace sdc

namespace na64dp {
namespace calib {
typedef ::sdc::CalibDataTraits<RTCalibEntry>::Collection<RTCalibEntry> RTEntries;
}  // namespace ::na64dp::calib

namespace handlers {

/**\brief Uses R(T) calibration to track scores, where applicable.
 *
 * \note Currently supports only Straw detectors handle by NA64TDC chip.
 * */
class ApplyRT : public AbstractHitHandler<event::StwTDCHit>
              , public calib::Handle<calib::RTEntries>
              {
public:
    enum OutliersTreatmentMode {
        kNoDistance,  /// Do not assign distance to this hits
        kRemove,  /// Remove off-time hits
        kUse,  /// Use hits anyway (probably with invalid distance)
    };
protected:
    /// Mode to handle outliers
    OutliersTreatmentMode _outliersMode;
    /// Cache of R(T) objects by detector ID
    std::unordered_map<DetID, calib::RT> _rts;
    /// Collection of detector names with missed R(T) calibrations
    std::unordered_set<std::string> _missedDets;
protected:
    /// Updates cache of R(T) objects
    void handle_update( const calib::RTEntries & rts ) override {
        for(const auto & rte : rts) {
            auto detID = naming()[rte.first];
            _rts[detID] = rte.second;
            _log << log4cpp::Priority::DEBUG
                 << "Updated R(T) calibrations for " << naming()[detID];
        }
    }
public:
    ApplyRT( calib::Dispatcher & cdsp
           , const std::string & sel
           , OutliersTreatmentMode outliersMode
           ) : AbstractHitHandler<event::StwTDCHit>( cdsp, sel )
             , calib::Handle<calib::RTEntries>("default", cdsp)
             , _outliersMode(outliersMode)
             {}
    bool process_hit(EventID, HitKey, event::StwTDCHit &) override;
    /// Prints warning with the list of missed calibration planes names
    void finalize() override;
};  // class ApplyRT


bool
ApplyRT::process_hit(EventID eid, HitKey hitKey, event::StwTDCHit & hit) {
    PlaneKey planeKey(hitKey);
    auto it = _rts.find(planeKey);
    if( _rts.end() == it ) {
        _missedDets.insert(naming()[planeKey]);
        return true;
    }
    calib::RT & rt = it->second;
    hit.distance = rt(hit.rawData->time);

    #if 0  // XXX
    std::cout << naming()[planeKey] << ": t=" << hit.rawData->time
              << " -> R=" << hit.distance << std::endl;
    #endif

    if( (std::isnan(hit.distance) || hit.distance < 0 || hit.distance > rt.maxR)
     && _outliersMode != kUse) {
        if( _outliersMode == kNoDistance ) {
            hit.distanceError = hit.distance = std::nan("0");
            return true;
        } else if( _outliersMode == kRemove ) {
            _set_hit_erase_flag();
            return true;
        }
    }
    assert(_outliersMode != kUse && hit.distance >= 0);  // XXX
    #warning "FIXME: hardcoded straw R(T) error"
    hit.distanceError = .05;  // TODO
    return true;
}

void
ApplyRT::finalize() {
    if( _missedDets.empty() ) return;
    std::set<std::string> sorted(_missedDets.begin(), _missedDets.end());
    std::ostringstream oss;
    bool isFirst = true;
    for( auto & nm : sorted ) {
        oss << (isFirst ? "" : ", ") << nm;
        isFirst = false;
    }
    _log << log4cpp::Priority::WARN << "Following detectors missed R(T)"
        " calibrations: " << oss.str() << ".";
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( ApplyRT, mgr, cfg
                , "Applies R(T) to straw hits to get hit distance within a wire" ) {
    // assure calibration data conversion is set up
    mgr.get_loader<na64dp::calib::SDCWrapper>("sdc")
        .enable_sdc_type_conversion<na64dp::EventID, na64dp::calib::RTCalibEntry>();
    na64dp::calib::CIDataAliases::self()
            .add_dependency(sdc::CalibDataTraits<na64dp::calib::RTCalibEntry>::typeName, "naming");
    na64dp::handlers::ApplyRT::OutliersTreatmentMode mode;
    {
        const std::string outlMStr = cfg["outliers"] ? cfg["outliers"].as<std::string>() : "noDistance";
        if( outlMStr == "noDistance" ) {
            mode = na64dp::handlers::ApplyRT::kNoDistance;
        } else if( outlMStr == "remove" ) {
            mode = na64dp::handlers::ApplyRT::kRemove;
        } else if( outlMStr == "use" ) {
            mode = na64dp::handlers::ApplyRT::kUse;
        } else {
            NA64DP_RUNTIME_ERROR("Invalid outliers treatment mode: \"%s\","
                    " expected one of \"noDistance\", \"remove\", \"use\".");
        }
    }
    // instantiate handler
    return new ::na64dp::handlers::ApplyRT( mgr
                                          , na64dp::aux::retrieve_det_selection(cfg)
                                          , mode
                                          );
}

