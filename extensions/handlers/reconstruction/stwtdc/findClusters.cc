#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"
#include "na64detID/wireID.hh"

#include <TH1D.h>

namespace na64dp {
namespace handlers {

/**\brief Makes clusters from straw hits
 *
 * Contrary to APV detectors, the clustering procedure is much simpler in some
 * sense as single particle can not affect many wires. We assume a "cluster"
 * to be (mostly) of two consecutive wires (it doubtly to be more).
 *
 * Yet, hit consideration has to properly take into account L/R ambiguity and
 * R/T relations, typical for this kind of detectors. */
class FindClustersStraw : public AbstractHitHandler<event::StwTDCHit> {
public:
    /// Allocated for each encountered plane, maintains (sorted) array of
    /// triggered wire descriptors, used at the end of event to find sequencies
    struct PlaneCache {
        struct HitEntry {
            unsigned int wireNo:10; // we don't expect wire number to be > 1024
            unsigned int hitID:22;  // used to back-reference a hit
            float time;  // rough estimation of time
            /// This is the simplest comparison operator ignoring time info
            bool operator<(const HitEntry & other) const {
                return this->wireNo < other.wireNo;
            }
        };
        std::vector<HitEntry> entries;
    };
protected:
    /// Hit caches to use for clustering
    std::unordered_map<PlaneKey, PlaneCache> _eventCaches;
public:

    FindClustersStraw( calib::Dispatcher & ch
                     , const std::string & only
                     ) : AbstractHitHandler<event::StwTDCHit>(ch, only) {}
    /// Dispatches call to `process_hit()`, at the end of event does clustering
    /// creating new clusters
    virtual AbstractHandler::ProcRes process_event(event::Event & ) override;
    /// Fills appropriate `PlaneCache` instance
    virtual bool process_hit( EventID
                            , DetID
                            , event::StwTDCHit &) override;

};

bool
FindClustersStraw::process_hit( EventID
                              , DetID did
                              , event::StwTDCHit & hit ) {
    // strip wire info to get plane ID
    PlaneKey pk(did);
    // find cache entry and put hit entry there
    auto it = _eventCaches.emplace(pk, PlaneCache()).first;
    it->second.entries.push_back(PlaneCache::HitEntry{});  // TODO: ids
    return true;
}

FindClustersStraw::ProcRes
FindClustersStraw::process_event(event::Event & event) {
    // forward call to parent's implementation to fill the caches
    AbstractHitHandler<event::StwTDCHit>::process_event(event);
    // sort hits in caches
    // ...
    // do the clustering
    // ...
    return kOk;
}

}

REGISTER_HANDLER( FindClustersStraw, ch, yamlNode
                , "Handler for testing Straw detectors" ) {
    return new handlers::FindClustersStraw( ch
            , aux::retrieve_det_selection(yamlNode) );
}
}
