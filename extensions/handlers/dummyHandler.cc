#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/// A trivial handler that does nothing
class DummyHandler : public AbstractHandler {
public:
    /// Does nothing but allows event to be propagated through the handler
    virtual AbstractHandler::ProcRes process_event(event::Event & event) override;
};

//                          * * *   * * *   * * *

AbstractHandler::ProcRes
DummyHandler::process_event(event::Event &) {
    return kOk;
    // ^^^ this return code indicates that event may be propagated to the next
    // handler in the pipeline
}

}

// the macro below makes the handler to be registered in a system
REGISTER_HANDLER( Dummy  //< name of the registered handler class
                , ch  //< name of calibration data dispatcher possibly
                      // used below
                , yamlNode  //< name of the "configuration" variable possibly
                            // used below
                , "Dummy handler"  //< brief textual description of the handler
                ) {
    // code in {} after REGISTER_HANDLER macro shall return pointer to a new
    // instance of our handler class constructied with respect of parameters
    // provided by "configuration" object.
    // We have no parameters in constructor, so here we construct a simple
    // instance without any calls to "configuration"
    return new handlers::DummyHandler();
}

}

