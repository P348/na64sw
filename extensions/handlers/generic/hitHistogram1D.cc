#include "getterBasedROOTHistHandler.hh"

namespace na64dp {
namespace handlers {

using namespace event;

/**\brief A 1D histogram plotting handler
 *
 * A generic handler used to plot a single (optionally, weighted) variable
 * distribution.
 *
 * \image html handlers/Histogram1D-example-1.png
 * \image html handlers/Histogram1D-example-2.png
 *
 * Usage:
 * \code{.yaml}
 *     - _type: Histogram1D
 *       # Specifies a getter to use to retrieve a value from subject
 *       # type; str, required
 *       value: ...
 *       # A base _type for the histogram; str, required
 *       histName: ...
 *       # A description string template; required, str
 *       histDescr: ...
 *       # Number of bins in the histogram; int, required
 *       nBins: ...
 *       # Axis range to use; list of two floats, required
 *       range: [..., ...]
 *       # A multiplication factor apply for the value. Useful to re-scale
 *       # histogram in different units (for instance, MeV to GeV); float, optional
 *       factor: 1.0
 *       # To plot weighted histograms, this parameter may refer to a value
 *       # (getter) to be considered as a weighting function; float, optional
 *       weight: null
 *       # A path template within a TFile for the histogram object. If not
 *       # specified (or empty), a default one for the collection type will be
 *       # used; optional, str
 *       path: ""
 * \endcode
 *
 * Possible `subject`s (see links for lists of available getters for `value`):
 * * `sadcHits` -- na64dp::event::SADCHit
 * * `apvHits` -- na64dp::event::APVHit, fine granularity
 * * `apvHits-per-plane` -- na64dp::event::APVHit, per plane
 * * `f1Hits` -- na64dp::event::F1Hit, fine granularity
 * * `f1Hit-per-plane` -- na64dp::event::F1Hit, per plane
 * * `stwtdcHits` -- na64dp::event::StwTDCHit, fine granularity
 * * `stwtdcHits-per-plane` -- na64dp::event::StwTDCHit, per plane
 * * `apvClusters` -- na64dp::event::APVCluster
 * * `caloHits` -- na64dp::event::CaloHit
 * * `trackPoints` -- na64dp::event::TrackPoint
 * * `vertices` -- na64dp::event::Vertex
 *
 * The `...-per-plane` differs from its counterpart (like `APVHit-per-plane` from
 * `apvHits`) in the sense that for `-per-plnae` version a one histogram per
 * plane, while its counterpart bounds with an *elementary sensitive element*
 * (e.g. wire for GEMs or MMs).
 *
 * Users typically won't need the `-per-wire` versions unless very fine
 * granularity is need (e.g. cross-talk studies).
 *
 * \note Defined as a template alias to generic template `GetterBasedROOTHistHandler`
 *
 * \ingroup generic-handlers
 */
template<typename HitT>
using Histogram1D = handlers::GetterBasedROOTHistHandler<HitT, 1, TH1F>;

template<typename HitT> static AbstractHandler *
_construct_handler( calib::Manager & ch, const YAML::Node & cfg ) {
    std::string value = cfg["value"].as<std::string>();
    size_t firstDelim = value.find('.');
    assert( firstDelim != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<HitT> axisParams[1] = {
            { { cfg["nBins"].as<size_t>()
              , {cfg["range"][0].as<double>(), cfg["range"][1].as<double>()}
              , cfg["factor"] ? cfg["factor"].as<float>() : 1.
              }
            , util::value_getter<HitT>(value.substr(firstDelim + 1))
            }
        };
    return new Histogram1D<HitT>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<HitT>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

// Event specialization
template<> AbstractHandler *
_construct_handler<Event>( calib::Manager & ch, const YAML::Node & cfg ) {
    std::string value = cfg["value"].as<std::string>();
    aux::HstAxisParams<Event> axisParams[1] = {
            { { cfg["nBins"].as<size_t>()
              , {cfg["range"][0].as<double>(), cfg["range"][1].as<double>()}
              , cfg["factor"] ? cfg["factor"].as<float>() : 1.
              }
            , util::value_getter<Event>(value)
            }
        };
    return new GetterBasedROOTHistHandler<Event, 1, TH1F, void>( ch
                , axisParams
                , cfg["weight"] ? util::value_getter<Event>(cfg["weight"].as<std::string>()) : nullptr
                , cfg["histName"].as<std::string>()
                , cfg["histDescr"].as<std::string>()
                , cfg["path"] ? cfg["path"].as<std::string>() : ""
                , ::na64dp::aux::get_logging_cat(cfg)
                );
}

// TrackScore specialization
template<> AbstractHandler *
_construct_handler<event::ScoreFitInfo>( calib::Manager & ch, const YAML::Node & cfg ) {
    auto & L = ::na64dp::aux::get_logging_cat(cfg);
    std::string value = cfg["value"].as<std::string>();
    size_t firstDelim = value.find('.');
    assert( firstDelim != std::string::npos );  // guaranteed by caller

    const std::string getterName = value.substr(firstDelim + 1);
    // try to resolve extended getter by name first
    ExtendedTrackScoreGetters::Getter getter
        = ExtendedTrackScoreGetters::get_extended_getter(getterName);
    if(!getter) {
        // no extended getter -- try to resolve ordinary (stateless) getter
        getter.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(getterName);
        assert(getter.cllb.plainGetter);
        // ^^^ NOTE: throws exception if failed; if passed, mark it as plain
        //     getter and proceed
        getter.isPlain = true;
    } else {
        getter.isPlain = false;
    }
    ExtendedTrackScoreGetters::Getter weightGetter;
    if(cfg["weight"]) {
        // similar as above, but for weights this time
        const std::string weightGetterName
            = cfg["weight"].as<std::string>();
        weightGetter = ExtendedTrackScoreGetters::get_extended_getter(weightGetterName);
        if(!weightGetter) {
            weightGetter.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(weightGetterName);
            weightGetter.isPlain = true;
            assert(weightGetter.cllb.plainGetter);
        } else {
            weightGetter.isPlain = false;
        }
    } else {
        // NOTE: since `Getter' unionstruct has no ctr, one has to init it
        //       externally.
        weightGetter.isPlain = false;
        bzero(&weightGetter.cllb, sizeof(weightGetter.cllb));
    }

    aux::HstAxisParams<event::ScoreFitInfo> axisParams[1] = {
            { { cfg["nBins"].as<size_t>()
              , {cfg["range"][0].as<double>(), cfg["range"][1].as<double>()}
              , cfg["factor"] ? cfg["factor"].as<float>() : 1.
              }
            , getter
            }
        };

    const std::string dftDetSel = ::na64dp::aux::retrieve_det_selection(cfg);
    if(!dftDetSel.empty()) {
        L.warn("Default selector is discouraged for this subject"
                " type. Use `applyToTracks' to restrict consideration with"
                " certain tracks and `applyToDetectors' to select detectors within"
                " (filtered) tracks. Default selector is interpreted as"
                " `applyToDetectors' if the latter not given explicitly." );

    }
    const std::string trackSelExpr = cfg["applyToTracks"] ? cfg["applyToTracks"].as<std::string>() : ""
                    , detSelExpr = cfg["applyToDetectors"] ? cfg["applyToDetectors"].as<std::string>() : dftDetSel
                    ;

    // handlers::GetterBasedROOTHistHandler<HitT, 1, TH1F>;
    return new handlers::GetterBasedROOTHistHandler<event::ScoreFitInfo, 1, TH1F, DetID>( ch
                            , trackSelExpr, detSelExpr
                            , axisParams
                            , weightGetter
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , L
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

// This is the special one: uses plane ID as histogram key, omitting wire
// number from the hit ID (in fact, more commonly used than per-wire one)
template<typename HitT> static AbstractHandler *
_construct_handler_per_plane( calib::Manager & ch, const YAML::Node & cfg ) {

    std::string value = cfg["value"].as<std::string>();
    size_t firstDelim = value.find('.');
    assert( firstDelim != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<HitT> axisParams[1] = {
            { { cfg["nBins"].as<size_t>()
              , {cfg["range"][0].as<double>(), cfg["range"][1].as<double>()}
              , cfg["factor"] ? cfg["factor"].as<float>() : 1.
              }
            , util::value_getter<HitT>(value.substr(firstDelim + 1))
            }
        };

    return new handlers::GetterBasedROOTHistHandler<HitT, 1, TH1F, PlaneKey>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<HitT>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

// This is another special specialized by track zone ID
static AbstractHandler *
_construct_handler_per_zone( calib::Manager & ch, const YAML::Node & cfg ) {

    std::string value = cfg["value"].as<std::string>();
    size_t firstDelim = value.find('.');
    assert( firstDelim != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<event::Track> axisParams[1] = {
            { { cfg["nBins"].as<size_t>()
              , {cfg["range"][0].as<double>(), cfg["range"][1].as<double>()}
              , cfg["factor"] ? cfg["factor"].as<float>() : 1.
              }
            , util::value_getter<event::Track>(value.substr(firstDelim + 1))
            }
        };

    return new handlers::GetterBasedROOTHistHandler<event::Track, 1, TH1F, ZoneID>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<event::Track>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

REGISTER_HANDLER( Histogram1D
                , ch, cfg
                , "Plots certain value within an event's collection as 1D histogram" ) {
    if( !cfg["value"] )
        NA64DP_RUNTIME_ERROR( "No \"value\" argument provided within"
                " \"Histogram1D\" parameters section." );
    const std::string valueStr = cfg["value"].as<std::string>();
    size_t subjPos = valueStr.find('.');
    std::string subject = std::string::npos == subjPos 
                        ? "event"
                        : valueStr.substr(0, subjPos)
                        ;
    #if 1
    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return _construct_handler<event::type>( ch, cfg );              \
    }
    if( subject == "event" ) {
        return _construct_handler<event::Event>( ch, cfg );
    } else if( "apvHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<APVHit>(ch, cfg);
    } else if( "f1HitsPerPlane" == subject ) {
        return _construct_handler_per_plane<F1Hit>(ch, cfg);
    } else if( "stwtdcHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    } else if( "tracksPerZone" == subject ) {
        return _construct_handler_per_zone(ch, cfg);
    } else if( "fittedScores" == subject ) {
        return _construct_handler<event::ScoreFitInfo>(ch, cfg);
    }
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
    #else
    if( "event" == subject ) {
        return _construct_handler<Event>(ch, cfg);
    } else if( "SADCHit" == subject ) {
        return _construct_handler<SADCHit>(ch, cfg);
    } else if( "APVHit" == subject ) {
        return _construct_handler_per_plane<APVHit>(ch, cfg);
    } else if( "APVHit-per-wire" == subject ) {
        return _construct_handler<APVHit>(ch, cfg);
    } else if( "F1Hit" == subject ) {
        return _construct_handler_per_plane<F1Hit>(ch, cfg);
    } else if( "F1Hit-per-wire" == subject ) {
        return _construct_handler<F1Hit>(ch, cfg);
    } else if( "StwTDCHit" == subject ) {
        return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    } else if( "StwTDCHit-per-wire" == subject ) {
        return _construct_handler<StwTDCHit>(ch, cfg);
    } else if( "APVCluster" == subject ) {
        return _construct_handler<APVCluster>(ch, cfg);
    } else if( "CaloHit" == subject ) {
        return _construct_handler<CaloHit>(ch, cfg);
    } else if( "TrackPoint" == subject ) {
        return _construct_handler<TrackPoint>(ch, cfg);
    } else if( "Track" == subject ) {
        return _construct_handler<Track>(ch, cfg);
    }
    // ... (other standard hits)
    else {
        NA64DP_RUNTIME_ERROR("Unknown subject provided to generic handler"
                " construction: \"%s\"", subject.c_str());
    }
    #endif
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp
