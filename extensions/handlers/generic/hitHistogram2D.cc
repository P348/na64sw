#include "getterBasedROOTHistHandler.hh"
#include <TProfile.h>

namespace na64dp {
namespace handlers {

using namespace event;

/**\brief A 2D histogram plotting handler
 *
 * A generic handler used to plot a pair (optionally, weighted) variable
 * distribution, joint within certain hit type.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: Histogram2D
 *       # A base _type for the histogram; str, required
 *       histName: ...
 *       # A description string template; required, str
 *       histDescr: ...
 *       # To plot weighted histograms, this parameter may refer to a value
 *       # (getter) to be considered as a weighting function; float, optional
 *       weight: null
 *       # A path template within a TFile for the histogram object. If not
 *       # specified (or empty), a default one for the collection type will be
 *       # used; optional, str
 *       path: ""
 *       # When enabled a TProfile is used instead of TH2F. Note, that second
 *       # (y) axis parameters will be ignored, but still required, if this
 *       # flag is set; optional, bool
 *       profile: flase
 *
 *       # Specifies a getter to use to retrieve an abscissa value (horizontal
 *       # axis) from subject
 *       # type; str, required
 *       valueX: ...
 *       # Number of bins in the histogram (horizontal axis); int, required
 *       nBinsX: ...
 *       # A range to use by horizontal axis; list of two floats, required
 *       rangeX: [..., ...]
 *       # A multiplication factor apply for horizontal axis. Useful to
 *       # re-scale histogram in different units (for instance, MeV to
 *       # GeV); float, optional
 *       factorX: 1.0
 *
 *       # Specifies a getter to use to retrieve an ordinate value (vertical
 *       # axis) from subject
 *       # type; str, required
 *       valueY: ...
 *       # Number of bins in the histogram (vertical axis); int, required
 *       nBinsY: ...
 *       # A range to use by vertical axis; list of two floats, required
 *       rangeY: [..., ...]
 *       # A multiplication factor apply for vertical axis; float, optional
 *       factorY: 1.0
 * \endcode
 *
 * Possible `subject`s (see links for lists of available getters for `value`):
 * * `SADCHit` -- na64dp::event::SADCHit
 * * `APVHit` -- na64dp::event::APVHit
 * * `APVHit-per-wire` -- na64dp::event::APVHit, fine granularity
 * * `F1Hit` -- na64dp::event::F1Hit
 * * `F1Hit-per-wire` -- na64dp::event::F1Hit, fine granularity
 * * `StwTDCHit` -- na64dp::event::StwTDCHit
 * * `StwTDCHit-per-wire` -- na64dp::event::StwTDCHit, fine granularity
 * * `APVCluster` -- na64dp::event::APVCluster
 * * `CaloHit` -- na64dp::event::CaloHit
 * * `TrackPoint` -- na64dp::event::TrackPoint
 *
 * \note `Track` (`na64dp::event::Track`) is not supported yet
 * \note Defined as a template alias to generic template `GetterBasedROOTHistHandler`
 *
 * \ingroup generic-handlers
 */
template<typename HitT>
using Histogram2D = handlers::GetterBasedROOTHistHandler<HitT, 2, TH1F>;

template<typename HitT>
using Profile = handlers::GetterBasedROOTHistHandler<HitT, 2, TProfile>;

template<typename HitT> static AbstractHandler *
_construct_handler( calib::Manager & ch, const YAML::Node & cfg ) {
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<HitT> axisParams[2] = {
            { { cfg["nBinsX"].as<size_t>()
              , {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
              , cfg["factorX"] ? cfg["factorX"].as<float>() : 1.
              }
            , util::value_getter<HitT>(valueX.substr(firstDelimX + 1))
            },
            { { cfg["nBinsY"].as<size_t>()
              , {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
              , cfg["factorY"] ? cfg["factorY"].as<float>() : 1.
              }
            , util::value_getter<HitT>(valueY.substr(firstDelimY + 1))
            }
        };
    return new handlers::GetterBasedROOTHistHandler<HitT, 2, TH2F>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<HitT>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

// This is the special one: uses plane ID as histogram key, omitting wire
// number from the hit ID (in fact, more commonly used than per-wire one)
template<typename HitT> static AbstractHandler *
_construct_handler_per_plane( calib::Manager & ch, const YAML::Node & cfg ) {
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<HitT> axisParams[2] = {
            { { cfg["nBinsX"].as<size_t>()
              , {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
              , cfg["factorX"] ? cfg["factorX"].as<float>() : 1.
              }
            , util::value_getter<HitT>(valueX.substr(firstDelimX + 1))
            },
            { { cfg["nBinsY"].as<size_t>()
              , {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
              , cfg["factorY"] ? cfg["factorY"].as<float>() : 1.
              }
            , util::value_getter<HitT>(valueY.substr(firstDelimY + 1))
            }
        };
    return new handlers::GetterBasedROOTHistHandler<HitT, 2, TH2F, PlaneKey>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<HitT>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

static AbstractHandler *
_construct_handler_per_zone( calib::Manager & ch, const YAML::Node & cfg ) {
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<event::Track> axisParams[2] = {
            { { cfg["nBinsX"].as<size_t>()
              , {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
              , cfg["factorX"] ? cfg["factorX"].as<float>() : 1.
              }
            , util::value_getter<event::Track>(valueX.substr(firstDelimX + 1))
            },
            { { cfg["nBinsY"].as<size_t>()
              , {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
              , cfg["factorY"] ? cfg["factorY"].as<float>() : 1.
              }
            , util::value_getter<event::Track>(valueY.substr(firstDelimY + 1))
            }
        };
    return new handlers::GetterBasedROOTHistHandler<event::Track, 2, TH2F, ZoneID>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , axisParams
                            , cfg["weight"] ? util::value_getter<event::Track>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

// Event specialization
template<> AbstractHandler *
_construct_handler<event::Event>( calib::Manager & ch, const YAML::Node & cfg ) {
    using event::Event;
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller

    aux::HstAxisParams<Event> axisParams[2] = {
            { { cfg["nBinsX"].as<size_t>()
              , {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
              , cfg["factorX"] ? cfg["factorX"].as<float>() : 1.
              }
            , util::value_getter<Event>(valueX.substr(firstDelimX + 1))
            },
            { { cfg["nBinsY"].as<size_t>()
              , {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
              , cfg["factorY"] ? cfg["factorY"].as<float>() : 1.
              }
            , util::value_getter<Event>(valueY.substr(firstDelimY + 1))
            }
        };
    return new handlers::GetterBasedROOTHistHandler<Event, 2, TH2F, void>( ch
                            , axisParams
                            , cfg["weight"] ? util::value_getter<Event>(cfg["weight"].as<std::string>()) : nullptr
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            );
}

// Score fit info specialization
template<> AbstractHandler *
_construct_handler<event::ScoreFitInfo>( calib::Manager & ch, const YAML::Node & cfg ) {
    auto & L = ::na64dp::aux::get_logging_cat(cfg);
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller

    const std::string getterNameX = valueX.substr(firstDelimX + 1);
    const std::string getterNameY = valueY.substr(firstDelimY + 1);
    // try to resolve extended getter by name first
    ExtendedTrackScoreGetters::Getter getterX
        = ExtendedTrackScoreGetters::get_extended_getter(getterNameX);
    if(!getterX) {
        // no extended getter -- try to resolve ordinary (stateless) getter
        getterX.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(getterNameX);
        assert(getterX.cllb.plainGetter);
        // ^^^ NOTE: throws exception if failed; if passed, mark it as plain
        //     getter and proceed
        getterX.isPlain = true;
    } else {
        getterX.isPlain = false;
    }
    ExtendedTrackScoreGetters::Getter getterY
        = ExtendedTrackScoreGetters::get_extended_getter(getterNameY);
    if(!getterY) {
        getterY.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(getterNameY);
        assert(getterY.cllb.plainGetter);
        getterY.isPlain = true;
    } else {
        getterY.isPlain = false;
    }
    ExtendedTrackScoreGetters::Getter getterW;
    if(cfg["weight"]) {
        std::string getterNameW = cfg["weight"].as<std::string>();
        getterW = ExtendedTrackScoreGetters::get_extended_getter(getterNameW);
        if(!getterW) {
            getterW.cllb.plainGetter = util::value_getter<event::ScoreFitInfo>(getterNameW);
            assert(getterW.cllb.plainGetter);
            getterW.isPlain = true;
        } else {
            getterW.isPlain = false;
        }
    } else {
        // NOTE: since `Getter' unionstruct has no ctr, one has to init it
        //       externally.
        getterW.isPlain = false;
        bzero(&getterW.cllb, sizeof(getterW.cllb));
    }

    aux::HstAxisParams<event::ScoreFitInfo> axisParams[2] = {
            { { cfg["nBinsX"].as<size_t>()
              , {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
              , cfg["factorX"] ? cfg["factorX"].as<float>() : 1.
              }
            , getterX
            },
            { { cfg["nBinsY"].as<size_t>()
              , {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
              , cfg["factorY"] ? cfg["factorY"].as<float>() : 1.
              }
            , getterY
            }
        };

    const std::string dftDetSel = ::na64dp::aux::retrieve_det_selection(cfg);
    if(!dftDetSel.empty()) {
        L.warn("Default selector is discouraged for this subject"
                " type. Use `applyToTracks' to restrict consideration with"
                " certain tracks and `applyToDetectors' to select detectors within"
                " (filtered) tracks. Default selector is interpreted as"
                " `applyToDetectors' if the latter not given explicitly." );

    }
    const std::string trackSelExpr = cfg["applyToTracks"] ? cfg["applyToTracks"].as<std::string>() : ""
                    , detSelExpr = cfg["applyToDetectors"] ? cfg["applyToDetectors"].as<std::string>() : dftDetSel
                    ;

    if(cfg["profile"] && cfg["profile"].as<bool>()) {
        return new handlers::GetterBasedROOTHistHandler<event::ScoreFitInfo, 2, TProfile, DetID>( ch
                            , trackSelExpr, detSelExpr
                            , axisParams
                            , getterW
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , L
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
    } else {
        return new handlers::GetterBasedROOTHistHandler<event::ScoreFitInfo, 2, TH2F, DetID>( ch
                            , trackSelExpr, detSelExpr
                            , axisParams
                            , getterW
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , L
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
    }
}

REGISTER_HANDLER( Histogram2D
                , ch, cfg
                , "Plots a pair of certain values within an event's collection as 2D histogram" ) {

    if( !cfg["valueX"] )
        NA64DP_RUNTIME_ERROR( "No \"valueX\" argument provided within"
                " \"Histogram2D\" parameters section." );
    const std::string valueStr = cfg["valueX"].as<std::string>();
    size_t subjPos = valueStr.find('.');
    if( std::string::npos == subjPos ) {
        NA64DP_RUNTIME_ERROR( "No path delimeter found for \"valueX\" parameter"
                " in \"%s\".", valueStr.c_str() );
    }
    const std::string subject = valueStr.substr(0, subjPos);
    {  // consistency check
        if( !cfg["valueY"] )
            NA64DP_RUNTIME_ERROR( "No \"valueY\" argument provided within"
                    " \"Histogram2D\" parameters section." );
        const std::string valueStrY = cfg["valueY"].as<std::string>();
        size_t subjPosY = valueStrY.find('.');
        if( std::string::npos == subjPosY ) {
            NA64DP_RUNTIME_ERROR( "No path delimeter found for \"valueY\" parameter"
                    " in \"%s\".", valueStrY.c_str() );
        }
        if( valueStrY.substr(0, subjPosY) != subject ) {
            NA64DP_RUNTIME_ERROR( "Subject collection differs from abscissa and"
                    " ordinate axes of \"Histogram2D\" (\"%s\" and \"%s\")."
                    , valueStr.c_str(), valueStrY.c_str() );
        }
    }

    #if 1
    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return _construct_handler<event::type>( ch, cfg );              \
    }
    if( subject == "event" ) {
        return _construct_handler<event::Event>( ch, cfg );
    } else if( "apvHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<APVHit>(ch, cfg);
    } else if( "f1HitsPerPlane" == subject ) {
        return _construct_handler_per_plane<F1Hit>(ch, cfg);
    } else if( "stwtdcHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    } else if( "tracksPerZone" == subject ) {
        return _construct_handler_per_zone(ch, cfg);
    } else if( "fittedScores" == subject ) {
        return _construct_handler<event::ScoreFitInfo>(ch, cfg);
    }
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
    #else
    // TODO: support "event" subject type (default)
    //const std::string subject = cfg["subject"] ? cfg["subject"].as<std::string>()
    //                                           : "event"
    //                                           ;
    if( "SADCHit" == subject ) {
        return _construct_handler<SADCHit>(ch, cfg);
    } else if( "APVHit" == subject ) {
        return _construct_handler_per_plane<APVHit>(ch, cfg);
    } else if( "APVHit-per-wire" == subject ) {
        return _construct_handler<APVHit>(ch, cfg);
    } else if( "F1Hit" == subject ) {
        return _construct_handler_per_plane<F1Hit>(ch, cfg);
    } else if( "F1Hit-per-wire" == subject ) {
        return _construct_handler<F1Hit>(ch, cfg);
    } else if( "StwTDCHit" == subject ) {
        return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    } else if( "StwTDCHit-per-wire" == subject ) {
        return _construct_handler<StwTDCHit>(ch, cfg);
    } else if( "APVCluster" == subject ) {
        return _construct_handler<APVCluster>(ch, cfg);
    } else if( "CaloHit" == subject ) {
        return _construct_handler<CaloHit>(ch, cfg);
    } else if( "TrackPoint" == subject ) {
        return _construct_handler<TrackPoint>(ch, cfg);
    } else if( "Track" == subject ) {
        return _construct_handler<Track>(ch, cfg);
    }
    // ... (other standard hits)
    else {
        NA64DP_RUNTIME_ERROR("Unknown subject provided to generic handler"
                " construction: \"%s\"", subject.c_str());
    }
    #endif
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp
