#include "na64dp/abstractHandler.hh"
#include "na64dp/pipeline.hh"
#include "na64util/runtimeDirs.hh"

#include "sub.hh"

namespace na64dp {
namespace handlers {

AbstractHandler::ProcRes
Subpipe::process_event(event::Event & event) {
    std::pair<bool, bool> hpFlags = Pipeline::process(event, lmem());
    #if 1
    return forward_result(static_cast<AbstractHandler::ProcRes>(
                      (hpFlags.first  ? kStopProcessing    : 0x0)
                    | (hpFlags.second ? kDiscriminateEvent : 0x0)
                )
                , _flags);
    #else
    const bool hStop = hpFlags.first
             , hDisc = hpFlags.second;
    uint8_t pr = kOk;
    if(hDisc) {
        if(_flags & kForward) pr |= AbstractHandler::ProcRes::kDiscriminateEvent;
        else if( _flags & kStop ) pr |= AbstractHandler::ProcRes::kStopProcessing;
        #ifndef NDEBUG
        else assert( _flags == kIgnore );
        #endif
    }
    if(hStop) {
        if((_flags >> 3) & kForward) pr |= AbstractHandler::ProcRes::kStopProcessing;
        else if((_flags >> 3) & kDiscriminate ) pr |= AbstractHandler::ProcRes::kDiscriminateEvent;
        #ifndef NDEBUG
        else assert((_flags >> 3) == kIgnore );
        #endif
    }
    return static_cast<AbstractHandler::ProcRes>(pr);
    #endif
}

const unsigned int Subpipe::kIgnore       = 0x0
                 , Subpipe::kForward      = 0x1
                 , Subpipe::kDiscriminate = 0x2
                 , Subpipe::kStop         = 0x4;

unsigned int
Subpipe::build_forward_flags_from(const std::string & onStop
        , const std::string & onDiscriminate) {
    unsigned int flags = 0x0;
    if( "ignore" == onStop ) {
        flags |= handlers::Subpipe::kIgnore;
    } else if( "forward" == onStop ) {
        flags |= handlers::Subpipe::kForward;
    } else if( "discriminate" == onStop ) {
        flags |= handlers::Subpipe::kDiscriminate;
    } else {
        NA64DP_RUNTIME_ERROR( "Can't interpret \"onStop\" value \"%s\";"
                " expected one of: ignore, forward, discriminate."
                , onStop.c_str() );
    }
    flags <<= 3;
    if( "ignore" == onDiscriminate ) {
        flags |= handlers::Subpipe::kIgnore;
    } else if( "forward" == onDiscriminate ) {
        flags |= handlers::Subpipe::kForward;
    } else if( "stop" == onDiscriminate ) {
        flags |= handlers::Subpipe::kDiscriminate;
    } else {
        NA64DP_RUNTIME_ERROR( "Can't interpret \"onDiscriminate\" value \"%s\";"
                " expected one of: ignore, forward, stop."
                , onStop.c_str() );
    }
    return flags;
}

Subpipe *
Subpipe::from_YAML(const YAML::Node & cfg, calib::Manager & mgr) {
    const std::string onStop = cfg["onStop"]
                             ? cfg["onStop"].as<std::string>()
                             : "forward"
                    , onDiscriminate = cfg["onDiscriminate"]
                             ? cfg["onDiscriminate"].as<std::string>()
                             : "forward"
                             ;
    unsigned int flags = handlers::Subpipe::build_forward_flags_from(onStop, onDiscriminate);

    if( cfg["pipeline"].IsSequence() ) {  // first form used
        return new handlers::Subpipe( cfg["pipeline"]
                                    , mgr, flags );
    }
    try {
        // look for the file otherwise
        RunCfg rCfg( cfg["pipeline"].as<std::string>() + "{,.yaml,.yml}"
                   , getenv(NA64SW_RUN_CFG_PATH_ENVVAR)
                   );
        YAML::Node readNode = rCfg.get();
        if( !readNode["pipeline"] ) {
            NA64DP_RUNTIME_ERROR("No \"pipeline\" node at first level.")
        }
        return new handlers::Subpipe( readNode["pipeline"]
                                    , mgr, flags );
    } catch( std::exception & e ) {
        NA64DP_RUNTIME_ERROR("Error while constructing sub-pipeline"
                " from \"%s\": %s"
                , cfg["pipeline"].as<std::string>().c_str(), e.what() );
    }
}

AbstractHandler::ProcRes
Subpipe::forward_result(ProcRes pr_, unsigned int flags) {
    const bool hStop = pr_ & AbstractHandler::kStopProcessing
             , hDisc = pr_ & AbstractHandler::kDiscriminateEvent
             ;
    uint8_t pr = kOk;
    if(hDisc) {
        if(flags & kForward) pr |= AbstractHandler::ProcRes::kDiscriminateEvent;
        else if( flags & kStop ) pr |= AbstractHandler::ProcRes::kStopProcessing;
        #ifndef NDEBUG
        else assert( flags == kIgnore );
        #endif
    }
    if(hStop) {
        if((flags >> 3) & kForward) pr |= AbstractHandler::ProcRes::kStopProcessing;
        else if((flags >> 3) & kDiscriminate ) pr |= AbstractHandler::ProcRes::kDiscriminateEvent;
        #ifndef NDEBUG
        else assert((flags >> 3) == kIgnore );
        #endif
    }
    return static_cast<AbstractHandler::ProcRes>(pr);
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( Subpipe, mgr, cfg
                , "Embeds a pipeline" ) {
    return handlers::Subpipe::from_YAML(cfg, mgr);
}

}  // namespace na64dp


