#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64dp/pipeline.hh"
#include "na64util/runtimeDirs.hh"

namespace na64dp {
namespace handlers {

/**\brief A recurrent pipeline
 *
 * Provides a way to embed a pipeline within another pipeline.
 *
 * Usage note: embedded handlers list shall be provided in `pipeline` property
 * as either a list of objects (similar to usual `pipeline` list) or as a
 * filename or path fragment that will be searched in the items of what is
 * given under environment variable named `NA64SW_RUN_CFG_PATH_ENVVAR`
 * column-separated list of paths.
 *
 * \code{.yaml}
 *     - _type: Subpipe
 *       # How to handle "stop processing" flag raised by one of the handlers.
 *       # str, opt
 *       onStop: "forward"  # forward / ignore / discriminate
 *       # How to handle "discriminate event" flag raised by one of the
 *       # handlers; str, opt
 *       onDiscriminate: "forward"  # forward / ignore / stop
 *       # Usual pipeline object OR path string goes here
 *       pipeline:
 *          - _type ...
 *            ...
 * \endcode
 *
 * \ingroup handlers generic-handlers */
class Subpipe : public AbstractHandler
              , public Pipeline {
public:
    static const unsigned int kIgnore, kForward, kDiscriminate, kStop;
protected:
    unsigned int _flags;
    ProcRes _stopAsPR;
public:
    Subpipe( const YAML::Node & cfg
           , calib::Manager & mgr
           , unsigned int flags_=((kForward << 3) | kForward)
           ) : Pipeline(cfg, mgr)
             , _flags(flags_)
             {}
    ProcRes process_event(event::Event & event) override;

    static unsigned int build_forward_flags_from(const std::string & onStop
            , const std::string & onDiscriminate);

    /// Constructs heap-allocated sub-pipe from given YAML node
    static Subpipe * from_YAML(const YAML::Node & cfg
           , calib::Manager & mgr);

    static ProcRes forward_result(ProcRes pr, unsigned int flags);
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

