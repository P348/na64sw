#include "getterBasedROOTHistHandler2D.hh"

#include <TH2F.h>

namespace na64dp {
namespace handlers {

using namespace event;

// (internal) macro instantiating the concrete handler
template< typename HitT1, typename HitT2
        , typename HistType
        , typename HistKeyT=std::pair< typename event::Association<event::Event, HitT1>::Collection::key_type
                                     , typename event::Association<event::Event, HitT2>::Collection::key_type
                                     >
        > static AbstractHandler *
_construct_handler( calib::Manager & ch
                  , const YAML::Node & cfg
                  ) {
    auto getter1 = util::value_getter<HitT1>(cfg["valueX"].as<std::string>());
    auto getter2 = util::value_getter<HitT2>(cfg["valueY"].as<std::string>());
    size_t nBins1 = cfg["nBinsX"].as<size_t>()
         , nBins2 = cfg["nBinsY"].as<size_t>()
         ;
    double range1[2] = {cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>()}
         , range2[2] = {cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>()}
         ;
    const std::string sel1 = cfg["applyToX"] ? cfg["applyToX"].as<std::string>() : ""
                    , sel2 = cfg["applyToY"] ? cfg["applyToY"].as<std::string>() : ""
                    ;

    return new HeteroHist2D<HitT1, HitT2, HistType, HistKeyT, typename na64dp::util::PairHash>( ch
                , sel1, getter1
                , nBins1, range1[0], range1[1]
                , sel2, getter2
                , nBins2, range2[0], range2[1]
                , cfg["histName"].as<std::string>()
                , cfg["histDescr"].as<std::string>()
                , cfg["path"] ? cfg["path"].as<std::string>() : ""
                , aux::get_logging_cat(cfg)
                , aux::get_naming_class(cfg)
                );
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp


//
// Add handlers implementations here

REGISTER_HANDLER( BiHitHistogram_SADC_SADC  // XXX "((todo: hits x-macro))"
                , mgr, cfg
                , "Bi-hit TH2F histogram for SADC hit type."
                ) {
    return na64dp::handlers::_construct_handler< na64dp::event::SADCHit
                                               , na64dp::event::SADCHit
                                               , TH2F
                                               >( mgr, cfg );
}

REGISTER_HANDLER( BiHitHistogram_Calo_Calo  // XXX ((todo: hits x-macro))
                , mgr, cfg
                , "Bi-hit TH2F histogram for calorimeter hit type."
                ) {
    return na64dp::handlers::_construct_handler< na64dp::event::CaloHit
                                               , na64dp::event::CaloHit
                                               , TH2F
                                               >( mgr, cfg );
}

REGISTER_HANDLER( BiHitHistogram_TrackScore_TrackScore  // XXX ((todo: hits x-macro))
                , mgr, cfg
                , "Bi-hit TH2F histogram for track score hit type."
                ) {
    return na64dp::handlers::_construct_handler< na64dp::event::TrackScore
                                               , na64dp::event::TrackScore
                                               , TH2F
                                               >( mgr, cfg );
}

#if 0
REGISTER_HANDLER( BiHitHistogram
                , mgr, cfg
                , "Bi-hit TH2F histogram."
                ) {
    std::string vx = na64dp::aux::get_val_entry_type(cfg["valueX"].as<std::string>())
              , vy = na64dp::aux::get_val_entry_type(cfg["valueY"].as<std::string>())
              ;
    if( vx == "SADCHit" && vx == vy ) {
        return na64dp::handlers::_construct_handler< na64dp::event::SADCHit
                                                   , na64dp::event::SADCHit
                                                   , TH2F
                                                   >( mgr, cfg );
    }
    #if 1
    throw std::runtime_error("TODO: BiHitHistogram");
    #else
    return na64dp::handlers::_construct_handler< na64dp::event::CaloHit
                                               , na64dp::event::CaloHit
                                               , TH2F
                                               >( mgr, cfg );
    #endif
}
#endif

