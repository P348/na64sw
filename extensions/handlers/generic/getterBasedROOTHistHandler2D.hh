#pragma once

#include "plotsSet.hh"
#include "na64util/pair-hash.hh"

#include <cmath>

namespace na64dp {
namespace handlers {

/**\brief Generic getter-based handler template for two distinct hit types
 *
 * Plots 2D ROOT histogram of values taken from distinct hit types.
 *
 * \todo Treat returned result; foresee need for 1-to-1 correspondance, etc
 * */
template< typename HitType1, typename HitType2
        , typename HistTypeT
        , typename HistKeyT=std::pair< typename event::Association<event::Event, HitType1>::Collection::key_type
                                     , typename event::Association<event::Event, HitType2>::Collection::key_type
                                     >
        , typename HistKeyHash=typename na64dp::util::PairHash
        >
class HeteroHist2D : public AbstractHitHandler<HitType1> {
protected:
    /// Internal handler subclass, acually fills histogram
    struct SubHandler : public AbstractHitHandler<HitType2>
                      , public PlotsPerID< HistTypeT
                                         , HistKeyT
                                         , HistKeyHash
                                         > {
        /// Current key of first hit
        typename event::Association<event::Event, HitType1>::Collection::key_type key1;
        /// Current value got from hit of first type
        double value1;
        /// Getter for type2
        typename event::Traits<HitType2>::Getter getter2;
        /// Overriden path for the histogram
        const std::string overridenPath;

        /// Histogram parameters: bin X/Y
        size_t nBins[2];
        /// Histogram parameters: ranges, X/Y
        double ranges[2][2];

        SubHandler( calib::Dispatcher & dsp
                  , const std::string & selection2
                  , typename event::Traits<HitType2>::Getter getter2_
                  , size_t nBins1, double min1, double max1
                  , size_t nBins2, double min2, double max2
                  , const std::string & hstName
                  , const std::string & descr
                  , const std::string & basePath
                  , log4cpp::Category & logCat
                  , const std::string & nmSubcls="default"
                  ) : PlotsPerID< HistTypeT
                                , HistKeyT
                                , HistKeyHash
                                >::TDirAdapterType(dsp)
                    , AbstractHitHandler<HitType2>(dsp, selection2, logCat, nmSubcls)
                    , PlotsPerID< HistTypeT
                                , HistKeyT
                                , HistKeyHash
                                >(dsp, hstName, descr)
                    , value1(std::nan("0"))
                    , getter2(getter2_)
                    , overridenPath(basePath)
                    , nBins{nBins1, nBins2}
                    , ranges{{min1, max1}, {min2, max2}}
                    {}

        bool process_hit( EventID eventID
                        , typename AbstractHitHandler<HitType2>::HitKey key2
                        , HitType2 & hit2 ) override {
            this->get_or_create( HistKeyT(key1, key2)
                               , nBins[0], ranges[0][0], ranges[0][1]
                               , nBins[1], ranges[1][0], ranges[1][1]
                               )->Fill(value1, getter2(hit2));
            return true;
        }
    } _subHandler;
    /// Getter for type1
    typename event::Traits<HitType1>::Getter _getter1;
public:
    HeteroHist2D( calib::Dispatcher & cdsp
                , const std::string & selection1
                , typename event::Traits<HitType1>::Getter getter1
                , size_t nBins1, double min1, double max1
                , const std::string & selection2
                , typename event::Traits<HitType2>::Getter getter2
                , size_t nBins2, double min2, double max2
                , const std::string & hstName
                , const std::string & description
                , const std::string & basePath
                , log4cpp::Category & logCat
                , const std::string & namingClass="default"
                ) : AbstractHitHandler<HitType1>(cdsp, selection1, logCat, namingClass)
                  , _subHandler( cdsp, selection2, getter2
                               , nBins1, min1, max1
                               , nBins2, min2, max2
                               , hstName
                               , description
                               , basePath
                               , logCat
                               , namingClass
                               )
                  , _getter1(getter1)
                  {}

    ///\brief Picks up hit of first type and forwards event to sub-handler
    bool process_hit( EventID eventID
                    , typename AbstractHitHandler<HitType1>::HitKey key1
                    , HitType1 & hit1 ) override {
        _subHandler.value1 = _getter1(hit1);
        _subHandler.key1 = key1;
        _subHandler.process_event(this->_current_event());
        // ^^^ TODO: consider return result here
        return true;
    }

    virtual void finalize() override {
        //if( _subHandler.empty() ) {
        //    this->log().warn("No objects were created for \"%s\" (\"%s\")."
        //            , _hstBaseName.c_str()
        //            , _hstDescription.c_str() );
        //}
        _subHandler.PlotsPerID< HistTypeT
                              , HistKeyT
                              , HistKeyHash
                              >::finalize();
    }
};  // class HeteroHist2D


}  // namespace ::na64dp::handlers
}  // namespace na64dp

