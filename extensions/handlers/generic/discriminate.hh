#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/functions.hh"

#if defined(GTEST_FOUND) && GTEST_FOUND
#   include <gtest/gtest_prod.h>
#endif

#include <regex>

namespace na64dp {
#if defined(GTEST_FOUND) && GTEST_FOUND
namespace test {
class Discriminate_binaryComparisonObjectConstructedAndApplied_Test;
class Discriminate_ternaryComparisonObjectConstructedAndApplied_Test;
};
#endif
namespace util {

///\brief Regular expression to match binary comparison expression with getters
///
/// Groups (a cmp b):
///     1 - a as getter
///     2 - a as number
///     3 - cmp func
///     4 - b as getter
///     5 - b as number
extern const std::regex gRxBinOp;

///\brief Regular expression to match ternary (range) comparison expression with getters
///
/// Groups (a cmp1 b cmp2 c):
///     1 - a as getter
///     2 - a as number
///     3 - cmp1 func
///     4 - b as getter
///     5 - b as number
///     6 - cmp2 func
///     7 - c as getter
///     8 - c as number
extern const std::regex gRxTernaryOp;

/// Returns set of subject names used in the expression
std::set<std::string> get_expression_subjects(const std::string &);

/**\brief Simple comparator structure for single hit entity, constructible
 *        from string expression matchin `gRxBinOp` `gRxTernaryOp`
 *
 * Template class representing comparison binary or ternary comparison
 * expression to compare values of certain subject.
 *
 * \todo Will be superseded by JIT-compiled expressions.
 * */
template<typename HitT>
class Comparison {
protected:
    /// Binary flags defining how to interpret unions: 0x1 -- ternary
    /// expression, 0x2 -- first is a getter, 0x4 -- second is a getter, 0x8 --
    /// third is a getter.
    uint8_t _flags;
    /// Internal struct to represent operand, meaning steered by bits in `_flags`
    union Operand {
        typename event::Traits<HitT>::Getter getter;
        double constval;
    } _a  ///< Leftmost argument for binary and ternary comparison
    , _b  ///< Right argument for binary, middle arg for ternary
    , _c  ///< Rightmost argument, used only for ternary
    ;
    functions::Comparator _cmpf1  ///< comparison func for binary (1st for ternary)
                        , _cmpf2  ///< 2nd comparison func for ternary
                        ;
public:
    /// Tests given subject instance
    bool operator()(const HitT & hit) const {
        const auto val1 = _flags & 0x2 ? _a.getter(hit) : _a.constval
                 , val2 = _flags & 0x4 ? _b.getter(hit) : _b.constval
                 ;
        if(0x1 & _flags) {
            auto val3 = _flags & 0x8 ? _c.getter(hit) : _c.constval;
            return _cmpf1(val1, val2) && _cmpf2(val2, val3);
        } else {
            return _cmpf1( val1, val2 );
        }
    }

    /// Constructs a comparator from string expression
    Comparison(const std::string & expr) : _flags(0x0) {
        std::cmatch m;
        struct {
            Operand & o;
            int nGroup;
            uint8_t getterFlag;
        } operands[] = { { _a, 1, 0x2 }, { _b, 4, 0x4 }, {_c, 7, 0x8} };
        if( std::regex_match(expr.c_str(), m, gRxBinOp) ) {
            // binary comparison
            // (do nothing yet)
        } else if( std::regex_match(expr.c_str(), m, gRxTernaryOp) ) {
            // ternary (range) comparison
            _flags |= 0x1;
            _cmpf2 = functions::get_comparator( m[6].str().c_str() );
        } else {
            NA64DP_RUNTIME_ERROR( "Unable to parse comparison expression: \"%s\"."
                    , expr.c_str() );
        }
        _cmpf1 = functions::get_comparator( m[3].str().c_str() );
        for( int i = 0; i < 3; ++i ) {
            if(i == 2 && !(0x1 & _flags)) break;
            if( ! m[operands[i].nGroup].str().empty() ) {
                const std::string & tok = m[operands[i].nGroup].str();
                size_t n = tok.find('.');
                operands[i].o.getter = util::value_getter<HitT>( std::string::npos != n
                                                               ? tok.substr(n + 1)
                                                               : tok );
                assert(operands[i].o.getter);
                _flags |= operands[i].getterFlag;
                continue;
            }
            // otherwise, try to interpret literal
            const std::string & ltrl = m[operands[i].nGroup + 1].str();
            assert(!ltrl.empty());
            if( std::string::npos != ltrl.find('x') ) {  // hex int
                NA64DP_RUNTIME_ERROR( "TODO: Hex literal in comparison expr" );  // TODO
            } else if( std::string::npos != ltrl.find('o') ) {  // octal int
                NA64DP_RUNTIME_ERROR( "TODO: Octal literal in comparison expr" );  // TODO
            } else {  // double/float/dec int
                try {
                    operands[i].o.constval = std::stod(ltrl.data());
                } catch( std::invalid_argument & e ) {
                    NA64DP_RUNTIME_ERROR( "Can not interpret \"%s\" as"
                            " numerical literal", ltrl.data() );
                }
            }
        }
    }
    #if defined(GTEST_FOUND) && GTEST_FOUND
    FRIEND_TEST(test::Discriminate,  binaryComparisonObjectConstructedAndApplied);
    FRIEND_TEST(test::Discriminate, ternaryComparisonObjectConstructedAndApplied);
    #endif
};

}  // namespace ::na64dp::util

namespace handlers {

/**\brief Removes items from event and/or stops event propagation based on
 *        certain criterion.
 * 
 * Usage (generic form):
 * \code{.yaml}
 *     - _type: Discriminate
 *       # Specifies a getter to use to retrieve a value tested by
 *       # criterion, the comparison operation and the value with wich to
 *       # compare; str, required
 *       expression: ...
 *       # Inverts meaning of above expression; str, optional
 *       invert: false
 *       # Whether to remove hit from event on match (if getter relates to a
 *       # hit); bool, optional
 *       removeHit: false
 *       # Whether to stop iterating of entities of the subject type within
 *       # current event; bool, optional
 *       abruptHitProcessing: false
 *       # Whether to stop propagating this event by pipeline; bool, optional
 *       discriminateEvent: false
 *       # optional, hit selection expression
 *       applyTo: ...
 * \endcode
 *
 * Using this handler in generic form may be tedious because of the boolean
 * flags, and that's why some shortcuts are available (this is the same
 * handler, but with different defaults):
 *
 * Shortcut to remove hits (all hits iterated, event propagated further), not
 * available for `Event` subject (1st level getters cause error):
 * \code{.yaml}
 *     - _type: RemoveHits
 *       # Specifies a criterion and sets handler in the state when hits
 *       # that does not match to criterion will be removed. Mutually exclusive
 *       # with `ifDoesNotMatch`; str, required one of
 *       ifDoesNotMatch: ...
 *       # Specifies a criterion and sets handler in the state when only hits
 *       # that passes criterion will be left in an event. Mutually exclusive
 *       # with `ifMatches`; str, required one of
 *       ifDoesNotMatch: ...
 *       # optional, hit selection expression
 *       applyTo: ...
 * \endcode
 *
 * Examples for `expression`/`ifMatches`/... field:
 *  - binary comparison: `sadcHits.eDep > 1.5`, `23 == apvHits.rawData.rawData`, etc.
 *  - ternanry (ranged) comparison: `10 <= caloHit.eDep < 100`
 *
 * If condition in `expression` is satisfied, the effect defined in following
 * parameters will take place (in order of precedence):
 *      * if `removeHit` is set, hit will be deleted from *basic event's
 *        collection*;
 *      * if `abruptHitProcessing` no hit in this collection will be considered
 *        in the pipeline anymore (say, no more SADC hits checked in current
 *        event);
 *      * if `discriminateEvent` is set, event won't propagate further by
 *        the pipeline.
 *
 * Please, take note that removing an item (a hit) from event does not
 * necessarily mean it will be removed from collections that indirectly
 * refer it. For instance, if `APVHit` is part of `APVCluster` and it is
 * "removed" by this handler with `removeHit: true`, this hit will remain
 * in the cluster, but none of subsequent handlers operating at the
 * event level will see it.
 *
 * \todo This handler will be probably removed in favour of more generic one,
 *       once the JIT-compiled DSL for value selection will be implemented.
 *
 * \warning Note that `NaN` value results `false` for every comparison
 *          expression ("equals", "less than" etc), so if your configuration is
 *          meant to remove also `NaN` values, a formulation with `invert`
 *          may come in handy.
 *
 * \ingroup handlers generic-handlers
 * */
template<typename HitT>
class Discriminate : public AbstractHitHandler<HitT> {
private:
    /// Comparison to test against
    util::Comparison<HitT> _cmp;
    bool _removeHit  ///< whether to remove hit from event
       , _discriminateEvent  ///< whether to stop event propagation
       , _stopEventProcessing  ///< makes handler to stop owning pipeline
       , _abruptHitsProcessing  ///< whether to stop processing the hits
       , _invertExpressionMeaning  ///< whether to invert meaning of logical expression
       ;
public:
    /**\brief Constructs versatile hit discriminating handler
     *
     * \param dsp calib dispatcher to subscribe for hit selection
     * \param cmp comparator object to test entities
     * \param removeHit whether to remove a hit from event
     * \param discriminateEvent whether to stop event propagation over pipeline
     * \param stopEventsProcessing whether to stop processing event (i.e. exit the pipeline)
     * \param abruptHitProcessing whether to stop iterating over hits
     * \param only subset of detector entities to select entities only from
     * \param invertExpressionMeaning whether to invert meaning of logical expression
     * \param logCat reference to logging category
     * \param namingClass naming calib info class name
     * */
    Discriminate( calib::Dispatcher & dsp
                , const util::Comparison<HitT> & cmp
                , bool removeHit
                , bool discriminateEvent
                , bool stopEventsProcessing
                , bool abruptHitProcessing
                , const std::string & only
                , bool invertExpressionMeaning
                , log4cpp::Category & logCat
                , const std::string & namingClass
                ) : AbstractHitHandler<HitT>(dsp, only, logCat, namingClass)
                  , _cmp(cmp)
                  , _removeHit( removeHit )
                  , _discriminateEvent( discriminateEvent )
                  , _stopEventProcessing( stopEventsProcessing)
                  , _abruptHitsProcessing(abruptHitProcessing)
                  , _invertExpressionMeaning(invertExpressionMeaning)
                  {
        logCat << log4cpp::Priority::DEBUG
            << "Handler " << ((void*) this)
            << " discrimination parameters: "
            << (invertExpressionMeaning ? "" : "invert expression meaning, ")
            << (removeHit ? "" : "don't ")
            << "remove hit, " << (discriminateEvent ? "" : "don't")
            << " discriminate event, " << (stopEventsProcessing ? "stop" : "continue")
            << " event processing, " << (abruptHitProcessing ? "abrupt" : "process")
            << " current event; restrict selection with \""
            << (only.empty() ? "none" : only.c_str())
            << "\"."
            ;
    }
   
    /// Considers a hit of certain type within the event
    bool process_hit( EventID
                    , typename event::Association<event::Event, HitT>::Collection::key_type
                    , HitT & currentHit ) override {
        bool satisfies = _cmp(currentHit);
        if( _invertExpressionMeaning ) satisfies = ! satisfies;
        if( satisfies ) {
            this->_set_hit_erase_flag();
            int pr = 0x0;
            if( _discriminateEvent ) {
                // current event must not be propagated further by the pipeline
                pr |= AbstractHandler::kDiscriminateEvent;
            }
            if( _stopEventProcessing ) {
                pr |= AbstractHandler::kStopProcessing;
            }
            this->_set_event_processing_result((AbstractHandler::ProcRes) pr);
            if( _abruptHitsProcessing ) {
                // abrupt hit processing within the current event
                return false;
            }
        }
        return true;
    }
};

template<>
class Discriminate<event::Event> : public AbstractHandler {
private:
    /// Comparison expression
    util::Comparison<event::Event> _cmp;
    bool _discriminateEvent  ///< whether to stop event propagation
       , _stopEventProcessing  ///< whether to stop event iteration
       ;
public:
    Discriminate( const util::Comparison<event::Event> & cmp
                , bool discriminateEvent
                , bool stopEventsProcessing
                , log4cpp::Category & logCat
                ) : AbstractHandler(logCat)
                  , _cmp(cmp)
                  , _discriminateEvent( discriminateEvent )
                  , _stopEventProcessing( stopEventsProcessing ) {}

    ProcRes process_event(event::Event & e) override {
        bool satisfies = _cmp(e);
        if( satisfies ) {
            return (ProcRes) ( ((int) (_discriminateEvent ? kDiscriminateEvent : 0x0))
                             | ((int) (_stopEventProcessing ? kStopProcessing : 0x0 ))
                             );
        }
        return kOk;
    }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

