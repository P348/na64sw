#include "discriminate.hh"

namespace na64dp {
namespace util {

// link to online editor + UTs:
//      for binary .... : https://regex101.com/r/6gzUbd/2
//      for ternary ... : https://regex101.com/r/P7icIY/2
const std::regex
    gRxBinOp(R"~(^\s*(?:([A-Za-z][\w.]+)|([+-]?[\d.]+(?:[eE][+-]?\d+)?))\s*([!<>=]=?)\s*(?:([A-Za-z][\w.]+)|([+-]?[\d.]+(?:[eE][+-]?\d+)?))\s*$)~"),
    gRxTernaryOp(R"~(^\s*(?:([A-Za-z][\w.]+)|([+-]?[\d.]+(?:[eE][+-]?\d+)?))\s*([<>]=?)\s*(?:([A-Za-z][\w.]+)|([+-]?[\d.]+(?:[eE][+-]?\d+)?))\s*([<>]=?)\s*(?:([A-Za-z][\w.]+)|([+-]?[\d.]+(?:[eE][+-]?\d+)?))\w*$)~")
    ;

std::set<std::string>
get_expression_subjects(const std::string & expr) {
    std::set<std::string> r;
    std::cmatch m;
    std::vector<int> nGroups;
    if( std::regex_match(expr.c_str(), m, gRxTernaryOp) ) {
        nGroups = std::vector<int>({1, 4, 7});
    } else if( std::regex_match(expr.c_str(), m, gRxBinOp) ) {
        nGroups = std::vector<int>({1, 4});
    } else {
        return r;  // empty set expression doesn't match
    }
    for( const int nGroup : nGroups ) {
        const std::string tok = m[nGroup].str();
        if( tok.empty() ) continue;
        size_t n = tok.find('.');
        if(std::string::npos == n) {
            // no delimeter -- 1st level (event's) getter
            r.emplace("event");
        } else {
            r.emplace(tok.substr(0, n));
        }
    }
    return r;
}


}  // namespace ::na64dp::util

//                          * * *   * * *   * * *
template<typename HitT> AbstractHandler *
_construct_handler( calib::Dispatcher & calibDsp
                  , const YAML::Node & cfg
                  ) {
    auto & L = aux::get_logging_cat(cfg);
    const std::string strexpr = cfg["expression"].as<std::string>();
    L << log4cpp::Priority::DEBUG
      << "Constructing event sub-item discrimination handler for expression \""
      << strexpr << "\"";
    return new handlers::Discriminate<HitT>( calibDsp
           , util::Comparison<HitT>(strexpr)
           , cfg["removeHit"] ? cfg["removeHit"].as<bool>() : false
           , cfg["discriminateEvent"] ? cfg["discriminateEvent"].as<bool>() : false
           , cfg["stopEventsProcessing"] ? cfg["stopEventsProcessing"].as<bool>() : false
           , cfg["abruptHitProcessing"] ? cfg["abruptHitProcessing"].as<bool>() : false
           , aux::retrieve_det_selection(cfg)
           , cfg["invert"] ? cfg["invert"].as<bool>() : false
           , L
           , aux::get_naming_class(cfg)
           );
}

template<> AbstractHandler *
_construct_handler<event::Event>( calib::Dispatcher & calibDsp
                                , const YAML::Node & cfg
                                ) {
    if( cfg["removeHit"] ) {
        NA64DP_RUNTIME_ERROR("Possibly error: `removeHit' parameter is set,"
                " but the subject is `Event'.");
    }
    auto & L = aux::get_logging_cat(cfg);
    const std::string strexpr = cfg["expression"].as<std::string>();
    L << log4cpp::Priority::DEBUG
      << "Constructing event discrimination handler for expression \""
      << strexpr << "\"";
    return new handlers::Discriminate<event::Event>(
              util::Comparison<event::Event>(cfg["expression"].as<std::string>())
            , cfg["discriminateEvent"] ? cfg["discriminateEvent"].as<bool>() : false
            , cfg["stopEventsProcessing"] ? cfg["stopEventsProcessing"].as<bool>() : false
            , aux::get_logging_cat(cfg)
            );
}

REGISTER_HANDLER( Discriminate, ch, cfg
        , "Discriminates hits and/or events by certain criteria based on"
          " certain subject's value" ) {
    using namespace event;
    if( !cfg["expression"] )
        NA64DP_RUNTIME_ERROR( "No \"expression\" provided to"
                " \"Discriminate\" handler." );
    const std::string exprstr = cfg["expression"].as<std::string>();
    auto subjs = util::get_expression_subjects(exprstr);
    if(subjs.size() > 1) {
        NA64DP_RUNTIME_ERROR("Expression \"%s\" considers more than one"
                " objects, this is currently not supported."
                , exprstr.c_str() );
    }
    if(subjs.empty()) {
        NA64DP_RUNTIME_ERROR("Expression \"%s\" is invalid or trivial."
                , exprstr.c_str());
    }
    const std::string subject = *subjs.begin();
    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return _construct_handler<event::type>( ch, cfg );              \
    }
    if( subject == "event" ) {
        return _construct_handler<event::Event>( ch, cfg );
    }
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
}

//
// `RemoveHit' shortcut

template<typename HitT> AbstractHandler *
_construct_rm_hit_handler( const std::string & expr, bool invert
                         , calib::Dispatcher & calibDsp
                         , const YAML::Node & cfg
                         ) {
    return new handlers::Discriminate<HitT>( calibDsp
           , util::Comparison<HitT>(expr)
           , cfg["removeHit"] ? cfg["removeHit"].as<bool>() : true
           , cfg["discriminateEvent"] ? cfg["discriminateEvent"].as<bool>() : false
           , cfg["stopEventsProcessing"] ? cfg["stopEventsProcessing"].as<bool>() : false
           , cfg["abruptHitProcessing"] ? cfg["abruptHitProcessing"].as<bool>() : false
           , aux::retrieve_det_selection(cfg)
           , invert
           , aux::get_logging_cat(cfg)
           , aux::get_naming_class(cfg)
           );
}

REGISTER_HANDLER( RemoveHits, ch, cfg
        , "Removes hits from events by certain criteria based on"
          " certain value (shortcut for \"Discriminate\")." ) {
    using namespace event;

    const bool ifDoesNotMatch = cfg["ifDoesNotMatch"] ? true : false
             , ifMatches = cfg["ifMatches"] ? true : false;
    if(ifDoesNotMatch == ifMatches) {
        NA64DP_RUNTIME_ERROR("`RemoveHit' handler shall set exactly one of the"
                " parameters: \"ifDoesNotMatch\"/\"ifMatches\"." );
    }
    std::string exprstr;
    if( ifDoesNotMatch )
        exprstr = cfg["ifDoesNotMatch"].as<std::string>();
    else
        exprstr = cfg["ifMatches"].as<std::string>();

    auto subjs = util::get_expression_subjects(exprstr);
    if(subjs.size() > 1) {
        NA64DP_RUNTIME_ERROR("Expression \"%s\" considers more than one"
                " objects, this is currently not supported."
                , exprstr.c_str() );
    }
    if(subjs.empty()) {
        NA64DP_RUNTIME_ERROR("Expression \"%s\" is invalid or trivial."
                , exprstr.c_str());
    }
    const std::string subject = *subjs.begin();

    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return _construct_rm_hit_handler<event::type>( exprstr, ifDoesNotMatch, ch, cfg ); \
    }
    if( subject == "event" ) {
        NA64DP_RUNTIME_ERROR("`RemoveHit' handler relies on event's getter"
                " -- unable to deduce subject for expression \"%s\"."
                , exprstr.c_str() );
    }
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
}

}  // namespace ::na4dp::util

