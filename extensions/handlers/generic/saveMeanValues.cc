#include "na64calib/dispatcher.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64event/data/event.hh"
#include "na64util/numerical/online.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/// Value scorer interface (summator)
struct iScorer {
    /// Adds value to consideration
    virtual bool add(event::StdFloat_t) = 0;
    /// Should return collected mean value
    virtual double get() const = 0;
    /// Should return uncertainty of derived mean
    virtual double variance() const = 0;
    /// Should reset the scorer
    virtual void reset() = 0;
};

///\brief Direct scorer built on two double precision floating point numbers
class DirectScorer : public iScorer {
private:
    double _sum, _sqSum;
    size_t _accounted;
public:
    /// Adds value to the sum, unconditionally
    bool add(event::StdFloat_t v) override {
        _sum += v;
        _sqSum += v*v;
        return true;
    }
    /// Returns unbiased mean
    double get() const override {
        if(_accounted > 1)
            return _sum/_accounted;
        else
            return std::nan("0");
    }
    /// Returns unbiased MSE or nan
    double variance() const override {
        if(_accounted > 1) {
            double mean = get();
            return sqrt((_sqSum - _accounted*mean*mean)/(_accounted-1));
        } else
            return std::nan("0");
    }
    /// Re-sets the sums
    void reset() override {
        _sum = 0.;
        _sqSum = 0.;
    }
};

///\brief Uses Klein summation (generalized Kahan-Babuška) for sum and sum of squares
class KleinScorer : public iScorer {
private:
    numerical::KleinScorer _sum, _sqSum;
    size_t _accounted;
public:
    /// Adds value to the sum, unconditionally
    bool add(event::StdFloat_t v) override {
        _sum.add(v);
        _sqSum.add(v*v);
        return true;
    }
    /// Returns unbiased mean
    double get() const override {
        if(_accounted > 1)
            return _sum.result()/_accounted;
        else
            return std::nan("0");
    }
    /// Returns unbiased MSE or nan
    double variance() const override {
        if(_accounted > 1) {
            double mean = get()
                 , sqSum = _sqSum.result();
            return sqrt((sqSum - _accounted*mean*mean)/(_accounted-1));
        } else
            return std::nan("0");
    }
    /// Re-sets the sums
    void reset() override {
        _sum.reset();
        _sqSum.reset();
    }
};

/**\brief Retrieve and save mean stats for certain value
 *
 * Collects and saves files with per-spill statistics on certain value of
 * hits. Useful to calculate dirft/per-spill mean values for pedestals,
 * LED references, etc.
 *
 * Should be parameterised with getter. Each getter can be defined as
 * either an object or as a simple string referring to plain getter (a short
 * form). Getter definition in object form should include getter name (one of
 * hit type's standard getters), scorer type in use (one of "direct", "pairwise",
 * "kahan", etc) and name parameter (how to call its values in a file).
 * Optionally, scorer parameters can be appended to this object. Examples:
 *
 * Usage example:
 * \code{.yaml}
 *     - _type: SaveMeanValues
 *       # subject and getter string; str, required
 *       subject: ...
 *       # destination file; str, required
 *       output: ecal.txt
 *       # range for averaging, optional
 *       range: [-inf, +inf]
 *       # options controllign ouptut format, see doc; opt, list of strings
 *       options: []
 *       # detectors selector expr; opt, str
 *       applyTo: "kin == ECAL"
 *       # turns on averaging by spill or run; opt, str if given
 *       meanOn: spill
 *       # scorer type and parameters; opt, object
 *       scorer:
 *          _type: Kahan
 * \endcode
 *
 * Generated file can be of binary or textual (ASCII) form, providing tabular
 * content for further analysis or usage. The structure consists of ASCII
 * header providing string prefix "na64sw-means-v1/" + decimal representation
 * of unsigned integer containign bit flags. Bit flags composition defines
 * further content: presence of certain columns and format (binary or text).
 * Bit flags are defined by "options" argument.
 *
 * Order of columns is fixed:
 *  1. Item key -- detector ID (string or numeric), track ID, etc
 *  2. Event ID, if set
 *  3. mean, if set
 *  4. Variance, if set
 *  5, number of entries, if set
 *
 * Available options (as of version #1):
 *  
 *  - "text" -- switches the handler to ASCII output, disables bit 0x1
 *  - "noMean" -- turns off mean estimation output, disables bit 0x2
 *  - "noVariance"  -- turns off variance estimation output, disables bit 0x4
 *  - "eventID" -- turns on output of event ID used for averaging, enables bit 0x8
 *  - "noNEntries" -- turns off writing of number of used entries, disables bit 0x10
 *  - "numericKeys" -- turns off text keys for detector names, disables key 0x20
 *
 * So, default features of the file:
 *
 *  - of binary form
 *  - prefixed with text keys
 *  - has mean, variance, number of entries
 *
 * \todo foresee ROOT output
 * */
template<typename HitT>
class SaveMeanValues : public AbstractHitHandler<HitT> {
public:
    typedef typename event::Association<event::Event, HitT>::Collection::key_type KeyType;

    static constexpr uint32_t writeBinary       = 0x1
                            , writeMean         = 0x2
                            , writeVariance          = 0x4
                            , writeEventID      = 0x8
                            , writeNEntries     = 0x10
                            , writeKeyAsString  = 0x20
                            ;
protected:
    /// Write flags
    uint32_t _flags;
    /// Getter in use
    typename event::Traits<HitT>::Getter _getter;
    /// Collections of scorers by detector IDs
    std::unordered_map< KeyType, std::pair<size_t, iScorer *> > _scorersByDetID;
    /// Parameters for scorer in use
    const YAML::Node _scorerParameters;
    /// Permitted range
    double _range[2];
    /// Number of entries to perform averaging; combined with `_perSpill`
    /// flag to define when to save scorer values and reset the scorer. If set
    /// to 0, no limit is used
    size_t _entriesLimit;
    /// If set, values will be saved and scorer will be re-set once new spill
    /// arrives.
    bool _perSpill;

    /// Writes scorer value(s) and re-sets the scorer wrt flags currently set
    void _drop_scorer(KeyType, size_t, iScorer &);
    /// Creates new scorer with current parameters
    iScorer * _create_scorer();
private:
    /// Latest spill value, used to identify different spills in contiguous
    /// mode
    na64sw_spillNo_t _lastSpillNo;
    /// File to write into
    std::ofstream _ofs;
public:
    /// ... features of the ctr, if any
    SaveMeanValues( calib::Dispatcher & cdsp
                  , const std::string & selection
                  , const std::string & fileName
                  , uint32_t flags
                  , typename event::Traits<HitT>::Getter getter
                  , const YAML::Node & scorerCfg
                  , size_t entriesLimit
                  , bool perSpill
                  , log4cpp::Category & logCat
                  , double vMin=-std::numeric_limits<double>::infinity()
                  , double vMax=std::numeric_limits<double>::infinity()
                  , const std::string & namingSubclass="default"
                  )
        : AbstractHitHandler<HitT>(cdsp, selection, logCat, namingSubclass)
        , _flags(flags)
        , _getter(getter)
        , _scorerParameters(scorerCfg)
        , _range{vMin, vMax}
        , _entriesLimit(entriesLimit)
        , _perSpill(perSpill)
        , _lastSpillNo(0)
        , _ofs( fileName
              , std::ofstream::out //  | ( _flags & writeBinary ? std::ios::binary : 0x0)
              )
        {
        std::ostringstream oss;
        oss << "na64sw-means-v1/" << _flags << std::endl;
        std::string header = oss.str();
        _ofs.write(header.c_str(), header.size());
    }
    /// Forwards to parent and updates last spill no
    AbstractHandler::ProcRes process_event(event::Event &e) override;
    /// short summary on how the hit gets processed
    bool process_hit( EventID
                    , KeyType
                    , HitT & hit) override;
    /// writes scorer values
    void finalize() override;

    template<typename HitT_> friend const nameutils::DetectorNaming *
    get_naming_T(const SaveMeanValues<HitT_> * instance);
};  // class SaveMeanValues

// For detID returns ptr to detector naming object, otherwise returns nullptr
// stub
template<typename HitT> const nameutils::DetectorNaming *
get_naming_T(const SaveMeanValues<HitT> * instance) {
    return &instance->naming();
}

template<> const nameutils::DetectorNaming *
get_naming_T(const SaveMeanValues<event::Track> * instance) {
    return nullptr;
}

#if 0  // TODO
template<typename HitT> void
SaveMeanValues<HitT>::_drop_scorer( KeyType k
                                  , size_t nEntries
                                  , iScorer & scorer
                                  ) {
    na64dp::util::json::Parameters jsps{get_naming_T(this)};
    std::string detName;
    if(_flags & writeKeyAsString) {
        // TODO: use key traits to stringify the key
        throw std::runtime_error("TODO");
    }
    if(_flags & writeBinary) {
        if(writeKeyAsString) _ofs.write(detName.c_str(), detName.size() + 1);
        else _ofs.write(reinterpret_cast<const char *>(&k), sizeof(k));
        if(_flags & writeEventID) {
            na64sw_EventID_t eid = this->_current_event().id;
            _ofs.write(reinterpret_cast<const char *>(&eid), sizeof(eid));
        }
        double v;
        if(_flags & writeMean) {
            v = scorer.get();
            _ofs.write(reinterpret_cast<const char *>(&v), sizeof(v));
        }
        if(_flags & writeVariance) {
            v = scorer.variance();
            _ofs.write(reinterpret_cast<const char *>(&v), sizeof(v));
        }
        if(_flags & writeNEntries) {
            _ofs.write(reinterpret_cast<const char *>(&nEntries), sizeof(nEntries));
        }
    } else {
        if(writeKeyAsString) _ofs << detName;
        else _ofs << util::json::KeyTraits<KeyType>::to_js_key(k, jsps);
        _ofs << "\t";
        if(_flags & writeEventID)  _ofs << this->_current_event().id.to_str() << "\t";
        if(_flags & writeMean)     _ofs << scorer.get() << "\t";
        if(_flags & writeVariance)      _ofs << scorer.variance() << "\t";
        if(_flags & writeNEntries) _ofs << nEntries << "\t";
    }
    scorer.reset();
}
#endif

template<typename HitT> bool
SaveMeanValues<HitT>::process_hit( EventID eid
        , KeyType key
        , HitT & hit
        ) {
    event::StdFloat_t v = _getter(hit);
    if(v < _range[0] || v > _range[1]) return true;

    auto it = _scorersByDetID.find(key);
    if( _scorersByDetID.end() == it ) {
        it = _scorersByDetID.emplace(key,
                std::pair<size_t, iScorer *>(0, _create_scorer())).first;
    }
    
    if( !it->second.second->add(v) ) return true;

    ++(it->second.first);
    // drop scorer value if:
    //  _perSpill is set and spill number has changed
    bool doDropScorer = (_perSpill && (_lastSpillNo != eid.spill_no()));
    //  ...or _entriesLimit is not zero and _entriesLimit is reached
    // Note that if we got here than entries count on this corer is
    // guaranteed to be non-zero
    doDropScorer |= _entriesLimit && _entriesLimit >= it->second.first;
    if(!doDropScorer) return true;
    // write entry and reset the scorer
    //_ofs.write(&key,);

    return true;
}

template<typename HitT> AbstractHandler::ProcRes
SaveMeanValues<HitT>::process_event(event::Event & e) {
    AbstractHitHandler<HitT>::process_event(e);
    _lastSpillNo = e.id.spill_no();
    return AbstractHandler::kOk;
}

template<typename HitT> void
SaveMeanValues<HitT>::finalize() {
    for(const auto & p : _scorersByDetID) {
        if(!p.second.first) continue;
        _drop_scorer(p.first, p.second.first, *p.second.second);
    }
}


template<typename HitT> SaveMeanValues<HitT> *
instantiate_save_mean_values_scorer(
          calib::Dispatcher & cdsp
        , const YAML::Node & cfg
        ) {
    double range[2] = { -std::numeric_limits<double>::infinity()
                      ,  std::numeric_limits<double>::infinity()
                      };
    uint32_t flags
        = SaveMeanValues<HitT>::writeBinary
        | SaveMeanValues<HitT>::writeMean
        | SaveMeanValues<HitT>::writeVariance
        //| SaveMeanValues<HitT>::writeEventID
        | SaveMeanValues<HitT>::writeNEntries
        | SaveMeanValues<HitT>::writeKeyAsString
        ;
    std::vector<std::string> strOpts;
    if( cfg["options"] ) {
        strOpts = cfg["options"].as<std::vector<std::string>>();
    }
    for(const auto & strOpt : strOpts) {
        if(strOpt == "text") {
            flags &= ~SaveMeanValues<HitT>::writeBinary;
        } else if(strOpt == "noMean") {
            flags &= ~SaveMeanValues<HitT>::writeMean;
        } else if(strOpt == "noMSE") {
            flags &= ~SaveMeanValues<HitT>::writeVariance;
        } else if(strOpt == "eventID") {
            flags |= SaveMeanValues<HitT>::writeEventID;
        } else if(strOpt == "noNEntries") {
            flags &= ~SaveMeanValues<HitT>::writeNEntries;
        } else if(strOpt == "numericKeys") {
            flags &= ~SaveMeanValues<HitT>::writeKeyAsString;
        } else {
            NA64DP_RUNTIME_ERROR("Unknown option \"%s\"", strOpt.c_str());
        }
    }
    return new SaveMeanValues<HitT>( cdsp
            , aux::retrieve_det_selection(cfg)
            , cfg["output"].as<std::string>()
            , flags
            , util::value_getter<HitT>(cfg["value"].as<std::string>())
            , cfg["scorer"]
            , cfg["nEntries"] ? cfg["nEntries"].as<size_t>() : 0
            , cfg["perSpill"] ? cfg["perSpill"].as<bool>() : false
            , aux::get_logging_cat(cfg)
            , range[0], range[1]
            , aux::get_naming_class(cfg)
            );
}

template<> SaveMeanValues<TrackID> *
instantiate_save_mean_values_scorer<TrackID>(
          calib::Dispatcher & cdsp
        , const YAML::Node & cfg
        ) {
    throw std::runtime_error("TODO: scorer");
    // ...
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( SaveMeanValues, cdsp, cfg
                , "Retrieve and save per-spill statistics"
                ) {
    if( !cfg["value"] )
        NA64DP_RUNTIME_ERROR( "No \"value\" argument provided within"
                " \"Histogram1D\" parameters section." );
    const std::string valueStr = cfg["value"].as<std::string>();
    size_t subjPos = valueStr.find('.');
    std::string subject = std::string::npos == subjPos 
                        ? "event"
                        : valueStr.substr(0, subjPos)
                        ;

    using ::na64dp::handlers::instantiate_save_mean_values_scorer;
    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return instantiate_save_mean_values_scorer<na64dp::event::type>( cdsp, cfg );  \
    }
    if( subject == "event" ) {
        throw std::runtime_error("TODO: event subject");
    }
    #if 0  // TODO
    if( subject == "event" ) {
        return _construct_handler<event::Event>( ch, cfg );
    } else if( "apvHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<APVHit>(ch, cfg);
    } else if( "f1HitsPerPlane" == subject ) {
        return _construct_handler_per_plane<F1Hit>(ch, cfg);
    } else if( "stwtdcHitsPerPlane" == subject ) {
        return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    } else if( "tracksPerZone" == subject ) {
        return _construct_handler_per_zone(ch, cfg);
    } else if( "fittedScores" == subject ) {
        return _construct_handler<event::ScoreFitInfo>(ch, cfg);
    }
    #endif
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
    //return na64dp::handlers::SaveMeanValues<>
}

