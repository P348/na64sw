#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

namespace na64dp {
namespace handlers {

/**\brief A helper mixin class for handlers, keeping set of objects
 *
 * This template provides generic procedure for creation/retrieval of the
 * objects allocated on heap, organized as associative array indexed with
 * some key.
 *
 * Typical case is per-plane or per-station collection of uniform plots.
 *
 * \todo usage example
 * */
template< typename T
        , typename KeyT=DetID
        , typename HashT=std::hash<KeyT>
        , typename KeyEqual=std::equal_to<KeyT>
        >
class PlotsPerID : public virtual util::GenericTDirAdapter<KeyT, HashT, KeyEqual>
                 , public std::unordered_map< KeyT
                                            , T *
                                            , HashT
                                            , KeyEqual
                                            > {
public:
    typedef util::GenericTDirAdapter<KeyT, HashT, KeyEqual> TDirAdapterType;
    typedef std::unordered_map< KeyT
                              , T *
                              , HashT
                              , KeyEqual
                              > Container;
private:
    std::string _baseName
              , _descr
              ;
public:
    PlotsPerID( calib::Dispatcher & dsp
              , const std::string & nm
              , const std::string & descr )
            : TDirAdapterType(dsp)
            , _baseName(nm)
            , _descr(descr)
            {}

    /// (Creates if need and) returns object
    template<typename ... CtrArgsT>
    T * get_or_create(KeyT k, CtrArgsT && ... args ) {
        auto it = this->Container::find(k);
        if( Container::end() == it ) {
            std::string nm = _baseName
                      , descr = _descr
                      ;
            TDirAdapterType::dir_for(k, nm)->cd();
            auto ir = Container::emplace( k, new T( nm.c_str()
                                                  , descr.c_str()
                                                  , args ...) );
            assert(ir.second);
            it = ir.first;
        }
        return it->second;
    }

    virtual void finalize() {
        for( auto idHstPair : *this ) {
            this->tdirectories()[idHstPair.first]->cd();
            idHstPair.second->Write();
        }
    }

    /// Frees allocated objects
    virtual ~PlotsPerID() {
        for( auto & e : *this ) {
            delete e.second;
        }
    }
};  // class PlotsPerID

}  // namespace ::na64dp::handlers
}  // namespace na64dp

