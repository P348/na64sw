#include "extensions/std/getterBasedROOTHistHandler.hh"
#include "extensions/std/getterBasedROOTHistHandler2D.hh"

using namespace na64dp;
using na64dp::handlers::aux::HstAxisParams;

/**\brief Operates with track scores plotting value from a core vs value of
 *        the associated hit
 *
 * The main difference with respect to generic `Histogram2D` here is that this
 * templated class provides access to the object associated with a
 * score (usually, a hit).
 * */
template< typename HitT
        , typename KeyT=typename event::Association<event::Event, HitT>::Collection::key_type
        , typename HistT=TH2F
        >
class PlotScoreVsHit
    : public AbstractHitHandler<event::TrackScore>
    , public util::GenericTDirAdapter<KeyT>
    {
public:
    typedef util::GenericTDirAdapter<KeyT> ThisTDirAdapter;
    typedef handlers::THistTraits<HistT, HitT> HistTraits;

    PlotScoreVsHit( calib::Dispatcher & cdsp
                  , const std::string & sel
                  , const HstAxisParams<HitT> & hitHstP
                  , const HstAxisParams<event::TrackScore> & scoreHstP
                  , bool scoreByX
                  , const std::string & baseName
                  , const std::string & description
                  , const std::string & overridenPath
                  , log4cpp::Category & logCat
                  , bool absCoordinate=false  // XXX
                  , const std::string & namingClass="default"
                  )
            : AbstractHitHandler<event::TrackScore>(cdsp, sel, logCat, namingClass)
            , ThisTDirAdapter(cdsp, namingClass)
            , _hitHstParameters(hitHstP)
            , _scoreHstParameters(scoreHstP)
            , _scoreByX(scoreByX)
            , _hstBaseName(baseName)
            , _hstDescription(description)
            , _overridenPath(overridenPath)
            , _absCoordinates(absCoordinate)  // XXX
            {}

    /// 
    bool process_hit( EventID
                    , DetID scoreKey
                    , event::TrackScore & score ) override {
        // assure score keeps single hit of expected type
        if(score.hitRefs.size() != 1) {
            NA64DP_RUNTIME_ERROR("Score %s refers to %d != 1 scores."
                    , this->naming()[scoreKey].c_str()
                    , (int) score.hitRefs.size()
                    );
        }
        if(!std::holds_alternative< mem::Ref<HitT> >(score.hitRefs.begin()->second)) {
            NA64DP_RUNTIME_ERROR("Score %s refers to hit of different type."
                    , this->naming()[scoreKey].c_str()
                    );
        }
        const HitT & hit = *(std::get< mem::Ref<HitT> >(score.hitRefs.begin()->second));

        // use getters to obtain value
        bool hasValues = false;
        double values[2] = {
            _scoreHstParameters.getter(score),  // * ... .factor, todo?
            _hitHstParameters.getter(hit)  // * ... .factor, todo?
        };
        if(_absCoordinates) values[0] += score.lR[0];  // XXX
        for( size_t i = 0; i < 2; ++i ) {
            if(!std::isnan(values[i])) hasValues = true;
        }
        if(!hasValues) return true;  // no non-nan values
        if(!_scoreByX) std::swap(values[0], values[1]);

        KeyT key = KeyT(score.hitRefs.begin()->first);
        auto it = _histograms.find(key);
        if( _histograms.end() == it ) {
            handlers::aux::CommonHstAxisParams chp[] = {
                    { _scoreHstParameters.nBins
                    , { _scoreHstParameters.range[0], _scoreHstParameters.range[1] }
                    , _scoreHstParameters.factor
                    },
                    { _hitHstParameters.nBins
                    , { _hitHstParameters.range[0], _hitHstParameters.range[1] }
                    , _hitHstParameters.factor
                    }
                };
            if(!_scoreByX) std::swap(chp[0], chp[1]);
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            auto substCtx = ThisTDirAdapter::subst_dict_for(key, hstName);
            std::string description = util::str_subst( _hstDescription, substCtx );
            auto p = ThisTDirAdapter::dir_for( key, substCtx, _overridenPath);
            p.second->cd();
            it = _histograms.emplace( key
                    , HistTraits::create( p.first
                                        , description
                                        , chp )
                    ).first;
        }
        // fill bin content in the histogram
        HistTraits::fill(*(it->second), values, 1);  // , _getWeight ? _getWeight(cHit) : 1 );, todo?
        return true;
    }

    ~PlotScoreVsHit() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }

    /// Mandatory to force ROOT to save its histograms to a file. Otherwise it
    /// will just drop them even despite on that they were created in TDirs.
    void finalize() override {
        if( _histograms.empty() ) {
            this->log().warn("No objects were created for \"%s\" (\"%s\")."
                    , _hstBaseName.c_str()
                    , _hstDescription.c_str() );
        }
        for( auto idHstPair : _histograms ) {
            if(ThisTDirAdapter::tdirectories()[idHstPair.first]) {
                assert(ThisTDirAdapter::tdirectories()[idHstPair.first]);
                ThisTDirAdapter::tdirectories()[idHstPair.first]->cd();
            } else {
                gFile->cd();
            }
            if(idHstPair.second) {
                idHstPair.second->Write();
            }
        }
    }
protected:
    HstAxisParams<HitT> _hitHstParameters;
    HstAxisParams<event::TrackScore> _scoreHstParameters;
    std::unordered_map<KeyT, TH2F *> _histograms;
    const bool _scoreByX;
    const std::string _hstBaseName;
    const std::string _hstDescription;
    const std::string _overridenPath;
    bool _absCoordinates;  // XXX
};  // class PlotScoreVsHit

template<typename HitT>
using Histogram2D = handlers::GetterBasedROOTHistHandler<HitT, 2, TH1F>;

// 
template<typename HitT, typename KeyT> static AbstractHandler *
_construct_handler( calib::Manager & ch, const YAML::Node & cfg, bool scoreByX ) {
    std::string valueX = cfg["valueX"].as<std::string>();
    size_t firstDelimX = valueX.find('.');
    assert( firstDelimX != std::string::npos );  // guaranteed by caller
    const std::string strGetterX = valueX.substr(firstDelimX + 1);

    std::string valueY = cfg["valueY"].as<std::string>();
    size_t firstDelimY = valueY.find('.');
    assert( firstDelimY != std::string::npos );  // guaranteed by caller
    const std::string strGetterY = valueY.substr(firstDelimY + 1);

    const double ranges[2][2] = {
            { cfg["rangeX"][0].as<double>(), cfg["rangeX"][1].as<double>() },
            { cfg["rangeY"][0].as<double>(), cfg["rangeY"][1].as<double>() }
        };
    const size_t nBins[2] = {
            cfg["nBinsX"].as<size_t>(), cfg["nBinsY"].as<size_t>()
        };

    HstAxisParams<event::TrackScore> scoreParameters
        = { { nBins[scoreByX ? 0 : 1]
            , {ranges[scoreByX ? 0 : 1][0], ranges[scoreByX ? 0 : 1][1]}
            , 1.  // todo: factor?
            }
          , util::value_getter<event::TrackScore>( scoreByX ? strGetterX : strGetterY )
          };
    HstAxisParams<HitT> hitParameters
        = { { nBins[scoreByX ? 1 : 0]
            , {ranges[scoreByX ? 1 : 0][0], ranges[scoreByX ? 1 : 0][1]}
            , 1.  // todo: factor?
            }
          , util::value_getter<HitT>( scoreByX ? strGetterY : strGetterX )
          };

    return new PlotScoreVsHit<HitT, KeyT>( ch
                            , ::na64dp::aux::retrieve_det_selection(cfg)
                            , hitParameters
                            , scoreParameters
                            , scoreByX
                            , cfg["histName"].as<std::string>()
                            , cfg["histDescr"].as<std::string>()
                            , cfg["path"] ? cfg["path"].as<std::string>() : ""
                            , ::na64dp::aux::get_logging_cat(cfg)
                            , cfg["absCoordinates"] ? cfg["absCoordinates"].as<bool>() : false  // XXX
                            , ::na64dp::aux::get_naming_class(cfg)
                            );
}

REGISTER_HANDLER( PlotScoreVsHit
                , cmgr
                , cfg
                , "Value of track score vs value of associated hit"
                ) {
    if( !cfg["valueX"] )
        NA64DP_RUNTIME_ERROR( "No \"valueX\" argument provided." );
    const std::string valueStrX = cfg["valueX"].as<std::string>();
    size_t subjPosX = valueStrX.find('.');
    if( std::string::npos == subjPosX ) {
        NA64DP_RUNTIME_ERROR( "No path delimeter found for \"valueX\" parameter"
                " in \"%s\".", valueStrX.c_str() );
    }
    const std::string xSubject = valueStrX.substr(0, subjPosX);

    if( !cfg["valueY"] )
        NA64DP_RUNTIME_ERROR( "No \"valueY\" argument provided." );
    const std::string valueStrY = cfg["valueY"].as<std::string>();
    size_t subjPosY = valueStrY.find('.');
    if( std::string::npos == subjPosY ) {
        NA64DP_RUNTIME_ERROR( "No path delimeter found for \"valueY\" parameter"
                " in \"%s\".", valueStrY.c_str() );
    }
    const std::string ySubject = valueStrY.substr(0, subjPosY);

    bool scoreValueByX = false;
    // this handler expects that one of the subjects is TrackScore
    if( xSubject == "trackScores" ) {
        scoreValueByX = true;
    }
    if( ySubject == "trackScores" ) {
        if(scoreValueByX) {
            NA64DP_RUNTIME_ERROR("Exactly one of the axis value has to"
                " have \"TrackScore\" as a subject.");

        }
        scoreValueByX = false;
    } else if(!scoreValueByX) {
        NA64DP_RUNTIME_ERROR("One of the axis value has to have \"TrackScore\""
                " as a subject.");
    }
    const std::string & subjHit = scoreValueByX ? ySubject : xSubject;
    bool perHitID = cfg["perHitID"] ? cfg["perHitID"].as<bool>() : false;

    // TODO: if we'll have X-macro for oneOf kind of association, substitute
    // this clauses with it
    #define INST(tp, nm) if(subjHit == nm) {                                \
        if(!perHitID) return _construct_handler<event:: tp, PlaneKey>(cmgr, cfg, scoreValueByX); \
        else return _construct_handler<event:: tp, DetID>(cmgr, cfg, scoreValueByX);            \
    }
    INST(APVCluster, "apvClusters") else
    INST(StwTDCHit, "stwtdcHits") else
    INST(CaloHit, "caloHits") else
    INST(F1Hit, "f1Hits") else {
        NA64DP_RUNTIME_ERROR("Unknown/unsupported score's subject"
                " type \"%s\"", subjHit.c_str());
    }
    #undef INST
}

