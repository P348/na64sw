#include "discriminate.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace test {

static struct RxExpressionTestCase {
    const std::string expression;
    const std::string groups[10];
} gBinaryCases[] = {
    { "1 < 2", { "", "1"
               , "<"
               , "", "2"
               , "#"
               }
    }, {
      "ab != zx", { "ab", ""
                  , "!="
                  , "zx", ""
                  , "#"
                  }
    }, {
      "foo <= 5e10", { "foo", ""
                     , "<="
                     , "", "5e10"
                     , "#"
                     }
    }, {
      "-.5 == foo.bar", { "", "-.5"
                       , "=="
                       , "foo.bar", ""
                       , "#"
                       }
    }
}, gTernaryCases[] = {
    { " .1e2 <= +0.00 <= foo.bar", { "", ".1e2"
                               , "<="
                               , "", "+0.00"
                               , "<="
                               , "foo.bar", ""
                               , "#"
                               }
    }, {
        "100500 < xyz < -1.23e-15", { "", "100500"
                                     , "<"
                                     , "xyz", ""
                                     , "<"
                                     , "", "-1.23e-15"
                                     , "#"
                                     }
    }
};

TEST( DiscriminateHandler, binaryExpressionPattern ) {
    for( size_t i = 0; i < sizeof(gBinaryCases)/sizeof(*gBinaryCases); ++i ) {
        std::cmatch m;
        const auto & tc = gBinaryCases[i];
        ASSERT_TRUE(std::regex_match(tc.expression.c_str(), m, util::gRxBinOp))
            << " at expression \"" << tc.expression << "\"" << std::endl;
        for( size_t j = 0; j < sizeof(tc.groups)/sizeof(*tc.groups) && tc.groups[j] != "#"; ++j ) {
            EXPECT_STREQ(tc.groups[j].c_str(), m[j+1].str().c_str());
        }
    }
}

TEST( DiscriminateHandler, ternaryExpressionPattern ) {
    for( size_t i = 0; i < sizeof(gTernaryCases)/sizeof(*gTernaryCases); ++i ) {
        std::cmatch m;
        const auto & tc = gTernaryCases[i];
        ASSERT_TRUE(std::regex_match(tc.expression.c_str(), m, util::gRxTernaryOp))
            << " at expression \"" << tc.expression << "\"" << std::endl;
        for( size_t j = 0; j < sizeof(tc.groups)/sizeof(*tc.groups) && tc.groups[j] != "#"; ++j ) {
            EXPECT_STREQ(tc.groups[j].c_str(), m[j+1].str().c_str());
        }
    }
}

TEST(Discriminate,  binaryComparisonObjectConstructedAndApplied) {
    util::Comparison<event::F1Hit> c("f1Hits.hitPosition < 3");
    EXPECT_FALSE(c._flags & 0x1);  // binary expression
    EXPECT_TRUE(c._flags & 0x2);  // left arg is getter
    EXPECT_FALSE(c._flags & 0x4);  // right arg is const num
    EXPECT_NEAR(c._b.constval, 3, 1e-6);
    EXPECT_EQ(c._cmpf1, na64dp::functions::get_comparator("lt"));

    char buf[128];
    na64dp::mem::PlainBlock evMemBlock(buf, sizeof(buf));
    LocalMemory lmem(evMemBlock);
    event::F1Hit hit(lmem);
    util::reset(hit);
    hit.hitPosition = 1.23;

    EXPECT_TRUE(c(hit));

    hit.hitPosition = 3.01;
    EXPECT_FALSE(c(hit));
}

TEST(Discriminate, ternaryComparisonObjectConstructedAndApplied) {
    util::Comparison<event::F1Hit> c(" -1.e-1 <= f1Hits.hitPosition <= +1e+1");

    EXPECT_TRUE(c._flags & 0x1);  // ternary expression
    EXPECT_FALSE(c._flags & 0x2);  // left arg is constant num
    EXPECT_NEAR(c._a.constval, -0.1, 1e-6);
    
    EXPECT_TRUE(c._flags & 0x4);  // middle arg is getter

    EXPECT_FALSE(c._flags & 0x8);  // middle arg is constant num
    EXPECT_NEAR(c._c.constval, 10, 1e-6);

    EXPECT_EQ(c._cmpf1, na64dp::functions::get_comparator("le"));
    EXPECT_EQ(c._cmpf2, na64dp::functions::get_comparator("le"));

    char buf[128];
    na64dp::mem::PlainBlock evMemBlock(buf, sizeof(buf));
    LocalMemory lmem(evMemBlock);
    event::F1Hit hit(lmem);
    util::reset(hit);
    hit.hitPosition = -1;

    EXPECT_FALSE(c(hit));

    hit.hitPosition = 10. + 1e-6;
    EXPECT_FALSE(c(hit));
}

TEST(Discriminate, getTwoSubjectsTernary) {
    auto subjs = util::get_expression_subjects("foo.bar < one.two < foo.zum");
    EXPECT_EQ(subjs.size(), 2);
    EXPECT_NE(subjs.find("foo"), subjs.end());
    EXPECT_NE(subjs.find("one"), subjs.end());
}

TEST(Discriminate, getTwoSubjectsTernaryWithEventGetter) {
    auto subjs = util::get_expression_subjects("foo.bar < one < foo.zum");
    EXPECT_EQ(subjs.size(), 2);
    EXPECT_NE(subjs.find("foo"), subjs.end());
    EXPECT_NE(subjs.find("event"), subjs.end());
}

TEST(Discriminate, getSubjectBinary) {
    auto subjs = util::get_expression_subjects("1 != foo.bar");
    EXPECT_EQ(subjs.size(), 1);
    EXPECT_NE(subjs.find("foo"), subjs.end());
}

TEST(Discriminate, getSubjectBinaryWithEventGetter) {
    auto subjs = util::get_expression_subjects("one < 1.23");
    EXPECT_EQ(subjs.size(), 1);
    EXPECT_NE(subjs.find("event"), subjs.end());
}

TEST(Discriminate, getEmptySubjectBinary) {
    auto subjs = util::get_expression_subjects("1e-10 < 1.23");
    EXPECT_TRUE(subjs.empty());
}

TEST(Discriminate, getEmptySubjectTernary) {
    auto subjs = util::get_expression_subjects("1e-10 < 1.23 < 42");
    EXPECT_TRUE(subjs.empty());
}

}  // namespace ::na64dp::test
}  // namespace na64dp

