#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64dp/extendedTrackScoreGetters.hh"
#include "na64util/TDirAdapter.hh"

#include <TH1F.h>
#include <TH2F.h>
#include <TProfile.h>
#include <TH3F.h>

#include "na64event/data/event.hh"

/**\file
 * \brief Common base helper for ROOT histograms and plots of static arity
 *
 * ROOT object system relies on runtime polymorphism that requires some
 * tedious code to create histograms with only difference in arity.
 *
 * This handler base implements templated behaviour for multiple histograms and
 * plots provided by ROOT. Static polymorphism is defined by particular traits
 * definition above.
 * */

namespace na64dp {
namespace handlers {

namespace aux {
struct CommonHstAxisParams {
    size_t nBins;  /// number of bins by axis
    double range[2];  /// axis range
    double factor;  /// multiplication factor
};

/// Structure defining histogram axis parameters
template<typename HitT>
struct HstAxisParams : public CommonHstAxisParams {
    typename event::Traits<HitT>::Getter getter;  /// getter used for axis
};

template<>
struct HstAxisParams<event::ScoreFitInfo> : public CommonHstAxisParams {
    typename ExtendedTrackScoreGetters::Getter getter;
};
}  // namespace ::na64dp::handlers::aux

template<typename THistT, typename HitT> struct THistTraits;
// shall define following stuff:
//      THistT * create( const std::string & name
//                     , const std::string & description
//                     , const (&HstAxisParams)[1]
//                     )
//      static void fill( TH1F & obj
//                    , const double (&values)[1]
//                    , double weight
//                    );

/**\brief Defines a generic templated base handler calss for N-ary ROOT histos
 *
 * Plot type traits must be defined within a `THistTraits` struct. Class
 * implements `AbstractHitHandler::process_hit()` forwarding histogram creation
 * and filling to traits' methods. Getters are applied automatically (+ one
 * extra getter is foreseen for weight).
 *
 * \todo Will be generalized even more once we'll elaborate selectors DSL
 * */
template< typename HitT
        , size_t arityT
        , typename HistTypeT
        , typename HistKeyT=typename event::Association<event::Event, HitT>::Collection::key_type
        >
class GetterBasedROOTHistHandler : public AbstractHitHandler<HitT>
                                 , public util::GenericTDirAdapter<HistKeyT> {
public:
    /// Hit key type; one provided by association collection with `Event`
    typedef typename event::Association<event::Event, HitT>::Collection::key_type HitKey;
    /// Histogram indexing key; must be constructible from `HitKey`. May be the
    /// same as `HitKey` for identity (a single entity is a single histogram)
    typedef HistKeyT HistKey;
    /// Current histogram traits type
    typedef THistTraits<HistTypeT, HitT> HistTraits;
    /// Currently used `GenericTDirAdapter` type
    typedef util::GenericTDirAdapter<HistKeyT> ThisTDirAdapter;
protected:
    /// Histograms index
    std::unordered_map<HistKey, HistTypeT *> _histograms;
    /// Histogram parameters
    aux::HstAxisParams<HitT> _hstParams[arityT];
    /// Getter for weights; may be null
    typename event::Traits<HitT>::Getter _getWeight;
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Histogram path template. Leave empty to not override default one
    std::string _overridenPath;
public:
    GetterBasedROOTHistHandler( calib::Dispatcher & cdsp
                              , const std::string & only
                              , const aux::HstAxisParams<HitT> (&hstp)[arityT]
                              , typename event::Traits<HitT>::Getter weightGetter
                              , const std::string & baseName
                              , const std::string & description
                              , const std::string & overPath
                              , log4cpp::Category & logCat
                              , const std::string & namingClass
                              ) : AbstractHitHandler<HitT>(cdsp, only, logCat, namingClass)
                                , ThisTDirAdapter(cdsp, namingClass)
                                , _hstParams(hstp)
                                , _getWeight(weightGetter)
                                , _hstBaseName(baseName)
                                , _hstDescription(description)
                                , _overridenPath(overPath)
                                { }
    /// Deletes created objects
    ~GetterBasedROOTHistHandler() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }
    /// ...
    virtual bool process_hit( EventID
                            , HitKey hitKey
                            , HitT & cHit ) override {
        bool hasValues = false;
        double values[arityT];
        for( size_t i = 0; i < arityT; ++i ) {
            values[i] = _hstParams[i].getter(cHit)*_hstParams[i].factor;
            if(!std::isnan(values[i])) hasValues = true;
        }
        if(!hasValues) return true;  // no non-nan values

        HistKey key(hitKey);
        auto it = _histograms.find(key);
        if( _histograms.end() == it ) {
            aux::CommonHstAxisParams chp[arityT];
            for( size_t i = 0; i < arityT; ++i ) {
                chp[i].nBins = _hstParams[i].nBins;
                chp[i].range[0] = _hstParams[i].range[0];
                chp[i].range[1] = _hstParams[i].range[1];
                chp[i].factor = _hstParams[i].factor;
            }
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            auto substCtx = ThisTDirAdapter::subst_dict_for(key, hstName);
            std::string description = util::str_subst( _hstDescription, substCtx );
            auto p = ThisTDirAdapter::dir_for( key, substCtx, _overridenPath);
            p.second->cd();
            it = _histograms.emplace( key
                    , HistTraits::create( p.first
                                        , description
                                        , chp )
                    ).first;
        }
        // fill bin content in the histogram
        HistTraits::fill(*(it->second), values, _getWeight ? _getWeight(cHit) : 1 );
        return true;
    }
    /// Writes histogram into current TFile
    virtual void finalize() override {
        // well, traits method for seems to be redundant to just call
        // Write() yet more consistent; decided to call it directly so far
        // since runtime polymorphism is da way of ROOT stuff
        if( _histograms.empty() ) {
            this->log().warn("No objects were created for \"%s\" (\"%s\")."
                    , _hstBaseName.c_str()
                    , _hstDescription.c_str() );
        }
        for( auto idHstPair : _histograms ) {
            if(ThisTDirAdapter::tdirectories()[idHstPair.first]) {
                assert(ThisTDirAdapter::tdirectories()[idHstPair.first]);
                ThisTDirAdapter::tdirectories()[idHstPair.first]->cd();
            } else {
                gFile->cd();
            }
            if(idHstPair.second) {
                idHstPair.second->SetOption("colz");
                idHstPair.second->Write();
            }
        }
    }
};

/// Partial specialization for event type
///
/// Specialization introduced to uniformely consider event's entry getters and
/// event's (itslef) getters.
template< size_t arityT
        , typename HistTypeT
        >
class GetterBasedROOTHistHandler< event::Event
                                , arityT
                                , HistTypeT
                                , void
                                > : public AbstractHandler {
public:
    typedef THistTraits<HistTypeT, event::Event> HistTraits;
protected:
    /// Histograms index
    HistTypeT * _histogram;
    /// Histogram parameters
    aux::HstAxisParams<event::Event> _hstParams[arityT];
    /// Getter for weights; may be null
    typename event::Traits<event::Event>::Getter _getWeight;
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Histogram path template. Leave empty to not override default one
    std::string _overridenPath;
public:
    GetterBasedROOTHistHandler( calib::Dispatcher & cdsp
                              , const aux::HstAxisParams<event::Event> (&hstp)[arityT]
                              , typename event::Traits<event::Event>::Getter weightGetter
                              , const std::string & baseName
                              , const std::string & description
                              , const std::string & overPath
                              , log4cpp::Category & logCat
                              ) : AbstractHandler(logCat)
                                , _histogram(nullptr)
                                , _hstParams(hstp)
                                , _getWeight(weightGetter)
                                , _hstBaseName(baseName)
                                , _hstDescription(description)
                                , _overridenPath(overPath)
                                { }
    /// Deletes created objects
    ~GetterBasedROOTHistHandler() {
        if(_histogram)
            delete _histogram;
    }
    /// ...
    virtual ProcRes process_event( event::Event & e ) override {
        if(!_histogram) {
            aux::CommonHstAxisParams chp[arityT];
            for( size_t i = 0; i < arityT; ++i ) {
                chp[i].nBins = _hstParams[i].nBins;
                chp[i].range[0] = _hstParams[i].range[0];
                chp[i].range[1] = _hstParams[i].range[1];
                chp[i].factor = _hstParams[i].factor;
            }
            _histogram = HistTraits::create( _hstBaseName
                                           , _hstDescription
                                           , chp );
        }
        // fill bin content in the histogram
        double values[arityT];
        for( size_t i = 0; i < arityT; ++i ) {
            values[i] = _hstParams[i].getter(e)*_hstParams[i].factor;
        }
        HistTraits::fill(
                *_histogram, values, _getWeight ? _getWeight(e) : 1);
        return kOk;
    }
    /// Writes histogram into current TFile
    virtual void finalize() override {
        //ThisTDirAdapter::tdirectories()[idHstPair.first]->cd();
        gFile->cd();
        if(_histogram) {
            _histogram->SetOption("colz");
            _histogram->Write();
        }
    }
};

///\brief Track score fit info specialization
///
/// Supports extended (geometry-defined) track score getters.
template< size_t arityT
        , typename HistTypeT
        >
class GetterBasedROOTHistHandler< event::ScoreFitInfo
                                , arityT
                                , HistTypeT
                                //, event::Association<event::Track, event::TrackFitInfo>::Collection::key_type
                                , DetID
                                >
        : public AbstractHitHandler<event::Track>
        , public util::GenericTDirAdapter< //event::Association< event::Track
                                           //                  , event::ScoreFitInfo
                                           //                  >::Collection::key_type
                                           DetID
                                         >
        , public ExtendedTrackScoreGetters
        , public SelectiveHandlerMixin
        {
public:
    /// Hit key type; one provided by association collection with `Event`
    typedef DetID HitKey;  // typename event::Association<event::Track, event::TrackScore>::Collection::key_type
    /// Histogram indexing key; must be constructible from `HitKey`. May be the
    /// same as `HitKey` for identity (a single entity is a single histogram)
    typedef HitKey HistKey;
    /// Current histogram traits type
    typedef THistTraits<HistTypeT, event::ScoreFitInfo> HistTraits;
    /// Currently used `GenericTDirAdapter` type
    typedef util::GenericTDirAdapter<HistKey> ThisTDirAdapter;
protected:
    /// Histograms index
    std::unordered_map<HistKey, HistTypeT *> _histograms;
    /// Histogram parameters
    aux::HstAxisParams<event::ScoreFitInfo> _hstParams[arityT];
    /// Getter for weights; may be null
    typename ExtendedTrackScoreGetters::Getter _getWeight;
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Histogram path template. Leave empty to not override default one
    std::string _overridenPath;
public:
    ///\brief Specialized ctr: special getters and placements calib class name
    GetterBasedROOTHistHandler( calib::Dispatcher & cdsp
                              , const std::string & tracksOnly
                              , const std::string & detsOnly
                              , const aux::HstAxisParams<event::ScoreFitInfo> (&hstp)[arityT]
                              , Getter weightGetter
                              , const std::string & baseName
                              , const std::string & description
                              , const std::string & overPath
                              , log4cpp::Category & logCat
                              , const std::string & namingClass
                              , const std::string & placementsClass="default"
                              ) : AbstractHitHandler<event::Track>(cdsp, tracksOnly, logCat, namingClass)
                                , ThisTDirAdapter(cdsp, namingClass)
                                , ExtendedTrackScoreGetters(cdsp, logCat, namingClass, placementsClass)
                                , SelectiveHandlerMixin(cdsp, detsOnly, logCat, namingClass)
                                , _hstParams(hstp)
                                , _getWeight(weightGetter)
                                , _hstBaseName(baseName)
                                , _hstDescription(description)
                                , _overridenPath(overPath)
                                { }
    /// Deletes created objects
    ~GetterBasedROOTHistHandler() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }
    /// ...
    virtual bool process_hit( EventID
                            , TrackID
                            , event::Track & track) override {
        for(const auto & p : track.scores) {
            const DetID hitKey = p.first;
            if( !SelectiveHandlerMixin::matches(hitKey) ) {
                continue;
            }
            assert(p.second);
            const event::ScoreFitInfo & scoreFitInfo = *p.second;
            bool hasValues = false;
            double values[arityT];
            for( size_t i = 0; i < arityT; ++i ) {
                values[i] = _hstParams[i].getter(hitKey, scoreFitInfo, *this)*_hstParams[i].factor;
                if(!std::isnan(values[i])) hasValues = true;
            }
            if(!hasValues) return true;  // no non-nan values

            HistKey key(hitKey);
            auto it = _histograms.find(key);
            if( _histograms.end() == it ) {
                aux::CommonHstAxisParams chp[arityT];
                for( size_t i = 0; i < arityT; ++i ) {
                    chp[i].nBins = _hstParams[i].nBins;
                    chp[i].range[0] = _hstParams[i].range[0];
                    chp[i].range[1] = _hstParams[i].range[1];
                    chp[i].factor = _hstParams[i].factor;
                }
                // No histogram entry exists for current detector entity -- create
                // and insert one
                std::string hstName = _hstBaseName;
                auto substCtx = ThisTDirAdapter::subst_dict_for(key, hstName);
                std::string description = util::str_subst( _hstDescription, substCtx );
                auto p = ThisTDirAdapter::dir_for( key, substCtx, _overridenPath);
                p.second->cd();
                it = _histograms.emplace( key
                        , HistTraits::create( p.first
                                            , description
                                            , chp )
                        ).first;
            }
            // fill bin content in the histogram
            HistTraits::fill(*(it->second), values, _getWeight ? _getWeight(hitKey, scoreFitInfo, *this) : 1 );
        }
        return true;
    }
    /// Writes histogram into current TFile
    virtual void finalize() override {
        // well, traits method for seems to be redundant to just call
        // Write() yet more consistent; decided to call it directly so far
        // since runtime polymorphism is da way of ROOT stuff
        if( _histograms.empty() ) {
            this->log().warn("No objects were created for \"%s\" (\"%s\")."
                    , _hstBaseName.c_str()
                    , _hstDescription.c_str() );
        }
        for( auto idHstPair : _histograms ) {
            if(ThisTDirAdapter::tdirectories()[idHstPair.first]) {
                assert(ThisTDirAdapter::tdirectories()[idHstPair.first]);
                ThisTDirAdapter::tdirectories()[idHstPair.first]->cd();
            } else {
                gFile->cd();
            }
            if(idHstPair.second) {
                idHstPair.second->Write();
            }
        }
    }
};


// Traits for some common histograms/plots/etc.

template<typename HitT>
struct THistTraits<TH1F, HitT> {
    static TH1F * create( const std::string & name
                        , const std::string & description
                        , const aux::CommonHstAxisParams (&pars)[1]
                        ) {
        return new TH1F( name.c_str(), description.c_str()
                       , pars[0].nBins
                       , pars[0].range[0], pars[0].range[1] );
    }
    static void fill( TH1F & obj
                    , const double (&values)[1]
                    , double weight
                    ) {
        obj.Fill( values[0], weight );
    }
};

template<typename HitT>
struct THistTraits<TH2F, HitT> {
    static TH2F * create( const std::string & name
                        , const std::string & description
                        , const aux::CommonHstAxisParams (&pars)[2]
                        ) {
        return new TH2F( name.c_str(), description.c_str()
                       , pars[0].nBins, pars[0].range[0], pars[0].range[1]
                       , pars[1].nBins, pars[1].range[0], pars[1].range[1]
                       );
    }
    static void fill( TH2F & obj
                    , const double (&values)[2]
                    , double weight
                    ) {
        obj.Fill( values[0], values[1], weight );
    }
};

// NOTE: for TProfile we request two axis parameters set to not mess up with
// the arity, yet only 1st is used effectively
template<typename HitT>
struct THistTraits<TProfile, HitT> {
    static TProfile * create( const std::string & name
                        , const std::string & description
                        , const aux::CommonHstAxisParams (&pars)[2]
                        ) {
        return new TProfile( name.c_str(), description.c_str()
                       , pars[0].nBins, pars[0].range[0], pars[0].range[1]
                       );
    }
    static void fill( TProfile & obj
                    , const double (&values)[2]
                    , double weight
                    ) {
        obj.Fill( values[0], values[1], weight );
    }
};

template<typename HitT>
struct THistTraits<TH3F, HitT> {
    static TH3F * create( const std::string & name
                        , const std::string & description
                        , const aux::CommonHstAxisParams (&pars)[3]
                        ) {
        return new TH3F( name.c_str(), description.c_str()
                       , pars[0].nBins, pars[0].range[0], pars[0].range[1]
                       , pars[1].nBins, pars[1].range[0], pars[1].range[1]
                       , pars[2].nBins, pars[2].range[0], pars[2].range[1]
                       );
    }
    static void fill( TH3F & obj
                    , const double (&values)[3]
                    , double weight
                    ) {
        obj.Fill( values[0], values[1], values[1], weight );
    }
};

}  // namespace ::na64dp::handlers
}  // namespace na64dp

