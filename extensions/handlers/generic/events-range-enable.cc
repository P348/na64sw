#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64dp/pipeline.hh"
#include "na64event/data/event.hh"

#include "na64util/str-fmt.hh"
#include "sub.hh"

#include <memory>
#include <na64/ddd-ext/event-selection.hh>

#include <stdexcept>
#include <vector>
#include <yaml-cpp/node/detail/iterator_fwd.h>

namespace na64dp {
namespace handlers {

/**\brief Enables handlers for certain events range
 *
 * This handler conditionally forwards event to one of its descendants, based
 * on current event ID. Used for conditional execution of certain
 * reconstruction routine, depending on run ID, data taking period, etc.
 * Resembles `switch/case` clause from imperative languages.
 *
 * \code{.yaml}
 *     - _type: EventIDSwitch
 *       switch:
 *          - case: ['123.1.1', '123.1.2', '124.*.*', '125.1.1-127.3.4']
 *            pipeline:
 *              # ...
 *          - case: "./selected-events.txt"
 *            pipeline:
 *              # ...
 *       # how to forward steering flags from sub-pipelines
 *       onStop: forward          # "forward"/"ignore"/"discriminate"
 *       onDiscriminate: forward  # "forward"/"ignore"/"stop"
 *       # fallback cases
 *       # - no match found
 *       onNoMatch: error         # "ignore"/"discriminate"/"error" or define fallback in pipeline
 *       # - selection overlaps
 *       onMultipleMatch: error   # "applyAll"/"applyNone"/"discriminate"/"error" or define fallback in pipeline
 * \endcode
 *
 * \ingroup handlers generic-handlers
 * */
class EventIDSwitch
            : public AbstractHandler
            {
public:
    typedef std::pair< ddd_ext::EventSelection
                     , std::shared_ptr<Subpipe>
                     > RangedPipeline;
    enum ActionOnBadMatch {
        kError,
        kApplyAll,      ///< apply all matched sub-pipes, only for "multiple match" case
        kApplyNone,     ///< apply nothing from matched sub-pipes, only for "multiple match" case
        kDiscriminate,
        kFallback
    };
protected:
    /// List of pairs: event selection (ranges), sub-pipe to execute
    std::vector<RangedPipeline> _rangedPipelines;
    /// Internal cache of sub-pipe candidates
    std::vector<Subpipe *> _candidates;
    /// How to deal with the flags recieved from individual cases
    unsigned int _ownFlags;

    /// Action code for none matched case
    const ActionOnBadMatch _noMatchAction;
    /// Fallback pipeline in no-match case, can be null
    std::shared_ptr<Subpipe> _fallbackNoMatch;

    /// Action code for multiple match case
    const ActionOnBadMatch _multMatchAction;
    /// Fallback pipeline in multiple match case, can be null
    std::shared_ptr<Subpipe> _fallbackMultMatch;
public:
    /// ... features of the ctr, if any
    EventIDSwitch( std::vector<RangedPipeline> & ppls
                , unsigned int ownFlags
                , log4cpp::Category & logCat
                //
                , ActionOnBadMatch noMatchAction=kError
                , std::shared_ptr<Subpipe> fallbackNoMatch=nullptr
                //
                , ActionOnBadMatch multMatchAction=kError
                , std::shared_ptr<Subpipe> fallbackMultMatch=nullptr
                //
                , const std::string & namingSubclass="default"
                )
            : _rangedPipelines(ppls)
            //
            , _ownFlags(ownFlags)
            //
            , _noMatchAction(noMatchAction)
            , _fallbackNoMatch(fallbackNoMatch)
            //
            , _multMatchAction(multMatchAction)
            , _fallbackMultMatch(fallbackMultMatch)
            {
        if(kFallback != noMatchAction && fallbackNoMatch) {
            NA64DP_RUNTIME_ERROR("Bad arguments: error requested for no-match"
                    " case and fallback pipeline given (should be either).");
        }
        if(kFallback != multMatchAction && fallbackMultMatch) {
            NA64DP_RUNTIME_ERROR("Bad arguments: simple action requested for"
                    " multiple-match case and fallback pipeline given"
                    " (should be either).");
        }
    }

    ProcRes process_event(event::Event &event) override;

    static ddd_ext::EventSelection selection_from_YAML(const YAML::Node &);
};  // class EventIDSwitch


ddd_ext::EventSelection
EventIDSwitch::selection_from_YAML(const YAML::Node & node) {
    // TODO: somewhat quick and dirty implementation, possibly to be
    //       substituted in a future for something more expressive and
    //       elegant ...
    ddd_ext::EventSelection sel = ddd_ext::EventSelection::none();
    // simple string
    if(node.IsScalar()) {
        ddd_ext::concat_selection_str(sel, node.as<std::string>(), "OR");
        return sel;
    }
    // array, concatenated with OR
    if(node.IsSequence()) {
        for(auto it = node.begin(); it != node.end(); ++it) {
            sel = sel || selection_from_YAML(*it);
        }
        return sel;
    }
    // object -- expect "superset" and "exclude"
    if(node.IsMap()) {
        throw std::runtime_error("TODO: interpret complex YAML object as"
                " event selection");
        //if(!node["include"]) {
        //    NA64DP_RUNTIME_ERROR("\"include\" attribute is not defined for"
        //            " YAML Object node of event ID selection.");
        //}
        //sel = selection_from_YAML(node["enableFor"]);
        //if(node["exclude"])
        //return sel;
    }
    NA64DP_RUNTIME_ERROR("Can't interpret event selection expression.");
}

AbstractHandler::ProcRes
EventIDSwitch::process_event(event::Event &event) {
    // find sub-pipelines matching given event ID
    _candidates.clear();
    _candidates.reserve(_rangedPipelines.size());
    for( auto it = _rangedPipelines.begin()
       ; _rangedPipelines.end() != it
       ; ++it ) {
        if(! it->first.has({event.id.run_no(), event.id.spill_no(), event.id.event_no()})) continue;
        _candidates.push_back(it->second.get());
    }
    std::pair<bool, bool> hpFlags;
    // if just one matched, simple proceed with it and that's it (expected to
    // be most frequent case)
    if(1 == _candidates.size()) {
        hpFlags = _candidates[0]->process(event, lmem());
    } else if(_candidates.empty()) {
        // none matched -- do what was defined in "onNoMatch"
        if(_noMatchAction == kError) {
            assert(!_fallbackNoMatch);  // guaranteed by ctr
            NA64DP_RUNTIME_ERROR("Event %s did not match any cases."
                    , event.id.to_str().c_str());
        } else if(_noMatchAction == kFallback) {
            assert(_fallbackNoMatch);
            hpFlags = _fallbackNoMatch->process(event, lmem());
        } else if(_noMatchAction == kApplyNone ) {
            assert(!_fallbackNoMatch);
            #ifndef NDEBUG
            log().debug("Event %s did not match any of known cases,"
                    " quietly ignored.", event.id.to_str().c_str());
            #endif
            return kOk;  // important
        } else if(_noMatchAction == kDiscriminate) {
            assert(!_fallbackNoMatch);  // guaranteed by ctr
            log().debug("Discriminating event %s as it does not match any"
                    " cases.", event.id.to_str().c_str());
            return AbstractHandler::kDiscriminateEvent;
        }
        #ifndef NDEBUG
        else {
            assert(false);  // _noMatchAction is none of permitted cases
        }
        #endif
    } else {
        // mutliple match -- do what was defined in "onMultipleMatch"
        assert(_candidates.size() > 1);
        if(_multMatchAction == kError) {
            assert(!_fallbackMultMatch);  // guaranteed by ctr
            NA64DP_RUNTIME_ERROR("Event %s matched multiple cases."
                    , event.id.to_str().c_str());
        } else if(_multMatchAction == kApplyNone) {
            assert(!_fallbackMultMatch);  // guaranteed by ctr
            log().debug("Event %s did not match any of known cases,"
                    " quietly ignored.", event.id.to_str().c_str());
            return kOk;  // important
        } else if(_multMatchAction == kApplyAll) {
            for(auto ptr : _candidates) {
                auto hp = ptr->process(event, lmem());
                if(hp.first || hp.second) {
                    // TODO: handle it?
                    log().warn("Ignoring non-default sub-pipeline result"
                            " within case block.");
                }
            }
            return kOk;  // todo: affect hpFlags ?
        } else if(_multMatchAction == kFallback) {
            assert(_fallbackMultMatch);  // guaranteed by ctr
            hpFlags = _fallbackMultMatch->process(event, lmem());
        } else {
            log().debug("Event %s did not match any of known cases,"
                    " quietly ignored.", event.id.to_str().c_str());
            return kOk;  // important
        }
    }
    return Subpipe::forward_result(static_cast<AbstractHandler::ProcRes>(
                      (hpFlags.first  ? kStopProcessing    : 0x0)
                    | (hpFlags.second ? kDiscriminateEvent : 0x0)
                )
                , _ownFlags);
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

REGISTER_HANDLER( EventIDSwitch, cdsp, cfg
                , "Conditional fork for event stream based on event ID"
                ) {
    using namespace na64dp;
    auto & L = aux::get_logging_cat(cfg);

    // get own flags which override ones from individual pipelines
    unsigned int ownFlags = handlers::Subpipe::build_forward_flags_from(
              cfg["onStop"]         ? cfg["onStop"].as<std::string>()         : "forward"
            , cfg["onDiscriminate"] ? cfg["onDiscriminate"].as<std::string>() : "forward"
            );

    auto & rngs = cfg["switch"];
    if(!rngs) {
        NA64DP_RUNTIME_ERROR("No \"ranges\" node in EventIDSwitch node config.");
    }

    std::vector<handlers::EventIDSwitch::RangedPipeline> ppls;
    for(YAML::const_iterator it = rngs.begin(); it != rngs.end(); ++it) {
        const YAML::Node & rangedNode = *it;
        // check event selection
        if(!rangedNode["case"]) {
            NA64DP_RUNTIME_ERROR("No \"case\" attribute in case-node #%zu"
                    " of EventIDSwitch handler."
                    , ppls.size() + 1 );
        }
        // TODO: pop "case" from rangedNode (Subpipe::from_YAML() is not
        // using it)

        // build sub-pipe
        std::shared_ptr<handlers::Subpipe> subPipe;
        subPipe.reset(handlers::Subpipe::from_YAML(rangedNode, cdsp));
        ppls.push_back({handlers::EventIDSwitch::selection_from_YAML(rangedNode["case"])
                , subPipe});
        L.debug("Conditional pipeline of ... handlers constructed for range ...");  // TODO: details
    }

    // action if none matched
    handlers::EventIDSwitch::ActionOnBadMatch onNoMatchAction
            = handlers::EventIDSwitch::kError;
    std::shared_ptr<handlers::Subpipe> noMatchFallbackPpl;
    if(cfg["onNoMatch"]) {
        if(cfg["onNoMatch"].IsScalar()) {
            // interpret as scalar (str, one of "ignore", "error")
            const std::string val = cfg["onNoMatch"].as<std::string>();
            if(val == "ignore") onNoMatchAction = handlers::EventIDSwitch::kApplyNone;
            else if(val == "error") onNoMatchAction = handlers::EventIDSwitch::kError;
            else if(val == "discriminate") onNoMatchAction = handlers::EventIDSwitch::kDiscriminate;
            else {
                NA64DP_RUNTIME_ERROR("Can't interpret string value"
                        " for \"onNoMatch\": \"%s\", must be one of:"
                        " \"ignore\", \"error\", \"discriminate\".", val.c_str());
            }
        } else if(cfg["onNoMatch"].IsMap()) {
            // build fallback pipeline for no match
            noMatchFallbackPpl.reset(handlers::Subpipe::from_YAML(cfg["onNoMatch"], cdsp));
        }
    }
    // action if multiple matched
    handlers::EventIDSwitch::ActionOnBadMatch onMultMatch
            = handlers::EventIDSwitch::kError;
    std::shared_ptr<handlers::Subpipe> multMatchFallbackPpl;
    if(cfg["onMultipleMatch"]) {
        if(cfg["onMultipleMatch"].IsScalar()) {
            // interpret as scalar (str, one of "ignore", "error")
            const std::string val = cfg["onMultipleMatch"].as<std::string>();
            if(val == "error")          onMultMatch = handlers::EventIDSwitch::kError;
            else if(val == "applyAll")  onMultMatch = handlers::EventIDSwitch::kApplyAll;
            else if(val == "applyNone") onMultMatch = handlers::EventIDSwitch::kApplyNone;
            else if(val == "discriminate") onMultMatch = handlers::EventIDSwitch::kDiscriminate;
            else {
                NA64DP_RUNTIME_ERROR("Can't interpret string value"
                        " for \"onMultipleMatch\": \"%s\", must be one of:"
                        " \"applyAll\", \"applyNone\", \"error\", \"discriminate\"", val.c_str());
            }
        } else if(cfg["onMultipleMatch"].IsMap()) {
            // build fallback pipeline for no match
            onMultMatch = handlers::EventIDSwitch::kFallback;
            multMatchFallbackPpl.reset(handlers::Subpipe::from_YAML(cfg["onMultipleMatch"], cdsp));
        }
    }

    return new handlers::EventIDSwitch( 
            ppls, ownFlags
            , L
            , onNoMatchAction, noMatchFallbackPpl
            , onMultMatch, multMatchFallbackPpl
            , aux::get_naming_class(cfg)
            );
}

