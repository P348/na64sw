#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <vector>
#include <fstream>
#include <gsl/gsl_statistics.h>

namespace na64dp {
namespace handlers {

/**\brief Depicts value changes over time
 *
 * Using given value getter, extracts value from hits of certain type, puts
 * them at temporary storage of fixed capacity ("window") once window is full,
 * computes key statistical features like mean, standard deviation, median,
 * skewness, etc stores them by events range key and flushes the storage.
 *
 * Differs from "moving statistics" (sliding window) algorithm family in that
 * it does not perform statistical extractions for each sample \f$x_i\f$, but
 * instead once per \f$N\f$ samples.
 *
 * Helps to observe some time- or event-dependant systematics like in-spill
 * drifts, periodical fluctuations and so on.
 *
 * \todo unit tests on various working modes
 * */
class TimeSeriesTracker {
public:
    /// Extracted statistics entry.
    struct DistributionParameters {
        EventID evStart  ///< First event number
              , evEnd   ///< Last event number
              ;
        std::pair<time_t, uint32_t> timeStart, timeEnd;
        size_t nSamples;  ///< Number of samples being considered for this entry
        double mean  ///< Mean value
             , stddev  ///< Standard deviation value
             , skewness  ///< Skewness (3-rd central moment)
             , kurtosis  ///< Kurtosis (4-th central moment)
             , median  ///< Median value of distributon
             , autocorr ///< Autocorrelation
             , lowQ  ///< Lower quartile
             , upQ  ///< Upper quartile
             ;
        /// List of outliers (out of 2nd quantile)
        std::vector<double> outliers;
    };
    /// Defines the way of events breakdown
    enum BreakType {
        /// Makes new entry each N of events
        kFixedWindowLength,
        /// Makes new entry each N of events, dropping only the first half
        kHalfOverlap,
        /// Makes new entry each on spill number change (high memory consumption)
        kBySpill,
        /// Makes new entry based on time gap (or by the end of the spill)
        kPeriodic,
    };
private:
    const BreakType _bt;
    const bool _accountOutliers;
protected:
    /// List of current samples list
    std::vector<double> _samples;
    /// First event this samples set is valid for
    EventID _firstEvent;
    /// First event time
    std::pair<time_t, uint32_t> _firstEventTime;
    /// Last event this samples set is valid for
    EventID _lastEvent;
    /// Time of the last event being considered
    std::pair<time_t, uint32_t> _lastEventTime;

    /// Index of considered entries
    std::vector<DistributionParameters> _entries;

    /// Number of samples to consider
    const size_t _winSize;
    /// Invoked at the end of entry accumulation
    virtual void _finalize_entry( bool removeOutliers=false );
    /// Re-sets first/last event identifiers
    void _reset_events_ids();
    /// Has to be set for periodic
    std::pair<time_t, uint32_t> _period;
public:
    static BreakType break_type_by_str( const std::string & );
    static void extract_distribution_parameters( std::vector<double> & samples
                                               , bool removeOutliers
                                               , DistributionParameters & );

    TimeSeriesTracker( BreakType bt
                     , size_t winSize
                     , std::pair<time_t, uint32_t> timeWindow
                     , bool accountOutliers=true) : _bt(bt)
                                                  , _accountOutliers(accountOutliers)
                                                  , _winSize(winSize)
                                                  , _period(timeWindow)
                                                  {
        _reset_events_ids();
    }
    virtual ~TimeSeriesTracker() {}

    /// Returns break type this instance was initialized
    BreakType break_type() const { return _bt; }
    /// Shall be called on new event processing starts, prior to sample
    /// consideration; may invoke _entry_finalize()
    void account_event_no(EventID evID, const std::pair<time_t, uint32_t> & );
    /// Adds the given sample to statistics; may invoke _entry_finalize()
    void account_sample(double);
    /// Forces dropping the stats
    void finalize_current_entry(bool removeOutliers)
        { _finalize_entry(removeOutliers); }

    const std::vector<DistributionParameters> & entries() const { return _entries; }
};

std::ostream & operator<<(std::ostream &, const TimeSeriesTracker::DistributionParameters &);

/**\brief Template implementation for time series analyzing handler
 *
 * Common use case of this handler is restricted rather by raw data as it is
 * expected to track value changes over contigious series of events.
 *
 * Usage:
 * \code{.yaml}
 *     - _type: TimeSeries
 *       # Specifies particular hit type to iterate over; str, required
 *       subject: ...
 *       # Specifies a getter to use to retrieve a value; str, required
 *       value: ...
 *       # A base name for the histogram; str, required
 *       histName: ...
 *       # Break up types. Variants: "fixed", "half", "spill",
 *       # "periodic"; string, required
 *       breakBy: ...
 *       # Number of samples to consider (meaning depends on break type);
 *       # int, optional
 *       nSamples: 0
 *       # Whether to exclude outliers from consideration ; bool, optional
 *       trimOutliers: false
 *       # optional, hit selection expression
 *       applyTo: null
 * \endcode
 * 
 * \ingroup handlers generic-handlers
 *
 * \note currently supports only the hardcoded CSV output; foresee file name
 *       parameterisation, ROOT output or whatsoever
 *
 * \todo Support different output destinations
 * */
template<typename HitT>
class TimeSeries : public AbstractHitHandler<HitT>
                 , public util::TDirAdapter {
public:
    struct TimeSeriesCtrArgs {
        TimeSeriesTracker::BreakType bt;
        size_t winSize;
        std::pair<time_t, uint32_t> timeWindow;
        bool accountOutliers;
    };

    typedef typename event::Association<event::Event, HitT>::Collection::key_type Key;
protected:
    /// Common time series tracker constructor arguments
    TimeSeriesCtrArgs _ctrArgs;
    /// Current event time
    std::pair<time_t, uint32_t> _cEvTime;
    /// Index of time series, individual to detector
    std::map<Key, TimeSeriesTracker *> _series;
    /// A value getter callback
    typename event::Traits<HitT>::Getter _getter;
    /// Common name suffix
    const std::string _hstName;
public:
    TimeSeries( calib::Dispatcher & cdsp
              , const std::string & selection
              , typename event::Traits<HitT>::Getter getter
              , const std::string & hstName
              , TimeSeriesTracker::BreakType bt
              , size_t winSize
              , std::pair<time_t, uint32_t> timeWindow
              , bool accountOutliers
              , log4cpp::Category & logCat
              ) : AbstractHitHandler<HitT>(cdsp, selection, logCat)
                , util::TDirAdapter(cdsp)
                , _ctrArgs{bt, winSize, timeWindow, accountOutliers}
                , _getter(getter)
                , _hstName(hstName)
                {}

    /// Overriden to get the event time
    virtual AbstractHandler::ProcRes process_event( event::Event & event ) override {
        _cEvTime = event.time;
        return AbstractHitHandler<HitT>::process_event(event);
    }

    /// Takes into account value
    virtual bool process_hit( EventID eid
                            , Key did
                            , HitT & currentHit) override {
        assert(_getter);
        auto it = _series.find(did);
        if( _series.end() == it ) {
            // No series instance exists for current detector entity -- create
            // and insert one
            TimeSeriesTracker * trackerPtr = new TimeSeriesTracker(
                        _ctrArgs.bt, _ctrArgs.winSize, _ctrArgs.timeWindow
                        , _ctrArgs.accountOutliers
                    );
            it = _series.emplace(did, trackerPtr).first;  // `first' is an iterator
        }
        // fill bin content in the histogram
        it->second->account_sample( _getter(currentHit) );
        it->second->account_event_no( eid, _cEvTime );
        return true;
    }

    /// Writes saved statistics
    virtual void finalize() override {
        std::ofstream ofs("/tmp/time-series.csv");  // TODO: parameterization
        for( auto p : _series ) {
            #if 0
            std::string detName = nm.detector_ddd_name_by_id(p.first);
            #else
            std::map<std::string, std::string> ctx;
            ctx["hist"] = _hstName;
            this->util::TDirAdapter::naming().append_subst_dict_for(p.first, ctx);
            std::string path
                = util::str_subst( *nameutils::DetectorIDStrSubstTraits<Key>
                    ::path_template_for(naming(), p.first), ctx );
            // util::str_subst( this->util::TDirAdapter::naming().path_template(p.first), ctx );
            std::string detName = path.substr( path.rfind('/') + 1, std::string::npos );
            #endif
            p.second->finalize_current_entry(_ctrArgs.accountOutliers);
            for( auto entry : p.second->entries() ) {
                ofs << detName << "," << entry << std::endl;
            }
            delete p.second;
        }
    }
};

//                          * * *   * * *   * * *
using event::SADCHit;

void
TimeSeriesTracker::_finalize_entry( bool removeOutliers ) {
    if( _samples.empty() ) return;
    DistributionParameters en;
    en.evStart = _firstEvent;
    en.evEnd = _lastEvent;
    en.timeStart = _firstEventTime;
    en.timeEnd = _lastEventTime;
    extract_distribution_parameters( _samples, removeOutliers, en );
    _entries.push_back(en);
    // For half dropping mode, we have to clear only half
    if( kHalfOverlap != break_type() ) {
        _samples.clear();
    } else {
        _samples.erase( _samples.begin(), _samples.begin() + _samples.size()/2 );
    }
    _reset_events_ids();
}

void
TimeSeriesTracker::_reset_events_ids() {
    // todo: rewrite once na64ee will make it easier...
    _firstEvent = EventID(0);
    _lastEvent = EventID(0);
}

void
TimeSeriesTracker::extract_distribution_parameters( std::vector<double> & samples
                                                  , bool removeOutliers
                                                  , TimeSeriesTracker::DistributionParameters & en ) {
    en.outliers.clear();
    en.nSamples = samples.size();
    en.mean = gsl_stats_mean( samples.data(), 1, samples.size() );
    en.stddev = gsl_stats_sd_m( samples.data(), 1, samples.size(), en.mean );
    en.autocorr = gsl_stats_lag1_autocorrelation_m( samples.data(), 1, samples.size()
                                                  , en.mean );
    // Fill outliers / remove outliers from samples
    std::vector<double> * regularSamples = nullptr;
    if( removeOutliers ) {
        regularSamples = new std::vector<double>();
        regularSamples->reserve( samples.size() );
    }
    for( auto sample : samples ) {
        if( fabs(en.mean - sample) > 3*en.stddev ) {
            en.outliers.push_back(sample);
            continue;
        }
        if( removeOutliers ) {
            assert(regularSamples);
            regularSamples->push_back(sample);
        }
    }
    std::vector<double> & s = removeOutliers ? *regularSamples : samples;
    std::sort( s.begin(), s.end() );
    if( removeOutliers ) {
        en.mean = gsl_stats_mean( s.data(), 1, s.size() );
        en.stddev = gsl_stats_variance_m( s.data(), 1, s.size(), en.mean );
    }
    en.skewness = gsl_stats_skew_m_sd( s.data(), 1, s.size()
                                     , en.mean, en.stddev );
    en.kurtosis = gsl_stats_kurtosis_m_sd( s.data(), 1, s.size()
                                         , en.mean, en.stddev );
    en.median = gsl_stats_median_from_sorted_data( s.data(), 1, s.size() );
    en.lowQ = gsl_stats_quantile_from_sorted_data( s.data(), 1, s.size()
                                                 , .25 );
    en.upQ  = gsl_stats_quantile_from_sorted_data( s.data(), 1, s.size()
                                                 , .75 );
    if( regularSamples ) {
        delete regularSamples;
    }
}

void
TimeSeriesTracker::account_event_no( EventID eid
                                   , const std::pair<time_t, uint32_t> & evTime ) {
    _lastEvent = eid;
    _lastEventTime = evTime;
    if( 0 == _firstEvent.run_no() && 0 == _firstEvent.spill_no() ) {
        _firstEvent = eid;
        _firstEventTime = evTime;
        return;
    }
    if((eid.run_no() != _lastEvent.run_no()
     || eid.spill_no() != _lastEvent.spill_no()) ) {
        // Finalize by spill number condition
        _finalize_entry(!_accountOutliers);
        return;
    }
    if( kPeriodic == break_type() ) {
        // Find interval length and compare with first event time
        if( evTime.first - _firstEventTime.first >= _period.first
         && evTime.second - _firstEventTime.second >= _period.second ) {
            _finalize_entry(!_accountOutliers);
            return;
        }
    }
}

void
TimeSeriesTracker::account_sample(double sample) {
    _samples.push_back(sample);
    if( ( kFixedWindowLength == break_type()
       || kHalfOverlap == break_type() ) && _winSize == _samples.size() ) {
        // Finalize by fixed width conditions
        _finalize_entry(!_accountOutliers);
    }
}

TimeSeriesTracker::BreakType
TimeSeriesTracker::break_type_by_str( const std::string & name ) {
    if( name == "fixed" ) {
        return kFixedWindowLength;
    } else if( name == "half" ) {
        return kHalfOverlap;
    } else if( name == "spill" ) {
        return kBySpill;
    } else if( name == "periodic" ) {
        return kPeriodic;
    }
    NA64DP_RUNTIME_ERROR( "Unknown break type provided: \"%s\"."
                        , name.c_str() );
}

std::ostream &
operator<<(std::ostream & os, const TimeSeriesTracker::DistributionParameters & ps) {
    // column 1, 2: event ID for start and end
    os << ps.evStart << ",";
    os << ps.evEnd << ",";
    // column 3, 4 -- sec, usec of first event
    os << ps.timeStart.first << "," << ps.timeStart.second << ",";
    // column 5, 6 -- sec, usec of last event
    os << ps.timeEnd.first << "," << ps.timeEnd.second << ",";
    os << ps.nSamples  // column 7 -- number of samples
       << "," << ps.mean  // column 8 -- mean
       << "," << ps.stddev  // column 9 -- standard deviation
       << "," << ps.skewness  // column 10 -- skewness
       << "," << ps.kurtosis  // column 11 -- curtosis
       << "," << ps.median  // column 12 -- median (50% percentile)
       << "," << ps.autocorr  // column 13 -- autocorrelation
       << "," << ps.lowQ  // column 14 -- low quartile (25% percentile)
       << "," << ps.upQ   // column 15 -- upper quartile (75% percentile)
       ;
    // Rest columns are for outliers
    for( auto outl : ps.outliers ) {
        os << "," << outl;
    }
    return os;
}

}  // namespace ::na64dp::handlers

template<typename HitT> static AbstractHandler *
_construct_handler( calib::Manager & ch, const YAML::Node & cfg ) {
    std::pair<time_t, uint32_t> timeWindow = {0, 0};  // TODO
    return new handlers::TimeSeries<HitT>( ch
            , aux::retrieve_det_selection(cfg)
            , util::value_getter<HitT>(cfg["value"].as<std::string>())
            , cfg["histName"].as<std::string>()
            , handlers::TimeSeriesTracker::break_type_by_str(cfg["breakBy"].as<std::string>())
            , cfg["nSamples"] ? cfg["nSamples"].as<size_t>() : 0
            , timeWindow
            , cfg["trimOutliers"] ? (! cfg["trimOutliers"].as<bool>()) : false
            , aux::get_logging_cat(cfg)
            );
}

REGISTER_HANDLER( TimeSeries
                , ch, cfg
                , "Collects value distributions over time series" ) {
    using namespace event;
    const std::string valueStr = cfg["value"].as<std::string>();
    size_t subjPos = valueStr.find('.');
    std::string subject = std::string::npos == subjPos 
                        ? "event"
                        : valueStr.substr(0, subjPos)
                        ;
    #if 1
    #define CONSTRUCT_HANDLER(name, key, type, ...)                     \
    else if( subject == #name ) {                                       \
        return _construct_handler<event::type>( ch, cfg );              \
    }
    if( subject == "event" ) {
        NA64DP_RUNTIME_ERROR("TODO: time series for \"event\" subject");
        //return _construct_handler<event::Event>( ch, cfg );
    }
    //else if( "apvHitsPerPlane" == subject ) {
    //    return _construct_handler_per_plane<APVHit>(ch, cfg);
    //} else if( "f1HitsPerPlane" == subject ) {
    //    return _construct_handler_per_plane<F1Hit>(ch, cfg);
    //} else if( "stwtdcHitsPerPlane" == subject ) {
    //    return _construct_handler_per_plane<StwTDCHit>(ch, cfg);
    //}
    M_for_every_Event_collection_attribute(CONSTRUCT_HANDLER)
    #undef CONSTRUCT_HANDLER
    else {
        NA64DP_RUNTIME_ERROR( "Unkown value subject type: \"%s\""
                            , subject.c_str() );
    }
    #else
    if( !cfg["subject"] )
        NA64DP_RUNTIME_ERROR( "No \"subject\" argument provided within"
                " \"Histogram2D\" parameters section." );
    const std::string subject = cfg["subject"].as<std::string>();
    // TODO: support "event" subject type (default)
    //const std::string subject = cfg["subject"] ? cfg["subject"].as<std::string>()
    //                                           : "event"
    //                                           ;
    if( "SADCHit" == subject ) {
        return _construct_handler<SADCHit>(ch, cfg);
    } else if( "APVHit" == subject ) {
        return _construct_handler<APVHit>(ch, cfg);
    } else if( "F1Hit" == subject ) {
        return _construct_handler<F1Hit>(ch, cfg);
    } else if( "StwTDCHit" == subject ) {
        return _construct_handler<StwTDCHit>(ch, cfg);
    } else if( "APVCluster" == subject ) {
        return _construct_handler<APVCluster>(ch, cfg);
    } else if( "CaloHit" == subject ) {
        return _construct_handler<CaloHit>(ch, cfg);
    } else if( "TrackPoint" == subject ) {
        return _construct_handler<TrackPoint>(ch, cfg);
    }
    // ... (other standard hits)
    //else if( "Track" == subject ) {
    //    return _construct_handler<Track>(ch, cfg);
    //}
    else {
        NA64DP_RUNTIME_ERROR("Unknown subject provided to generic handler"
                " construction: \"%s\"", subject.c_str());
    }
    #endif
}
}

