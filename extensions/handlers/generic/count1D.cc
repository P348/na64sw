#include "na64calib/manager.hh"
#include "na64calib/evType.hh"
#include "na64sw-config.h"
#include <TDirectory.h>
#include <unordered_map>

#if defined(ROOT_FOUND) && ROOT_FOUND

#include "na64dp/abstractHandler.hh"
#include "na64event/data/event.hh"
#include "na64event/reset-values.hh"

#include <log4cpp/Category.hh>

#include <TH1F.h>
#include <TFile.h>

namespace na64dp {
namespace handlers {

/**\brief Draws bar chart with counts of unique values
 *
 * Depicts (weighted) counts for discontinuous values set.
 *
 * \todo This is a draft */
class PlotDistinctValuesCounts : public AbstractHandler {
public:
    typedef long Key_t;  // TODO: template parameter
protected:
    const std::string _hstName, _hstDescr;
    std::map<Key_t, double> _counts;
    event::Traits<event::Event>::Getter _getter, _wGetter;
public:
    PlotDistinctValuesCounts( const std::string & histogramName
            , const std::string & histogramDescription
            , event::Traits<event::Event>::Getter valueGetter
            , event::Traits<event::Event>::Getter weightGetter
            , log4cpp::Category & L)
        : AbstractHandler(L)
        , _hstName(histogramName), _hstDescr(histogramDescription)
        , _getter(valueGetter)
        , _wGetter(weightGetter)
        {}

    /// Get value, increase counter per value
    ProcRes process_event(event::Event & event) override {
        event::StdFloat_t value = _getter(event)
                        , w = _wGetter ? _wGetter(event) : 1.
                        ;
        Key_t k = (Key_t) value;
        auto ir = _counts.emplace(k, 0);
        ir.first->second += w;
        return ProcRes::kOk;
    }

    /// Create and write histogram
    void finalize() override {
        gFile->cd();
        TH1F * hst = new TH1F( _hstName.c_str(), _hstDescr.c_str()
                , _counts.size(), 0, _counts.size()
                );
        size_t n = 1;  // 1st bin is for underflow (see ROOT conventions)
        char binLabelBf[64];
        for( const auto & p : _counts ) {
            snprintf(binLabelBf, sizeof(binLabelBf), "%#lx", p.first);
            hst->SetBinContent(n, p.second);
            hst->GetXaxis()->SetBinLabel(n, binLabelBf);
            ++n;
        }
        hst->Write();
    }
};


/**\brief Ad-hoc handler plotting counts of event types and trigger bits
 * */
class PlotEventTypeStats
            : public AbstractHandler
            , public calib::Handle<EventBitTags>
            {
protected:
    const std::string _hstTrigName, _hstTrigDescr
                    , _hstEvTpName, _hstEvTpDescr
                    ;
    /// Counters arrays
    std::map<std::string, size_t> _trigCountsStr
                                , _evTypesStr
                                ;
    /// Access dictionaries, by integer values
    std::unordered_map<decltype(event::Event::trigger), size_t *> _trigCounts;
    std::unordered_map<decltype(event::Event::evType),  size_t *> _evTypeCounts;

    void handle_update(const EventBitTags & ebt) override {
        calib::Handle<EventBitTags>::handle_update(ebt);
        // update existing entries in the access dictionaries
        for(auto & acp: _trigCounts) {
            const std::string key = ebt.triggers.bitflags_to_str(acp.first);
            auto it = _trigCountsStr.find(key);
            assert(it != _trigCountsStr.end());
            acp.second = &it->second;
        }
        for(auto & acp: _evTypeCounts) {
            const std::string key = ebt.types.bitflags_to_str(acp.first);
            auto it = _evTypesStr.find(key);
            assert(it != _trigCountsStr.end());
            acp.second = &it->second;
        }
    }
public:
    PlotEventTypeStats( calib::Dispatcher & cdsp
            , const std::string & trigHstName, const std::string & trigHstDescr
            , const std::string & evTpHstName, const std::string & evTpHstDescr
            , log4cpp::Category & L)
        : AbstractHandler(L)
        , calib::Handle<EventBitTags>("default", cdsp)
        , _hstTrigName(trigHstName), _hstTrigDescr(trigHstDescr)
        , _hstEvTpName(evTpHstName), _hstEvTpDescr(evTpHstDescr)
        {}

    /// Get value, increase counter per value
    ProcRes process_event(event::Event & event) override {
        {  // trigger bits
            auto it = _trigCounts.find(event.trigger);
            if(it == _trigCounts.end()) {
                const std::string strKey = Handle<EventBitTags>::get().triggers.bitflags_to_str(event.trigger);
                auto strIt = _trigCountsStr.emplace(strKey, 0).first;
                it = _trigCounts.emplace(event.trigger, &strIt->second).first;
            }
            ++(*it->second);
        }
        {  // event types
            auto it = _evTypeCounts.find(event.evType);
            if(it == _evTypeCounts.end()) {
                const std::string strKey = Handle<EventBitTags>::get().types.bitflags_to_str(event.evType);
                auto strIt = _evTypesStr.emplace(strKey, 0).first;
                it = _evTypeCounts.emplace(event.evType, &strIt->second).first;
            }
            ++(*it->second);
        }

        return ProcRes::kOk;
    }

    /// Create and write histogram
    void finalize() override {
        size_t nEntries;
        gFile->cd();
        TH1I * hstTriggers = new TH1I( _hstTrigName.c_str(), _hstTrigDescr.c_str()
                , _trigCountsStr.size(), 0, _trigCountsStr.size()
                )
           , * hstEvTypes = new TH1I( _hstEvTpName.c_str(), _hstEvTpDescr.c_str()
                , _evTypesStr.size(), 0, _evTypesStr.size()
                )
           ;
        hstTriggers->SetOption("HIST TEXT0");
        hstEvTypes->SetOption("HIST TEXT0");

        //size_t n = 1;  // 1st bin is for underflow (see ROOT conventions)
        //char binLabelBf[64];
        nEntries = 0;
        for( const auto & p : _trigCountsStr ) {
            #if 0
            hst->Fill(n, p.second);
            hst->GetXaxis()->SetBinLabel(n, binLabelBf);
            ++n;
            #else
            hstTriggers->Fill(p.first.c_str(), p.second);
            nEntries += p.second;
            #endif
        }
        hstTriggers->SetEntries(nEntries);
        //hstTriggers->Write(0, TObject::kOverwrite);

        nEntries = 0;
        for( const auto & p : _evTypesStr ) {
            hstEvTypes->Fill(p.first.c_str(), p.second);
            nEntries += p.second;
        }
        hstEvTypes->SetEntries(nEntries);
        //hstEvTypes->Write(0, TObject::kOverwrite);
    }
};

}  // namespace ::na64dp::event
}  // namespace na64dp

REGISTER_HANDLER( PlotDistinctValuesCounts, cdsp, cfg
                , "Creates barchart of distinct value counts" ) {
    na64dp::event::Traits<na64dp::event::Event>::Getter getter, wGetter;
    getter = na64dp::util::value_getter<na64dp::event::Event>(cfg["value"].as<std::string>());
    if(cfg["weight"])
        wGetter = na64dp::util::value_getter<na64dp::event::Event>(cfg["weight"].as<std::string>());
    else
        wGetter = nullptr;
    return new na64dp::handlers::PlotDistinctValuesCounts(
                cfg["histName"].as<std::string>(),
                cfg["histDescr"].as<std::string>(),
                getter, wGetter,
                na64dp::aux::get_logging_cat(cfg)
            );
}

REGISTER_HANDLER( PlotEventTypeStats, cdsp, cfg
                , "Creates barchart of event triggers and event types" ) {
    return new na64dp::handlers::PlotEventTypeStats( cdsp
                , cfg["trigHistName"]    ? cfg["trigHistName"].as<std::string>() : "triggers"
                , cfg["trigHistDescr"]   ? cfg["trigHistDescr"].as<std::string>() : "Event trigger types ; type ; counts"
                , cfg["evTypeHistName"]  ? cfg["evTypeHistName"].as<std::string>() : "eventTypes"
                , cfg["evTypeHistDescr"] ? cfg["evTypeHistDescr"].as<std::string>() : "Event types ; type ; counts"
                , na64dp::aux::get_logging_cat(cfg)
            );
}

#endif  // defined(ROOT_FOUND) && ROOT_FOUND

