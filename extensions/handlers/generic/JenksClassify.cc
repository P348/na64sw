#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"
#include "na64util/numerical/natBreaks.hh"
#include "na64util/str-fmt.hh"
#include "na64event/data/event.hh"

#include <stdexcept>
#include <TArrayF.h>
#include <TFile.h>

namespace na64dp {
namespace handlers {

/**\brief Applies Jenks natural breaks classification procedure by getter func
 *
 * Usage:
 * \code{.yaml}
 *     - _type: SADCJenksBreaks
 *       # value getter name; requred, str
 *       value: ...
 *       # array name in the TFile; required, str
 *       name: ...
 *       # number of classes to break onto; required, int
 *       nBreaks: ...
 *       # optional, hit selection expression; str, opt
 *       applyTo: ...
 *       # path in the TFile to write breaks to; str, opt
 *       path: ""
 * \endcode 
 *
 * \todo Results output requires some work with naming stuff
 * \todo Rewrite as generalized, HDQL-based version
 * */
template< typename HitT
        , typename HistKeyT=typename event::Association<event::Event, HitT>::Collection::key_type
        >
class JenksClassify : public AbstractHitHandler<HitT>
                    , public util::GenericTDirAdapter<HistKeyT>
                    {
public:
    /// Currently used `GenericTDirAdapter` type
    typedef util::GenericTDirAdapter<HistKeyT> ThisTDirAdapter;
private:
    /// Base name for stored breaks array
    const std::string _baseName;
    /// Overriden path, can be empty
    const std::string _overridenPath;
    /// Number of breaks to be applied
    const int _nBreaks;
    /// Value getter to classify
    typename event::Traits<HitT>::Getter _getter;
    /// Containers to be filled.
    std::unordered_map<HistKeyT, std::vector<double>> _values;
public:
    JenksClassify( calib::Dispatcher & cdsp
                 , const std::string & only
                 , typename event::Traits<HitT>::Getter getter
                 , const std::string & baseName
                 , int nBreaks
                 , log4cpp::Category & logCat
                 , const std::string & namingClass
                 , const std::string & overridenPath
                 ) : AbstractHitHandler<HitT>(cdsp, only, logCat, namingClass)
                   , ThisTDirAdapter(cdsp, namingClass)
                   , _baseName(baseName)
                   , _overridenPath(overridenPath)
                   , _nBreaks(nBreaks)
                   , _getter(getter)
                   {
        assert( getter );
    }

    virtual bool process_hit( EventID
                            , typename event::Association<event::Event, HitT>::Collection::key_type key
                            , HitT & currentEcalHit) override {
        auto it = _values.emplace(key, std::vector<double>()).first;
        it->second.push_back( _getter(currentEcalHit) );
        return true;
    }

    virtual void finalize() override {
        using namespace na64dp::util::jenks;
        bool doPrintDbg = this->log().getPriority() >= log4cpp::Priority::DEBUG;
        for(auto & p : _values) {
            std::ostringstream dbgOss;
            msg_debug( this->log()
                     , "Read %zu events for Jenks classifier %p"
                     , p.second.size(), this );
            if( p.second.size() < (size_t) _nBreaks ) {
                this->log().warn( "Nothing to classify for item \"%s\" on %s"
                        " (there are only %zu items for %d breaks)."
                        , AbstractHitHandler<HitT>::naming()[p.first].c_str()
                        , _baseName.c_str()
                        , p.second.size(), (int) _nBreaks );
                continue;
            }
            this->log().debug( "Sorting and deduplicating values"
                      " for Jenks classifier %s on item \"%s\"."
                    , _baseName.c_str()
                    , AbstractHitHandler<HitT>::naming()[p.first].c_str()
                    );
            ValueCountPairContainer sortedUniqueValueCounts;
            GetValueCountPairs( sortedUniqueValueCounts
                                           , p.second.data()
                                           , _values.size() );
            this->log().debug( "%zu unique values retained for Jenks classifier"
                    " %s on item \"%s\""
                     , sortedUniqueValueCounts.size()
                     , _baseName.c_str()
                     , AbstractHitHandler<HitT>::naming()[p.first].c_str()
                     );
            if( sortedUniqueValueCounts.size() < (size_t) _nBreaks ) {
                this->log().warn( "Nothing to classify for item \"%s\" on %s"
                        " (there are only %zu unique items retained after"
                        " sorting for %d breaks)."
                        , AbstractHitHandler<HitT>::naming()[p.first].c_str()
                        , _baseName.c_str()
                        , p.second.size(), (int) _nBreaks );
                continue;
            }
            this->log().debug( "Finding Jenks breaks for Jenks classifier on"
                    " item \"%s\"."
                     , AbstractHitHandler<HitT>::naming()[p.first].c_str() );
            LimitsContainer resultingbreaksArray;
            ClassifyJenksFisherFromValueCountPairs( resultingbreaksArray
                                                  , _nBreaks
                                                  , sortedUniqueValueCounts
                                                  );

            gFile->cd();

            std::string baseName = _baseName;
            auto substCtx = ThisTDirAdapter::subst_dict_for(p.first, baseName);
            auto tDirItem = ThisTDirAdapter::dir_for( p.first, substCtx, _overridenPath);
            tDirItem.second->cd();

            if(doPrintDbg) {
                dbgOss << AbstractHitHandler<HitT>::naming()[p.first] << ": ";
            }

            TArrayF arr(resultingbreaksArray.size());
            size_t n = 0;
            for( double breakValue : resultingbreaksArray ) {
                arr[n++] = breakValue;
                if(dbgOss) dbgOss << breakValue << ", ";
            }

            tDirItem.second->WriteObject(&arr, baseName.c_str());

            if(doPrintDbg) {
                this->log().debug("Breaks values for %s on %s"
                        , dbgOss.str().c_str(), _baseName.c_str());
            }

            # if 0
            TDirectory * jenksDir = (TDirectory *) gFile->Get("breaks");
            if( !jenksDir ) {
                jenksDir = gFile->mkdir("jenksBreaks");
            } else {
                jenksDir[1] = (TDirectory *) jenksDir[0]->Get("preshower");
                jenksDir[2] = (TDirectory *) jenksDir[0]->Get("main");
            }
            if( _id.is_preshower() ) {
                jenksDir[1]->cd();
            } else {
                jenksDir[2]->cd();
            }
            char bf[128];
            snprintf( bf, sizeof(bf), "breaks_%s_%d_%d_%d"
                    , _valueName.c_str()
                    , _id.get_x(), _id.get_y(), _id.is_preshower() ? 0 : 1 );
            gDirectory->WriteObject( &arr, bf );
            # endif
        }
    }
};

}  // namespace ::na64dp::handlers

//                          * * *   * * *   * * *

using event::SADCHit;
using event::APVHit;
using event::APVCluster;
// ...

REGISTER_HANDLER( SADCJenksBreaks
                , ch, cfg
                , "Evaluates Jenks natural breaks procedure on given (M)SADC"
                  " hit value" ) {
    return new handlers::JenksClassify<SADCHit>( ch
                , aux::retrieve_det_selection(cfg)
                , util::value_getter<SADCHit>(cfg["value"].as<std::string>())
                , cfg["arrName"].as<std::string>()
                , cfg["nBreaks"].as<int>()
                , aux::get_logging_cat(cfg)
                , aux::get_naming_class(cfg)
                , cfg["path"] ? cfg["path"].as<std::string>() : ""
                );
}

#if 0

REGISTER_HANDLER( JenksBreaksAPV
                , ch, cfg
                , "Evaluates Jenks natural breaks procedure on given value"
                  " of APV hit" ) {
    return new handlers::JenksClassify<APVHit>( ch
                , aux::retrieve_det_selection(cfg)
                , util::value_getter<APVHit>(cfg["value"].as<std::string>())
                , cfg["nBreaks"].as<int>()
                , aux::get_logging_cat(cfg)
                , aux::get_naming_class(cfg)
                );
}

REGISTER_HANDLER( JenksBreaksAPVCluster
                , ch, cfg
                , "Evaluates Jenks natural breaks procedure on given value"
                  " of APV cluster" ) {
    return new handlers::JenksClassify<APVCluster>( ch
                , aux::retrieve_det_selection(cfg)
                , util::value_getter<APVCluster>(cfg["value"].as<std::string>())
                , cfg["nBreaks"].as<int>()
                , aux::get_logging_cat(cfg)
                , aux::get_naming_class(cfg)
                );
}
#endif

}

#if 0
REGISTER_HANDLER( JenksClassify, yamlNode
                , "performs Jenks-Fisher natural breaks optimization over a"
                  " choosen value subset for certain cell" ){
    return new JenksClassify( yamlNode["cellID"][0].as<int>()
                            , yamlNode["cellID"][1].as<int>()
                            , yamlNode["cellID"][2].as<int>()
                            , yamlNode["nBreaks"].as<int>()
                            , yamlNode["value"].as<std::string>() );
} 
#endif

