#include "na64dp/abstractHitHandler.hh"
#include "na64calib/setupGeoCache.hh"

#include "na64util/numerical/linalg.hh"

namespace na64dp {
namespace handlers {

/**\brief A Monte-Carlo helper handler breaking up 3D hit scores
 *
 * Usage:
 * \code{.yaml}
 *     - _type: BreakTrackScores
 *       # optional, hit selection expression
 *       applyTo: ...
 *       # Do ignore scores with no `mcTruth` attribute (non
 *       # Monte-Carlo scores); str, opt
 *       omitNonMC: true
 *       # Defines priority of scores position source and tolerance
 *       priority: [["own", false], ["mc-truth", true]]
 *       # What to do with the original score: compose, keep, remove; str, opt
 *       mode: 
 * \endcode
 *
 * Priority list defines source priorities to obtain global spatial point: can
 * contain "own" and/or "mc-truth" strings and is required flags. Examples:
 *
 * - `[["own", true]]` will use only `gR` field and will throw exception if score
 *   does not provide `gR`
 * - `[["own", false], ["mc-truth", true]]` will use `gR` if given. Otherwise
 *   will try to use `mcTruth.globalPosition` and will emit an error if latter
 *   is not provided in score
 * - `[["mc-truth", false]]` will try to use `mcTruth.globalPosition` if score
 *   has it, otherwise will pass by the score intact.
 *
 * `mode` parameter can have following values:
 * - `compose` created sub-scores will be included into original (source) score
 * - `keep` sub-scores will be created as standalone ones, original is kept intact
 * - `remove` sub-scores will be created as standalone ones, original is removed
 *
 * Breaks track scores with defined global position onto smaller scores
 * corresponding to various detector projection.
 *
 * \todo Handler does not work -- not fully implemented
 * \ingroup handlers tracking-handlers mc-handlers
 * */
class BreakTrackScores : public AbstractHitHandler<event::TrackScore>
                       , public calib::SetupGeometryCache
                       {
public:
    enum Mode { kCompose, kKeep, kRemove };
    enum Priority { kOwn, kMC };

    #if 0
    struct BaseBreakProcedure {
        virtual void break_score(const util::Vec3 &) = 0;
    };

    // Assumes most common case of planar sub-detectors. In this case
    // a `p` vector of global coordinates is decomposed by basis.
    struct PlaneBreakProcedure {
        std::unordered_map<DetID, util::Matrix3> subDetectors;
        util::Vec3 o;
        void break_score(const util::Vec3 & p) override {
            for(auto & sub : subDetectors) {
                util::Vec3 r = sub.second*(p - o);
            }
        }
    };
    #endif
    // ... other score break types?
protected:
    typedef void (BreakTrackScores::*BreakMethod)(const util::Vec3 &);

    const Mode _mode;
    const bool _omitNoMC;
    const std::vector<std::pair<Priority, bool>> _priorities;

    //std::unordered_map<DetID, BreakMethod> _breakMethods;
    //std::unordered_map<DetID, >

    void _break_on_wired_plane_hits(const util::Vec3 &, LocalMemory &);
public:
    BreakTrackScores( Mode mode
                    , bool omitNoMCScores
                    , const std::vector<std::pair<Priority, bool>> & ps
                    , calib::Dispatcher & cdsp
                    , log4cpp::Category & logCat)
        : AbstractHitHandler<event::TrackScore>(cdsp, logCat)
        , calib::SetupGeometryCache(cdsp, logCat)
        , _mode(mode)
        , _omitNoMC(omitNoMCScores)
        , _priorities(ps)
        {}

    void handle_single_placement_update(const calib::Placement &) override;
    //AbstractHandler::ProcRes process_event( event::Event & e ) override;
    bool process_hit(EventID, DetID, event::TrackScore &) override;
};

//AbstractHandler::ProcRes
//BreakTrackScores::process_event( event::Event & e ) {
//    AbstractHitHandler<event::TrackScore>::process_event(e);
//}

void
handle_single_placement_update(const calib::Placement & pl) {
    util::Vec3 o, u, v, w;
    util::cardinal_vectors(pl.center, pl.size, pl.rot, o, u, v, w);
    // a*ux + b*vx + c*wx = dx
    // a*uy + b*vy + c*wy = dy,  => M*x = d,  => x = M^-1*d
    // a*uz + b*vz + c*wz = dy
    const util::Matrix3 m
            =(util::Matrix3{{{ u.c.x, v.c.x, w.c.x }
                           , { u.c.y, v.c.y, w.c.y }
                           , { u.c.z, v.c.z, w.c.z }
                           }}).inv();
}

void
BreakTrackScores::_break_on_wired_plane_hits( const util::Vec3 & r
        , LocalMemory & lmem
        ) {

}

bool
BreakTrackScores::process_hit( EventID eid
                             , DetID did
                             , event::TrackScore & score) {
    if(std::isnan(score.lR[1])) return true;  // omit 1D score
    if(_omitNoMC && !score.mcTruth) return true;  // omit no-MC score
    // Prefer global coordinates from score (can be intentionally smeared
    // already), otherwise try to consider MCs if possible
    const std::remove_extent<decltype(event::TrackScore::gR)>::type * cs = nullptr;
    for(const auto & p : _priorities) {
        if(p.first == kOwn && !std::isnan(score.gR[0])) {
            cs = score.gR;
            break;
        } else if( p.second ) {
            NA64DP_RUNTIME_ERROR("Score entry \"%s\" has no global"
                    " coordinates although it explicitly requested -- can"
                    " not break up score."
                    , calib::SetupGeometryCache::naming()[did].c_str() );
        }
        if(p.first == kMC && (!score.mcTruth) && !std::isnan(score.mcTruth->globalPosition[0])) {
            cs = score.mcTruth->globalPosition;
            break;
        } else if( p.second ) {
            NA64DP_RUNTIME_ERROR("Score entry \"%s\" has no true simulated"
                    " coordinates although it explicitly requested -- can"
                    " not break up score."
                    , calib::SetupGeometryCache::naming()[did].c_str() );
        }
    }
    if(!cs) return true;  // no global coordinates -- nothing to do



    // for every sub-detector, find intersection and create local score
    util::cardinal_vectors( const float *
                          , const float *
                          , const float *, Vec3 &o, Vec3 &u, Vec3 &v, Vec3 &w)
    util::Vec3 d[] = {{cs[0], cs[1], cs[2]}};
    //const Vec3 w = 
    


    if( _mode == kRemove ) _set_hit_erase_flag();  // mark original score for remove
    return true;
}

}  // namespace ::na64dp::handlers
}  // namespace na64dp

