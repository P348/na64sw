/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include <cstdint>
#include <cstdlib>
#include <cassert>
#include <unordered_map>

#include "na64sw-config.h"

namespace na64dp {

///\brief Track numerical identifier type
///
/// Leftmost 8 bits are reserved for zone pattern information.
typedef uint32_t TrackID_t;

///\brief Tracking zone ID type
typedef uint8_t ZoneID_t;

/**\brief A track ID type
 *
 * This object interface wraps a single number representing a track ID. This
 * number encodes two fields:
 *  - a zone pattern identifying geometrical zones of detectors contributing
 *    to track;
 *  - a number of track of arbitrary meaning.
 * */
struct TrackID {
    TrackID_t code;

    static constexpr size_t numberBitlen = 24;
    static constexpr TrackID_t numberMax = (1 << numberBitlen) - 1;
    static constexpr ZoneID_t zoneMax = (1 << (sizeof(TrackID_t)*8 - numberBitlen)) - 1;

    explicit TrackID( ZoneID_t zones, TrackID_t nTrack )
        : code((zones << numberBitlen) | nTrack)
          { assert(nTrack < numberMax); }
    TrackID() : code(0x0) {}
    TrackID(const TrackID & o) : code(o.code) {}

    ZoneID_t zones() const { return (ZoneID_t) (code >> numberBitlen); }
    void zones(ZoneID_t);  // TODO
    TrackID_t number() const;
    void number(TrackID_t);  // TODO
    //void assign_zone(ZoneID_t zid) { code |= (((TrackID_t) zid) << 24); }
    //void reset_zones() { code &= (~(((TrackID_t) 0xf) << 24)); }
};

inline bool operator==(const TrackID & k1, const TrackID & k2)
        { return k1.code == k2.code; }

inline bool operator<(const TrackID & k1, const TrackID & k2)
        { return k1.code < k2.code; }


/// Special type used in type traits
struct ZoneID {
    ZoneID_t zoneCode;
    ZoneID(TrackID tID) : zoneCode(tID.zones()) {}
};

inline bool operator==(const ZoneID & k1, const ZoneID & k2)
        { return k1.zoneCode == k2.zoneCode; }

inline bool operator<(const ZoneID & k1, const ZoneID & k2)
        { return k1.zoneCode < k2.zoneCode; }

}  // namespace na64dp

namespace std {

template <> struct hash<na64dp::TrackID> {
    std::size_t operator()(const na64dp::TrackID & k) const {
        return k.code;
    }
};

template<>
struct less<na64dp::TrackID> {
    bool operator()(const na64dp::TrackID a, const na64dp::TrackID & b) const {
        return a.code < b.code;
    }
};

template <> struct hash<na64dp::ZoneID> {
    std::size_t operator()(const na64dp::ZoneID & k) const {
        return k.zoneCode;
    }
};

template<>
struct less<na64dp::ZoneID> {
    bool operator()(const na64dp::ZoneID a, const na64dp::ZoneID & b) const {
        return a.zoneCode < b.zoneCode;
    }
};

}  // namespace std

