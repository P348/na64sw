#pragma once

///\defgroup track-combining-hits Combination and conjugation rules for track scoring hits

/**\file
 * \brief Interfaces for hit conjugation rules
 *
 * This header provides abstraction for rules that shall define how to combine
 * multiple hits into track score.
 *
 * Rules are not the subject of runtime extensions as their frequent
 * modification is not foreseen in NA64sw.
 * */

#include "na64util/cartesian-product.hh"
#include "na64detID/wireID.hh"

#include <log4cpp/Category.hh>

namespace na64dp {
namespace event { struct TrackScore; };
namespace util {

/** \addtogroup track-combining-hits
 *  @{
 */

/**\brief Generator producing hits combinations
 *
 * The represents a state to iterate over set of combined hits of certain unit
 * (usually, a station composed from few layers).
 */
template<typename HitRefT, Arity_t NT>
struct iCombinedHitsGenerator {
    /// Shall returns a new hit ids combination
    virtual bool get(std::array<HitRefT, NT> &) = 0;
    /// Re-sets generator state to initial
    virtual void reset() = 0;
};

/**\brief A rule maintaining collections of hits
 *
 * A rule essentially defines how to consider the hits and iterate over
 * combinations of them. Defines a method to produce a sequence generator (see
 * `iCombinedHitsGenerator`).
 *
 * Has three-staged lifecycle:
 *
 *  1. filling it with `consider_hit()`
 *  2. (optionally) retrieving combinations of hits with `hits_generator()`
 *  3. Emptying collected hits with `reset()`
 *
 * This interface also exposes methods to _conjugate_ hits geometrically
 * into local (detector) reference system and global (main) reference system
 * with `calc_score_values()` method.
 * */
template< typename HitRefT
        , typename CollectionIDT
        , Arity_t NT
        >
struct iHitCombinationRule {
    /// Shall consider hit for adding into consideration. Returns `true` if
    /// hit has been considered.
    virtual bool consider_hit(HitRefT, CollectionIDT &) = 0;
    /// Returns new state to iterate over the hits.
    virtual std::shared_ptr< iCombinedHitsGenerator<HitRefT, NT> >
        hits_generator(CollectionIDT) = 0;
    /// Shall append given vector with list of available collection IDs
    virtual void collection_ids(std::vector<CollectionIDT> &) = 0;  /// TODO: const?
    /// Re-sets rule's state
    virtual void reset() = 0;

    ///\brief Sets local X, Y coordinates using scores
    ///
    /// Sets first and second element of `drsXY` to X and Y coordinates in
    /// detector reference system (correspondingly). Third element (if
    /// allocated) of `localXY` is not accessed. Rotation matrices and ofsets
    /// are not taken into account.
    virtual void calc_score_values( event::TrackScore &
                                  , const std::array<HitRefT, NT> & ) = 0;
};

/**\brief Template implementation to collect n-tuplets of hits per station.
 *
 * This is probably most common case of hits conjugation rule. Assumes station
 * with N layers so that hit on each layer can be part of the combined spatial
 * point. How exactly multiple hits should be combined is defined by
 * subclasses, but certain features are constrained at the level of this class'
 * utilities.
 *
 * At this level a rule should be parameterised by number of layers (template
 * parameter), and the `Generator` constructor class (factory). Generator
 * instance is used to iterate over hits combinations and is customizable.
 *
 * Interface of rule at this level is defined by `_check_plane_id()` that may
 * select stations by ID and `_n_set()` which is responsible for choosing n-th
 * set (returns layer number) of hits to provide a cartesian product (by
 * overriding `hits_generator()`).
 *
 * `iHitCombinationRule::hits_generator()` is implemented to delegate generator
 * construction to per-station generator constructor.
 *
 * \note to utilize cartesian generator for partial combinations, consider
 *       adding "empty" valued items into the set.
 * */
template< typename HitRefT
        , util::Arity_t NT
        >
class iPerStationHitCombinationRule : public iHitCombinationRule<HitRefT, StationKey, NT> {
public:
    /// Type for hits IDs collection
    typedef std::vector<HitRefT> Hits;
    /// Generator construct callback type
    typedef std::shared_ptr<iCombinedHitsGenerator<HitRefT, NT> >
        (*GeneratorCtr)( StationKey
                       , const std::array<Hits, NT> &
                       , const iPerStationHitCombinationRule<HitRefT, NT> &
                       );

    /// Generic implementation of N-ary generator
    ///
    /// This implementation is pretty common and just provides all possible
    /// combinations of collected hits for different planes.
    class BaseCartesianGenerator : public iCombinedHitsGenerator<HitRefT, NT> {
    protected:
        util::CartesianProduct<NT, Hits> _cartesianProduct;
    public:
        template<typename ... ArgsT>
        BaseCartesianGenerator(ArgsT && ... args) : _cartesianProduct(args ...) {}
        /// Returns cartesian product without additional check.
        ///
        /// For advanced combinations, consider overloading it.
        bool get(std::array<HitRefT, NT> & dest) override {
            return _cartesianProduct >> dest;
        }
        /// Re-sets generator for reentrant usage.
        void reset() override {
            _cartesianProduct.reset();
        }
    };
    /// Default ctr for N-tuplet product
    ///
    /// See `CartesianProduct`.
    static std::shared_ptr<iCombinedHitsGenerator<HitRefT, NT> >
        construct_basic_cartesian_generator( StationKey
                                           , const std::array<Hits, NT> &
                                           , const iPerStationHitCombinationRule<HitRefT, NT> &
                                           );
protected:
    /// Reference to log category in use
    log4cpp::Category & _L;
    /// Type of collections container
    typedef std::unordered_map< StationKey
                              , std::pair< std::array<Hits, NT>
                                         , GeneratorCtr
                                         >
                              > Collections;
    /// Stored collection of IDs per station
    Collections _byStationID;
    /// Default constructor for generators
    GeneratorCtr _dftCtr;
protected:
    /// Shall return whether rule is appliable to this plane ID
    virtual bool _check_hit_ref(HitRefT did) const = 0;
    /// Shall return number of set to push ID
    virtual size_t _n_set(HitRefT did) const = 0;
    /// Shall deduce station ID by hit reference
    virtual StationKey _station_key_from_hit_ref(HitRefT) const = 0;
    ///\brief Called once new station collection is allocated.
    ///
    /// Subclasses may override this method to pre-fill the collection with
    /// values (e.g. to provide empty values for partial cartesian product)
    virtual void _init_hits_collection(std::array<Hits, NT> &) {}
public:
    /// Must be initialized with default constructor for generators
    iPerStationHitCombinationRule( log4cpp::Category & logCat
                                 , GeneratorCtr dftCtr=construct_basic_cartesian_generator
                                 ) : _L(logCat)
                                   , _dftCtr(dftCtr)
                                   {}
    /// Checks plane ID with `_check_hit_ref()` and emplaces ID into set
    /// of the station which number is returned by `_n_set()`.
    bool consider_hit(HitRefT hitRef, StationKey & stationID) override {
        if(!_check_hit_ref(hitRef)) return false;
        stationID = _station_key_from_hit_ref(hitRef);
        auto it = _byStationID.find(stationID);
        if( _byStationID.end() == it ) {
            typedef typename Collections::mapped_type EntryType;
            it = _byStationID.emplace(stationID
                    , EntryType(std::array<Hits, NT>(), nullptr)).first;
            _init_hits_collection(it->second.first);
        }
        const size_t n = _n_set(hitRef);
        assert(n < NT);
        it->second.first[n].push_back(hitRef);
        return true;
    }
    /// Returns generator instance to iterate over the hits
    ///
    /// Will return NULL if constructor for station is not available.
    std::shared_ptr<iCombinedHitsGenerator<HitRefT, NT> >
    hits_generator(StationKey sk) override {
        auto it = _byStationID.find(sk);
        if( _byStationID.end() == it ) {
            return nullptr;
        }
        if( it->second.second )
            return it->second.second(sk, it->second.first, *this);
        else
            return _dftCtr(sk, it->second.first, *this);
    }

    /// Appends vector with station IDs
    void collection_ids(std::vector<StationKey> & dest) override {
        dest.reserve(dest.size() + _byStationID.size());
        std::transform( _byStationID.begin(), _byStationID.end()
                      , std::back_inserter(dest)
                      , [](const typename decltype(_byStationID)::value_type & p)
                            { return p.first; }
                      );
    }

    void reset() override {
        for(auto & p : _byStationID) {
            for(Arity_t n = 0; n < NT; ++n)
                p.second.first[n].clear();
            _init_hits_collection(p.second.first);
        }
    }
};  // iPerStationHitCombinationRule

template<typename HitRefT, util::Arity_t NT> std::shared_ptr<iCombinedHitsGenerator<HitRefT, NT> >
iPerStationHitCombinationRule<HitRefT, NT>::construct_basic_cartesian_generator( StationKey
                , const std::array<Hits, NT> & set
                , const iPerStationHitCombinationRule<HitRefT, NT> &
                ) {
    return std::make_shared<BaseCartesianGenerator>(util::unwind_array_to_constructor< 
                BaseCartesianGenerator,
                std::array<Hits, NT>
                >( std::move(set)
                 , std::make_integer_sequence<Arity_t, NT>{}
                 ));
}

/** @}*/

}  // namespace ::na64dp::util
}  // namespace na64dp

