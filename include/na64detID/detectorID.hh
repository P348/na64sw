/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"
#include "na64detID/detectorIDSelect.h"

#include <cstdint>
#include <cassert>
#include <functional>
#include <stddef.h>  // for size_t in global ns

/**\file
 * \brief Numerical detector id accessories.
 * 
 * This file defines `DetID` type used to encode file semantics into numeric
 * identifiers.
 * */

namespace na64dp {

struct PlaneKey;  // fwd

/// Fully determines detector within the NA64 experiment.
typedef uint32_t DetID_t;
/// Defines detector chip (e.g. SADC, APV, F1, etc)
typedef uint8_t DetChip_t;
/// Defines detector kin (e.g. ECAL, MM, GM, LYSO, etc)
typedef uint8_t DetKin_t;
/// Defines the orderly number type within the DDD naming scheme, i.e. the
/// postfix in names like "HCAL2" (2), "GM05" (5).
typedef uint8_t DetNumber_t;
/// Additional detector-identifying information (like cell number, x/y-index
/// and so on) may be also encoded in the payload part of the DetectorID.
typedef uint16_t DetIDPayload_t;

namespace aux {

template< typename T
        , uint8_t OffsetP
        , DetID_t MaxP
        , DetID_t MaskP> struct DetIDFieldHelper {
    static void unset(DetID_t & id) {
        id &= ~MaskP;
    }
    static void set(DetID_t & id, T val) {
        assert( val <= MaxP );
        unset(id);
        id |= ((val+1) << OffsetP);
    }
    static T get(const DetID_t & id) {
        return ((id & MaskP) >> OffsetP)-1;
    }
    static bool is_set(const DetID_t & id) {
        return (id & MaskP) >> OffsetP;
    }
};

/* Nasty implementation details:
 *  3 bits: chip number (1-7)
 *  5 bits: detector kin (1-31)
 *  8 bits: detector number (1-255)
 * 16 bits: payload
 */
constexpr uint8_t gChipIDBOffset    = 29
                , gKinIDBOffset     = 24
                , gDetNumOffset     = 16
                , gPayloadOffset    = 0
                ;

constexpr DetID_t gChipIDMax    = 0b110
                , gKinIDMax     = 0b11110
                , gDetNumMax    = 0b11111110
                , gPayloadMax   = 0xfffe
                ;

constexpr DetID_t gMaskChipID   = (0b111 << gChipIDBOffset)
                , gMaskKinID    = (0b11111 << gKinIDBOffset)
                , gMaskDetNum   = (0b11111111 << gDetNumOffset)
                , gMaskPayload  = (0xffff << gPayloadOffset)
                ;

typedef DetIDFieldHelper<DetChip_t,      gChipIDBOffset, gChipIDMax,  gMaskChipID>  ChipID;
typedef DetIDFieldHelper<DetKin_t,       gKinIDBOffset,  gKinIDMax,   gMaskKinID>   KinID;
typedef DetIDFieldHelper<DetNumber_t,    gDetNumOffset,  gDetNumMax,  gMaskDetNum>  DetNumID;
typedef DetIDFieldHelper<DetIDPayload_t, gPayloadOffset, gPayloadMax, gMaskPayload> DetPlID;

}  // namespace aux

struct DetID;  // fwd

/**\brief ...
 *
 * \ingroup auxDetID
 */
struct HitTypeKey {
    /// Numerical identifier of the detector entity.
    DetID_t id;

    explicit HitTypeKey( DetID_t id_ ) : id(id_ & aux::gMaskChipID) {}
    explicit HitTypeKey( DetChip_t chipID ) : id(0x0) {if(chipID) chip(chipID);}
    HitTypeKey() : id(0x0) {}
    HitTypeKey( DetID );

    /// Sets chip code
    void chip( DetChip_t v ) { aux::ChipID::set(id, v); }
    /// Returns true if chip value is set
    bool is_chip_set() const { return aux::ChipID::is_set(id); }
    /// Clears chip code
    void unset_chip() { aux::ChipID::unset(id); }
    /// Returns chip code
    DetChip_t chip() const { assert(is_chip_set()); return aux::ChipID::get(id); }
};

/**\brief ...
 *
 * \ingroup auxDetID
 */
struct DetKinKey : public HitTypeKey {
    explicit DetKinKey( DetID_t id_ ) : HitTypeKey(id_) { id |= (id_ & aux::gMaskKinID); }
    DetKinKey() : HitTypeKey() {}
    DetKinKey( DetID );
    DetKinKey( DetChip_t chipID
             , DetKin_t kinID ) : HitTypeKey(chipID) {
        if(kinID) kin(kinID);
    }

    /// Sets detector kin
    void kin( DetKin_t v ) { aux::KinID::set(id, v); }
    /// Returns true if kin value is set
    bool is_kin_set() const { return aux::KinID::is_set(id); }
    /// Clears kin code
    void unset_kin() { aux::KinID::unset(id); }
    /// Returns detector kin
    DetKin_t kin() const { assert(is_kin_set()); return aux::KinID::get(id); }
};

/**\brief ...
 *
 * \ingroup auxDetID
 */
struct StationKey : public DetKinKey {
    explicit StationKey( DetID_t id_ ) : DetKinKey(id_) { id |= (id_ & aux::gMaskDetNum); }
    StationKey() : DetKinKey() {}
    explicit StationKey( DetID );
    explicit StationKey( DetChip_t chipID
              , DetKin_t kinID
              , DetNumber_t num=0xff
              ) : DetKinKey( chipID, kinID ) {
        if(0xff != num) number(num);
    }

    /// Sets detector orderly number (e.g. 2 for HCAL2, 6 for GM06, etc)
    void number( DetNumber_t v )  { aux::DetNumID::set(id, v); }
    /// Returns true if number value is set
    bool is_number_set() const { return aux::DetNumID::is_set(id); }
    /// Clears number
    void unset_number() { aux::DetNumID::unset(id); }
    /// Returns detector orderly number (e.g. 2 for HCAL2, 6 for GM06, etc)
    DetNumber_t number() const { assert(is_number_set()); return aux::DetNumID::get(id) ; }
};

///\brief An OO-wrapper over the detector number.
///
/// This struct wraps DetID_t value offering a bit more convenient way to
/// operate with its bit-encoded information.
/// \ingroup mainDetID
struct DetID : public StationKey {
    /// Construct the wrapper around value.
    explicit DetID(DetID_t id_) : StationKey(id_) { id |= (id_ & aux::gMaskPayload); }
    /// Construct the zeroed id.
    DetID() : StationKey(0x0) {}
    /// A dedicated constructor accepting all the members: chip, kin, number
    /// and payload as separated arguments. Default values causes corresponding
    /// fields to not be initialized.
    /// \note for number "unint" marker is 255 (0xff)
    DetID( DetChip_t chipID
         , DetKin_t kinID
         , DetNumber_t num=0xff
         , DetIDPayload_t pl=0x0
         ) : StationKey(chipID, kinID, num) {
        if(pl) payload(pl);
    }

    /// Explicit ctr from hit type key
    explicit DetID(HitTypeKey htk) { chip(htk.chip()); }
    /// Explicit ctr from detector kin key
    explicit DetID(DetKinKey dkk) : DetID(dkk.chip(), dkk.kin()) {}
    /// Explicit ctr from station identifier
    explicit DetID(StationKey dsk) : DetID( dsk.chip()
                                          , dsk.kin()
                                          , dsk.number()
                                          ) {}
    /// Explicit ctr from detector plane identifier
    explicit DetID(PlaneKey dpk);

    /// Sets the fine identifier value (for certain station)
    void payload(DetIDPayload_t v)  { aux::DetPlID::set(id, v); }
    /// Returns true if payload value is set
    bool is_payload_set() const { return aux::DetPlID::is_set(id); }
    /// Clears payload
    void unset_payload() { aux::DetPlID::unset(id); }
    /// Returns the fine identifier value (for certain station)
    DetIDPayload_t payload() const { assert(is_payload_set()); return aux::DetPlID::get(id); }

    // access/conversion helpers

    /// Returns `true` if fields being set in this detector ID are the same of
    /// the ones being set in the given ones.
    ///
    /// \todo re-implement using bitmasks (will be more efficient)
    bool matches(const DetID &) const;

    /// Type-casting operator, simplifying the conversion.
    operator DetID_t () const { return id; }

    #if 1
    /// experimental feature
    template<typename T>
    class DetIDPayloadProxy {
    private:
        DetID & _ref;
        T _pl;
    public:
        DetIDPayloadProxy( DetID & didRef ) : _ref(didRef), _pl(_ref.payload()) {}
        ~DetIDPayloadProxy() {
            _ref.payload(_pl.id);
        }
        T * operator->() {
            return &_pl;
        }
    };

    template<typename T>
    class DetIDPayloadProxyConst {
    private:
        const T _pl;
    public:
        DetIDPayloadProxyConst( const DetID & didRef ) : _pl(didRef.payload()) {}
        const T * operator->() const {
            return &_pl;
        }
    };

    /// (experimental feature)
    template<typename T> DetIDPayloadProxy<T> payload_as() {
        return DetIDPayloadProxy<T>(*this);
    }

    template<typename T> DetIDPayloadProxyConst<T> payload_as() const {
        return DetIDPayloadProxyConst<T>(*this);
    }
    #endif
};

inline HitTypeKey::HitTypeKey( DetID did ) : HitTypeKey(did.id) {}
inline DetKinKey::DetKinKey( DetID did ) : DetKinKey(did.id) {}
inline StationKey::StationKey( DetID did ) : StationKey(did.id) {}

namespace util {
/// Common selectors set for detector selection DSL, NULL-terminated
extern const na64dpsu_SymDefinition * gDetIDGetters;
}

inline bool operator==(const StationKey & k1, const StationKey & k2) { return k1.id == k2.id; }

}  // namespace na64dp

namespace std {

template <> struct hash<na64dp::HitTypeKey> {
    std::size_t operator()(const na64dp::HitTypeKey & k) const {
        return k.id & ( na64dp::aux::gMaskChipID );
    }
};

template<>
struct less<na64dp::HitTypeKey> {
    bool operator()(const na64dp::HitTypeKey a, const na64dp::HitTypeKey & b) const {
        return (a.id & ( na64dp::aux::gMaskChipID ))
             < (b.id & ( na64dp::aux::gMaskChipID ))
             ;
    }
};


template <> struct hash<na64dp::DetKinKey> {
    std::size_t operator()(const na64dp::DetKinKey & k) const {
        return k.id & ( na64dp::aux::gMaskChipID
                      | na64dp::aux::gMaskKinID );
    }
};

template<>
struct less<const na64dp::DetKinKey> {
    bool operator()(const na64dp::DetKinKey a, const na64dp::DetKinKey & b) const {
        return (a.id & ( na64dp::aux::gMaskChipID | na64dp::aux::gMaskKinID ))
             < (b.id & ( na64dp::aux::gMaskChipID | na64dp::aux::gMaskKinID ))
             ;
    }
};


template <> struct hash<na64dp::StationKey> {
    std::size_t operator()(const na64dp::StationKey & k) const {
        return k.id & ( na64dp::aux::gMaskChipID
                      | na64dp::aux::gMaskKinID
                      | na64dp::aux::gMaskDetNum );
    }
};

template<>
struct less<na64dp::StationKey> {
    bool operator()(const na64dp::DetKinKey a, const na64dp::DetKinKey & b) const {
        return (a.id & ( na64dp::aux::gMaskChipID
                       | na64dp::aux::gMaskKinID
                       | na64dp::aux::gMaskDetNum ))
             < (b.id & ( na64dp::aux::gMaskChipID
                       | na64dp::aux::gMaskKinID
                       | na64dp::aux::gMaskDetNum ))
             ;
    }
};


template <> struct hash<na64dp::DetID> {
    std::size_t operator()(const na64dp::DetID & k) const {
        return k.id;
    }
};

template<>
struct less<na64dp::DetID> {
    bool operator()(const na64dp::DetID a, const na64dp::DetID & b) const {
        return a.id < b.id;
    }
};

}  // namespace std

