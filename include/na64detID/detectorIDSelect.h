/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef H_DETECTOR_ID_SELECTION_H
#define H_DETECTOR_ID_SELECTION_H

#include <stdio.h>

/**\file
 * \brief Defines an API of detectors selection expressions
 *
 * The generated parser is a subject of static linkage, with only few functions
 * exposed for external calls. Related auxiliary API is exposed here.
 * */

#ifdef __cplusplus
extern "C" {
#endif

/** Has to be set to the upper limit of 0-th address page */
#define NA64DP_DETID_MAX_DICT_OFFSET 4095

/** Maximum number of getters that may be defined */
#define NA64DP_DSUL_MAX_GETTERS 4095

#define NA64DP_DSUL_DYNAMIC_GETTER_CHAR '~'
#define NA64DP_DSUL_STATIC_GETTER_CHAR '$'
#define NA64DP_DSUL_EXTERN_RESOLVER_CHAR '+'

typedef const void * na64dp_dsul_PrincipalValue_t;

/** Detector value's code/value information atom */
typedef unsigned int na64dp_dsul_CodeVal_t;

/** Maximum value that may be kept within DSuL literals. Has to correspond
 * all significant bits of CodeVal_t value - 1. */
#define NA64DP_DSUL_CODE_VAL_MAX  ((~((na64dp_dsul_CodeVal_t) 0x0)) >> 1)

/** Detector ID value getter callback type */
typedef na64dp_dsul_CodeVal_t (*na64dp_dsul_DetIDSemanticGetter)(na64dp_dsul_PrincipalValue_t);

/** Callback type that accepts considered token and object of some arbitrary
 * type to get the value corresponding to the token. */
typedef int (*na64dp_dsul_DetIDExternResolver)(const char *, const void *, na64dp_dsul_CodeVal_t * );

/**\brief Value definition table entry
 *
 * Detector selectors interpreter relies on a set of externally-defined
 * identifiers. Strictly speaking any identifier as well as a numeric literal
 * belongs to one of these three groups:
 *  - constant (literals or aliased values like certain chip or kin)
 *  - static (values unconditionally derived from the detector ID)
 *  - dynamic (values conditionally derived from the detector ID)
 * Difference between static and dynamic values are subtle and comes from
 * the idea that payload features for one chip type may not have sense for
 * another (e.g. projection ID from APV payload is not appliable to SADC chip).
 *
 * All the available value-resolution aliases have to be defined in the entries
 * of these type. The "name" field must start with
 *  * `+` if value has to be eternally resolved (like detector kin name -- "MM",
 *      "GM", "ECAL", etc)
 *  * `$` if value is statically obtained from detector ID (like "chip", "kin",
 *      "number", etc)
 *  * `~` if value is dynamic (like "projection", "wire", "xIdx", etc)
 *
 * The symbol definitions table, once created may be safely used by multiple
 * evaluation contexts as none of its data is intended to undergo modification.
 * */
struct na64dpsu_SymDefinition {
    /** Definition name (with one-char prefix of special meaning) */
    const char * name;
    union {
        /** Getter function callback, used for static/dynamic definitions */
        na64dp_dsul_DetIDSemanticGetter get;
        /** Constant value setter callback, used for constant definitions */
        na64dp_dsul_DetIDExternResolver resolve_name;
    } callback;
    /** Description of symbol */
    const char * description;
};

/** Parses the detector selection expression into evaluation buffer */
int na64dp_dsul_compile_detector_selection( char * strexpr
            , const struct na64dpsu_SymDefinition * gettersTable
            , void * externalResolver
            , char * wsBuffer, int bufferSize
            , char * errorMsgBuffer, int errMsgBufferSize
            , FILE * dbgStream );

/** Prints descriptive information on the compiled expression */
void na64dp_dsul_dump( const char *, FILE * );

/**\brief Updates externally-resolved symbols within evaluation context
 *
 * Constant values are some externally-defined numerical values that is
 * supposed to be updated rarely. To provide updates conditioned by some
 * third-party contexts, we keep symbol names in evaluation buffer and this
 * routine will invoke corresponding getters with this names and given object
 * of arbitrary type to update values by cached offsets within a buffer.
 * */
void na64dp_dsul_update_extern_symbols( char * evalBuffer
                                      , const void * obj );

/**\brief Updates constants within predicate evaluation context
 *
 * Constant values are some externally-defined numerical values that is
 * supposed to be updated rarely.
 * */
void na64dp_dsul_update_static_symbols( char * evalBuffer
                                      , na64dp_dsul_PrincipalValue_t pv );


/**\brief Checks match of principle value wrt the expression
 *
 * Uses evaluation buffer to check the logical correspondance.
 *
 * Expects, that principal was previously set by
 * `na64dp_dsul_update_static_symbols()`. Returns `0` if check failed (`false`)
 * and `1` if passed (`true`).
 */
int na64dp_dsul_eval( const char * buffer );

#ifdef __cplusplus
}
#endif

#endif
