/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64detID/detectorID.hh"

#include <limits>
#include <map>
#include <string>

namespace na64dp {

constexpr uint8_t gWireNoOffset = 4;
constexpr DetIDPayload_t gProjectionMask = 0b1111;

/**\brief Auxiliary identifier for APV plane ID
 *
 * Identifies plane for APV-based detectors, unique within a station.
 * For globally uniuqe detector ID type, see `PlaneKey`.
 *
 * \todo Full support for from-string conversion for exotic projection codes...
 *       and cover it with UTs!
 *
 * \ingroup auxDetID
 * */
struct APVPlaneID {
    DetIDPayload_t id;

    // 0000
    // ^^^+- 1st/second coordinate (X,U -> 0, Y,U -> 1)
    // ||+-- chooses pair X,Y or U/v
    // |+--- supp pair, if set (i.e. X2, U2, etc)
    // +---- when not set, is a special (reserved) code, e.g. "unknown"

    /// Describes projection plane of the hit.
    enum Projection { kUnknown = 0
        /* Main projection codes */
        ,  kX = 0x8,                kY = 0x8 | 0x1
        ,  kU = 0x8 | 0x2,          kV = 0x8 | 0x2 | 0x1
        /* Supplementary projection codes */
        , kX2 = 0x8 | 0x4,         kY2 = 0x8 | 0x4 | 0x1
        , kU2 = 0x8 | 0x4 | 0x2,   kV2 = 0x8 | 0x4 | 0x2 | 0x1
        /* Special codes */
        , kXY = 0x1, kUV = 0x2  // usually used by supplementary digits
        , kP = 0x3,  // this is for F1/BMS
    };
    /// Returns projection code conjugated to given (Y for X, X for Y, V for
    /// U, etc). 
    static Projection conjugated_projection_code( Projection );
    /// Returns a single character corresponding to projection enumeration
    /// value: X, Y, U, V. For *2 adjoint codes returns ones without `2`.
    static char proj_label( Projection );
    /// Returns a projection code by single character
    static Projection proj_code( char );
    /// Returns adjoint projection code (X2 for X, V for V2, etc)
    static Projection adjoint_proj_code( Projection );
    /// Returns true if projection code is supplementary (e.g. true for 'X2',
    /// false for 'Y'.
    static bool proj_is_adjoint( Projection p ) { return 0x4 & p; }

    /// Ctr, accepts numerical ID
    APVPlaneID( DetIDPayload_t id_ ) : id(id_) {}
    /// Ctr, sets only the projection
    APVPlaneID(Projection pj) : id(0) {
        proj(pj);
    }

    /// Projection getter
    Projection proj() const {
        assert( proj_is_set() );  // LCOV_EXCL_LINE
        return (Projection) (gProjectionMask & id);
    }
    /// Projection setter
    void proj( Projection p ) {
        //assert( p < 5 );  // LCOV_EXCL_LINE
        //assert( p < 7 );  // xxx, remove "P"-projection LCOV_EXCL_LINE
        unset_proj();
        id |= p;
    }
    /// Unsets the projection value
    void unset_proj() {
        id &= ~gProjectionMask;
    }
    /// Returns `true` if projection is set
    bool proj_is_set() const {
        return id & gProjectionMask;
    }
    /// Returns `true` for special (non-projection) codes
    bool proj_is_special() const {
        return !(proj() & 0x8);
    }
};

/**\brief An on-wire hit identifier
 *
 * Define a bit structure of hit identifier related to certain wire on tracking
 * detectors. Used within the DetectorID structure at payloads for APV
 * detectors like GEMs or MuMegas.
 *
 * Stores projection axis identifier (X, Y, U, V) as well as the number of wire
 * (physical or raw). Max wire number is limited by 2^(16-3) = 8192.
 *
 * One has to distinguish "unset" and zero identifier, similar to `CellID`
 * class.
 *
 * \ingroup mainDetID
 * */
struct WireID : public APVPlaneID {
    constexpr static DetIDPayload_t wireMax
        = (std::numeric_limits<DetIDPayload_t>::max() >> gWireNoOffset) - 1;

    /// Unset ID ctr
    WireID() : APVPlaneID(0x0) {}
    /// Copy ctr
    WireID(DetIDPayload_t id_) : APVPlaneID(id_) {}
    /// Initializes projection ID and wire number
    WireID(Projection pj, uint16_t wn) : APVPlaneID(pj) {
        wire_no(wn);
    }

    /// Wire number getter
    uint16_t wire_no() const {
        return (id >> gWireNoOffset) - 1;
    }

    /// Wire number setter
    void wire_no( uint16_t wn) {
        id &= ~((wireMax+1) << gWireNoOffset);
        id |= ((wn+1) << gWireNoOffset);
    }

    /// Makes wire number be not set
    void unset_wire_no() {
        id &= ~((wireMax+1) << gWireNoOffset);
    }

    /// Returns true if wire number is set
    bool wire_no_is_set() const {
        return id & ((wireMax+1) << gWireNoOffset);
    }

    /// Appends textual template completion context with `proj` and `wireNo`
    static void append_completion_context( DetID
                                         , std::map<std::string, std::string> & );
    /// Converts from WireID to string
    static void to_string( DetIDPayload_t, char *, size_t available );
    /// Converts from string to WireID 
    static DetIDPayload_t from_string( const char * );
};

/**\brief ...
 *
 * \ingroup auxDetID
 */
struct PlaneKey : public DetID {
    /// Constructs unique plane key
    PlaneKey( DetID_t id_ ) : DetID( id_ ) {
        WireID wid(payload());
        wid.unset_wire_no();
        payload(wid.id);
    }

    explicit PlaneKey( DetID id_ ) : DetID( id_ ){
        WireID wid(payload());
        wid.unset_wire_no();
        payload(wid.id);
    }

    PlaneKey( DetChip_t chip
            , DetKin_t kin
            , DetNumber_t num
            , WireID::Projection pj
            ) : DetID( chip, kin, num ) {
        WireID wid;
        wid.proj(pj);
        payload(wid.id);
    }

    explicit PlaneKey() : DetID(0x0, 0x0, 0) {unset_payload();}

    /// Projection getter
    WireID::Projection proj() const {
        return WireID(payload()).proj();
    }
    /// Projection setter
    void proj( WireID::Projection p ) {
        WireID wid(payload());
        wid.proj(p);
        payload(wid.id);
    }
    /// Unsets the projection value
    void unset_proj() {
        WireID wid;
        wid.unset_wire_no();
        payload(wid.id);
    }
    /// Returns `true` if projection is set
    bool proj_is_set() const {
        return WireID(payload()).proj_is_set();
    }
};

}  // namespace ::na64dp

namespace std {

template <> struct hash<na64dp::PlaneKey> {
    std::size_t operator()(const na64dp::DetID & k) const {
        return k.id;
    }
};

template<>
struct less<na64dp::PlaneKey> {
    bool operator()(const na64dp::PlaneKey a, const na64dp::PlaneKey & b) const {
        return a.id < b.id;
    }
};

}

