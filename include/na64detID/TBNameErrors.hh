/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/str-fmt.hh"

#include <cstring>

/**\file
 *
 * \brief Errors related to detector name/detector ID conversions.
 *
 * Contains declaration of the exceptions thrown by `DetectorNaming` class.
 * */

namespace na64dp {
namespace errors {
/// \brief Insertion failure exception class -- invalid ID provided with user
/// input.
class IDIsOutOfRange : public std::runtime_error {
private:
    int _proposedID;
    int _maximumValue;
public:
    IDIsOutOfRange( const char * what_, int proposed, int maximum ) throw();
    int proposed_id() const throw() { return _proposedID; }
    int max_id() const throw() { return _maximumValue; }
};

/// \brief Insertion failure exception class. Proposed chip/kin/detector name
/// is not unique.
class NameIsNotUniq : public std::runtime_error {
private:
    char _name[64];
public:
    NameIsNotUniq( const char * what_, const char * nm_ ) throw();
    const char * name() const throw() { return _name; }
};

/// \brief Insertion failure exception class. Proposed detector ID is not
/// unique.
class IDIsNotUniq : public std::runtime_error {
private:
    int _proposedID;
public:
    IDIsNotUniq( const char * what_, int proposed_ ) throw()
        : std::runtime_error(what_), _proposedID(proposed_) {}
    int proposed_id() const throw() { return _proposedID; }
};

///\brief A TBname decoding failure exception class.
class TBNameParsingFailure : public std::runtime_error {
private:
    char _TBName[64];
public:
    TBNameParsingFailure(const char * msg, const char * culprit_) throw();
    const char * culprit() const throw() { return _TBName; }
};

class UnknownKin : public TBNameParsingFailure {
public:
    std::string base;
    UnknownKin( const std::string & base_
              , const char * culprit_) throw() : TBNameParsingFailure(
            (std::string("Base name \"") + base_ + "\" does not refer to known kin.").c_str()
            , culprit_ )
        , base(base_) {}
};

/// \brief Detector name and/or the additional information is not complete
/// or not valid to identify the detector ID.
///
/// Raised by postfix conversion procedures.
class IncompleteDetectorName : public std::runtime_error {
private:
    char _culprit[32];
public:
    IncompleteDetectorName( const char * whatStr, const char * name )
            : std::runtime_error(whatStr) {
        strncpy(_culprit, name, sizeof(_culprit));
    }
    /// Returns string name that caused this exception.
    const char * culprit() const throw() {
        return _culprit;
    }
};

/// \brief At least one failed substitution during name formatting
///
/// Raised by ID-to-string conversion methods, if any curly bracket character
/// remains after substitution
class IncompleteDetectorID : public std::runtime_error {
private:
    char _template[64]
       , _formatted[64]
       ;
public:
    IncompleteDetectorID( const char * templateStr
                        , const char * formattedStr )
            : std::runtime_error( util::format("Detector name string pattern \"%s\" is"
                                  " incomplete after substitution: \"%s\""
                                , templateStr
                                , formattedStr).c_str() ) {
        strncpy(_template, templateStr, sizeof(_template));
        strncpy(_formatted, formattedStr, sizeof(_formatted));
    }
    /// Returns string name that caused this exception.
    const char * template_str() const throw() {
        return _template;
    }
    const char * formatted_str() const throw() {
        return _formatted;
    }
};

/// \brief No converters entry exists, but postfix is provided.
class NoMappingDefined : public std::runtime_error {
public:
    NoMappingDefined( const char * whatStr ) : std::runtime_error(whatStr) {}
};

///\brief Thrown when no entry is defined for key.
template<typename T>
class NoEntryForKey : public std::runtime_error {
private:
    T _culprit;
public:
    NoEntryForKey(const char * msg, const T & culprit_) throw()
        : std::runtime_error( msg )
        , _culprit(culprit_) {}
    const T & culprit() const throw() { return _culprit; }
};

}  // namespace na64dp::errors
}  // namespace na64dp

