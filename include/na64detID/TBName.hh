/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64detID/detectorID.hh"
#include "na64detID/trackID.hh"
#include "na64util/selector.hh"

#include "na64detID/chips.hh"
#include "na64detID/kins.hh"

#include "na64detID/TBNameErrors.hh"

/**\file
 * \brief Defines API to convert between detector names and numeric IDs.
 *
 * The TBName comes from NA58 (COMPASS) nomenclature. This short string ID
 * usually looks like "ECAL0", "GM01X1", "DC05Y2__" and should uniquely
 * identify a particular detector assembly. This file defines
 * `DetectorNaming` type and related functions facilitating conversion between
 * detector names and numeric IDs.
 */

namespace YAML {
class Node;  //fwd
}

namespace na64dp {
namespace nameutils {

/// \brief Defines mapping of detector textual ID to numerical ones.
///
/// This class seems to be more complex that simple task of one-to-one
/// correspondance would require, but actually its goal is to expose all the guts
/// of two-staged naming conversion scheme to user code. Considering two
/// use-cases:
///
/// - Conversion of naitve DDD TBNames, like `ECAL0`, `GM03X__` to numerical ID
/// - Conversion of NA64DP extended TBnames like `ECAL0:1-1`, `HCAL3:2-2` to
/// numerical ID.
///
/// Instance of this object is usually associated with calibration data (see
/// class CalibHandle).
class DetectorNaming : protected ChipFeaturesTable
                     , protected KinFeaturesTable {
protected:
    /// Internal version of subst dictionary appending function that relies
    /// on chin&kin features being already found in tables
    static void _append_subst_dict_for( DetID did
                                      , std::map<std::string, std::string> & m
                                      , const ChipFeaturesTable::Features * chipFtsPt=nullptr
                                      , const KinFeaturesTable::Features * kinFtsPtr=nullptr );
public:
    typedef ChipFeaturesTable::Features ChipFeatures;
    using ChipFeaturesTable::chip_id;
    using ChipFeaturesTable::chip_features;
    using ChipFeaturesTable::chip_add;
    using ChipFeaturesTable::has_chip;

    typedef KinFeaturesTable::Features KinFeatures;
    using KinFeaturesTable::KinID;
    using KinFeaturesTable::kin_id;
    using KinFeaturesTable::kin_features;
    using KinFeaturesTable::define_kin;
    using KinFeaturesTable::has_kin;

    /// Wipes out all added entries.
    void clear();

    /// Defines new detector kin entry with chip given by its name
    KinFeatures & define_kin( const std::string & kinName
                            , DetKin_t kinID
                            , const std::string & chipName
                            , const std::string & fmt="{kin}{statNum}"
                            , const std::string & kinDescription=""
                            , const std::string & histsTPath="" );

    /// Returns DAQ detector entity name by numerical detector identifier.
    std::string name( DetID did ) const;
    /// Returns partial DAQ detector entity name by numerical detector
    /// identifier, corresponds to a single station.
    /// \todo Additional checks and assertions
    std::string name( StationKey sk ) const;

    /// Shortcut for "name()"
    std::string operator[]( DetID did ) const { return name(did); }
    /// Shortcut for "name()" based on station key
    std::string operator[]( StationKey sk ) const { return name(sk); }
    // ...TODO: other shortcuts?

    /// Returns detector numerical identifier by given DAQ name
    DetID id(const std::string & nm, bool noThrow=false) const;
    /// Shortcut for "id()"
    DetID operator[]( const std::string & nm ) const { return id(nm); }

    /// Appends given map with detector-specific substitution entries
    void append_subst_dict_for(DetID_t, std::map<std::string, std::string> &) const;

    /// Appends given map with track-specific substitution entries
    void append_subst_dict_for(TrackID, std::map<std::string, std::string> &) const;

    /// Returns string template for histograms
    const std::string & name_template(DetID) const;

    /// Returns path string template for detector (by ID)
    const std::string & path_template(StationKey detID) const;
};

/// Defines names to be emitted into string substitution context for certain
/// identifier type
template<typename EntityIDT, typename=void> struct DetectorIDStrSubstTraits;

/// Specialization for usual detectorID
template< typename EntityIDT >
struct DetectorIDStrSubstTraits<EntityIDT
        , typename std::enable_if<std::is_base_of<HitTypeKey, EntityIDT>::value>::type
        > {
    static void append_context( const nameutils::DetectorNaming & nm
                              , EntityIDT did
                              , util::StrSubstDict & ctx
                              ) {
        nm.append_subst_dict_for(DetID(did), ctx);
        try {
            ctx["TBName"] = nm[did];
        } catch( errors::IncompleteDetectorID & e ) {
            ; // pass (no TBname available)
        }
    }
    static const std::string * path_template_for( const nameutils::DetectorNaming & nm
                                                , EntityIDT did ) {
        return &nm.path_template(did);
    }
};

/// Specialization for track IDs
template<>
struct DetectorIDStrSubstTraits<TrackID> {
    /// Appends context with "first.* and "second.*" entities got from first
    /// and second detector ID
    static void append_context( const nameutils::DetectorNaming &
                              , TrackID tid
                              , util::StrSubstDict & ctx
                              ) {
        char bf[32];
        snprintf(bf, sizeof(bf), "%zu", (size_t) tid.code);
        ctx["id"] = bf;
        snprintf(bf, sizeof(bf), "%#x", tid.zones() );
        ctx["trackZonePattern"] = bf;
    }
    /// Returns a default path template string for all the binary keys
    static const std::string * path_template_for( const nameutils::DetectorNaming & //nm
                                                , TrackID //tid
                                                ) {
        static const std::string tmpl("byTrackID/{trackZonePattern}-{id}/{hist}");
        return &tmpl;
    }
};

/// Specialization for zone IDs
template<>
struct DetectorIDStrSubstTraits<ZoneID> {
    /// Appends context with "first.* and "second.*" entities got from first
    /// and second detector ID
    static void append_context( const nameutils::DetectorNaming &
                              , ZoneID tid
                              , util::StrSubstDict & ctx
                              ) {
        char bf[32];
        snprintf(bf, sizeof(bf), "%#x", (int) tid.zoneCode );
        ctx["trackZonePattern"] = bf;
    }
    /// Returns a default path template string for all the binary keys
    static const std::string * path_template_for( const nameutils::DetectorNaming & //nm
                                                , ZoneID //tid
                                                ) {
        static const std::string tmpl("byTrackZone/{trackZonePattern}/{hist}");
        return &tmpl;
    }
};

/// Specialization for integral (vertex) IDs
template<>
struct DetectorIDStrSubstTraits<size_t> {
    /// Appends context with "first.* and "second.*" entities got from first
    /// and second detector ID
    static void append_context( const nameutils::DetectorNaming &
                              , size_t id
                              , util::StrSubstDict & ctx
                              ) {
        char bf[32];
        snprintf(bf, sizeof(bf), "%zu", id);
        ctx["id"] = bf;
    }
    /// Returns a default path template string for all the binary keys
    static const std::string * path_template_for( const nameutils::DetectorNaming & //nm
                                                , size_t //id
                                                ) {
        static const std::string tmpl("byID/{hist}-{id}");
        return &tmpl;
    }
};

/// Specialization for pair of detector IDs
template<>
struct DetectorIDStrSubstTraits< std::pair<DetID, DetID> > {
    /// Appends context with "first.* and "second.*" entities got from first
    /// and second detector ID
    static void append_context( const nameutils::DetectorNaming & nm
                              , std::pair<DetID, DetID> did
                              , util::StrSubstDict & ctx
                              );
    /// Returns a default path template string for all the binary keys
    static const std::string * path_template_for( const nameutils::DetectorNaming & nm
                                                , std::pair<DetID, DetID> k );
};


namespace detail {
template<std::string KinFeaturesTable::Features::*attr> struct DetStrFts;
// nameFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::nameFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::nameFormat;
};
// dddNameFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::dddNameFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::dddNameFormat;
};
// pathFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::pathFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::pathFormat;
};
};

/// Returns detector string pattern according to order of overriding
template<std::string KinFeaturesTable::Features::*attr> const std::string &
detector_string_patern( const ChipFeaturesTable::Features & chipFts
                      , const KinFeaturesTable::Features & kinFts ) {
    if( ! ((kinFts.*attr).empty()) ) return kinFts.*attr;
    return chipFts.*(detail::DetStrFts<attr>::chipPtr);
}

}  // namespace na64dp::utils


/// Alias for common detector ID selection type
typedef dsul::Selector<DetID, nameutils::DetectorNaming> DetSelect;

}  // namespace na64dp
