/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"

#include "na64detID/detectorID.hh"

#include <string>
#include <map>

namespace na64dp {
namespace nameutils {

class KinFeaturesTable {
public:
    struct Features {
        /// A name of detector kin
        const std::string name;
        std::string description  ///< A human-readable decription of detector kin
                  , nameFormat  ///< Name template of the detector entities
                  , dddNameFormat  ///< Format of DDD detector name
                  , pathFormat  ///< Path template for various relevant objects
                  ;
        /// Constructs new instance with name being set
        Features( const std::string & nm ) : name(nm) {}
    };
    /// Identifier of detector kin -- must include relevant chip ID
    typedef std::pair<DetChip_t, DetKin_t> KinID;
    /// Kin table type
    typedef std::map<KinID, Features> KinTable;
private:
    KinTable _kinTraits;
    std::map<std::string, KinID> _kinIDs;
public:
    /// Returns kin ID (chip + kin-in-chip) by its string name. Accepts name
    /// as it was given to `define_kin()`, raises exception if kin not found.
    KinID kin_id( const std::string & ) const;
    /// Returns kin features entry (by ID) as it was provided to `define_kin()`
    const Features & kin_features( KinID ) const;
    /// Returns kin features entry (by name)
    const Features & kin_features( const std::string & ) const;
    /// Defines new detector chip entry returning new entry for subsequent
    /// initialization
    /// \param kinName a string name of detector kin (family), like MM, ECAL
    /// \param kinID a desired kin ID number
    /// \param chipID a numeric chip identifier number
    /// \param nameFormat name pattern
    /// \param kinDescription a detector kin description (for humans)
    /// \param histsTPath a common histograms path template
    /// \return new `Features` instance
    Features & define_kin( const std::string & kinName
                         , DetKin_t kinID
                         , DetChip_t chipID
                         , const std::string & nameFormat="{kin}{statNum}"
                         , const std::string & kinDescription=""
                         , const std::string & histsTPath=""
                         );
    /// Returns true if kin with given name is defined
    bool has_kin(const std::string & name) const
            { return _kinIDs.end() != _kinIDs.find(name); }
    /// Wipes out all chip features
    void clear();
};

}
}
