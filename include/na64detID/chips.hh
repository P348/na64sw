/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"
#include "na64detID/detectorID.hh"

#include <map>
#include <string>

namespace na64dp {
namespace nameutils {

/**\brief Table of chip's features
 *
 * Represents runtime configuration of detector chips features. Although we
 * generally do not expect have much types of detector chips in NA64, this
 * helper class is intended to be extensible container for multiple chip types.
 *
 * The key feature of the chip, according to the way NA64 DAQ is implemented is
 * to define how the corresponding detector's hit shall be interpreted.
 * Although the hit interpretation itself is somewhat we can not define at the
 * runtime in C++ (it is, therefore, a subject of type traits technique), the
 * dynamic naming-relevant part is configured via various string templates.
 *
 * The purpose of this auxiliary class is to provide some shortcuts over chip
 * traits index.
 *
 * See docs for nested `ChipFeaturesTable::Features` structure documentation
 * for further insights.
 * */
class ChipFeaturesTable {
public:
    /// Represents general chip features, relevant for detector naming
    struct Features {
        /// Chip name
        const std::string name;
        std::string description  ///< Chip description
                  , pathFormat  ///< Generic format for objects path
                  , dddNameFormat  ///< Format of DDD detector name
                  , nameFormat ///< Common detector entity name format
                  ;
        /// Interprets the payload part of detector ID and appends the
        /// string substitution dictionary
        void (*append_completion_context)( DetID, std::map<std::string, std::string> & );
        /// Produces human- and machine-readable suffix according to payload,
        /// uniquely identifying the detector entity
        void (*to_string)( DetIDPayload_t, char *, size_t available );
        /// Parses the human- and machine-readable suffix into payload number
        DetIDPayload_t (*from_string)( const char * );

        /// Constructs empty features entry
        Features( const std::string & name_ ) : name(name_)
                                              , append_completion_context(nullptr)
                                              , to_string(nullptr)
                                              , from_string(nullptr)
                                              { assert(!name.empty()); }
    };
    /// Chips table storage type
    typedef std::map<DetChip_t, Features> ChipsTable;
private:
    /// Chip entries table, indexed by ID
    ChipsTable _chipFeatures;
    /// Chip IDs by name
    std::map<std::string, DetChip_t> _chipIDs;
public:
    /// Returns chip ID by its string name. Raises runtime error if not found.
    DetChip_t chip_id( const std::string & ) const;
    /// Returns chip features entry (by ID)
    const Features & chip_features( DetChip_t ) const;
    /// Returns chip features entry (by name)
    const Features & chip_features( const std::string & ) const;
    /// Defines new detector chip entry returning new entry for subsequent
    /// initialization
    Features & chip_add( const std::string & chipName
                       , DetChip_t chipID
                       , const std::string & chipDescription=""
                       , const std::string & histsTPath=""
                       );
    /// Returns true if chip with such name is defined
    bool has_chip( const std::string & nm ) const 
            { return _chipIDs.find(nm) != _chipIDs.end(); }
    /// Wipes out all chip features
    void clear();
};


}
}

