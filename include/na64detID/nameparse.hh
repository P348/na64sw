/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once
#include "na64sw-config.h"

#include <string>
#include <regex>

namespace na64dp {
namespace nameutils {

/**\brief Returns true if given token may be splitted into name components
 *
 * Uses regular expression internally to split the provided token into
 * detector name components. If string does not match, returns false. This
 * function defines only basic (lexical) match -- it does not guarantee that
 * given string is really valid DDD TBName or full detector name.
 *
 * \param[in] str String that has to be interpreted as detector name
 * \param[out] detName detector name part. Can not be empty if `str` matches.
 * \param[out] number detector number part. Can be empty even on match.
 * \param[out] payload detector name payload part. Can be empty.
 * \return whether provided string matches the naming scheme.
 * */
bool
tokenize_name_str( const std::string & str
                 , std::string & detName
                 , std::string & number
                 , std::string & payload );

}  // namespace ::na64dp::nameutils
}  // namespace na64dp
