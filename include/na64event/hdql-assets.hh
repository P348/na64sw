#pragma once
#include "na64sw-config.h"

struct hdql_Context;  // fwd

#if defined(hdql_FOUND) && hdql_FOUND
#include <hdql/context.h>

namespace hdql {
namespace helpers {
class CompoundTypes;  // fwd
}
}

namespace na64dp {
namespace calib {
class Dispatcher;  // fwd
}

void init_root_hdql_context(hdql_Context *, hdql::helpers::CompoundTypes *&, calib::Dispatcher & cdsp);
void destroy_root_hdql_context(hdql_Context *, hdql::helpers::CompoundTypes * compoundTypes);

}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND

