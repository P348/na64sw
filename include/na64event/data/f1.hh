#pragma once

/**\file
 * \brief F1 TDC hit
 * 
 * Representation of hit on the F1-chip based detector
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"



namespace na64dp {

namespace event {
/// Raw data of an F1 digit
///
/// **GETTER**:
/// * `sourceID` \-- DAQ channel source ID
/// * `portID` \-- DAQ port ID
/// * `channel` \-- DAQ detector channel number
/// * `channelPosition` \-- channel position
/// * `time` \-- Time associated with the channel
/// * `timeUnit` \-- Time unit
/// * `timeDecoded` \-- time in ns wrt trigger time
/// * `timeReference` \-- ...
struct RawDataF1 {
    /// DAQ channel source ID
    uint16_t sourceID;
    
    /// DAQ port ID
    uint16_t portID;
    
    /// DAQ detector channel number
    int32_t channel;
    
    /// channel position
    int32_t channelPosition;
    
    /// Time associated with the channel
    int32_t time;
    
    /// Time unit
    double timeUnit;
    
    /// time in ns wrt trigger time
    double timeDecoded;
    
    /// ...
    double timeReference;
    
    /// Default ctr (creates uninitialized instance of RawDataF1.
    RawDataF1(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of RawDataF1.
    /// Needed to simplify compatibility with template code.
    RawDataF1( LocalMemory & ) {}
    
};  // struct RawDataF1

#define M_for_every_RawDataF1_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<RawDataF1> {
    static constexpr char typeName[] = "RawDataF1";
    /// Callback function type, receiving an instance of type `RawDataF1` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const RawDataF1 & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=RawDataF1>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "sourceID"), obj.sourceID );
        callable( CallableTraits<CallableT>::attr_id(1, "portID"), obj.portID );
        callable( CallableTraits<CallableT>::attr_id(2, "channel"), obj.channel );
        callable( CallableTraits<CallableT>::attr_id(3, "channelPosition"), obj.channelPosition );
        callable( CallableTraits<CallableT>::attr_id(4, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(5, "timeUnit"), obj.timeUnit );
        callable( CallableTraits<CallableT>::attr_id(6, "timeDecoded"), obj.timeDecoded );
        callable( CallableTraits<CallableT>::attr_id(7, "timeReference"), obj.timeReference );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `RawDataF1` to uninitialized state
void reset( event::RawDataF1 & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::RawDataF1> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// Representation of a hit on F1-based detector entity
///
/// **GETTER**:
/// * `rawData.sourceID` \-- DAQ channel source ID
///   (propagated from `rawData:RawDataF1`, returns `sourceID`)
/// * `rawData.portID` \-- DAQ port ID
///   (propagated from `rawData:RawDataF1`, returns `portID`)
/// * `rawData.channel` \-- DAQ detector channel number
///   (propagated from `rawData:RawDataF1`, returns `channel`)
/// * `rawData.channelPosition` \-- channel position
///   (propagated from `rawData:RawDataF1`, returns `channelPosition`)
/// * `rawData.time` \-- Time associated with the channel
///   (propagated from `rawData:RawDataF1`, returns `time`)
/// * `rawData.timeUnit` \-- Time unit
///   (propagated from `rawData:RawDataF1`, returns `timeUnit`)
/// * `rawData.timeDecoded` \-- time in ns wrt trigger time
///   (propagated from `rawData:RawDataF1`, returns `timeDecoded`)
/// * `rawData.timeReference` \-- ...
///   (propagated from `rawData:RawDataF1`, returns `timeReference`)
/// * `hitPosition` \-- Reconstructed position of hit
struct F1Hit {
    /// Raw data as was decoded from DAQ for F1 chip
    mem::Ref<RawDataF1> rawData;
    
    /// Reconstructed position of hit
    double hitPosition;
    
    /// A ctr, needs the pool allocator instance to be bound with
    F1Hit( LocalMemory & );
};  // struct F1Hit

#define M_for_every_F1Hit_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<F1Hit> {
    static constexpr char typeName[] = "F1Hit";
    /// Callback function type, receiving an instance of type `F1Hit` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const F1Hit & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of F1Hit instances
    typedef DetID NaturalKey;
    

    template<typename CallableT, typename ObjectT=F1Hit>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "rawData"), obj.rawData );
        callable( CallableTraits<CallableT>::attr_id(1, "hitPosition"), obj.hitPosition );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `F1Hit` to uninitialized state
void reset( event::F1Hit & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::F1Hit> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist




}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_f1_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
