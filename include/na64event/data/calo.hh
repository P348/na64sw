#pragma once

/**\file
 * \brief Declarations for calorimetry data
 * 
 * The calorimeter assembly energy deposition sum. May be not unique on event
 * to facilitate pile-up separation.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/detectorID.hh"
#include "na64event/data/sadc.hh"



namespace na64dp {

namespace event {
/// \brief Represents a group of SADC hits in the calorimeter
/// 
/// May imply different hit grouping strategies. Associated hits may be
/// considered partially (for instance, if time cluster label is set for the
/// this `CaloHit` instamce, probably only eponymous pulses were considered
/// in the hit).
///
/// **GETTER**:
/// * getter for field `CaloHit::hits` **disabled**
/// * `timeClusterLabel` \-- Time cluster label, may be not set
/// * `eDep` \-- Energy deposition sum, MeV
/// * `eDepError` \-- Estimated energy deposition's error
/// * `time` \-- Mean time
/// * `timeStddev` \-- Time standard deviation (of hits)
/// * `nHits` \-- Returns number of SADC hits
struct CaloHit {
    /// Elementary hits summed up into this calo hit
    event::Map<DetID, SADCHit, MapTag::sparse_ambig> hits;
    
    /// Time cluster label, may be not set
    int timeClusterLabel;
    
    /// Energy deposition sum, MeV
    double eDep;
    
    /// Estimated energy deposition's error
    double eDepError;
    
    /// Mean time
    double time;
    
    /// Time standard deviation (of hits)
    double timeStddev;
    
    /// A ctr, needs the pool allocator instance to be bound with
    CaloHit( LocalMemory & );
};  // struct CaloHit

#define M_for_every_CaloHit_collection_attribute( m, ... ) \
    m(hits, DetID, SADCHit, __VA_RGS__) \
    /* ... */
template<>
struct Traits<CaloHit> {
    static constexpr char typeName[] = "CaloHit";
    /// Callback function type, receiving an instance of type `CaloHit` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const CaloHit & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of CaloHit instances
    typedef StationKey NaturalKey;
    

    template<typename CallableT, typename ObjectT=CaloHit>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "hits"), obj.hits );
        callable( CallableTraits<CallableT>::attr_id(1, "timeClusterLabel"), obj.timeClusterLabel );
        callable( CallableTraits<CallableT>::attr_id(2, "eDep"), obj.eDep );
        callable( CallableTraits<CallableT>::attr_id(3, "eDepError"), obj.eDepError );
        callable( CallableTraits<CallableT>::attr_id(4, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(5, "timeStddev"), obj.timeStddev );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `CaloHit` to uninitialized state
void reset( event::CaloHit & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::CaloHit> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist


template<>
struct Association<event::CaloHit, event::SADCHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `SADCHit` instances with
    /// instance `CaloHit`.
    typedef event::Map<DetID, SADCHit, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `SADCHit` instances associated
    /// with `CaloHit` by attribute `hits`.
    static Collection & map(event::CaloHit & parent) { return parent.hits; }
    /// Remove instance of `SADCHit` from (main) collection
    /// associated it with `CaloHit` by attribute `hits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};




}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_calo_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
