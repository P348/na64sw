#pragma once

/**\file
 * \brief Declarations for hits data on SADC managed detectors.
 * 
 * The NA64/NA58 standard sampling ADC chip (SADC) performs sampling
 * analog-to-digital conversion of many detectors signals. Once triggered, it
 * measures the voltage on its input 16 times with time interval of 25ns.
 * In NA64 DAQ a pair of chips are always enabled in parallel at a single
 * channel with phase shift of ~12.5ns providing 32 samples with 12.5ns
 * interval. Each chip has its own zero, but measurement scale is expected
 * to be the same for both.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/detectorID.hh"



namespace na64dp {

namespace event {
/// SADC waveform fitting parameters for Moyal function
///
/// **GETTER**:
/// * `mu` \-- Centers of peaks
/// * `sigma` \-- Widths of peaks
/// * `S` \-- Scaling factor
/// * getter for field `MoyalSADCWfFitParameters::interval` **disabled**
struct MoyalSADCWfFitParameters {
    /// Centers of peaks
    float mu;
    
    /// Widths of peaks
    float sigma;
    
    /// Scaling factor
    float S;
    
    /// Validity interval of this Moyal fit
    float interval[2];
    
    /// Default ctr (creates uninitialized instance of MoyalSADCWfFitParameters.
    MoyalSADCWfFitParameters(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of MoyalSADCWfFitParameters.
    /// Needed to simplify compatibility with template code.
    MoyalSADCWfFitParameters( LocalMemory & ) {}
    
};  // struct MoyalSADCWfFitParameters

#define M_for_every_MoyalSADCWfFitParameters_collection_attribute( m, ... ) \
    m(interval, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<MoyalSADCWfFitParameters> {
    static constexpr char typeName[] = "MoyalSADCWfFitParameters";
    /// Callback function type, receiving an instance of type `MoyalSADCWfFitParameters` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const MoyalSADCWfFitParameters & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=MoyalSADCWfFitParameters>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "mu"), obj.mu );
        callable( CallableTraits<CallableT>::attr_id(1, "sigma"), obj.sigma );
        callable( CallableTraits<CallableT>::attr_id(2, "S"), obj.S );
        callable( CallableTraits<CallableT>::attr_id(3, "interval"), obj.interval );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `MoyalSADCWfFitParameters` to uninitialized state
void reset( event::MoyalSADCWfFitParameters & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::MoyalSADCWfFitParameters> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// Results/estimations of (pre-)fitting peak(s) with Gaussian PDF
///
/// **GETTER**:
/// * `mu` \-- Centers of peaks
/// * `sigma` \-- Widths of peaks
/// * `S` \-- Scaling factor
/// * getter for field `GaussianSADCFitParameters::interval` **disabled**
struct GaussianSADCFitParameters {
    /// Centers of peaks
    float mu;
    
    /// Widths of peaks
    float sigma;
    
    /// Scaling factor
    float S;
    
    /// Validity interval of this Gaussian (pre-)fit
    float interval[2];
    
    /// Default ctr (creates uninitialized instance of GaussianSADCFitParameters.
    GaussianSADCFitParameters(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of GaussianSADCFitParameters.
    /// Needed to simplify compatibility with template code.
    GaussianSADCFitParameters( LocalMemory & ) {}
    
};  // struct GaussianSADCFitParameters

#define M_for_every_GaussianSADCFitParameters_collection_attribute( m, ... ) \
    m(interval, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<GaussianSADCFitParameters> {
    static constexpr char typeName[] = "GaussianSADCFitParameters";
    /// Callback function type, receiving an instance of type `GaussianSADCFitParameters` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const GaussianSADCFitParameters & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=GaussianSADCFitParameters>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "mu"), obj.mu );
        callable( CallableTraits<CallableT>::attr_id(1, "sigma"), obj.sigma );
        callable( CallableTraits<CallableT>::attr_id(2, "S"), obj.S );
        callable( CallableTraits<CallableT>::attr_id(3, "interval"), obj.interval );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `GaussianSADCFitParameters` to uninitialized state
void reset( event::GaussianSADCFitParameters & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::GaussianSADCFitParameters> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem


namespace event {
/// Discrete fourier transform of SADC signal
///
/// **GETTER**:
/// * getter for field `MSADCDFFTCoefficients::real` **disabled**
/// * getter for field `MSADCDFFTCoefficients::imag` **disabled**
struct MSADCDFFTCoefficients {
    /// Real part of signal Fourier transform coefficients
    double real[16];
    
    /// Imag. part of signal Fourier transform coefficients, 1st elem is reserved
    double imag[16];
    
    /// Default ctr (creates uninitialized instance of MSADCDFFTCoefficients.
    MSADCDFFTCoefficients(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of MSADCDFFTCoefficients.
    /// Needed to simplify compatibility with template code.
    MSADCDFFTCoefficients( LocalMemory & ) {}
    
};  // struct MSADCDFFTCoefficients

#define M_for_every_MSADCDFFTCoefficients_collection_attribute( m, ... ) \
    m(real, , double, __VA_RGS__) \
    m(imag, , double, __VA_RGS__) \
    /* ... */
template<>
struct Traits<MSADCDFFTCoefficients> {
    static constexpr char typeName[] = "MSADCDFFTCoefficients";
    /// Callback function type, receiving an instance of type `MSADCDFFTCoefficients` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const MSADCDFFTCoefficients & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=MSADCDFFTCoefficients>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "real"), obj.real );
        callable( CallableTraits<CallableT>::attr_id(1, "imag"), obj.imag );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `MSADCDFFTCoefficients` to uninitialized state
void reset( event::MSADCDFFTCoefficients & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::MSADCDFFTCoefficients> {
    static constexpr int id = 2;
};
}  // namespace ::na64dp::mem


namespace event {
/// Maximum of (M)SADC detector waveform
///
/// **GETTER**:
/// * `time` \-- Waveform peak's time estimate
/// * `timeErr` \-- Waveform peak's time estimate error
/// * `timeClusterLabel` \-- Waveform max time cluster label
/// * `amp` \-- Waveform peak's amp/square estimate
/// * `ampErr` \-- Waveform peak's amp/square estimate error
/// * `sumCoarse` \-- Waveform peak's coarse integral estimate (usually, by interpolation)
/// * `sumFine` \-- Waveform peak's fine integral estimate (usually, by fitting with model)
/// * `sumFineErr` \-- Waveform peak's fine integral estimate error
/// * `chi2` \-- Peak fitting chi2
/// * `ndf` \-- Peak fitting NDF
/// * `lastFitStatusCode` \-- Encoded fit status (contextual).
/// * `moyalFit.mu` \-- Centers of peaks
///   (propagated from `moyalFit:MoyalSADCWfFitParameters`, returns `mu`)
/// * `moyalFit.sigma` \-- Widths of peaks
///   (propagated from `moyalFit:MoyalSADCWfFitParameters`, returns `sigma`)
/// * `moyalFit.S` \-- Scaling factor
///   (propagated from `moyalFit:MoyalSADCWfFitParameters`, returns `S`)
/// * `sqRatio` \-- Ratio b/w coarse and fine integral estimates
struct MSADCPeak {
    /// Waveform peak's time estimate
    float time;
    
    /// Waveform peak's time estimate error
    float timeErr;
    
    /// Waveform max time cluster label
    int timeClusterLabel;
    
    /// Waveform peak's amp/square estimate
    float amp;
    
    /// Waveform peak's amp/square estimate error
    float ampErr;
    
    /// Waveform peak's coarse integral estimate (usually, by interpolation)
    float sumCoarse;
    
    /// Waveform peak's fine integral estimate (usually, by fitting with model)
    float sumFine;
    
    /// Waveform peak's fine integral estimate error
    float sumFineErr;
    
    /// Peak fitting chi2
    double chi2;
    
    /// Peak fitting NDF
    int ndf;
    
    /// Encoded fit status (contextual).
    int lastFitStatusCode;
    
    /// Results of peak fitting with Moyal function.
    mem::Ref<MoyalSADCWfFitParameters> moyalFit;
    
    /// A ctr, needs the pool allocator instance to be bound with
    MSADCPeak( LocalMemory & );
};  // struct MSADCPeak

#define M_for_every_MSADCPeak_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<MSADCPeak> {
    static constexpr char typeName[] = "MSADCPeak";
    /// Callback function type, receiving an instance of type `MSADCPeak` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const MSADCPeak & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=MSADCPeak>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(1, "timeErr"), obj.timeErr );
        callable( CallableTraits<CallableT>::attr_id(2, "timeClusterLabel"), obj.timeClusterLabel );
        callable( CallableTraits<CallableT>::attr_id(3, "amp"), obj.amp );
        callable( CallableTraits<CallableT>::attr_id(4, "ampErr"), obj.ampErr );
        callable( CallableTraits<CallableT>::attr_id(5, "sumCoarse"), obj.sumCoarse );
        callable( CallableTraits<CallableT>::attr_id(6, "sumFine"), obj.sumFine );
        callable( CallableTraits<CallableT>::attr_id(7, "sumFineErr"), obj.sumFineErr );
        callable( CallableTraits<CallableT>::attr_id(8, "chi2"), obj.chi2 );
        callable( CallableTraits<CallableT>::attr_id(9, "ndf"), obj.ndf );
        callable( CallableTraits<CallableT>::attr_id(10, "lastFitStatusCode"), obj.lastFitStatusCode );
        callable( CallableTraits<CallableT>::attr_id(11, "moyalFit"), obj.moyalFit );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `MSADCPeak` to uninitialized state
void reset( event::MSADCPeak & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::MSADCPeak> {
    static constexpr int id = 3;
};
}  // namespace ::na64dp::mem


namespace event {
/// Raw SADC waveform and subsidiary related information.
///
/// **GETTER**:
/// * getter for field `RawDataSADC::wave` **disabled**
/// * getter for field `RawDataSADC::pedestals` **disabled**
/// * `sum` \-- Computed sum of the waveform, number of channels
/// * `maxSample` \-- Number of waveform's sample considered as global maximum.
/// * `maxAmp` \-- Maximum of the waveform in raw SADC units.
/// * `maxAmpError` \-- Error of the waveform amplitude max. in raw SADC units.
/// * `canBeZero` \-- Zero probability measure (0-1)
/// * `ledCorr` \-- Correction by LED in use, ratio
/// * `calibCoeff` \-- Calibration coefficient in use, chan/MeV
/// * `ampThreshold` \-- Peak-selection threshold, chan
/// * getter for field `RawDataSADC::maxima` **disabled**
/// * `pedestalEven` \-- Returns even pedestal value (for sample 0, 2, 4 ...)
/// * `pedestalOdd` \-- Returns odd pedestal value (for sample 1, 3, 5 ...)
/// * `chargeVsMaxAmpRatio` \-- Calculates ratio between sum and max. amplitude
struct RawDataSADC {
    /// Array of measured waveform samples
    StdFloat_t wave[32];
    
    /// Discrete Fourer transform results
    mem::Ref<MSADCDFFTCoefficients> fft;
    
    /// ADC amplitude pedestal values (zeroes) for even and odd sampler.
    StdFloat_t pedestals[2];
    
    /// Computed sum of the waveform, number of channels
    StdFloat_t sum;
    
    /// Number of waveform's sample considered as global maximum.
    uint8_t maxSample;
    
    /// Maximum of the waveform in raw SADC units.
    StdFloat_t maxAmp;
    
    /// Error of the waveform amplitude max. in raw SADC units.
    StdFloat_t maxAmpError;
    
    /// Zero probability measure (0-1)
    StdFloat_t canBeZero;
    
    /// Correction by LED in use, ratio
    StdFloat_t ledCorr;
    
    /// Calibration coefficient in use, chan/MeV
    StdFloat_t calibCoeff;
    
    /// Peak-selection threshold, chan
    StdFloat_t ampThreshold;
    
    /// Peaks found in the waveform, used in reconstruction.
    event::Map<int, MSADCPeak, MapTag::ordered_sparse> maxima;
    
    /// A ctr, needs the pool allocator instance to be bound with
    RawDataSADC( LocalMemory & );
};  // struct RawDataSADC

#define M_for_every_RawDataSADC_collection_attribute( m, ... ) \
    m(wave, , StdFloat_t, __VA_RGS__) \
    m(pedestals, , StdFloat_t, __VA_RGS__) \
    m(maxima, int, MSADCPeak, __VA_RGS__) \
    /* ... */
template<>
struct Traits<RawDataSADC> {
    static constexpr char typeName[] = "RawDataSADC";
    /// Callback function type, receiving an instance of type `RawDataSADC` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const RawDataSADC & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=RawDataSADC>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "wave"), obj.wave );
        callable( CallableTraits<CallableT>::attr_id(1, "fft"), obj.fft );
        callable( CallableTraits<CallableT>::attr_id(2, "pedestals"), obj.pedestals );
        callable( CallableTraits<CallableT>::attr_id(3, "sum"), obj.sum );
        callable( CallableTraits<CallableT>::attr_id(4, "maxSample"), obj.maxSample );
        callable( CallableTraits<CallableT>::attr_id(5, "maxAmp"), obj.maxAmp );
        callable( CallableTraits<CallableT>::attr_id(6, "maxAmpError"), obj.maxAmpError );
        callable( CallableTraits<CallableT>::attr_id(7, "canBeZero"), obj.canBeZero );
        callable( CallableTraits<CallableT>::attr_id(8, "ledCorr"), obj.ledCorr );
        callable( CallableTraits<CallableT>::attr_id(9, "calibCoeff"), obj.calibCoeff );
        callable( CallableTraits<CallableT>::attr_id(10, "ampThreshold"), obj.ampThreshold );
        callable( CallableTraits<CallableT>::attr_id(11, "maxima"), obj.maxima );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `RawDataSADC` to uninitialized state
void reset( event::RawDataSADC & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::RawDataSADC> {
    static constexpr int id = 4;
};
}  // namespace ::na64dp::mem


namespace event {
/// Representation of a hit on SADC-based detector entity.
///
/// **GETTER**:
/// * `rawData.sum` \-- Computed sum of the waveform, number of channels
///   (propagated from `rawData:RawDataSADC`, returns `sum`)
/// * `rawData.maxSample` \-- Number of waveform's sample considered as global maximum.
///   (propagated from `rawData:RawDataSADC`, returns `maxSample`)
/// * `rawData.maxAmp` \-- Maximum of the waveform in raw SADC units.
///   (propagated from `rawData:RawDataSADC`, returns `maxAmp`)
/// * `rawData.maxAmpError` \-- Error of the waveform amplitude max. in raw SADC units.
///   (propagated from `rawData:RawDataSADC`, returns `maxAmpError`)
/// * `rawData.canBeZero` \-- Zero probability measure (0-1)
///   (propagated from `rawData:RawDataSADC`, returns `canBeZero`)
/// * `rawData.ledCorr` \-- Correction by LED in use, ratio
///   (propagated from `rawData:RawDataSADC`, returns `ledCorr`)
/// * `rawData.calibCoeff` \-- Calibration coefficient in use, chan/MeV
///   (propagated from `rawData:RawDataSADC`, returns `calibCoeff`)
/// * `rawData.ampThreshold` \-- Peak-selection threshold, chan
///   (propagated from `rawData:RawDataSADC`, returns `ampThreshold`)
/// * `rawData.pedestalEven` \-- Returns even pedestal value (for sample 0, 2, 4 ...)
///   (propagated from custom getter `rawData:RawDataSADC`, returns `pedestalEven`)
/// * `rawData.pedestalOdd` \-- Returns odd pedestal value (for sample 1, 3, 5 ...)
///   (propagated from custom getter `rawData:RawDataSADC`, returns `pedestalOdd`)
/// * `rawData.chargeVsMaxAmpRatio` \-- Calculates ratio between sum and max. amplitude
///   (propagated from custom getter `rawData:RawDataSADC`, returns `chargeVsMaxAmpRatio`)
/// * `eDep` \-- Reconstructed energy deposition, MeV.
/// * `eDepError` \-- Error estimation of reconstructed energy deposition (sigma), MeV
/// * `time` \-- Reconstructed time of hit, nanoseconds
/// * `timeError` \-- Estimateion of reconstructed time error (sigma), nanoseconds
struct SADCHit {
    /// Hit raw data (as got from DDD digit)
    mem::Ref<RawDataSADC> rawData;
    
    /// Reconstructed energy deposition, MeV.
    float eDep;
    
    /// Error estimation of reconstructed energy deposition (sigma), MeV
    float eDepError;
    
    /// Reconstructed time of hit, nanoseconds
    float time;
    
    /// Estimateion of reconstructed time error (sigma), nanoseconds
    float timeError;
    
    /// A ctr, needs the pool allocator instance to be bound with
    SADCHit( LocalMemory & );
};  // struct SADCHit

#define M_for_every_SADCHit_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<SADCHit> {
    static constexpr char typeName[] = "SADCHit";
    /// Callback function type, receiving an instance of type `SADCHit` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const SADCHit & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of SADCHit instances
    typedef DetID NaturalKey;
    

    template<typename CallableT, typename ObjectT=SADCHit>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "rawData"), obj.rawData );
        callable( CallableTraits<CallableT>::attr_id(1, "eDep"), obj.eDep );
        callable( CallableTraits<CallableT>::attr_id(2, "eDepError"), obj.eDepError );
        callable( CallableTraits<CallableT>::attr_id(3, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(4, "timeError"), obj.timeError );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `SADCHit` to uninitialized state
void reset( event::SADCHit & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::SADCHit> {
    static constexpr int id = 5;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist






template<>
struct Association<event::RawDataSADC, event::MSADCPeak> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::ordered_sparse;
    /// Type of (main) association of `MSADCPeak` instances with
    /// instance `RawDataSADC`.
    typedef event::Map<int, MSADCPeak, MapTag::ordered_sparse> Collection;
    /// Returns (main) collection of `MSADCPeak` instances associated
    /// with `RawDataSADC` by attribute `maxima`.
    static Collection & map(event::RawDataSADC & parent) { return parent.maxima; }
    /// Remove instance of `MSADCPeak` from (main) collection
    /// associated it with `RawDataSADC` by attribute `maxima`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};





}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_sadc_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
