#pragma once

/**\file
 * \brief Declarations for hits data on APV managed detectors.
 * 
 * APV chip is analogue demultiplexer generally used to communicate a vast
 * number of analog wires into sampling ADC input. Though the ADC is
 * generally a SADC chip we distinguish between SADC and APV managed detectors
 * following convention of original COMPASS/NA64 DAQ data decoding library.
 * 
 * For reconstruction, individual APV hits are usually joined together within
 * a cluster that then represents a fact of ionazing particle passing through
 * the detector's active medium.
 * 
 * These clusters then used in tracking algorithms to reconstruct particle
 * tracks.
 * 
 * \author Alexey Shevelev
 * \author Renat Dusaev
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/wireID.hh"

namespace na64dp {
namespace event {
/// Strip number type
typedef /*NA64DP_APVStripNo_t*/ uint16_t APVPhysWire_t;
/// Wire number type
typedef /*NA64DP_APVWireNo_t*/ uint16_t APVDAQWire_t;
/// Wires number size type
typedef /*NA64DP_APVNWires_t*/ uint8_t APVNWires_t;
}
}


namespace na64dp {

namespace event {
/// Raw data of APV digit
///
/// **GETTER**:
/// * `wireNo` \-- Triggered wire number (detector's channel)
/// * `chip` \-- chip ID wrt DDD DAQ
/// * `srcID` \-- source ID wrt DDD DAQ
/// * `adcID` \-- adc ID wrt DDD DAQ
/// * `chipChannel` \-- channel number inside chip (before remapping)
/// * `timeTag` \-- Time with the respect to trigger time
/// * getter for field `RawDataAPV::samples` **disabled**
struct RawDataAPV {
    /// Triggered wire number (detector's channel)
    APVDAQWire_t wireNo;
    
    /// chip ID wrt DDD DAQ
    uint16_t chip;
    
    /// source ID wrt DDD DAQ
    uint16_t srcID;
    
    /// adc ID wrt DDD DAQ
    uint16_t adcID;
    
    /// channel number inside chip (before remapping)
    uint16_t chipChannel;
    
    /// Time with the respect to trigger time
    uint32_t timeTag;
    
    /// Measured amplitude samples on a wire
    uint32_t samples[3];
    
    /// Default ctr (creates uninitialized instance of RawDataAPV.
    RawDataAPV(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of RawDataAPV.
    /// Needed to simplify compatibility with template code.
    RawDataAPV( LocalMemory & ) {}
    
};  // struct RawDataAPV

#define M_for_every_RawDataAPV_collection_attribute( m, ... ) \
    m(samples, , uint32_t, __VA_RGS__) \
    /* ... */
template<>
struct Traits<RawDataAPV> {
    static constexpr char typeName[] = "RawDataAPV";
    /// Callback function type, receiving an instance of type `RawDataAPV` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const RawDataAPV & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=RawDataAPV>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "wireNo"), obj.wireNo );
        callable( CallableTraits<CallableT>::attr_id(1, "chip"), obj.chip );
        callable( CallableTraits<CallableT>::attr_id(2, "srcID"), obj.srcID );
        callable( CallableTraits<CallableT>::attr_id(3, "adcID"), obj.adcID );
        callable( CallableTraits<CallableT>::attr_id(4, "chipChannel"), obj.chipChannel );
        callable( CallableTraits<CallableT>::attr_id(5, "timeTag"), obj.timeTag );
        callable( CallableTraits<CallableT>::attr_id(6, "samples"), obj.samples );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `RawDataAPV` to uninitialized state
void reset( event::RawDataAPV & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::RawDataAPV> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// Representation of a hit on APV-based detector entity.
///
/// **GETTER**:
/// * `rawData.wireNo` \-- Triggered wire number (detector's channel)
///   (propagated from `rawData:RawDataAPV`, returns `wireNo`)
/// * `rawData.chip` \-- chip ID wrt DDD DAQ
///   (propagated from `rawData:RawDataAPV`, returns `chip`)
/// * `rawData.srcID` \-- source ID wrt DDD DAQ
///   (propagated from `rawData:RawDataAPV`, returns `srcID`)
/// * `rawData.adcID` \-- adc ID wrt DDD DAQ
///   (propagated from `rawData:RawDataAPV`, returns `adcID`)
/// * `rawData.chipChannel` \-- channel number inside chip (before remapping)
///   (propagated from `rawData:RawDataAPV`, returns `chipChannel`)
/// * `rawData.timeTag` \-- Time with the respect to trigger time
///   (propagated from `rawData:RawDataAPV`, returns `timeTag`)
/// * `maxCharge` \-- Hit max charge wrt APV waveform
/// * `timeRise` \-- Time of rising front
/// * `a02` \-- APV samples amplitude ratio a0/a2
/// * `a12` \-- APV samples amplitude ratio a1/a2
/// * `t02` \-- Tangent values of raising front, at a0 and a2
/// * `t12` \-- Tangent values of raising front, at a1 and a2
/// * `t02sigma` \-- Error estimation of tangent raising front
/// * `t12sigma` \-- Error estimation of tangent raising front
/// * `time` \-- Estimated hit time, [SADC sample #]
/// * `timeError` \-- Error of hit time estimation, [SADC sample #]
struct APVHit {
    /// Hit raw data (as got from DDD digit)
    mem::Ref<RawDataAPV> rawData;
    
    /// Hit max charge wrt APV waveform
    double maxCharge;
    
    /// Time of rising front
    /// \todo seemed to be not used; remove?
    double timeRise;
    
    /// APV samples amplitude ratio a0/a2
    double a02;
    
    /// APV samples amplitude ratio a1/a2
    double a12;
    
    /// Tangent values of raising front, at a0 and a2
    /// \todo More doc on it
    double t02;
    
    /// Tangent values of raising front, at a1 and a2
    /// \todo More doc on it
    double t12;
    
    /// Error estimation of tangent raising front
    /// \todo More doc on it
    double t02sigma;
    
    /// Error estimation of tangent raising front
    /// \todo More doc on it
    double t12sigma;
    
    /// Estimated hit time, [SADC sample #]
    double time;
    
    /// Error of hit time estimation, [SADC sample #]
    double timeError;
    
    /// A ctr, needs the pool allocator instance to be bound with
    APVHit( LocalMemory & );
};  // struct APVHit

#define M_for_every_APVHit_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<APVHit> {
    static constexpr char typeName[] = "APVHit";
    /// Callback function type, receiving an instance of type `APVHit` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const APVHit & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of APVHit instances
    typedef DetID NaturalKey;
    

    template<typename CallableT, typename ObjectT=APVHit>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "rawData"), obj.rawData );
        callable( CallableTraits<CallableT>::attr_id(1, "maxCharge"), obj.maxCharge );
        callable( CallableTraits<CallableT>::attr_id(2, "timeRise"), obj.timeRise );
        callable( CallableTraits<CallableT>::attr_id(3, "a02"), obj.a02 );
        callable( CallableTraits<CallableT>::attr_id(4, "a12"), obj.a12 );
        callable( CallableTraits<CallableT>::attr_id(5, "t02"), obj.t02 );
        callable( CallableTraits<CallableT>::attr_id(6, "t12"), obj.t12 );
        callable( CallableTraits<CallableT>::attr_id(7, "t02sigma"), obj.t02sigma );
        callable( CallableTraits<CallableT>::attr_id(8, "t12sigma"), obj.t12sigma );
        callable( CallableTraits<CallableT>::attr_id(9, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(10, "timeError"), obj.timeError );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `APVHit` to uninitialized state
void reset( event::APVHit & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::APVHit> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem


namespace event {
/// Represents a group of hits on APV-based detector.
///
/// **GETTER**:
/// * getter for field `APVCluster::hits` **disabled**
/// * `position` \-- Coordinate of cluster's center, wire number units
/// * `positionError` \-- Coordinate uncertainty, wire units
/// * `charge` \-- Cluster charge sum (unnormed), sum of all hit's amplitude measurements
/// * getter for field `APVCluster::mcTruePosition` **disabled**
/// * `time` \-- Cluster time
/// * `timeError` \-- Cluster time estimated error
/// * `size` \-- Returns number of hits included in cluster.
/// * `avrgCharge` \-- Average charge value.
struct APVCluster {
    /// Basic APV hits definign the cluster
    event::Map<APVPhysWire_t, APVHit, MapTag::sparse_ambig_ordered> hits;
    
    /// Coordinate of cluster's center, wire number units
    double position;
    
    /// Coordinate uncertainty, wire units
    double positionError;
    
    /// Cluster charge sum (unnormed), sum of all hit's amplitude measurements
    double charge;
    
    /// True position of track intersection
    double mcTruePosition[3];
    
    /// Cluster time
    /// \todo units?
    double time;
    
    /// Cluster time estimated error
    /// \todo units?
    double timeError;
    
    /// A ctr, needs the pool allocator instance to be bound with
    APVCluster( LocalMemory & );
};  // struct APVCluster

#define M_for_every_APVCluster_collection_attribute( m, ... ) \
    m(hits, APVPhysWire_t, APVHit, __VA_RGS__) \
    m(mcTruePosition, , double, __VA_RGS__) \
    /* ... */
template<>
struct Traits<APVCluster> {
    static constexpr char typeName[] = "APVCluster";
    /// Callback function type, receiving an instance of type `APVCluster` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const APVCluster & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of APVCluster instances
    typedef PlaneKey NaturalKey;
    

    template<typename CallableT, typename ObjectT=APVCluster>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "hits"), obj.hits );
        callable( CallableTraits<CallableT>::attr_id(1, "position"), obj.position );
        callable( CallableTraits<CallableT>::attr_id(2, "positionError"), obj.positionError );
        callable( CallableTraits<CallableT>::attr_id(3, "charge"), obj.charge );
        callable( CallableTraits<CallableT>::attr_id(4, "mcTruePosition"), obj.mcTruePosition );
        callable( CallableTraits<CallableT>::attr_id(5, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(6, "timeError"), obj.timeError );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `APVCluster` to uninitialized state
void reset( event::APVCluster & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::APVCluster> {
    static constexpr int id = 2;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist




template<>
struct Association<event::APVCluster, event::APVHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig_ordered;
    /// Type of (main) association of `APVHit` instances with
    /// instance `APVCluster`.
    typedef event::Map<APVPhysWire_t, APVHit, MapTag::sparse_ambig_ordered> Collection;
    /// Returns (main) collection of `APVHit` instances associated
    /// with `APVCluster` by attribute `hits`.
    static Collection & map(event::APVCluster & parent) { return parent.hits; }
    /// Remove instance of `APVHit` from (main) collection
    /// associated it with `APVCluster` by attribute `hits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};




}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_apv_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
