#pragma once

/**\file
 * STRAW detectors are used for tracking in NA64. They are listened by a
 * dedicated NA64-specific DAQ entity (called "NA64TDC").
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1.dev
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"



namespace na64dp {

namespace event {
/// Raw hit data on straw detector hit; managed by dedicated chip
///
/// **GETTER**:
/// * `wireNo` \-- Triggered wire number (physical, decoded)
/// * `time` \-- Time on a wire (internal units)
/// * `timeDecoded` \-- Time in ns with the respect to trigger time
struct StwTDCRawData {
    /// Triggered wire number (physical, decoded)
    uint16_t wireNo;
    
    /// Time on a wire (internal units)
    int16_t time;
    
    /// Time in ns with the respect to trigger time
    double timeDecoded;
    
    /// Default ctr (creates uninitialized instance of StwTDCRawData.
    StwTDCRawData(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of StwTDCRawData.
    /// Needed to simplify compatibility with template code.
    StwTDCRawData( LocalMemory & ) {}
    
};  // struct StwTDCRawData

#define M_for_every_StwTDCRawData_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<StwTDCRawData> {
    static constexpr char typeName[] = "StwTDCRawData";
    /// Callback function type, receiving an instance of type `StwTDCRawData` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const StwTDCRawData & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=StwTDCRawData>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "wireNo"), obj.wireNo );
        callable( CallableTraits<CallableT>::attr_id(1, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(2, "timeDecoded"), obj.timeDecoded );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `StwTDCRawData` to uninitialized state
void reset( event::StwTDCRawData & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::StwTDCRawData> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// MC information for a Straw hit
///
/// **GETTER**:
/// * getter for field `StwMCInfo::entryPoint` **disabled**
/// * getter for field `StwMCInfo::exitPoint` **disabled**
/// * `eDep` \-- energy deposition of incident particle
/// * `trackE` \-- energy of initiating track
/// * `particlePDG` \-- PDG code of initiating particle
/// * `geant4TrackID` \-- Geant4 track ID
/// * `r` \-- distance according to MC geometry
/// * `u` \-- measured (local) coordinate according to MC geometry
struct StwMCInfo {
    /// global coordinates of entry point
    float entryPoint[3];
    
    /// global coordinates of exit point
    float exitPoint[3];
    
    /// energy deposition of incident particle
    double eDep;
    
    /// energy of initiating track
    float trackE;
    
    /// PDG code of initiating particle
    int particlePDG;
    
    /// Geant4 track ID
    int geant4TrackID;
    
    /// distance according to MC geometry
    float r;
    
    /// measured (local) coordinate according to MC geometry
    float u;
    
    /// Default ctr (creates uninitialized instance of StwMCInfo.
    StwMCInfo(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of StwMCInfo.
    /// Needed to simplify compatibility with template code.
    StwMCInfo( LocalMemory & ) {}
    
};  // struct StwMCInfo

#define M_for_every_StwMCInfo_collection_attribute( m, ... ) \
    m(entryPoint, , float, __VA_RGS__) \
    m(exitPoint, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<StwMCInfo> {
    static constexpr char typeName[] = "StwMCInfo";
    /// Callback function type, receiving an instance of type `StwMCInfo` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const StwMCInfo & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=StwMCInfo>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "entryPoint"), obj.entryPoint );
        callable( CallableTraits<CallableT>::attr_id(1, "exitPoint"), obj.exitPoint );
        callable( CallableTraits<CallableT>::attr_id(2, "eDep"), obj.eDep );
        callable( CallableTraits<CallableT>::attr_id(3, "trackE"), obj.trackE );
        callable( CallableTraits<CallableT>::attr_id(4, "particlePDG"), obj.particlePDG );
        callable( CallableTraits<CallableT>::attr_id(5, "geant4TrackID"), obj.geant4TrackID );
        callable( CallableTraits<CallableT>::attr_id(6, "r"), obj.r );
        callable( CallableTraits<CallableT>::attr_id(7, "u"), obj.u );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `StwMCInfo` to uninitialized state
void reset( event::StwMCInfo & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::StwMCInfo> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem


namespace event {
/// Hit on straw detector
///
/// **GETTER**:
/// * `rawData.wireNo` \-- Triggered wire number (physical, decoded)
///   (propagated from `rawData:StwTDCRawData`, returns `wireNo`)
/// * `rawData.time` \-- Time on a wire (internal units)
///   (propagated from `rawData:StwTDCRawData`, returns `time`)
/// * `rawData.timeDecoded` \-- Time in ns with the respect to trigger time
///   (propagated from `rawData:StwTDCRawData`, returns `timeDecoded`)
/// * `distance` \-- estimated by time (ambiguous) distance from the wire (isochrone surface radius)
/// * `distanceError` \-- error of distance estimation
/// * `mcInfo.eDep` \-- energy deposition of incident particle
///   (propagated from `mcInfo:StwMCInfo`, returns `eDep`)
/// * `mcInfo.trackE` \-- energy of initiating track
///   (propagated from `mcInfo:StwMCInfo`, returns `trackE`)
/// * `mcInfo.particlePDG` \-- PDG code of initiating particle
///   (propagated from `mcInfo:StwMCInfo`, returns `particlePDG`)
/// * `mcInfo.geant4TrackID` \-- Geant4 track ID
///   (propagated from `mcInfo:StwMCInfo`, returns `geant4TrackID`)
/// * `mcInfo.r` \-- distance according to MC geometry
///   (propagated from `mcInfo:StwMCInfo`, returns `r`)
/// * `mcInfo.u` \-- measured (local) coordinate according to MC geometry
///   (propagated from `mcInfo:StwMCInfo`, returns `u`)
/// * `correctedTime` \-- Time corrected for T0
struct StwTDCHit {
    /// Raw data of the straw hit
    mem::Ref<StwTDCRawData> rawData;
    
    /// estimated by time (ambiguous) distance from the wire (isochrone surface radius)
    float distance;
    
    /// error of distance estimation
    float distanceError;
    
    /// A "true" MC information
    mem::Ref<StwMCInfo> mcInfo;
    
    /// Time corrected for T0
    StdFloat_t correctedTime;
    
    /// A ctr, needs the pool allocator instance to be bound with
    StwTDCHit( LocalMemory & );
};  // struct StwTDCHit

#define M_for_every_StwTDCHit_collection_attribute( m, ... ) \
    /* ... */
template<>
struct Traits<StwTDCHit> {
    static constexpr char typeName[] = "StwTDCHit";
    /// Callback function type, receiving an instance of type `StwTDCHit` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const StwTDCHit & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of StwTDCHit instances
    typedef PlaneKey NaturalKey;
    

    template<typename CallableT, typename ObjectT=StwTDCHit>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "rawData"), obj.rawData );
        callable( CallableTraits<CallableT>::attr_id(1, "distance"), obj.distance );
        callable( CallableTraits<CallableT>::attr_id(2, "distanceError"), obj.distanceError );
        callable( CallableTraits<CallableT>::attr_id(3, "mcInfo"), obj.mcInfo );
        callable( CallableTraits<CallableT>::attr_id(4, "correctedTime"), obj.correctedTime );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `StwTDCHit` to uninitialized state
void reset( event::StwTDCHit & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::StwTDCHit> {
    static constexpr int id = 2;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist





}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_stwTDC_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
