#pragma once

/**\file
 * A (reconstructed) particle track data declaration.
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/detectorID.hh"
#include "na64event/data/apv.hh"
#include "na64event/data/calo.hh"
#include "na64event/data/f1.hh"
#include "na64event/data/stwTDC.hh"

#include "na64detID/trackID.hh"


namespace na64dp {

namespace event {
/// \brief Additional features for track scores on drift detectors
/// 
/// Aux data for drift-like detectors
///
/// **GETTER**:
/// * `distance` \-- Estimated distance, cm
/// * `distanceError` \-- Error of distance estimation, cm
/// * `maxDistance` \-- max distance (cm) from the wire; typically a wire's stride
/// * getter for field `DriftDetScoreFeatures::wireCoordinates` **disabled**
/// * `sideHitUError` \-- unbiased error on other side of isochrone
/// * `sideHitBError` \-- biased error on other side of isochrone
/// * `sideWeight` \-- tracking weight of other side isochrone
/// * getter for field `DriftDetScoreFeatures::uError` **disabled**
struct DriftDetScoreFeatures {
    /// Estimated distance, cm
    float distance;
    
    /// Error of distance estimation, cm
    float distanceError;
    
    /// max distance (cm) from the wire; typically a wire's stride
    float maxDistance;
    
    /// wire spatial coordinates (two endpoints)
    float wireCoordinates[6];
    
    /// unbiased error on other side of isochrone
    float sideHitUError;
    
    /// biased error on other side of isochrone
    float sideHitBError;
    
    /// tracking weight of other side isochrone
    float sideWeight;
    
    /// residuals wrt drift center (e.g. wire)
    float uError[3];
    
    /// Default ctr (creates uninitialized instance of DriftDetScoreFeatures.
    DriftDetScoreFeatures(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of DriftDetScoreFeatures.
    /// Needed to simplify compatibility with template code.
    DriftDetScoreFeatures( LocalMemory & ) {}
    
};  // struct DriftDetScoreFeatures

#define M_for_every_DriftDetScoreFeatures_collection_attribute( m, ... ) \
    m(wireCoordinates, , float, __VA_RGS__) \
    m(uError, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<DriftDetScoreFeatures> {
    static constexpr char typeName[] = "DriftDetScoreFeatures";
    /// Callback function type, receiving an instance of type `DriftDetScoreFeatures` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const DriftDetScoreFeatures & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=DriftDetScoreFeatures>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "distance"), obj.distance );
        callable( CallableTraits<CallableT>::attr_id(1, "distanceError"), obj.distanceError );
        callable( CallableTraits<CallableT>::attr_id(2, "maxDistance"), obj.maxDistance );
        callable( CallableTraits<CallableT>::attr_id(3, "wireCoordinates"), obj.wireCoordinates );
        callable( CallableTraits<CallableT>::attr_id(4, "sideHitUError"), obj.sideHitUError );
        callable( CallableTraits<CallableT>::attr_id(5, "sideHitBError"), obj.sideHitBError );
        callable( CallableTraits<CallableT>::attr_id(6, "sideWeight"), obj.sideWeight );
        callable( CallableTraits<CallableT>::attr_id(7, "uError"), obj.uError );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `DriftDetScoreFeatures` to uninitialized state
void reset( event::DriftDetScoreFeatures & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::DriftDetScoreFeatures> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// \brief True parameters of the generated track score
///
/// **GETTER**:
/// * getter for field `MCTrueTrackScore::globalPosition` **disabled**
/// * `geant4TrackID` \-- (only for MC) Geant4 track ID for this score
struct MCTrueTrackScore {
    /// Unsmeared true intersection 3D point that generated this score
    float globalPosition[3];
    
    /// (only for MC) Geant4 track ID for this score
    int geant4TrackID;
    
    /// Default ctr (creates uninitialized instance of MCTrueTrackScore.
    MCTrueTrackScore(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of MCTrueTrackScore.
    /// Needed to simplify compatibility with template code.
    MCTrueTrackScore( LocalMemory & ) {}
    
};  // struct MCTrueTrackScore

#define M_for_every_MCTrueTrackScore_collection_attribute( m, ... ) \
    m(globalPosition, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<MCTrueTrackScore> {
    static constexpr char typeName[] = "MCTrueTrackScore";
    /// Callback function type, receiving an instance of type `MCTrueTrackScore` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const MCTrueTrackScore & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=MCTrueTrackScore>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "globalPosition"), obj.globalPosition );
        callable( CallableTraits<CallableT>::attr_id(1, "geant4TrackID"), obj.geant4TrackID );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `MCTrueTrackScore` to uninitialized state
void reset( event::MCTrueTrackScore & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::MCTrueTrackScore> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem


namespace event {
/// \brief A particle hit point representation
/// 
/// _Track score_ is a helper object representing set of ra measurement (or
/// set of raw measurements) bound together to represent a spatial item that
/// can become a subject of track fitting.
/// 
/// Keeps references to raw measurements (hits) together with DRS-normalized
/// coordinates and uncertainties.
///
/// **GETTER**:
/// * getter for field `TrackScore::hitRefs` **disabled**
/// * getter for field `TrackScore::lR` **disabled**
/// * getter for field `TrackScore::gR` **disabled**
/// * getter for field `TrackScore::lRErr` **disabled**
/// * `driftFts.distance` \-- Estimated distance, cm
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `distance`)
/// * `driftFts.distanceError` \-- Error of distance estimation, cm
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `distanceError`)
/// * `driftFts.maxDistance` \-- max distance (cm) from the wire; typically a wire's stride
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `maxDistance`)
/// * `driftFts.sideHitUError` \-- unbiased error on other side of isochrone
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `sideHitUError`)
/// * `driftFts.sideHitBError` \-- biased error on other side of isochrone
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `sideHitBError`)
/// * `driftFts.sideWeight` \-- tracking weight of other side isochrone
///   (propagated from `driftFts:DriftDetScoreFeatures`, returns `sideWeight`)
/// * `time` \-- Hit time, global to event (averaged on hits or provided by MC)
/// * `mcTruth.geant4TrackID` \-- (only for MC) Geant4 track ID for this score
///   (propagated from `mcTruth:MCTrueTrackScore`, returns `geant4TrackID`)
/// * `u` \-- Local U of the measurement
/// * `v` \-- Local V of the measurement
/// * `w` \-- Local W of the measurement
/// * `uE` \-- Local uncertainty by U of the measurement
/// * `vE` \-- Local uncertainty by V of the measurement
/// * `wE` \-- Local uncertainty by W of the measurement
struct TrackScore {
    /// Collection of hits this score is built from
    MapTraits<DetID, std::variant<std::monostate , mem::Ref<APVCluster>, mem::Ref<StwTDCHit>, mem::Ref<CaloHit>, mem::Ref<F1Hit>, mem::Ref<TrackScore> >, MapTag::sparse_ambig, NA64SW_EVMALLOC_STRATEGY>::type hitRefs;
    
    /// Local track (point) coordinates, up to three: u, v, w, normalized to (-0.5/0.5)
    float lR[3];
    
    /// Assumed global coordinates of track point, MRS: x, y, z, [cm]
    float gR[3];
    
    /// Local track uncertainties, up to three: u, v, w, normalized
    float lRErr[3];
    
    /// Supplementary data for drift detectors.
    mem::Ref<DriftDetScoreFeatures> driftFts;
    
    /// Hit time, global to event (averaged on hits or provided by MC)
    float time;
    
    /// Monte-Carlo true values
    mem::Ref<MCTrueTrackScore> mcTruth;
    
    /// A ctr, needs the pool allocator instance to be bound with
    TrackScore( LocalMemory & );
};  // struct TrackScore

#define M_for_every_TrackScore_collection_attribute( m, ... ) \
    m(hitRefs, DetID, {'oneOf': {'apvCluster': {'doc': 'A cluster on the APV detector', 'type': {'cppType': 'mem::Ref<APVCluster>', 'referencedType': 'APVCluster', 'isPOD': False}}, 'stwtdcHit': {'doc': 'An straw hit', 'type': {'cppType': 'mem::Ref<StwTDCHit>', 'referencedType': 'StwTDCHit', 'isPOD': False}}, 'caloHit': {'doc': 'A calorimeter measurement', 'type': {'cppType': 'mem::Ref<CaloHit>', 'referencedType': 'CaloHit', 'isPOD': False}}, 'f1Hit': {'doc': 'Hit provided by F1 chip (typically, BMS)', 'type': {'cppType': 'mem::Ref<F1Hit>', 'referencedType': 'F1Hit', 'isPOD': False}}, 'score': {'doc': 'Another score', 'type': {'cppType': 'mem::Ref<TrackScore>', 'referencedType': 'TrackScore', 'isPOD': False}}}, 'isPOD': False, 'cppType': 'std::variant<std::monostate , mem::Ref<APVCluster>, mem::Ref<StwTDCHit>, mem::Ref<CaloHit>, mem::Ref<F1Hit>, mem::Ref<TrackScore> >'}, __VA_RGS__) \
    m(lR, , float, __VA_RGS__) \
    m(gR, , float, __VA_RGS__) \
    m(lRErr, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<TrackScore> {
    static constexpr char typeName[] = "TrackScore";
    /// Callback function type, receiving an instance of type `TrackScore` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const TrackScore & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of TrackScore instances
    typedef StationKey NaturalKey;
    

    template<typename CallableT, typename ObjectT=TrackScore>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "hitRefs"), obj.hitRefs );
        callable( CallableTraits<CallableT>::attr_id(1, "lR"), obj.lR );
        callable( CallableTraits<CallableT>::attr_id(2, "gR"), obj.gR );
        callable( CallableTraits<CallableT>::attr_id(3, "lRErr"), obj.lRErr );
        callable( CallableTraits<CallableT>::attr_id(4, "driftFts"), obj.driftFts );
        callable( CallableTraits<CallableT>::attr_id(5, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(6, "mcTruth"), obj.mcTruth );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `TrackScore` to uninitialized state
void reset( event::TrackScore & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::TrackScore> {
    static constexpr int id = 2;
};
}  // namespace ::na64dp::mem


namespace event {
/// \brief Track-to-score association object
/// 
/// Association (shimming) class binding track score with certain track object.
/// Maintains set of information related to local fit object: local coordinates,
/// tangents, etc.
///
/// **GETTER**:
/// * `score.time` \-- Hit time, global to event (averaged on hits or provided by MC)
///   (propagated from `score:TrackScore`, returns `time`)
/// * `score.u` \-- Local U of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `u`)
/// * `score.v` \-- Local V of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `v`)
/// * `score.w` \-- Local W of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `w`)
/// * `score.uE` \-- Local uncertainty by U of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `uE`)
/// * `score.vE` \-- Local uncertainty by V of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `vE`)
/// * `score.wE` \-- Local uncertainty by W of the measurement
///   (propagated from custom getter `score:TrackScore`, returns `wE`)
/// * getter for field `ScoreFitInfo::lR` **disabled**
/// * getter for field `ScoreFitInfo::lRUErr` **disabled**
/// * getter for field `ScoreFitInfo::lRBErr` **disabled**
/// * getter for field `ScoreFitInfo::lTan` **disabled**
/// * `qop` \-- charge over momentum, [GeV/c]
/// * `time` \-- estimated fit time, [TODO: GenFit2 RK units?]
/// * `weight` \-- weight for track fit, if appliable
/// * `biasedResidualU` \-- Biased residual by U, cm
/// * `biasedResidualV` \-- Biased residual by V, cm
/// * `biasedResidualW` \-- Biased residual by W, cm
/// * `unbiasedResidualU` \-- Unbiased residual by U, cm
/// * `unbiasedResidualV` \-- Unbiased residual by V, cm
/// * `unbiasedResidualW` \-- Unbiased residual by W, cm
/// * `tanU` \-- First DRS tangent of track at intersection point
/// * `tanV` \-- Second DRS tangent of track at intersection point
/// * `tanW` \-- Third DRS tangent of track at intersection point
/// * `u` \-- First local coordinate, cm
/// * `v` \-- Second local coordinate, cm
/// * `w` \-- Third local coordinate, cm
struct ScoreFitInfo {
    /// reference to subject track score object
    mem::Ref<TrackScore> score;
    
    /// Local track fit coordinates, metric
    float lR[3];
    
    /// Unbiased errors of the local coordinates, DRS, up to three: u, v, w, normalized
    float lRUErr[3];
    
    /// Biased errors of the local coordinates, DRS, up to three: u, v, w, normalized
    float lRBErr[3];
    
    /// track tangent(s) at intersection point, DRS
    float lTan[3];
    
    /// charge over momentum, [GeV/c]
    float qop;
    
    /// estimated fit time, [TODO: GenFit2 RK units?]
    float time;
    
    /// weight for track fit, if appliable
    float weight;
    
    /// A ctr, needs the pool allocator instance to be bound with
    ScoreFitInfo( LocalMemory & );
};  // struct ScoreFitInfo

#define M_for_every_ScoreFitInfo_collection_attribute( m, ... ) \
    m(lR, , float, __VA_RGS__) \
    m(lRUErr, , float, __VA_RGS__) \
    m(lRBErr, , float, __VA_RGS__) \
    m(lTan, , float, __VA_RGS__) \
    /* ... */
template<>
struct Traits<ScoreFitInfo> {
    static constexpr char typeName[] = "ScoreFitInfo";
    /// Callback function type, receiving an instance of type `ScoreFitInfo` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const ScoreFitInfo & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=ScoreFitInfo>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "score"), obj.score );
        callable( CallableTraits<CallableT>::attr_id(1, "lR"), obj.lR );
        callable( CallableTraits<CallableT>::attr_id(2, "lRUErr"), obj.lRUErr );
        callable( CallableTraits<CallableT>::attr_id(3, "lRBErr"), obj.lRBErr );
        callable( CallableTraits<CallableT>::attr_id(4, "lTan"), obj.lTan );
        callable( CallableTraits<CallableT>::attr_id(5, "qop"), obj.qop );
        callable( CallableTraits<CallableT>::attr_id(6, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(7, "weight"), obj.weight );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `ScoreFitInfo` to uninitialized state
void reset( event::ScoreFitInfo & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::ScoreFitInfo> {
    static constexpr int id = 3;
};
}  // namespace ::na64dp::mem


namespace event {
/// General information on track fitting results
///
/// **GETTER**:
/// * getter for field `TrackFitInfo::positionSeed` **disabled**
/// * getter for field `TrackFitInfo::momentumSeed` **disabled**
/// * getter for field `TrackFitInfo::covarSeed` **disabled**
/// * `chi2` \-- chi-squared value of track fit
/// * `ndf` \-- Number of degrees of freedom of track fit
/// * `pval` \-- p-value of track fitting
/// * `chi2ndf` \-- chi^2/NDF value
struct TrackFitInfo {
    /// Initial position of the particle
    double positionSeed[3];
    
    /// Initial momentum of the particle motion (direction vector)
    double momentumSeed[3];
    
    /// Initial covariance matrix
    double covarSeed[6];
    
    /// chi-squared value of track fit
    float chi2;
    
    /// Number of degrees of freedom of track fit
    int32_t ndf;
    
    /// p-value of track fitting
    float pval;
    
    /// Default ctr (creates uninitialized instance of TrackFitInfo.
    TrackFitInfo(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of TrackFitInfo.
    /// Needed to simplify compatibility with template code.
    TrackFitInfo( LocalMemory & ) {}
    
};  // struct TrackFitInfo

#define M_for_every_TrackFitInfo_collection_attribute( m, ... ) \
    m(positionSeed, , double, __VA_RGS__) \
    m(momentumSeed, , double, __VA_RGS__) \
    m(covarSeed, , double, __VA_RGS__) \
    /* ... */
template<>
struct Traits<TrackFitInfo> {
    static constexpr char typeName[] = "TrackFitInfo";
    /// Callback function type, receiving an instance of type `TrackFitInfo` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const TrackFitInfo & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=TrackFitInfo>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "positionSeed"), obj.positionSeed );
        callable( CallableTraits<CallableT>::attr_id(1, "momentumSeed"), obj.momentumSeed );
        callable( CallableTraits<CallableT>::attr_id(2, "covarSeed"), obj.covarSeed );
        callable( CallableTraits<CallableT>::attr_id(3, "chi2"), obj.chi2 );
        callable( CallableTraits<CallableT>::attr_id(4, "ndf"), obj.ndf );
        callable( CallableTraits<CallableT>::attr_id(5, "pval"), obj.pval );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `TrackFitInfo` to uninitialized state
void reset( event::TrackFitInfo & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::TrackFitInfo> {
    static constexpr int id = 4;
};
}  // namespace ::na64dp::mem


namespace event {
/// Particle track representation
///
/// **GETTER**:
/// * getter for field `Track::scores` **disabled**
/// * `momentum` \-- Initial momentum of the track
/// * `pdg` \-- Particle type PDG code
/// * `fitInfo.chi2` \-- chi-squared value of track fit
///   (propagated from `fitInfo:TrackFitInfo`, returns `chi2`)
/// * `fitInfo.ndf` \-- Number of degrees of freedom of track fit
///   (propagated from `fitInfo:TrackFitInfo`, returns `ndf`)
/// * `fitInfo.pval` \-- p-value of track fitting
///   (propagated from `fitInfo:TrackFitInfo`, returns `pval`)
/// * `fitInfo.chi2ndf` \-- chi^2/NDF value
///   (propagated from custom getter `fitInfo:TrackFitInfo`, returns `chi2ndf`)
/// * `zonePattern` \-- Bitmask of zones for the track
/// * `nscores` \-- Number of scores in a track.
struct Track {
    /// Scores defining the track
    event::Map<DetID, ScoreFitInfo, MapTag::sparse_ambig> scores;
    
    /// Initial momentum of the track
    double momentum;
    
    /// Particle type PDG code
    int pdg;
    
    /// General information of track fitting results
    mem::Ref<TrackFitInfo> fitInfo;
    
    /// Bitmask of zones for the track
    uint32_t zonePattern;
    
    /// A ctr, needs the pool allocator instance to be bound with
    Track( LocalMemory & );
};  // struct Track

#define M_for_every_Track_collection_attribute( m, ... ) \
    m(scores, DetID, ScoreFitInfo, __VA_RGS__) \
    /* ... */
template<>
struct Traits<Track> {
    static constexpr char typeName[] = "Track";
    /// Callback function type, receiving an instance of type `Track` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const Track & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of Track instances
    typedef size_t NaturalKey;
    

    template<typename CallableT, typename ObjectT=Track>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "scores"), obj.scores );
        callable( CallableTraits<CallableT>::attr_id(1, "momentum"), obj.momentum );
        callable( CallableTraits<CallableT>::attr_id(2, "pdg"), obj.pdg );
        callable( CallableTraits<CallableT>::attr_id(3, "fitInfo"), obj.fitInfo );
        callable( CallableTraits<CallableT>::attr_id(4, "zonePattern"), obj.zonePattern );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `Track` to uninitialized state
void reset( event::Track & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::Track> {
    static constexpr int id = 5;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist







template<>
struct Association<event::Track, event::ScoreFitInfo> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `ScoreFitInfo` instances with
    /// instance `Track`.
    typedef event::Map<DetID, ScoreFitInfo, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `ScoreFitInfo` instances associated
    /// with `Track` by attribute `scores`.
    static Collection & map(event::Track & parent) { return parent.scores; }
    /// Remove instance of `ScoreFitInfo` from (main) collection
    /// associated it with `Track` by attribute `scores`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};




}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_track_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
