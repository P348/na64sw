#pragma once

/**\file
 * \brief Main event structure declaration
 * 
 * The event structure is the principal declaration affecting every handler
 * within the pipeline. Handlers take and modify the data from/within an event
 * instance, by accessing its members.
 * 
 * Refer to :ref:`event structure` for insights of
 * how to modify the events composition.
 *
 * \warning This is a file generated for event's data structure.
 * \note Generated at 08/03/2025 15:22:46 with template utils/evstruct/templates/cpp/decl.hh
 * \version 0.1
 */

#include <stdexcept>
#include <variant>
#include "na64event/mem-mapping.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"
#include "na64event/stream.hh"
#include "na64detID/detectorID.hh"
#include "na64util/na64/event-id.hh"
#include "na64event/data/apv.hh"
#include "na64event/data/calo.hh"
#include "na64event/data/f1.hh"
#include "na64event/data/sadc.hh"
#include "na64event/data/stwTDC.hh"
#include "na64event/data/track.hh"



namespace na64dp {

namespace event {
/// \brief Singular event field for Monte-Carlo true values.
/// 
/// These structure was copied from "official reconstruction" and corresponds to
/// field found in MK MC package.
///
/// **GETTER**:
/// * `weight` \-- A "statistical weight" of the event
/// * getter for field `MCTruthInfo::beamPosition` **disabled**
/// * getter for field `MCTruthInfo::beamMomentum` **disabled**
/// * `beamE0` \-- initializing particle initial full E (TODO: redundant?)
/// * getter for field `MCTruthInfo::ecalEntryPoisition` **disabled**
/// * `beamPDG` \-- PDG code of initializing particle
struct MCTruthInfo {
    /// A "statistical weight" of the event
    double weight;
    
    /// initializing particle initial position
    double beamPosition[3];
    
    /// initializing particle initial momentum
    double beamMomentum[3];
    
    /// initializing particle initial full E (TODO: redundant?)
    double beamE0;
    
    /// initializing particle initial full E (TODO: redundant?)
    double ecalEntryPoisition[3];
    
    /// PDG code of initializing particle
    int beamPDG;
    
    /// Default ctr (creates uninitialized instance of MCTruthInfo.
    MCTruthInfo(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of MCTruthInfo.
    /// Needed to simplify compatibility with template code.
    MCTruthInfo( LocalMemory & ) {}
    
};  // struct MCTruthInfo

#define M_for_every_MCTruthInfo_collection_attribute( m, ... ) \
    m(beamPosition, , double, __VA_RGS__) \
    m(beamMomentum, , double, __VA_RGS__) \
    m(ecalEntryPoisition, , double, __VA_RGS__) \
    /* ... */
template<>
struct Traits<MCTruthInfo> {
    static constexpr char typeName[] = "MCTruthInfo";
    /// Callback function type, receiving an instance of type `MCTruthInfo` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const MCTruthInfo & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    

    template<typename CallableT, typename ObjectT=MCTruthInfo>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "weight"), obj.weight );
        callable( CallableTraits<CallableT>::attr_id(1, "beamPosition"), obj.beamPosition );
        callable( CallableTraits<CallableT>::attr_id(2, "beamMomentum"), obj.beamMomentum );
        callable( CallableTraits<CallableT>::attr_id(3, "beamE0"), obj.beamE0 );
        callable( CallableTraits<CallableT>::attr_id(4, "ecalEntryPoisition"), obj.ecalEntryPoisition );
        callable( CallableTraits<CallableT>::attr_id(5, "beamPDG"), obj.beamPDG );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `MCTruthInfo` to uninitialized state
void reset( event::MCTruthInfo & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::MCTruthInfo> {
    static constexpr int id = 0;
};
}  // namespace ::na64dp::mem


namespace event {
/// \brief Production and/or decay vertex
/// 
/// This vertex usually corresponds to various Dark Matter production spatial
/// point (MC-modelled or reconstructed).
///
/// **GETTER**:
/// * `E0` \-- Initializing particle energy before DM producton [GeV]
/// * `E` \-- Initializing paritcle energy after DM production [GeV]
/// * getter for field `Vertex::position` **disabled**
/// * getter for field `Vertex::decayPosition` **disabled**
/// * getter for field `Vertex::lvEm` **disabled**
/// * getter for field `Vertex::lvEp` **disabled**
/// * `initPDG` \-- PDG code of the initializing particle (MC typically)
/// * `DMTRID1` \-- (unknown MK's MC datum) \todo
/// * `DMTRID2` \-- (unknown MK's MC datum) \todo
struct Vertex {
    /// Initializing particle energy before DM producton [GeV]
    double E0;
    
    /// Initializing paritcle energy after DM production [GeV]
    double E;
    
    /// DM production vertex
    double position[3];
    
    /// DM decay position
    double decayPosition[3];
    
    /// Lorentz vector of electron in DM decay, e mas is 4th
    double lvEm[3];
    
    /// Lorentz vector of positron in DM decay, e mas is 4th
    double lvEp[3];
    
    /// PDG code of the initializing particle (MC typically)
    int initPDG;
    
    /// (unknown MK's MC datum) \todo
    int DMTRID1;
    
    /// (unknown MK's MC datum) \todo
    int DMTRID2;
    
    /// Default ctr (creates uninitialized instance of Vertex.
    Vertex(){}
    /// A ctr, ignores memory arg (creates uninitialized instance of Vertex.
    /// Needed to simplify compatibility with template code.
    Vertex( LocalMemory & ) {}
    
};  // struct Vertex

#define M_for_every_Vertex_collection_attribute( m, ... ) \
    m(position, , double, __VA_RGS__) \
    m(decayPosition, , double, __VA_RGS__) \
    m(lvEm, , double, __VA_RGS__) \
    m(lvEp, , double, __VA_RGS__) \
    /* ... */
template<>
struct Traits<Vertex> {
    static constexpr char typeName[] = "Vertex";
    /// Callback function type, receiving an instance of type `Vertex` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const Vertex & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of Vertex instances
    typedef size_t NaturalKey;
    

    template<typename CallableT, typename ObjectT=Vertex>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "E0"), obj.E0 );
        callable( CallableTraits<CallableT>::attr_id(1, "E"), obj.E );
        callable( CallableTraits<CallableT>::attr_id(2, "position"), obj.position );
        callable( CallableTraits<CallableT>::attr_id(3, "decayPosition"), obj.decayPosition );
        callable( CallableTraits<CallableT>::attr_id(4, "lvEm"), obj.lvEm );
        callable( CallableTraits<CallableT>::attr_id(5, "lvEp"), obj.lvEp );
        callable( CallableTraits<CallableT>::attr_id(6, "initPDG"), obj.initPDG );
        callable( CallableTraits<CallableT>::attr_id(7, "DMTRID1"), obj.DMTRID1 );
        callable( CallableTraits<CallableT>::attr_id(8, "DMTRID2"), obj.DMTRID2 );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `Vertex` to uninitialized state
void reset( event::Vertex & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::Vertex> {
    static constexpr int id = 1;
};
}  // namespace ::na64dp::mem


namespace event {
/// This struct maps hit instances over various identification schemes for
/// faster access. Instance of this structure is the major subject of
/// modification and data retrieval for all the handlers composing the pipeline.
/// 
/// Refer to :ref:`event structure` for reference on modifying the content of
/// this struct.
/// 
/// Type is not copyable since creation of copies must require
/// explicitly defined hit (re-)creation.
///
/// **GETTER**:
/// * `id` \-- Event's unique identifier.
/// * `trigger` \-- Trigger code of this event
/// * `evType` \-- Event type code
/// * getter for field `Event::time` **disabled**
/// * `masterTime` \-- Relative event master time, [ns]
/// * `mcTruth.weight` \-- A "statistical weight" of the event
///   (propagated from `mcTruth:MCTruthInfo`, returns `weight`)
/// * `mcTruth.beamE0` \-- initializing particle initial full E (TODO: redundant?)
///   (propagated from `mcTruth:MCTruthInfo`, returns `beamE0`)
/// * `mcTruth.beamPDG` \-- PDG code of initializing particle
///   (propagated from `mcTruth:MCTruthInfo`, returns `beamPDG`)
/// * getter for field `Event::sadcHits` **disabled**
/// * getter for field `Event::apvHits` **disabled**
/// * getter for field `Event::stwtdcHits` **disabled**
/// * getter for field `Event::f1Hits` **disabled**
/// * getter for field `Event::caloHits` **disabled**
/// * getter for field `Event::apvClusters` **disabled**
/// * getter for field `Event::trackScores` **disabled**
/// * getter for field `Event::tracks` **disabled**
/// * getter for field `Event::vertices` **disabled**
struct Event {
    /// Event's unique identifier.
    EventID id;
    
    /// Trigger code of this event
    uint16_t trigger;
    
    /// Event type code
    uint16_t evType;
    
    /// Event time represented by pair: seconds since epoch, usec
    std::pair<time_t, uint32_t> time;
    
    /// Relative event master time, [ns]
    double masterTime;
    
    /// "True" MC data
    mem::Ref<MCTruthInfo> mcTruth;
    
    /// SADC-based detectors hits (ECAL, WCAL, HCAL, VETO, etc.)
    event::Map<DetID, SADCHit, MapTag::sparse> sadcHits;
    
    /// APV-based detectors hits (MuMegas, GEMs)
    event::Map<DetID, APVHit, MapTag::sparse> apvHits;
    
    /// NA64TDC-based detectors hits (STRAW)
    event::Map<DetID, StwTDCHit, MapTag::sparse> stwtdcHits;
    
    /// F1-based detectors hits (BMS, etc.)
    event::Map<DetID, F1Hit, MapTag::sparse_ambig> f1Hits;
    
    /// Energy deposition in calorimetric detectors (e.g. ECAL, WCAL, HCAL, etc)
    event::Map<DetID, CaloHit, MapTag::sparse_ambig> caloHits;
    
    /// \brief 1D clusters (contains references to the APV hits)
    ///       
    /// Cluster contains references to hits located in neighbouring group of
    /// wires.
    /// Clusters here are indexed by "incomplete" detector ID that contains only
    /// chip ID, kin ID, number of station and projection ID (no wire number).
    event::Map<PlaneKey, APVCluster, MapTag::sparse_ambig> apvClusters;
    
    /// Track point candidates collection
    event::Map<DetID, TrackScore, MapTag::sparse_ambig> trackScores;
    
    /// Collection of reconstructed tracks
    event::Map<TrackID, Track, MapTag::sparse> tracks;
    
    /// Collection of vertices \-- particle's birth/decay spatial points
    event::Map<size_t, Vertex, MapTag::ordered> vertices;
    
    /// A ctr, needs the pool allocator instance to be bound with
    Event( LocalMemory & );
};  // struct Event

#define M_for_every_Event_collection_attribute( m, ... ) \
    m(sadcHits, DetID, SADCHit, __VA_RGS__) \
    m(apvHits, DetID, APVHit, __VA_RGS__) \
    m(stwtdcHits, DetID, StwTDCHit, __VA_RGS__) \
    m(f1Hits, DetID, F1Hit, __VA_RGS__) \
    m(caloHits, DetID, CaloHit, __VA_RGS__) \
    m(apvClusters, PlaneKey, APVCluster, __VA_RGS__) \
    m(trackScores, DetID, TrackScore, __VA_RGS__) \
    m(tracks, TrackID, Track, __VA_RGS__) \
    m(vertices, size_t, Vertex, __VA_RGS__) \
    /* ... */
template<>
struct Traits<Event> {
    static constexpr char typeName[] = "Event";
    /// Callback function type, receiving an instance of type `Event` and
    /// returning certain `double` value.
    typedef StdFloat_t (*Getter)(const Event & obj);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter> > Getters;

    /// A map of (standard) getters; key is "name:str" of the getter, entry pair
    /// is ("description:str", "callback:func")
    static const Getters getters;

    
    /// Natural key type to index collection of Event instances
    typedef EventID NaturalKey;
    

    template<typename CallableT, typename ObjectT=Event>
    static void for_each_attr( CallableT callable, ObjectT & obj ) {
        callable( CallableTraits<CallableT>::attr_id(0, "id"), obj.id );
        callable( CallableTraits<CallableT>::attr_id(1, "trigger"), obj.trigger );
        callable( CallableTraits<CallableT>::attr_id(2, "evType"), obj.evType );
        callable( CallableTraits<CallableT>::attr_id(3, "time"), obj.time );
        callable( CallableTraits<CallableT>::attr_id(4, "masterTime"), obj.masterTime );
        callable( CallableTraits<CallableT>::attr_id(5, "mcTruth"), obj.mcTruth );
        callable( CallableTraits<CallableT>::attr_id(6, "sadcHits"), obj.sadcHits );
        callable( CallableTraits<CallableT>::attr_id(7, "apvHits"), obj.apvHits );
        callable( CallableTraits<CallableT>::attr_id(8, "stwtdcHits"), obj.stwtdcHits );
        callable( CallableTraits<CallableT>::attr_id(9, "f1Hits"), obj.f1Hits );
        callable( CallableTraits<CallableT>::attr_id(10, "caloHits"), obj.caloHits );
        callable( CallableTraits<CallableT>::attr_id(11, "apvClusters"), obj.apvClusters );
        callable( CallableTraits<CallableT>::attr_id(12, "trackScores"), obj.trackScores );
        callable( CallableTraits<CallableT>::attr_id(13, "tracks"), obj.tracks );
        callable( CallableTraits<CallableT>::attr_id(14, "vertices"), obj.vertices );
        
    }
};

} // namespace ::na64dp::event

namespace util {
/// Sets instance of `Event` to uninitialized state
void reset( event::Event & );
}  // namespace ::na64dp::util


namespace mem {
template<> struct PoolTraits<event::Event> {
    static constexpr int id = 2;
};
}  // namespace ::na64dp::mem



namespace event {

template<typename fromT, typename toT> struct Association;  // default def does not exist




template<>
struct Association<event::Event, event::SADCHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse;
    /// Type of (main) association of `SADCHit` instances with
    /// instance `Event`.
    typedef event::Map<DetID, SADCHit, MapTag::sparse> Collection;
    /// Returns (main) collection of `SADCHit` instances associated
    /// with `Event` by attribute `sadcHits`.
    static Collection & map(event::Event & parent) { return parent.sadcHits; }
    /// Remove instance of `SADCHit` from (main) collection
    /// associated it with `Event` by attribute `sadcHits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::APVHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse;
    /// Type of (main) association of `APVHit` instances with
    /// instance `Event`.
    typedef event::Map<DetID, APVHit, MapTag::sparse> Collection;
    /// Returns (main) collection of `APVHit` instances associated
    /// with `Event` by attribute `apvHits`.
    static Collection & map(event::Event & parent) { return parent.apvHits; }
    /// Remove instance of `APVHit` from (main) collection
    /// associated it with `Event` by attribute `apvHits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::StwTDCHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse;
    /// Type of (main) association of `StwTDCHit` instances with
    /// instance `Event`.
    typedef event::Map<DetID, StwTDCHit, MapTag::sparse> Collection;
    /// Returns (main) collection of `StwTDCHit` instances associated
    /// with `Event` by attribute `stwtdcHits`.
    static Collection & map(event::Event & parent) { return parent.stwtdcHits; }
    /// Remove instance of `StwTDCHit` from (main) collection
    /// associated it with `Event` by attribute `stwtdcHits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::F1Hit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `F1Hit` instances with
    /// instance `Event`.
    typedef event::Map<DetID, F1Hit, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `F1Hit` instances associated
    /// with `Event` by attribute `f1Hits`.
    static Collection & map(event::Event & parent) { return parent.f1Hits; }
    /// Remove instance of `F1Hit` from (main) collection
    /// associated it with `Event` by attribute `f1Hits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::CaloHit> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `CaloHit` instances with
    /// instance `Event`.
    typedef event::Map<DetID, CaloHit, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `CaloHit` instances associated
    /// with `Event` by attribute `caloHits`.
    static Collection & map(event::Event & parent) { return parent.caloHits; }
    /// Remove instance of `CaloHit` from (main) collection
    /// associated it with `Event` by attribute `caloHits`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::APVCluster> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `APVCluster` instances with
    /// instance `Event`.
    typedef event::Map<PlaneKey, APVCluster, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `APVCluster` instances associated
    /// with `Event` by attribute `apvClusters`.
    static Collection & map(event::Event & parent) { return parent.apvClusters; }
    /// Remove instance of `APVCluster` from (main) collection
    /// associated it with `Event` by attribute `apvClusters`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::TrackScore> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse_ambig;
    /// Type of (main) association of `TrackScore` instances with
    /// instance `Event`.
    typedef event::Map<DetID, TrackScore, MapTag::sparse_ambig> Collection;
    /// Returns (main) collection of `TrackScore` instances associated
    /// with `Event` by attribute `trackScores`.
    static Collection & map(event::Event & parent) { return parent.trackScores; }
    /// Remove instance of `TrackScore` from (main) collection
    /// associated it with `Event` by attribute `trackScores`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::Track> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::sparse;
    /// Type of (main) association of `Track` instances with
    /// instance `Event`.
    typedef event::Map<TrackID, Track, MapTag::sparse> Collection;
    /// Returns (main) collection of `Track` instances associated
    /// with `Event` by attribute `tracks`.
    static Collection & map(event::Event & parent) { return parent.tracks; }
    /// Remove instance of `Track` from (main) collection
    /// associated it with `Event` by attribute `tracks`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};



template<>
struct Association<event::Event, event::Vertex> {
    /// Map features
    constexpr static MapTag::Code mapTag = MapTag::ordered;
    /// Type of (main) association of `Vertex` instances with
    /// instance `Event`.
    typedef event::Map<size_t, Vertex, MapTag::ordered> Collection;
    /// Returns (main) collection of `Vertex` instances associated
    /// with `Event` by attribute `vertices`.
    static Collection & map(event::Event & parent) { return parent.vertices; }
    /// Remove instance of `Vertex` from (main) collection
    /// associated it with `Event` by attribute `vertices`.
    static void remove( Collection & parent, typename Collection::const_iterator it ) {
        //throw std::runtime_error("hits remove is not implemented");  // TODO
        parent.erase(it);
    }
};


#define M_for_every_compound_type( m, ... ) \
    m( MoyalSADCWfFitParameters, __VA_ARGS__ ) \
    m( GaussianSADCFitParameters, __VA_ARGS__ ) \
    m( MSADCDFFTCoefficients, __VA_ARGS__ ) \
    m( MSADCPeak, __VA_ARGS__ ) \
    m( RawDataSADC, __VA_ARGS__ ) \
    m( SADCHit, __VA_ARGS__ ) \
    m( RawDataAPV, __VA_ARGS__ ) \
    m( APVHit, __VA_ARGS__ ) \
    m( APVCluster, __VA_ARGS__ ) \
    m( StwTDCRawData, __VA_ARGS__ ) \
    m( StwMCInfo, __VA_ARGS__ ) \
    m( StwTDCHit, __VA_ARGS__ ) \
    m( RawDataF1, __VA_ARGS__ ) \
    m( F1Hit, __VA_ARGS__ ) \
    m( CaloHit, __VA_ARGS__ ) \
    m( DriftDetScoreFeatures, __VA_ARGS__ ) \
    m( MCTrueTrackScore, __VA_ARGS__ ) \
    m( TrackScore, __VA_ARGS__ ) \
    m( ScoreFitInfo, __VA_ARGS__ ) \
    m( TrackFitInfo, __VA_ARGS__ ) \
    m( Track, __VA_ARGS__ ) \
    /* ... */


}  // namespace ::na64dp::event
}  // namespace na64dp

#ifdef hdql_FOUND
namespace hdql {
namespace helpers {
class CompoundTypes;
}  // namespace ::hdql::helpers
}  // namespace hdql
struct hdql_Context;
namespace na64dp {
namespace event {
void define_event_hdql_compounds(::hdql::helpers::CompoundTypes &, hdql_Context *);
}  // namespace ::na64dp::event
}  // namespace na64dp
#endif
