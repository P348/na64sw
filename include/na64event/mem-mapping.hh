/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

/**\brief Defines memory mapping strategy used by `na64sw` event structure
 *
 * This header provides various definitions utilized by generated C/C++ code
 * working with events.
 * */

#include "na64util/mem/alloc.hh"
#include "na64util/mem/mapping.hh"
#include "na64util/mem/pools.hh"

#include "na64util/mem/plainBlock.hh"

namespace na64dp {

/// A local memory type to use in the pipeline applications
typedef mem::Pools<NA64SW_EVMALLOC_STRATEGY> LocalMemory;

namespace event {

/// Shortcut with default na64sw memory allocation strategy
template<typename KeyT, typename ValueT, int mT, typename StrategyT=NA64SW_EVMALLOC_STRATEGY>
using Map = typename MapTraits< KeyT, mem::Ref<ValueT>
                              , (MapTag::Code) (mT)
                              , StrategyT
                              >::type;

#if 0  // TODO: not sure if such shortcuts are needed
template<typename KeyT, typename ValueT>
using UnorderedMap = Map<KeyT, ValueT, MapTag::sparse>;

template<typename KeyT, typename ValueT>
using OrderedMap = Map<KeyT, ValueT, MapTag::sparse | MapTag::ordered>;

template<typename KeyT, typename ValueT>
using UnorderedMultimap = Map<KeyT, ValueT, MapTag::sparse | MapTag::ambig>;

template<typename KeyT, typename ValueT>
using Multimap = Map<KeyT, ValueT, MapTag::ordered | MapTag::sparse | MapTag::ambig>;
#endif

/// An object creating function returning the shared pointer to the instance
/// allocated with provided memory allocation strategy
//template<typename T, typename StrategyT, typename... Args> std::shared_ptr<T>
//create( StrategyT & alloc, Args&&... args ) {
//    return std::allocate_shared< T,
//           na64dp::mem::Allocator<T, StrategyT> >( alloc.of<T>(),
//                   std::forward<Args>(args)...);
//}

}  // namespace ::na64dp::event
}  // namespace na64dp

