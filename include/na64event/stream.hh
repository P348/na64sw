/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/mem/mapping.hh"
#include "na64util/pair-hash.hh"
#include "na64util/demangle.hh"
#include "na64util/str-fmt.hh"

#include <type_traits>
#include <typeinfo>
#include <unordered_map>
#include <variant>
#include <stdexcept>

namespace na64dp {
namespace event {
namespace detail {
/// Routes execution to typed serialization for scalar attributes, maps,
/// compound types
template<typename T> struct AttrTypeTraits;
}  // namespace ::na64dp::event::detail

/**\brief Template shim adapting stream to callable interface of event objects
 *
 * This class provides a reentrant visitor object used to recursively traverse
 * the event object using `event::Traits<>::for_each_attr()` function.
 *
 * Implementation stream should define following traits:
 *
 *  - `ObjectDefinition` -- identifier to object being read or recorded
 *  - `process_attribute(attrID, objectDefinition, value, this)` template
 *    method adding/reading named attribute into/from current object.
 *    ... (TODO: doc)
 * */
template<typename ImplT>
class StreamingShim {
public:
    /// Handle to attribute, defined by the implementation
    typedef typename ImplT::AttrID AttrID;
private:
    /// Reference to stream in use
    ImplT & _impl;
    /// Handle to current (compound) object type, defined by implementation
    typename ImplT::ObjectDefinition _objectHandle;
public:
    /// Ctr must be parameterised by dest/src stream in use
    StreamingShim(ImplT & impl) : _impl(impl) {}

    /// Returns ref to stream object
    ImplT & implementation() { return _impl; }
    /// Returns ref to stream object (const)
    const ImplT & implementation() const { return _impl; }

    /// Sets current object handle
    void current_object(typename ImplT::ObjectDefinition odef) {
        _objectHandle = odef;
    }
    /// Returns current object handle
    typename ImplT::ObjectDefinition current_object() {
        return _objectHandle;
    }
    /// Returns current object handle (const)
    typename ImplT::ObjectDefinition & current_object() const {
        return _objectHandle;
    }

    /// Considers attribute of type `T`
    ///
    /// * Simple (arithmetic scalars and plain arrays of fixed size) types will
    ///   be forwarded to stream `pod_attr()` call.
    /// * `mem::Ref<>` are forwarded to traits
    /// * Maps are treated ...
    template<typename T> void operator()(AttrID attrID, T & attr) {
        detail::AttrTypeTraits<typename std::remove_const<T>::type>::template process_attribute<ImplT>(
                attrID, current_object(), attr, *this);
    }
};

#include "stream-traits.hh"

#if 0
template<typename ImplT> class Stream;

/**\brief Keeps reference to already stored entries from collections
 *
 * To avoid data duplication in formats that do not natively support links,
 * an interim mapping object is defined to keep indexes.
 *
 * This template class is parameterised with desired link reference type.
 * Depending on stream implementation this can be a simple enumerator, certain
 * IO descriptors, item ID, DB primary key, etc (`EntryIDT`).
 *
 * Then every collection item must be uniquely mapped into this `EntryIDT`.
 * Note that, to disambiguate duplicating keys and support multiple types
 * an actual *key* to which those IDs correspond to is a composite structure
 * consisting of:
 *  - Key type C++ hash
 *  - Value type C++ hash
 *  - Key value
 *  - Value reference
 * */
template<typename EntryIDT>
class Collections {
private:
    /// Base class for collection links
    struct iCollectionLinks {virtual ~iCollectionLinks() {}};

    ///\brief Collects links to collection items of certain type
    ///
    /// Event's sub-items are stored in various types of mapping which for most
    /// of the cases correspond to array-like collections. This class maps
    /// particular sub-item `mem::Ref<>` to the orderly number of mapping.
    template<typename KeyT, typename ValueT>
    class CollectionLinks
                : public iCollectionLinks
                , public std::unordered_map
                      < std::pair< KeyT, typename std::remove_const<ValueT>::type *>
                      , EntryIDT
                      , util::PairHash
                      > {};

    /// Collections by their type ID
    std::unordered_map< std::type_index, std::shared_ptr<iCollectionLinks> >
        _collections;
public:
    ///\brief Adds entry to refer to with given ID
    ///
    ///\returns `false` if key is duplicating (insertion failed).
    template<typename KeyT, typename ValueT> bool
    add_entry(KeyT k, ValueT * v, EntryIDT id) {
        typedef CollectionLinks<typename std::remove_const<KeyT>::type, ValueT> Collection_t;  // for readability
        const std::type_info & colTypeID = typeid(Collection_t);
        auto it = this->_collections.find(colTypeID);
        if(this->_collections.end() == it) {
            it = this->_collections.emplace(colTypeID, std::make_shared<Collection_t>()).first;
        }
        auto ir = static_cast<Collection_t *>(it->second.get())->emplace(
                    std::pair<typename std::remove_const<KeyT>::type, ValueT *>(k, v), id );
        return ir.second;
    }
    /// Checks, whether the item is in collections
    template<typename KeyT, typename ValueT> bool
    has_entry(KeyT k, ValueT * v) const {
        typedef CollectionLinks<typename std::remove_const<KeyT>::type, ValueT> Collection_t;  // for readability
        const std::type_info & colTypeID = typeid(Collection_t);
        auto it = this->_collections.find(colTypeID);
        if(this->_collections.end() == it) {
            return false;
        }
        Collection_t * colPtr = static_cast<Collection_t *>(it->second.get());
        auto iit = colPtr->find(std::pair<typename std::remove_const<KeyT>::type, ValueT *>(k, v));
        return iit != colPtr->end();
    }
    /// Returns ID for entry
    template<typename KeyT, typename ValueT> EntryIDT
    get_link_to(KeyT k, ValueT * v) const {
        typedef CollectionLinks<typename std::remove_const<KeyT>::type, ValueT> Collection_t;  // for readability
        const std::type_info & colTypeID = typeid(Collection_t);
        auto it = this->_collections.find(colTypeID);
        if(this->_collections.end() == it) {
            throw std::runtime_error("No collection type");  // todo: dedicated exception
        }
        Collection_t * colPtr = static_cast<Collection_t *>(it->second.get());
        auto iit = colPtr->find(std::pair<typename std::remove_const<KeyT>::type, ValueT *>(k, v));
        if( iit == colPtr->end() ) {
            throw std::runtime_error("No such entry");  // todo: dedicated exception
        }
        return iit->second;
    }
};  // class Collections


/// Specialization for simple arithmetic types (bool, int, float, etc)
template<typename T>
struct Traits<T, typename std::enable_if<std::is_arithmetic<T>::value>::type> {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, T & v )
        { ds.impl().template scalar<T>(v); }
};

/// Specialization for arrays
template<typename T>
struct Traits< T
             , typename std::enable_if<
                        std::is_array<T>::value &&
                        std::is_arithmetic<
                                typename std::remove_pointer<
                                    typename std::decay<T>::type
                                >::type
                            >::value
                    >::type
             > {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, T & v )
        { ds.impl().template scalar<T>(v); }
};

/// Specialization for native C-pointers (external classes)
template<typename T>
struct Traits< T
             , typename std::enable_if<
                        std::is_pointer<T>::value
                    >::type
             > {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, T & v )
        { ds.impl().template scalar<T>(v); }
};

// ...

template<typename T1, typename T2>
struct Traits< std::pair<T1, T2> > {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, std::pair<T1, T2> & v )
        { ds.impl().template tuple_<T1, T2>(v.first, v.second); }
};

// NOTE: if we got there, this reference must be considered as the original
// (destination of the links collections), so the object itself must be the
// subject (de-)serialization, not the "link"
template<typename T>
struct Traits< mem::Ref<T> > {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, mem::Ref<T> & v ) {
        if( StreamImplT::isInput ) {
            // TODO: foresee checks for existance of data in the stream
            throw std::runtime_error("TODO: read optional event data");
        } else {
            if( (bool) v ) {
                Traits<T>::stream_op(ds, *(v.get()));
            } else {
                ds.impl().none();
            }
        }
    }
};

//template<>
//struct Traits<EventID> {
//    template<typename StreamImplT>
//    static void stream_op( Stream<StreamImplT> & ds, double & v )
//        { ds.impl().template scalar<EventID>(v); }
//};

// ... (other scalars)

#if 1
/// \brief Implements insertion differently for list/object/scalars
///
/// Default traits implementation is for scalar entries
//template<typename T> struct Traits;
//{
//    template<typename ValueT>
//    static void stream_op( ImplT & impl, KeyT key, ValueT value) {
//        impl.template scalar<ValueT>(key, value);
//    }
//};  // struct StreamTypeTratis, default

/// Specialization for maps (ordered, sparse, non-ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct Traits<
            std::map< KeyT, MappedT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, MappedT>
                                    , StrategyT
                                    >
                    >
          > {
    typedef std::map< KeyT, MappedT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, MappedT>
                                    , StrategyT
                                    >
                    > ValueType;
    /// Adds values with translated to (number, key)
    template<typename ImplT>
    static void stream_op( Stream<ImplT> & ds
                         , ValueType & value
                         ) {
        ds.impl().ordered_map(ds, value);  // < HERE
    }
};  // struct StreamTypeTratis, ordered map

/// Specialization for multimaps (ordered, sparse, ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct Traits<
            std::multimap< KeyT, MappedT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, MappedT>
                                         , StrategyT
                                         >
                         >
        > {
    typedef std::multimap< KeyT, MappedT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, MappedT>
                                         , StrategyT
                                         >
                         > ValueType;
    template<typename ImplT>
    static void stream_op( Stream<ImplT> & ds
                         , ValueType & value
                         ) {
        ds.impl().ordered_multimap(ds, value);  // < HERE
    }
};  // struct StreamTypeTratis, ordered multimap

/// Specialization for unordered maps (unordered, sparse, non-ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct Traits<
            std::unordered_map< KeyT, MappedT
                              , std::hash<KeyT>
                              , std::equal_to<KeyT>
                              , mem::Allocator< std::pair<const KeyT, MappedT>
                                              , StrategyT
                                              >
                              >
        > {
    typedef std::unordered_map< KeyT, MappedT
                              , std::hash<KeyT>
                              , std::equal_to<KeyT>
                              , mem::Allocator< std::pair<const KeyT, MappedT>
                                              , StrategyT
                                              >
                              > ValueType;
    template<typename ImplT>
    static void stream_op( Stream<ImplT> & ds
                         , ValueType & value
                         ) {
        ds.impl().unordered_map(ds, value);  // < HERE
    }
};  // struct StreamTypeTratis, unordered map

/// Specialization for unordered maps (unordered, sparse, non-ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct Traits<
            std::unordered_multimap< KeyT, MappedT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, MappedT>
                                                   , StrategyT
                                                   >
                                   >
        > {
    typedef std::unordered_multimap< KeyT, MappedT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, MappedT>
                                                   , StrategyT
                                                   >
                                   > ValueType;
    template<typename ImplT>
    static void stream_op( Stream<ImplT> & ds
                         , ValueType & value
                         ) {
        ds.impl().unordered_multimap(ds, value);  //< HERE
    }
};  // struct StreamTypeTratis, unordered multimap
#endif


/**\brief An event data stream adapter
 *
 * Provides thin wrapper around certain implementation of the event stream.
 * Uses generated event types traits to traverse throught the event object and
 * forward call to implementation-defined invokations.
 *
 * Maintains internal stack of states: `push_state()` and
 * `pop_state()` are called every time execution steps into sub-object:
 *
 * Implementation class has to provide following methods:
 *  - push_state()/pop_state() template methods denote creation and
 *    finalization of object of certain key. Accepts key type and object type
 *    as template parameters and key instance as function arguments. For
 *    schemaless formats should correspond to insertion/retrieval of
 *    map/object/hash with key corresponding to field name.
 *  - `scalar()` template method should read/retrieve a scalar value (of
 *    object or collection)
 *  - `[un]ordered_[multi]map()` ...
 *  - `none()`
 *  - `link()`
 * */
template<typename ImplT>
class Stream {
public:
    typedef Collections<typename ImplT::Link_t> Collections_t;
protected:
    /// Reference to actual implementation
    ImplT & _impl;
    /// Reference to event's collections to build the links
    Collections_t & _colls;
    /// Push/pops the stack state at exit (pops state at destructor).
    template<typename T, typename KeyT>
    class Guard {
        Stream<ImplT> & _ref;
        KeyT _key;
    public:
        // TODO: (note of copying key) reading has to modify output key
        Guard( Stream<ImplT> & ref
             , const KeyT & key ) : _ref(ref), _key(key) {
            _ref._impl.template push_state<KeyT, T>(_key);
        }
        ~Guard() {
            _ref._impl.template pop_state<KeyT, T>(_key);
        }
    };
public:
    /// Creates stream adapter around certain stream implementation
    Stream(ImplT & impl, Collections_t & collections)
            : _impl(impl), _colls(collections) {}
    /// Considers one-of item (std::variant<>)
    template<typename KeyT, typename ... VariantsT> Stream<ImplT> &
    op(KeyT & k, std::variant<std::monostate, mem::Ref<VariantsT>... > & v) {
        typedef std::variant<std::monostate, mem::Ref<VariantsT>... > Value_t;
        //auto guard = Guard<Value_t, KeyT>(*this, k);
        //Traits<T>::template stream_op<ImplT>(*this, v);
        //Traits<>
        _impl.template push_state<KeyT, Value_t>(k);
        try {
            std::visit([this](auto & vv){
                    Traits<typename std::remove_reference<decltype(vv)>::type>::template stream_op<ImplT>(*this, vv);
                }, v);
        } catch( ... ) {
            _impl.template pop_state<KeyT, Value_t>(k);
            throw;
        }
        _impl.template pop_state<KeyT, Value_t>(k);
        return *this;
    }
    /// Considers an array of items
    template<typename T, typename KeyT>
    typename std::enable_if<!std::is_array<KeyT>::value, Stream<ImplT> &>::type 
    op(KeyT & k, T & v) {
        auto guard = Guard<T, KeyT>(*this, k);
        Traits<T>::template stream_op<ImplT>(*this, v);
        return *this;
    }
    #if 1
    /// Considers an item containing reference (explots links to collections)
    template<typename T, typename KeyT> Stream<ImplT> &
    op(KeyT & k, mem::Ref<T> & v) {
        auto guard = Guard<mem::Ref<T>, KeyT>(*this, k);
        //auto & col = collections().template links_for<KeyT, T>();
        //auto it = col.find(k, v.get());
        if(!collections().template has_entry<KeyT, T>(k, v.get())) {
            Traits< mem::Ref<T> >::template stream_op<ImplT>(*this, v);
        } else {
            this->impl().link( collections()
                    .template get_link_to<KeyT, T>(k, v.get()));
        }
        return *this;
    }
    #endif
    /// Considers an item
    template<typename T, typename KeyT>
    typename std::enable_if< std::is_array<KeyT>::value, Stream<ImplT> &>::type 
    op(KeyT & k, T & v) {
        auto guard = Guard<T, std::string>(*this, k);
        Traits<T>::template stream_op<ImplT>(*this, v);
        return *this;
    }
    /// Returns reference to destination stream implementation
    ImplT & impl() { return _impl; }
    /// Returns reference to collections
    Collections_t & collections() { return _colls; }
    /// Null node
    void no_entry() {  }  // ? TODO
};

template<> struct Traits<std::monostate> {
    template<typename StreamImplT>
    static void stream_op(event::Stream<StreamImplT> & ds, std::monostate &) {
        ds.no_entry();
    }
};
#endif

}  // namespace ::na64dp::event
}  // namespace na64dp
