#pragma once

/**\file
 * \brief HDQL-related assets for events structures */

#include "na64sw-config.h"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64event/data/event.hh"

#include "na64util/na64/event-id.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/trackID.hh"
#include "na64detID/wireID.hh"
#include <hdql/helpers/compounds.hh>

// Override HDQL helper traits for EventID type to consider it as a compound
// type

namespace hdql {
namespace helpers {

namespace detail {
template<> struct ArithTypeNames<::na64dp::DetID>    { static constexpr const char * name = "DetID"; };
template<> struct ArithTypeNames<::na64dp::PlaneKey> { static constexpr const char * name = "PlaneKey"; };
template<> struct ArithTypeNames<::na64dp::TrackID>  { static constexpr const char * name = "TrackID"; };
};

//                ____________________________________________________________
// _____________/ Helpers specialization for EventID type as a scalar compound

// Should be called before auto-generate compound creation routines to provide
// helpers with existing definition for event ID
void create_event_id_compound(hdql::helpers::CompoundTypes &, hdql_Context * context);

template<>
struct TypeInfoMixin<::na64dp::EventID> {
    static constexpr bool isCompound = true;
    static hdql_Compound *
    type_info(const hdql_ValueTypes * valTypes, Compounds & compounds) {
        assert(valTypes);
        auto it = compounds.find(typeid(::na64dp::EventID));
        assert(compounds.end() != it);
        return it->second;
    }
};  // TypeInfoMixin<EventID>

template<typename OwnerT, ::na64dp::EventID OwnerT::*ptr>
struct IFace<ptr>
        : public TypeInfoMixin<::na64dp::EventID> {
    static constexpr bool isCollection = false;

    static hdql_Datum_t
    dereference( hdql_Datum_t root  // owning object
               , hdql_Datum_t dynData  // allocated with `instantiate()`
               , struct hdql_CollectionKey * // may be NULL
               , const hdql_Datum_t  // may be NULL
               , hdql_Context_t
               ) {
        auto p = &(reinterpret_cast<OwnerT *>(root)->*ptr);
        return reinterpret_cast<hdql_Datum_t>(p);
    }

    static hdql_ScalarAttrInterface iface() {
        return hdql_ScalarAttrInterface{
                  .definitionData = NULL
                , .instantiate = NULL
                , .dereference = dereference
                , .reset = NULL
                , .destroy = NULL
            };
    }

    static constexpr auto create_attr_def = detail::AttrDefCallback<true, false>::create_attr_def;
};  // scalar compound attribute


//        ____________________________________________________________________
// _____/ Helpers specialization for event time type as a scalar compound type

// Caveat: pair of integers of the same type can, in principle, appear in the
// event structures. In that case an automated interface instantiation will
// follow this specialization as well.
template<>
struct TypeInfoMixin<std::pair<time_t, uint32_t>> {
    static constexpr bool isCompound = true;
    static hdql_Compound *
    type_info(const hdql_ValueTypes * valTypes, Compounds & compounds) {
        assert(valTypes);
        auto it = compounds.find(typeid(std::pair<time_t, uint32_t>));
        assert(compounds.end() != it);
        return it->second;
    }
};  // TypeInfoMixin<std::pair<time_t, uint32_t>>

// contrary to `TypeInfoMixin<pair<...>>` above this specialization is
// parameterized with pointer-to member explicitly and won't appear in other
// structs
template<>
struct IFace<&::na64dp::event::Event::time >
        : public TypeInfoMixin< std::pair<time_t, uint32_t> > {
    static constexpr bool isCollection = false;

    static hdql_Datum_t
    dereference( hdql_Datum_t root  // owning object
               , hdql_Datum_t dynData  // allocated with `instantiate()`
               , struct hdql_CollectionKey * // may be NULL
               , const hdql_Datum_t  // may be NULL
               , hdql_Context_t
               ) {
        auto p = &(reinterpret_cast<::na64dp::event::Event *>(root)->time);
        return reinterpret_cast<hdql_Datum_t>(p);
    }

    static hdql_ScalarAttrInterface iface() {
        return hdql_ScalarAttrInterface{
                  .definitionData = NULL
                , .instantiate = NULL
                , .dereference = dereference
                , .reset = NULL
                , .destroy = NULL
            };
    }

    static constexpr auto create_attr_def = detail::AttrDefCallback<true, false>::create_attr_def;
};  // scalar compound attribute

}  // namespace ::hdql::helpers
}  // namespace hdql

#endif  // defined(hdql_FOUND) && hdql_FOUND

