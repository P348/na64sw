#pragma once

/**\file
 * \brief Helper classes for streaming formats with links
 *
 * Most of the IO formats involves certain type of references or links that
 * should be maintained by dedicated tables. This helper classes provides
 * some degree of boilerplate.
 * */

#include "na64util/str-fmt.hh"
#include "na64event/mem-mapping.hh"

namespace na64dp {
namespace event {
/// \brief Streaming traits used for collections
///
/// Should define types:
///  * `CollectedItemID` copyable type used to identify particular item in
///    collection
///  * `WritableCollectionPayload` is struct or class to be included into
///    `WritableCollection`
/// Should define methods:
///  * `CollectedItemID add(const T *, ImplT &)` has to add item to collection
///     and return its ID
///  * ... (get)
//template<typename ImplT>
//struct CollectionTraits;  // XXX

/// Base collection class
struct iCollection {
    virtual ~iCollection() {}
    virtual void reset() = 0;
};

/// Typed collection class
template<typename T, typename ImplT>
struct WritableCollection
            : public iCollection
            , public ImplT::template WritableCollectionIndex<T>
            //, protected std::unordered_map<const T *, typename ImplT::CollectedItemID>
            //, ... impl-specific parent?
            {
    typedef typename ImplT::template WritableCollectionIndex<T> Parent;
    /// Using pointer to an instance, should add and return or get and
    /// return ID of object item
    typename ImplT::CollectedItemID
    id_of(const T * ptr, ImplT & impl) {
        auto it = Parent::find(ptr);
        if(it == Parent::end()) {
            typename ImplT::CollectedItemID id = impl.add_collectible(ptr);
            it = Parent::emplace(ptr, id).first;
        }
        return it->second;
    }

    void reset() override {
        Parent::clear();
    }
};

template<typename ImplT>
class WritableCollections {
private:
    /// Map of typed collections
    std::unordered_map<std::type_index, iCollection *> _collections;
public:
    /// Returns typed collection instance
    template<typename ObjectT> WritableCollection<ObjectT, ImplT> &
    collection_of() {
        auto it = _collections.find(typeid(ObjectT));
        if(_collections.end() == it) {
            auto wcPtr = new WritableCollection<ObjectT, ImplT>();  // todo: impl-specific args?
            it = _collections.emplace(typeid(ObjectT), wcPtr).first;
        }
        return *static_cast<WritableCollection<ObjectT, ImplT>*>(it->second);
    }

    void reset() {
        for(auto & p: _collections) {
            if(p.second) p.second->reset();
        }
    }

    virtual ~WritableCollections() {
        for(auto & p: _collections) {
            if(p.second) delete p.second;
        }
    }
};


//
// Readable

/// Typed collection class
template<typename T, typename ImplT>
struct ReadableCollection
            : public iCollection
            , public ImplT::template ReadableCollectionIndex<T>
            //, protected std::unordered_map<typename ImplT::CollectedItemID, mem::Ref<T>>
            //, ... impl-specific parent?
            {
    typedef typename ImplT::template ReadableCollectionIndex<T> Parent;

    /**\brief May return nullptr if collection ID is set to special value
     * */
    mem::Ref<T>
    value_by(typename ImplT::CollectedItemID id) {
        auto it = Parent::find(id);
        assert(Parent::end() != it);  // otherwise, a serious error occured
        return it->second;
    }

    void reset() override {
        Parent::clear();
    }
};

template<typename ImplT>
class ReadableCollections {
private:
    /// Map of typed collections
    std::unordered_map<std::type_index, iCollection *> _collections;
public:
    /// Returns typed collection instance
    template<typename ObjectT> ReadableCollection<ObjectT, ImplT> &
    collection_of() {
        auto it = _collections.find(typeid(ObjectT));
        if(_collections.end() == it) {
            auto wcPtr = new ReadableCollection<ObjectT, ImplT>();  // todo: impl-specific args?
            it = _collections.emplace(typeid(ObjectT), wcPtr).first;
        }
        return *static_cast<ReadableCollection<ObjectT, ImplT>*>(it->second);
    }

    void reset() {
        for(auto & p: _collections) {
            if(p.second) p.second->reset();
        }
    }

    virtual ~ReadableCollections() {
        for(auto & p: _collections) {
            if(p.second) delete p.second;
        }
    }
};

}  // namespace ::na64dp::event
}  // namespace na64dp
