/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64event/mem-mapping.hh"

#include <type_traits>
#include <limits>
#include <variant>

namespace na64dp {
namespace util {

/// Sets the variable given by reference for arithmetic type to quiet NaN
template<typename T>
typename std::enable_if< std::numeric_limits<T>::has_quiet_NaN >::type
reset_arithmetic_value(T & v) {
    v = std::numeric_limits<T>::quiet_NaN();
}

/// Sets the variable given by reference for arithmetic type to min value
template<typename T>
typename std::enable_if< ! std::numeric_limits<T>::has_quiet_NaN >::type
reset_arithmetic_value(T & v) {
    v = std::numeric_limits<T>::min();
}

/// Arithmetic type reset; dispatches call to `reset_arithmetic_value()`
template<typename T>
typename std::enable_if< std::is_arithmetic<T>::value >::type
reset(T & v) {
    reset_arithmetic_value(v);
}

/// Array unwinding reset
template<typename T>
typename std::enable_if< std::is_array<T>::value >::type
reset(T & v) {
    for( unsigned i = 0; i < std::extent<T>::value; ++i ) {
        reset(v[i]);
    }
}

/// Template specialization for `mem::Ref` template instances
template<typename T> void
reset(mem::Ref<T> & ref) {
    ref.reset();
}

/// Template specialization for any pointer
template<typename T> void
reset(T *& ptr) {
    ptr = nullptr;
}

}  // namespace ::na64dp::util

namespace event {

/// Standard-precision floating number type to be used in events representation
typedef float StdFloat_t;

/**\brief Access traits for the event field/attribute subtype
 *
 * Default is not defined. Specialized traits must provide:
 *  - `Getter` type of the callback function receiving the structure instance
 *  and returning `double` value.
 *  - `getters`
 * */
template<typename T, typename=void> struct Traits;

#if 0  // TODO: not sure we need it, kept for possible future usage
template<typename ImplT> class Stream;
/// `std::variant` value traits
template<typename ... VariantsT>
struct Traits< std::variant<std::monostate, mem::Ref<VariantsT>... >
             , void
             > {
    typedef std::variant<std::monostate, mem::Ref<VariantsT>... > ValueType;
    typedef double (*Getter)(const ValueType &);
    typedef std::unordered_map<std::string, std::pair<std::string, Getter>> Getters;
    static Getters getters;
    // no NaturalKey type
    template<typename StreamImplT>
    static void stream_op(Stream<StreamImplT> & ds, ValueType & obj);
};
#endif

template<typename T>
struct CallableTraits {
    ///\brief Called to increase inheritance level for the current object
    ///
    /// Default implementation retrurns reference to the original callable
    /// instance. For some data serialization formats, can change callable
    /// state to handle encompassed type.
    template<typename ParentT> static T &
    inherited(T & callable) {
        return callable;
    }

    ///\brief Returns attribute ID useful for callable
    ///
    /// Accpets strign identifier and integer index of the attribute.
    /// By default returns same string identifier.
    static const char *
    attr_id(int attrIndex, const char * attrName) {
        return attrName;
    }
};

}  // namesapce ::na64dp::event
}  // namespace na64dp
