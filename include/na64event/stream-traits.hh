namespace detail {
/// Generic implementation, assumes scalar value
///
/// This (default) implementation delegates (de)serialization to `pod_attr<T>()`
/// method of stream implementation.
template<typename T>
struct AttrTypeTraits {
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition objDef
                                 , T & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation()
            .template pod_attr<typename std::remove_const<T>::type>(objDef, attrID, attrValue);
    }

    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition objDef
                                 , const T & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation()
            .template pod_attr<typename std::remove_const<T>::type>(objDef, attrID, attrValue);
    }
};  // struct Traits<>

/// Compound types implementation
///
/// Compound types are stored in the collections and instead of direct
/// (de)serialization of attribute of compound type we should either retrieve
/// and provide output stream with an implementation-defined link to
/// corresponding entry in typed collection, or set the `mem::Ref<>`.
template<typename T>
struct AttrTypeTraits<mem::Ref<T>> {
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition objDef
                                 , mem::Ref<T> & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation()
            .template typed_attr(objDef, attrID, attrValue);
    }
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition objDef
                                 , const mem::Ref<T> & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation()
            .template typed_attr(objDef, attrID, attrValue);
    }
};  // struct Traits<>

//
// Maps
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct AttrTypeTraits<
            std::map< KeyT, MappedT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, MappedT>
                                    , StrategyT
                                    >
                    >
          > {
    typedef std::map< KeyT, MappedT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, MappedT>
                                    , StrategyT
                                    >
                    > MapType;
    /// Adds values with translated to (number, key)
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }

    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , const MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }
};  // struct StreamTypeTratis, ordered map

/// Specialization for multimaps (ordered, sparse, ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct AttrTypeTraits<
            std::multimap< KeyT, MappedT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, MappedT>
                                         , StrategyT
                                         >
                         >
        > {
    typedef std::multimap< KeyT, MappedT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, MappedT>
                                         , StrategyT
                                         >
                         > MapType;
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }

    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , const MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }
};  // struct StreamTypeTratis, ordered multimap

/// Specialization for unordered maps (unordered, sparse, non-ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct AttrTypeTraits<
            std::unordered_map< KeyT, MappedT
                              , std::hash<KeyT>
                              , std::equal_to<KeyT>
                              , mem::Allocator< std::pair<const KeyT, MappedT>
                                              , StrategyT
                                              >
                              >
        > {
    typedef std::unordered_map< KeyT, MappedT
                              , std::hash<KeyT>
                              , std::equal_to<KeyT>
                              , mem::Allocator< std::pair<const KeyT, MappedT>
                                              , StrategyT
                                              >
                              > MapType;
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }

    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , const MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }
};  // struct StreamTypeTratis, unordered map

/// Specialization for unordered maps (unordered, sparse, non-ambiguous)
template< typename MappedT
        , typename KeyT
        , typename StrategyT
        >
struct AttrTypeTraits<
            std::unordered_multimap< KeyT, MappedT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, MappedT>
                                                   , StrategyT
                                                   >
                                   >
        > {
    typedef std::unordered_multimap< KeyT, MappedT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, MappedT>
                                                   , StrategyT
                                                   >
                                   > MapType;
    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }

    template<typename ImplT>
    static void process_attribute( typename StreamingShim<ImplT>::AttrID attrID
                                 , typename ImplT::ObjectDefinition obj
                                 , const MapType & attrValue
                                 , StreamingShim<ImplT> & shim
                                 ) {
        shim.implementation().map_attr(obj, attrID, attrValue);
    }
};  // struct StreamTypeTratis, unordered multimap

}  // namespace ::na64dp::event::detail
