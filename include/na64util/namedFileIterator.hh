#include "na64sw-config.h"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include <TFile.h>
#include <TPRegexp.h>

#include <stack>

namespace na64dp {
namespace util {

/// Stateful iterator over TFile, searching for objects of certain class
/// matching certain regular expression
class NamedFileIterator {
public:
    typedef std::pair<TIter, TDirectory *> StackEntry;
private:
    /// A target file
    TFile * _file;
    /// A name of the (ancestor) class to retrieve
    TString _className;
    /// Regular expression instance
    TPRegexp _rx;
    /// Pointer to debug log stream (can be null)
    std::ostream * _debugLogPtr;

    /// Current state
    std::stack<StackEntry> _dirStack;
public:
    /// Ctr initializing object for single pass
    NamedFileIterator( TFile * file
                     , const char * clName
                     , const char * regex
                     , std::ostream * debugLogPtr=nullptr
                     );

    /// Returns pointer of object or null when there is no more matching objects
    TObject * get_match(TObjArray ** subs=nullptr);
};

}
}

#endif  // ROOT_FOUND
