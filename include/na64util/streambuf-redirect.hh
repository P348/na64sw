#pragma once

#include <streambuf>

#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>

namespace na64dp {
namespace util {

/**\brief Used to capture `std::stream` output
 *
 * Set destination category and priority to use for stream's messages.
 * Ise it whenever `std::ostream` is required as logging stream, e.g.:
 *  
 *      std::ostream( _streambuf = new util::Log4Cpp_StreambufRedirect(logCat, log4cpp::Priority::WARN) )
 *
 * \warning `sync()` does not neccessarily empties the buffer!
 * */
class Log4Cpp_StreambufRedirect : public std::streambuf {
protected:
    ///\brief Overriden STL
    ///
    /// Redirects `c` message into log4cpp's category
    std::streamsize xsputn(const char_type * c, std::streamsize n) override;
    ///\brief Overriden STL
    ///
    /// Puts character
    int_type overflow(int_type ch) override;
    ///\brief Flushes buffered message(s)
    int sync() override;
    /// Concatenates newlined buffers
    std::string _buf;

    void _print_message_buffer();
public:
    Log4Cpp_StreambufRedirect( log4cpp::Category & c
                             , log4cpp::Priority::PriorityLevel p
                             );
    log4cpp::Category & get_category() { return _category; }
private:
    log4cpp::Category & _category;  ///< Destination logging category
    log4cpp::Priority::PriorityLevel _priority;  ///< Priority for the messages
    std::string _pdNl;  ///< platform-dependent newline
};

}  // namespace ::na64dp::util
}  // namespace na64dp

