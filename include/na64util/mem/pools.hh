#pragma once

/**\file
 * \brief Memory pools used to count references across complex structures
 *
 * Object pools provide storage for pointers with reference counting. Reference
 * counting decreases complexity of object creation/deletion within
 * hierarchical data (for instance, physical event description) where same
 * instance might be referenced within multiple levels of the hierarchy.
 * However, this comes by cost of an additional CPU overhead for the
 * increment/decrement operations of the reference counter for each instance
 * being considered.
 *
 * These helper class is designed in hope to mitigate this possible performance
 * impact by keeping counters close to the allocated objects, if appropriate
 * memory allocation strategy is in use. Additional enhancement is done for
 * arrays of controlled objects as these arrays are not re-allocated
 * (e.g. pools are designed for reentrant usage).
 * */

#include <vector>
#include <map>
#include <unordered_map>
#include <typeindex>
#include <memory>
#include <cstring>

#include <log4cpp/Category.hh>

#include "na64util/mem/alloc.hh"

#include "na64util/mem/plainBlock.hh"
// TODO: ... other strategies?

#include "na64util/str-fmt.hh"

namespace na64dp {
namespace mem {

/**\brief A base object pool class
 *
 * This is a basic
 * */
struct AbstractPool {
    virtual void clear() = 0;
    virtual ~AbstractPool() {}
};

template<typename T> using Ref = std::shared_ptr<T>;  // TODO decltype(create)

/**\brief A pool of shared pointers corresponding to event data structs
 *
 * Instance represents a set of objects of certain type associated by the mean
 * of shared (reference counting) object (`mem::Ref<T>` that is typically a
 * `std::shared_ptr`).
 *
 * Typically, used to manage objects of a current event instance (e.g. hits,
 * an event itself, etc).
 *
 * Provides mechanism to remove the data that must be considered not being used
 * by any association. For instance, noise hits that has been removed from main
 * event's collection and do not participate in any cluster/track ossociation
 * map will be only referenced by the pool and their refcount will be =1 (e.g.
 * only used by pool).
 * */
template< typename T  //< object type
        , typename StrategyT  //< object allocation strategy type
        //, template<typename> class ContainerAllocatorTT=std::allocator //< pool container allocator
        , typename ContainerAllocatorT=std::allocator< Ref<T> >
        >
class Pool : public AbstractPool
           , public std::vector<Ref<T>, ContainerAllocatorT> {
public:
    typedef std::vector<Ref<T>, ContainerAllocatorT> Parent;
private:
    /// Allocator for objects managed by shared pointer
    mem::Allocator<Ref<T>, StrategyT> _objAllocr;
public:
    /// Ctr, dispatches allocators
    Pool( mem::Allocator<Ref<T>, StrategyT> allocr
        , ContainerAllocatorT && cntrAllocator )
            : Parent(cntrAllocator)
            , _objAllocr(allocr)
            {}

    /// \brief Creates new object on pool
    ///
    /// \returns copyable reference (smart pointer) to constructed instance
    template<class... Args> Ref<T> create(Args&&... args) {
        #if 1  // TODO: this is sub-optimal
        auto ptr = std::allocate_shared< T,
               na64dp::mem::Allocator<T, PlainBlock> >(
                          //na64dp::mem::Allocator<T, PlainBlock>(this->get_allocator())
                          _objAllocr //this->get_allocator()  // 1st arg of allocate_shared()
                        , std::forward<Args>(args)...  // ...this relate to actual ctr
                        );
        this->push_back(ptr);
        #else
        this->emplace_back( Pool<T, StrategyT>(_strategy), std::forward<Args>(args)... );
        #endif
        return this->back();
    }

    /// Clears the objects (leaving the allocated memory intact)
    virtual void clear() override {
        Parent::clear();
    }
};

template<typename ObjectT> struct PoolTraits;  // undef

/**\brief A full set of pool allocators typically used together with a single
 * reentrant event instance
 *
 * Instance of this class typically represents a local memory storage related
 * to a single event processing thread. It dispatches the memory-allocation
 * strategy to object creation, construction and destruction calls also
 * facilitating the event object reference counting pools.
 *
 * Full set of event pools are provided by execution API to each handler/source
 * together with current event (and may be retrieved from the event at any time).
 *
 * \todo Parameterise container allocator instance (see creation in
 *      `pool_of()`, etc). Probably this allocator(s) too may require some
 *      parameters.
 */
template< typename StrategyT
        , template<typename> class PoolContainerAllocatorT=std::allocator
        >
class Pools {
private:
    /// Pools referenced by unique object index (statically resolved)
    /*static*/ AbstractPool ** _pools;
    /// Event memory allocation strategy
    StrategyT & _strategy;
public:

    /// Creates a set of pools managed by current strategy instance
    ///
    /// \todo container allocator parameters
    Pools( StrategyT & s )
            : _pools(nullptr)
            , _strategy(s)
            {
        // Use current strategy to allocate array with pool pointers
        typedef PoolContainerAllocatorT<AbstractPool *> PoolAllocator;
        PoolAllocator allocr;  //< todo: parameters for allocator?
        _pools = std::allocator_traits<PoolAllocator>::allocate( allocr
                    , NA64SW_MAX_POOLS );
        // Nullate the pointers array
        bzero( _pools, sizeof(AbstractPool*)*NA64SW_MAX_POOLS );
        assert(_pools);
    }

    /// Destroys allocated pools
    ~Pools() {
        // Free pools that have been created
        for( AbstractPool ** poolPtr = _pools
           ; poolPtr != _pools + NA64SW_MAX_POOLS
           ; ++poolPtr ) {
            if( ! *poolPtr ) continue;  // was not created
            typedef PoolContainerAllocatorT<AbstractPool> PoolAllocator;
            typedef std::allocator_traits<PoolAllocator> AlTraits;
            PoolAllocator allocr;  //< todo: parameters for allocator?
            // free the instance (though we do not know its particular type)
            AlTraits::destroy( allocr, *poolPtr );
            AlTraits::deallocate( allocr, *poolPtr, 1 );
        }
        {  // free array itself
            typedef PoolContainerAllocatorT<AbstractPool*> PoolAllocator;
            typedef std::allocator_traits<PoolAllocator> AlTraits;
            PoolAllocator allocr;  //< todo: parameters for allocator?
            AlTraits::deallocate( allocr, _pools, NA64SW_MAX_POOLS );
            _pools = nullptr;
        }
    }

    ///\brief Clears existing pools (typically, at the end of each event).
    /// 
    /// Deletes the content of the pools. Since most of the pool
    /// implementations do not actually release memory blocks associated to
    /// the underlying container object, keeping instances saves some CPU.
    void reset() {
        for( AbstractPool ** poolPtr = _pools
           ; poolPtr != _pools + NA64SW_MAX_POOLS
           ; ++poolPtr ) {
            if( ! *poolPtr ) continue;  // omit pools that were not created
            (*poolPtr)->clear();
        }
    }

    /// Returns pool of objects of certain type
    ///
    /// This access function creates, and/or returns reference to pool object
    /// of certain type.
    ///
    /// \todo Extend it with runtime-resolved pools using SFINAE?
    template<typename T> Pool<T, StrategyT> & pool_of() {
        assert(PoolTraits<T>::id < NA64SW_MAX_POOLS);
        #if 0  // XXX, workaround, a complicated unresolved bug
        // The `_pools` has to be initialized once the first instance of this
        // template is created. However, it appears to be null in certain
        // contexts, that I could not trace even with GDB `watch`. Valgrind
        // neither provided info.
        //#warning "FIXME: a weird static template member behaviour"
        if(!_pools) {
            // Use current strategy to allocate array with pool pointers
            typedef PoolContainerAllocatorT<AbstractPool *> PoolAllocator;
            PoolAllocator allocr;  //< todo: parameters for allocator?
            _pools = std::allocator_traits<PoolAllocator>::allocate( allocr
                        , NA64SW_MAX_POOLS );
            // Nullate the pointers array
            bzero( _pools, sizeof(AbstractPool*)*NA64SW_MAX_POOLS );
        }
        #endif
        assert(_pools);
        AbstractPool * absPoolPtr = _pools[PoolTraits<T>::id];
        if( ! absPoolPtr ) {
            // create pool
            //typedef Allocator<Pool<T, StrategyT>, StrategyT> Allocr;
            //typedef std::allocator_traits<Allocr> AlTraits;

            typedef PoolContainerAllocatorT< Pool<T, StrategyT> > PoolAllocator;
            typedef std::allocator_traits<PoolAllocator> AlTraits;
            PoolAllocator allocr;  //< todo: parameters for allocator?

            //Allocr allocr(_strategy);
            Pool<T, StrategyT> * poolPtr = AlTraits::allocate(allocr, 1);
            AlTraits::construct( allocr  // construct() arg -- allocator for ...
                               , poolPtr  // allocated Pool<> instance ptr to construct on
                               , mem::Allocator<Ref<T>, StrategyT>(_strategy)  // object allocator
                               , PoolContainerAllocatorT< Ref<T> >()  // container allocator, TODO?
                               );
            assert(poolPtr);
            absPoolPtr = _pools[PoolTraits<T>::id] = poolPtr;
        }
        return static_cast<Pool<T, StrategyT>&>(*absPoolPtr);
    }

    /// Implicit conversion operator to allocator subtype, needed by `event` ctrs
    template<typename T> operator Allocator<T, StrategyT>() { return _strategy; }

    /// Instantiates pools with default parameters.
    //static Pools<StrategyT> instantiate();
    // Instantiates pools with default parameters.
    //static Pools<StrategyT> instantiate( YAML:: ... );
    
    /// Shortcut for lmem.pool_of<T>().create(lmem, ...)
    template<typename T, class... Args> Ref<T> create(Args&&... args) {
        return this->pool_of<T>().create(std::forward<Args>(args)...);
    }
};

//template<typename StrategyT, template<typename> class PoolContainerAllocatorT>
//AbstractPool ** Pools<StrategyT, PoolContainerAllocatorT>::_pools = nullptr;

}
}

