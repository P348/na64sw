#pragma once

/**\file
 * \brief Provides implementation of simple short memory block
 *
 * A declaration of plain memory block allocation strategy.
 * */

#include "na64util/mem/alloc.hh"
#include "na64util/str-fmt.hh"

#include <cassert>

namespace na64dp {
namespace errors {

/**\brief Indicates exhaustion of the statically pre-allocated block */
class PreallocatedBlockExhausted : public GenericRuntimeError {
public:
    const void * blockPtr;
    const size_t preallocatedBlockLength
               , lastRequestedLength
               ;

    PreallocatedBlockExhausted( void * blockPtr_
                              , size_t preallocatedBlockLength_
                              , size_t lastRequestedLength_
                              ) : GenericRuntimeError( util::format(
                                      "Pre-allocated memory block %p of"
                                      " size %zub depleted with last"
                                      " allocation of size %zub."
                                      , blockPtr_
                                      , preallocatedBlockLength_
                                      , lastRequestedLength_
                                      ).c_str() )
                                , blockPtr(blockPtr_)
                                , preallocatedBlockLength(preallocatedBlockLength_)
                                , lastRequestedLength(lastRequestedLength_)
                                {}
};

}  // namespace ::na64dp::errors

namespace mem {

/**\brief A single, limited memory block
 *
 * This is an extremely simple allocator strategy implementation. It is
 * designed for a single-pass with low allocations (in fact, it does not
 * even de-allocates memory). It is expected that this allocator will
 * leverage CPU cache for computation-intensive operations by avoiding
 * memory fragmentation and benefit from CPU cache an small data chunks.
 *
 * Recommended usage is for short, CPU-intensive calculations with arithmetics,
 * container lookup, with few insertions of data ~10kbytes. Do not use it for
 * general purpose.
 *
 * \warning
 *      De-allocation of managed memory block is done only on the deletion of
 *      strategy itself.
 *  \warning
 *      Memory block capacity is limited, it won't resize on the overflow
 */
class PlainBlock {
public:
    typedef uint32_t Size_t;
private:
    char *_block  ///< First byte addr of mem block
       , *_blockEnd  ///< Last byte addr of mem block
       , *_cur  ///< Current position in mem block
       ;
protected:  // except for "Allocator<>"
    ///\brief Allocates memory sub-block
    ///
    /// Accessed by `Allocator` instance.
    template<typename T> T * acquire(Size_t sz) {
        _cur += sz*sizeof(T);
        if( _cur >= _blockEnd ) {
            throw errors::PreallocatedBlockExhausted(
                    _block, _blockEnd - _block, sz);
        }
        return reinterpret_cast<T *>(_cur - sz*sizeof(T));
    }
    ///\brief Frees memory sub-block
    ///
    /// Accessed by `Allocator` instance.
    template<typename T> void release(T * p, Size_t sz) {
        // ... do nothing
    }
    ///\brief Returns maximum capacity of objects of certain type
    ///
    /// Accessed by `Allocator` instance.
    template<typename T> Size_t max_for() const noexcept {
        return (_blockEnd - _cur)/sizeof(T);
    }
public:
    /// Explicitly prohibit the copy ctr to avoid copying
    PlainBlock( const PlainBlock & ) = delete;
    
    /// Constructs the block of certain size (in bytes)
    PlainBlock( void * blockBgn, size_t len ) {
        _block = _cur = static_cast<char*>(blockBgn);
        _blockEnd = _block + len;
    }

    /// Does nothing.
    ~PlainBlock() {;}

    /// Returns handle (allocator) to typed "pool"
    template<typename T> Allocator<T, PlainBlock> of() { return Allocator<T, PlainBlock>(*this); }

    /// Sets the "current" pointer to the beginning of the block, thus
    /// invalidating all previous allocations.
    void reset() { _cur = _block; }

    #if 0
    /// A "fast" object allocation method forwarding its arguments to ctr
    ///
    /// \returns `std::shared_ptr` with proper deleter
    template<typename T, typename... Args> std::shared_ptr<T>
    create_object( Args&&... args ) {
        return std::allocate_shared< T, Allocator<T, PlainBlock> >(
                of<T>(), std::forward<Args>(args)...);
    }
    #endif

    template<typename T, typename Strategy> friend class Allocator;
};

#if 0
/// A "fast" object allocation function acting on top of our default allocator
template<typename T, typename... Args> std::shared_ptr<T>
fast_new( PlainBlock & alloc, Args&&... args ) {
    return std::allocate_shared< T,
           na64dp::mem::Allocator<T, PlainBlock> >( alloc.of<T>(),
                   std::forward<Args>(args)...);
}
#endif

}  // namespace ::na64dp::mem
}  // namespace na64dp

