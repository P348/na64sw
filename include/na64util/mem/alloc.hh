#pragma once

#include "na64sw-config.h"

#include <memory>

/**\file
 * \brief Pool allocators
 *
 * These classes express the API to manage memory block in a bit less generic
 * way than is provided by default STL allocators.
 *
 * We presume that in the frame of memory allocation strategy, all
 * type-specialized allocators refer to single object (called "Strategy") which
 * alone maintains allocate/create/destroy lifecycle. This is a deduction
 * of what default STL logic foresees (where one can specify particular
 * allocators for every type with `allocator_traits`).
 *
 * This API simplifies usage of custom allocators.
 * For example usage see `PlainBlock`.
 * */

namespace na64dp {
namespace mem {

/// A pool for a single type, acting as a single-type allocator
///
/// Internally, keeps reference to a current `StrategyT` instance which,
/// in fact, manages memory access.
template<typename T, typename StrategyT>
class Allocator {
protected:
    StrategyT * _pools;
public:
    Allocator() = delete;
    Allocator(StrategyT & pools) : _pools(&pools) {}
    Allocator(const Allocator<T, StrategyT> & other) : _pools(other._pools) {}

    // compiler made me do this! I expected it rather use traits::rebind(),
    // but it seems to ignore it.
    Allocator & operator=(const Allocator<T, StrategyT> & other)
        { _pools = const_cast<Allocator<T, StrategyT>&>(other)._pools;
          return *this; }
    template<typename otherT> Allocator( const Allocator<otherT, StrategyT> & other )
        : _pools(const_cast<Allocator<otherT, StrategyT>&>(other)._pools) {}

    /// required typedef for `std::allocator` implementation
    using value_type = T;
protected:  // (except friend traits)   
    T * acquire(typename StrategyT::Size_t sz) { return _pools->template acquire<T>(sz); }
    void release( T* p, typename StrategyT::Size_t s) { _pools->template release<T>(p, s); }
    typename StrategyT::Size_t remaining_capacity() const noexcept { return _pools->template max_for<T>(); }

    // I'm not a master of friendship, may be this are an overkill.
    // In principle, friends must be restricted with same type of
    // `StrategyT', but T/otherT may vary.
    template<typename A> friend class ::std::allocator_traits;
    template<typename otherT, typename StrategyT2> friend class Allocator;
public:
    bool operator!=(const Allocator<T, StrategyT> & other) const {
        return _pools != other._pools;
    }
};

}  // namespace ::na64dp::mem
}  // namespace na64dp

// this must define `constexpr' for C++20 onwards; dunno compiler macro
// to control this at the moment
# define CPP20_ALLOC_CONSTEXPR
namespace std {
template<typename ObjT, typename StrategyT>
struct allocator_traits< na64dp::mem::Allocator<ObjT, StrategyT> > {
    typedef na64dp::mem::Allocator<ObjT, StrategyT> allocator_type;
    typedef ObjT value_type;
    typedef value_type* pointer;
    typedef const pointer const_pointer;
    typedef void * void_pointer;
    typedef typename StrategyT::Size_t difference_type;
    typedef typename StrategyT::Size_t size_type;

    typedef std::true_type propagate_on_container_copy_assignment;
    typedef std::true_type propagate_on_container_move_assignment;
    typedef std::true_type propagate_on_container_swap;
    typedef std::false_type is_always_equal; // (since c++17)

    static pointer allocate( allocator_type & pool
                           , size_type n ) {
        return pool.acquire(n);
    }

    static CPP20_ALLOC_CONSTEXPR void deallocate( allocator_type & pool
                                                , pointer ptr
                                                , size_type n ) {
        pool.release( ptr, n );
    }

    template< class T, class... Args >
    static CPP20_ALLOC_CONSTEXPR void construct( allocator_type & pool
                                               , T* ptr
                                               , Args&&... args ) {
        ::new (static_cast<void*>(ptr)) T(std::forward<Args>(args)...);
    }

    template< class T > static CPP20_ALLOC_CONSTEXPR
    void destroy( allocator_type & pool
                , T* p ) {
        p->~T();
    }

    static CPP20_ALLOC_CONSTEXPR size_type max_size( const allocator_type & pool ) noexcept {
        return pool.remaining_capacity();
    }

    //static CPP20_ALLOC_CONSTEXPR allocator_type
    //select_on_container_copy_construction(const allocator_type & o)
    //{ return o; }

    template<typename T> using rebind_alloc 
            = na64dp::mem::Allocator<T, StrategyT>;
    template<typename T> using rebind_traits = std::allocator_traits< rebind_alloc<T> >;

    // TODO: perhaps, this is not appropriate behaviour (in STL this is
    // implemented with SFINAE), but the intention was to prohibit allocator
    // copy construction.
    static allocator_type
        select_on_container_copy_construction( const allocator_type & a ) {
        return a;
    }
    //{   // get allocator to use
    //return (_Alloc_select::_Fn(0, _Al));
    //}
};
}  // namespace std
#undef CPP20_ALLOC_CONSTEXPR

