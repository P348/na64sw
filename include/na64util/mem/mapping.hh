#pragma once

#include "na64sw-config.h"

#include <memory>  // for shared_ptr

#include <map>
#include <vector>
#include <set>
#include <unordered_map>

/**\brief Object mapping declarations
 *
 * This header contains various definitions related to mapping API of `na64sw`.
 * The *mapping* establishes association between sets of objects of two certain
 * types.
 *
 * These maps rely on memory allocation strategies and this header provides
 * a somewhat generic code capable to cope with any strategy defined within
 * the `na64sw` memory API.
 *
 * The most important case is, of course, event data association to which we
 * have all kinds of mapping. To enhance the data retrieval, insertion and
 * deletion among collections of various parts of the event, we provide
 * mapping containers with custom allocators.
 */

namespace na64dp {

/// This enumeration is used to defer the particular mapping type based on the
/// "tags".
struct MapTag {
    enum Code {
        ordered = 0x1,      sparse = 0x2,       ambig = 0x4,
        ordered_sparse = ordered | sparse,
        ordered_ambig = ordered | ambig,
        sparse_ambig = sparse | ambig,
        sparse_ambig_ordered = sparse | ambig | ordered,
    };
};

template< typename KeyT
        , typename ValueT
        , MapTag::Code TTag
        , typename StrategyT
        > struct MapTraits;

/// Ordered, non-sparse, non-ambiguous container container traits
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::ordered, StrategyT> {
    constexpr static int mapTag = MapTag::ordered;
    typedef StrategyT Strategy_t;
    #if 0
    typedef std::vector< ValueT
                       , mem::Allocator< ValueT
                                       , StrategyT
                                       >
                       > type;
    #else
    typedef std::map< KeyT, ValueT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, ValueT>
                                    , StrategyT
                                    >
                    > type;
    #endif
};

/// Unordered, sparse, non-ambiguous container
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::sparse, StrategyT> {
    constexpr static int mapTag = MapTag::sparse;
    typedef StrategyT Strategy_t;
    typedef std::unordered_map< KeyT, ValueT
                              , std::hash<KeyT>
                              , std::equal_to<KeyT>
                              , mem::Allocator< std::pair<const KeyT, ValueT>
                                              , StrategyT
                                              >
                              > type;
};

/// Unordered, non-sparse, ambiguous container
/// \todo Currently is `std::unordered_multimap`, but must not allow gaps
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::ambig, StrategyT> {
    constexpr static int mapTag = MapTag::ambig;
    typedef StrategyT Strategy_t;
    typedef std::unordered_multimap< KeyT, ValueT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, ValueT>
                                                   , StrategyT
                                                   >
                                   > type;
};

/// Ordered, sparse, non-ambiguous container traits (perfectly corresponds to `std::map`)
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::ordered_sparse, StrategyT> {
    constexpr static int mapTag = MapTag::ordered_sparse;
    typedef StrategyT Strategy_t;
    typedef std::map< KeyT, ValueT
                    , std::less<const KeyT>
                    , mem::Allocator< std::pair<const KeyT, ValueT>
                                    , StrategyT
                                    >
                    > type;
};

/// Ordered, non-sparse, ambiguous container
///\todo Must not allow gaps (currenly is `std::multimap`)
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::ordered_ambig, StrategyT> {
    constexpr static int mapTag = MapTag::ordered_ambig;
    typedef StrategyT Strategy_t;
    typedef std::multimap< KeyT, ValueT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, ValueT>
                                         , StrategyT
                                         >
                        > type;
};

/// Unordered, sparse, ambiguous container
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::sparse_ambig, StrategyT> {
    constexpr static int mapTag = MapTag::sparse_ambig;
    typedef StrategyT Strategy_t;
    typedef std::unordered_multimap< KeyT, ValueT
                                   , std::hash<KeyT>
                                   , std::equal_to<KeyT>
                                   , mem::Allocator< std::pair<const KeyT, ValueT>
                                                   , StrategyT
                                                   >
                                   > type;
};

/// Ordered, sparse, ambiguous mapping container (perfectly corresponds to `std::multimap`
template< typename KeyT
        , typename ValueT
        , typename StrategyT
        >
struct MapTraits<KeyT, ValueT, MapTag::sparse_ambig_ordered, StrategyT> {
    constexpr static int mapTag = MapTag::sparse_ambig_ordered;
    typedef StrategyT Strategy_t;
    typedef std::multimap< KeyT, ValueT
                         , std::less<KeyT>
                         , mem::Allocator< std::pair<const KeyT, ValueT>
                                         , StrategyT
                                         >
                         > type;
};

}  // namespace na64dp

