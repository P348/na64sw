#pragma once

/**\file
 * \brief Provides foward declarations for memory allocators utils.
 *
 * Memory management classes used in the data processing are the subject of
 * use for a number of application. To decrease compilation time, the forward
 * declaration header is provided.
 */

#include "na64sw-config.h"

#include <memory>  // for std::allocator

namespace na64dp {
namespace mem {
template<typename T, template<typename> class PoolContainerAllocatorT> class Pools;
// Memory allocation strategies
class PlainBlock;
// ... (other strategies are to be added)
}

namespace event {
/// Default local memory management type for event processing
typedef mem::Pools<NA64SW_EVMALLOC_STRATEGY, std::allocator> LocalMemory;
}

}

