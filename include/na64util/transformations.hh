#pragma once

#include <cmath>

#include "na64sw-config.h"
#include "str-fmt.hh"
#include "na64util/numerical/linalg.hh"

//#if defined(ROOT_FOUND) && ROOT_FOUND
//#include <TVector3.h>
//#endif

namespace na64dp {
namespace util {

/// Template adapter for `na64sw_rotation_matrix_*()`
template<typename FloatT> Matrix3T<FloatT>
rotation_matrix( na64sw_RotationOrder, const FloatT * angles );

/// Forwards to `na64sw_rotation_matrix_f()`
template<> Matrix3T<float>
rotation_matrix<float>( na64sw_RotationOrder, const float * angles );

/// Forwards to `na64sw_rotation_matrix_d()`
template<> Matrix3T<double>
rotation_matrix<double>( na64sw_RotationOrder, const double * angles );

/**\brief Framework-wide standard rotation order
 *
 * This constant defines default axis order applied for the angular rotation
 * triplets wherever appliable.
 * */
extern const na64sw_RotationOrder gStdRotationOrder;

/**\brief Converts wire units to DRS-normalized coordinate
 *
 * This is widely used formula that maps wire number \f$n\f$ of \f$N\f$N
 * equidistant wires to range \f$(0, 1)\f$. Depending on wire coordinate type
 * half-stride shift for position can be applied (applied for integer types).
 *
 * \warning This function assumes that conversion from discrete elements
 *          (slabs, tubes, etc) is done from integer types while cluster
 *          centers and other floating point position values already account
 *          for half-stride shift.
 * */
template< typename PositionT> typename std::enable_if< std::is_floating_point<PositionT>::value
                                               , Float_t
                                               >::type
wire_to_normalized_DRS(PositionT nWire, size_t nWires) {
    return nWire/nWires - .5;
}

template< typename PositionT> typename std::enable_if< std::is_integral<PositionT>::value
                                               , Float_t
                                               >::type
wire_to_normalized_DRS(PositionT nWire, size_t nWires) {
    return (nWire + .5)/nWires - .5;
}

/**\brief Combined transformation (rotation, translation, scaling and shear)
 * 
 * This class defines complete set of affine transformation between two \f$\mathbb{R}_3\f$
 * spaces in terms of:
 * 
 * * a translation vector \f$\vec{o}\f$
 * * a rotation matrix \f$R\f$
 * * a scale/shear matrix \f$W\f$ composed from local bases vectors
 *   \f$\vec{u}\f$, \f$\vec{v}\f$, \f$\vec{w}\f$.
 * 
 * As a result, having a measurement vector \f$\vec{m}\f$ expressed in local
 * (detector's) frame one can obtain global coordinates as:
 * 
 * \f[
 *     \vec{r} = T(\vec{m}) = \vec{o} + R (W \vec{m})
 * \f]
 * 
 * This class can be constructed from (and can provide back) quantities
 * that are more convenient to deal with:
 * 
 *  * rotation angles triplet in certain (user-defined) order
 *  * offset vector
 *  * local bases vectors of dimensions length (usually corresponding to
 *    detector's sensitive volume).
 * 
 * \note Matrix of affine transform (rotation and scaling in local basis) can
 *       be used to "rotate" the covariance.
 * 
 * \warning Dealing with angles, please note that due to mathematical
 *          ambiguity returned angular values of \f$> \pi/2\f$ can not always
 *          be spatially resolved matching the exact input.
 * 
 * \todo Support or combination (merging) operations and transformations chaining
 * \todo Full support for inversion
 * \todo Method for covariance transformation
 * 
 * \ingroup numerical-utils
 * */
struct Transformation {
private:
    /// Set if affine and rotation matrix caches are valid
    mutable bool _cachesValid;
    /// (cache) spatial rotation matrix cache (order-invariant)
    mutable Matrix3 _rotation;
    /// (cache) shear and scale matrix (local basis)
    mutable Matrix3 _shearAndScale;
    /// (cache) full affine transformation matrix cache
    mutable Matrix3 _affineMatrix;
protected:
    /// Rotation order in use
    na64sw_RotationOrder _rotationOrder;
    /// True for inverted transformation
    ///
    /// Translation (offset) is not a linear transform, so to avoid homegenous
    /// coordinates we simply use a special flag for inverted transformations
    bool _invertOffset;
    /// Transformation offset
    Vec3 _offset;
    /// Local basis (\f$\vec{u}, \vec{v}, \vec{w}\f$) as shear+scale product.
    Vec3 _u, _v, _w;
    /// Rotation angles
    Float_t _angles[3];

    /// Recalculates affine transformation with respect to values set
    void _recache_affine_transforms() const;
    /// Resets "cache valid" flag (for descendant classes)
    void _invalidate_caches() { _cachesValid = false; }
public:
    /// Creates identity transformation
    Transformation();
    /// Creates transformation defined by values
    Transformation( const Vec3 & offset_
                  , const Vec3 & u_, const Vec3 & v_, const Vec3 & w_
                  , Float_t alpha, Float_t beta, Float_t gamma
                  , na64sw_RotationOrder rotationOrder=gStdRotationOrder
                  , bool invertOffset=false
                  );
    /// Copy ctr
    Transformation(const Transformation &);

    

    // simple getters/setters

    /// Returns `true` for reverse transformation
    bool is_inverse() const { return _invertOffset; }
    ///\brief Returns offset in global (lab) frame
    ///
    /// Offset vector set for the transformation.
    ///
    /// \note Returns same value of same meaning for direct and inverted
    ///       transform.
    const Vec3 & o() const { return _offset; }
    /// Sets the offset vector (in lab frame)
    ///
    /// Does not invalidate the cache with affine transformations
    void o(const Vec3 & o_) { _offset = o_; }

    /// Returns 1st basis vector
    const Vec3 & u() const { return _u; }
    ///\brief Returns 1st basis vector rotated to the global frame
    Vec3 gU() const { return rotation_matrix()*_u; }
    ///\brief Sets 1st basis vector
    ///
    /// Invalidates the cache of affine transformations
    void u(const Vec3 & u_) { _invalidate_caches(); _u = u_; }

    /// Returns 2nd basis vector
    const Vec3 & v() const { return _v; }
    ///\brief Returns 2nd basis vector rotated to the global frame
    Vec3 gV() const { return rotation_matrix()*_v; }
    ///\brief Sets 2nd basis vector
    ///
    /// Invalidates the cache of affine transformations
    void v(const Vec3 & v_) { _invalidate_caches(); _v = v_; }

    /// Returns 3rd basis vector
    const Vec3 & w() const { return _w; }
    ///\brief Returns 3rd basis vector rotated to the global frame
    Vec3 gW() const { return rotation_matrix()*_w; }
    ///\brief Sets 3rd basis vector
    ///
    /// Invalidates the cache of affine transformations
    void w(const Vec3 & w_) { _invalidate_caches(); _w = w_; }

    /// Returns rotation order
    na64sw_RotationOrder rotation_order() const { return _rotationOrder; }
    ///\brief Sets rotation order
    ///
    /// Invalidates cache of affine transformations
    void rotation_order(na64sw_RotationOrder order_)
        { _invalidate_caches(); _rotationOrder = order_; }

    ///\brief retruns N-th rotation angle
    ///
    /// Returns one of three rotation angles (called in ctr as alpha, beta,
    /// gamma) that can have different meaning depending on rotation order set.
    ///
    /// \throw Runtime error if index is out of range.
    Float_t angle(int n) const;
    ///\brief Sets N-th rotation angle
    ///
    /// Sets one of three rotation angles (called in ctr as alpha, beta,
    /// gamma) that can have different meaning depending on rotation order set.
    /// Invalidates cache of affine transformations.
    ///
    /// \throw Runtime error if index is out of range.
    void angle(int n, Float_t);


    // Complex getters and operations

    /// Returns rotation matrix
    const Matrix3 & rotation_matrix() const;
    /// Returns shear-and-scale transformation matrix built on local basis
    const Matrix3 & basis() const;
    /// Returns full affine transform matrix (without global offset)
    const Matrix3 & affine_matrix() const;

    /// Applies spatial transformation to the vector, modifying it
    void apply(Vec3 & m) const { m = this->operator()(m); }
    /// Shortcut for copying form of `apply()`
    Vec3 operator()(const Vec3 & m) const { return o() + affine_matrix()*m; }

    /// Returns inverse affine transformation
    Transformation inverted() const;
};

#if 0  // TODO
/**\brief Multiple ordered chained transformation
 *
 * Having chained representation of affine trnaforms is useful for ceertain
 * cases.
 * */
class TransformationChain : public std::vector<Transformation> {
public:
    /// Applies spatial transformation to the vector, modifying it
    void apply(Vec3 & m) const { m = this->operator()(m); }
    /// Shortcut for copying form of `apply()`
    Vec3 operator()(const Vec3 & m) const { return o() + affine_matrix()*m; }

    /// Returns inverse spatial transformation
    Transformation inverted() const;
};
#endif

#if 0
/**\brief Performs "standard" NA64sw rotation
 *
 * This function rotates point at given `r` by three angles provided in `rot`
 * array. The `rot` array should contain axes-sorted angles (X, Y, Z), while
 * the particular order of the rotations effectively applied is different
 * and is the subject of convention. */
void apply_std_rotation( Vec3 & r, const float rot[3]);

/**\brief Sets plane's cardinal vectors wrt given placements info
 *
 * Sets the cardinal vectors \f$o, u, v\f$ of the plane. \f$o\f$ is the
 * center in a global (lab) reference system, \f$u\f$ is ort vector for
 * measured coordinate and \f$v\f$ is the wire vector.
 *
 * Given length units will be preserved, angular measure is expected to be
 * given in radians.
 *
 * Order of spatial vector components (placement center and size) is
 * standard: x,y,z.
 *
 * Order of rotation angles is standard: x,y,z. Yet, note, that in NA64sw
 * rotation order is defined as sequence of intrinsic rotations (Tait-Btyan
 * angles) in order Z,Y,X, so Z rotation is applied first, then Y applied
 * within the modified frame and, finally, X rotation is applied in modified
 * frame.
 * */
void cardinal_vectors( const float center[3]
                     , const float size[3]
                     , const float rotation[3]
                     , Vec3 & o
                     , Vec3 & u, Vec3 & v, Vec3 & w
                     );
#endif

#define NA64SW_FOR_EVERY_UNITS( MACRO, ... )                    \
    /* length */                                                \
    MACRO( cm,               1,        "length" __VA_ARGS__ )   \
    MACRO( m,              100,        "length" __VA_ARGS__ )   \
    MACRO( mm,             0.1,        "length" __VA_ARGS__ )   \
    /* magnetic field induction */                              \
    MACRO( kGauss,           1, "mag.induction" __VA_ARGS__ )   \
    MACRO( Gauss,        0.001, "mag.induction" __VA_ARGS__ )   \
    MACRO( T,               10, "mag.induction" __VA_ARGS__ )   \
    /* energy */                                                \
    MACRO( GeV,              1,        "energy" __VA_ARGS__ )   \
    MACRO( MeV,          0.001,        "energy" __VA_ARGS__ )   \
    MACRO( keV,         1.0e-6,        "energy" __VA_ARGS__ )   \
    /* angle */                                                 \
    MACRO( rad,              1,         "angle" __VA_ARGS__ )   \
    MACRO( deg,      M_PI/180.,         "angle" __VA_ARGS__ )   \
    MACRO( mrad,          1e-3,         "angle" __VA_ARGS__ )   \
    /*...*/

struct Units {
    #define DECLARE_CONSTEXPR(unitName, unitNum, unitType) static constexpr float unitName = unitNum;
    NA64SW_FOR_EVERY_UNITS(DECLARE_CONSTEXPR)
    #undef DECLARE_CONSTEXPR
    
    /**\brief Returns converion factor for units in string form
     *
     * First argument must be one of the units to convert from.
     * Second argument may be either units to convert to, or expected name of
     * quantity to convert ("length", "energy", "mag.induction", etc), or empty
     * string. Then returned value depends on:
     * 
     *  - if second argument is name of the units, a multiplication factor to
     *    convert from `strUnitsFrom` to `strUnitsToOrQuantity` is returned. If
     *    conversion is requested for units of different quantity,
     *    an `errors::QuantityMismatch` exception is thrown;
     *  - if second argument is name of the quantity, a multiplication factor
     *    to convert from `strUnitsFrom` to standard NA64sw units is returned,
     *    checking that quantity of `strUnitsFrom` is of what
     *    is `strUnitsToOrQuantity`.
     *  - if second argument is empty string, a multiplication factor
     *    to convert from `strUnitsFrom` to standard NA64sw units is returned.
     *
     * If `strUnitsFrom` does not match to any of units (of certain quantity or
     * at all, an `errrors::UnknownUnit` or `errors::UnknownUnitOrQuantity` is
     * thrown.
     * */
    static float get_units_conversion_factor( const std::string & strUnitsFrom
                                            , const std::string & strUnitsToOrQuantity="" );
};

}  // namespace ::na64dp::util


//
// Errors

namespace errors {

/// Quantity unit name is unknown for the framework
class UnknownUnit : public errors::GenericRuntimeError {
public:
    const std::string unitName;
    const std::string unitOrQuantity;
    UnknownUnit( const std::string & unitName_ )
        : errors::GenericRuntimeError( util::format("Unknown unit name: \"%s\"", unitName_.c_str()).c_str())
        , unitName(unitName_)
        {}
    UnknownUnit( const std::string & unitName_
               , const std::string & unitOrQuantity_ )
        : errors::GenericRuntimeError( util::format("Unknown unit name: \"%s\""
                    " for conversion to/quantity \"%s\""
                    , unitName_.c_str()
                    , unitOrQuantity_.c_str()).c_str()
                )
        , unitName(unitName_)
        , unitOrQuantity(unitOrQuantity_)
        {}
};

/// Quantity mismatch during unit conversion
class QuantityMismatch : public errors::GenericRuntimeError {
public:
    const std::string unitName;
    const std::string expectedQuantity;
    QuantityMismatch( const std::string & unitName_
                    , const std::string & expectedQuantity_ )
        : errors::GenericRuntimeError( util::format("Quantity mismatch;"
                    " got \"%s\" units, expected %s quantity."
                    , unitName_.c_str()
                    , expectedQuantity_.c_str()
                    ).c_str())
        , expectedQuantity(expectedQuantity_)
        {}
};

}  // namespace ::na64dp::errors

}  // namespace na64dp

