#pragma once

#include <memory>
#include <typeindex>
#include <cxxabi.h>  // needed for abi::__cxa_demangle

namespace na64dp {
namespace util {

std::shared_ptr<char>
demangle_cpp(const char *abiName);

}
}
