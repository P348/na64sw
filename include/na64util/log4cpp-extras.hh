#pragma once

#include <log4cpp/LayoutAppender.hh>
#include <log4cpp/AppendersFactory.hh>
#include <log4cpp/LayoutsFactory.hh>

#include <unordered_map>
#include <mutex>

namespace na64dp {
namespace util {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
typedef std::auto_ptr<log4cpp::Appender> Log4cppAppendersFactoryResult_t;
typedef std::auto_ptr<log4cpp::Layout> Log4cppLayoutFactoryResult_t;
#pragma GCC diagnostic pop

/**\brief Adds support for some auxiliary logging facilities
 *
 * Will add following entities to `log4cpp` object factories:
 *  - Appenders:
 *      -- (TODO) "ostream-stdout" -- present in native `log4cpp` properties
 *          configurator, but weirdly absent in factory
 *      -- (TODO) "Geant4 UI" -- does nothing, if there is no running Geant4 session
 *
 * \todo JSON layout
 * \todo ZMQ/remote/EPI appender
 * */
void inject_extras_to_log4cpp();

/**\brief Custom stream layout appender
 *
 * Extends basic behaviour to print status line if FD is TTY. In principle,
 * should have exclusive access to standard output stream for a working
 * process.
 * */
class StdoutStreamAppender : public log4cpp::LayoutAppender {
protected:
    StdoutStreamAppender( const std::string & name
                        , int fd=1 );
    virtual ~StdoutStreamAppender()
        { close(); }
    /// log4cpp API -- does nothing for this class
    bool reopen() override { return true; }
    /// log4cpp API -- does nothing for this class
    void close() override {}
private:
    /// destination file descriptor
    int _fd;
    /// ptr to self instance, if instantiated
    static StdoutStreamAppender * _self;
    /// guards stdout
    std::mutex _lockStatusline;
    /// keeps status line to print
    char _statusline[128];
protected:
    /// log4cpp API -- actually prints an event
    void _append(const log4cpp::LoggingEvent & event) override;
public:
    /// return ptr to (singleton) instance or `null`
    static StdoutStreamAppender * self();
    /// sets the status text to be printed in console
    void update_status_text(const char *);

    friend Log4cppAppendersFactoryResult_t
        create_ostream_appender(const log4cpp::AppendersFactory::params_t & p);
};  // class StdoutStreamAppender

/**\brief Custom pattern layout for log4cpp
 *
 * This is an extended variation based on standard `log4cpp::PatternLayout`
 * class.
 *
 * Besides of same formatting characters it also has minor enhancements to
 * colorize the output, shorten level, etc. Coloring somehow became important
 * since our logs is the primary feedback to user about app being ran.
 * */
class PatternLayout : public log4cpp::Layout {
public:
    /// Layout component base class
    struct iComponent {
        virtual void append_msg(const log4cpp::LoggingEvent &, std::ostringstream &) = 0;
        virtual ~iComponent() {}
    };
    /// Component constructor
    typedef iComponent * (*ComponentConstructor)(const std::unordered_map<std::string, std::string> &);
    /// Additional tags for coloring/text decorations
    struct DecorationSuffixes {
        std::string titleOpen, titleClose, messageOpen, messageClose;
    };
protected:
    /// Dictionary of component constructors available for this type
    /// of pattern layout
    std::unordered_map<std::string, ComponentConstructor> _constructors;
    /// Active components
    std::vector<iComponent *> _components;
    /// Suffixes, by priority/100
    std::vector<DecorationSuffixes> _suffixes;
public:
    /// ctr (empty)
    PatternLayout();
    ~PatternLayout();
    /// Sets pattern to use
    void set_pattern(const std::string &);
    // Sets suffixes assets, TODO: implement it?
    //void set_coloring(const std::vector<DecorationSuffixes> &);
    /// String formatting implementation
    std::string format(const log4cpp::LoggingEvent &) override;

    /// Standard log4cpp creator function callback to cope with its native
    /// factory
    static Log4cppLayoutFactoryResult_t create(const log4cpp::LayoutsFactory::params_t &);
};  // class ColoredPatternLayout

}  // namespace ::na64dp::util
}  // namespace na64dp

