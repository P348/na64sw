#pragma once

#include <array>
#include <vector>
#include <cassert>
#include <memory>

#include "na64util/array-unwinding.hh"

namespace na64dp {
namespace util {

/**\brief Helper class representing stateful generator for cartesian product
 *
 * Initialized with a sequence of homogeneous STL containers, implements an
 * iterator over cartesian product results (pairs, triplets, quadruplets, etc).
 *
 * \ingroup numerical-utils
 * */
template<Arity_t NT, typename SeqT>
class CartesianProduct {
private:
    /// Set of ends for hits collections
    std::array<typename SeqT::const_iterator, NT> _begins
                                                , _state
                                                , _ends
                                                ;
public:
    template<typename ... ArgsT>
    CartesianProduct( ArgsT & ... args ) : _begins{args.begin()...}
                                         , _state(_begins)
                                         , _ends{args.end()...}
                                         {}
    CartesianProduct( CartesianProduct<NT, SeqT> && o)
        : _begins(std::move(o._begins))
        , _state(std::move(o._state))
        , _ends(std::move(o._ends)) {}
    /// Writes set of IDs to given destination, if possible. If not,
    /// returns `false`
    bool operator>>(std::array<typename SeqT::value_type, NT> & dest) {
        for( Arity_t i = 0; i < NT; ++i ) {
            if( _ends[i] == _state[i] ) return false;
            dest[i] = *_state[i];
        }
        ++_state[NT-1];
        // advance states
        for( Arity_t i = NT-1; i != 0; --i) {
            if( _state[i] != _ends[i] ) break;
            _state[i] = _begins[i];
            ++_state[i-1];
        }
        return true;
    }

    void reset() { _state = _begins; }
};

template<typename ObjectT, typename ArgT, Arity_t... Ns> ObjectT
unwind_array_to_constructor( const ArgT && arg, std::index_sequence<Ns...> ) {
    return ObjectT( std::move(arg [Ns]) ... );
}

template<typename ObjectT, typename ArgT, Arity_t... Ns> std::shared_ptr<ObjectT>
unwind_array_to_constructor_shared( ArgT && arg, std::index_sequence<Ns...> ) {
    return std::shared_ptr<ObjectT>( std::move(arg [Ns]) ... );
}

//template<typename ObjectT, typename ArgT, Arity_t... Ns> ObjectT
//unwind_array_to_constructor( const ArgT & arg, std::index_sequence<Ns...> ) {
//    return ObjectT( arg [Ns] ... );
//}
//
//template<typename ArgT, typename CallableT, Arity_t... Ns> auto
//unwind_array_to_call( CallableT c, const ArgT & arg, std::index_sequence<Ns...> ) {
//    return c( arg [Ns] ... );
//}

/**\brief Product result in array of fixed arity
 *
 * \ingroup numerical-utils
 * */
template<typename T, Arity_t NT>
struct CartesianProductCollections : protected std::array<std::vector<T>, NT> {
    /// Adds new element into n-th set
    void add(Arity_t n, const T & element) {
        assert(n < NT);
        std::array<std::vector<T>, NT>::operator[](n).push_back(element);
    }
    /// Clears collected sets
    void clear() {
        for(Arity_t n = 0; n < NT; ++n) {
            std::array<std::vector<T>, NT>::operator[](n).clear();
        }
    }

    auto get() const {
        return unwind_array_to_constructor< CartesianProduct< NT, std::vector<T> >
                , std::array<std::vector<T>, NT>
                >( std::move(static_cast<const std::array<std::vector<T>, NT>&>(*this))
                 , std::make_integer_sequence<Arity_t, NT>{} );
    }
};

}  // namespace ::na64dp::util
}  // namespace na64dp

