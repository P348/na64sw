#pragma once

#include "na64detID/TBName.hh"
#include "na64calib/dispatcher.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include "na64detID/TBName.hh"
#include "na64util/str-fmt.hh"
#include "na64detID/TBNameErrors.hh"

#include <TDirectory.h>
#include <TFile.h>
#include <map>

namespace na64dp {
namespace util {

/**\brief A ROOT's TDirectory manager helper class.
 *
 * Implements lyfecycle support for TDirectory instance for
 * classes that use ROOT histograming functions, etc. Helps to organize
 * entities in the output ROOT file, by catalogues. Relies on structure
 * provided by naming.
 *
 * \todo recaching (`handle_update()` implem)
 * \todo parallel access guarding locks?
 * */
template< typename KeyT
        , typename HashT=std::hash<KeyT>
        , typename KeyEqualT=std::equal_to<KeyT>
        >
class GenericTDirAdapter
            : public util::Observable<nameutils::DetectorNaming>::iObserver {
protected:
    std::unordered_map<KeyT, TDirectory *, HashT, KeyEqualT> _rootDirs;
    /// Recached existing `_rootDirs` to correspond new detector IDs
    void handle_update( const nameutils::DetectorNaming & tbn ) override {
        if( _rootDirs.empty() ) {  // first update, no recaching needed
            _names = &tbn;
            return;
        }
        NA64DP_RUNTIME_ERROR( "TODO: recache TDirAdapter dict" );
        // ^^^ if you've got this exception, then recaching of TBNames mapping
        // happened after a while. It is foreseen use case, albeit rarely needed,
        // so we postponed implementation for uncertain future...
    }
    /// Ptr to the name mappings (null until first update)
    const nameutils::DetectorNaming * _names;
public:
    /// Creates an instance with certain layout scheme.
    GenericTDirAdapter( calib::Dispatcher & dsp
                      , const std::string & namingClass="default"
                      )  : _names(nullptr) {
        dsp.subscribe<nameutils::DetectorNaming>(*this, namingClass);
    }
    /// Returns full substitution dictionary for certain detectors to be used
    /// in string templates
    util::StrSubstDict
                subst_dict_for(KeyT k, const std::string & histName) const {
        util::StrSubstDict ctx;
        ctx["hist"] = histName;
        nameutils::DetectorIDStrSubstTraits<KeyT>::append_context(naming(), k, ctx);
        return ctx;
    }
    /// Creates (or returns if exists) a TDirectory for entity identified
    /// by certain detector ID.
    ///
    /// \arg did DetectorID
    /// \arg hstNm histogram name string that may be superseded by naming template
    /// \arg nm Current name mappings
    TDirectory *
        dir_for( KeyT k
               , std::string & hstNm
               , const std::string & overridenTemplate="" ) {
        util::StrSubstDict ctx = subst_dict_for(k, hstNm);
        auto p = dir_for(k, ctx, overridenTemplate);
        hstNm = p.first;
        return p.second;
    }
    /// Overloaded version of `dir_for()` accepting external string context for
    /// formatting
    std::pair<std::string, TDirectory *>
        dir_for( KeyT k
               , const util::StrSubstDict & ctx
               , const std::string & overridenTemplate="" )  {

        const std::string * strPathTmplPtr = &overridenTemplate;
        if( strPathTmplPtr->empty() ) {
            strPathTmplPtr = nameutils::DetectorIDStrSubstTraits<KeyT>
                    ::path_template_for(naming(), k);
            if( strPathTmplPtr->empty() ) {
                NA64DP_RUNTIME_ERROR( "No path template available for"
                        " detector ID key(s)." );  // TODO: elaborate reason
                // ^^^ this exception means that no path template is available
                //     for certain key. It was not defined by traits (for
                //     instance, if key is pair, there no default mechanism to
                //     provide such path).
            }
        }
        std::string path;
        try {
            path = util::str_subst(
                    *strPathTmplPtr,
                    ctx );
            util::assert_str_has_no_fillers(path);
        } catch( errors::StringIncomplete & e ) {
            std::ostringstream oss;
            bool isFirst = true;
            for( const auto & p : ctx ) {
                oss << (!isFirst ? ", " : "")
                    << "\"" << p.first << "\": \"" << p.second << "\"";
                isFirst = false;
            }
            log4cpp::Category::getInstance("utils.TDirAdapter")
                << log4cpp::Priority::WARN
                << "`StringIncomplete' error raised during rendering of path"
                    " for entity. Entries available in the context: "
                << oss.str() << ".";
            throw;
        }
        // Get last token that typically is the histogram name and put it into
        // histName
        size_t pos = path.rfind('/');
        std::string dirPath, histName;
        if( std::string::npos != pos ) {
            dirPath = path.substr( 0, pos );
            histName = path.substr( pos+1, std::string::npos );
            TDirectory * tdPtr = mkdir_p( dirPath );
            if( !tdPtr ) {
                NA64DP_RUNTIME_ERROR( "Failed to create TDirectory \"%s\" (for"
                        " histogram \"%s\").", dirPath.c_str(), histName.c_str() );
            }
            _rootDirs.emplace( k, tdPtr );
            return std::make_pair(histName, tdPtr);
        } else {
            histName = path;
            _rootDirs.emplace( k, gFile );
            return std::make_pair(histName, gFile);
        }
    }

    /// Returns a map of directories indexed by detector IDs and created by
    /// this class
    decltype(_rootDirs) & tdirectories() { return _rootDirs; }
    /// Returns reference to a naming object
    const nameutils::DetectorNaming & naming() const {
       assert(_names);  // premature dereferencing
        return *_names;
    }

    /// Creates full path of `TDirectory`(es) recursively, similar as
    /// `mkdir -p` does in shell, starting from `gFile`'s root
    static TDirectory * mkdir_p( const std::string & path) {
        TDirectory * tdPtr = gFile;
        size_t prevPos = 0;
        while( std::string::npos != prevPos ) {
            size_t pos = path.find('/', prevPos);
            std::string cDirName = path.substr( prevPos, pos - prevPos );
            assert(!cDirName.empty());
            TDirectory * subDPtr = tdPtr->GetDirectory( cDirName.c_str() );
            if( !subDPtr ) {
                subDPtr = tdPtr->mkdir( cDirName.c_str(), "" );
            }
            assert(subDPtr);
            tdPtr = subDPtr;
            prevPos = std::string::npos == pos ? pos : pos + 1;
        }
        return tdPtr;
    }
};

/// Most frequently used instantiation of `GenericTDirAdapter<>` template
typedef GenericTDirAdapter<DetID> TDirAdapter;

}  // namespace ::na64dp::util
}  // namespace na64dp

#endif

