#pragma once

#include "na64util/exception.hh"

#include <string>
#include <stdexcept>
#include <map>
#include <vector>
#include <algorithm>
#include <functional>
#include <sstream>

namespace na64dp {
namespace util {

/// A C++ bastard of printf() function
std::string format(const char *fmt, ...) throw();

/// String substitution dictionary type
typedef std::map<std::string, std::string> StrSubstDict;

extern const struct StringSubstFormat {
    char bgnMarker[4], endMarker[4];
} gDefaultStrFormat;

/**\brief Renders string template wrt given context dictionary
 *
 * This function performs substitution in string expression `pattern` using the
 * set of key+value pairs provided by `context`.
 *
 * Produces output relying on format string and a map of key-value pairs.
 * E.g. for string template (1st argument) `"Hi, {user}!"` and pair
 * `"user" -> "Joe"` the output string will be `"Hi, Joe!"`. This function is
 * used across the library to "render" various "string templates" producing
 * unique string for various detector types, messages, etc.
 *
 * If `requireCompleteness` is invokes `assert_str_has_no_fillers()` on result.
 *
 * \todo Recusrsive subst (e.g. "{kin}" -> "{kin}{statNum}") leads to
 *       infinite loop. Add exception for this.
 */
std::string str_subst( const std::string & pattern
                     , const StrSubstDict & context
                     , bool requireCompleteness=true
                     , const StringSubstFormat * fmt=&gDefaultStrFormat );

/// Returns if the string has no template fillers
bool str_has_no_fillers( const std::string &, const StringSubstFormat * fmt=&gDefaultStrFormat );

/// Raises an exception if string has template fillers
void assert_str_has_no_fillers( const std::string &, const StringSubstFormat * fmt=&gDefaultStrFormat );

/// A generic utility function splitting space-separated tokens with
/// single-leveled commas expression support.
std::vector<std::string> tokenize_quoted_expression(const std::string &);

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

/// Traits for context substitution; shall provide `append_dict(dct, val)`
template<typename T> struct StringSubstContextTraits;

/// Performs substitution of the string with placeholders with respect to
/// context dictionary taken from traits
template<typename T> std::string
str_subst_val( const T & val
             , const std::string & pattern
             , bool requireCompleteness=true
             , const StringSubstFormat * fmt=&gDefaultStrFormat
             ) {
    StrSubstDict dct;
    StringSubstContextTraits<T>::append_dict(dct, val);
    return str_subst(pattern, dct, requireCompleteness, fmt);
}

/// Replaces phrase in a string
std::string
str_replace( std::string src
           , const std::string & search
           , const std::string & replace
           );

/**\brief Converts string expression to boolean option
 *
 * Kind of similar to `lexical_cast<bool>()` proposed by many libs around:
 * converts string option that is supposed to be boolean option.
 * Case-insensitive.
 * Variants yielding `true`: "1", "yes", "true", "enable", "on".
 * Variants yielding `false`: null ptr, "0", "no", "false", "disable", "off", empty string.
 * All other strings lead to `errors::InvalidOptionExpression` error.
 * */
bool str_to_bool(std::string);

/**\brief Concatenates string-convertible items into delimited string
 *
 * This templated function comes in hand at many places to produce
 * human-friendly form of comma-separated strings, mostly from STL-compatible
 * containers. */
template<typename IteratorT> std::string
str_join( IteratorT itemsBgn
        , IteratorT itemsEnd
        , const std::string & delimiter=", "
        , std::function<std::string(IteratorT)> f=[](IteratorT tokIt){return *tokIt;}
        ) {
    std::ostringstream oss;
    bool isFirst = true;
    for(IteratorT it = itemsBgn; it != itemsEnd; ++it) {
        if(isFirst) isFirst = false; else oss << delimiter;
        oss << f(it);
    }
    return oss.str();
}

/**\brief Time string formatting function
 *
 * Converts real value which is assumed to be rough estimation of seconds
 * into human-readable string:
 *
 *      [<hours>h][<minutes>m]<seconds>.<mseconds>
 *
 * Time formatted this way is widely used for logging as it seems to be a good
 * tradeoff between precision, simplicity and a kind of compact form for
 * typical application lifetime (from few seconds up to few hours). */
size_t seconds_to_hmsm(float seconds, char * dest, size_t destLen);

/**\brief Parses time+date string expression of somewhat standard form
 *
 * Generally, this function is to fix certain time+date standard format across
 * NA64sw framework.
 *
 * Examples: "2018-05-14-20:14:57", "2016-07-09-18:50:42", i.e. corresponding
 * `strptime()`-format is `"%s-%m-%d-%H:%M:%S"`.
 *
 * Symmetric to `str_format_timedate()`.
 *
 * \note technically, returned value is a real timestamp only at current
 *       timezone
 * */
time_t
parse_timedate_strexpr( const char * c );

/**\brief Returns time+date string expression of given `time_t` struct
 *
 * Symmetric to `parse_timedate_strexpr()`.
 *
 * \note technically, returned value is a real timestamp only at current
 *       timezone
 * */
std::string
str_format_timedate(const time_t &);

}  // namespace ::na64dp::util

namespace errors {

class NoDataInEventError : public GenericRuntimeError {
public:
    NoDataInEventError( const char * s ) throw() : GenericRuntimeError( s ) {}
};

class StringIncomplete : public std::runtime_error {
private:
    std::string _tok;
    std::string _s;
public:
    StringIncomplete( const std::string & s, const std::string & tok ) throw()
            : std::runtime_error( util::format( "String \"%s\" has template markup"
                        " after substitution.", s.c_str() ) )
            , _tok(tok)
            , _s(s)
            {}
    const std::string & incomplete_string() const throw() {
        return _s;
    }

    const std::string & token() const throw() {
        return _tok;
    }
};  // class StringIncomplete

class InvalidOptionExpression : public GenericRuntimeError {
public:
    InvalidOptionExpression( const std::string & expr ) throw()
            : GenericRuntimeError( util::format( "Invalid option expression: \"%s\"", expr.c_str() ).c_str() )
            {}
};

}  // namespace na64dp::errors
}  // namespace na64dp

#ifndef NA64DP_RUNTIME_ERROR
#define NA64DP_RUNTIME_ERROR( fmt, ... ) {                                    \
    throw ::na64dp::errors::GenericRuntimeError(::na64dp::util::format(       \
                "at %s:%d: " fmt, __FILE__, __LINE__, ##__VA_ARGS__           \
            ).c_str() );                                                      \
}
#endif

#ifndef NA64DP_ASSERT_EVENT_DATA
#define NA64DP_ASSERT_EVENT_DATA( data, fmt, ... ) if(!data){                 \
    throw ::na64dp::errors::NoDataInEventError(::na64dp::util::format(        \
                "at %s:%d: requested data is not set/does not exist in the"   \
                " event (" fmt ").", __FILE__, __LINE__, ##__VA_ARGS__        \
            ).c_str());                                                       \
}
#endif
