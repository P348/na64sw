#pragma once

#include "na64sw-config.h"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace errors {
class GSLError : public GenericRuntimeError {
public:
    const int gslErrorCode;
protected:
    GSLError( int gslErrorCode_
            , const char * );
public:
    GSLError( int gslErrorCode_ );

    static void
    na64sw_gsl_error_handler( const char * reason
                            , const char * file
                            , int line
                            , int gsl_errno );
};  // class GSLError

}  // namespace ::na64dp::errors
}  // namespace na64dp

