#pragma once

#include <streambuf>

namespace na64dp {
namespace util {

/**\brief A streambuf subclass tracking current reading position.
 *
 * This `std::streambuf` subclass is designed for human-readable ASCII files
 * where it is often convenient to refer a line/column number to identify
 * (last) reading position. Use this helper for more descriptive identification
 * and/or diagnostics of the issues appeared in the input text files.
 *
 * \code{.cpp}
 *  std::ifstream file("somefile.txt");
 *  // "pipe" through counting stream buffer
 *  util::CountingStreamBuffer cntstreambuf(file.rdbuf());
 *  std::istream is(&cntstreambuf);
 * \endcode
 *
 * Based on [this answer on StackOverflow](https://stackoverflow.com/a/4821494/1734499).
 * */
class CountingStreamBuffer : public std::streambuf {
private:
    std::streambuf *    _streamBuf;  ///< hosted streambuffer
    size_t _lineNo  ///< current line number
         , _lastLineNo  ///< line number of last read character
         , _col  ///< current column
         , _prevCol  ///< previous column
         ;
    std::streamsize _filePos;       /// file position
protected:
    CountingStreamBuffer(const CountingStreamBuffer&);
    CountingStreamBuffer& operator=(const CountingStreamBuffer&);

    /// extract next character from stream w/o advancing read pos
    std::streambuf::int_type underflow() {
        return _streamBuf->sgetc();
    }

    // extract next character from stream
    std::streambuf::int_type uflow() {
        int_type rc = _streamBuf->sbumpc();
        _lastLineNo = _lineNo;
        if (traits_type::eq_int_type(rc, traits_type::to_int_type('\n'))) {
            ++_lineNo;
            _prevCol = _col + 1;
            _col = static_cast<size_t>(-1);
        }

        ++_col;
        ++_filePos;
        return rc;
    }

    /// put back last character
    std::streambuf::int_type pbackfail(std::streambuf::int_type c) {
        if (traits_type::eq_int_type(c, traits_type::to_int_type('\n'))) {
            --_lineNo;
            _lastLineNo = _lineNo;
            _col = _prevCol;
            _prevCol = 0;
        }
        --_col;
        --_filePos;
        if (c != traits_type::eof())
            return _streamBuf->sputbackc(traits_type::to_char_type(c));  
        else 
            return _streamBuf->sungetc();
    }

    /// change position by offset, according to way and mode  
    virtual std::ios::pos_type seekoff( std::ios::off_type pos
                                      , std::ios_base::seekdir dir
                                      , std::ios_base::openmode mode ) override {
        if( dir == std::ios_base::beg
         && pos == static_cast<std::ios::off_type>(0)) {
            _lastLineNo = 1;
            _lineNo = 1;
            _col = 0;
            _prevCol = static_cast<size_t>(-1);
            _filePos = 0;
            return _streamBuf->pubseekoff(pos, dir, mode);
        } else {
            return std::streambuf::seekoff(pos, dir, mode);
        }
    }

    /// change to specified position, according to mode
    virtual std::ios::pos_type seekpos( std::ios::pos_type pos
                                      , std::ios_base::openmode mode ) override {
        if (pos == static_cast<std::ios::pos_type>(0)) {
            _lastLineNo = 1;
            _lineNo = 1;
            _col = 0;
            _prevCol = static_cast<unsigned int>(-1);
            _filePos = 0;
            return _streamBuf->pubseekpos(pos, mode);
        } else {
            return std::streambuf::seekpos(pos, mode);
        }
    }
public:
    /// Ctr, accpets ptr to `std::streambuf` instance to wrap
    CountingStreamBuffer(std::streambuf* sbuf)
            : _streamBuf(sbuf)
            , _lineNo(1)
            , _lastLineNo(1)
            , _col(0)
            , _prevCol(static_cast<unsigned int>(-1))
            , _filePos(0) {}
    /// Get current line number
    size_t line_no() const  { return _lineNo; }
    /// Get line number of previously read character
    size_t prev_line_no() const { return _lastLineNo; }
    /// Get current column
    size_t column() const { return _col; }
    /// Get file position
    std::streamsize filepos() const { return _filePos; }
};

}
}

