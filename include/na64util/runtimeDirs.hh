#pragma once

#include <string>
#include <vector>
#include <sstream>

namespace na64dp {
namespace util {

/**\brief Class represents a cached state for accessible directories.
 *
 * Helper class keeping a set of directories to perform file lookup. Designed
 * for reentrant usage to speedup momentary usage (does not update itself
 * automatically).
 *
 * Internally a POSIX `glob()` is used to locate file(s).
 *
 * \note thread-unsafe due to `glob()`
 * */
class RuntimeDirs : public std::vector<std::string> {
private:
    std::stringstream _mlLog;
public:
    /**\brief Accepts colon-separated list of directories
     *
     * During instantiation, filters existing, accessible directories and puts
     * them into parental class (vector) for subsequent usage.
     * */
    RuntimeDirs( const char * locations="." );

    /// Find file(s)  by given pattern
    std::vector<std::string> locate_files( const std::string & ) const;

    /// Returns internal log reference for 
    std::string dir_lookup_log() const { return _mlLog.str(); }

    #if 0
    /**\brief Locates file(s) in directory(-ies)
     *
     * For given path (or columns-separated list of directories) and given file(s)
     * (or wildcard) performs search for files. Both, paths and name patterns may
     * be a column-separated lists.
     *
     * Upon successful lookup a string is returned containing the path to a file
     * found first in the paths list. If `multiple` is `true`, returned string may
     * contain multiple file paths delimited with column.
     * */
    static std::string find_files_by_name_in( const std::string & path
                                            , const std::string & namePattern
                                            , bool multiple=false
                                            );
    #endif
};

}
}

