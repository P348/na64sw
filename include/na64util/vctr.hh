#pragma once

#include <typeinfo>
#include <typeindex>
#include <stdexcept>
#include <map>
#include <memory>
#include <cassert>
#include <iostream>

namespace na64dp {

namespace errors {

/// Excpetion thrown by virtual constructor instance when requested name is not
/// found among registered entries of certain type.
class CtrNotFound : public std::runtime_error {
private:
    char _name[128];
    const std::type_info & _typeInfo;
public:
    CtrNotFound( const std::string & name
               , const std::type_info & typeInfo ) throw();
    const char * name() const throw() { return _name; }
    const std::type_info & type_info() throw() { return _typeInfo; }
};

/** Excpetion thrown by virtual constructor when there are
 * no entries defined for requested type.
 *
 * \note This exception is thrown when the abstract type for the entities is
 * defined together with its traits, but no entry was added until the
 * construction was requested. Contrary, if VCtr is parameterized with
 * incorrect type you got compiling error instead of this runtime exception as
 * traits are not defined.
 */
class NoCtrsOfType : public std::runtime_error {
private:
    const std::type_info & _typeInfo;
public:
    NoCtrsOfType( const std::type_info & typeInfo ) throw();
    const std::type_info & type_info() throw() { return _typeInfo; }
};

};

/// Virtual constructor traits; shall define `Constructor` type
template<typename T> struct CtrTraits;

/**\brief Common virtual constructor dispatcher implementation
 *
 * The "virtual constructor" is a common idiom that implies instantiation of
 * classes with some common base by runtime arguments. This class implements
 * a generalized registry for virtual multiple constructors based on the C++
 * type ID of the bases.
 *
 * \note Dynamically loaded modules uses this singleton before main logging
 *       is initialized.
 */
class VCtr {
public:
    /// Abstract base class for all virtual constructors
    class iCtrRegistry {};

    /// Virtual constructor registry -- indexes constructors of classes with
    /// common ancestor by their string identifiers (names)
    template<typename T>
    class CtrRegistry : public iCtrRegistry
                      , public std::map< std::string
                                       , std::pair<typename CtrTraits<T>::Constructor, std::string> > {
    public:
        /// Registers new constructor
        bool register_class( const std::string & name
                           , typename CtrTraits<T>::Constructor ctr
                           , const std::string & description ) {
            auto ir = this->emplace( name, std::make_pair(ctr, description) );
            return ir.second;
        }
        /// Constructs typed entry
        template<typename... ArgsT> T *
        make( const std::string & name, ArgsT &... args) {
            auto it = this->find(name);
            if( this->end() == it ) {
                throw errors::CtrNotFound( name, typeid(T) );
            }
            return it->second.first( args... );
        }
    };
private:
    /// Pointer to global self instance of the registry
    static VCtr * _self;
    /// Index of all registries
    std::map< std::type_index, std::unique_ptr<iCtrRegistry> > _regs;
public:
    /**\brief Returns registry instance of certain base type (const)
     *
     * Raises `NoCtrsOfType` exception if type does not exist.
     */
    template<typename T> const CtrRegistry<T> & registry() const {
        auto it = _regs.find( typeid(T) );
        if( _regs.end() == it ) {
            // In principle, this exception shall never be thrown 
            throw errors::NoCtrsOfType(typeid(T));
        }
        const CtrRegistry<T> * reg = static_cast<const CtrRegistry<T> *>(it->second.get());
        return *reg;
    }
    /**\brief Returns registry instance of certain base type
     *
     * Raises `NoCtrsOfType` exception if type does not exist.
     */
    template<typename T> CtrRegistry<T> & registry() {
        return const_cast<CtrRegistry<T> &>(const_cast<const VCtr*>(this)->registry<T>());
    }

    /// Registers new entry in the corresponding registry
    template<typename T> bool
    register_class( const std::string & name
                  , typename CtrTraits<T>::Constructor ctr
                  , const std::string & description ) {
        auto it = _regs.find( typeid(T) );
        if( _regs.end() == it ) {
            auto ir = _regs.emplace( typeid(T), new CtrRegistry<T>() );
            assert( ir.second );
            it = ir.first;
        }
        CtrRegistry<T> * regPtr = static_cast<CtrRegistry<T>*>(it->second.get());
        return regPtr->register_class(name, ctr, description);
    }

    /// Constructs entity of type `T` with arguments provided by args list.
    template<typename T, typename... ArgsT> T *
    make( const std::string & name, ArgsT & ... args ) {
        return registry<T>().make( name, args... );
    }

    /// Returns global instance of the virtual constructor object
    static VCtr & self() {
        if( _self ) return *_self;
        return *( _self = new VCtr() );
    }
};

}  // namespace na64dp

/* \defgroup vctr-defs Virtual constructor definitions
 *
 * This group contains C/C++ preprocessor macros that define a runtime
 * extension of a certain type. They typically start with `REGISTER_` prefix
 * and are used to shorten the definition of new "virtual constructor"
 * function (see `VCtr`).
 *
 * Note that these macros not necessarily accomplish the type registration
 * task -- one can still invoke the `VCtr<>::register_class()` directly. Though
 * these macros significantly improve readability, they migt be avoided in
 * favor of direct calls in case of, say, explicit template instantiation. */
