#pragma once

#include "na64util/str-fmt.hh"
#include "na64util/observer.hh"
#include "na64util/pair-hash.hh"

#include <unordered_map>
#include <unordered_set>
#include <typeindex>

namespace na64dp {

namespace error {
/// Exception thrown if type of an observable value does not correspond to any
/// observable in obserbles set
class UnknownObservableType : public std::runtime_error {
private:
    const std::type_info & _typeInfo;
public:
    UnknownObservableType( const std::type_info & ti ) throw()
        : std::runtime_error(
                util::format( "No observable of type \"%s\" in set."
                             , ti.name() ).c_str()
            )
        , _typeInfo(ti) {}
    const std::type_info & type_info() const throw() { return _typeInfo; }
};

/// Exception thrown on double association attempt
class DuplicatingObservableType : public std::runtime_error {
private:
    const std::type_info & _typeInfo;
public:
    DuplicatingObservableType( const std::type_info & ti ) throw()
        : std::runtime_error(
                util::format( "Observable of type \"%s\" exists in set."
                             , ti.name() ).c_str()
            )
        , _typeInfo(ti) {}
    const std::type_info & type_info() const throw() { return _typeInfo; }
};
}

namespace util {

/// Template implementation of observer pattern for certain type of messages
template<typename T>
class Observable {
public:
    /// Observer (subscriber) interface
    struct iObserver {
        virtual void handle_update( const T & ) = 0;
    };
private:
    /// A set of observers to be notified on change
    std::unordered_set<iObserver *> _observers;
public:
    /// Injects dependance (binds observer)
    virtual bool bind_observer( iObserver & dep )
            { return _observers.insert(&dep).second; }
    /// Removes dependence (unbinds observer)
    virtual void unbind_observer( iObserver & dep )
            { _observers.erase(&dep); }
    /// Notifies all the observers
    virtual void notify_observers( const T & d )
            { for( auto p : _observers ) p->handle_update( d ); }
    /// Returns number of bound observers
    virtual size_t n_observers() const { return _observers.size(); }
};

/** Implements a set of observable values indexed by their type (RTTI used)
 *
 * The idea is to dynamically dispatch changes among observers for certain
 * type. No additional filtration is provided at this level.
 */
class Observables {
public:
    struct CommonObservable {};
    template<typename T> struct ConcreteObservable : public CommonObservable
                                                   , public Observable<T> {};
protected:
    /// Indexes observables by their `std::type_index`
    std::unordered_map<std::type_index, CommonObservable *> _observables;
public:
    /// Adds new observable instance to set
    template<typename T> void
    add_observable( ConcreteObservable<T> & observable ) {
        auto ir = _observables.emplace( std::type_index(typeid(T)), &observable );
        if( ! ir.second ) {
            throw error::DuplicatingObservableType( typeid(T) );
        }
    }

    /// Returns observable instance reference when parameterized by type of the
    /// observable variable
    template<typename T> ConcreteObservable<T> &
    get_observable() {
        auto it = _observables.find( std::type_index(typeid(T)) );
        if( _observables.end() == it ) {
            throw error::UnknownObservableType( typeid(T) );
        }
        return static_cast<ConcreteObservable<T> &>(*(it->second));
    }
};

}
}

