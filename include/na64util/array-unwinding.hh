#pragma once

#include <iostream>
#include <type_traits>
#include <cassert>
#include <array>

namespace na64dp {
namespace util {

typedef size_t Arity_t;

/// Default implementation does nothing
template<typename T>
struct ArrayUnwindingCallableTraits {
    static void push(T&) {}
    static void pop(T&) {}
    static void inc(T&) {}
    template<typename VT> static void call(T& f, VT & v) { f(v); }
};

namespace aux {
/**\brief Utility class to combine with `unwind_array<>()` metafuncs
 *
 * Provides static implementation for automated unwinding of C-style arrays
 * with index tracing.
 * */
template< typename WrappedT
        , Arity_t maxArityT=16
        >
struct IdxStack {
    int cNDim;
    size_t idcs[maxArityT];
    WrappedT wt;

    IdxStack( WrappedT ref_ ) : cNDim(-1), idcs{0}, wt(ref_) {}
};
}  // namespace aux

/// Wrapper for the callable object, tracing the index changes
template< typename WrappedT
        , Arity_t maxArityT
        >
struct ArrayUnwindingCallableTraits< aux::IdxStack<WrappedT, maxArityT> > {
    typedef aux::IdxStack<WrappedT, maxArityT> F;
    /// Pushes new level in the index stack
    static void push( F & f ) {
        ++f.cNDim;
        assert((Arity_t) f.cNDim < maxArityT);
        f.idcs[f.cNDim] = 0;
    }
    /// Pops level from index stack
    static void pop( F & f ) {
        --f.cNDim;
        assert(f.cNDim > -2);
    }
    /// Increments current level's index counter
    static void inc( F & f ) {
        ++f.idcs[f.cNDim];
    }
    /// Calls the callable instance
    template<typename VT> static void call(F & f, VT & v) {
        f.wt(v, f.idcs, f.cNDim + 1);
    }
};

namespace aux {
/**\brief End-point metafunction for C-style arrays unwinding */
template<typename T, typename funcT>
typename std::enable_if<std::is_array<typename std::remove_pointer<T>::type>::value>::type
unwind_array( T a, funcT && func ) {
    ArrayUnwindingCallableTraits<funcT>::push(func);
    for( size_t i = 0; i < std::extent<T>::value; ++i ) {
        unwind_array< typename std::remove_extent<T>::type
                    , funcT>( a[i], func );
        ArrayUnwindingCallableTraits<funcT>::inc(func);
    }
    ArrayUnwindingCallableTraits<funcT>::pop(func);
}

/**\brief End-point metafunction for C-style arrays unwinding */
template<typename T, typename funcT>
typename std::enable_if<std::is_array<T>::value>::type
unwind_array( T a, funcT & func ) {
    ArrayUnwindingCallableTraits<funcT>::push(func);
    for( size_t i = 0; i < std::extent<T>::value; ++i ) {
        unwind_array<typename std::remove_extent<T>::type>( a[i], func );
        ArrayUnwindingCallableTraits<funcT>::inc(func);
    }
    ArrayUnwindingCallableTraits<funcT>::pop(func);
}

/**\brief Generic recursive metafunction for C-style arrays unwinding */
template<typename T, typename funcT>
typename std::enable_if<!std::is_array<typename std::remove_pointer<T>::type>::value>::type
unwind_array( T a, funcT && func ) {
    ArrayUnwindingCallableTraits<funcT>::call(func, a);
}

/**\brief Generic recursive metafunction for C-style arrays unwinding */
template<typename T, typename funcT>
typename std::enable_if<!std::is_array<T>::value>::type
unwind_array( T a, funcT & func ) {
    ArrayUnwindingCallableTraits<funcT>::call(func, a);
}

}  // namespace ::na64dp::util::aux

///\brief Invokes given callable with each element in the N-ary array
template< typename T
        , typename WrappedT
        , uint8_t maxArityT=16  //< TODO: extent?
        > void
for_each_in_array( T a, WrappedT f ) {
    typedef aux::IdxStack< WrappedT
                         , maxArityT
                         > Wrapper;
    aux::unwind_array<T, Wrapper>(a, Wrapper(f));
}

#if 0  // kept for example
struct Foo {
    float one[3][4][2];
    int two[2];
};

struct TestCallable {
    void operator()(float v, size_t * idxs, int ND) {
        for(int nd = 0; nd < ND; ++nd) {
            if(nd) std::cout << "x";
            std::cout << idxs[nd];
        }
        std::cout << " " << v << std::endl;
    }
};

int
main(int argc, char * argv[]) {
    Foo foo;
    bzero(&foo, sizeof(foo));
    unwind_array<decltype(Foo::one)>(foo.one, [](float v){
                std::cout << v << std::endl;  // XXX
            });
    std::cout << " * * *" << std::endl;
    unwind_array<decltype(Foo::two)>(foo.two, [](float v){
                std::cout << v << std::endl;  // XXX
            });
    std::cout << " * * *" << std::endl;
    for_each_in_array<decltype(Foo::one)>(foo.one, []( float v
                                                     , size_t * idxs
                                                     , int ND ){
                for(int nd = 0; nd < ND; ++nd) {
                    if(nd) std::cout << "x";
                    std::cout << idxs[nd];
                }
                std::cout << " " << v << std::endl;
            });
    std::cout << " * * *" << std::endl;
    TestCallable tstc;
    for_each_in_array<decltype(Foo::one)>(foo.one, tstc);
}
#endif

namespace detail {
    template <typename T, Arity_t ... Is>
    constexpr std::array<T, sizeof...(Is)>
    create_array(T value, std::index_sequence<Is...>){
        // cast Is to void to remove the warning: unused value
        return {{(static_cast<void>(Is), value)...}};
    }
}  // namespace ::na64dp::util::detail

template <Arity_t N, typename T>
constexpr std::array<T, N> create_array(const T& value) {
    return detail::create_array(value, std::make_index_sequence<N>());
}

}  // namespace ::na64dp::util
}  // namespace na64dp

