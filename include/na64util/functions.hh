#pragma once

/**\file
 * \brief Various runtime math or logic functions
 *
 * This header defines utility functions that may be consumed by handlers as
 * comparators, pre-defined, transformations or condition checkers.
 * */

namespace na64dp {
namespace functions {

/// Readability alias: two-values comparison logical function type
typedef bool (*Comparator)(double, double);

/**\brief Returns simple two-values comparison function based on its string repr
 *
 * Both, two-characters code and math form are acceptable for following
 * two-values comparison functions:
 * - "gt"/">" (greater than)
 * - "ge"/">=" (greater than or equal)
 * - "lt"/"<" (less than)
 * - "le"/"<=" (less than or equal)
 * If given string does not match to any of the above patters, the
 * `std::runtime_error` will be thrown.
 * */
Comparator get_comparator(const char *);

}
}
