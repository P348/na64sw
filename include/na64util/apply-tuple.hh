#pragma once

#include <tuple>
#include <utility>

/**\file
 * \brief Meta-function that unwinds tuple to function arguments
 *
 * This file contains a set of utility meta-functions (C++ template
 * definitions) that is devoted generally to unwind the `std::tuple<>` object
 * into a function call and looped operations.
 *
 * At least partially these definitions may be superseded by STL
 * implementations of >=C++17, so this header may be deleted from the project
 * at some point.
 *
 * Doxygen usually does poor job for documenting templates, so user interested
 * in use case scenarios would be better to look for inline comments and unit
 * test.
 * */

namespace na64dp {
namespace meta {

//
// Helpers

namespace aux {

// for_each() unwinding helpers
template<int... Is> struct seq {};
template<int N, int... Is> struct gen_seq /** @cond */ : gen_seq<N - 1, N - 1, Is...> /** @endcond */ { };
template<int... Is> struct gen_seq<0, Is...> : seq<Is...> { };

// apply() unwinding helpers
template<int...> struct index_tuple{}; 
template<int I, typename IndexTuple, typename... Types> 
struct make_indexes_impl; 

template<int I, int... indexes, typename T, typename ... Types> 
struct make_indexes_impl<I, index_tuple<indexes...>, T, Types...> {
    typedef typename make_indexes_impl<I + 1, index_tuple<indexes..., I>, Types...>::type type; 
};

template<int I, int... indexes>
struct make_indexes_impl<I, index_tuple<indexes...> > { 
    typedef index_tuple<indexes...> type; 
};

template<typename ... Types>
struct make_indexes : make_indexes_impl<0, index_tuple<>, Types...> {};

}  // namespace ::na64dp::meta::aux

//
// Functions to decompose std::tuple<>

template<typename T, typename F, int... Is>
void for_each(T & t, F & f, aux::seq<Is...>) {
    auto l = { (f(std::get<Is>(t)), 0)... };
    (void) l;
}

template<typename... Ts, typename F>
void for_each_in_tuple(std::tuple<Ts...> & t, F & f) {
    meta::for_each(t, f, aux::gen_seq<sizeof...(Ts)>());
}

// Use to retrieve first element from tuple (completes `tail()`)
template < typename T , typename... Ts >
auto head( std::tuple<T,Ts...> t ) -> T {
   return  std::get<0>(t);
}

// aux specification for `tail()`
template < std::size_t... Ns , typename... Ts >
auto tail_impl( std::integer_sequence<std::size_t, Ns...> , std::tuple<Ts...> t ) {
   return  std::make_tuple( std::get<Ns+1u>(t)... );
}

// Use it to retrieve "all except first" elements from a tuple
template < typename... Ts >
auto tail( std::tuple<Ts...> t ) {
   return  tail_impl( std::make_index_sequence<sizeof...(Ts) - 1u>() , t );
}

//
// apply() meta-function

// - forwards values from the tuple into a function
// Usage example:
//
// void func_a(int a1, std::string arg) {
//     std::cout << "func_a(" << a1 << ", \"" << arg << "\")"
//               << std::endl;
// }
// 
// void func_b(int a1, const std::string & arg) {
//     std::cout << "func_b(" << a1 << ", \"" << arg << "\")"
//               << std::endl;
// }
// ...
// std::string some = "some";
// std::tuple<int, const std::string &> tpl(42, some);
// 
// aux::meta::apply( func_a, std::make_tuple(12, std::move(std::string("bar"))) );
// aux::meta::apply( func_b, tpl );

template< typename RetT
        , typename ... ArgsTs
        , int ... indexes > RetT
apply_helper( RetT (*pf)(ArgsTs...)
            , aux::index_tuple< indexes... >
            , std::tuple<ArgsTs...> && tpl
            ) {
    return pf( std::forward<ArgsTs>( std::get<indexes>(tpl))... );
}

template<typename RetT, typename ... ArgsT> RetT
apply( RetT (*f)(ArgsT ...), std::tuple<ArgsT ...> & args ) {
    return apply_helper( f
                       , typename aux::make_indexes<ArgsT...>::type()
                       , std::tuple<ArgsT...>(args)
                       );
}

template<class RetT, class ... ArgsT> RetT
apply(RetT (*pf)(ArgsT...), std::tuple<ArgsT...> && tpl) {
    return apply_helper( pf
                       , typename aux::make_indexes<ArgsT...>::type()
                       , std::forward< std::tuple<ArgsT...> >(tpl)
                       );
}

// - apply with first argument as a reference
//
// Practical usecase is to provide some collection as a reference for first
// argument. Useful for calibration readers that transforms tuple of values
// read from lines of CSV files into entries within some mapping container.
//
// Usage example (TODO: not very representative since we do not modify
// first argument here):
//
// void func_1a( std::string & one, float two, int three ) {
//     std::cout << "func_1a(" << one << ", " << two << ", " << three << ")"
//               << std::endl;
// }
// 
// void func_1b( std::string & one, float two, std::string three ) {
//     std::cout << "func_1b(" << one << ", " << two << ", \"" << three << "\")"
//               << std::endl;
// }
// ...
// std::tuple<float, int> tpl1(2.34, 42);
// std::string s1 = "string-one";
//
// aux::meta::apply( func_1a, s1, tpl1 );
// aux::meta::apply( func_1b, s1, std::make_tuple(float(34.12), std::move(std::string("foo"))) );

template< typename RetT
        , typename T1
        , typename ... ArgsTs
        , int ... indexes > RetT
apply_helper_1( RetT (*pf)(T1 &, ArgsTs...)
              , T1 & a1
              , aux::index_tuple< indexes... >
              , std::tuple<ArgsTs...> && tpl
              ) {
    return pf( a1, std::forward<ArgsTs>( std::get<indexes>(tpl))... );
}

template<typename RetT, typename T1, typename ... ArgsT> RetT
apply( RetT (*f)(T1 &, ArgsT ...), T1 & a1, std::tuple<ArgsT ...> & args ) {
    return apply_helper_1( f
                         , a1
                         , typename aux::make_indexes<ArgsT...>::type()
                         , std::tuple<ArgsT...>(args)
                        );
}

template<typename RetT, typename T1, typename ... ArgsT> RetT
apply( RetT (*f)(T1 &, ArgsT ...), T1 & a1, std::tuple<ArgsT ...> && args ) {
    return apply_helper_1( f
                         , a1
                         , typename aux::make_indexes<ArgsT...>::type()
                         , std::forward< std::tuple<ArgsT...> >(args)
                        );
}

// - apply with method and class instance
// Usage example:
//
// struct Some {
//     double value;
//     double & method_1( std::string arg1, unsigned long arg2 ) {
//         std::cout << "Some::method_1(\"" << arg1 << "\", " << arg2 << ")" << std::endl;
//         return value;
//     }
// };
// ...
// Some obj{-1.23e-12};
// std::tuple<std::string, unsigned long> mtpl("one", 234);
// assert( obj.value == aux::meta::apply( &Some::method_1, obj, mtpl ) );
// aux::meta::apply( &Some::method_1, obj
//                 , std::make_tuple(std::move(std::string("moveme")), (unsigned long) 112)
//                 );

template< typename RetT
        , typename T
        , typename ... ArgsTs
        , int ... indexes > RetT
apply_helper_m( RetT (T::*m)(ArgsTs...)
              , T & obj
              , aux::index_tuple< indexes... >
              , std::tuple<ArgsTs...> && tpl
              ) {
    return (obj.*m)( std::forward<ArgsTs>( std::get<indexes>(tpl))... );
}

template<typename RetT, typename T, typename ... ArgsT> RetT
apply( RetT (T::*m)(ArgsT ...), T & obj, std::tuple<ArgsT ...> & args ) {
    return apply_helper_m( m
                         , obj
                         , typename aux::make_indexes<ArgsT...>::type()
                         , std::tuple<ArgsT...>(args)
                        );
}

template<typename RetT, typename T, typename ... ArgsT> RetT
apply( RetT (T::*m)(ArgsT ...), T & obj, std::tuple<ArgsT ...> && args ) {
    return apply_helper_m( m
                         , obj
                         , typename aux::make_indexes<ArgsT...>::type()
                         , std::forward< std::tuple<ArgsT...> >(args)
                        );
}

}  // namespace ::na64dp::meta
}  // namespace na64dp

