#pragma once

#include <stdexcept>
#include <cstdio>
#include <cstdint>

#include "na64detID/detectorIDSelect.h"

namespace na64dp {

namespace errors {

class SelectorBuildupFailure : public std::runtime_error {
private:
    int _rc;
    char _errBf[256];
public:
    SelectorBuildupFailure( int rc, const char * errBf ) throw();
    int compile_return_code() const throw() { return _rc; }
    virtual const char * what() const throw() { return _errBf; }
};

}

namespace dsul {

typedef na64dp_dsul_CodeVal_t CodeVal_t;
typedef na64dp_dsul_PrincipalValue_t Principal_t;

/**\brief Represents a generalized selector predicate run-time function
 *
 * The details on DSuL is given on :ref:`DSuL` page. This class maintains the
 * lifecycle of expression being "compiled" into some kind of in-memory buffer,
 * capable for reasonably fast reentrant evaluation over principal values.
 *
 * This template class has to be parameterised with some name resolution
 * context type. Instances of this class has to be considered "valid" only
 * while particular instance name-resolution context is "valid".
 * */
template< typename PrincipalT
        , typename ContextT >
class Selector {
private:
    /// Evaluation buffer pointer
    char * _buffer;
public:
    typedef na64dp_dsul_PrincipalValue_t PrincipalValue;
    typedef na64dpsu_SymDefinition SymDef;
public:
    /// "Compiles" selection given by string expression and list of definitions
    Selector( const std::string & expr
            , const SymDef * getters
            , const ContextT & ctx
            , std::ostream * dbgStream=nullptr ) : _buffer(nullptr) {
        char errBf[128];
        std::string expr_(expr);

        int rc;
        size_t bufferSize = 512;
        char * dump = nullptr;
        size_t dumpSize = 0;
        do {
            if( _buffer ) {
                free(_buffer);
            }
            FILE * dbgF = NULL;
            if( dbgStream ) {
                if( dump ) {
                    free(dump);
                    dump = NULL;
                    dumpSize = 0;
                }
                dbgF = open_memstream( &dump, &dumpSize );
            }
            _buffer = (char*) malloc(bufferSize);
            rc = na64dp_dsul_compile_detector_selection( const_cast<char*>(expr_.c_str())
                        , getters
                        , const_cast<ContextT*>(&ctx)
                        , _buffer, bufferSize
                        , errBf, sizeof(errBf)
                        , dbgF );
            bufferSize *= 2;  // enlarge buffer each failed attempt
            if( dbgF ) {
                fclose(dbgF);
            }
        } while( -2 == rc );
        if( dbgStream ) {
            *dbgStream << std::string(dump, dumpSize);
            free(dump);
        }
        if( 0 != rc ) {
            throw errors::SelectorBuildupFailure( rc, errBf );
        }
        na64dp_dsul_update_extern_symbols( _buffer, const_cast<ContextT*>(&ctx) );
    }
    /// Deletes connected evaluation buffer, if any.
    ~Selector() {
        if(_buffer) 
            free(_buffer);
    }
    /// Updates extern definitions
    void reset_context( const ContextT & ctx ) {
        na64dp_dsul_update_extern_symbols( _buffer, &ctx );
    }
    /// Retruns `true` if given detector ID matches the expression
    bool matches( PrincipalT pv ) const {
        // TODO: to-voidptr cast violates generocity; move to traits
        na64dp_dsul_update_static_symbols( _buffer
                                         , (Principal_t) ((uintptr_t) pv) );
        return na64dp_dsul_eval( _buffer );
    }
    /// Shortcut for `matches()` method
    bool operator()( PrincipalT pv ) const { return matches(pv); }
    /// Dumps the evaluation buffer details into given stream
    void dump_eval_buffer(std::ostream & os) const {
        char * dump;
        size_t dumpSize;
        FILE * dumpF = open_memstream( &dump, &dumpSize );
        na64dp_dsul_dump( _buffer, dumpF );
        fclose(dumpF);
        os << std::string( dump, dumpSize );
        free(dump);
    }
};

}  // namespace dsul

}

template< typename PrincipalT
        , typename ContextT > std::ostream &
operator<<(std::ostream & os, const na64dp::dsul::Selector<PrincipalT, ContextT> & s ) {
    s.dump_eval_buffer(os);
    return os;
}

