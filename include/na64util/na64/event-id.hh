/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

# include "na64util/na64/event-id.h"

# include <iostream>
# include <set>
# include <utility>

namespace na64dp {

/**\brief Event ID C++ wrapper around numerical type
 *
 * \todo Uncommon event ID ctr for identifier type bearing information of
 * physical event data layout
 * */
class EventID {
private:
    na64sw_EventID_t _eid;
public:
    /// Default ctr, creates event ID of `{0, 0, 0}`
    EventID() : _eid(0) {}
    /// Common event ID ctr, makes "chunkless" event identifier
    EventID( na64sw_runNo_t runNo
           , na64sw_spillNo_t spillNo
           , na64sw_eventNo_t evNo )
            : _eid(na64sw_assemble_event_id(runNo, spillNo, evNo)) {}
    /// Copy ctr, initialized underlying struct
    explicit EventID( const na64sw_EventID_t & eid ) : _eid(eid) {}
    /// Implicit `to-number` cast operator
    operator na64sw_EventID_t () const { return _eid; }
    #if 0
    /// Copies full ID by copying numerical value
    EventID & operator=( na64sw_EventID_t nid ) {
        _eid = nid;
        return *this;
    }
    #endif
    /// Returns run number of an event being identified
    na64sw_runNo_t run_no() const { return na64sw_get_runNo(_eid); }
    /// Sets the run number of an event being identified
    void run_no( na64sw_runNo_t rn ) { na64sw_set_runNo( &_eid, rn ); }
    /// Returns spill number of an event being identified
    na64sw_spillNo_t spill_no() const { return na64sw_get_spillNo(_eid); }
    /// Sets the spill number of an event being identified
    void spill_no( na64sw_spillNo_t sn ) { na64sw_set_spillNo(&_eid, sn); }
    /// Returns the event's in-spill number of an event being identified
    na64sw_eventNo_t event_no() const { return na64sw_get_eventNo(_eid); }
    /// Sets the event's in-spill number of an event being identified
    void event_no( na64sw_eventNo_t en ) { na64sw_set_eventNo(&_eid, en); }
    /// Returns `std::string` copying from internal buffer
    std::string to_str() const;
    /// Reads the event ID from string assuming its default str format
    static EventID from_str( const std::string & );
};

inline bool
operator<(const EventID & a, const EventID & b) {
    return ((na64sw_EventID_t) a) < ((na64sw_EventID_t) b);
}

std::ostream & operator<<( std::ostream & stream, const EventID & );

namespace util {
void reset(EventID &);
}

namespace event {
template<typename T, typename> struct Traits;  // fwd
template<typename T> struct Stream;  // fwd

template<>
struct Traits<EventID, void> {
    template<typename StreamImplT>
    static void stream_op( Stream<StreamImplT> & ds, EventID & v ) {
        if( StreamImplT::isInput ) {
            na64sw_runNo_t eid;
            ds.impl().template scalar<na64sw_runNo_t>(eid);
            v = EventID(eid);
        } else {
            ds.impl().template scalar<EventID>(v);
        }
    }
};
}

}  // namespace na64dp

