#ifndef H_NA64SW_UTIL_MD5_SUM_H
#define H_NA64SW_UTIL_MD5_SUM_H

extern "C" int na64sw_md5sum_file(const char * path, char * destBuf);

#endif  /* H_NA64SW_UTIL_MD5_SUM_H */
