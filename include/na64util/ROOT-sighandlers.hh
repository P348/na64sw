#pragma once

#include "na64sw-config.h"

#if defined(ROOT_FOUND) && ROOT_FOUND
/* ROOT signal handlers are inefficient and useless most of the time.
 * They produce a lot of fuss and unpredicted behaviour in many crucial
 * situations by failing to attach debugger properly, freezing system and
 * thus complicating diagnostics and debugging. We do recommend to disable
 * them as early as possible for better development experience.
 */

void disable_ROOT_sighandlers();
#endif

