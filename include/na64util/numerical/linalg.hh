#pragma once

#include "na64sw-config.h"

#include "na64util/numerical/spatialRotations.h"

#include <iostream>
#include <cmath>
#include <cassert>

/**\file
 * \brief Basic spatial vectors
 *
 * This header defines few simple linear algebra types and operations. The
 * goal is to facilitate some very basic operations commonly met in the NA64sw,
 * to minimize dependencies. The subset is not meant to provide
 * precision or efficiency (use specialized libs for massive algebra) -- where
 * possible it is recommended to rely on third-party libs or frameworks (GSL,
 * LAPAC, Eigen3, ROOT, etc)
 *
 * \todo Since we anyway strictly depend on GSL, foresee precise routines
 *
 * \ingroup numerical-utils
 * */

///\defgroup numerical-utils Numerical utility types and functions

namespace na64dp {
namespace util {

template<typename FloatT=Float_t>
union Matrix3T;

///\brief 3-dim spatial vector representation used in the lib
///
///\ingroup numerical-utils
template<typename FloatT=Float_t>
union Vec3T {
    /// Data acessed by index
    FloatT r[3];
    /// Data acessed by component name
    struct { FloatT x, y, z; } c;

    /// Component getter
    FloatT operator[](int i) const { return r[i]; }
    /// Component reference getter
    FloatT & operator[](int i) { return r[i]; }

    /// Scalar product
    template<typename FloatT2>
    FloatT operator*(const Vec3T<FloatT2> & b) const
        { return c.x*b.c.x + c.y*b.c.y + c.z*b.c.z; }
    /// Vector multiplied by number
    template<typename FloatT2>
    Vec3T<FloatT> operator*(FloatT2 a) const
        { return {{ static_cast<FloatT>(a*c.x)
                  , static_cast<FloatT>(a*c.y)
                  , static_cast<FloatT>(a*c.z)
                  }}; }
    /// In-place multiplication by number
    template<typename FloatT2>
    Vec3T<FloatT> & operator*=(FloatT2 a)
        { r[0] *= a; r[1] *= a; r[2] *= a;
          return *this; }
    /// Vector divided by number
    template<typename FloatT2>
    Vec3T<FloatT> operator/(FloatT2 a) const
        { return {{c.x/a, c.y/a, c.z/a}}; }
    /// In-place division by number
    template<typename FloatT2>
    Vec3T<FloatT> & operator/=(FloatT2 a)
        { r[0] /= a; r[1] /= a; r[2] /= a;
          return *this; }
    /// Vector sum
    template<typename FloatT2>
    Vec3T<FloatT> operator+(const Vec3T<FloatT2> & b) const
        { return Vec3T{c.x + b.c.x, c.y + b.c.y, c.z + b.c.z}; }
    /// In-place vector sum
    template<typename FloatT2>
    Vec3T<FloatT> & operator+=(const Vec3T<FloatT2> & b)
        { c.x += b.c.x; c.y += b.c.y; c.z += b.c.z;
          return *this; }
    /// Vector sub
    template<typename FloatT2>
    Vec3T<FloatT> operator-(const Vec3T<FloatT2> & b) const
        { return Vec3T{{c.x - b.c.x, c.y - b.c.y, c.z - b.c.z}}; }
    /// In-place vector sub
    template<typename FloatT2>
    Vec3T<FloatT> & operator-=(const Vec3T<FloatT2> & b)
        { c.x -= b.c.x; c.y -= b.c.y; c.z -= b.c.z;
          return *this; }
    /// Vector cross product
    template<typename FloatT2>
    Vec3T<FloatT> cross(const Vec3T<FloatT2> & b) const {
        return Vec3T{{
                // a1b2 - a2b1
                r[1]*b.r[2] - r[2]*b.r[1],
                // a2b0 - a0b2
                r[2]*b.r[0] - r[0]*b.r[2],
                // a0b1 - a1b0
                r[0]*b.r[1] - r[1]*b.r[0],
            }};
    }
    /// Returns norm of the vector
    FloatT norm() const {
        return sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]);
    }
    /// Returns unit (orth) of this vector
    Vec3T<FloatT> unit() const {
        Float_t nrm = norm();
        if(!nrm) return *this;  // zero vector
        return {{ r[0]/nrm, r[1]/nrm, r[2]/nrm }};
    }
    /// Unary minus (inversion) operator
    Vec3T<FloatT> operator-() const {
        return Vec3T{{-c.x, -c.y, -c.z}};
    }
};

///\brief Print operator
template<typename FloatT> std::ostream &
operator<<(std::ostream & os, const Vec3T<FloatT> & v) {
    os << "{" /*<< std::setprecision(2)*/ << v.r[0]
       << ", " << v.r[1]
       << ", " << v.r[2]
       << "}";
    return os;
}

///\brief 3x3 matrix representation
///
///\ingroup numerical-utils
template<typename FloatT>
union Matrix3T {
    /// Data representations
    FloatT cm[3][3];
    FloatT m[9];

    /// Returns RO-ptr to the 1st element in Nth row
    const FloatT * operator[](int n) const {
        assert(n >= 0);
        assert(n < 3);
        return m + 3*n;
    }
    /// Returns RW-ptr to the 1st element in Nth row
    FloatT * operator[](int n) {
        assert(n >= 0);
        assert(n < 3);
        return m + 3*n;
    }

    /// Matrix.Vector product
    template<typename FloatT2>
    Vec3T<FloatT> operator*(const Vec3T<FloatT2> & v) const {
        Vec3T<FloatT> p;
        for( int j = 0; j < 3; ++j ) {
            p.r[j] = 0.;
            for( int i = 0; i < 3; ++i ) {
                p.r[j] += v.r[i]*operator[](j)[i];
            }
        }
        return p;
    }
    /// Matrix.Matrix product
    template<typename FloatT2>
    Matrix3T<FloatT> operator*(const Matrix3T<FloatT2> & bm) const {
        Matrix3T rm;
        for( int j = 0; j < 3; ++j ) {
            for( int i = 0; i < 3; ++i ) {
                rm.operator[](i)[j] = 0.;
                for( int k = 0; k < 3; ++k ) {
                    rm.cm[i][j] += operator[](i)[k]*bm[k][j];
                }
            }
        }
        return rm;
    }
    /// Matrix determinant
    ///
    /// Directly computes matrix determinant based on first line.
    ///
    ///\warning Must not be used for general purpose as can be numerically
    ///         unstable for small values in first line.
    FloatT det() const {
        const auto & M = *this;
        //   a00(a22a11-a21a12)
        // - a10(a22a01-a21a02)
        // + a20(a12a01-a11a02)
        return M[0][0]*(M[2][2]*M[1][1] - M[2][1]*M[1][2])
             - M[1][0]*(M[2][2]*M[0][1] - M[2][1]*M[0][2])
             + M[2][0]*(M[1][2]*M[0][1] - M[1][1]*M[0][2])
             ;
    }
    ///\brief Returns inverted matrix
    ///
    ///\warning Uses direct calculus for inversion, not recommended for general
    ///         use as it is numerically unstable.
    Matrix3T<FloatT> inv() const;
    /// Returns transposed matrix
    Matrix3T<FloatT> transposed() const;
};

//
// Matrix method implementation

template<typename FloatT>
Matrix3T<FloatT>
Matrix3T<FloatT>::inv() const {
    FloatT d = det();
    const auto & M = *this;
    // |  a22a11-a21a12  -(a22a01-a21a02)   a12a01-a11a02 |
    // |-(a22a10-a20a12)   a22a00-a21a02  -(a12a00-a10a02)|
    // |  a21a10-a20a11  -(a21a00-a20a01)   a11a00-a10a01 |
    return Matrix3T{ {{  ( M[2][2]*M[1][1] - M[2][1]*M[1][2] )/d
                      , -( M[2][2]*M[0][1] - M[2][1]*M[0][2] )/d
                      ,  ( M[1][2]*M[0][1] - M[1][1]*M[0][2] )/d
                      }
                   ,  { -( M[2][2]*M[1][0] - M[2][0]*M[1][2] )/d
                      ,  ( M[2][2]*M[0][0] - M[2][0]*M[0][2] )/d
                      , -( M[1][2]*M[0][0] - M[1][0]*M[0][2] )/d
                      }
                   ,  {  ( M[2][1]*M[1][0] - M[2][0]*M[1][1] )/d
                      , -( M[2][1]*M[0][0] - M[2][0]*M[0][1] )/d
                      ,  ( M[1][1]*M[0][0] - M[1][0]*M[0][1] )/d
                      }
                   } };
}

//
// Common utilities

/// Returns rotation matrix over `axis` by `angle`
template<typename FloatT=Float_t> Matrix3T<FloatT>
arbitrary_rotation_matrix(const Vec3T<FloatT> & axis, FloatT angle) {
    const FloatT l = axis.r[0]
               , p = axis.r[1]
               , n = axis.r[2]
               , cost = cos(angle)
               , sint = sin(angle)
               , cost1 = 1 - cost
               ;
    Matrix3T<FloatT> om;

    om[0][0] = l*l*cost1 +   cost;
    om[0][1] = p*l*cost1 - n*sint;
    om[0][2] = n*l*cost1 + p*sint;

    om[1][0] = l*p*cost1 + n*sint;
    om[1][1] = p*p*cost1 +   cost;
    om[1][2] = n*p*cost1 - l*sint;

    om[2][0] = l*n*cost1 - p*sint;
    om[2][1] = p*n*cost1 + l*sint;
    om[2][2] = n*n*cost1 +   cost;

    return om;
}

//std::ostream & operator<<(std::ostream & os, const Matrix3T & m);

/// Framework-wide standard spatial vector type
typedef Vec3T<Float_t> Vec3;
/// Framework-wide standard 3x3 matrix type
typedef Matrix3T<Float_t> Matrix3;

}  // namespace ::na64dp::util
}  // namespace ::na64dp

