#ifndef H_NA64SW_UTIL_PLANAR_FIT
#define H_NA64SW_UTIL_PLANAR_FIT
#endif

#include "na64sw-config.h"
#include <stdlib.h>

# ifdef __cplusplus
extern "C" {
# endif

/**\brief Fits the given set of linear segments with plane
 *
 * Original method is taken from documentation of Geometric
 * Tools \cite geomToolsSegmentsOrPlane .
 *
 * \image html utils/plane-fit-approx_few.jpg
 *
 * \image html utils/plane-fit-approx_multiple.jpg
 *
 * \ingroup numerical-utils
 * */
int
na64sw_fit_line_segments_w_plane_unweighted(
          na64sw_Float_t * segments[]
        , size_t nSegments
        , size_t nDims
        , na64sw_Float_t * A
        , na64sw_Float_t * N
        );

# ifdef __cplusplus
}
# endif
