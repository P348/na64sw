#pragma once

/**\file
 * \brief Routines for online statistics
 *
 * Contains various application classes implementing windowed (sliding,
 * rolling) algorithms.
 *
 * \ingroup numerical-utils
 */

///\defgroup numerical-utils-online-stats Numerical utils for online statistics operations

#include "na64util/numerical/sums.h"

#include <deque>
#include <vector>

#include <stddef.h>  // size_t

namespace na64dp {
namespace numerical {

///\brief C++ interface over C implementation of Klein's scorer
///
///\see na64dp_KleinScorer
///\ingroup numerical-utils-online-stats
class KleinScorer : protected na64dp_KleinScorer_t {
public:
    /// Constructs initialized scorer instance
    KleinScorer() {
        na64dp_sum_klein_init(this);
    }
    /// Adds the value to sum
    void add(double v) {
        na64dp_sum_klein_add(this, v);
    }
    /// Adds the value to sum (operator shortcut)
    KleinScorer & operator+=(double v) {
        add(v);
        return *this;
    }
    /// Returns computed sum
    double result() const {
        return na64dp_sum_klein_result(this);
    }
    /// Returns computed sum (operator shortcut)
    operator double() const {
        return result();
    }
    /// Drops the scorer value to 0.
    void reset() { na64dp_sum_klein_init(this); }
};

///\brief A C++ wrapper around `na64dp_Covariance`
///
///\ingroup numerical-utils-online-stats
class CorrelationScorer : protected na64dp_Covariance {
public:
    /// Initializes and constructs covariance scorer instance
    CorrelationScorer() {
        na64dp_cov_init(this);
    }
    /// Accounts pair of values
    void account( double x, double y ) {
        na64dp_cov_account( this, x, y );
    }
    /// Returns unbiased covariance value
    double covariance() const {
        return na64dp_covariance_get(this);
    }

    /// Returns Pearson's correlation coefficient
    double pearson_correlation() const {
        return na64dp_covariance_get(this)/sqrt(variance_x()*variance_y());
    }

    /// Returns number of accounted pairs
    unsigned long n_accounted() const { return n; }
    /// Returns mean X value
    double mean_x() const { return na64dp_sum_klein_result(&xMean); }
    /// Returns mean Y value
    double mean_y() const { return na64dp_sum_klein_result(&yMean); }
    /// Returns sum of squared deviations of X value
    double sq_dev_x() const { return na64dp_sum_klein_result(&d2x); }
    /// Returns variance of X
    double variance_x() const { return sq_dev_x()/(n-1); }
    /// Returns sum of squared deviations of Y value
    double sq_dev_y() const { return na64dp_sum_klein_result(&d2y); }
    /// Returns variance of Y
    double variance_y() const { return sq_dev_y()/(n-1); }
};

/**\brief Covariance matrix wrapper around `na64dp_CovarianceMatrix` C struct
 *
 * This implementation is a bit more efficient than multiple
 * `CorrelationScorer` instances though it explicitly requires that none of the
 * considered vector components may be NaN.
 *
 *\ingroup numerical-utils-online-stats
 * */
class CovarianceMatrix : protected na64dp_CovarianceMatrix_t {
public:
    /// Creates pairwise covariance scorer for vector of `d` 
    CovarianceMatrix( unsigned short v ) {
        na64dp_mcov_init( this, v );
    }
    /// Frees covariance scorer (heap used)
    ~CovarianceMatrix() {
        na64dp_mcov_free(this);
    }
    /// Takes into account given sample vector
    void account( const double * v ) {
        na64dp_mcov_account(this, v);
    }
    /// Returns i-th mean value
    double mean(int i) const { return na64dp_sum_klein_result(means + i); }
    /// Returns number of accounted vectors
    unsigned long n_accounted() const { return n; }
    /// Returns dimensionality
    unsigned short n_dimensions() const { return d; }
    /// Returns i-th mean value
    double mean(int i) { return na64dp_sum_klein_result(means + i); }  // sqds, covs
    /// Returns i-th  squared deviation value
    double sq_dev(int i) { return na64dp_sum_klein_result(sqds + i); }
    /// Returns variance value of i-th component
    double variance(int i) { return sq_dev(i)/(n-1); }
    /// Returns covariance value of i-th and j-th component
    double covariance(int i, int j) { return na64dp_mcov( this, i, j ); }
    /// Returns Pearson's correlation value of i-th and j-th component
    double pearson_correlation(int i, int j) {
        return covariance(i, j)/sqrt(variance(i)*variance(j));
    }
};

///\brief Utility interface class representing windowed (sliding) statistics.
///
///\ingroup numerical-utils-online-stats
class iMovStats {
public:
    virtual bool is_outlier(double, double threshold=3.) const = 0;
    virtual double mean() const = 0;
    virtual void account(double) = 0;
    virtual std::vector<double> data_copy() const = 0;
    virtual size_t n_samples() const = 0;
};

namespace aux {

template<typename ScorerT> void reset_summation_scorer(ScorerT &);

template<> void reset_summation_scorer<double>(double &);
template<> void reset_summation_scorer<KleinScorer>(KleinScorer &);

}

/** Implements moving statistics using strightforward approach
 *
 * Maintains a deque of N samples, dynamically recalculating mean/stddev values
 * each time new sample is accounted.
 *
 *\ingroup numerical-utils-online-stats
 */
template<typename ScorerT>
class MovingStats : public iMovStats
                  , public std::deque<double> {
private:
    /// Numbor ef entries since last recaching
    size_t _recacheCounter;
    /// Threshold number of entries for recaching
    const size_t _recacheCounterThreshold;
protected:
    /// Threshold (in units of \f$\sigma\f$) of the values to be omitted
    double _excludeOutliersThreshold;
    /// Threshold (in units of \f$\sigma\f$) of the values to cause recache
    double _recacheOnOutliersThreshold;
    /// \f$\sum\limits_{i=1}^N x_i\f$
    ScorerT _runningSum;
    /// \f$\sum\limits_{i=1}^N x_i^2\f$
    ScorerT _runningSqSum;
    const size_t _windowSize;

    /// Re-calculates the sums
    void _recache() {
        aux::reset_summation_scorer<ScorerT>(_runningSum);
        aux::reset_summation_scorer<ScorerT>(_runningSqSum);
        for( auto v : *this ) {
            _runningSum += v;
            _runningSqSum += v*v;
        }
        _recacheCounter = 0;
    }
public:
    /**\brief Constructs empty sliding scorer
     *
     * Used to create new instances of the scorer in cases when no initial data
     * is available. Various estimations derived by this instance (like mean
     * and stdev) at first `N` events will not be properly conditioned because
     * of lack of the data, so wherever is possible an alternative form of
     * constructor has to be used.
     *
     * \param N size of sliding window
     * \param excludeOutliers \f$k\f$ in \f$k \times \sigma\f$ to classify outliers
     * \param recacheOnOutliers \f$k\f$ in \f$k \times \sigma\f$ to recache
     * \param recachingFrac fraction of events to force sums recache
     * */
    MovingStats( size_t N
               , double excludeOutliers=6.
               , double recacheOnOutliers=6.
               , double recachingFrac=1.
               ) : _recacheCounter(0)
                 , _recacheCounterThreshold(N*recachingFrac)
                 , _excludeOutliersThreshold(excludeOutliers)
                 , _recacheOnOutliersThreshold(recacheOnOutliers)
                 , _windowSize(N) {}

    /**\brief Constructs empty sliding scorer
     *
     * Used to create new instances of the scorer in cases when no initial data
     * is available. Various estimations derived by this instance (like mean
     * and stdev) at first `N` events will not be properly conditioned because
     * of lack of the data, so wherever is possible an alternative form of
     * constructor has to be used.
     *
     * \param initial shall contain at least `N` samples for initial state of accumulator
     * \param N size of sliding window
     * \param excludeOutliers \f$k\f$ in \f$k \times \sigma\f$ to classify outliers
     * \param recacheOnOutliers \f$k\f$ in \f$k \times \sigma\f$ to recache
     * \param recachingFrac fraction of events to force sums recache
     * */
    MovingStats( double * initial
               , size_t N
               , double excludeOutliers=6.
               , double recacheOnOutliers=6.
               , double recachingFrac=1.
               ) : std::deque<double>(initial, initial + N)
                 , _recacheCounter(0)
                 , _recacheCounterThreshold(N*recachingFrac)
                 , _excludeOutliersThreshold(excludeOutliers)
                 , _recacheOnOutliersThreshold(recacheOnOutliers)
                 , _windowSize(N) {}

    /// Returns true if given sample is beyond $6 \f$\sigma\f$
    virtual bool is_outlier( double sample, double threshold=3.) const override {
        if( empty() ) return false;
        return fabs(sample - mean()) > sqrt(sigma_sq())*threshold;
    }

    /// Returns mean value \f$(1/N) \sum\limits_{i=1}^N x_i\f$
    virtual double mean() const override {
        if( !empty() )
            return _runningSum/size();
        return std::nan("0");
    }

    /// Takes into account sample \f$x_i\f$
    virtual void account(double sample) override {
        if( _excludeOutliersThreshold
        && is_outlier(sample, _excludeOutliersThreshold) ) return;
        if( _recacheCounterThreshold && ++_recacheCounter > _recacheCounterThreshold ) {
            // Recache sums to avoid (mitigate) catstrophic cancellation
            _recache();
        }
        if( size() < _windowSize ) {
            _runningSum += sample;
            _runningSqSum += sample*sample;
            push_back( sample );
            if( !_recacheCounterThreshold ) _recache();
            return;
        }
        const double prevFirst = *begin();
        _runningSum += sample - prevFirst;
        _runningSqSum += sample*sample - prevFirst*prevFirst;
        pop_front();
        push_back( sample );
        if( _recacheOnOutliersThreshold
         && is_outlier(sample, _recacheOnOutliersThreshold) ) {
            _recache();
        }
    }

    /// Returns (unbiased, sliding) \f$ \sigma^2 \f$
    double sigma_sq() const {
        if( size() > 1 )
            return (_runningSqSum/size() - mean()*mean())/(size()-1);
        return std::nan("0");
    }

    /// Copies entries from deque to vector
    virtual std::vector<double> data_copy() const override {
        return std::vector<double>( this->begin(), this->end() );
    }

    virtual size_t n_samples() const {
        return size();
    }
};

}
}

