#include <vector>

#include "na64sw-config.h"

namespace na64dp {
namespace util {
namespace jenks {

typedef std::size_t                  SizeT;
typedef SizeT                        CountType;
typedef std::pair<double, CountType> ValueCountPair;
typedef std::vector<double>          LimitsContainer;
typedef std::vector<ValueCountPair>  ValueCountPairContainer;

SizeT GetTotalCount( const ValueCountPairContainer & vcpc );

// helper struct JenksFisher

// captures the intermediate data and methods for the calculation of Natural
// Class Breaks.
struct JenksFisher {
    SizeT m_M  // overall number of elements
        , m_K  // number of clusters
        , m_BufSize  // M - k + 1
        ;
    ValueCountPairContainer m_CumulValues;  // cumulative sums

    std::vector<double> m_PrevSSM;  // prev sum of squared deviations of the first i vals classified to j clusters
    std::vector<double> m_CurrSSM;
    std::vector<SizeT>               m_CB;  // value index of the last class-break
    std::vector<SizeT>::iterator     m_CBPtr;

    SizeT                  m_NrCompletedRows;

    JenksFisher( const ValueCountPairContainer & vcpc
               , SizeT k );

    /// Gets sum of weighs for elements b..e.
    double GetW(SizeT b, SizeT e);

    /// Gets sum of weighed values for elements b..e
    double GetWV(SizeT b, SizeT e);

    /// Gets the Squared Mean for elements b..e, multiplied by weight.
    /// Note that n*mean^2 = sum^2/n when mean := sum/n
    double GetSSM(SizeT b, SizeT e);

    /// finds CB[i+m_NrCompletedRows] given that 
    /// the result is at least bp+(m_NrCompletedRows-1)
    /// and less than          ep+(m_NrCompletedRows-1)
    /// Complexity: O(ep-bp) <= O(m)
    SizeT FindMaxBreakIndex(SizeT i, SizeT bp, SizeT ep);

    /// find CB[i+m_NrCompletedRows]
    /// for all i>=bi and i<ei given that
    /// the results are at least bp+(m_NrCompletedRows-1)
    /// and less than            ep+(m_NrCompletedRows-1)
    /// Complexity: O(log(ei-bi)*Max((ei-bi),(ep-bp))) <= O(m*log(m))
    void CalcRange(SizeT bi, SizeT ei, SizeT bp, SizeT ep);

    /// complexity: O(m*log(m)*k)
    void CalcAll();
};

void
GetCountsDirect( ValueCountPairContainer & vcpc
               , const double * values
               , SizeT size );

struct ValueCountPairContainerArray : std::vector<ValueCountPairContainer> {
    void resize(SizeT k);
    void GetValueCountPairs( ValueCountPairContainer & vcpc
                           , const double * values
                           , SizeT size
                           , unsigned int nrUsedContainers );
};

void
GetValueCountPairs( ValueCountPairContainer & vcpc
                  , const double * values
                  , SizeT n );

void
ClassifyJenksFisherFromValueCountPairs( LimitsContainer & breaksArray
                                      , SizeT k
                                      , const ValueCountPairContainer& vcpc );

}  // namespace na64dp::util::jenks
}  // namespace na64dp::util
}  // namespace na64dp

