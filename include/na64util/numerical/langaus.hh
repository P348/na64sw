#pragma once

#include "na64sw-config.h"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include <TF1.h>
#include <TH1F.h>

namespace na64dp {
namespace util {

extern const Double_t gLandauFMax;  ///< Landau maximum location

/**\brief A convoluted Landau and Gauss distribution functions with scaling parameter
 *
 * This function is taken from official ROOT tutorial.
 * See, e.g.: https://root.cern/doc/v608/langaus_8C.html
 *
 * Fit parameters:
 * \param x variable
 * \param width (scale) parameter of Landau density
 * \param mp most Probable (MP, location) parameter of Landau density
 * \param area total area (integral -inf to inf, normalization constant)
 * \param sigma width (sigma) of convoluted Gaussian function
 * \param np number of convolution steps
 * \param sc convolution extends to +-sc Gaussian sigmas
 * 
 * In the Landau distribution (represented by the CERNLIB approximation),
 * the maximum is located at x=-0.22278298 (defined in `gLandauFMax` global
 * const) with the location parameter=0. This shift is corrected within this
 * function, so that the actual maximum is identical to the MP parameter.
 * 
 * \ingroup numerical-utils
 * */
Double_t langaus( Double_t x
                , Double_t width
                , Double_t mp
                , Double_t area
                , Double_t sigma
                , Int_t np=100
                , Double_t sc=5.0
                );

/// `langaus()` alias for ROOT's fitting routines
///
/// \ingroup numerical-utils
Double_t langausfun(Double_t *x, Double_t *par);

/** Will fit provided histogram with `langaus()` function with standard ROOT
 * fitting algorithms returning function and model parameters.
 *
 * \param[in] his histogram to fit
 * \param[in] fitrange lo and hi boundaries of fit range
 * \param[in] startvalues 4-element, reasonable start values for the fit
 * \param[in] parlimitslo 4-element, lower parameter limits
 * \param[in] parlimitshi 4-element, upper parameter limits
 * \param[in] fitOpts sets fitting options. Default is "LRQ0" (when NULL)
 * \param[out] fitparams 4-element returns the final fit parameters
 * \param[out] fiterrors 4-element returns the final fit errors
 * \param[out] chiSqr returns the chi square
 * \param[out] ndf returns ndf
 * \return an instance of `TF1` representing fit function
 *
 * \note This function allocates `TF1` instance on heap. One has to `delete`
 *       it afterwards.
 *
 * \ingroup numerical-utils
 * */
std::pair<TF1 *, bool>
      langausfit( TH1F * his
                , Double_t * fitrange
                , Double_t * startvalues
                , Double_t * parlimitslo
                , Double_t * parlimitshi
                , const char * fitOpts
                , Double_t * fitparams
                , Double_t * fiterrors
                , Double_t & chiSqr
                , Int_t & ndf
                );

}
}

#endif

