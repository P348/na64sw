#ifndef H_NA64SW_NUMERICAL_ROUTINE_BIPLANE_INTERSECTION_H
#define H_NA64SW_NUMERICAL_ROUTINE_BIPLANE_INTERSECTION_H

#include "na64sw-config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**\brief Finds "intersection" points defined by two non-complanar measurements
 *
 * A solution for rather common case of what is assumed to be a "track point",
 * when it should be defined by 1D measurements of two planes. Each "measurement"
 * define a line in space, but two given lines do not intersect (despite
 * are may be very close) -- *skew lines* case.
 *
 * The proposed solution is, having some "sight" direction, obtain a "seen"
 * intersection. For most of the cases this direction is defined either by
 * closest approach (perpendicular to both lines), or by expected ionizing
 * particle direction.
 *
 * Found solution \f$\vec{x1}, \vec{x2}\f$ is a line segment directed according
 * to "sight" direction with endpoints lying on first and second line.
 * Applications may use those points independently or take an average to define
 * a spatial point of approximate hit position.
 *
 * \arg[in] k1 direction vector of 1st measurement
 * \arg[in] k2 direction vector of 2nd measurement
 * \arg[in] k3 "sight" direction; if NULL -- calculated as closest approach
 * \arg[in] r01 some point on 1st line
 * \arg[in] r02 some point on 2nd line
 * \arg[out] x1 "intersection" point on first line (or equidistant point if x2 is NULL)
 * \arg[out] x2 "intersection" point on first line (or NULL)
 * \returns 0 on success, -1 if no solution exists
 *
 * \note Uses GSL inside (thus, may invoke GSL error handler in some
 *       circumustances).
 *
 * \ingroup numerical-utils
 * */
int
na64sw_projected_lines_intersection( const na64sw_Float_t * k1, const na64sw_Float_t * k2, const na64sw_Float_t * k3
                                   , const na64sw_Float_t * r01, const na64sw_Float_t * r02
                                   , na64sw_Float_t * x1, na64sw_Float_t * x2
                                   );

#if 0
/**\brief Fits a spatial line to minimize distances from the line to lines and
 *        points
 *
 * With given set of (weighted) points and lines, tries to find parameters of
 * a closest line.
 * */
int
na64sw_linear_fit( const na64sw_Float_t * pts, const na64sw_Float_t * pw, size_t nPoints
                 , const na64sw_Float_t * lns, const na64sw_Float_t * lw, size_t nLines
                 , na64sw_Float_t * direction, na64sw_Float_t * pt
                 );
#endif

#ifdef __cplusplus
}
#endif

#endif  /* H_NA64SW_NUMERICAL_ROUTINE_BIPLANE_INTERSECTION_H */

