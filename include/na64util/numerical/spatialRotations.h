#ifndef H_NA64_UTIL_NUMERIC_ROTATIONS_H
#define H_NA64_UTIL_NUMERIC_ROTATIONS_H

/**\file
 * \brief Defines routines related to spatial rotation angles
 *
 * File defines function to build (row-major) rotation matrices based on Euler
 * or Tait-Bryan angular triplets and function to compute rotation angles based
 * on rotation matrix respecting particular rotation order. Both functions
 * are provided in two variants: for single and double precision floating point
 * type.
 *
 * Note, that rotation by extrinsic angles always can be inferred from
 * intrinsic rotations by reordering the rotation sequence.
 *
 * \note due to natural ambiguity of angular representation of
 *       rotations, angles > M_PI/2 can violate the identity of angular
 *       triplets conversion although the result is mathematically correct.
 * */

#ifdef __cplusplus
extern "C" {
#endif

/**\brief Type encoding rotation order (Euler or Tait-Bryan) */
enum na64sw_RotationOrder {
    na64sw_Rotation_xzx =  0,
    na64sw_Rotation_xyx =  1,
    na64sw_Rotation_yxy =  2,
    na64sw_Rotation_yzy =  3,
    na64sw_Rotation_zyz =  4,
    na64sw_Rotation_zxz =  5,
    na64sw_Rotation_xzy =  6,
    na64sw_Rotation_xyz =  7,
    na64sw_Rotation_yxz =  8,
    na64sw_Rotation_yzx =  9,
    na64sw_Rotation_zyx = 10,
    na64sw_Rotation_zxy = 11
};

/**\brief Returns order encoding int by given rotation order string.
 *
 * First argument shall encode rotation angles according to following rules:
 *  - three letters corresponding to the given angles of same case
 *  - one of the letters: "xyzXYZ"
 *  - extrinsic rotation angles shall be denoted by lower case, external by
 *    uppercase
 *  - no consequitive rotations by same axes (intrinsic or extrinsic), i.e.
 *    rotations like "xx" or "YY" are prohibited
 * Second order is the ptr to result destination. Returned code is 0 if code
 * was found and 1 if an error occured.
 *
 * \param[in] str Order-encoding string
 * \param[out] dest Order-encoded enum destination ptr
 * \return 1 on error, 0 if match has been found.
 * */
int na64sw_rotation_order_from_str(const char * str, enum na64sw_RotationOrder * dest);

/**\brief Returns rotation matrix for the angular triplet (4-byte float)
 *
 * This function computes rotation matrix (in row-first format) respecting
 * angular order fiven as 3-char lowercase string (i.e. "xyz", "xyx", etc.,
 * terminated or not by zero character). Only standard Euler or Tait-Bryan
 * (Cardan) angles are supported.
 *
 * \param[in] order 3-char downcase string of rotation order
 * \param[in] angles 3-element array of angles to rotate
 * \param[out] dest ptr to 9-element matrix array to write element values to
 * */
int na64sw_rotation_matrix_f( enum na64sw_RotationOrder order
                            , const float  * angles, float  * dest );
/**\brief Returns rotation matrix for the angular triplet (8-byte float)
 *
 * See docs for `na64sw_rotation_matrix_f()`, the only difference here is the
 * operation type. */
int na64sw_rotation_matrix_d( enum na64sw_RotationOrder order
                            , const double * angles, double * dest );

/**\brief Computes rotation angular triplet for given rotation matrix (float)
 *
 * For given rotation matrix, calculates corresponding spatial angles,
 * respecting assumed rotation order. For notes on order syntax, see
 * `na64sw_rotation_matrix_f()` docs.
 *
 * \param[in] order 3-char downcase string of rotation order
 * \param[in] R 9-element array representation of rotation matrix
 * \param[out] angles ptr to 3-element dest array to write corresponding angles
 */
int na64sw_find_rotation_for_f( enum na64sw_RotationOrder order
                              , const float *  R, float *  angles );
/**\brief Computes rotation angular triplet for given rotation matrix (double)
 *
 * See docs for `na64sw_find_rotation_for_f()`, the only difference here is the
 * operation type. */
int na64sw_find_rotation_for_d( enum na64sw_RotationOrder order
                              , const double * R, double * angles );

#ifdef __cplusplus
}  /* extern "C"*/
#endif

#endif  /*H_NA64_UTIL_NUMERIC_ROTATIONS_H*/

