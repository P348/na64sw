#pragma once

#include <utility>
#include <cstddef>  // std::size_t
#include <functional>  // std::hash

namespace na64dp {
namespace util {

struct PairHash {
    template <class T1, class T2> size_t
    operator()(const std::pair<T1, T2>& p) const {
        auto hash1 = std::hash<typename std::remove_const<T1>::type>{}(p.first);
        auto hash2 = std::hash<typename std::remove_const<T2>::type>{}(p.second);
        return hash1 ^ hash2;
    }
};

}
}

