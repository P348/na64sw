#pragma once

#include "na64util/apply-tuple.hh"

#include <istream>
#include <regex>

namespace na64dp {

namespace error {

/** \brief Tabular data (CSV or similar) parsing error exception.
 *
 * Exception is thrown from within CSV parsing utility code when some violation
 * of basic CSV syntax occurs. Typically bears information on exact place and
 * reason of an error.
 * */
class CSVParsingError : public std::runtime_error {
private:
    size_t _nLine, _nToken;
public:
    /// Accepts error text description, optionally line number and token
    /// (column) number
    CSVParsingError( const char * what_
                   , size_t nLine=0
                   , size_t nToken=0 ) throw();
    /// Sets line number of an error (for re-throw)
    void n_line(size_t nLine) throw() { _nLine = nLine; }
    /// Sets token number of an error (for re-throw)
    void n_token(size_t nToken) throw() { _nToken = nToken; }
    /// Returns line number of an error
    size_t n_line() const throw() { return _nLine; }
    /// Returns token (column) number within an error
    size_t n_token() const throw() { return _nToken; }
};

}  // namespace error

namespace csv {

/// ASCII/CSV parsing traits implementations for CSV IO
template<typename T> struct ValueParser;

template <typename T, std::size_t N>
struct ValueParser< std::array<T, N> > {
    static size_t read( std::sregex_iterator & it, std::array<T, N> & value ) {
        size_t nRead = 0;
        for( size_t n = 0; n < N; ++n ) {
            nRead += ValueParser<T>::read( it, value[n] );
        }
        return nRead;
    }
};

template<>
struct ValueParser<std::string> {
    static size_t read( std::sregex_iterator & it, std::string & value ) {
        value = it++->str();
        return 1;
    }
};

template<>
struct ValueParser<double> {
    static size_t read( std::sregex_iterator & it, double & value ) {
        value = stod(it++->str());
        return 1;
    }
};

template<>
struct ValueParser<int> {
    static size_t read( std::sregex_iterator & it, int & value ) {
        value = stoi(it++->str());
        return 1;
    }
};

template<>
struct ValueParser<size_t> {
    static size_t read( std::sregex_iterator & it, size_t & value ) {
        value = stoul(it++->str());
        return 1;
    }
};

/**\brief Reader for the ASCII streams that holds tabular entries
 *
 * \todo Detailed description shall be added once API will become distilled.
 * \todo Errors/warning
 * \todo Ignore comments symbols
 * */
class Reader {
private:
    size_t _lineCount;  ///< line counter
    size_t _tokCount;  ///< tokens (columns) counter
    std::istream & _is;  ///< stream ref to be read
    std::regex _wrx;
    std::sregex_iterator _i
                       , _begin
                       , _end;
protected:
    virtual std::string _get_next_line();
    std::string _tail;
    size_t _lastMatchPos;
public:
    /**\brief Constructs reader instance over a stream */
    Reader( std::istream & is
             , size_t lineCountStart=0
             , const std::string & tokRx="[^\\s]+" );

    /**\brief Entry-line reading method, handy for loop usage
     *
     * Use it within a `while()` expression to read a sequence of
     * entries from the stream */
    template<typename ... ArgsT>
    bool read( std::tuple<ArgsT...> & values ) {
        _tail.clear();
        _lastMatchPos = std::string::npos;
        auto line = _get_next_line();
        if( line.empty() ) return false;
        _begin = std::sregex_iterator( line.begin()
                                     , line.end()
                                     , _wrx );
        _i = _begin;
        _tokCount = 0;
        try {
            ::na64dp::meta::for_each_in_tuple( values, *this );
        } catch( ::na64dp::error::CSVParsingError & e ) {
            e.n_line( _lineCount );
            throw e;
        }
        if( _lastMatchPos < line.size() ) {
            _tail = line.substr( _lastMatchPos );
            _lastMatchPos = _tail.find_first_not_of(" \t");
            if( _lastMatchPos == std::string::npos ) {
                _tail.clear();  // wipe empty line
            } else {
                _tail = _tail.substr(_lastMatchPos);
            }
        }
        return true;
    }

    /// Returns string after last token that correspondied to parsed line
    std::string tail() const { return _tail; }

    /// Convenient to use in "one entry per call" case
    template<typename ... ArgsT>
    Reader & operator>>(std::tuple<ArgsT...> & values) {
        while( !read( values ) ) {}
        return *this;
    }

    // note: shall be used only by each_in_tuple(), but it would be too messy
    // to make them friends
    template<typename T>
    void operator()( T & value ) {
        try {
            if( _end == _i ) {
                throw ::na64dp::error::CSVParsingError("Bad tokens (columns) count in line.");
            }
            _tokCount += ValueParser<T>::read(_i, value);
        } catch( error::CSVParsingError & e ) {
            e.n_token( _tokCount );
            throw e;
        }
        _lastMatchPos = _i->position() + _i->length();
    }
};

}  // namespace ::na64dp::csv
}  // namespace na64dp

