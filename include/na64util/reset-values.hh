#pragma once

#include <variant>

namespace na64dp {
namespace util {

template<typename T> void reset_values(T &);

template<typename ... ArgsT> void
reset_values< std::variant<std::monostate, ArgsT ...> >( std::variant<std::monostate, ArgsT ...> & obj ) {
    obj = std::monostate();
}

}
}
