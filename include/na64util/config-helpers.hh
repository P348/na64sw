#include "na64util/exception.hh"
#include "na64util/str-fmt.hh"

#include <cstdio>
#include <limits>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <typeinfo>
#include <cassert>

#include <unordered_set>
#include <yaml-cpp/exceptions.h>
#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace errors {

/// Root exception class for all the PDef errors
class PDefError : public GenericRuntimeError {
public:
    std::list<std::string> path;
public:
    PDefError(const char * msg) : GenericRuntimeError(msg) {}
};

/// No item found in section by given key
class NoKeyInSection : public PDefError {
public:
    const std::string key;
public:
    NoKeyInSection(const char * key_)
        : PDefError("Item not found in section")
        , key(key_)
        {}
};

/// No item found in section by given key
class NoItemInArray : public PDefError {
public:
    const ssize_t key;
public:
    NoItemInArray(ssize_t key_)
        : PDefError("No item of this number in array")
        , key(key_)
        {}
};

/// Basic error thrown by validators
class InvalidParameter : public PDefError {
public:
    InvalidParameter(const char * msg) : PDefError(msg) {}
};

///\brief Requested type does not support the value
///
/// Thrown when assignment (destination) type is not large enough to keep
/// the source value
class ParameterTypeOverflow : public InvalidParameter {
public:
    ParameterTypeOverflow(const char * msg) : InvalidParameter(msg) {}
};

/// Basic error thrown by validators if value has permitted type but
/// value is prohibited
class InvalidParameterValue : public InvalidParameter {
public:
    InvalidParameterValue(const char * msg) : InvalidParameter(msg) {}
};

/// Thrown when bad parameter or parameter section or array name is provided
class InvalidParameterName : public InvalidParameter {
public:
    InvalidParameterName(const char * msg) : InvalidParameter(msg) {}
};

/// Type cast errors
class InvalidParameterType : public InvalidParameter {
public:
    InvalidParameterType(const char * msg) : InvalidParameter(msg) {}
};

/// Parameter is not set, but its value requested in runtime context
class ParameterValueIsNotSet : public InvalidParameter {
public:
    ParameterValueIsNotSet() : InvalidParameter("Parameter value is not set") {}
};

/// Thrown if path expression is invalid
class BadPath : public PDefError {
public:
    std::string path;
    size_t nChar;
    BadPath(const char * msg, size_t nc) : PDefError(msg), nChar(nc) {}
};

/// Thrown in case of inconsistent parameter definition
class PDefInconsistency : public PDefError {
public:
    PDefInconsistency(const char * msg) : PDefError(msg) {}
};

/// Thrown in case of re-definition already defined node
class NodeRedefinition : public PDefInconsistency {
public:
    NodeRedefinition(const char * msg) : PDefInconsistency(msg) {}
};

/// Root exception class for all YAML-related errors
class GenericYAMLError : public PDefError {
public:
    YAML::Exception origError;
public:
    GenericYAMLError(const YAML::Exception & e, const char * msg)
        : PDefError(msg)
        , origError(e)
        {}
};

}  // namespace ::na64dp::error
namespace util {
class Composer;  // fwd
namespace pdef {

//
// Runtime parameters path

/// Returns true if given string can be used as parameter name (scalar,
/// section, array, etc)
bool validate_parameter_name(const char * nm);

/// Single path token entry
struct PathToken {
    enum {
        kUnknown = 0x0,

        kAnyStrKey = 0x2,
        kStrKey = kAnyStrKey | 0x1,

        kAnyIndex = 0x4,
        kIntKey = kAnyIndex | 0x1,
    } type;
    union {
        const char * stringKey;
        ssize_t index;
    } pl;

    PathToken() : type(kUnknown), pl{nullptr} {}
    PathToken(const char * ptr) : type(kStrKey), pl{ptr} {}
    PathToken(ssize_t i) : type(kIntKey), pl{.index=i} {}
};

///\brief Path to parameter
/// Modifies provided string to obtain representation of parameter path
///
/// Path examples:
///  ""
///  "one"
///  "one.two"
///  "one[0].two.three"
///  "one.*.three"
///  "one.foo*.bar[34]"
///  "one[*].two"
class Path : public std::vector<PathToken> {
private:
    /// Null-delimited list of tokens of size `_strBufLen`, may be not allocated
    char * _strBuf;
    /// Size of path string buffer
    size_t _strBufLen;
public:
    Path(const char * strexpr=nullptr);
    Path(const std::string & cppStr) : Path(cppStr.c_str()) {} 
    Path(const Path & orig) : Path(orig.to_string().c_str()) {}
    ~Path();

    Path & operator=(const Path &);

    std::string to_string() const;

    operator std::string() const { return to_string(); }
};

//
// Scalars

/// Parameter definition base class
struct BasePDef {
    virtual void set(const YAML::Node &) = 0;
    virtual ~BasePDef() {}
};

/// Common base for scalar parameters
class BaseScalar : public BasePDef {
private:
    /// RO attr, C++ RTTI type
    const std::type_info & _typeInfo;
protected:
    bool _isSet;
public:
    BaseScalar(const std::type_info & ti) : _typeInfo(ti), _isSet(false) {} 
    /// Returns ref to C++ RTTI type
    const std::type_info & type_info() const { return _typeInfo; }

    bool is_set() const { return _isSet; }

    template<typename T> void set_as_of_type(const T &);
    template<typename T> T get_as_of_type() const;
};

template<typename T>
class Scalar : public BaseScalar {
public:
    struct iValidator {
        /// Shall throw exception if parameter is not valid
        virtual void validate(const T &) const = 0;
    };
protected:
    std::shared_ptr<iValidator> _validator;
    T _value;
public:
    Scalar() : BaseScalar(typeid(T))
             , _validator(nullptr)
             {}

    void set_validator(std::shared_ptr<iValidator> vv) {
        _validator = vv;
    }

    void set(const T & v) {
        _value = v;
        if(_validator) _validator->validate(_value);
        _isSet = true;
    }

    void set(const YAML::Node & yamlNode) {
        _value = yamlNode.as<T>();
        if(_validator) _validator->validate(_value);
        _isSet = true;
    }

    T get() const {
        if(!is_set()) throw errors::ParameterValueIsNotSet();
        return _value;
    }
};  // class Scalar

// TODO: this getters now throw `std::bad_cast` in case of type mismatch. It is
// possible, however, to involve a table of permitted conversions to perform
// implicit static casts

template<typename T> void
BaseScalar::set_as_of_type(const T & v) {
    dynamic_cast<Scalar<T>>(*this).set(v);
}

template<typename T> T
BaseScalar::get_as_of_type() const {
    return dynamic_cast<Scalar<T>>(*this).get();
}

//
// Basic validators for scalar parameters

template<typename T>
struct RangeValidator : public Scalar<T>::iValidator {
    T left, right;
    RangeValidator() : left( std::numeric_limits<T>::min())
                     , right(std::numeric_limits<T>::max())
                     {}
    void validate(const T & v) const override {
        if(v >= left && v <= right) return;
        std::ostringstream oss;
        if(  left != std::numeric_limits<T>::min()
         && right != std::numeric_limits<T>::max()
         ) {
            oss << "value " << v
                << " is out of range [" << left << ", " << right << "]";
        } else if( right != std::numeric_limits<T>::max() ) {
            oss << "value " << v
                << " should be less than or equal to " << right;
        } else {
            oss << "value " << v
                << " should be greater than or equal to " << left;
        }
        throw errors::InvalidParameterValue(oss.str().c_str());
    }
};

template<typename T>
struct SetValidator : public Scalar<T>::iValidator {
    std::unordered_set<T> permittedValues;
    SetValidator() {}

    void validate(const T & v) const override {
        auto it = permittedValues.find(v);
        if(it != permittedValues.end()) return;
        std::ostringstream oss;
        if(permittedValues.size() > 6) {  // TODO: configurable
            oss << "Value \"" << v << "\" is not recognized.";
        } else {
            for(const auto & pv : permittedValues) {
                std::set<std::string> sorted( permittedValues.begin()
                                            , permittedValues.end()
                                            );
                oss << "Value \"" << v << "\" is not one of the permitted options: "
                    << str_join(sorted.begin(), sorted.end());
            }
        }
        throw errors::InvalidParameterValue(oss.str().c_str());
    }
};

// TODO: regex validator
// TODO: non-zero (single prohibited value) validator

//
// Parameter tree node

/// Node of parameter definition tree
class Node {
public:
    /// This node description (public RW)
    std::string description;
    /// Node-section type
    typedef std::unordered_map<std::string, std::shared_ptr<Node>> Section;
    /// Node-array type; can have homogeneous topology
    struct Array : public std::vector<std::shared_ptr<Node>> {
        std::shared_ptr<Node> topology;  ///< desired topology for array, can be null
    };
private:
    // this is not exposed to parent scope, should be steered by this class only
    /// This node type
    enum {
        kUnset = 0,  ///< empty node, no value type defined
        kScalar,  ///< node contains some kind of scalar value
        kArray,  ///< list/array node
        kSection,  ///< (sub-)section node
        kUnion,  ///< union type
        kCustom  ///< node with custom parameter
    } _type;
    /// Node payload
    union {
        util::pdef::BasePDef * parameterPtr;
        Section * section;
        Array * array;
    } _pl;
public:
    Node() : _type(kUnset)
           , _pl{nullptr}
           {}

    bool is_unset() const { return _type == kUnset; }
    bool is_section() const { return _type == kSection; }
    bool is_scalar() const { return _type == kScalar; }
    bool is_array() const { return _type == kArray; }
    bool is_union() const { return _type == kUnion; }
    bool is_custom() const { return _type == kCustom; }

    const char * node_type_name() const;

    ///\brief Marks unset node as scalar of given type
    ///
    ///\throws errors::NodeRedefinition if node already has type
    template<typename T>
    pdef::Scalar<T> & set_scalar() {
        if(_type != kUnset) throw errors::NodeRedefinition("Node type has been set already");
        _type = kScalar;
        _pl.parameterPtr = new Scalar<T>();
        return *static_cast<pdef::Scalar<T>*>(_pl.parameterPtr);
    }
    ///\brief Marks node as section node
    ///
    ///\throws errors::NodeRedefinition if node already has type
    Section & set_section();
    ///\brief Marks node as section node
    ///
    ///\throws errors::NodeRedefinition if node already has type
    Array & set_array();
    // TODO: ?
    //\brief Marks node as union node
    //
    //\throws errors::NodeRedefinition if node already has type
    //Union & set_union();
    
    BaseScalar & as_scalar();
    const BaseScalar & as_scalar() const;
    template<typename T> Scalar<T> & as_scalar_of_type() {
        auto & bs = as_scalar();
        return dynamic_cast<Scalar<T>&>(bs);
    }
    template<typename T> const Scalar<T> & as_scalar_of_type() const {
        auto & bs = as_scalar();
        return dynamic_cast<const Scalar<T>&>(bs);
    }

    Section & as_section();
    const Section & as_section() const;

    Array & as_array();
    const Array & as_array() const;

    // section getter shortcuts
    Node & operator[](const std::string &);
    const Node & operator[](const std::string &) const;
    bool has(const std::string &) const;

    // array getter shortcuts
    Node & operator[](ssize_t);
    const Node & operator[](ssize_t) const;
    bool has(ssize_t) const;
};

#if 1
class Composer {
private:
    struct NodeEntry {
        Node & node;
        std::string name;
        size_t index;
        std::shared_ptr<Node> _lastNode;
    };
    std::vector<NodeEntry> _stack;
protected:
    void _assure_array();
    void _assure_section();
public:
    Composer() = delete;
    Composer(Node & node);

    /// Defines required scalar named parameter (attribute), with name and
    /// optional description
    template<typename T>
    Composer & p( const char * name
                , const char * description=nullptr
                ) {
        _assure_section();
        if(!validate_parameter_name(name))
            throw errors::InvalidParameterName("Invalid parameter name.");

        auto nodePtr = std::make_shared<Node>();
        nodePtr->set_scalar<T>();
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_section().emplace(name, nodePtr);
        _stack.back()._lastNode = nodePtr;
        return *this;
    }

    /// Defines required scalar named parameter (attribute), with name,
    /// validator and optional description
    template<typename T>
    Composer & p( const char * name
                , std::shared_ptr<typename Scalar<T>::iValidator> & validator
                , const char * description=nullptr
                ) {
        _assure_section();
        if(!validate_parameter_name(name))
            throw errors::InvalidParameterName("Invalid parameter name.");

        auto nodePtr = std::make_shared<Node>();
        nodePtr->set_scalar<T>().set_validator(validator);
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_section().emplace(name, nodePtr);
        _stack.back()._lastNode = nodePtr;
        return *this;
    }

    /// Defines (optional) scalar named parameter (attribute), with name,
    /// default value and optional description
    template<typename T>
    Composer & p( const char * name
                , T dftVal
                , const char * description=nullptr
                ) {
        _assure_section();
        if(!validate_parameter_name(name))
            throw errors::InvalidParameterName("Invalid parameter name.");

        auto nodePtr = std::make_shared<Node>();
        nodePtr->set_scalar<T>().set(dftVal);
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_section().emplace(name, nodePtr);
        _stack.back()._lastNode = nodePtr;
        return *this;
    }

    /// Defines (optional) scalar named parameter (attribute), with name,
    /// validator, default value and optional description
    template<typename T>
    Composer & p( const char * name
                , std::shared_ptr<typename Scalar<T>::iValidator> & validator
                , T dftVal
                , const char * description=nullptr
                ) {
        _assure_section();
        if(!validate_parameter_name(name))
            throw errors::InvalidParameterName("Invalid parameter name.");

        auto nodePtr = std::make_shared<Node>();
        auto & s = nodePtr->set_scalar<T>();
        s.set_validator(validator);
        s.set(dftVal);
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_section().emplace(name, nodePtr);
        _stack.back()._lastNode = nodePtr;
        return *this;
    }

    /// Defines required scalar parameter element, with optional description
    template<typename T>
    Composer & el( const char * description=nullptr ) {
        _assure_array();
        auto nodePtr = std::make_shared<Node>();
        nodePtr->set_scalar<T>();
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_array().push_back(nodePtr);
        _stack.back()._lastNode = nodePtr;
        return *this;
    }

    /// Defines attribute sub-node (section or array)
    Composer & bgn_sub( const char * name
                      , const char * description=nullptr
                      ) {
        _assure_section();
        if(!validate_parameter_name(name))
            throw errors::InvalidParameterName("Invalid subsection name.");

        auto nodePtr = std::make_shared<Node>();
        if(description)
            nodePtr->description = description;
        _stack.back().node.as_section().emplace(name, nodePtr);
        _stack.back()._lastNode = nodePtr;
        _stack.push_back(NodeEntry{*nodePtr, name, 0});
        return *this;
    }

    /// Finalizes attribute sub-node (section or array)
    Composer & end_sub( const char * name=nullptr ) {
        if(name) {
            if(_stack.size() < 2) {
                throw errors::InvalidParameterName(format("No "
                            " opened subsections to finalize \"%s\""
                            , name).c_str());
            }
            auto it = _stack.rbegin();
            if(name != it->name) {
                throw errors::InvalidParameterName(format("Finilizing"
                            " subsection \"%s\" while lates open is \"%s\""
                            , name, it->name.c_str()
                            ).c_str());
            }
        }
        _stack.pop_back();
        return *this;
    }

    #if 0
    /// Defines required parameter with validator, name and description
    template<typename T>
    Composer & p( const char * name
                  , std::shared_ptr<typename pdef::Scalar<T>::iValidator> & validator
                  , const char * description=nullptr
                  ) {
        if(_lastNode) _finalize_last_node();
        assert(nullptr == _lastNode);
        _lastNodeName = name;
        _lastNode = new pdef::Node(description);
        auto pp = _lastNode->_set_scalar<T>();
        pp->set_validator(validator);
        _lastNode->_set_scalar(pp);
        return *this;
    }
    /// Defines parameter with default value (optional), with name and description
    template<typename T>
    Composer & p( const char * name
                  , const T & dft
                  , const char * description=nullptr
                  ) {
        if(_lastNode) _finalize_last_node();
        assert(nullptr == _lastNode);
        _lastNodeName = name;
        _lastNode = new pdef::Node(description);
        auto pp = _lastNode->_set_scalar<T>();
        pp->set(dft);
        return *this;
    }
    /// Defines parameter with default value and validator, with name and description
    template<typename T>
    Composer & p( const char * name
                  , const T & dft
                  , std::shared_ptr<typename pdef::Scalar<T>::iValidator> & validator
                  , const char * description=nullptr
                  ) {
        if(_lastNode) _finalize_last_node();
        assert(nullptr == _lastNode);
        _lastNodeName = name;
        _lastNode = new pdef::Node(description);
        auto pp = _lastNode->_set_scalar<T>();
        pp->set_validator(validator);
        pp->set(dft);
        return *this;
    }

    //RuntimeCfg & start_section(const char * name, const char * description=nullptr);
    //RuntimeCfg & end_section(const char * name=nullptr);

    //void set(const YAML::Node &);

    /// Returns node by path string
    pdef::Node & get(const char * path);

    /// Returns node by path string, const version
    const pdef::Node & get(const char *) const;
    #endif
};
#endif

}  // namespace ::na64dp::util::pdef
}  // namespace ::na64dp::util
}  // namespace na64dp

