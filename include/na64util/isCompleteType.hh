#pragma once

#include <type_traits>

namespace na64dp {
namespace util {

template<class First, std::size_t>
using first_t = First;

template<class T>
struct is_complete_type: std::false_type {};

template<class T>
struct is_complete_type<first_t<T, sizeof(T)>> : std::true_type {};

}  // namespace ::na64dp::util
}  // namespace na64dp

