#pragma once

#include "na64sw-config.h"

#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
#if defined(BFD_FOUND) && BFD_FOUND
// see discussion: https://github.com/mlpack/mlpack/issues/574
#ifndef PACKAGE
  #define PACKAGE
  #ifndef PACKAGE_VERSION
    #define PACKAGE_VERSION
    #include <bfd.h>
    #undef PACKAGE_VERSION
  #else
    #include <bfd.h>
  #endif
  #undef PACKAGE
#else
  #include <bfd.h>
#endif
#   include <link.h>
#else  // no BFD
// When no bfd defined, alias type as this.
typedef unsigned long long bfd_vma;
#endif
#endif

#include <stdexcept>
#include <forward_list>

namespace na64dp {
namespace errors {

#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
typedef std::string SafeString;  // TODO: custom allocator
struct StackRecord {
    unsigned int lFound;    ///< 1 if line was found, 0 otherwise.
    unsigned int lineno;    ///< line number
    SafeString function,    ///< function name (possibly, mangled)
               elfFilename, ///< object filename
               srcFilename, ///< source filename
               failure;     ///< failure reason
#if defined(BFD_FOUND) && BFD_FOUND
    bfd_vma     addr,           ///< runtime ELF address
                soLibAddr;      ///< static shared library ELF address
    asymbol **  sTable;
# endif
};
typedef std::forward_list<StackRecord> StackRecords; // TODO: custom allocator
//std::ostream & operator<<(std::ostream& os, const StackRecord & t);
#endif

/**\brief Base class of exception in NA64sw environment
 *
 * Any error at some point anticipated by NA64sw or its "standard" extensions
 * have to inherit this class in order to indicate runtime error related to
 * NA64sw ecosystem.
 *
 * \note inherited from `std::runtime_error` so subject errors runtime errors
 * \note may provide stacktrace info, if `NA64DP_ENABLE_EXC_STACKTRACE` macro enabled
 * */
class GenericRuntimeError : public std::runtime_error {
#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
private:
    /// Keeps call stack of an error
    StackRecords _stackRecords;
public:
    /// Returns introspection info of the error
    const StackRecords & stack() const throw() { return _stackRecords; }
#endif
public:
    GenericRuntimeError( const char * s ) throw();
};

#if defined(NA64DP_ENABLE_EXC_STACKTRACE) && NA64DP_ENABLE_EXC_STACKTRACE
/// Prints colored stacktrace for easy reading
void fancy_print_stacktrace(std::ostream &, const StackRecords &);
#endif

}  // namespace ::na64dp::errors
}  // namespace na64dp

