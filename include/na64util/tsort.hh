#pragma once

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <set>
#include <cstdint>

#include <iostream>  // XXX

namespace na64dp {
namespace util {

/**\brief Implements Kahn's algorithm for topological sort
 *
 * A rather straightforward implementation of Kahn's algorithm for topological
 * sort.
 *
 * \note Instances are disposable. Graph is invalidated after `sorted()` returns
 * \todo profile, optimize?
 */
template< typename T
        , typename HashT=std::hash<T>
        , typename EqualT=std::equal_to<T>
        , typename CompareT=std::less<T>
        >
class DAG : private std::unordered_multimap<T, T, HashT, EqualT> {
public:
    typedef std::unordered_multimap<T, T, HashT, EqualT> Parent;
    typedef typename Parent::value_type Edge;

    using Parent::empty;

    /// Returns sorted sequence and invalidates the object
    ///
    /// \note that if this graph has edges after this methood done, it means
    ///       the graph contains cycles (yet, returned result is not empty).
    std::vector<T> sorted() {
        std::vector<T> result;
        std::unordered_set<T, HashT> freeNodes;
        std::unordered_map<T, uint32_t, HashT> indegree;
        {
            // get free nodes as subtraction of the sets
            std::unordered_set<T, HashT, EqualT> from, to;
            std::transform( Parent::begin(), Parent::end()
                          , std::inserter(from, from.begin())
                          , []( const Edge & edge ) {return edge.first;} );
            std::transform( Parent::begin(), Parent::end()
                          , std::inserter(to, to.begin())
                          , []( const Edge & edge ) {return edge.second;} );
            std::set_difference( from.begin(), from.end()
                               , to.begin(), to.end()
                               , std::inserter(freeNodes, freeNodes.begin())
                               );
            if(freeNodes.empty()) return result;  // trivial, or perfect cyclic graph.
            std::unordered_set<T, HashT> ins;
            std::set_difference( to.begin(), to.end()
                               , from.begin(), from.end()
                               , std::inserter(ins, ins.end()));
            std::transform( ins.begin(), ins.end()
                          , std::inserter(indegree, indegree.begin())
                          , []( const T & k ) { return std::pair<T, uint32_t>{k, 0}; } );
            for( const auto & p : *this ) { ++indegree[p.second]; }
        }

        while(!freeNodes.empty()) {
            T n = *freeNodes.begin();
            freeNodes.erase(freeNodes.begin());
            result.push_back(n);
            auto er = Parent::equal_range(n);
            for( auto it = er.first; it != er.second; ++it ) {
                if( ! (--indegree[it->second]) ) {
                    freeNodes.insert(it->second);
                }
            }
            Parent::erase(er.first, er.second);
        }
        return result;
    }
    
    /// Adds edge "from-to"
    void add( const T & from, const T & to ) {
        Parent::emplace(from, to);
    }

    /// Returns (remaining) edges, useful for debug
    const Parent & edges() { return *this; }
};

}  // namespace ::na64dp::util
}  // namespace na64dp

