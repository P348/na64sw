/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/str-fmt.hh"

namespace na64dp {
namespace calib { class Manager; }  // fwd
class iEvProcInfo;  // fwd

/**\brief A base class for (loadable) extension instance
 *
 * User handlers may bring some additional dependencies of global/singleton
 * objects that have to be initialized/bound to NA64SW infrastructure in other
 * way as other handlers.
 *
 * Typical example is ROOT objects like geometry manager, event display window
 * or other common singletons. This base class provides a way to register such
 * objects, initialize and access them further with single entry point
 * (`na64dp::Extensions`).
 * */
struct iRuntimeExtension {
    virtual ~iRuntimeExtension() {}
    virtual void init(calib::Manager &, iEvProcInfo * epi) = 0;
    //virtual std::unordered_map<std::string, std::string> parameters() const = 0;
    virtual void finalize() {}
    virtual void set_parameter(const std::string &, const std::string &) = 0;

    /// Helper method, shortcut for `dynamic_cast<>()`
    template<typename T> T & as() {
        T * p = dynamic_cast<T*>(this);
        if(!p) {
            // TODO: details (demangled RTTI type, etc)
            NA64DP_RUNTIME_ERROR("Bad cast for extension.");
        }
        return *p;
    }
};

/**\brief An entry point for runtime extensions
 *
 * Manages instances of (loadable) extension classes (subclasses of
 * `iRuntimeExtension`). Implied lifecycle:
 *  1. Loadable modules adds their extension objects with `add()` method
 *  2. After logging, monitor and calibration manager are instantiated, an
 *    `init()` method called forwarding calls to extension requested.
 *  3. After app is done, it calls `shutdown()` method to clear resources used
 *     by extensions.
 * Note, that some extensions may not create objects to be used during
 * `init()`, depending on their configuration.
 * */
class Extensions : private std::unordered_map<std::string, iRuntimeExtension *> {
private:
    static Extensions * _self;
    /// Private ctr invoked by `self()`
    Extensions();
public:
    /// Returns instance (singleton)
    static Extensions & self();

    ///\brief Adds runtime extension to be managed by this registry
    ///
    ///\throws `DuplicateExtensionName` if extension name is not unique
    ///\note Delegates ownershit to `Extensions`; assumed to be allocated
    ///      with `new`.
    bool add(const std::string &, iRuntimeExtension *);
    /// Returns ref to named `iRuntimeExtension` subclass instance previously
    /// added with `add()`
    iRuntimeExtension & operator[](const std::string &);

    /// Sets an extension's parameter
    void set_parameter(const std::string &, const std::string &);
    /// Forwars execution to `iExtension::init()`
    void init( calib::Manager &
             , iEvProcInfo * epi
             );
    /// Called after application done with data processing
    void finalize();
    /// Deletes all instantiated extensions
    void shutdown();
};  // class Extensions

namespace errors {

class NoExtension : public GenericRuntimeError {
public:
    const std::string extName;

    NoExtension( const std::string & extName_ )
            : GenericRuntimeError( util::format("Extension \"%s\" is not loaded."
                        , extName_.c_str() ).c_str() )
            , extName(extName_) {}
};

class DuplicateExtensionName : public GenericRuntimeError {
public:
    const std::string extName;

    DuplicateExtensionName( const std::string & extName_ )
            : GenericRuntimeError( util::format("Extension name \"%s\" is"
                        " already taken (repeated instantiation?)."
                        , extName_.c_str() ).c_str() )
            , extName(extName_) {}
};

}  // namespace ::na64dp::errors
}  // namespace na64dp

/**\brief Registers new runtime extension
 * \ingroup vctr-defs
 *
 * Registers subclasses of `iRuntimeExtension` managed with `Extensions`.
 *
 * \param clsName The name of the extension instance
 */
# define REGISTER_EXTENSION( clsName )  \
static bool _regResult_ ## clsName =    \
    ::na64dp::Extensions::self().add( # clsName, new clsName );

