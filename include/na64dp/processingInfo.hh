/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

# pragma once

/**\file
 * \brief Data processing statistics monitor
 *
 * This header file defines interface to track event processing statistics for
 * the pipeline. These entities get updated in the realtime and foresee
 * periodic/realtime dispatch for online monitors, real-time logging, etc.
 *
 * Header also provides simple implementation of the console output writing
 * number of events being processed -- `TTYStatusProcessingInfo`.
 * */

#include "na64sw-config.h"
#include "na64util/mem/fwd.hh"

#include <yaml-cpp/yaml.h>

# include <map>
# include <vector>
# include <thread>
# include <mutex>

namespace na64dp {

class AbstractHandler;
class AbstractEventSource;
class Pipeline;
class HitHandlerStats;

// TODO: use high resolution clock with NA64SW_HANDLERS_PROFILING maxro

namespace aux {
/**\brief Statistical entry struct, follows basic statistics on the running
 *        handler at the pipeline.
 * 
 *  `iEvProcInfo` maintains set of such entries during processing to gather
 *  statistics on event and hits processed and discriminated. Also tracks
 *  time profile, that can be used for rough benchmarks and pipeline
 *  optimization. */
struct HandlerStatisticsEntry {
    /// Name of the handler.
    std::string name;
    /// Counter corresponding to discriminated events on certain handler in
    /// pipeline
    size_t nDiscriminated;
    // For hit handler -- number of hits being considered
    //size_t nHitsConsidered;
    // For hit handler -- number of hits being discriminated
    //size_t nHitsDiscriminated;
    /// Time elapsed on computation.
    clock_t elapsed
          , _started;

    /// Used for reverse lookup in summary printout; always set
    const AbstractHandler * handlerPtr;
    /// Used to retrieve per-hit metrics, can be null for event level handlers
    const HitHandlerStats * hitHandlerPtr;


    HandlerStatisticsEntry(const std::string & nm) 
            : name(nm)
            , nDiscriminated(0)
            , elapsed(0)
            , _started(0)
            , handlerPtr(nullptr)
            , hitHandlerPtr(nullptr)
        {}
};

}  // namespace ::na64dp::aux

namespace util {
namespace PoD {

#if 0
///\brief Interface of data processing manager
///
/// Interface defines a event processor steering contract:
///
///  * On pipeline startup 
///
/// Default implementation of methods always provides positive control
/// response.
struct iProcManager {
    enum ExecStatus {
        shutdown,
        processEvents,
    };

    /// Returns `true` if instance shall manage pipeline processing
    virtual bool manages_processing() const { return false; }

    /// Called after logging and calibrations initialized, before instantiating
    /// source and processing pipeline
    virtual ExecStatus startup( std::vector<std::string> & //inputs
                              , YAML::Node & //srcCfg
                              , YAML::Node & //runCfg
                              )
        { return processEvents; }
    /// Called after instantiating source, before instantiating pipeline
    virtual ExecStatus source_constructed( na64dp::AbstractEventSource *
                                         , const std::vector<std::string> &
                                         , const YAML::Node & //srcCfg
                                         , YAML::Node & //runCfg
                                         )
        { return processEvents; }
};
#endif

}  // namespace ::na64dp::util::PoD
}  // namespace ::na64dp::util

/**\brief Processing info base class
 *
 * Interface for processing info aggregation endpoint. */
struct iEvProcInfo {
    /**\brief Called at the beginning of proc info lifecycle
     *
     * Used to initialize counters, structures, etc. */
    virtual void start_event_processing() {}
    /**\brief Creates new statistical entry for the handler
     *
     * Might be called at any time to indicate new handler created in the
     * pipeline */
    virtual void register_handler( const AbstractHandler *, const std::string & ) = 0;
    /**\brief Called by pipeline at start of event processing */
    virtual void notify_event_read() = 0;
    /**\brief Called by pipeline when event was discriminated by certain handler */
    virtual void notify_event_discriminated( const AbstractHandler * hPtr ) = 0;
    /**\brief Called when event is provided to a certain handler for
     * processing */
    virtual void notify_handler_starts( const AbstractHandler * hPtr ) = 0;
    /**\brief Called by pipeline when certain handler done processing current
     * event. */
    virtual void notify_handler_done( const AbstractHandler * hPtr ) = 0;
    /** Called at the end of proc info lifecycle */
    virtual void finalize() {}

    virtual ~iEvProcInfo() {}
};


/** \brief Event processing info registry
 *
 * An interim utility abstract class, implements basic interface to track
 * individual handlers statistics. Maintains index of
 * `aux::HandlerStatisticsEntry` by handler pointer, provides basic interface
 * for querying.
 *
 * \note descendants usually protect access to registry members with mutex, so
 *       by default most of the getters are qualified as `protected'. */
struct iEvStatProcInfo : public iEvProcInfo
                       //, public util::PoD::iProcManager,
                       , protected std::map< const AbstractHandler *
                                       , aux::HandlerStatisticsEntry> {
private:
    size_t _nEvsProcessed  ///< number of processed events, overall
         , _nEvsToProcess  ///< number of discriminated events, overall
         ;
    clock_t _started;  ///< processing started at
    clock_t _ended;  ///< processing ended at (can be 0)
    /// ordered list of handlers stats
    std::vector<const aux::HandlerStatisticsEntry *> _ordered;
protected:
    /// Returns statistics entry for handler
    aux::HandlerStatisticsEntry & stats_for( const AbstractHandler * );

    iEvStatProcInfo( size_t nEvsToProcess ) : _nEvsProcessed(0)
                                            , _nEvsToProcess(nEvsToProcess)
                                            , _started(0), _ended(0)
                                            {}

    ///\brief Creates new handler entry in the registry
    ///
    /// Modifies map of handlers (creates new item) and ordered list.
    ///
    /// \todo sub-pipeline
    void register_handler( const AbstractHandler *, const std::string & ) override;
    ///\brief Increments overall events counter, starts overall clock if not started
    void notify_event_read() override;
    ///\brief Increments handler's discriminated events counter
    void notify_event_discriminated( const AbstractHandler * hPtr ) override;
    ///\brief Affects handler's clock (start)
    void notify_handler_starts( const AbstractHandler * hPtr ) override;
    ///\brief Increments handler's elapsed time clock
    void notify_handler_done( const AbstractHandler * hPtr ) override;
public:
    virtual ~iEvStatProcInfo() {}
    /// Returns number of events being processed
    size_t n_events_processed() const { return _nEvsProcessed; }
    /// Returns number of events being discriminated within the pipeline
    size_t n_events_discriminated() const;
    /// Returns number of events expected to be read
    size_t n_events_to_process() const { return _nEvsToProcess; }
    /// Returns clock() value when processing started
    clock_t started() const { return _started; }
    /// Returns elapsed time (in ticks)
    ///
    /// Depending on whether `finalize()` was called returns time difference
    /// between current or finalized time
    clock_t elapsed() const;
    /// Returns ordered list of processor stats
    const std::vector<const aux::HandlerStatisticsEntry *> & ordered_stats() {
        return _ordered; }

    virtual void finalize() override;

    ///\brief Prints event processing summary as an ASCII table
    ///
    /// ...
    /// \todo
    virtual void print_summary(std::ostream &);

    friend class Pipeline;
};

/**\brief Wrapper for parallel dispatching of events processing info.
 *
 * Tracks the event processing speed, guarantees reasonable refresh rate while
 * dispatching some runtime information: events being read and processed,
 * average events processing rate, events discrimination statistics, etc.
 *
 * The rationale is to prevent significant slowdown appearing on frequent I/O
 * operations (FIFO, TTY, network connections, etc) by yielding of a thread
 * dedicated to these operations. Though, synchronization via mutex may still
 * introduce noticeable impact. */
class EvProcInfoDispatcher : public iEvStatProcInfo {
private:
    /// Refresh interval, msec
    const unsigned int _rrMSec;
    /// A thread for periodical print-out of the info
    std::thread * _dispatcherThread;
    /// True, when periodical print-out shall proceed
    bool _keep;
    /// Info synchronization mutex
    std::mutex _m;
    /// In-thread runner
    void _dispatch_f();
protected:
    /// A record storing the starting time of the processing procedure
    clock_t _processingStartTime;  // XXX?

    ///\brief Shall actually dispatche/update the target
    ///
    /// During this function execution the statistics kept by the instance is
    /// frozen and won't change.
    virtual void _update_event_processing_info() = 0;
    /// Initializes basic state.
    EvProcInfoDispatcher( size_t nMaxEvs
                        , unsigned int mSecRefreshInterval
                        );
public:
    virtual ~EvProcInfoDispatcher();
    /// Issues a monitoring thread
    void start_event_processing() override;
    /// Stops monitoring thread
    void finalize() override;

    /// Guarded version: increases "passed events" counter
    void notify_event_read() override;
    /// Guarded version: increases "discriminated events" counters
    void notify_event_discriminated( const AbstractHandler * hPtr ) override;
    /// Guarded version: handler has been invoked
    void notify_handler_starts( const AbstractHandler * hPtr ) override;
    /// Guarded version: handler has finished
    void notify_handler_done( const AbstractHandler * hPtr ) override;
};

/**\brief Prints handlers I/O stats as a simple unbuffered ASCII display.
 *
 * A simplest possible case of TTY display, that in case of
 * interactive terminal provides basic print-out of number of events being
 * processed till this moment. */
class TTYStatusProcessingInfo : public EvProcInfoDispatcher {
protected:
    const std::string _format;
    virtual void _update_event_processing_info() override;
public:
    /// Accepts a C-file descriptor and refresh interval in msec.
    TTYStatusProcessingInfo( const std::string & format
                           , size_t nMaxEvents=0
                           , int mSecRefreshInterval=200 )
            : EvProcInfoDispatcher( nMaxEvents, mSecRefreshInterval )
            , _format(format)
            {}
};

}  // namespace na64dp

