/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/YAMLAssertions.hh"
#include "na64util/str-fmt.hh"
#include "na64util/vctr.hh"
#include "na64util/mem/fwd.hh"
#include "na64event/reset-values.hh"

#include "log4cpp/Category.hh"

#include <map>
#include <yaml-cpp/yaml.h>

namespace na64dp {

namespace calib { class Manager; }  // fwd
namespace event { struct Event; }  // fwd

///\defgroup handlers Data processing handlers
///\defgroup generic-handlers Handlers parameterised with type or attribute

/**\brief An abstract handler class
 *
 * Defines an interface for all the handlers implemented in the pipeline.
 *
 * \todo document local memory behaviour (invokation code must guarantee (re-)set).
 */
class AbstractHandler {
public:
    ///\brief Type of the numerical code returned by handler adter processing done.
    enum ProcRes : uint8_t {
        /// This is the default return code meaning that event should be
        /// propagated further by the pipeline.
        kOk = 0x0,
        /// This flag in returning result stops the caller code to
        /// propagate the event to next handlers in pipeline. After that the
        /// caller code may continue with next event from source.
        kDiscriminateEvent = 0x2,
        /// This flag must be considered by the caller code as a terminative
        /// signal -- no more event must be processed by the caller code.
        /// Together with `kDiscriminateEvent' flag it stops the further
        /// propagation of current event as well
        kStopProcessing = 0x4,
        /// A shortcut for instant stop of processing.
        kAbortProcessing = kDiscriminateEvent | kStopProcessing,
    };
private:
    /// Local memory pointer; used by handlers that may allocate new event data
    /// properties.
    LocalMemory * _lmemPtr;
protected:
    /// Handlers logger category
    log4cpp::Category & _log;

    /// Returns local memory reference during the event processing
    LocalMemory & lmem() { assert(_lmemPtr); return *_lmemPtr; }
public:
    /// Default ctr, only initializes logger for "handlers" category.
    AbstractHandler();
    /// Default ctr, only initializes logger.
    AbstractHandler(log4cpp::Category &);
    /// Abstract method processing the event. Doing most of the handler's work.
    /// Returning value of type `ProcRes` signaling the pipeline how to
    /// proceed with the event.
    virtual ProcRes process_event(event::Event & event) = 0;
    /// Method performing some operations after all the
    /// events were processed and before the resources will be freed (e.g.
    /// before output ROOT file is closed).
    virtual void finalize();
    /// Dtr made virtual to provide generic deletion of handler subclass
    /// instances, does nothing.
    virtual ~AbstractHandler() {}
    /// Returns current logger instance affiliated to handlers objects.
    log4cpp::Category & log() { return _log; }

    /// Sets the local memory reference prior to the invokation to the
    /// processing.
    /// Must be called by invokation code explicitly, typically prior to `try`.
    void set_local_memory( LocalMemory & );
    /// Reets the local memory reference after invokation to the processing.
    /// Must be called by invokation code explicitly, typically within the
    /// `catch` block.
    void reset_local_memory();
};

///\brief Virtual constructor traits for `AbstractHandler` class
///
/// Provides constructor callback type that requires calibration data
/// dispatcher and configuration node to be used to construct the handler.
template<> struct CtrTraits<AbstractHandler> {
    typedef AbstractHandler * (*Constructor)
        ( calib::Manager &, const YAML::Node & );
};

namespace util {

/// Returns getter instance by name
template<typename T> typename event::Traits<T>::Getter
value_getter( const std::string & nm ) {
    auto it = event::Traits<T>::getters.find(nm);
    if( event::Traits<T>::getters.end() == it ) {
        NA64DP_RUNTIME_ERROR( "No getter named \"%s\".", nm.c_str() );
    }
    return it->second.second;
}

}  // namespace na64dp::util

namespace aux {

///\brief Gets logging category from handler-config
///
/// Standard way to obtain logging category from handler configuration. First,
/// looks for parameter `_log` in given config node and returns its value if
/// given. Otherwise, looks for `_label` and retrieves `handlers.` + label
/// category. If failed, a "handlers" category will be returned.
log4cpp::Category & get_logging_cat(const YAML::Node &);

}  // namespace aux
}  // namespace na64dp

/**\brief Registers new event handler type for pipeline data processing
 * \ingroup vctr-defs
 *
 * Registers subclasses of `AbstractHandler` base to be created with `VCtr`.
 *
 * \param clsName The name of the handler type
 * \param calibDsp The name of calibations manager instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param desc An event handler type description string
 */
# define REGISTER_HANDLER( clsName, calibDsp, ni, desc )                        \
static na64dp::AbstractHandler * _new_ ## clsName ## _instance( na64dp::calib::Manager &  \
                                                      , const YAML::Node & );    \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<na64dp::AbstractHandler>( # clsName, _new_ ## clsName ## _instance, desc ); \
static na64dp::AbstractHandler * _new_ ## clsName ## _instance( __attribute__((unused)) na64dp::calib::Manager & calibDsp  \
                                                      , __attribute__((unused)) const YAML::Node & ni )

