/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"
#include "na64util/selector.hh"
#include "na64detID/TBName.hh"
#include "na64event/data/event.hh"
#include "na64util/str-fmt.hh"

#include <algorithm>

namespace na64dp {

///\defgroup sadc-handlers Data processing handlers related to (M)SADC hits
///\defgroup apv-handlers Data processing handlers related to APV hits
///\defgroup f1-handlers Data processing handlers related to F1-based detector hits
///\defgroup apv-cluster-handlers Data processing handlers related to APV clusters
///\defgroup calo-handlers Data processing handlers related to reconstructed calorimeter hits
///\defgroup track-score-handlers Data processing handlers related to reconstructed track score
///\defgroup tracking-handlers Data processing handlers related to reconstructed track
///\defgroup mc-handlers Data manipulation/processing handlers facilitating simulation routines

/// Small non-template shim providing hit handler statistics for monitoring
class HitHandlerStats : public AbstractHandler {
protected:
    /// Number of considered hits
    size_t _nHitsConsidered;
    /// Number of discriminated hits
    size_t _nHitsDiscriminated;
public:
    HitHandlerStats(log4cpp::Category & logCat)
                      : AbstractHandler(logCat)
                      , _nHitsConsidered(0)
                      , _nHitsDiscriminated(0)
                      {}
    /// Returns number of hits being considered by this handler
    size_t n_hits_considered() const { return _nHitsConsidered; }
    /// Returns number of hits being discriminated by this handler
    size_t n_hits_discriminated() const { return _nHitsDiscriminated; }
};

/**\brief Generic mixing (stub) for hits that do not support selections.
 *
 * Methods here are just stubs for generic code that usually re-implemented
 * by SFINAE-based specializations. The stubs are used by `HitHandler` template
 * in its generic code.
 * */
template<typename HitT, typename=void>
class SelectiveHitHandler {
public:
    SelectiveHitHandler( calib::Dispatcher & cdsp
                       , const std::string & sel
                       , log4cpp::Category & logCat
                       ) {}
    typedef typename event::Association<event::Event, HitT>::Collection::key_type HitKey;
    /// Default stub for hit handler which index doesn't support selection,
    /// returns `false` (handler is NOT selective).
    constexpr bool is_selective() const { return false; }
    /// Default stub for hit handler which index doesn't support selection
    /// always returns `true` (all IDs match).
    constexpr bool matches( const HitKey ) const { return true; }
};

class SelectiveHandlerMixin
        : protected calib::Handle<nameutils::DetectorNaming> {
public:
    typedef std::pair<std::string, DetSelect *> Selector;
private:
    /// Selection limits detector IDs this handler is applicable to.
    Selector _selector;
public:
    SelectiveHandlerMixin( calib::Dispatcher & dspt
                         , const std::string & selExpr
                         , log4cpp::Category & logCat
                         , const std::string & detNamingSubclass="default"
                         )
                : calib::Handle<nameutils::DetectorNaming>(detNamingSubclass, dspt)
                , _selector( selExpr, nullptr ) {}
    ~SelectiveHandlerMixin() {
        if( _selector.second ) {
            delete _selector.second;
        }
    }

    ///\brief Performs basic conversion of the names set to detector IDs set.
    ///
    /// At the level of AbstractHandler<> template, performs conversion of
    /// string set _selector to set _onlyDetectors of numerical IDs.
    /// Subclasses may call this parent function to avoid duplication.
    virtual void handle_update( const nameutils::DetectorNaming & nm ) override {
        calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
        if( _selector.first.empty() ) return;
        if( ! _selector.second ) {
            _selector.second = new DetSelect( _selector.first.c_str()
                                            , util::gDetIDGetters
                                            , nm );
        } else {
            _selector.second->reset_context( nm );
        }
    }
    /// Returns current naming instance
    const nameutils::DetectorNaming & naming() const {
        return static_cast<const calib::Handle<nameutils::DetectorNaming>*>(this)->operator*();
    }
    /// Returns true if detector ID matches selection expression
    bool matches( DetID did ) const {
        if( _selector.first.empty() ) return true;
        if( !_selector.second ) {  // handle_update() wasn't called yet
            NA64DP_RUNTIME_ERROR("Detector selector was not compiled (is there"
                    " forgotten Parent::handle_update() call?)");
        }
        return _selector.second->matches(did);
    }
    /// Returns true if selection expression is set.
    bool is_selective() const { return !_selector.first.empty(); }
    /// Returns selector expression that this handler is set to process. Empty
    /// for all hits of certain type.
    ///\todo rename to detector_selection_str()
    const Selector & detector_selection() const { return _selector; }
};  // class SelectiveHandlerMixin

/**\brief Helper class implementing frequent functions on name slections
 *
 * This class is designed for handlers that depends on "current" instance of
 * `util::TBNameMappings` calibration information entry, thus reducing
 * genericity of handler to single dispatcher instance.
 *
 * This specialization is enabled for `DetID` and all its superclasses.
 * */
template<typename HitT>
class SelectiveHitHandler<HitT, typename std::enable_if<
                std::is_constructible< typename event::Association<event::Event, HitT>::Collection::key_type
                                     , DetID >::value
                ||
                std::is_convertible< typename event::Association<event::Event, HitT>::Collection::key_type
                                   , DetID
                               >::value
            >::type>
    : protected SelectiveHandlerMixin {
public:
    typedef typename event::Association<event::Event, HitT>::Collection::key_type HitKey;
public:
    /// Initializes hit selection mixing
    SelectiveHitHandler( calib::Dispatcher & dspt
                       , const std::string & selExpr
                       , log4cpp::Category & logCat
                       , const std::string & detNamingSubclass="default"
                       )
                : SelectiveHandlerMixin(dspt, selExpr, logCat, detNamingSubclass) {}
};

///\todo rename to `iHitHandler`
template<typename HitT>
class AbstractHitHandler : public HitHandlerStats
                         , public SelectiveHitHandler<HitT> {
public:
    using typename SelectiveHitHandler<HitT>::HitKey;
private:
    /// process_hit() may set this to the resulting code of process_event()
    ProcRes _pr;
    /// process_hit() may set this to remove current entry from map
    bool _removeFieldEntry;
    /// process_hit() may retrieve current event reference
    event::Event * _cEvPtr;
protected:
    /// A reference to hit currently being processed.
    typename event::Association<event::Event, HitT>::Collection::iterator _cHitIt;
    /// `process_hit()` may call this to retrieve reference to the current event
    event::Event & _current_event() { assert( _cEvPtr ); return *_cEvPtr; }
    /// Const version of `_current_event()`
    const event::Event & _current_event() const { assert( _cEvPtr ); return *_cEvPtr; }
    /// process_hit() may call this to retrieve reference to the current hit
    auto _current_hit_iterator() { return _cHitIt; }
    /// process_hit() should call this to set the return value of the
    /// process_event()
    virtual void _set_event_processing_result( ProcRes r ) { _pr = r; }
    /// process_hit() should call this to erase current hit entry from
    /// processing
    virtual void _set_hit_erase_flag() { _removeFieldEntry = true; }
public:
    AbstractHitHandler( calib::Dispatcher & dsp
                      , log4cpp::Category & logCat
                      ) : HitHandlerStats(logCat)
                        , SelectiveHitHandler<HitT>(dsp, logCat, "")
                        , _cEvPtr(nullptr) {}
    /// Initializes discriminative hit handler (handles only certain detector
    /// IDs).
    AbstractHitHandler( calib::Dispatcher & dsp
                      , const std::string & sel
                      , log4cpp::Category & logCat
                      , const std::string & namingSubclass="default"
                      ) : HitHandlerStats(logCat)
                        , SelectiveHitHandler<HitT>(dsp, sel, logCat, namingSubclass)
                        , _cEvPtr(nullptr) {}

    ///\brief Shortcut constructor
    ///
    /// This constructor sets logging category for handler to be "handlers" and
    /// detector naming assumed to be "default". Although it most often usecase,
    /// wide usage of this shortcut has to be generally discouraged.
    AbstractHitHandler( calib::Dispatcher & dsp
                      , const std::string & selExpr
                      )
                : HitHandlerStats(log4cpp::Category::getInstance("handlers"))
                ,  SelectiveHitHandler<HitT>(dsp, selExpr, log4cpp::Category::getInstance("handlers"), "default")
                {}

    /// Performs iteration over hits of certain type within an event and
    /// delegates particular treatment to process_hit(). Will return true
    /// if all the process_hit() invocations returned true AND there were at
    /// least one process_hit() invocation.
    AbstractHandler::ProcRes process_event( event::Event & e ) override;

    /// Shall perform treatment of particular hit and return `false' to abrupt
    /// an iteration over event hits.
    virtual bool process_hit( EventID eventID
                            , HitKey
                            , HitT & hit ) = 0;

    friend class EventSteeringObject;
};

template<typename HitT> AbstractHandler::ProcRes
AbstractHitHandler<HitT>::process_event( event::Event & e ) {
    typedef event::Association<event::Event, HitT> Association;
    _pr = kOk;
    bool doEFProc = false;
    _cEvPtr = &e;  // set current event ptr
    std::vector< typename Association::Collection::iterator > hitsToRemove;
    auto & hm = Association::map(e);
    if( !SelectiveHitHandler<HitT>::is_selective() ) {  // selection is not enabled, just iterate over entries
        for( auto it = hm.begin(); hm.end() != it; ++it ) {
            ++_nHitsConsidered;
            auto hitEntry = *it;
            _removeFieldEntry = false;
            auto ref = (_cHitIt = it)->second;
            assert(ref);  // otherwise ref set to nullptr, check creating routines
            doEFProc = process_hit( e.id, hitEntry.first, *ref );
            if( _removeFieldEntry ) {
                hitsToRemove.push_back( it );
            }
            if( ! doEFProc ) break;
        }
    } else {  // selection is enabled, apply selection
        for( auto it = hm.begin(); hm.end() != it; ++it ) {
            if( ! SelectiveHitHandler<HitT>::matches(DetID(it->first)) ) continue;  // selection doesn't match
            ++_nHitsConsidered;
            _removeFieldEntry = false;
            auto ref = (_cHitIt = it)->second;
            assert(ref);  // otherwise ref set to nullptr, check creating routines
            doEFProc = process_hit( e.id, it->first, *ref );
            if( _removeFieldEntry ) {
                hitsToRemove.push_back( it );
            }
            if( ! doEFProc ) break;
        }
    }
    _cHitIt = hm.end();
    for( auto it : hitsToRemove ) {
        Association::remove( Association::map(e), it );
    }
    _nHitsDiscriminated += hitsToRemove.size();
    _cEvPtr = nullptr;  // unset current event ptr
    return _pr;
}

//
// Specialization for track iterating handlers
template<>
class AbstractHitHandler<event::Track> : public HitHandlerStats {
private:
    /// Track is accounted if any of this patterns matches this set
    std::unordered_set<int> _permittedZonePatterns;
    /// Track is accounted if bitwise-AND of this value and track zone pattern
    /// is non-zero
    int _permittedZones;
    /// If true, alterate result of track zone pattern matching
    bool _invertZonePatternMatching;

    /// process_hit() may set this to the resulting code of process_event()
    ProcRes _pr;
    /// process_hit() may set this to remove current entry from map
    bool _removeFieldEntry;
    /// process_hit() may retrieve current event reference
    event::Event * _cEvPtr;
    /// A reference to hit currently being processed.
    mem::Ref<event::Track> _cTrack;
protected:
    /// Pointer to track entry currently being processed
    typename event::Association<event::Event, event::Track>::Collection::iterator _cTrackIt;
    /// process_hit() may call this to retrieve reference to the current event
    event::Event & _current_event() { assert( _cEvPtr ); return *_cEvPtr; }
    /// Const version of `_current_event()`
    const event::Event & _current_event() const { assert( _cEvPtr ); return *_cEvPtr; }
    /// process_hit() may call this to retrieve reference to the current hit
    mem::Ref<event::Track> _current_hit_ref() { return _cTrack; }
    /// process_hit() should call this to set the return value of the
    /// process_event()
    virtual void _set_event_processing_result( ProcRes r ) { _pr = r; }
    /// process_hit() should call this to erase current hit entry from
    /// processing
    virtual void _set_hit_erase_flag() { _removeFieldEntry = true; }
public:
    AbstractHitHandler( const std::unordered_set<int> permittedPatterns
                      , int permittedZones
                      , bool invert
                      , log4cpp::Category & logCat
                      ) : HitHandlerStats(logCat)
                        , _permittedZonePatterns(permittedPatterns)
                        , _permittedZones(permittedZones)
                        , _invertZonePatternMatching(invert)
                        {}
    AbstractHitHandler( calib::Dispatcher &
                      , const std::string &
                      , log4cpp::Category & logCat
                      , const std::string & namingClass="default"
                      );
    /// Virtual method to be implemented by subclasses
    virtual bool process_hit(EventID, TrackID, event::Track &) = 0;

    /// Returns true if given zone pattern matches current selection
    virtual bool zone_pattern_matches(int) const;
    /// Invokes `process_hit()` for tracks matching zone pattern
    ProcRes process_event(event::Event &) override;
};  // class AbstractHitHandler<event::Track>

namespace aux {  // TODO: util

/// Parses the "applyTo" list of strings if given within the node
std::string retrieve_det_selection( const YAML::Node & );

///\brief Looks for "detectorNamingClass" parameter, returns "default" if not
/// found
std::string get_naming_class(const YAML::Node &);

///\brief Looks for "placementsNamingClass" parameter, returns "default" if not
/// found
std::string get_placements_class(const YAML::Node &);

///\brief Gets the hit type part from getter strings
///
/// This function is frequently used to determine the C++ type of the classes
/// to be returned in VCtr. It is pretty simple, though.
///
/// \todo Correctly infer "event" when ((todo: hits x-macro)) implemented
std::string get_val_entry_type(const std::string &);

}  // namespace ::na64dp::aux

}  // namespace na64dp

