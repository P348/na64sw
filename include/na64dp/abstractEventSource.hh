/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64dp/processingInfo.hh"
#include "na64calib/manager.hh"
#include "na64util/YAMLAssertions.hh"
#include "na64util/vctr.hh"
#include "na64util/mem/fwd.hh"
#include "na64detID/TBName.hh"

#include <map>
#include <yaml-cpp/yaml.h>

namespace na64dp {

namespace errors {
class UnsupportedDataSourceFeature : public GenericRuntimeError {
public:
    UnsupportedDataSourceFeature(const char * reason) throw()
        : GenericRuntimeError(reason) {}
};
}  // namespace errors

namespace event {
struct Event;  // fwd
}  // namespace event

///\brief An interface for event source entities (files, sockets, etc).
class AbstractEventSource {
public:
    typedef uint8_t  Features_t;
    static constexpr Features_t kBackwardReading = 0x1;
    static constexpr Features_t kRandomAccess = 0x2;
    static constexpr Features_t kProvidesEventsList = 0x4;
protected:
    /// Sources logger category
    log4cpp::Category & _log;
    /// Online event processing stats monitor handle
    iEvProcInfo * _epi;
public:
    /// Returns features flags; default is `forward reading only`
    virtual Features_t features() const { return 0x0; }

    /// Default ctr, optionally can bind source to `iEvProcInfo` instance
    AbstractEventSource( iEvProcInfo * epi=nullptr );

    /// Recommended ctr, with explicitly specified logging category, optionally
    /// can bind source to `iEvProcInfo` instance
    AbstractEventSource( iEvProcInfo * epi, log4cpp::Category & logCat );

    ///\brief Read next event
    ///
    /// Must be implemented for any particular data source type.
    /// Shall read event from file, socket, whatever into reentrant buffer.
    /// Shall not clear event, but rather rely on event has been already
    /// re-set. Must return whether the event iteration shall be proceed.
    virtual bool read( event::Event &, event::LocalMemory & lmem ) = 0;

    ///\brief Reads "previous" event
    ///
    /// Shall read "previous" event from or data source of any kind.
    /// Default implementation throws `errors::UnsupportedDataSourceFeature`.
    /// Subclasses implementing this method must set `kBackwardReading` flag
    /// in `features()`.
    virtual bool read_prev( event::Event &, event::LocalMemory & lmem );

    ///\brief Reads event specified by ID
    ///
    /// Shall read specified event from or data source of any kind.
    /// Default implementation throws `UnsupportedDataSourceFeature`.
    /// Subclasses implementing this method must set `kRandomAccess` flag
    /// in `features()`.
    virtual bool read_by_id( EventID, event::Event &, event::LocalMemory & lmem );

    ///\brief Should fill event IDs with given pagination settings
    ///
    /// Shall return list of events IDs available in source respecting given
    /// pagination settings.
    /// Default implementation throws `UnsupportedDataSourceFeature`.
    /// Subclasses implementing this method must set `kProvidesEventsList` flag
    /// in `features()`.
    virtual size_t list_events( std::vector<EventID> &, size_t nPage, size_t nPerPage );

    ///\brief Should append node with type-specific information
    ///
    /// This method is sometimes used by various monitoring utils that may
    /// or may not assume specific type. Second argument is used to specify
    /// various options (e.g. a querystring). Default implementation does
    /// nothing.
    virtual void append_state_info(YAML::Node &,
            const std::unordered_map<std::string, std::string> &) {}

    /// This method is called by pipeline code when the source is processed.
    /// Used to clean up things after reading is done, print reports, etc.
    virtual void finalize() {}
    virtual ~AbstractEventSource() {}
    /// Returns logger instance affiliated to sources objects.
    log4cpp::Category & log() { return _log; }
};

template<> struct CtrTraits<AbstractEventSource> {
    typedef AbstractEventSource * (*Constructor)
        (calib::Manager &, const YAML::Node &, const std::vector<std::string> & );
};

///\brief Common helper class for NA64 cachinh chip and detector codes
class AbstractNameCachedEventSource : public AbstractEventSource
                                    , protected calib::Handle<nameutils::DetectorNaming>
                                    {
public:
    /// Cache instance keeping numerical codes for chips and detector types
    struct NameCache {
        DetChip_t kSADC, kAPV, kStwTDC, kF1;
        DetKin_t kECAL, kHCAL, kWCAL, kZDCAL, kSRD, kVETO, kS, kVTEC, kVTWC
               , kWCAT, kVHCAL, kDM, kV
               ;
        DetKin_t kMM, kGEM;
        DetKin_t kSt, kStt;
        DetKin_t kBMS;
        // ... others, used by ChipTraits
    };
private:
    /// Internal detector naming cache
    NameCache _detIDsCache;
protected:
    /// Reference to calibration manager, used to handle new event number
    calib::Manager & _mgr;
protected:
    /// Updates `_detIDsCache` name cache
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

    AbstractNameCachedEventSource( calib::Manager & mgr
                                 , log4cpp::Category & loggingCategory
                                 , const std::string & namingClass="default"
                                 , iEvProcInfo * epi=nullptr
                                 );
    virtual ~AbstractNameCachedEventSource() {}
    /// Returns reference to known calib manager instance
    calib::Manager & calib_manager() { return _mgr; }
    /// Returns reference to known calib manager instance (const)
    const calib::Manager & calib_manager() const { return _mgr; }
    /// Returns current detector naming instance
    const NameCache & naming() const;
};

}  // namespace na64dp

/**\brief Registers new event source type for pipeline data processing
 * \ingroup vctr-defs
 *
 * Registers subclasses of `AbstractEventSource` base to be created with `VCtr`.
 *
 * \param clsName The name of the handler type
 * \param calibMgr The name of calibations manager instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param ids The name for list of input source IDs to be used in ctr
 * \param desc A source type description string
 */
# define REGISTER_SOURCE( clsName, calibMgr, ni, ids, desc )                    \
static na64dp::AbstractEventSource * _new_ ## clsName ## _instance(             \
                                                            na64dp::calib::Manager & \
                                                          , const YAML::Node &  \
                                                          , const std::vector<std::string> & );  \
static bool _regResult_ ## clsName =                                            \
    ::na64dp::VCtr::self().register_class<::na64dp::AbstractEventSource>( # clsName, _new_ ## clsName ## _instance, desc ); \
                                                                                \
static na64dp::AbstractEventSource * _new_ ## clsName ## _instance(             \
                                                            na64dp::calib::Manager & calibMgr \
                                                          , const YAML::Node & ni \
                                                          , const std::vector<std::string> & ids )

