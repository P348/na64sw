#pragma once

#include "na64calib/placementsDefinedCache.hh"
#include "na64event/data/track.hh"

namespace na64dp {
namespace aux {
struct ExtendedTrackScoreGetterCacheEntry {
    util::Transformation t;
    calib::Placement placement;
    Float_t uNorm, vNorm, wNorm;

    ExtendedTrackScoreGetterCacheEntry(const calib::Placement & pl)
        : t(util::transformation_by(pl))
        , placement(pl)
        , uNorm(t.gU().norm()), vNorm(t.gV().norm()), wNorm(t.gW().norm())
        {}
};
}  // namespace aux

class ExtendedTrackScoreGetters
        : public calib::PlacementsDefinedCache<aux::ExtendedTrackScoreGetterCacheEntry> {
public:
    typedef event::StdFloat_t EventGetterValue_t;  // TODO: take it from event traits
    /// Pointer to plain score fit info's getter (does not need instance)
    typedef event::Traits<event::ScoreFitInfo>::Getter PlainScoreGetter;
    /// Pointer to stateful getter (one of the `ExtendedTrackScoreGetters` method)
    typedef EventGetterValue_t (ExtendedTrackScoreGetters::*MethodGetter) (
                DetID, const event::ScoreFitInfo &
            ) const;
    /// Unified getter reference
    struct Getter {
        /// True if `cllb.plainGetter` should be used
        bool isPlain;
        /// Union of the getter addrs
        union {PlainScoreGetter plainGetter; MethodGetter methodGetter;} cllb;

        /// Forwards call to appropriate getter
        Float_t operator() ( DetID did
                           , const event::ScoreFitInfo & scoreFitInfo
                           , const ExtendedTrackScoreGetters & self
                           ) const {
            if(isPlain) return cllb.plainGetter(scoreFitInfo);
            return (self.*cllb.methodGetter)(did, scoreFitInfo);
        }
        operator bool() const { return cllb.methodGetter || cllb.plainGetter; }
    };
    ///\brief Returns supp. getter by name
    ///
    /// Not efficient comparing to map, enough performance for init. Returns
    /// object with all union's field being `NULL` if name is not found.
    static Getter get_extended_getter(const std::string &);
public:
    ExtendedTrackScoreGetters( calib::Dispatcher & cdsp
            , log4cpp::Category & logCat
            , const std::string & namingCalibClass="default"
            , const std::string & placementsCalibClass="default"
            ) : PlacementsDefinedCache(cdsp, logCat, namingCalibClass, placementsCalibClass)
              {}

    //
    // Stateful getters
    /// Returns metric estimation of uncertainty by 1st coordinate in DRS
    EventGetterValue_t metric_uncertainty_u( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric estimation of uncertainty by 2nd coordinate in DRS
    EventGetterValue_t metric_uncertainty_v( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric estimation of uncertainty by 3rd coordinate in DRS
    EventGetterValue_t metric_uncertainty_w( DetID, const event::ScoreFitInfo & ) const;

    /// Returns metric unbiased residual by 1st coordinate in DRS
    EventGetterValue_t metric_unbiased_err_u( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric unbiased residual by 2nd coordinate in DRS
    EventGetterValue_t metric_unbiased_err_v( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric unbiased residual by 3rd coordinate in DRS
    EventGetterValue_t metric_unbiased_err_w( DetID, const event::ScoreFitInfo & ) const;

    /// Returns metric biased residual by 1st coordinate in DRS
    EventGetterValue_t metric_biased_err_u( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric biased residual by 2nd coordinate in DRS
    EventGetterValue_t metric_biased_err_v( DetID, const event::ScoreFitInfo & ) const;
    /// Returns metric biased residual by 3rd coordinate in DRS
    EventGetterValue_t metric_biased_err_w( DetID, const event::ScoreFitInfo & ) const;

    /// X of detector's center
    EventGetterValue_t detector_cx( DetID, const event::ScoreFitInfo & ) const;
    /// Y of detector's center
    EventGetterValue_t detector_cy( DetID, const event::ScoreFitInfo & ) const;
    /// Z of detector's center
    EventGetterValue_t detector_cz( DetID, const event::ScoreFitInfo & ) const;

    /// Detector U-size
    EventGetterValue_t detector_su( DetID, const event::ScoreFitInfo & ) const;
    /// Detector V-size
    EventGetterValue_t detector_sv( DetID, const event::ScoreFitInfo & ) const;
    /// Detector W-size
    EventGetterValue_t detector_sw( DetID, const event::ScoreFitInfo & ) const;
};

}  // namespace na64dp

