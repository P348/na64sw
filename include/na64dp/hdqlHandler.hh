#pragma once

#include "na64sw-config.h"

#if defined(hdql_FOUND) && hdql_FOUND

#include "na64dp/abstractHandler.hh"

#include <log4cpp/Category.hh>

struct hdql_Context;  // fwd
struct hdql_Query;  // fwd
struct hdql_CollectionKey;  // fwd
struct hdql_Compound;  // fwd

namespace na64dp {

/**\brief Event handler based on HDQL expressions
 *
 * Process event object using HDQL expression.
 * */
class AbstractHDQLBasedHandler : public AbstractHandler {
public:
    static hdql_Compound * eventCompound;
private:
    hdql_Context * _ownContext;
    hdql_Query * _query;
    hdql_CollectionKey * _keys;
public:
    AbstractHDQLBasedHandler( const std::string & expression
                    , hdql_Context * rootContext
                    , bool keysNeeded
                    , log4cpp::Category & logCat
                    );
    ~AbstractHDQLBasedHandler();

    ProcRes process_event(event::Event &) override;
    void finalize() override;
};  // class AbstractHDQLBasedHandler

}  // namespace na64dp

#endif  // defined(hdql_FOUND) && hdql_FOUND

