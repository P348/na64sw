#pragma once

/**\file
 * \brief Defines Hit combination rule based on DSuL
 *
 * This template class introduces implementation of detector IDs selection
 * for hit conjugation (combination) rules to pre-select hits using DSuL
 * expressions. It is an utility class, not directly related to general data
 * processing (thus, a bit redundant header for `na64dp` lib).
 * */

#include "na64detID/conjugationRule.hh"
#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace util {

/**\brief Hit conjugation rule based on DSuL
 *
 * Implements hit-selection interface of `iPerStationHitCombinationRule` to
 * use DSuL-based detector-selection mechanism. Parameterised with (input) hit
 * type.
 *
 * \ingroup track-combining-hits
 * */
template<util::Arity_t NT, typename HitT>
class DSuLHitCombinationRule
        : public iPerStationHitCombinationRule<
                std::pair<bool, typename event::Association<event::Event, HitT>::Collection::iterator>, NT>
        , public SelectiveHitHandler<HitT>
        {
public:
    /// Type used to refer to a particular hit; since in most cases we operate
    /// with ambigious DetID-to-hit sets, we keep iterator here
    //typedef typename event::Association<event::Event, HitT>::Collection::iterator HitRef_t;
    typedef std::pair<bool, typename event::Association<event::Event, HitT>::Collection::iterator> HitRef_t;
protected:
    bool _permitEmptyCombinations;
protected:
    /// Checks hit ID with DSuL selector
    bool _check_hit_ref(HitRef_t p) const override
        { return SelectiveHitHandler<HitT>::matches(p.second->first); }
    /// Relies on simple DetID conversion
    StationKey _station_key_from_hit_ref(HitRef_t hr) const override
        { return StationKey(DetID(hr.second->first)); }

    virtual void _init_hits_collection(std::array<std::vector<HitRef_t>, NT> & a) override {
        if(!_permitEmptyCombinations) return;
        for(util::Arity_t i = 0; i < NT; ++i) {
            a[i].push_back(HitRef_t(false, {}));
        }
    }
public:
    DSuLHitCombinationRule( calib::Dispatcher & cdsp
            , const std::string & sel
            , log4cpp::Category & logCat
            , bool permitEmptyCombinations
            , typename iPerStationHitCombinationRule<HitRef_t, NT>::GeneratorCtr dftCtr
                    =iPerStationHitCombinationRule<HitRef_t, NT>::construct_basic_cartesian_generator
            , const std::string &detNamingSubclass="default"
            ) : iPerStationHitCombinationRule<HitRef_t, NT>(logCat, dftCtr)
              , SelectiveHitHandler<HitT>(cdsp, sel
                      , logCat
                      , detNamingSubclass)
              , _permitEmptyCombinations(permitEmptyCombinations)
              {}
};

}  // namespace ::na64dp::util
}  // namespace na64dp

