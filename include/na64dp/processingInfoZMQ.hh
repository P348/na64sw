/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64dp/processingInfo.hh"

#if defined(ZMQ_FOUND) && ZMQ_FOUND

#include <zmq.hpp>

namespace na64dp {

/**\brief Event processing info implementation for ZMQ-based display
 *
 * This implementation is not of much use and marked for removal.
 *
 * \todo Deprecated.
 * */
class ZMQPubEventProcessingInfo : public EvProcInfoDispatcher {
private:
    zmq::context_t _zCtx;
    zmq::socket_t _zPubSock;
    std::vector<char> _msgBuf;
protected:
    virtual void _update_event_processing_info() override;
public:
    ZMQPubEventProcessingInfo( int portNo
                            , size_t nMaxEventsm
                            , unsigned int refreshInterval=200
                            );
};

}  // namespace na64dp

# endif

