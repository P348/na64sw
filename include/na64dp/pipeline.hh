/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"

/**\namespace na64dp
 * \brief Main project's namespace
 *
 * NA64DP project (DP stands for "data processing") is written on C++ and all
 * the declarations (types, classes and variables) must be declared within
 * this namespace.
 * */
namespace na64dp {

class iEvProcInfo;  // FWD

/**\brief Ordered handlers array with utility methods
 *
 * For details explaining what is the behind of the pipeline idea, see
 * "Key concepts" section in "related pages" of the generated doc.
 *
 * This implementation of the pipeline concepts assumes that all handlers are
 * to be an objects sublassing `AbstractHandler` class. The `Pipeline` stores
 * their pointers in the array, subsequently calling ther `process_event()`
 * on each event instance provided within `process()` method.
 * */
class Pipeline : public std::vector<AbstractHandler*> {
protected:
    /// Reference to logger instance
    log4cpp::Category & _log;
    /// Pointer to the running monitoring handle
    iEvProcInfo * _procInfoPtr;
public:
    /// Constructs empty pipeline.
    Pipeline(iEvProcInfo * pi=nullptr);
    /// Recieves YAML object to be configured with and uses
    /// virtual constructors registry for `AbstractHandler` to assemble a
    /// handlers chain (the pipeline).
    /// Optionally a ptr to `EventProcessingInfo` instance may be used to gain
    /// useful runtime information.
    Pipeline( YAML::Node, calib::Manager &, iEvProcInfo * pi=nullptr );
    /// Deletes all the associated handlers.
    ~Pipeline();
    ///\brief Propagate the event instance through the pipeline, feeding it to each
    /// handler in chain unless one of the handlers will return
    /// `kDiscriminateEvent' code.
    ///
    /// \returns First bool is `true' if at least one of the
    ///          handlers has set the `kStopProcessing' flag, second is set
    ///          to `true` if event has been discriminated by one of the
    ///          handlers.
    std::pair<bool, bool> process(event::Event &, LocalMemory & lmem);
};

/**\brief A helper class constructing pipeline from config file 
 *
 * Initialized with list of runtime paths and name of the runtime
 * configuration, builds a pipeline.
 *
 * \todo Provide interface to override certain parameters
 * */
class RunCfg {
private:
    const std::string _paths;
    std::string _filename;
    YAML::Node _root;
protected:
    std::string _find_rcfg_file(const std::string &);
public:
    ///\brief initializes object with given set of paths
    RunCfg( const std::string & name
          , const std::string & paths="");

    ///\brief Constructs run config from given node
    ///
    /// Root node may be provided externally, but to resolve includes, `RunCfg`
    /// is still convenient.
    RunCfg( YAML::Node &, const std::string & paths="" );

    /// 
    //void add_definition( const std::string & strExpr );

    ///\brief Returns YAML node compatible with `Pipeline` ctr
    YAML::Node get();

    //TODO: override_parameter()...
};

}  // namespace na64dp

