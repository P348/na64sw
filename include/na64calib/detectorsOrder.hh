/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2023 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "setupGeoCache.hh"

namespace na64dp {
namespace calib {

///\brief Defines spatial ordering of the detectos
///
/// Can accept pointer to external selector instance to selectively
/// include/exclude certain placements (does not maintain its lifetime).
/// Items passed validation then become subject of sorting algorithm.
///
/// To cope with calib info update procedure order numbers will not be
/// calculated until order is requested first time
///
/// \todo Placements invalidation.
template<typename OrderedT>
class AbstractSpatialDetectorsOrder : public SetupGeometryCache {
protected:
    /// Ptrs to external selectors (can be empty)
    std::vector<const std::pair<std::string, DetSelect *> *> _selectorPtrs;
    /// List of permitted zones (empty means all)
    std::unordered_set<ZoneID_t> _permittedZones;
private:
    /// To-be sorted cache, filled by `handle_update()` for placements
    std::unordered_map<DetID, Placement> _placementsByID;
    /// Station ID to layer number mapping (CATSC uses orderly numbers)
    mutable OrderedT _ordered;
    /// Number of unique items (can differ from `_ordered` size)
    mutable size_t _nSortedGroups;
    /// Cache validation flag
    mutable bool _cacheValid;
protected:
    /// Recomputes layer numbering cache
    void _recache() const {
        _ordered = _sort_detectors(_placementsByID, _nSortedGroups);
        _cacheValid = true;
    }
    /// Subclasses shall define this method for particular sort
    virtual OrderedT _sort_detectors(const std::unordered_map<DetID, Placement> &, size_t &) const = 0;
public:
    AbstractSpatialDetectorsOrder( calib::Manager & cmgr
                                 , log4cpp::Category & logCat
                                 , const std::string & namingCalibClass="default"
                                 , const std::string & placementsCalibClass="default"
                  ) : SetupGeometryCache(cmgr, logCat, namingCalibClass, placementsCalibClass)
          , _cacheValid(false)
          {
        cmgr.subscribe<calib::Placements>(*this, placementsCalibClass);
    }
    /// Returns ptr to selector with which this order was ctrd
    const std::vector<const std::pair<std::string, DetSelect *> *> &
        det_selector() const { return _selectorPtrs; }
    /// Puts placement into internal container, invalidates the cache
    void handle_single_placement_update(const calib::Placement & pl) override {
        if(!(((int) calib::Placement::kDetectorsGeometry) & ((int) pl.suppInfoType))) return;
        _cacheValid = false;
        DetID did = naming()[pl.name];
        _placementsByID[did] = pl;
    }
    /// Re-caches if need and returns ordered detector entries
    const OrderedT & spatial_ordered_detectors() const {
        if(!_cacheValid) _recache();
        assert(_cacheValid);
        return _ordered;
    }
    size_t n_sorted_groups() const {
        if(!_cacheValid) _recache();
        assert(_cacheValid);
        return _nSortedGroups;
    }

    void additional_selection_criterion(const std::pair<std::string, DetSelect *> & sel) {
        _cacheValid = false;
        _selectorPtrs.push_back(&sel);
    }

    void set_zone_numbers(const std::vector<ZoneID_t> & zoneNums) {
        _cacheValid = false;
        _permittedZones = std::unordered_set<ZoneID_t>(
                zoneNums.begin(), zoneNums.end());
    }
};


///\brief Common sorting case for tracking -- stations ordered by z
///
/// It is intentionally designed to map layer's and stations keys into same
/// orderly number
class OrderedStations
        : public AbstractSpatialDetectorsOrder< std::map<DetID, unsigned int> > {
private:
    float (*_callback)(DetID, const Placement &);
protected:
    mutable size_t _nStations;
    std::map<DetID, unsigned int> _sort_detectors(
            const std::unordered_map<DetID, Placement> &, size_t & ) const override;
public:
    OrderedStations( calib::Manager & cmgr
                   , log4cpp::Category & logCat
                   , float (*cllb)(DetID, const Placement &)
                   , const std::string & namingCalibClass="default"
                   , const std::string & placementsCalibClass="default"
                   ) : AbstractSpatialDetectorsOrder( cmgr, logCat
                            , namingCalibClass, placementsCalibClass )
                    , _callback(cllb) { assert(_callback); }
    static float z_center(DetID, const Placement &);
    // ... other orsers?
};

}  // namespace ::na64dp::calib
}  // namespace na64dp
