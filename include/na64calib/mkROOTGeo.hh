/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"

#if defined(ROOT_FOUND) && ROOT_FOUND

#include "na64calib/placements.hh"

#include <list>
#include <log4cpp/Category.hh>

class TGeoVolume;
class TGeoMatrix;
class TGeoManager;
class TGeoMedium;

namespace na64dp {

/**\brief This class incrementally composes ROOT geometry using placements
 *
 * This is a simplified geometry composer to follow placements update. It
 * does not involve Geode/GDML geometry construction.
 * */
class PlacementsBasedGeometry {
protected:
    /// List of known placements. After geometry construction becomes empty.
    std::list<calib::Placement> _nativePlacements;
    
    /// A material mixture index. Most likely to be deleted in favour of more
    /// elaborated elements/material/mixture index (e.g. Geode)
    std::unordered_map<std::string, TGeoMedium *> _matMixtures;

    /// Ptr to current TGeoManager
    TGeoManager * _geoManager;
protected:
    log4cpp::Category & _L;
    /// Instantiates a ROOT's `TGeoCombiTrans` instance wrt
    /// given `calib::Placement` instance
    TGeoMatrix * _instantiate_ROOT_placement( const calib::Placement & );
    /// ...
    void _instantiate_materials();
    /// Instantiates volume wrt its supp type
    TGeoVolume * _instantiate_entity( const calib::Placement & pl
                                    , TGeoVolume * parent
                                    );
public:
    PlacementsBasedGeometry( TGeoManager *
                           , log4cpp::Category & L_
                           );
    ~PlacementsBasedGeometry();

    /// Called by placements update routines. Adds new native placement
    /// instance into the list to turn into ROOT geometrical primitive later
    /// on.
    void add_placement(const calib::Placement & pl );

    ///\brief World volume boundaries estimation
    ///
    /// Using already known placements, returns a pessimistic estimation of the
    /// world boundaries paraxial rectangular box. These boundaries are
    /// estimated roughly, ignoring actual rotation of the entities (square
    /// root of squared sum of their dimensions is taken as pessimistic
    /// estimation of their linear size).
    std::pair<TVector3, TVector3> world_boundaries() const;

    /// Constructs materials, volumes, etc wrt cached placements within given
    /// node as parental volume. Empties local native placements cache. Does
    /// NOT close geometry in order to let user code to manipulate with the
    /// geometry still.
    /// Expects `TGeoManager` instantiated (accesses it via `gGeoManager`)
    void instantiate_geometry( TGeoVolume * topVol );

    /// Get tracking medium by its name.
    ///
    /// Returns nullptr if not found.
    ///
    /// This method currently explots `_matMixtures` stub with hardcoded
    /// media.
    TGeoMedium * get_medium(const std::string &);
};

}  // namespace na64dp

#endif

