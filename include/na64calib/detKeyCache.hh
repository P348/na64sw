/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/dispatcher.hh"
#include "na64detID/detectorID.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace calib {

/**\brief A helper class caching the naming 
 *
 * A helper assists the frequent use case of calibration data using the naming
 * index. Typical use case scenario is when calibration data is defined
 * depending on the detector entity name.
 *
 * The name-to-id conversion is performed by standard `nameutils::DetectorNaming`
 * instance via ordinary handle.
 * */
template<typename T>
struct ByDetectorNames : public Handle< nameutils::DetectorNaming >
                       , public Handle< std::unordered_map<std::string, T> > {
public:
    /// Calibration data index by name (input read from source)
    typedef std::unordered_map<std::string, T> NameIndex;
    /// Calibration data index by ID (fast usable output provided by this helper)
    typedef std::unordered_map<DetID,       T> IDIndex;
protected:
    /// `true` when cache is valid
    mutable bool _isValid;
    /// Cached value
    mutable IDIndex _cache;
protected:
    /// User class
    virtual void _recache( const NameIndex & nameIndex
                         , IDIndex & idIndex ) const {
        idIndex.clear();
        for( const auto & p : nameIndex ) {
            idIndex.emplace( Handle<nameutils::DetectorNaming>::get()[p.first]
                           , p.second );
        }
    }
    /// Invalidates cache (names were updated)
    virtual void handle_update( const nameutils::DetectorNaming & nm ) override {
        invalidate();
        Handle<nameutils::DetectorNaming>::handle_update(nm);
    }
    /// Invalidates cache (index was updated)
    virtual void handle_update( const NameIndex & ni ) override {
        invalidate();
        Handle<NameIndex>::handle_update(ni);
    }
public:
    /// Ctr, requires calibration data dispatcher and, optionally, string
    /// identifiers for calibration data classes
    ByDetectorNames( Dispatcher & dsp
                   , const std::string calibDataClass="default" 
                   , const std::string namingClass="default"
                   ) : Handle<nameutils::DetectorNaming>(namingClass, dsp)
                     , Handle<NameIndex>(calibDataClass, dsp)
                     , _isValid(false)
                     {}
    /// Called by user code to indicate that cache is no longer valid
    /// (typically by updating code)
    void invalidate() { _isValid = false; }
    /// Re-caches index if needed
    const IDIndex & index() const {
        if( _isValid ) return _cache;
        _recache(Handle<NameIndex>::get(), _cache);
        _isValid = true;
        return _cache;
    }
};

}
}

