/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/index.hh"

namespace na64dp {
namespace calib {

/**\brief Calibration data loader interface
 *
 * This is type-agnostic generalization for calibration data loaders. Pointers
 * for this class may be used to generally treat (iterate, invoke, delete) a
 * collections of loaders.
 *
 * Loader subclasses incapsulates details of how the certain data type has to
 * be loaded using dispatcher isntance. `iLoader` subclasses implement the
 * concrete protocol of data retrieval.
 *
 * For instance performing a database request for certain data type and
 * certain event is the subject of loader instance. Retrieving data from a
 * file or fetching RESTful data are the other important examples.
 *
 * User may want to inherit this class for loader that supports conversion into
 * multiple types, like:
 *  1. Support for a source providing calibration data of multiple types
 *  2. Generalized codec (e.g. CSV) file loader converting string into the tuple.
 */
struct iLoader {
    ///\brief Type of the aliases dictionary provided by loader.
    ///
    /// Maps calibration data type string identifier (like "SADCPedestals" or
    /// "names" into internal ID).
    //typedef std::unordered_map<std::string, Dispatcher::CIDataID> CITypeAliases;

    virtual ~iLoader() {}
    /// Loads calibration data into given dispatcher instance
    virtual void load( const iUpdate &, Dispatcher & ) = 0;
    /// Shall return whether the loader is capable to load data by given update
    virtual bool can_handle( const iUpdate & ) { return false; }
    /// Shall dump general information about loader for debugging inspection
    virtual void to_yaml_as_loader(YAML::Node &) const = 0;
protected:
    /// \brief This method will be invoked by manager on adding the loader to
    /// the list of loaders to gather type aliases.
    ///
    /// The loader should either update this map immediately
    /// or cache reference to this map for later update (makes sense for
    /// generic loaders which can register additional types during their
    /// lifetime). The lifetime of the provided reference is guaranteed to
    /// excess the loader's lifetime.
    //virtual void put_provided_types( CITypeAliases & ) = 0;

    friend class Manager;
};

#if 0
/// Concrete calibration data type loader
template<typename T, typename UpdateT>
struct iSpecializedLoader : public iLoader<UpdateT> {
    virtual ~iSpecializedLoader(){}

    /// Forwards call to `load()` function, specialized on certain calibration
    /// data type, then uses dispatcher to deliver the loaded update.
    virtual void load_data( const UpdateT & updRecipe
                          , Dispatcher::CIDataID id
                          , Dispatcher & dsp ) override {
        dsp.set<T>(id.second, load(updRecipe, id.second));
    }

    /// Uses recipe to load the data of certain type; 
    virtual T load( const UpdateT & updRecipe
                  , const std::string & type
                  ) = 0;
};
#endif

}  // namespace ::na64dp::calib
}  // namespace na64dp

