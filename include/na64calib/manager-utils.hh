/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/manager.hh"
#include "na64calib/indices/yaml-index.hh"
#include "na64calib/loaders/yaml-document.hh"
#include "na64calib/loaders/csv.hh"

/**\file
 * \brief Various utility functions and shortcuts to deal with generic loaders
 *
 * This helper functions tends to provide shortcuts for operations with
 * common calibration routines: add simple YAML/CSV/etc. data type, etc.
 *
 * We do not define it with the main header of `Manager` class since this
 * helper functions require specialized loaders that are not needed for most
 * of the cases when calibration manager is required and will slow down
 * compilation significantly.
 * */

namespace na64dp {
namespace calib {

#if 0
template<typename T> void
add_yaml_data_type( Manager & m
            , const std::string & alias
            , void (*callback)( const YAML::Node &, Dispatcher & )
            , const std::string calibClass="default"
            ) {
    m.get_loader<na64dp::calib::YAMLDocumentLoader>("yaml-doc")
     .define_conversion<T>( alias, callback );
}

/**\brief Registers POD calibration data type constructed from CSV file
 *
 * Adds calibration reader of POD structure `T`. Provided argument must
 * define construction and modification of the reentrant calibration data
 * object by the tuples parsed from each line of the CSV.
 * */
template<typename T, typename ... ArgsT> void
add_csv_data_type( Manager & m
            , const std::string & alias
            , bool (*callback)(T &, ArgsT ... )
            , const std::string calibClass="default"
            ) {
    auto nc = new typename CSVLoader::TupleConversionFunction<T, ArgsT ...>(calibClass, callback);
    auto ir = m.get_loader<na64dp::calib::CSVLoader>("csv")
     .define_conversion<T>( alias
             , nc
             , calibClass );
    if( !ir ) {
        // insertion failed, but no exception thrown, meaning that we're trying
        // to register just the same conversion (repeated handlers
        // construction?) -- delete the conversion object as it won't be used
        delete nc;
    }
}
#endif

#if 0
/**\brief Registers array calibration data type constructed from CSV file
 *
 * Adds calibration reader of array POD structure `T`. Provided argument must
 * define construction and modification of the reentrant calibration data
 * object by the tuples parsed from each line of the CSV.
 * */
template<typename T, typename KeyT, typename ArrayElT, size_t N> void
add_array_data_type_from_csv( Manager & m
        , const std::string &
        , bool (*callback)( nameutils::DetectorNaming &, T &
                          , KeyT &
                          , const std::array<ArrayElT, N> &
                          )
        , const std::string calibClass="default"
        );
#endif

}  // namespace ::na64dp::calib
}  // namespace na64dp

