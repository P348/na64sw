/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/dispatcher.hh"
#include "na64calib/loader-generic.hh"
#include "na64detID/TBName.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace calib {

#if 0
/**\brief configures calibration data manager w.r.t. YAML node.
 *
 * This function implements a synchroneous setup of generic loader data indexes
 * and run indexes described by YAML node.
 *
 * The root YAML node has to be a map with keys corresponding to entries of
 * `ciCtrs`. The behaviour is following:
 *
 *  - for each node name in root node, the corresponding entry from `ciCtrs`
 *    will be checked. If no entry found, the `"calibrations.yaml"` level
 *    warning will be printed and this sub-node will be ignored
 *  - for entries being found, the `dataIndexPtr` will be checked. If it is
 *    null, the new instance will be created using `ctr` of corresponding entry
 *    and result will be assigned to `dataIndexPtr` writing info message.
 *  - within the current node, the integer field "validFromRun" has to be
 *    found. Its value is then used to compose an event ID (with
 *    spillNo=0, eventNo=0) that is then, along with the rest of the node data,
 *    forwarded to `append()` method of the `dataIndexPtr` (see
 *    `iYAMLDataIndex<T>::append()`). The info message confirming that data was
 *    appended will be printed.
 *  - the run index instance's (provided by `runIndex` argument) `add_entry()`
 *    method will be called with the event ID, the calibration data type ID and
 *    generic loader name (provided with `loaderName` arg).
 *
 * \param rootNode a root YAML node to iterate over
 * \param runIndex a run index to be appended synchroneously with the loader
 * \param loader a generic loader into that the data indexes are collected
 * \param loaderName the name of the (generic) loader to be refered from run index
 * \param ciCtrs a dictionary of data index constructors
 *
 * \note We have to highlight that existing data indexes (i.e. when
 *       `dataIndexPtr` is set and `ctr` is not called) will not be added to
 *       the loader as in this case we assume that these objects obey other
 *       lifycycle than this function suggests.
 * */
void
configure_from_YAML( YAML::Node & rootNode
                   , RangeOverrideRunIndex * runIndex
                   , GenericLoader * loader
                   , const std::string & loaderName
                   , std::unordered_map<std::string, YAMLCalibInfoCtr> & ciCtrs
                   );
#endif

/**\brief Reads naming provided as YAML document into the `DetectorNaming` object.
 * 
 * This function if supposed to be a callback for generic
 * `calib::YAMLDocumentLoader`.
 * 
 * \todo documentation on the naming APIs */
void YAML2Naming( const YAML::Node &, Dispatcher &);

/**\brief Reads event bit tags description into `EventBitTags` object.
 *
 * This function if supposed to be a callback for generic
 * `calib::YAMLDocumentLoader`.
 *
 * \todo API doc
 * */
void YAML2EventTags( const YAML::Node & node, Dispatcher & d );

/**\brief Sets detector name used to define master time
 *
 * A callback to `calib::YAMLDocumentLoader`. Dispatches changes of master time
 * by calibration data type `<"masterTimeDetName", std::string>`.
 * */
void dispatch_master_time_setting( const YAML::Node &, Dispatcher &);

}
}

