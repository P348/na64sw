/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#if 0
#include "na64util/csv-io.hh"
#include "na64util/uri.hh"
#include "na64calib/loader.hh"

#include <fstream>

namespace na64dp {
namespace calib {

#if 1

/**\brief Generic loader for calibration data from CSV file
 *
 * This generic loader provides multiple data types typicalle added at a
 * runtime. Requires the `source` field provided as a file path within the
 * `update` object emitted by index (`iIndex`).
 *
 * Supports only local files so far.
 *
 * \todo Support for remote fetch (REST, DB, etc)
 * */
class CSVLoader : public iLoader {
public:
    /// \brief Abstract base that performs conversion and dispatching of the
    /// data type requested.
    ///
    /// The CSV stream constructed from the calibration
    /// document is provided as an argument to a single virtual method that
    /// must construct the data and use dispatcher to set the data by itself.
    struct iConverter {
        virtual ~iConverter() {}
        virtual void convert_and_dispatch( csv::Reader &, Dispatcher & dsp ) = 0;
    };

    /// \brief A template helper for objects constructed iteratively from
    /// homogeneus entries. The tuple is used as carrier for read values.
    ///
    /// This one is convenient to be derived for short CSV rows.
    template<typename T, typename ... ArgsT>
    struct TupleConversionFunction : public iConverter {
        ///
        const std::string calibClass;
        ///
        bool (*callback)(T &, ArgsT ... );
        ///
        virtual void convert_and_dispatch( csv::Reader & reader
                                         , Dispatcher & dsp ) override {
            T obj;
            std::tuple<ArgsT ...> tuple;
            size_t lineCount = 0;
            while( reader.read(tuple) ) {
                //callback( obj, tuple );
                meta::apply( callback, obj, tuple );
                ++lineCount;
            }
            dsp.set(calibClass, std::move(obj));
        }

        TupleConversionFunction( const std::string & calibClass_
                               , bool (*callback_)(T &, ArgsT ...)
                               ) : calibClass(calibClass_)
                                 , callback(callback_)
                                 {}
        virtual ~TupleConversionFunction(){}
    };
private:
    /// Reference to the log object
    log4cpp::Category & _log;
    /// Collection of conversion callbacks identified with type identifier
    std::unordered_map< Dispatcher::CIDataID
                      , iConverter * // TODO: shared_ptr
                      , util::PairHash
                      > _converters;
    /// Provided aliases: strType -> calibration data ID
    iLoader::CITypeAliases * _aliases;
protected:
    /// Caches the ref to aliases dict (this is a generic loader)
    virtual void put_provided_types( iLoader::CITypeAliases & ) override;
public:
    CSVLoader()
        : _log(log4cpp::Category::getInstance(std::string("calib.CSV")))
        , _aliases(nullptr) {}
    /// Reaches the CSV file, constructs the `csv::Reader` instance around it and
    /// forwards execution to one of the sepcialized conversion callbacks that
    /// has to notify dispatcher.
    virtual void load_data( const iIndex::UpdateRecipe &
                          , Dispatcher::CIDataID
                          , Dispatcher & ) override;
    /// Return whether the loader is capable to load the data of given type
    /// (for this subclass, effectively check whether the conversion callback
    /// was registered).
    ///
    /// \todo Implement a check of whether the file is reachable
    virtual bool provides( const iIndex::UpdateRecipe &
                         , Dispatcher::CIDataID ) override;

    /// \brief Adds a new calibration conversion definition to generic loader
    ///
    /// One must pay attention to two string arguments here
    ///
    /// \param typeAlias must be human-readable alias for calibration data type
    /// \param callback is a conversion callback
    /// \param ciClassID is a string used in combination with C++ data type
    template<typename T>
    bool define_conversion( const std::string & typeAlias
                          , iConverter * cnvr
                          , const std::string & ciClassID="default"
                          ) {
        auto tpid = Dispatcher::info_id<T>(ciClassID);
        auto ir = _converters.emplace( tpid, cnvr );
        if( ! ir.second ) {
            if( ir.first->first != tpid ) {
                NA64DP_RUNTIME_ERROR( "Type alias collision for identifier \"%s\":"
                        " previously defined: %s, new: %s."
                        , typeAlias.c_str()
                        , util::calib_id_to_str( ir.first->first ).c_str()
                        , util::calib_id_to_str( tpid ).c_str()
                        );
            } else {
                return false;
            }
        }
        assert(_aliases);  // index is not associated with manager at the moment
        auto aliasIr = _aliases->emplace(typeAlias, tpid);
        if( ! aliasIr.second && aliasIr.first->second != tpid ) {
            _log.error( "Conflicting calibration data type aliases: generic"
                    " CSV loader callback "
                    " aliases type %s as \"%s\", while the same alias already"
                    " used for %s."
                    , util::calib_id_to_str( tpid ).c_str()
                    , typeAlias.c_str()
                    , util::calib_id_to_str( aliasIr.first->second ).c_str() );
        }
        return true;
    }
};

#else
/**\brief Reads CSV files into array of tuples
 * */
template<typename T, typename ... TupleTypesT>
class iCSVFilesDataIndex : public iYAMLDataIndex<T> {
public:
    typedef std::tuple<TupleTypesT...> Tuple;

    virtual void append_index_from_tuple( T &
                                        , const Tuple & ) = 0;

    virtual T yaml_to_data( const YAML::Node & node ) override {
        const std::string tokenRegexStr = node["tokenRegex"]
                                        ? node["tokenRegex"].as<std::string>()
                                        : std::string("[^\\s,]+");

        #if 0
        util::uriparser::URI srcURI( node["source"].as<std::string>() );
        if( !(srcURI.scheme().empty() || "file" == srcURI.scheme()) ) {
            NA64DP_RUNTIME_ERROR( "Retrieving CSV data by scheme or"
                    " protocol \"%s\" is not implemented."
                    , srcURI.scheme().c_str() );
        }
        // TODO: code below will be moved once we implement another data
        // fetching scheme or protocol
        assert( !srcURI.path().empty() );
        auto names = util::expand_names( srcURI.path() );
        # else
        std::string path = node["source"].as<std::string>();
        auto names = util::expand_names( path );
        # endif

        if( names.size() > 1 ) {
            NA64DP_RUNTIME_ERROR( "Path expression \"%s\" was expanded to"
                    " %d(!=1) names."
                    , path.c_str()
                    , names[0].c_str() );
        } else if(names.empty()) {
            NA64DP_RUNTIME_ERROR( "Can not interpret path \"%s\"."
                    , path.c_str() );
        }

        std::ifstream ifs( names[0] );
        na64dp::csv::Reader vr(ifs, 0, tokenRegexStr);
        std::tuple<TupleTypesT...> t;
        T dataIndex;
        while( vr.read<TupleTypesT...>(t) ) {
            append_index_from_tuple( dataIndex, t );
        }
        return dataIndex;
    }
};
#endif

}
}
#endif
