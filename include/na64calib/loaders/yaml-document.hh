/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/loader.hh"
#include "na64util/pair-hash.hh"

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace calib {

/**\brief Generic loader for calibration data from YAML documents
 *
 * This generic loader is capable to provide multiple data types added at a
 * runtime. Requires the `source` field given as file path to be provided
 * within the `update` object yielded from index update.
 *
 * Currently, only supports local files.
 *
 * \note Type alias must be registered at the time ...
 *
 * \todo Add support for remote fetch (HTTP, databases, etc).
 * */
class YAMLDocumentLoader : public iLoader {
public:
    /// Callback function that performs conversion and dispatching of the
    /// data type requested. The YAML node constructed from the calibration
    /// document is provided as an argument. Callback must construct the data
    /// and use dispatcher to set the data by itself.
    typedef void (*Conversion)(const YAML::Node &, Dispatcher &);
private:
    /// Reference to the log object
    log4cpp::Category & _log;
    /// Collection of conversion callbacks identified with type identifier
    std::unordered_map< std::string, Conversion> _conversions;
public:
    YAMLDocumentLoader()
        : _log(log4cpp::Category::getInstance(std::string("calib.yamlDoc")))
        {}
    /// Reaches the YAML document, parses it and forwards execution to one of
    /// the sepcialized conversion callbacks that has to notify dispatcher.
    virtual void load( const iUpdate &, Dispatcher & ) override;
    /// Return whether the loader is capable to load the data of given type
    /// (for this subclass, effectively check whether the conversion callback
    /// was registered).
    ///
    /// \todo Implement a check of whether the file is reachable
    virtual bool can_handle( const iUpdate & ) override;

    /// \brief Adds a new calibration conversion definition to generic loader
    ///
    /// Repeated calls with same values are quietly ignored. If, however,
    /// second callback provided for same type alias, an exception is thrown
    /// (conversions must be unique).
    ///
    /// \throw Runtime error on collision or
    /// \param alias str-name for the aliase in use
    /// \param callback is a conversion callback
    void define_conversion( const std::string & alias
                          , void (*callback)(const YAML::Node &, Dispatcher &)
                          );
    /// Puts loader state into YAML node for debug
    void to_yaml_as_loader( YAML::Node & ) const override;
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

