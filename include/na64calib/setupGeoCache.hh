/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/manager.hh"
#include "na64calib/placements.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace calib {

///\brief Abstract helper for pretty common case of placements data subscribers
class SetupGeometryCache : protected calib::Handle<nameutils::DetectorNaming>
                         , public util::Observable<calib::Placements>::iObserver 
                         {
protected:
    log4cpp::Category & _log;
public:
    SetupGeometryCache( calib::Dispatcher & cdsp
                      , log4cpp::Category & logCat
                      , const std::string & namingCalibClass="default"
                      , const std::string & placementsCalibClass="default"
                      );
    virtual ~SetupGeometryCache() {}
    const nameutils::DetectorNaming & naming() const
        { return calib::Handle<nameutils::DetectorNaming>::operator*(); }
    /**\brief Shall handle update for a single placement entry */
    virtual void handle_single_placement_update(const calib::Placement &) = 0;
    void handle_update( const calib::Placements & placements ) override;
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

