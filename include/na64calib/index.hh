/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/na64/event-id.hh"
#include "na64calib/dispatcher.hh"

#include <yaml-cpp/yaml.h>

namespace YAML { class Node; }  // fwd

namespace na64dp {
namespace calib {
namespace aux {
/// Default is not implemented
template<typename T> void update_payload_to_yaml(const T & pl, YAML::Node & node) {
    char bf[32];
    snprintf(bf, sizeof(bf), "%p", &pl);
    node["_payloadPtr"] = bf;
}
/// YAML to YAML
template<> void update_payload_to_yaml(const YAML::Node & pl, YAML::Node & node);
}  // namespace ::na64dp::calib::aux

struct iLoader;  // fwd

///\brief Single datum update recipe
///
/// A piece of information used to describe which data shall be loaded by
/// loader. Issued by `iIndex` subclasses, sorted by manager class and then
/// consumed by loaders to actually load the data.
struct iUpdate {
    virtual Dispatcher::CIDataID subject_data_type() const = 0;
    virtual iLoader * recommended_loader() const = 0;
    /// This is only for debug
    virtual void to_yaml(YAML::Node & node) const {
        char bf[32];
        snprintf(bf, sizeof(bf), "%p", this);
        node["_ptr"] = bf;
    }
    virtual ~iUpdate() {}
};

template<typename T>
struct iSpecificUpdate : public iUpdate {
    T payload;
    iSpecificUpdate(const T & o) : payload(o) {}
    iSpecificUpdate(const T && o) : payload(o) {}
    virtual ~iSpecificUpdate() {}
    /// This is only for debug
    virtual void to_yaml(YAML::Node & node) const {
        aux::update_payload_to_yaml(payload, node);
    }
};

/// Container type for list of the enqueued updates
typedef std::vector<iUpdate*> Updates;

/**\brief Defines what calibration data have to be updated based on event ID
 *
 * Class implementing this interface has to decide which calibration data types
 * has to be updated between runs.
 *
 * Typical subclassing example is, of course, a database querying object that
 * maintains a connection, issues queries, etc. However the calibration data
 * itself must not be fetched by index class (we need to follow
 * application-driven logic of resolving dependencies and order of updates
 * treatment). In simpler case it may be a config file or just a static,
 * hard-coded class mapping range to certain update instructions.
 *
 * Indices are type-agnostic at the level of `iLoader` meaning that one index
 * instance may update calibration data of various types.
 *
 * The typical user of this class is calibration manager instance that may
 * maintain a collection of the indices. This way an extensible modular approach
 * for maintaining calibration data is achieved: indices supersedes
 * updates (last update shall override previous), dependency resolution, etc.
 */
struct iIndex {
    /// Appends list of the run to be updated between events. Type collisions
    /// (conflicting updates) shall be resolved by displacing the existing with
    /// current.
    virtual void append_updates( EventID oldEventID, EventID newEventID
                               , const std::pair<time_t, uint32_t> & oldKey
                               , const std::pair<time_t, uint32_t> & newKey
                               , Updates & ) = 0;
    /// May be overriden to clean up temporary update objects
    virtual void clear_tmp_update_objects() {}
    virtual ~iIndex(){}

    virtual void to_yaml( YAML::Node & ) const = 0;
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

