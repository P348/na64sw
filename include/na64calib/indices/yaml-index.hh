/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include <yaml-cpp/yaml.h>

#include "na64calib/indices/range-override.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace calib {

/**\brief A calibration data index provided within the YAML file
 *
 * Expected input node is an array of objects. Each object shall define
 * at least following fields:
 *      * "dataType" : type name (alias), string
 *      * "validity": pair of runs (from, to)
 *      * "loader": name of loader
 * Rest nodes will be forwarded to an update body (update subtype is of
 * `RangeOverrideRunIndex<T, YAML::Node>` where `T` is `EventID` or `time_t`).
 *
 * This object is designed to handle updates with following features:
 *      - from documents that are not self-annotated (loaders "yaml-doc" or
 *        "csv-doc")
 *      - small datum defined entirely within this node
 * */
class YAMLIndex : public RangeOverrideRunIndex<EventID, YAML::Node>
                , public RangeOverrideRunIndex< std::pair<time_t, uint32_t>
                                              , YAML::Node
                                              >
                {
public:
    /// Creates empty index
    YAMLIndex() : iIndex() {}
    virtual ~YAMLIndex(){}
    /// Creates index from the node provided
    void add_records( const YAML::Node & entries
                    , const std::unordered_map<std::string, Dispatcher::CIDataID> & aliases
                    , const std::unordered_map<std::string, std::shared_ptr<iLoader>> & loadersByName
                    );
    /// Collects calibration information differences between two runs
    void append_updates( EventID oldEventID, EventID newEventID
                       , const std::pair<time_t, uint32_t> & oldDatetime
                       , const std::pair<time_t, uint32_t> & newDateTime
                       , Updates & ) override;
    /// Shall dump index content for inspection
    void to_yaml(YAML::Node &) const override;
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

