/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64sw-config.h"
#include "na64util/na64/event-id.hh"
#include "na64calib/manager.hh"
#include "na64util/demangle.hh"

#include <map>
#include <cassert>

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace errors {

/// Indexing container has no valid range wrt given value
class NoValidForRange : public std::runtime_error {
public:
    NoValidForRange()
            : std::runtime_error( "No valid entry has been found for event." ) {}
};

}  // namespace na64dp::error

namespace calib {

namespace aux {
/// This auxiliary function is used by RangeOverrideRunIndex::to_yaml() to
/// represent a key
template<typename KeyT> std::string key_to_str_for_yaml(KeyT k) {
    std::ostringstream oss;
    oss << k;
    return oss.str();
}

/// Specialization for time structure
template<> std::string key_to_str_for_yaml(std::pair<time_t, uint32_t>);

/// Specialization for run number
template<> std::string key_to_str_for_yaml(EventID);

template<typename KeyT> std::pair<KeyT, KeyT>
deduce_key( EventID oldEventID, EventID newEventID
          , const std::pair<time_t, uint32_t> & oldDatetime
          , const std::pair<time_t, uint32_t> & newDateTime
          );

template<> std::pair<EventID, EventID>
deduce_key( EventID oldEventID, EventID newEventID
          , const std::pair<time_t, uint32_t> & oldDatetime
          , const std::pair<time_t, uint32_t> & newDateTime
          );

template<> std::pair<std::pair<time_t, uint32_t>, std::pair<time_t, uint32_t>>
deduce_key( EventID oldEventID, EventID newEventID
          , const std::pair<time_t, uint32_t> & oldDatetime
          , const std::pair<time_t, uint32_t> & newDateTime
          );

}  // namespace ::na64dp::calib::aux

/**\brief A runs validity range index helper class.
 *
 * Maps objects that are valid _from_ certain run number. This helper class
 * incapsulates `std::upper_bound()` over sorted associative array to provide
 * a bit more intuitive way of retreiving the data associated with range.
 *
 * Ususal application is to index calibration data wrt runs ranges. */
template<typename KeyT, typename T, typename Compare = std::less<KeyT>>
class RangeIndex : public std::map<KeyT, T, Compare> {
public:
    typedef std::map<KeyT, T, Compare> Parent;
public:
    typename Parent::iterator
    get_valid_entry_for(KeyT rn) {
        assert( !Parent::empty() );
        // it->first > rn, i.e. get iterator to the next element
        auto it = Parent::upper_bound( rn );
        if( it == Parent::begin() ) {
            return Parent::end();
        }
        // Return element previous to upper bound
        return --it;
    }
};

/**\brief A basic externally-configured calibration index class.
 *
 * Indexes calibration entries by key. Each entry of certain
 * calibration type considered valid *from* certain key value *until* the
 * next key value. The `validTo` attribute to the update recipe is considered
 * only on the update loading and may result in a runtime expression.
 */
template<typename KeyT, typename T, typename Compare = std::less<KeyT>>
class RangeOverrideRunIndex : public virtual iIndex {
public:
    struct UpdateRecipe : public iSpecificUpdate<T> {
        Dispatcher::CIDataID subjectDataType;
        KeyT validTo;
        std::shared_ptr<iLoader> recommendedLoader;

        UpdateRecipe( Dispatcher::CIDataID dt
                    , KeyT validTo_
                    , std::shared_ptr<iLoader> lPtr
                    , const T pl
                    ) : iSpecificUpdate<T>(pl)
                      , subjectDataType(dt)
                      , validTo(validTo_)
                      , recommendedLoader(lPtr)
                      {}

        Dispatcher::CIDataID subject_data_type() const override
                { return subjectDataType; }
        iLoader * recommended_loader() const override
                { return recommendedLoader.get(); }
    };
private:
    /// Collection of run range sub-indexes, grouped by calibration data type
    std::unordered_map< std::string
                      , RangeIndex< KeyT, UpdateRecipe, Compare >
                      > _types;
public:
    virtual ~RangeOverrideRunIndex() {}
    /// Collects calibration information differences between two time
    /// moments/runs
    void append_updates( EventID oldEventID, EventID newEventID
                       , const std::pair<time_t, uint32_t> & oldDatetime
                       , const std::pair<time_t, uint32_t> & newDateTime
                       , Updates & ul ) override {
        auto keys = aux::deduce_key<KeyT>( oldEventID, newEventID
                                         , oldDatetime, newDateTime
                                         );
        for( auto it = _types.begin()
           ; _types.end() != it
           ; ++it ) {
            auto cidID = it->first;
            auto & runIdx = it->second;
            auto oldEntryIt = runIdx.get_valid_entry_for( keys.first )
               , newEntryIt = runIdx.get_valid_entry_for( keys.second )
               ;
            if( oldEntryIt == newEntryIt ) continue;
            if( runIdx.end() == newEntryIt ) {
                throw errors::NoValidForRange();
            }
            ul.push_back(&(newEntryIt->second));
            log4cpp::Category::getInstance("calibrations.indeces.rangeOverride")
                << log4cpp::Priority::DEBUG
                << "Update of (aliased) type \""
                << it->first
                << "\" with validity ("
                << aux::key_to_str_for_yaml(newEntryIt->first)
                << ") has been pushed into updates list for "
                << "/("
                << aux::key_to_str_for_yaml(oldEventID) << "), ("
                << aux::key_to_str_for_yaml(newEventID) << ")/ -> /("
                << aux::key_to_str_for_yaml(oldDatetime) << ", "
                << aux::key_to_str_for_yaml(newDateTime) << ")/"
                << "."
                ;
        }
    }
    
    /// Adds new calibration data entry into index
    void add_entry( KeyT k
                  , const std::string & typeKey
                  , const UpdateRecipe & updRecipe
                  ) {
        // find or insert typed entry (ranges index)
        auto tIt = _types.find( typeKey );
        if( _types.end() == tIt ) {
            auto ir = _types.emplace( typeKey
                                    , RangeIndex<KeyT, UpdateRecipe, Compare>() );
            tIt = ir.first;
        }
        // emplace new entry
        RangeIndex< KeyT, UpdateRecipe, Compare> & rangeIndex = tIt->second;
        auto ir = rangeIndex.emplace(k, updRecipe);
        if( ! ir.second ) {
            log4cpp::Category::getInstance("calibrations.indeces.rangeOverride").warn(
                        "Doubling specification for calibration"
                        " data %s."
                        , typeKey.c_str()
                        //, std::to_string(k.to_str())
                        );
        } else {
            log4cpp::Category::getInstance("calibrations.indeces.rangeOverride")
                << log4cpp::Priority::DEBUG
                << "RangeOverrideRunIndex::add_entry("
                << aux::key_to_str_for_yaml(k) << ", "
                << typeKey << ", ("
                << aux::key_to_str_for_yaml(updRecipe.validTo)
                << ", ...)"
                ;
        }
    }
    /// Dumps content of the index index to YAML node for debugging
    void to_yaml(YAML::Node & outNode) const override {
        outNode["keyType"]
            = std::string(util::demangle_cpp(typeid(KeyT).name()).get());
        outNode["valueType"]
            = std::string(util::demangle_cpp(typeid(T).name()).get());
        YAML::Node typesNode;
        for( auto subIdxPair : _types ) {
            char bf[64];
            // NOTE: subIdxPair.second is always of UpdateRecipe
            YAML::Node typedNode;
            size_t nEntry = 0;
            for( auto updPair : subIdxPair.second ) {
                YAML::Node entryNode;
                entryNode["validFrom"] = aux::key_to_str_for_yaml(updPair.first);
                entryNode["validTo"] = aux::key_to_str_for_yaml(updPair.second.validTo);
                entryNode["subjectType"] = util::calib_id_to_str(updPair.second.subjectDataType);
                snprintf(bf, sizeof(bf), "%p", updPair.second.recommendedLoader.get());
                typedNode["recommendedLoader"] = bf;
                updPair.second.to_yaml(entryNode);
                typedNode[nEntry++] = entryNode;
            }
            typesNode[subIdxPair.first] = typedNode;
        }
        outNode["byTypeAlias"] = typesNode;
    }
};

}  // namespace calib
}  // namespace na64dp

