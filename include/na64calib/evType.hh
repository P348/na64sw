#pragma once

#include "na64sw-config.h"

#include <cstdint>
#include <unordered_map>
#include <string>

#include <yaml-cpp/yaml.h>

namespace na64dp {

/**\brief Event bit tags for trigger and type */
struct EventBitTags {
    /**\brief Dictionary providing semantics for bits used in event fields
     *
     * Usually, used within calibration object specifying match between string tag
     * and bitwise value */
    struct EventSemanticBits : std::unordered_map<std::string, uint16_t> {
        /// Turns given bitmask to string for human-readable output
        std::string bitflags_to_str(uint16_t flags) const;
    };

    EventSemanticBits triggers;
    EventSemanticBits types;

    EventBitTags() {}
    EventBitTags(const YAML::Node &);
};

}  // namespace na64dp

