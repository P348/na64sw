/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

/**\file
 * \brief Defines "placements" calibration data type and its vctr features
 *
 * These are interim types used to represent parsed data. Users code most
 * probably would like to turn this generic information entries into own
 * task-specific cached info.
 */

#include "na64util/vctr.hh"
#include "na64util/transformations.hh"

#if defined(ROOT_FOUND) && ROOT_FOUND
#   include <TVector3.h>
#endif

#include <list>

//
// Placements types

namespace na64dp {
namespace calib {

/**\brief Supplementary information for planar detector
 *
 * Most common case of tracking detector is a plane with wires or strips
 * registering charged particle's intersection coordinate. This supplementary
 * info type is used to extend ordinary placement with relevant geometrical
 * parameters. */
struct RegularWiredPlane {
    unsigned int nWires;  ///< number of wires
    float resolution;  ///< detector resolution, if specified
};

/**\brief Supplementary information for planar detector with irregular wiring
 *
 * A plane measuring single coordinate with irregular stride. Refers to a
 * mapping function that translates wire (channel ID) into measured coordinate.
 * */
struct IrregularWiredPlaneCallback {
    char layoutName[128];
};

/**\brief Supplementary information for volumetric detector
 *
 * Supports externally-set resolutions
 * */
struct VolumetricDetector {
    float localUncertainties[3];
};

/**\brief Supplementary information for magnet description with mag.field map
 *
 * Refers to the magnetic field map file or other instance, by URI. If
 * map format does not facilitate self description of units these attributes
 * can force conversion. `techLabel` might be used by some subsidiary
 * identification systems.
 * */
struct FieldMap {
    char mapFilePath[192]  ///< dump URI path or path template
       , fieldUnits[8]     ///< units of the magnetic field, if need
       , lengthUnits[8]    ///< units of length, if need
       , techLabel[16]     ///< technical label in engineering nomenclature
       , fieldClass[32]    ///< virtual ctr name
       ;
};

/**\brief Individual placement item type
 *
 * A "placement" defines 3D position and spatial orientation of a
 * volumemetric object, some common geometrical properties, etc.
 *
 * Routines logging category is `calib.placements`.
 * */
struct Placement {
    std::string name;  ///< name of the detector spatial item (plane)
    float center[3]  ///< global coordinates of the item, cm
        , rot[3]  ///< Euler angles defining orientation of an item
        , size[3]  ///< size of the item, cm
        ;
    /// Rotation order for `rot`
    na64sw_RotationOrder rotationOrder;
    int zone;  ///< linear segments geometrical zone OR frame no. for magnets
    std::string material  ///< Name of material medium, detectors only
              ;
    enum SuppInfoType {
        // detectors:
        kDetectorsGeometry = 0x1,
        /// Reguraliry-wired detector plane
        kRegularWiredPlane   = kDetectorsGeometry | (0x1 << 8),
        /// Irregularily-wired detector plane (variable pitch)
        kIrregularWiredPlane = kDetectorsGeometry | (0x2 << 8),
        /// General case of volumetric detector (measures spatial points)
        kVolumetricDetector  = kDetectorsGeometry | (0x3 << 8),
        // ... (other detector types supp.info here)
        // magnets:
        kMagnetsGeometry = 0x2,
        /// Field map magnet item
        kFieldMap            = kMagnetsGeometry | (0x1 << 8),
        // ... (other magnet types supp.info here)
    } suppInfoType;
    union SuppInfo {
        // Detectors
        RegularWiredPlane regularPlane;
        IrregularWiredPlaneCallback irregularPlane;
        VolumetricDetector volumetricDetector;
        // ...
        FieldMap fieldMap;
    } suppInfo;

    bool is_detector() const {
        return ((int) suppInfoType) & ((int) kDetectorsGeometry);
    }
    bool is_magnet() const {
        return ((int) suppInfoType) & ((int) kMagnetsGeometry);
    }
};

/// List of placements (update type)
typedef std::list<Placement> Placements;
}  // namespace ::na64dp::calib

namespace util {

/**\brief Creates transformation object defined by placement object
 *
 * Takes into account center, rotation, sizes, etc.
 * */
Transformation transformation_by(const calib::Placement &);

}  // namespace ::na64dp::util
}  // namespace na64dp

