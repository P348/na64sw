/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2023 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "setupGeoCache.hh"
#include "na64util/transformations.hh"

namespace na64dp {
namespace calib {

template<typename T>
struct PlacementDefinedTypeTraits {
    static T create_item(const calib::Placement & pl) { return T(pl); }
};

template<>
struct PlacementDefinedTypeTraits<util::Transformation> {
    static util::Transformation create_item(const calib::Placement & pl)
        { return util::transformation_by(pl); }
};

///\brief Helper class keeping items defined by placements
template<typename T>
class PlacementsDefinedCache
        : public SetupGeometryCache
        , private std::unordered_map<DetID, T> {
protected:
    /// Caches item induced by detector's placement
    void _cache_item(DetID did, const calib::Placement & pl ) {
        const auto obj = PlacementDefinedTypeTraits<T>::create_item(pl);
        auto ir = this->emplace(did, obj);
        if(ir.second) {
            _log << log4cpp::Priority::DEBUG
                 << "Cached entry of item defined by placement " << naming()[did];
        } else {
            ir.first->second = T(obj);
            _log << log4cpp::Priority::DEBUG
                 << "Replaced entry of item defined by " << naming()[did];
        }
    }
public:
    PlacementsDefinedCache( calib::Dispatcher & cdsp
            , log4cpp::Category & logCat
            , const std::string & namingCalibClass="default"
            , const std::string & placementsCalibClass="default")
        : SetupGeometryCache(cdsp, logCat, namingCalibClass, placementsCalibClass)
        {}
    ///\brief Caches transformation induced by placement
    ///
    /// Forwards execution to `_cache_transform_by()`.
    void handle_single_placement_update(const calib::Placement & pl) override {
        if(!( ((int) pl.suppInfoType)
            & ((int) calib::Placement::kDetectorsGeometry)
            )
          ) return;  // omit non-detector items
        _cache_item(naming()[pl.name], pl);
    }
    ///\brief Returns transformation induced by detector's placement
    ///
    /// Should be previously added by `_cache_transform_by()`
    const T & get_placement_defined_item_for(DetID did) const {
        auto it = this->find(did);
        if(this->end() == it) {
            NA64DP_RUNTIME_ERROR( "No entry cached for %s."
                    , this->naming()[did].c_str() );
        }
        return it->second;
    }
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

