/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64calib/manager.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace calib {

#if 0
/**\brief Represents a collection of calibration data indices of different types
 *
 * Maintans set of calibration data indices, grouped by type. For given event
 * ID produces the list of the updates that must be then processed and
 * dispatched to the calibration data handles.
 * */
class GenericLoader : public iLoader {
public:
    /**\brief Value setter proxy
     *
     * Assures type correctness when data index sets calibration data into
     * dispatcher.
     * */
    class DispatcherProxy {
    private:
        Dispatcher & _dispatcher;
        Dispatcher::CIDataID _type;
        bool _wasUsed;
    protected:
        DispatcherProxy( Dispatcher & d, Dispatcher::CIDataID ciid )
            : _dispatcher(d), _type(ciid), _wasUsed(false) {}
        bool was_used() const { return _wasUsed; }
    public:
        template<typename T> void set(const T & v) {
            if(Dispatcher::info_id<T>(_type.second) != _type) {
                NA64DP_RUNTIME_ERROR( "Bad type assignment" );  // TODO?
            }
            _dispatcher.set<T>(_type.second, v);
            _wasUsed = true;
        }
        template<typename T> void set(T && v) {
            if(Dispatcher::info_id<T>(_type.second) != _type) {
                NA64DP_RUNTIME_ERROR( "Bad type assignment" );  // TODO?
            }
            _dispatcher.set<T>(_type.second, std::move(v));
            _wasUsed = true;
        }
        template<typename T> DispatcherProxy & operator=(const T & v) {
            set(v);
            return *this;
        }
        friend class GenericLoader;
    };

    /**\brief Abstract data indexing interface
     *
     * Subclass may define the way to work with C++ type and be additionally
     * specialized via `name` argument.
     */
    class AbstractCalibDataIndex {
    public:
        /// Should load data referenced by event ID and name into dispatcher
        virtual void put( EventID, DispatcherProxy & ) = 0;
        /// Should return whether index has data for the event
        virtual bool has( EventID ) = 0;
        //virtual void append( EventID, const YAML::Node & ) = 0;
        virtual ~AbstractCalibDataIndex() {}
    };
private:
    /// Known calibration data type indexes
    std::unordered_map< Dispatcher::CIDataID
                      , AbstractCalibDataIndex *
                      , util::PairHash > _dataIndexes;
public:
    /// Returns whether the specified calibration is available.
    virtual bool has( EventID eventID
                    , Dispatcher::CIDataID cidID ) override;
    /// Registers calibration data index instance for certain type
    virtual void add_data_index( Dispatcher::CIDataID
                               , AbstractCalibDataIndex * );

    #if 1
    /// Loads calibration data into given dispatcher instance.
    virtual void load_data( EventID eventID
                          , Dispatcher::CIDataID cidID
                          , Dispatcher & d ) override;
    #endif
};

}  // namespace ::na64dp::calib

namespace errors {
class CalibDataTypeIsNotDefined : public GenericRuntimeError {
private:
    calib::Dispatcher::CIDataID _cdid;
    calib::GenericLoader * _loaderPtr;
public:
    CalibDataTypeIsNotDefined( calib::Dispatcher::CIDataID
                             , calib::GenericLoader * collectionPtr ) throw();
    calib::Dispatcher::CIDataID calib_data_type_id() const throw() { return _cdid; }
    calib::GenericLoader * collection_ptr() const throw() { return _loaderPtr; }
};
#endif

}  // namespace ::na64dp::error
}  // namespace na64dp

