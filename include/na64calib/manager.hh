/* This file is a part of NA64SW software.
 * Copyright (C) 2015-2022 NA64 Collaboration, CERN
 *
 * NA64SW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#pragma once

#include "na64util/uri.hh"
#include "na64detID/detectorID.hh"
#include "na64calib/dispatcher.hh"
#include "na64calib/loader.hh"

#include <log4cpp/Category.hh>

#include "na64util/na64/event-id.hh"

namespace na64dp {
namespace nameutils {
class DetectorNaming;  // fwd, defined in na64detID/TBName.hh
}  // namespace na64dp::utils
namespace calib {

/**\brief Global singleton class indexing calibration data type aliases
 *
 * Provides general match between the string identifier used in runtime
 * configuration files and internal data type identifier used to uniquely
 * address particular calibration data type.
 *
 * Since aliases dictionary usually instantiated at the very early stage
 * of application runtime (when constructors get called), logging system
 * is not available for this isntance. App shall explicitly call the
 * `init_logging()` afterwards to make this class cope with general
 * logging system
 *
 * \note Dynamically loaded modules uses this singleton before main logging
 *       is initialized.
 * */
class CIDataAliases {
private:
    static CIDataAliases * _self;
public:
    /// Dictionary type for aliases (name to RTTI data type)
    typedef std::unordered_map<std::string, Dispatcher::CIDataID>
        TypeIDByName;
    /// Dictionary type for aliases (RTTI data type to name)
    typedef std::unordered_map<Dispatcher::CIDataID, std::string, util::PairHash>
        NameByTypeID;
    /// List of calib data type dependencies
    ///
    /// Organized as unordered multimap internally (pairs of "product" and
    /// "requirement"). Required type aliases are not resolved in this
    /// container. This way user code may mention dependencies before they
    /// actually appear at a runtime.
    typedef std::unordered_multimap< Dispatcher::CIDataID
                                   , std::string
                                   , util::PairHash > Dependencies;
protected:
    /// Aliases dictionary, RTTI data type by str name
    TypeIDByName _nameToTypeID;
    /// Reverse aliases dictionary, str name by RTTI data type
    NameByTypeID _typeIDToName;
    /// Type's dependencies
    Dependencies _depends;
    /// Ptr to logging category
    log4cpp::Category * _logPtr;

    /// Dependencies cache validity flag
    mutable bool _depsCacheValid;
    /// Dependencies cache
    mutable std::unordered_multimap< Dispatcher::CIDataID
                                   , Dispatcher::CIDataID
                                   , util::PairHash > _depsCache;


    CIDataAliases();
public:
    CIDataAliases(const CIDataAliases &) = delete;

    ///\brief Adds new alias for calibration data type
    ///
    ///\throw Runtime error on type collision.
    void add_alias( const std::string &, Dispatcher::CIDataID );
    /// Adds new alias for calibration data type (CIDataID derived
    /// automatically)
    template<typename T> Dispatcher::CIDataID
    add_alias_of( const std::string aliasName
                , const std::string strTypeID ) {
        auto td = Dispatcher::info_id<T>(strTypeID);
        add_alias(aliasName, td);
        return td;
    }
    /// Returns calibration data type aliases names indexed by type ID
    const NameByTypeID & name_by_type_id() const { return _typeIDToName; }
    /// Returns calibration data type IDs indexed by alias
    const TypeIDByName & type_id_by_name() const { return _nameToTypeID; }

    /// Defines calibration data dependency
    void add_dependency( const std::string & product
                       , const std::string & requirement
                       );
    /// Returns map of dependencies between types
    const std::unordered_multimap< Dispatcher::CIDataID
                                 , Dispatcher::CIDataID
                                 , util::PairHash > &
    dependency_map() const;


    /// Has to be invoked once the logging subsystem is initialized.
    void init_logging();

    /// Returns instance
    static CIDataAliases & self();
};

/// Macro for runtime register of the new calibration data type
#define REGISTER_CALIB_DATA_TYPE(T, alias, subClass)                            \
    static const na64dp::calib::Dispatcher::CIDataID _calibDataTypeID_ ## alias \
        = ::na64dp::calib::CIDataAliases::self().add_alias_of<T>( \
                # alias, subClass );

/**\brief Provides highest level API to access calibration data of all types
 *
 * Manages runtime calibration data fetch, retrieval, storage and
 * on-change notifications.
 *
 * Has multiple instances of run indexes and calibration data loader imposed
 * at a runtime. The first kind of entities implements interface
 * `iIndex` that shall define what kind of calibration data must be
 * updated and from which source. The former (loader) will load it.
 *
 * Order of insertion of the run indexes matters in case of collisions:
 * typically the last inserted will displace updates imposed by previous. This
 * behaviour is not strict and defined by index: `iIndex` instances
 * get the reentrant updates dictionary and may abstain from displacing
 * calibration data, depending on implementation (though, displacing is
 * implied).
 *
 * Manager does not maintain the lifetime run indexes and loaders.
 *
 * There must be a single instance of the manager in process.
 *
 * \todo Threading synchronization primitives as it can be accessed from
 * different threads.
 * */
class Manager : public Dispatcher
              {
protected:
    /// Ordered sequence of indeces to collect the updates
    std::vector<std::shared_ptr<iIndex>> _indeces;
    /// Ordered list of loaders to load the update; names used by some kind
    /// of indexes
    std::vector<std::pair<std::string, std::shared_ptr<iLoader>>> _loaders;
    /// Index of loaders by name (used for aux utils)
    std::unordered_map<std::string, std::shared_ptr<iLoader>> _loadersByName;

    /// Current event ID
    EventID _cEventID;
    /// Date+time of the current event (or 0)
    std::pair<time_t, uint32_t> _cDatetime;
protected:
    /// Collects updates from runs indexes returning a list of updates that
    /// have to be applied at certain event ID. Does not modify the state of
    /// calibration data.
    void _collect_updates( Updates & upds
                         , EventID eventID
                         , const std::pair<time_t, uint32_t> & eventTime
                         ) const;

    /// Sorts updates taking into account their dependencies
    ///
    /// Updates list not necessarily contains all the dependencies of depndee.
    /// They must be loaded beforehead (in previous invokations), so this
    /// method does not controll all the dependencies are satisfied, it just
    /// re-shuffles updates it got in a proper order.
    void _sort_updates( Updates & );

    /// Forwards update loading to appropriate loader
    void _load_updates( const Updates & updates );
public:
    Manager(log4cpp::Category & L);
    virtual ~Manager();

    /// Returns current run number
    EventID event_id() const { return _cEventID; }
    /// Returns time structure associated with the current event
    const std::pair<time_t, uint32_t> time() const { return _cDatetime; }

    /// Sets current event ID
    ///
    /// Forwards execution to `collect_updates()` and, if at least one update
    /// was found, calls `load_updates()`. Then sets the `_cEventID`.
    void event_id( EventID, const std::pair<time_t, uint32_t> & );
    

    void add_index( std::shared_ptr<iIndex> iPtr ) {
        _indeces.push_back( iPtr );
    }

    void add_loader( const std::string & name
                   , std::shared_ptr<iLoader> lPtr
                   ) {
        _loaders.push_back(std::pair<std::string, std::shared_ptr<iLoader>>(name, lPtr));
        _loadersByName.emplace(name, lPtr);
    }

    const std::unordered_map<std::string, std::shared_ptr<iLoader>> &
        loaders_by_name() const { return _loadersByName; }

    ///\brief Returns registered loader identified by name
    ///
    /// Since application usually provides some generalized loaders that have
    /// to be extended at a runtime, user code must be able to retrieve them
    /// for modification (yet, the modification of the list of the loaders
    /// itself is steered by `add_loader()`).
    ///
    /// \throw runtime error if loader is not found or wrong type is requested
    //         for cast.
    template<typename LoaderT>
    LoaderT & get_loader(const std::string & loaderName) {
        auto it = _loadersByName.find(loaderName);
        if( _loadersByName.end() == it ) {
            NA64DP_RUNTIME_ERROR( "No loader \"%s\" known to calibration data"
                    " dispatcher.", loaderName.c_str() );
        }
        try {
            return dynamic_cast<LoaderT&>(*it->second);
        } catch( std::bad_cast & ) {
            NA64DP_RUNTIME_ERROR( "Bad downcast of loader \"%s\"."
                                , loaderName.c_str()
                                );
        }
    }

    /// Dumps calibration data hierarchy to YAML node for inspection
    void to_yaml( YAML::Node & ) const;

    #if 0
    /// Adds calibration data loader instance
    virtual void add_loader( const std::string & loaderName
                           , iLoader * loaderPtr );
    /// Adds run index instance
    virtual void add_run_index( iIndex * runIndex );

    /// Returns reference to the list of registered loaders
    const std::unordered_map<std::string, iLoader *> & loaders() const { return _ciLoaders; }
    #endif
};

}  // namespace ::na64dp::calib
}  // namespace na64dp

