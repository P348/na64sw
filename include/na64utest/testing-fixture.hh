#pragma once

#include "na64sw-config.h"

#if defined(GTEST_FOUND) && GTEST_FOUND

#include <gtest/gtest.h>

#include "na64event/data/event.hh"

namespace na64dp {
namespace test {

// Common fixture with event buffer for event data creation, compatible with gtest
class BasicEventDataOperations : public ::testing::Test {
private:
    size_t _bufferSize;
    void * _bufferPtr;
    na64dp::mem::PlainBlock * _plainBlockPtr;
    LocalMemory * _lmemPtr;
protected:
    // Init ptr
    BasicEventDataOperations();
    // Free memory block for event data
    void TearDown() override;
    void _reallocate_event_buffer(size_t newSize);
public:
    LocalMemory & lmem();
    virtual ~BasicEventDataOperations() {}
};  // class BasicEventDataOperations

}  // namespace ::na64dp::test
}  // namespace na64dp

#endif  // GTEST_FOUND

